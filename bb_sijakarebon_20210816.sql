# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.36-MariaDB)
# Database: bb_sijakarebon_20210428
# Generation Time: 2021-08-16 02:35:01 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table _postcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postcategories`;

CREATE TABLE `_postcategories` (
  `PostCategoryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryName` varchar(50) NOT NULL,
  `PostCategoryLabel` varchar(50) DEFAULT NULL,
  `IsShowEditor` tinyint(1) NOT NULL DEFAULT '1',
  `IsAllowExternalURL` tinyint(1) NOT NULL DEFAULT '0',
  `IsDocumentOnly` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`PostCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_postcategories` WRITE;
/*!40000 ALTER TABLE `_postcategories` DISABLE KEYS */;

INSERT INTO `_postcategories` (`PostCategoryID`, `PostCategoryName`, `PostCategoryLabel`, `IsShowEditor`, `IsAllowExternalURL`, `IsDocumentOnly`)
VALUES
	(1,'Berita','#f56954',1,0,0),
	(2,'Galeri','#00a65a',0,0,0),
	(3,'Dokumen','#f39c12',0,0,1),
	(5,'Lainnya','#3c8dbc',1,0,0);

/*!40000 ALTER TABLE `_postcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _postimages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postimages`;

CREATE TABLE `_postimages` (
  `PostImageID` bigint(10) NOT NULL AUTO_INCREMENT,
  `PostID` bigint(10) NOT NULL,
  `FileName` varchar(250) NOT NULL,
  `Description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`PostImageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_postimages` WRITE;
/*!40000 ALTER TABLE `_postimages` DISABLE KEYS */;

INSERT INTO `_postimages` (`PostImageID`, `PostID`, `FileName`, `Description`)
VALUES
	(14,6,'Penguins.jpg',NULL),
	(15,6,'Lighthouse.jpg',NULL),
	(19,8,'pf1.jpeg',NULL),
	(20,9,'pf2.jpeg',NULL),
	(21,10,'Lighthouse1.jpg',NULL),
	(22,10,'Koala.jpg',NULL),
	(23,11,'Bukti_pengisian_SP2020_Online.pdf',NULL),
	(24,12,'JICOMFEST.pdf',NULL);

/*!40000 ALTER TABLE `_postimages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_posts`;

CREATE TABLE `_posts` (
  `PostID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryID` int(10) NOT NULL,
  `PostDate` date NOT NULL,
  `PostTitle` varchar(200) NOT NULL,
  `PostSlug` varchar(200) NOT NULL,
  `PostContent` longtext,
  `PostExpiredDate` date NOT NULL,
  `TotalView` int(11) NOT NULL DEFAULT '0',
  `LastViewDate` datetime DEFAULT NULL,
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '1',
  `FileName` varchar(250) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  PRIMARY KEY (`PostID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_posts` WRITE;
/*!40000 ALTER TABLE `_posts` DISABLE KEYS */;

INSERT INTO `_posts` (`PostID`, `PostCategoryID`, `PostDate`, `PostTitle`, `PostSlug`, `PostContent`, `PostExpiredDate`, `TotalView`, `LastViewDate`, `IsSuspend`, `FileName`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(6,1,'2021-08-15','Lorem Ipsum','lorem-ipsum','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n','2022-12-31',6,NULL,0,NULL,'admin','2021-08-15 14:11:44','admin','2021-08-15 14:23:29'),
	(8,2,'2021-08-15','By Developer','by-developer',NULL,'2022-12-31',0,NULL,0,NULL,'admin','2021-08-15 16:43:14','admin','2021-08-15 16:43:14'),
	(9,2,'2021-08-15','By Developer 2','by-developer-2',NULL,'2022-12-31',0,NULL,0,NULL,'admin','2021-08-15 16:43:31','admin','2021-08-15 16:43:31'),
	(10,2,'2021-08-15','By Developer 3','by-developer-3',NULL,'2022-12-31',0,NULL,0,NULL,'admin','2021-08-15 16:44:49','admin','2021-08-15 16:44:49'),
	(11,3,'2021-08-15','Dokumen 2','dokumen-2',NULL,'2022-12-31',0,NULL,0,NULL,'admin','2021-08-15 21:59:09','admin','2021-08-15 21:59:09'),
	(12,3,'2021-08-15','Dokumen 1','dokumen-1',NULL,'2022-12-31',0,NULL,0,NULL,'admin','2021-08-15 22:04:01','admin','2021-08-15 22:04:01');

/*!40000 ALTER TABLE `_posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_roles`;

CREATE TABLE `_roles` (
  `RoleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(50) NOT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_roles` WRITE;
/*!40000 ALTER TABLE `_roles` DISABLE KEYS */;

INSERT INTO `_roles` (`RoleID`, `RoleName`)
VALUES
	(1,'Administrator'),
	(2,'Bappeda'),
	(3,'Operator OPD'),
	(4,'Operator Bidang OPD'),
	(5,'Operator Sub Bidang OPD'),
	(6,'Operator Keuangan');

/*!40000 ALTER TABLE `_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_settings`;

CREATE TABLE `_settings` (
  `SettingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`SettingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_settings` WRITE;
/*!40000 ALTER TABLE `_settings` DISABLE KEYS */;

INSERT INTO `_settings` (`SettingID`, `SettingLabel`, `SettingName`, `SettingValue`)
VALUES
	(1,'SETTING_WEB_NAME','SETTING_WEB_NAME','BAGIAN ORGANISASI SETDA KAB. BATU BARA'),
	(2,'SETTING_WEB_DESC','SETTING_WEB_DESC','Portal Informasi dan Aplikasi\nBagian Organisasi Setda Kab. Batu Bara'),
	(3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','https://general-9.disqus.com/embed.js'),
	(4,'SETTING_ORG_NAME','SETTING_ORG_NAME','SETDA KAB. BATU BARA\nBAGIAN ORGANISASI'),
	(5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','Jl. Perintis Kemerdekaan No. 164, Lima Puluh Kota'),
	(6,'SETTING_ORG_LAT','SETTING_ORG_LAT',''),
	(7,'SETTING_ORG_LONG','SETTING_ORG_LONG',''),
	(8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','(0622) 96765'),
	(9,'SETTING_ORG_FAX','SETTING_ORG_FAX','-'),
	(10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','pemkab@batubarakab.go.id'),
	(11,'SETTING_WEB_API_FOOTERLINK','SETTING_WEB_API_FOOTERLINK','-'),
	(12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','logo.png'),
	(13,'SETTING_WEB_SKIN_CLASS','SETTING_WEB_SKIN_CLASS','skin-green-light'),
	(14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','loader-128x/Preloader_1.gif'),
	(15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.0'),
	(16,'SETTING_ORG_REGION','SETTING_ORG_REGION','PEMERINTAH KABUPATEN BATU BARA');

/*!40000 ALTER TABLE `_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _userinformation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_userinformation`;

CREATE TABLE `_userinformation` (
  `UserName` varchar(50) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `CompanyID` varchar(200) DEFAULT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `IdentityNo` varchar(50) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `ReligionID` int(10) DEFAULT NULL,
  `Gender` tinyint(1) DEFAULT NULL,
  `Address` text,
  `PhoneNumber` varchar(50) DEFAULT NULL,
  `EducationID` int(10) DEFAULT NULL,
  `UniversityName` varchar(50) DEFAULT NULL,
  `FacultyName` varchar(50) DEFAULT NULL,
  `MajorName` varchar(50) DEFAULT NULL,
  `IsGraduated` tinyint(1) NOT NULL DEFAULT '0',
  `GraduatedDate` date DEFAULT NULL,
  `YearOfExperience` int(10) DEFAULT NULL,
  `RecentPosition` varchar(250) DEFAULT NULL,
  `RecentSalary` double DEFAULT NULL,
  `ExpectedSalary` double DEFAULT NULL,
  `CVFilename` varchar(250) DEFAULT NULL,
  `ImageFilename` varchar(250) DEFAULT NULL,
  `RegisteredDate` date DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_userinformation` WRITE;
/*!40000 ALTER TABLE `_userinformation` DISABLE KEYS */;

INSERT INTO `_userinformation` (`UserName`, `Email`, `CompanyID`, `Name`, `IdentityNo`, `BirthDate`, `ReligionID`, `Gender`, `Address`, `PhoneNumber`, `EducationID`, `UniversityName`, `FacultyName`, `MajorName`, `IsGraduated`, `GraduatedDate`, `YearOfExperience`, `RecentPosition`, `RecentSalary`, `ExpectedSalary`, `CVFilename`, `ImageFilename`, `RegisteredDate`)
VALUES
	('admin','yoelrolas@gmail.com',NULL,'Administrator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-17'),
	('org.airputih','org.airputih','7.1.8.35','KECAMATAN AIR PUTIH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.bappeda','org.bappeda','5.1.5.5','BADAN PERENCANAAN PEMBANGUNAN DAERAH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.bkd','org.bkd','5.3.5.4','BADAN KEPEGAWAIAN DAERAH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.bpbd','org.bpbd','1.5.5.5','BADAN PENANGGULANGAN BENCANA DAERAH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.bpkad','org.bpkad','5.2.24.24','BADAN PENGELOLAAN KEUANGAN DAN ASSET DAERAH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.bpprd','org.bpprd','5.2.23.23','BADAN PENGELOLAAN PAJAK DAN RETRIBUSI DAERAH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.datuklimapuluh','org.datuklimapuluh','7.1.8.40','KECAMATAN DATUK LIMA PULUH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.datuktanahdatar','org.datuktanahdatar','7.1.8.41','KECAMATAN DATUK TANAH DATAR',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.dinaskebersihan','org.dinaskebersihan','2.11.6.6','DINAS LINGKUNGAN HIDUP, KEBERSIHAN DAN PERTAMANAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.dinaspendidikan','org.dinaspendidikan','1.1.2.22','DINAS PENDIDIKAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.dinkes','org.dinkes','1.2.16.16','DINAS KESEHATAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.dinsos','org.dinsos','1.6.19.19','DINAS SOSIAL',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.disdukcapil','org.disdukcapil','2.12.11.11','DINAS KEPENDUDUKAN DAN PENCATATAN SIPIL',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.dishub','org.dishub','2.15.5.1','DINAS PERHUBUNGAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.diskominfo','org.diskominfo','2.16.2.20','DINAS KOMUNIKASI DAN INFORMATIKA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.diskopukm','org.diskopukm','2.17.3.30','DINAS KOPERASI, USAHA KECIL DAN MENENGAH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.disnaker','org.disnaker','2.7.25.25','DINAS KETENAGAKERJAAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.disperindag','org.disperindag','3.31.3.30','DINAS PERINDUSTRIAN DAN PERDAGANGAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.dispora','org.dispora','2.19.3.26','DINAS KEPEMUDAAN, OLAHRAGA DAN PARIWISATA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.dpmd','org.dpmd','2.13.21.21','DINAS PEMBERDAYAAN MASYARAKAT DAN DESA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.dpmptsp','org.dpmptsp','2.18.10.10','DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.dppkb','org.dppkb','2.14.2.8','DINAS PENGENDALIAN PENDUDUK, KELUARGA BERENCANA, PEMBERDAYAAN PEREMPUAN DAN PERLINDUNGAN ANAK',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.inspektorat','org.inspektorat','6.1.13.13','INSPEKTORAT',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.kesbangpol','org.kesbangpol','1.5.8.31','BADAN KESATUAN BANGSA DAN POLITIK',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.ketapang','org.ketapang','3.27.2.9','DINAS TANAMAN PANGAN, HORTIKULTURA DAN KETAHANAN PANGAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.lauttador','org.lauttador','7.1.8.43','KECAMATAN LAUT TADOR',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.limapuluh','org.limapuluh','7.1.8.32','KECAMATAN LIMA PULUH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.limapuluhpesisir','org.limapuluhpesisir','7.1.8.39','KECAMATAN LIMA PULUH PESISIR',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.medangderas','org.medangderas','7.1.8.37','KECAMATAN MEDANG DERAS',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.nibunghangus','org.nibunghangus','7.1.8.42','KECAMATAN NIBUNG HANGUS',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.perikanan','org.perikanan','3.25.29.29','DINAS PERIKANAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.perkim','org.perkim','1.4.2.10','DINAS PERUMAHAN DAN KAWASAN PEMUKIMAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.perpustakaan','org.perpustakaan','2.23.2.24','DINAS PERPUSTAKAAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.peternakan','org.peternakan','3.27.3.30','DINAS PETERNAKAN DAN PERKEBUNAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.pupr','org.pupr','1.3.1.1','DINAS PEKERJAAN UMUM DAN PENATAAN RUAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.satpolpp','org.satpolpp','1.5.8.8','SATUAN POLISI PAMONG PRAJA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.seibalai','org.seibalai','7.1.8.38','KECAMATAN SEI BALAI',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.seisuka','org.seisuka','7.1.8.36','KECAMATAN SEI SUKA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.sekdprd','org.sekdprd','4.2.9.9','SEKRETARIAT DPRD',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.talawi','org.talawi','7.1.8.33','KECAMATAN TALAWI',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('org.tanjungtiram','org.tanjungtiram','7.1.8.34','KECAMATAN TANJUNG TIRAM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('setdakab','setdakab@batubarakab.go.id','4.1.5.7','Operator Setdakab',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-04-09');

/*!40000 ALTER TABLE `_userinformation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_users`;

CREATE TABLE `_users` (
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_users` WRITE;
/*!40000 ALTER TABLE `_users` DISABLE KEYS */;

INSERT INTO `_users` (`UserName`, `Password`, `RoleID`, `IsSuspend`, `LastLogin`, `LastLoginIP`)
VALUES
	('admin','3798e989b41b858040b8b69aa6f2ce90',1,0,'2021-08-15 21:56:30','::1'),
	('org.airputih','0ee9882773aa2f89cf6d89a277e8782e',3,0,NULL,NULL),
	('org.bappeda','c48c3df090b2ac4f4f9e842629d7f7cd',3,0,NULL,NULL),
	('org.bkd','b8027c7d04084908008838df126c244a',3,0,NULL,NULL),
	('org.bpbd','fbaf58b654c24890d122e1691a3044ac',3,0,NULL,NULL),
	('org.bpkad','8ba161bb70c0f4201b91ea90a0601887',3,0,NULL,NULL),
	('org.bpprd','78dbf23c4e54ab2721f5ad363773736a',3,0,NULL,NULL),
	('org.datuklimapuluh','d8b74cc53d20a8c934f24257d4434597',3,0,NULL,NULL),
	('org.datuktanahdatar','ecfc30f59ffc73df0fbf282c68ee8950',3,0,NULL,NULL),
	('org.dinaskebersihan','1e1645907568a525ef93948cc18d4099',3,0,NULL,NULL),
	('org.dinaspendidikan','612360a7b349b4f085cf07bc9b647d68',3,0,NULL,NULL),
	('org.dinkes','fd38597772ccda33782345f3eb77b39e',3,0,NULL,NULL),
	('org.dinsos','c22ccec74d98c773ba96c2ca8125c3a0',3,0,NULL,NULL),
	('org.disdukcapil','cc98061d8cb29a00a231f75a551a6486',3,0,NULL,NULL),
	('org.dishub','e64c64bd1a74bc6ea47c6ed68801c860',3,0,NULL,NULL),
	('org.diskominfo','dca919b54ec6cb0b4bfb125384861b41',3,0,NULL,NULL),
	('org.diskopukm','b22bc362fb8a035822bb8532aecdd796',3,0,NULL,NULL),
	('org.disnaker','d453af489e719b5bc8bf8bcf7609cec6',3,0,NULL,NULL),
	('org.disperindag','ffecceba559f68feb6a52d132306a123',3,0,NULL,NULL),
	('org.dispora','147e6c47fcd0f035a5019920f7a4a4fb',3,0,NULL,NULL),
	('org.dpmd','90dd874cabb897ffb133704d394c777a',3,0,NULL,NULL),
	('org.dpmptsp','20c77577d373c5d940345985c6fa7bfd',3,0,NULL,NULL),
	('org.dppkb','2bdd80acc317a9d9b0b90f658563fe7a',3,0,NULL,NULL),
	('org.inspektorat','f8399c7d53b95893fdc3e9e6c7a3e710',3,0,NULL,NULL),
	('org.kesbangpol','ef4491c0ad5a41735ef6fea3aac2cbd7',3,0,NULL,NULL),
	('org.ketapang','4c49ada104377595556c981d0db7a268',3,0,NULL,NULL),
	('org.lauttador','dfecd03e46f4b445802a2328d9b8ed82',3,0,NULL,NULL),
	('org.limapuluh','3bb16057915de2facc6d51af07ccd495',3,0,'2021-04-28 10:53:33','::1'),
	('org.limapuluhpesisir','c279f566926bbb0ad72190ade9b0a555',3,0,NULL,NULL),
	('org.medangderas','8651010e1e63b69b04eec5000a7e1359',3,0,NULL,NULL),
	('org.nibunghangus','9afe649c09d8878a96658d85aa073704',3,0,NULL,NULL),
	('org.perikanan','985773f144bb9544dfaf1fd52e241104',3,0,NULL,NULL),
	('org.perkim','9118e241960eab612549e9f6e8039075',3,0,NULL,NULL),
	('org.perpustakaan','30b00541c4d45f11c56ca51655cf5307',3,0,NULL,NULL),
	('org.peternakan','dad6f3187efd7a4bb271e5f02d9a4892',3,0,NULL,NULL),
	('org.pupr','f8db96aadb22a3630bd303c7e11e3338',3,0,NULL,NULL),
	('org.satpolpp','fcb86242ec40dd891d7b929a5d4423b8',3,0,NULL,NULL),
	('org.seibalai','95e66b14e5ff4561fe3105698c4929bb',3,0,NULL,NULL),
	('org.seisuka','d1b75121f4d4ea1ad3e1414889e35248',3,0,NULL,NULL),
	('org.sekdprd','d970e729adbc308a8c2551a6901170e9',3,0,NULL,NULL),
	('org.talawi','f5fad089089c96aec338197fa5d0ffa3',3,0,NULL,NULL),
	('org.tanjungtiram','2ce4c60ee4389dde535beced27d53bd1',3,0,NULL,NULL),
	('setdakab','e10adc3949ba59abbe56e057f20f883e',3,0,'2021-04-28 10:49:07','::1');

/*!40000 ALTER TABLE `_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ref_sub_unit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ref_sub_unit`;

CREATE TABLE `ref_sub_unit` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Nm_Sub_Unit` varchar(255) NOT NULL,
  `Nm_Pimpinan` varchar(255) DEFAULT NULL,
  `Nm_Kop1` varchar(255) DEFAULT NULL,
  `Nm_Kop2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`) USING BTREE,
  UNIQUE KEY `Uniq` (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ref_sub_unit` WRITE;
/*!40000 ALTER TABLE `ref_sub_unit` DISABLE KEYS */;

INSERT INTO `ref_sub_unit` (`Uniq`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Nm_Sub_Unit`, `Nm_Pimpinan`, `Nm_Kop1`, `Nm_Kop2`)
VALUES
	(33,1,1,2,22,'Dinas Pendidikan','-',NULL,NULL),
	(34,1,2,16,16,'Dinas Kesehatan','-',NULL,NULL),
	(35,1,3,1,1,'Dinas Pekerjaan Umum dan Penataan Ruan','-',NULL,NULL),
	(36,1,4,2,10,'Dinas Perumahan dan Kawasan Pemukiman','-',NULL,NULL),
	(37,1,5,5,5,'Badan Penanggulangan Bencana Daerah','-',NULL,NULL),
	(39,1,5,8,8,'Satuan Polisi Pamong Praja','-',NULL,NULL),
	(38,1,5,8,31,'Badan Kesatuan Bangsa dan Politik','-',NULL,NULL),
	(40,1,6,19,19,'Dinas Sosial','-',NULL,NULL),
	(41,2,7,25,25,'Dinas Ketenagakerjaan','-',NULL,NULL),
	(42,2,11,6,6,'Dinas Lingkungan Hidup, Kebersihan dan Pertamanan','-',NULL,NULL),
	(43,2,12,11,11,'Dinas Kependudukan dan Pencatatan Sipil','-',NULL,NULL),
	(44,2,13,21,21,'Dinas Pemberdayaan Masyarakat dan Desa','-',NULL,NULL),
	(45,2,14,2,8,'Dinas Pengendalian Penduduk, Keluarga Berencana, Pemberdayaan Perempuan dan Perlindungan Anak','-',NULL,NULL),
	(46,2,15,5,1,'Dinas Perhubungan','-',NULL,NULL),
	(47,2,16,2,20,'Dinas Komunikasi dan Informatika','-',NULL,NULL),
	(48,2,17,3,30,'Dinas Koperasi, Usaha Kecil dan Menengah','-',NULL,NULL),
	(49,2,18,10,10,'Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu','-',NULL,NULL),
	(50,2,19,3,26,'Dinas Kepemudaan, Olahraga dan Pariwisata','-',NULL,NULL),
	(51,2,23,2,24,'Dinas Perpustakaan','-',NULL,NULL),
	(52,3,25,29,29,'Dinas Perikanan','-',NULL,NULL),
	(53,3,27,2,9,'Dinas Tanaman Pangan, Hortikultura dan Ketahanan Pangan','-',NULL,NULL),
	(54,3,27,3,30,'Dinas Peternakan dan Perkebunan','-',NULL,NULL),
	(55,3,31,3,30,'Dinas Perindustrian dan Perdagangan','-',NULL,NULL),
	(32,4,1,5,7,'Sekretariat Daerah Kabupaten','H. Sakti Alam Siregar, SH',NULL,NULL),
	(56,4,2,9,9,'Sekretariat DPRD','-',NULL,NULL),
	(57,5,1,5,5,'Badan Perencanaan Pembangunan Daerah','-',NULL,NULL),
	(58,5,2,23,23,'Badan Pengelolaan Pajak dan Retribusi Daerah','-',NULL,NULL),
	(59,5,2,24,24,'Badan Pengelolaan Keuangan dan Asset Daerah','-',NULL,NULL),
	(60,5,3,5,4,'Badan Kepegawaian Daerah','-',NULL,NULL),
	(61,6,1,13,13,'Inspektorat','-',NULL,NULL),
	(62,7,1,8,32,'Kecamatan Lima Puluh','-',NULL,NULL),
	(63,7,1,8,33,'Kecamatan Talawi','-',NULL,NULL),
	(64,7,1,8,34,'Kecamatan Tanjung Tiram','-',NULL,NULL),
	(65,7,1,8,35,'Kecamatan Air Putih','-',NULL,NULL),
	(66,7,1,8,36,'Kecamatan Sei Suka','-',NULL,NULL),
	(67,7,1,8,37,'Kecamatan Medang Deras','-',NULL,NULL),
	(68,7,1,8,38,'Kecamatan Sei Balai','-',NULL,NULL),
	(69,7,1,8,39,'Kecamatan Lima Puluh Pesisir','-',NULL,NULL),
	(70,7,1,8,40,'Kecamatan Datuk Lima Puluh','-',NULL,NULL),
	(71,7,1,8,41,'Kecamatan Datuk Tanah Datar','-',NULL,NULL),
	(72,7,1,8,42,'Kecamatan Nibung Hangus','-',NULL,NULL),
	(73,7,1,8,43,'Kecamatan Laut Tador','-',NULL,NULL);

/*!40000 ALTER TABLE `ref_sub_unit` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_dpa_kegiatan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_dpa_kegiatan`;

CREATE TABLE `sakip_dpa_kegiatan` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Nm_KegiatanOPD` varchar(200) NOT NULL,
  `Is_Renja` tinyint(1) NOT NULL,
  `Total` double NOT NULL,
  `Budget` double DEFAULT NULL,
  `Pergeseran` double DEFAULT NULL,
  `Budget_TW1` double DEFAULT NULL,
  `Budget_TW2` double DEFAULT NULL,
  `Budget_TW3` double DEFAULT NULL,
  `Budget_TW4` double DEFAULT NULL,
  `Anggaran_TW1` double DEFAULT NULL,
  `Anggaran_TW2` double DEFAULT NULL,
  `Anggaran_TW3` double DEFAULT NULL,
  `Anggaran_TW4` double DEFAULT NULL,
  `Kd_SumberDana` varchar(200) NOT NULL,
  `Create_By` varchar(200) NOT NULL,
  `Create_Date` datetime NOT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`),
  KEY `FK_Subbid2` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FK_DPA_SasaranProgram2` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) REFERENCES `sakip_dpa_program_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_Subbid2` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_dpa_kegiatan` WRITE;
/*!40000 ALTER TABLE `sakip_dpa_kegiatan` DISABLE KEYS */;

INSERT INTO `sakip_dpa_kegiatan` (`Uniq`, `Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Nm_KegiatanOPD`, `Is_Renja`, `Total`, `Budget`, `Pergeseran`, `Budget_TW1`, `Budget_TW2`, `Budget_TW3`, `Budget_TW4`, `Anggaran_TW1`, `Anggaran_TW2`, `Anggaran_TW3`, `Anggaran_TW4`, `Kd_SumberDana`, `Create_By`, `Create_Date`, `Edit_By`, `Edit_Date`)
VALUES
	(1,1,1,1,1,1,2,2021,4,1,5,7,1,1,1,3,1,1,1,1,21303,'Peningkatan Kinerja dan Reformasi Birokrasi',1,0,200000000,NULL,0,50000000,0,150000000,0,47000000,0,150000000,'DAU','','0000-00-00 00:00:00',NULL,NULL);

/*!40000 ALTER TABLE `sakip_dpa_kegiatan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_dpa_kegiatan_grup
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_dpa_kegiatan_grup`;

CREATE TABLE `sakip_dpa_kegiatan_grup` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Nm_GrupKegiatanOPD` varchar(200) NOT NULL DEFAULT '',
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_SUBBID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_dpa_kegiatan_indikator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_dpa_kegiatan_indikator`;

CREATE TABLE `sakip_dpa_kegiatan_indikator` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Kd_SasaranKegiatanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorKegiatanOPD` bigint(10) NOT NULL,
  `Nm_IndikatorKegiatanOPD` varchar(200) NOT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  `Nm_PenanggungJawab` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_TW1` double DEFAULT NULL,
  `Target_TW2` double DEFAULT NULL,
  `Target_TW3` double DEFAULT NULL,
  `Target_TW4` double DEFAULT NULL,
  `Kinerja_TW1` double DEFAULT NULL,
  `Kinerja_TW2` double DEFAULT NULL,
  `Kinerja_TW3` double DEFAULT NULL,
  `Kinerja_TW4` double DEFAULT NULL,
  `Anggaran_TW1` double DEFAULT NULL,
  `Anggaran_TW2` double DEFAULT NULL,
  `Anggaran_TW3` double DEFAULT NULL,
  `Anggaran_TW4` double DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`,`Kd_SasaranKegiatanOPD`,`Kd_IndikatorKegiatanOPD`),
  CONSTRAINT `FK_DPA_SasaranKeg` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Kd_SasaranKegiatanOPD`) REFERENCES `sakip_dpa_kegiatan_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Kd_SasaranKegiatanOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_dpa_kegiatan_indikator` WRITE;
/*!40000 ALTER TABLE `sakip_dpa_kegiatan_indikator` DISABLE KEYS */;

INSERT INTO `sakip_dpa_kegiatan_indikator` (`Uniq`, `Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Kd_SasaranKegiatanOPD`, `Kd_IndikatorKegiatanOPD`, `Nm_IndikatorKegiatanOPD`, `Nm_Formula`, `Nm_SumberData`, `Nm_PenanggungJawab`, `Kd_Satuan`, `Target`, `Target_TW1`, `Target_TW2`, `Target_TW3`, `Target_TW4`, `Kinerja_TW1`, `Kinerja_TW2`, `Kinerja_TW3`, `Kinerja_TW4`, `Anggaran_TW1`, `Anggaran_TW2`, `Anggaran_TW3`, `Anggaran_TW4`)
VALUES
	(1,1,1,1,1,1,2,2021,4,1,5,7,1,1,1,3,1,1,1,1,21303,1,1,'Jumlah Dokumen Road Map Reformasi Birokrasi','-','-',NULL,'Dokumen',1,0,0,0,1,0,0,0,1,NULL,NULL,NULL,NULL),
	(2,1,1,1,1,1,2,2021,4,1,5,7,1,1,1,3,1,1,1,1,21303,1,2,'Jumlah Laporan Capaian Kinerja','-','-',NULL,'Aplikasi',1,0,1,0,0,0,1,0,0,NULL,NULL,NULL,NULL),
	(3,1,1,1,1,1,2,2021,4,1,5,7,1,1,1,3,1,1,1,1,21303,1,3,'Jumlah Laporan Monitoring dan Evaluasi Akuntabilitas Kinerja dan Reformasi Birokrasi Capaian Kinerja','-','-',NULL,'Dokumen',1,0,0,0,1,0,0,0,1,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `sakip_dpa_kegiatan_indikator` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_dpa_kegiatan_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_dpa_kegiatan_sasaran`;

CREATE TABLE `sakip_dpa_kegiatan_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Kd_SasaranKegiatanOPD` bigint(10) NOT NULL,
  `Nm_SasaranKegiatanOPD` varchar(200) NOT NULL,
  `Kd_Satuan` varchar(50) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_TW1` double DEFAULT NULL,
  `Target_TW2` double DEFAULT NULL,
  `Target_TW3` double DEFAULT NULL,
  `Target_TW4` double DEFAULT NULL,
  `Budget` double DEFAULT NULL,
  `Budget_TW1` double DEFAULT NULL,
  `Budget_TW2` double DEFAULT NULL,
  `Budget_TW3` double DEFAULT NULL,
  `Budget_TW4` double DEFAULT NULL,
  `Kinerja_TW1` double DEFAULT NULL,
  `Kinerja_TW2` double DEFAULT NULL,
  `Kinerja_TW3` double DEFAULT NULL,
  `Kinerja_TW4` double DEFAULT NULL,
  `Anggaran_TW1` double DEFAULT NULL,
  `Anggaran_TW2` double DEFAULT NULL,
  `Anggaran_TW3` double DEFAULT NULL,
  `Anggaran_TW4` double DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`,`Kd_SasaranKegiatanOPD`),
  KEY `FK_DPA_Subbid` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FK_DPA_Kegiatan` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`) REFERENCES `sakip_dpa_kegiatan` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_DPA_Subbid` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_dpa_kegiatan_sasaran` WRITE;
/*!40000 ALTER TABLE `sakip_dpa_kegiatan_sasaran` DISABLE KEYS */;

INSERT INTO `sakip_dpa_kegiatan_sasaran` (`Uniq`, `Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Kd_SasaranKegiatanOPD`, `Nm_SasaranKegiatanOPD`, `Kd_Satuan`, `Target`, `Target_TW1`, `Target_TW2`, `Target_TW3`, `Target_TW4`, `Budget`, `Budget_TW1`, `Budget_TW2`, `Budget_TW3`, `Budget_TW4`, `Kinerja_TW1`, `Kinerja_TW2`, `Kinerja_TW3`, `Kinerja_TW4`, `Anggaran_TW1`, `Anggaran_TW2`, `Anggaran_TW3`, `Anggaran_TW4`)
VALUES
	(1,1,1,1,1,1,2,2021,4,1,5,7,1,1,1,3,1,1,1,1,21303,1,'Meningkatnya Kinerja dan Reformasi Birokrasi',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `sakip_dpa_kegiatan_sasaran` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_dpa_program
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_dpa_program`;

CREATE TABLE `sakip_dpa_program` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Nm_ProgramOPD` varchar(200) NOT NULL,
  `Is_Renja` tinyint(1) NOT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`),
  KEY `FK_DPA_Bid` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `FK_DPA_Bid` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_dpa_program` WRITE;
/*!40000 ALTER TABLE `sakip_dpa_program` DISABLE KEYS */;

INSERT INTO `sakip_dpa_program` (`Uniq`, `Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Nm_ProgramOPD`, `Is_Renja`, `Create_By`, `Create_Date`, `Edit_By`, `Edit_Date`)
VALUES
	(1,1,1,1,1,1,2,2021,4,1,5,7,1,1,1,3,1,1,'Program Penunjang Urusan Pemerintahan Daerah Kabupaten / Kota',1,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `sakip_dpa_program` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_dpa_program_indikator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_dpa_program_indikator`;

CREATE TABLE `sakip_dpa_program_indikator` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) NOT NULL,
  `Nm_IndikatorProgramOPD` varchar(200) NOT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  `Nm_PenanggungJawab` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_TW1` double DEFAULT NULL,
  `Target_TW2` double DEFAULT NULL,
  `Target_TW3` double DEFAULT NULL,
  `Target_TW4` double DEFAULT NULL,
  `Kinerja_TW1` double DEFAULT NULL,
  `Kinerja_TW2` double DEFAULT NULL,
  `Kinerja_TW3` double DEFAULT NULL,
  `Kinerja_TW4` double DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_IndikatorProgramOPD`),
  CONSTRAINT `FK_DPA_SasaranProgram` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) REFERENCES `sakip_dpa_program_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_dpa_program_indikator` WRITE;
/*!40000 ALTER TABLE `sakip_dpa_program_indikator` DISABLE KEYS */;

INSERT INTO `sakip_dpa_program_indikator` (`Uniq`, `Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_IndikatorProgramOPD`, `Nm_IndikatorProgramOPD`, `Nm_Formula`, `Nm_SumberData`, `Nm_PenanggungJawab`, `Kd_Satuan`, `Target`, `Target_TW1`, `Target_TW2`, `Target_TW3`, `Target_TW4`, `Kinerja_TW1`, `Kinerja_TW2`, `Kinerja_TW3`, `Kinerja_TW4`)
VALUES
	(1,1,1,1,1,1,2,2021,4,1,5,7,1,1,1,3,1,1,1,1,'Nilai EEKPD','-','-',NULL,'Angka',0,0,0,0,100,0,NULL,NULL,NULL),
	(2,1,1,1,1,1,2,2021,4,1,5,7,1,1,1,3,1,1,1,2,'Persentase Kecamatan Berkinerja Baik','-','-',NULL,'Angka',0,0,0,0,100,0,NULL,NULL,NULL),
	(3,1,1,1,1,1,2,2021,4,1,5,7,1,1,1,3,1,1,1,3,'Nilai Survey IKM','-','-',NULL,'Angka',75,0,0,0,75,0,NULL,NULL,NULL),
	(4,1,1,1,1,1,2,2021,4,1,5,7,1,1,1,3,1,1,1,4,'Nilai AKIP','-','-',NULL,'OP',70,0,0,0,70,0,NULL,NULL,NULL);

/*!40000 ALTER TABLE `sakip_dpa_program_indikator` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_dpa_program_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_dpa_program_sasaran`;

CREATE TABLE `sakip_dpa_program_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Nm_SasaranProgramOPD` varchar(200) NOT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_TW1` double DEFAULT NULL,
  `Target_TW2` double DEFAULT NULL,
  `Target_TW3` double DEFAULT NULL,
  `Target_TW4` double DEFAULT NULL,
  `Kinerja_TW1` double DEFAULT NULL,
  `Kinerja_TW2` double DEFAULT NULL,
  `Kinerja_TW3` double DEFAULT NULL,
  `Kinerja_TW4` double DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`),
  KEY `FK_DPA_Bid2` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `DK_DPA_Program2` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`) REFERENCES `sakip_dpa_program` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_DPA_Bid2` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_dpa_program_sasaran` WRITE;
/*!40000 ALTER TABLE `sakip_dpa_program_sasaran` DISABLE KEYS */;

INSERT INTO `sakip_dpa_program_sasaran` (`Uniq`, `Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Nm_SasaranProgramOPD`, `Kd_Satuan`, `Target`, `Target_TW1`, `Target_TW2`, `Target_TW3`, `Target_TW4`, `Kinerja_TW1`, `Kinerja_TW2`, `Kinerja_TW3`, `Kinerja_TW4`)
VALUES
	(1,1,1,1,1,1,2,2021,4,1,5,7,1,1,1,3,1,1,1,'Meningkatkan Penyelenggaraan Pemerintahan Umum dan Otonomi Daerah',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `sakip_dpa_program_sasaran` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_individu_indikator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_individu_indikator`;

CREATE TABLE `sakip_individu_indikator` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) unsigned NOT NULL,
  `Kd_Misi` bigint(10) unsigned NOT NULL,
  `Kd_Tujuan` bigint(10) unsigned NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) unsigned NOT NULL,
  `Kd_Sasaran` bigint(10) unsigned NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) unsigned NOT NULL,
  `Kd_Tahun` bigint(10) unsigned NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) unsigned NOT NULL,
  `Kd_SasaranOPD` bigint(10) unsigned NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) unsigned NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) unsigned NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) unsigned NOT NULL,
  `Kd_SasaranSubbidang` bigint(10) unsigned NOT NULL,
  `Kd_SasaranIndividu` bigint(10) unsigned NOT NULL,
  `Kd_IndikatorIndividu` bigint(10) unsigned NOT NULL,
  `Nm_SasaranIndividu` varchar(255) NOT NULL DEFAULT '',
  `Nm_IndikatorIndividu` varchar(255) NOT NULL DEFAULT '',
  `Nm_Target` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_SasaranProgramOPD`,`Kd_IndikatorProgramOPD`,`Kd_SasaranSubbidang`,`Kd_SasaranIndividu`,`Kd_IndikatorIndividu`) USING BTREE,
  KEY `Uniq` (`Uniq`),
  KEY `FK_SUBBID2` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FK_Indikator_Sasaran` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_SasaranProgramOPD`, `Kd_IndikatorProgramOPD`, `Kd_SasaranSubbidang`) REFERENCES `sakip_individu_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_SasaranProgramOPD`, `Kd_IndikatorProgramOPD`, `Kd_SasaranSubbidang`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_individu_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_individu_sasaran`;

CREATE TABLE `sakip_individu_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) unsigned NOT NULL,
  `Kd_Misi` bigint(10) unsigned NOT NULL,
  `Kd_Tujuan` bigint(10) unsigned NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) unsigned NOT NULL,
  `Kd_Sasaran` bigint(10) unsigned NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) unsigned NOT NULL,
  `Kd_Tahun` bigint(10) unsigned NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) unsigned NOT NULL,
  `Kd_SasaranOPD` bigint(10) unsigned NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) unsigned NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) unsigned NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) unsigned NOT NULL,
  `Kd_SasaranSubbidang` bigint(10) unsigned NOT NULL,
  `Kd_SasaranIndividu` bigint(10) unsigned NOT NULL,
  `Nm_Jabatan` varchar(50) NOT NULL DEFAULT '',
  `Nm_Pegawai` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_SasaranProgramOPD`,`Kd_IndikatorProgramOPD`,`Kd_SasaranSubbidang`,`Kd_SasaranIndividu`) USING BTREE,
  KEY `Uniq` (`Uniq`),
  KEY `FK_Individu_Subbid` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FK_Individu_Subbid` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_mbid
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mbid`;

CREATE TABLE `sakip_mbid` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Nm_Bid` varchar(200) DEFAULT NULL,
  `Nm_Kabid` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  KEY `Uniq` (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_mbid` WRITE;
/*!40000 ALTER TABLE `sakip_mbid` DISABLE KEYS */;

INSERT INTO `sakip_mbid` (`Uniq`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Nm_Bid`, `Nm_Kabid`)
VALUES
	(1,4,1,5,7,1,'Bagian Organisasi','Ihramli Efendi'),
	(2,4,1,5,7,2,'Bagian Kesejahteraan Rakyat','Adnan Haris'),
	(3,4,1,5,7,3,'Bagian Perencanaan dan Keuangan','Agnes Angelia'),
	(4,4,1,5,7,4,'Bagian Tata Pemerintahan','Arif Hanafiah'),
	(5,4,1,5,7,5,'Bagian Umum','Sri Madani'),
	(6,4,1,5,7,6,'Bagian Perekonomian dan Sumber Daya Alam','Edwin Eldrian Sitorus'),
	(7,4,1,5,7,7,'Bagian Pengadaan Barang dan Jasa','Abdu Zahrul'),
	(8,4,1,5,7,8,'Bagian Administrasi Pembangunan','Frengky Siboro'),
	(9,4,1,5,7,9,'Bagian Protokol dan Komunikasi Pimpinan','Ikhwa'),
	(10,4,1,5,7,10,'Bagian Hukum','Rahmat Sirait');

/*!40000 ALTER TABLE `sakip_mbid` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_mbid_indikator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mbid_indikator`;

CREATE TABLE `sakip_mbid_indikator` (
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) DEFAULT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) NOT NULL,
  `Nm_IndikatorProgramOPD` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_SasaranProgramOPD`,`Kd_IndikatorProgramOPD`),
  KEY `FK_BID3` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `FK_BID3` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_BID_SASARAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_SasaranProgramOPD`) REFERENCES `sakip_mbid_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_SasaranProgramOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_mbid_indikator` WRITE;
/*!40000 ALTER TABLE `sakip_mbid_indikator` DISABLE KEYS */;

INSERT INTO `sakip_mbid_indikator` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_IndikatorProgramOPD`, `Nm_IndikatorProgramOPD`)
VALUES
	(1,1,1,1,1,2,2021,4,1,5,7,1,1,1,1,4,NULL,1,1,'Persentase Kecamatan Berkinerja Baik'),
	(1,1,1,1,1,2,2021,4,1,5,7,1,1,1,2,1,NULL,2,1,'Persentase Perangkat Daerah Dengan Nilai Survei IKM Minimal 75 (Kategori Baik)'),
	(1,1,1,1,1,2,2021,4,1,5,7,1,1,1,3,3,NULL,3,1,'Terwujudnya Tatakelola Pemerintahan Yang Baik Dan Reformasi Birokrasi'),
	(1,1,1,1,1,2,2021,4,1,5,7,1,1,2,1,1,NULL,1,1,'IKM Layanan Sekretariat Daerah'),
	(1,1,1,1,1,2,2021,4,1,5,7,1,1,3,1,10,NULL,1,1,'Persentase Rancangan Kebijakan yang disusun sesuai prosedur'),
	(1,1,1,1,1,2,2021,4,1,5,7,1,1,4,1,9,NULL,1,1,'Rasio Berita Baik');

/*!40000 ALTER TABLE `sakip_mbid_indikator` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_mbid_program
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mbid_program`;

CREATE TABLE `sakip_mbid_program` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Nm_ProgramOPD` varchar(200) DEFAULT NULL,
  `Nm_SasaranProgram` varchar(200) DEFAULT NULL,
  `Nm_IndikatorProgram` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_N1` double DEFAULT NULL,
  `IsEplan` tinyint(1) NOT NULL DEFAULT '1',
  `Remarks` varchar(200) DEFAULT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_OPD_IKSASARAN` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`),
  KEY `FK_BID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `FK_BID` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_OPD_IKSASARAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`) REFERENCES `sakip_mopd_iksasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_mbid_program` WRITE;
/*!40000 ALTER TABLE `sakip_mbid_program` DISABLE KEYS */;

INSERT INTO `sakip_mbid_program` (`Uniq`, `Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Nm_ProgramOPD`, `Nm_SasaranProgram`, `Nm_IndikatorProgram`, `Kd_Satuan`, `Awal`, `Target`, `Target_N1`, `IsEplan`, `Remarks`, `Create_By`, `Create_Date`, `Edit_By`, `Edit_Date`)
VALUES
	(1,1,1,1,1,1,2,2021,4,1,5,7,1,1,1,3,1,1,'Program Penunjang Urusan Pemerintahan Daerah Kabupaten / Kota',NULL,NULL,NULL,NULL,NULL,NULL,1,'-',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `sakip_mbid_program` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_mbid_program_indikator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mbid_program_indikator`;

CREATE TABLE `sakip_mbid_program_indikator` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) NOT NULL,
  `Nm_IndikatorProgramOPD` varchar(200) DEFAULT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  `Nm_PenanggungJawab` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Akhir` double DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_IndikatorProgramOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_Bid6` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `FK_Bid6` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_IndikatorSasaran` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) REFERENCES `sakip_mbid_program_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_mbid_program_indikator` WRITE;
/*!40000 ALTER TABLE `sakip_mbid_program_indikator` DISABLE KEYS */;

INSERT INTO `sakip_mbid_program_indikator` (`Uniq`, `Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_IndikatorProgramOPD`, `Nm_IndikatorProgramOPD`, `Nm_Formula`, `Nm_SumberData`, `Nm_PenanggungJawab`, `Kd_Satuan`, `Target`, `Awal`, `Akhir`)
VALUES
	(1,1,1,1,1,1,2,2021,4,1,5,7,1,1,1,3,1,1,1,1,'Nilai EEKPD','-','-',NULL,'Angka',0,0,0),
	(2,1,1,1,1,1,2,2021,4,1,5,7,1,1,1,3,1,1,1,2,'Persentase Kecamatan Berkinerja Baik','-','-',NULL,'Angka',0,0,0),
	(3,1,1,1,1,1,2,2021,4,1,5,7,1,1,1,3,1,1,1,3,'Nilai Survey IKM','-','-',NULL,'Angka',75,0,0),
	(4,1,1,1,1,1,2,2021,4,1,5,7,1,1,1,3,1,1,1,4,'Nilai AKIP','-','-',NULL,'OP',70,0,0);

/*!40000 ALTER TABLE `sakip_mbid_program_indikator` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_mbid_program_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mbid_program_sasaran`;

CREATE TABLE `sakip_mbid_program_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Nm_SasaranProgramOPD` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Akhir` double DEFAULT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_OPD_IKSASARAN` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`),
  KEY `FK_BID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `FK_SasaranBid` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SasaranProgram` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`) REFERENCES `sakip_mbid_program` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_mbid_program_sasaran` WRITE;
/*!40000 ALTER TABLE `sakip_mbid_program_sasaran` DISABLE KEYS */;

INSERT INTO `sakip_mbid_program_sasaran` (`Uniq`, `Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Nm_SasaranProgramOPD`, `Kd_Satuan`, `Awal`, `Target`, `Akhir`, `Create_By`, `Create_Date`, `Edit_By`, `Edit_Date`)
VALUES
	(1,1,1,1,1,1,2,2021,4,1,5,7,1,1,1,3,1,1,1,'Meningkatkan Penyelenggaraan Pemerintahan Umum dan Otonomi Daerah',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `sakip_mbid_program_sasaran` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_mbid_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mbid_sasaran`;

CREATE TABLE `sakip_mbid_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) DEFAULT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Nm_SasaranProgramOPD` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_SasaranProgramOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_BID2` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  KEY `FK_OPD_IKSASARAN_TO_BID_SASARAN` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`),
  CONSTRAINT `FK_BID2` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_OPD_IKSASARAN_TO_BID_SASARAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`) REFERENCES `sakip_mopd_iksasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_mbid_sasaran` WRITE;
/*!40000 ALTER TABLE `sakip_mbid_sasaran` DISABLE KEYS */;

INSERT INTO `sakip_mbid_sasaran` (`Uniq`, `Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Nm_SasaranProgramOPD`)
VALUES
	(1,1,1,1,1,1,2,2021,4,1,5,7,1,1,1,1,4,NULL,1,'Meningkatnya Penyelenggaraan Pemerintahan Umum'),
	(2,1,1,1,1,1,2,2021,4,1,5,7,1,1,1,2,1,NULL,2,'Meningkatnya Penyelenggaraan Pemerintahan Umum'),
	(3,1,1,1,1,1,2,2021,4,1,5,7,1,1,1,3,3,NULL,3,'Meningkatnya Penyelenggaraan Pemerintahan Umum'),
	(4,1,1,1,1,1,2,2021,4,1,5,7,1,1,2,1,1,NULL,1,'Meningkatnya Kualitas Pelayanan Setdakab'),
	(5,1,1,1,1,1,2,2021,4,1,5,7,1,1,3,1,10,NULL,1,'Meningkatnya Kualitas Perumusan Kebijakan Sesuai Prosedur'),
	(6,1,1,1,1,1,2,2021,4,1,5,7,1,1,4,1,9,NULL,1,'Meningkatnya opini Pemerintah Daerah');

/*!40000 ALTER TABLE `sakip_mbid_sasaran` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_mopd_file
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mopd_file`;

CREATE TABLE `sakip_mopd_file` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_Type` varchar(50) NOT NULL,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) DEFAULT NULL,
  `Nm_Keterangan` varchar(200) DEFAULT NULL,
  `Nm_File` varchar(200) NOT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `Uniq` (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_mopd_iksasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mopd_iksasaran`;

CREATE TABLE `sakip_mopd_iksasaran` (
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) DEFAULT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Nm_IndikatorSasaranOPD` varchar(500) DEFAULT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  `Nm_PenanggungJawab` varchar(200) DEFAULT NULL,
  `Nm_Target` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`),
  CONSTRAINT `FK_OPD_SASARAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`) REFERENCES `sakip_mopd_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_mopd_iksasaran` WRITE;
/*!40000 ALTER TABLE `sakip_mopd_iksasaran` DISABLE KEYS */;

INSERT INTO `sakip_mopd_iksasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Nm_IndikatorSasaranOPD`, `Nm_Formula`, `Nm_SumberData`, `Nm_PenanggungJawab`, `Nm_Target`)
VALUES
	(1,1,1,1,1,2,NULL,4,1,5,7,1,1,1,1,'Persentase Kecamatan Berkinerja Baik','-','Bagian Pemerintahan',NULL,'-'),
	(1,1,1,1,1,2,NULL,4,1,5,7,1,1,1,2,'Persentase Perangkat Daerah dengan NIlai Survey IKM Minimal 75','-','Bagian Organisasi',NULL,'-'),
	(1,1,1,1,1,2,NULL,4,1,5,7,1,1,1,3,'Nilai AKIP','-','Bagian Keuangan',NULL,'-'),
	(1,1,1,1,1,2,NULL,4,1,5,7,1,1,2,1,'IKM Layanan Sekretariat Daerah','-','-',NULL,'75'),
	(1,1,1,1,1,2,NULL,4,1,5,7,1,1,3,1,'Persentase Rancangan Kebijakan yang disusun sesuai prosedur','-','Bagian Hukum',NULL,'83'),
	(1,1,1,1,1,2,NULL,4,1,5,7,1,1,4,1,'Rasio Berita Baik','-','-',NULL,'90');

/*!40000 ALTER TABLE `sakip_mopd_iksasaran` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_mopd_iksasaran_capaian
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mopd_iksasaran_capaian`;

CREATE TABLE `sakip_mopd_iksasaran_capaian` (
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Target` varchar(200) DEFAULT NULL,
  `Target_TW1` varchar(200) DEFAULT NULL,
  `Target_TW2` varchar(200) DEFAULT NULL,
  `Target_TW3` varchar(200) DEFAULT NULL,
  `Target_TW4` varchar(200) DEFAULT NULL,
  `Realisasi` varchar(200) DEFAULT NULL,
  `Realisasi_TW1` varchar(200) DEFAULT NULL,
  `Realisasi_TW2` varchar(200) DEFAULT NULL,
  `Realisasi_TW3` varchar(200) DEFAULT NULL,
  `Realisasi_TW4` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_mopd_iktujuan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mopd_iktujuan`;

CREATE TABLE `sakip_mopd_iktujuan` (
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) DEFAULT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Nm_IndikatorTujuanOPD` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`),
  KEY `FK_OPD_TUJUAN` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_TujuanOPD`),
  CONSTRAINT `FK_OPD_TUJUAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`) REFERENCES `sakip_mopd_tujuan` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_mopd_iktujuan` WRITE;
/*!40000 ALTER TABLE `sakip_mopd_iktujuan` DISABLE KEYS */;

INSERT INTO `sakip_mopd_iktujuan` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Nm_IndikatorTujuanOPD`)
VALUES
	(1,1,1,1,1,1,NULL,4,1,5,7,1,1,'Terwujudnya Tatakelola Pemerintahan Yang Baik Dan Reformasi Birokrasi'),
	(1,1,1,1,1,2,NULL,4,1,5,7,1,1,'Terwujudnya Tata Kelola Pemerintahan yang Baik dan Reformasi Birokrasi');

/*!40000 ALTER TABLE `sakip_mopd_iktujuan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_mopd_lakip
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mopd_lakip`;

CREATE TABLE `sakip_mopd_lakip` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) DEFAULT NULL,
  `Kd_Tahun` bigint(10) DEFAULT NULL,
  `Kd_Urusan` bigint(10) DEFAULT NULL,
  `Kd_Bidang` bigint(10) DEFAULT NULL,
  `Kd_Unit` bigint(10) DEFAULT NULL,
  `Kd_Sub` bigint(10) DEFAULT NULL,
  `Nm_FormLakip` text,
  `Create_By` varchar(50) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_mopd_lakip` WRITE;
/*!40000 ALTER TABLE `sakip_mopd_lakip` DISABLE KEYS */;

INSERT INTO `sakip_mopd_lakip` (`Uniq`, `Kd_Pemda`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Nm_FormLakip`, `Create_By`, `Create_Date`)
VALUES
	(1,1,2021,4,1,5,7,'{\"text-opd\":\"4.1.5.7 Sekretariat Daerah Kabupaten\",\"Kd_Urusan\":\"4\",\"Kd_Bidang\":\"1\",\"Kd_Unit\":\"5\",\"Kd_Sub\":\"7\",\"Kd_Tahun\":\"2021\",\"12_narasi\":\"DEV\",\"13_narasi\":\"DEV\",\"14_narasi\":\"DEV\",\"15_narasi\":\"DEV\",\"16_narasi\":\"DEV\",\"21_narasi\":\"DEV\",\"211_narasi\":\"DEV\",\"212_narasi\":\"DEV\",\"213_narasi\":\"DEV\",\"23_narasi\":\"\",\"31_narasi\":\"\",\"32_narasi\":\"\",\"33_narasi\":\"\",\"34_narasi\":\"\",\"4_narasi\":\"\"}','setdakab','2021-04-09 14:56:24');

/*!40000 ALTER TABLE `sakip_mopd_lakip` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_mopd_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mopd_sasaran`;

CREATE TABLE `sakip_mopd_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) DEFAULT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Nm_SasaranOPD` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`),
  KEY `Uniq` (`Uniq`),
  CONSTRAINT `FK_OPD_IKTUJUAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`) REFERENCES `sakip_mopd_iktujuan` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_mopd_sasaran` WRITE;
/*!40000 ALTER TABLE `sakip_mopd_sasaran` DISABLE KEYS */;

INSERT INTO `sakip_mopd_sasaran` (`Uniq`, `Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Nm_SasaranOPD`)
VALUES
	(6,1,1,1,1,1,2,NULL,4,1,5,7,1,1,1,'Meningkatnya Penyelenggaraan Pemerintahan Umum dan Otonomi Daerah'),
	(11,1,1,1,1,1,2,NULL,4,1,5,7,1,1,2,'Meningkatnya Kualitas Pelayanan Setdakab'),
	(12,1,1,1,1,1,2,NULL,4,1,5,7,1,1,3,'Meningkatnya Kualitas Perumusan Kebijakan Sesuai Prosedur'),
	(13,1,1,1,1,1,2,NULL,4,1,5,7,1,1,4,'Meningkatnya opini Pemerintah Daerah');

/*!40000 ALTER TABLE `sakip_mopd_sasaran` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_mopd_tujuan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mopd_tujuan`;

CREATE TABLE `sakip_mopd_tujuan` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) DEFAULT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Nm_TujuanOPD` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`),
  KEY `Uniq` (`Uniq`),
  CONSTRAINT `FK_PEMDA_IKSASARAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`) REFERENCES `sakip_mpmd_iksasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_mopd_tujuan` WRITE;
/*!40000 ALTER TABLE `sakip_mopd_tujuan` DISABLE KEYS */;

INSERT INTO `sakip_mopd_tujuan` (`Uniq`, `Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Nm_TujuanOPD`)
VALUES
	(5,1,1,1,1,1,1,NULL,4,1,5,7,1,'Terwujudnya Tatakelola Pemerintahan Yang Baik Dan Reformasi Birokrasi'),
	(1,1,1,1,1,1,2,NULL,4,1,5,7,1,'Terwujudnya Tata Kelola Pemerintahan yang Baik dan Reformasi Birokrasi');

/*!40000 ALTER TABLE `sakip_mopd_tujuan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_mpemda
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mpemda`;

CREATE TABLE `sakip_mpemda` (
  `Kd_Pemda` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Prov` bigint(10) NOT NULL,
  `Kd_Kab` bigint(10) NOT NULL,
  `Kd_Tahun_From` bigint(10) NOT NULL,
  `Kd_Tahun_To` bigint(10) NOT NULL,
  `Nm_Kab` varchar(200) DEFAULT NULL,
  `Nm_Pejabat` varchar(200) DEFAULT NULL,
  `Nm_Posisi` varchar(200) DEFAULT NULL,
  `Nm_Visi` varchar(200) DEFAULT NULL,
  `Nm_Alamat_Kab` varchar(200) DEFAULT NULL,
  `Nm_Ket_Periode` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Prov`,`Kd_Kab`,`Kd_Tahun_From`,`Kd_Tahun_To`),
  KEY `Kd_Pemda` (`Kd_Pemda`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_mpemda` WRITE;
/*!40000 ALTER TABLE `sakip_mpemda` DISABLE KEYS */;

INSERT INTO `sakip_mpemda` (`Kd_Pemda`, `Kd_Prov`, `Kd_Kab`, `Kd_Tahun_From`, `Kd_Tahun_To`, `Nm_Kab`, `Nm_Pejabat`, `Nm_Posisi`, `Nm_Visi`, `Nm_Alamat_Kab`, `Nm_Ket_Periode`)
VALUES
	(1,12,16,2018,2023,'Batubara','Ir. H. Zahir, M. AP','Bupati','Menjadikan Masyarakat Kabupaten Batu Bara Masyarakat Industri yang Sejahtera, Mandiri dan Berbudaya',NULL,NULL);

/*!40000 ALTER TABLE `sakip_mpemda` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_mpmd_iksasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mpmd_iksasaran`;

CREATE TABLE `sakip_mpmd_iksasaran` (
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Nm_IndikatorSasaran` varchar(200) DEFAULT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`),
  CONSTRAINT `FK_PEMDA_SASARAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`) REFERENCES `sakip_mpmd_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_mpmd_iksasaran` WRITE;
/*!40000 ALTER TABLE `sakip_mpmd_iksasaran` DISABLE KEYS */;

INSERT INTO `sakip_mpmd_iksasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Nm_IndikatorSasaran`, `Nm_Formula`, `Nm_SumberData`)
VALUES
	(1,1,1,1,1,1,'Opini BPK','Opini Pemeriksaan BPK','BPK'),
	(1,1,1,1,1,2,'Nilai Evaluasi AKIP','Nilai Akuntabilitas Kinerja Instansi Pemerintah','Kemenpan RB');

/*!40000 ALTER TABLE `sakip_mpmd_iksasaran` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_mpmd_iksasaran_capaian
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mpmd_iksasaran_capaian`;

CREATE TABLE `sakip_mpmd_iksasaran_capaian` (
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Target` varchar(200) DEFAULT NULL,
  `Target_TW1` varchar(200) DEFAULT NULL,
  `Target_TW2` varchar(200) DEFAULT NULL,
  `Target_TW3` varchar(200) DEFAULT NULL,
  `Target_TW4` varchar(200) DEFAULT NULL,
  `Realisasi` varchar(200) DEFAULT NULL,
  `Realisasi_TW1` varchar(200) DEFAULT NULL,
  `Realisasi_TW2` varchar(200) DEFAULT NULL,
  `Realisasi_TW3` varchar(200) DEFAULT NULL,
  `Realisasi_TW4` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`),
  CONSTRAINT `sakip_mpmd_iksasaran_capaian_ibfk_1` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`) REFERENCES `sakip_mpmd_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_mpmd_iktujuan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mpmd_iktujuan`;

CREATE TABLE `sakip_mpmd_iktujuan` (
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Nm_IndikatorTujuan` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`),
  CONSTRAINT `FK_TUJUAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`) REFERENCES `sakip_mpmd_tujuan` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_mpmd_iktujuan` WRITE;
/*!40000 ALTER TABLE `sakip_mpmd_iktujuan` DISABLE KEYS */;

INSERT INTO `sakip_mpmd_iktujuan` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Nm_IndikatorTujuan`)
VALUES
	(1,1,1,1,'Terciptanya Reformasi Birokrasi di Kabupaten Batu Bara');

/*!40000 ALTER TABLE `sakip_mpmd_iktujuan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_mpmd_misi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mpmd_misi`;

CREATE TABLE `sakip_mpmd_misi` (
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Nm_Misi` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`),
  CONSTRAINT `FK_PEMDA` FOREIGN KEY (`Kd_Pemda`) REFERENCES `sakip_mpemda` (`Kd_Pemda`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_mpmd_misi` WRITE;
/*!40000 ALTER TABLE `sakip_mpmd_misi` DISABLE KEYS */;

INSERT INTO `sakip_mpmd_misi` (`Kd_Pemda`, `Kd_Misi`, `Nm_Misi`)
VALUES
	(1,1,'Meningkatkan Pelayanan Aparatur Pemerintah atas Pelayanan Publik dan Investasi'),
	(1,2,'Meningkatkan Jumlah dan Kualitas Infrastruktur dan Sarana Prasarana Pendukung Pertumbuhan Industri dan Perekonomian Masyarakat'),
	(1,3,'Mewujudkan Masyarakat yang Produktif, Inovatif dan Berbudaya'),
	(1,4,'Mewujudkan Industri Berbasis Sumber Daya Unggulan Kabupaten Batu Bara'),
	(1,5,'Meningkatkan Pemasaran Hasil Industri, Pertanian, dan Perikanan Secara Meluas Memanfaatkan Teknologi Berkembang'),
	(1,6,'Meningkatkan Kolaborasi Industri, Lembaga Pendidikan, dan Pemerintah Kabupaten Batu Bara'),
	(1,7,'Meningkatkan Kualitas Pendidikan, Kesehatan, dan Spiritual Masyarakat'),
	(1,8,'Meningkatkan Peran Serta Seluruh Elemen Masyarakat dalam Pembangunan Kabupaten Batu Bara');

/*!40000 ALTER TABLE `sakip_mpmd_misi` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_mpmd_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mpmd_sasaran`;

CREATE TABLE `sakip_mpmd_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Nm_Sasaran` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`),
  KEY `Uniq` (`Uniq`),
  CONSTRAINT `FK_PEMDA_IKTUJUAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`) REFERENCES `sakip_mpmd_iktujuan` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_mpmd_sasaran` WRITE;
/*!40000 ALTER TABLE `sakip_mpmd_sasaran` DISABLE KEYS */;

INSERT INTO `sakip_mpmd_sasaran` (`Uniq`, `Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Nm_Sasaran`)
VALUES
	(10,1,1,1,1,1,'Meningkatnya Akuntabilitas Keuangan dan Kinerja Birokrasi');

/*!40000 ALTER TABLE `sakip_mpmd_sasaran` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_mpmd_tujuan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mpmd_tujuan`;

CREATE TABLE `sakip_mpmd_tujuan` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Nm_Tujuan` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`),
  KEY `Uniq` (`Uniq`),
  CONSTRAINT `FK_MISI` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`) REFERENCES `sakip_mpmd_misi` (`Kd_Pemda`, `Kd_Misi`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_mpmd_tujuan` WRITE;
/*!40000 ALTER TABLE `sakip_mpmd_tujuan` DISABLE KEYS */;

INSERT INTO `sakip_mpmd_tujuan` (`Uniq`, `Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Nm_Tujuan`)
VALUES
	(7,1,1,1,'Terciptanya Reformasi Birokrasi di Kabupaten Batu Bara');

/*!40000 ALTER TABLE `sakip_mpmd_tujuan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_msatuan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_msatuan`;

CREATE TABLE `sakip_msatuan` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Satuan` varchar(50) NOT NULL,
  `Nm_Satuan` varchar(50) NOT NULL,
  `Create_By` varchar(200) NOT NULL,
  `Create_Date` datetime NOT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `sakip_msatuan` WRITE;
/*!40000 ALTER TABLE `sakip_msatuan` DISABLE KEYS */;

INSERT INTO `sakip_msatuan` (`Uniq`, `Kd_Satuan`, `Nm_Satuan`, `Create_By`, `Create_Date`, `Edit_By`, `Edit_Date`)
VALUES
	(1,'%','Persen','admin','2019-03-14 23:59:59',NULL,NULL),
	(2,'Nilai','Nilai','admin','2019-03-14 23:59:59',NULL,NULL),
	(3,'Indeks','Indeks','admin','2019-03-14 23:59:59',NULL,NULL),
	(4,'Unit','Unit','admin','2019-03-14 23:59:59','admin','2019-08-27 02:23:25'),
	(5,'ORG','Orang','admin','2019-03-14 23:59:59',NULL,NULL),
	(6,'OP','Opini','admin','2019-03-14 23:59:59',NULL,NULL),
	(7,'KEL','Kelompok','admin','2019-03-14 23:59:59',NULL,NULL),
	(8,'CABOR','Cabor','admin','2019-03-14 23:59:59',NULL,NULL),
	(9,'TIM','Tim','admin','2019-03-14 23:59:59',NULL,NULL),
	(10,'Dokumen','Dokumen','admin','2019-03-14 23:59:59','admin','2019-08-27 02:23:33'),
	(11,'Jasa','Jasa','admin','2019-03-14 23:59:59',NULL,NULL),
	(12,'Item','Item','admin','2019-03-14 23:59:59',NULL,NULL),
	(13,'Jenis','Jenis','admin','2019-03-14 23:59:59',NULL,NULL),
	(14,'Bahan Bacaan','Bahan Bacaan','admin','2019-03-14 23:59:59',NULL,NULL),
	(15,'Kebutuhan','Kebutuhan','admin','2019-03-14 23:59:59',NULL,NULL),
	(16,'Kali','Kali','admin','2019-03-14 23:59:59',NULL,NULL),
	(17,'Lembaga','Lembaga','admin','2019-03-14 23:59:59',NULL,NULL),
	(18,'Paket','Paket','admin','2019-03-14 23:59:59',NULL,NULL),
	(19,'Gugus & IGTK','Gugus & IGTK','admin','2019-03-14 23:59:59',NULL,NULL),
	(20,'Ruang','Ruang','admin','2019-03-14 23:59:59',NULL,NULL),
	(21,'KK-DATADIK PAUD','KK-DATADIK PAUD','admin','2019-03-14 23:59:59',NULL,NULL),
	(22,'LS','LS','admin','2019-03-14 23:59:59',NULL,NULL),
	(23,'Siswa','Siswa','admin','2019-03-14 23:59:59',NULL,NULL),
	(24,'Mapel','Mapel','admin','2019-03-14 23:59:59',NULL,NULL),
	(25,'Mata Lomba','Mata Lomba','admin','2019-03-14 23:59:59',NULL,NULL),
	(26,'Kegiatan','Kegiatan','admin','2019-03-14 23:59:59',NULL,NULL),
	(27,'Buku','Buku','admin','2019-03-14 23:59:59',NULL,NULL),
	(28,'Sekolah','Sekolah','admin','2019-03-14 23:59:59',NULL,NULL),
	(29,'Lokasi','Lokasi','admin','2019-03-14 23:59:59',NULL,NULL),
	(30,'Exemplar','Exemplar','admin','2019-03-14 23:59:59',NULL,NULL),
	(31,'Set','Set','admin','2019-03-14 23:59:59',NULL,NULL),
	(32,'Event','Event','admin','2019-03-14 23:59:59',NULL,NULL),
	(33,'Keping','Keping','admin','2019-03-14 23:59:59',NULL,NULL),
	(34,'Aplikasi','Aplikasi','admin','2019-03-14 23:59:59',NULL,NULL),
	(35,'Pagelaran','Pagelaran','admin','2019-03-14 23:59:59',NULL,NULL),
	(36,'Wadah','Wadah','admin','2019-03-14 23:59:59',NULL,NULL),
	(37,'Buah','Buah','admin','2019-03-14 23:59:59',NULL,NULL),
	(38,'Desa','Desa','admin','2019-03-14 23:59:59',NULL,NULL),
	(39,'Batas Perwilayahan','Batas Perwilayahan','admin','2019-03-14 23:59:59',NULL,NULL),
	(40,'Produk Hukum','Produk Hukum','admin','2019-03-14 23:59:59',NULL,NULL),
	(41,'WP','WP','admin','2019-03-14 23:59:59',NULL,NULL),
	(42,'Rekening','Rekening','admin','2019-03-15 08:49:13',NULL,NULL),
	(43,'OH','OH','admin','2019-03-15 08:49:23',NULL,NULL),
	(44,'Ha','Hektare','admin','2019-03-19 11:01:43','admin','2019-08-27 02:23:47'),
	(45,'Ton','Ton','admin','2019-03-19 11:01:59',NULL,NULL),
	(47,'Batang','Batang','admin','2019-03-19 11:02:30',NULL,NULL),
	(48,'Komoditi','Komoditi','admin','2019-03-19 11:02:51',NULL,NULL),
	(49,'Poktan','Poktan','admin','2019-03-19 12:15:56',NULL,NULL),
	(51,'Km','Kilometer','admin','2019-03-20 12:03:37',NULL,NULL),
	(52,'Penangkar','Penangkar','admin','2019-03-20 12:51:18',NULL,NULL),
	(53,'WTP','Wajar Tanpa Pengecualian','admin','2019-03-20 12:56:22',NULL,NULL),
	(54,'WDP','Wajar Dengan Pengecualian','admin','2019-03-20 12:56:37',NULL,NULL),
	(55,'Disclaimer','Disclaimer','admin','2019-03-20 12:57:04',NULL,NULL),
	(56,'Level','Level','admin','2019-03-20 12:57:14',NULL,NULL),
	(57,'Laporan','Laporan','admin','2019-03-20 12:57:25',NULL,NULL),
	(58,'Rencana Aksi','Rencana Aksi','admin','2019-03-20 12:57:41',NULL,NULL),
	(59,'OPD','OPD','admin','2019-03-20 12:57:59','admin','2019-08-22 09:09:23'),
	(60,'Koperasi','Koperasi','admin','2019-03-21 04:26:35',NULL,NULL),
	(61,'UMKM','UMKM','admin','2019-03-21 04:26:48',NULL,NULL),
	(62,'IKM','IKM','admin','2019-03-21 04:27:32',NULL,NULL),
	(63,'Pasar','Pasar','admin','2019-03-21 04:28:21',NULL,NULL),
	(64,'Rumah Tangga','Rumah Tangga','admin','2019-03-21 05:07:57',NULL,NULL),
	(65,'Meter','Meter','admin','2019-03-21 05:08:06',NULL,NULL),
	(66,'65','kecamatan','admin','2019-05-06 09:53:05',NULL,NULL),
	(67,'Bulan','Bulan','admin','2019-07-16 03:55:03',NULL,NULL),
	(68,'Unit Kerja','Unit Kerja','admin','2019-08-15 03:49:22',NULL,NULL),
	(69,'Perpustakaan','Perpustakaan','admin','2019-08-15 03:49:33',NULL,NULL),
	(72,'Ekor','Ekor','admin','2019-08-15 03:49:54',NULL,NULL),
	(74,'Hari','Hari','admin','2019-08-19 07:13:55',NULL,NULL),
	(75,'Bungkus','Bungkus','admin','2019-08-19 07:14:08',NULL,NULL),
	(76,'Tilang','Tilang','admin','2019-08-19 07:14:24',NULL,NULL),
	(77,'Perusahaan','Perusahaan','admin','2019-08-19 07:14:33',NULL,NULL),
	(78,'Organisasi','Organisasi','admin','2019-08-20 07:02:57',NULL,NULL),
	(79,'DI','Daerah Irigasi','admin','2019-08-20 07:03:07',NULL,NULL),
	(80,'Peserta','Peserta','admin','2019-08-20 11:05:19',NULL,NULL),
	(81,'kg','kg','admin','2019-08-21 03:43:21',NULL,NULL),
	(82,'kg/kapita/tahun','kg/kapita/tahun','admin','2019-08-21 03:46:01',NULL,NULL),
	(83,'Produk','Produk','admin','2019-08-21 04:14:14',NULL,NULL),
	(84,'Boot/Stand','Boot/Stand','admin','2019-08-21 04:15:10',NULL,NULL),
	(85,'Tepat Waktu','Tepat Waktu','admin','2019-08-21 08:07:00',NULL,NULL),
	(86,'SP2D','SP2D','admin','2019-08-21 08:07:22',NULL,NULL),
	(87,'Temuan','Temuan','admin','2019-08-21 08:07:29',NULL,NULL),
	(88,'Rp','Rupiah','admin','2019-08-21 08:07:55',NULL,NULL),
	(89,'RTP','RTP','admin','2019-08-21 09:11:11',NULL,NULL),
	(90,'Pedagang','Pedagang','admin','2019-08-21 09:29:02',NULL,NULL),
	(91,'Faskes','Fasilitas Kesehatan','admin','2019-08-21 09:36:01',NULL,NULL),
	(92,'PPKBD dan Sub PPKKBD','PPKBD dan Sub PPKKBD','admin','2019-08-21 09:55:14',NULL,NULL),
	(93,'Bangunan','Bangunan','admin','2019-08-21 10:00:22',NULL,NULL),
	(94,'Kepala Keluarga','Kepala Keluarga (KK)','admin','2019-08-21 10:04:27',NULL,NULL),
	(95,'Skor','Skor','admin','2019-08-21 11:20:46',NULL,NULL),
	(96,'Angka','Angka','admin','2019-08-21 11:20:57',NULL,NULL),
	(97,'Kampung KB','Kampung KB','admin','2019-08-21 11:32:27',NULL,NULL),
	(98,'Informasi','Informasi','admin','2019-08-22 02:41:47',NULL,NULL),
	(99,'Meter Persegi','Meter Persegi','admin','2019-08-22 05:20:17',NULL,NULL),
	(100,'KWT','KWT','admin','2019-08-22 07:56:32',NULL,NULL),
	(101,'Poktan','Poktan','admin','2019-08-22 08:13:52',NULL,NULL),
	(102,'Predikat','Predikat','admin','2019-08-22 09:09:04',NULL,NULL),
	(103,'Kasus','Kasus','admin','2019-08-27 09:29:28',NULL,NULL),
	(104,'Responden','Responden','admin','2019-08-27 10:18:45',NULL,NULL),
	(105,'Sertifikat','Sertifikat','admin','2019-08-27 11:36:15',NULL,NULL),
	(106,'Pelaku usaha dan atau kegiatan','Pelaku usaha dan atau kegiatan','admin','2019-08-27 12:52:54',NULL,NULL),
	(107,'Judul','Judul','admin','2019-08-28 00:39:02',NULL,NULL),
	(108,'Peraturan','Peraturan','admin','2019-08-28 01:09:45',NULL,NULL),
	(109,'Titik','Titik','admin','2019-08-28 04:53:05',NULL,NULL),
	(110,'Puskesmas','Puskesmas','admin','2019-08-28 04:56:10',NULL,NULL),
	(111,'Titik','Titik','admin','2019-08-28 05:27:58',NULL,NULL),
	(112,'Media Informasi','Media Informasi','admin','2019-08-28 07:43:41',NULL,NULL),
	(113,'Wajib Pajak','Wajib Pajak','admin','2019-08-28 09:50:29',NULL,NULL),
	(114,'SPPT','SPPT','admin','2019-08-28 09:51:15',NULL,NULL),
	(115,'Balai Penyuluhan','Balai Penyuluhan','admin','2019-08-28 12:08:03',NULL,NULL),
	(116,'Rumah','Rumah','admin','2019-08-29 04:37:30',NULL,NULL),
	(117,'Sampel','Sampel','admin','2019-08-30 03:05:53',NULL,NULL),
	(118,'Leaflet/baliho','Leaflet/baliho','admin','2019-08-30 03:06:12',NULL,NULL),
	(119,'Pemotong','Pemotong','admin','2019-08-30 03:24:07',NULL,NULL),
	(120,'Dosis','Dosis','admin','2019-08-30 04:25:40',NULL,NULL),
	(121,'Tahun','Tahun','admin','2020-05-26 14:23:20',NULL,NULL);

/*!40000 ALTER TABLE `sakip_msatuan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_msubbid
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_msubbid`;

CREATE TABLE `sakip_msubbid` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Nm_Subbid` varchar(200) DEFAULT NULL,
  `Nm_Kasubbid` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  KEY `Uniq` (`Uniq`),
  CONSTRAINT `FK_BID4` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_msubbid` WRITE;
/*!40000 ALTER TABLE `sakip_msubbid` DISABLE KEYS */;

INSERT INTO `sakip_msubbid` (`Uniq`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`, `Nm_Subbid`, `Nm_Kasubbid`)
VALUES
	(1,4,1,5,7,1,1,'Subbag Kinerja dan Reformasi Birokrasi','Abdul Gofar Asdiqi'),
	(2,4,1,5,7,1,2,'Subbag Pelayanan Publik dan Tata Laksana','Farhansyah Sembiring'),
	(3,4,1,5,7,1,3,'Subbag Kelembagaan dan Analisis Jabatan','Iqbal Ressa M'),
	(4,4,1,5,7,2,1,'subbagian bina mental spritual','Nurul Istihar'),
	(5,4,1,5,7,2,2,'subbagian kesejahteraan sosial','Safri'),
	(6,4,1,5,7,2,3,'Subbagian Kesejahteraan Masyarakat','Ahmad Jais'),
	(28,4,1,5,7,3,1,'Subbagian Perencanaan','ELIANA BUTAR-BUTAR'),
	(29,4,1,5,7,3,2,'Subbagian Keuangan','ASMARELI'),
	(30,4,1,5,7,3,3,'Subbagian Pelaporan','ALFIN HARIADI'),
	(32,4,1,5,7,4,1,'Subbagian Administrasi Pemerintahan','HARDIMAN SIHOMBING'),
	(33,4,1,5,7,4,2,'Subbagian Administrasi Kewilayahan','ERNA KARTIKA'),
	(34,4,1,5,7,4,3,'Subbagian Kerjasama Dan Otonomi Daerah','SUMIATI'),
	(22,4,1,5,7,5,1,'Subbagian Tata Usaha Pimpinan, Staf Ahli Dan Kepegawaian','ADI HAMONANGAN MARPAUNG'),
	(23,4,1,5,7,5,2,'Subbagian Perlengkapan','WIDHI PRASTIWI'),
	(24,4,1,5,7,5,3,'Subbagian Rumah Tangga','Syufri'),
	(11,4,1,5,7,6,1,'Subbagian Pembinaan BUMD dan BLUD','Yuly Erlina'),
	(14,4,1,5,7,6,2,'Subbagian Perekonomian','DIAN WINDHU FEBRIARIN'),
	(15,4,1,5,7,6,3,'Subbagian Sumber Daya Alam','ALISA RAHMI HANDAYANI'),
	(19,4,1,5,7,7,1,' Subbagian Pengelolaan Pengadaan Barang Dan Jas','MUSA'),
	(20,4,1,5,7,7,2,'Subbagian Pengelolaan Layanan Pengadaan Secara Elektronik','DENNI HARDEDI BANGUN'),
	(21,4,1,5,7,7,3,'Subbagian Pembinaan Dan Advokasi Pengadaan Barang Dan Jasa','BENYAMIN JULIANTO TARIGAN'),
	(16,4,1,5,7,8,1,'Subbagian Penyusunan Program','YUSLINDA MAIYAR'),
	(17,4,1,5,7,8,2,' Subbagian Pengendalian Program','BACHTARIZA'),
	(18,4,1,5,7,8,3,' Subbagian Evaluasi Dan Pelaporan','RICHMON SITORUS'),
	(25,4,1,5,7,9,1,'Subbagian Protokol','ANGGA MARAYUDHA SIMATUPANG'),
	(26,4,1,5,7,9,2,'Subbagian Komunikasi Pimpinan','Oriza Sativa'),
	(27,4,1,5,7,9,3,'Subbagian Dokumentasi Pimpinan','MUHAMMAD REZA PAHLEVI'),
	(8,4,1,5,7,10,1,'Subbagian Perundang Undangan','liza Chitra Lubis'),
	(9,4,1,5,7,10,2,'Subbagian Bantuan Hukum','Sutan Rahmadsyah Manurung'),
	(10,4,1,5,7,10,3,'Subbagian Dokumentasi dan Informasi','Netti Herawati Nainggolan');

/*!40000 ALTER TABLE `sakip_msubbid` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_msubbid_indikator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_msubbid_indikator`;

CREATE TABLE `sakip_msubbid_indikator` (
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_SasaranSubbidang` bigint(10) NOT NULL,
  `Kd_IndikatorSubbidang` bigint(10) NOT NULL,
  `Nm_IndikatorSubbidang` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_SasaranProgramOPD`,`Kd_IndikatorProgramOPD`,`Kd_Subbid`,`Kd_SasaranSubbidang`,`Kd_IndikatorSubbidang`),
  KEY `FK_SUBBID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FK_SUBBID5` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_SUBBID_SASARAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_SasaranProgramOPD`, `Kd_IndikatorProgramOPD`, `Kd_Subbid`, `Kd_SasaranSubbidang`) REFERENCES `sakip_msubbid_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_SasaranProgramOPD`, `Kd_IndikatorProgramOPD`, `Kd_Subbid`, `Kd_SasaranSubbidang`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_msubbid_kegiatan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_msubbid_kegiatan`;

CREATE TABLE `sakip_msubbid_kegiatan` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) DEFAULT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Nm_KegiatanOPD` varchar(200) DEFAULT NULL,
  `Nm_SasaranKegiatanOPD` varchar(200) DEFAULT NULL,
  `Nm_IndikatorKegiatanOPD` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Kd_SumberDana` varchar(200) DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_N1` double DEFAULT NULL,
  `Akhir` double DEFAULT NULL,
  `Total` double DEFAULT '0',
  `Total_N1` double DEFAULT NULL,
  `IsEplan` tinyint(1) NOT NULL DEFAULT '1',
  `Remarks` varchar(200) DEFAULT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_SUBBID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FK_Kegiatan_SasaranProg` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) REFERENCES `sakip_mbid_program_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) ON UPDATE CASCADE,
  CONSTRAINT `FK_SUBBID` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_msubbid_kegiatan` WRITE;
/*!40000 ALTER TABLE `sakip_msubbid_kegiatan` DISABLE KEYS */;

INSERT INTO `sakip_msubbid_kegiatan` (`Uniq`, `Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_IndikatorProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Nm_KegiatanOPD`, `Nm_SasaranKegiatanOPD`, `Nm_IndikatorKegiatanOPD`, `Kd_Satuan`, `Kd_SumberDana`, `Awal`, `Target`, `Target_N1`, `Akhir`, `Total`, `Total_N1`, `IsEplan`, `Remarks`, `Create_By`, `Create_Date`, `Edit_By`, `Edit_Date`)
VALUES
	(1,1,1,1,1,1,2,2021,4,1,5,7,1,1,1,3,1,1,1,NULL,1,21303,'Peningkatan Kinerja dan Reformasi Birokrasi',NULL,NULL,NULL,'DAU',NULL,NULL,NULL,NULL,214132375,NULL,1,'-',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `sakip_msubbid_kegiatan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_msubbid_kegiatan_grup
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_msubbid_kegiatan_grup`;

CREATE TABLE `sakip_msubbid_kegiatan_grup` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Nm_GrupKegiatanOPD` varchar(200) NOT NULL DEFAULT '',
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_SUBBID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_msubbid_kegiatan_indikator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_msubbid_kegiatan_indikator`;

CREATE TABLE `sakip_msubbid_kegiatan_indikator` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Kd_SasaranKegiatanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorKegiatanOPD` bigint(10) NOT NULL,
  `Nm_IndikatorKegiatanOPD` varchar(200) DEFAULT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  `Nm_PenanggungJawab` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Akhir` double DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`,`Kd_SasaranKegiatanOPD`,`Kd_IndikatorKegiatanOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_SUBBID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FK_IndikatorSasaranKeg` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Kd_SasaranKegiatanOPD`) REFERENCES `sakip_msubbid_kegiatan_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Kd_SasaranKegiatanOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_msubbid_kegiatan_indikator` WRITE;
/*!40000 ALTER TABLE `sakip_msubbid_kegiatan_indikator` DISABLE KEYS */;

INSERT INTO `sakip_msubbid_kegiatan_indikator` (`Uniq`, `Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Kd_SasaranKegiatanOPD`, `Kd_IndikatorKegiatanOPD`, `Nm_IndikatorKegiatanOPD`, `Nm_Formula`, `Nm_SumberData`, `Nm_PenanggungJawab`, `Kd_Satuan`, `Target`, `Awal`, `Akhir`)
VALUES
	(1,1,1,1,1,1,2,2021,4,1,5,7,1,1,1,3,1,1,1,1,21303,1,1,'Jumlah Dokumen Road Map Reformasi Birokrasi','-','-',NULL,'Dokumen',1,0,0),
	(2,1,1,1,1,1,2,2021,4,1,5,7,1,1,1,3,1,1,1,1,21303,1,2,'Jumlah Laporan Capaian Kinerja','-','-',NULL,'Aplikasi',1,0,0),
	(3,1,1,1,1,1,2,2021,4,1,5,7,1,1,1,3,1,1,1,1,21303,1,3,'Jumlah Laporan Monitoring dan Evaluasi Akuntabilitas Kinerja dan Reformasi Birokrasi Capaian Kinerja','-','-',NULL,'Dokumen',1,0,0);

/*!40000 ALTER TABLE `sakip_msubbid_kegiatan_indikator` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_msubbid_kegiatan_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_msubbid_kegiatan_sasaran`;

CREATE TABLE `sakip_msubbid_kegiatan_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Kd_SasaranKegiatanOPD` bigint(10) NOT NULL,
  `Nm_SasaranKegiatanOPD` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Akhir` double DEFAULT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`,`Kd_SasaranKegiatanOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_SUBBID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FK_SasaranKeg` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`) REFERENCES `sakip_msubbid_kegiatan` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SasaranSubbid` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_msubbid_kegiatan_sasaran` WRITE;
/*!40000 ALTER TABLE `sakip_msubbid_kegiatan_sasaran` DISABLE KEYS */;

INSERT INTO `sakip_msubbid_kegiatan_sasaran` (`Uniq`, `Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Kd_SasaranKegiatanOPD`, `Nm_SasaranKegiatanOPD`, `Kd_Satuan`, `Awal`, `Target`, `Akhir`, `Create_By`, `Create_Date`, `Edit_By`, `Edit_Date`)
VALUES
	(1,1,1,1,1,1,2,2021,4,1,5,7,1,1,1,3,1,1,1,1,21303,1,'Meningkatnya Kinerja dan Reformasi Birokrasi',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `sakip_msubbid_kegiatan_sasaran` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_msubbid_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_msubbid_sasaran`;

CREATE TABLE `sakip_msubbid_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_SasaranSubbidang` bigint(10) NOT NULL,
  `Nm_SasaranSubbidang` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_SasaranProgramOPD`,`Kd_IndikatorProgramOPD`,`Kd_Subbid`,`Kd_SasaranSubbidang`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_SUBBID2` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FK_BID_INDIKATOR` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_SasaranProgramOPD`, `Kd_IndikatorProgramOPD`) REFERENCES `sakip_mbid_indikator` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_SasaranProgramOPD`, `Kd_IndikatorProgramOPD`) ON UPDATE CASCADE,
  CONSTRAINT `FK_SUBBID4` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_msumberdana
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_msumberdana`;

CREATE TABLE `sakip_msumberdana` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_SumberDana` varchar(50) NOT NULL,
  `Nm_SumberDana` varchar(50) NOT NULL,
  `Create_By` varchar(200) NOT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_msumberdana` WRITE;
/*!40000 ALTER TABLE `sakip_msumberdana` DISABLE KEYS */;

INSERT INTO `sakip_msumberdana` (`Uniq`, `Kd_SumberDana`, `Nm_SumberDana`, `Create_By`, `Create_Date`, `Edit_By`, `Edit_Date`)
VALUES
	(1,'DAU','DAU','admin','2019-03-14 19:00:54',NULL,NULL),
	(2,'DAK','DAK','admin','2019-03-14 19:01:09','admin','2019-03-14 19:03:30'),
	(3,'PAD','PAD','admin','2019-03-14 19:01:35',NULL,NULL),
	(4,'DAU & DAK','DAU & DAK','admin','2019-08-28 05:28:44',NULL,NULL);

/*!40000 ALTER TABLE `sakip_msumberdana` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_pdpa_kegiatan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_pdpa_kegiatan`;

CREATE TABLE `sakip_pdpa_kegiatan` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Nm_KegiatanOPD` varchar(200) NOT NULL,
  `Is_Renja` tinyint(1) NOT NULL,
  `Total` double NOT NULL,
  `Budget` double DEFAULT NULL,
  `Budget_TW1` double DEFAULT NULL,
  `Budget_TW2` double DEFAULT NULL,
  `Budget_TW3` double DEFAULT NULL,
  `Budget_TW4` double DEFAULT NULL,
  `Anggaran_TW1` double DEFAULT NULL,
  `Anggaran_TW2` double DEFAULT NULL,
  `Anggaran_TW3` double DEFAULT NULL,
  `Anggaran_TW4` double DEFAULT NULL,
  `Kd_SumberDana` varchar(200) NOT NULL,
  `Create_By` varchar(200) NOT NULL,
  `Create_Date` datetime NOT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`),
  KEY `FKP_Subbid2` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FKP_DPA_SasaranProgram2` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) REFERENCES `sakip_pdpa_program_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FKP_Subbid2` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_pdpa_kegiatan_grup
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_pdpa_kegiatan_grup`;

CREATE TABLE `sakip_pdpa_kegiatan_grup` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Nm_GrupKegiatanOPD` varchar(200) NOT NULL DEFAULT '',
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_SUBBID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_pdpa_kegiatan_indikator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_pdpa_kegiatan_indikator`;

CREATE TABLE `sakip_pdpa_kegiatan_indikator` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Kd_SasaranKegiatanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorKegiatanOPD` bigint(10) NOT NULL,
  `Nm_IndikatorKegiatanOPD` varchar(200) NOT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  `Nm_PenanggungJawab` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_TW1` double DEFAULT NULL,
  `Target_TW2` double DEFAULT NULL,
  `Target_TW3` double DEFAULT NULL,
  `Target_TW4` double DEFAULT NULL,
  `Kinerja_TW1` double DEFAULT NULL,
  `Kinerja_TW2` double DEFAULT NULL,
  `Kinerja_TW3` double DEFAULT NULL,
  `Kinerja_TW4` double DEFAULT NULL,
  `Anggaran_TW1` double DEFAULT NULL,
  `Anggaran_TW2` double DEFAULT NULL,
  `Anggaran_TW3` double DEFAULT NULL,
  `Anggaran_TW4` double DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`,`Kd_SasaranKegiatanOPD`,`Kd_IndikatorKegiatanOPD`),
  CONSTRAINT `FKP_DPA_SasaranKeg` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Kd_SasaranKegiatanOPD`) REFERENCES `sakip_pdpa_kegiatan_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Kd_SasaranKegiatanOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_pdpa_kegiatan_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_pdpa_kegiatan_sasaran`;

CREATE TABLE `sakip_pdpa_kegiatan_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Kd_SasaranKegiatanOPD` bigint(10) NOT NULL,
  `Nm_SasaranKegiatanOPD` varchar(200) NOT NULL,
  `Kd_Satuan` varchar(50) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_TW1` double DEFAULT NULL,
  `Target_TW2` double DEFAULT NULL,
  `Target_TW3` double DEFAULT NULL,
  `Target_TW4` double DEFAULT NULL,
  `Budget` double DEFAULT NULL,
  `Budget_TW1` double DEFAULT NULL,
  `Budget_TW2` double DEFAULT NULL,
  `Budget_TW3` double DEFAULT NULL,
  `Budget_TW4` double DEFAULT NULL,
  `Kinerja_TW1` double DEFAULT NULL,
  `Kinerja_TW2` double DEFAULT NULL,
  `Kinerja_TW3` double DEFAULT NULL,
  `Kinerja_TW4` double DEFAULT NULL,
  `Anggaran_TW1` double DEFAULT NULL,
  `Anggaran_TW2` double DEFAULT NULL,
  `Anggaran_TW3` double DEFAULT NULL,
  `Anggaran_TW4` double DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`,`Kd_SasaranKegiatanOPD`),
  KEY `FKP_DPA_Subbid` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FKP_DPA_Kegiatan` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`) REFERENCES `sakip_pdpa_kegiatan` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FKP_DPA_Subbid` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_pdpa_program
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_pdpa_program`;

CREATE TABLE `sakip_pdpa_program` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Nm_ProgramOPD` varchar(200) NOT NULL,
  `Is_Renja` tinyint(1) NOT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`),
  KEY `FKP_DPA_Bid` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `FKP_DPA_Bid` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_pdpa_program_indikator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_pdpa_program_indikator`;

CREATE TABLE `sakip_pdpa_program_indikator` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) NOT NULL,
  `Nm_IndikatorProgramOPD` varchar(200) NOT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  `Nm_PenanggungJawab` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_TW1` double DEFAULT NULL,
  `Target_TW2` double DEFAULT NULL,
  `Target_TW3` double DEFAULT NULL,
  `Target_TW4` double DEFAULT NULL,
  `Kinerja_TW1` double DEFAULT NULL,
  `Kinerja_TW2` double DEFAULT NULL,
  `Kinerja_TW3` double DEFAULT NULL,
  `Kinerja_TW4` double DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_IndikatorProgramOPD`),
  CONSTRAINT `FKP_DPA_SasaranProgram` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) REFERENCES `sakip_pdpa_program_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_pdpa_program_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_pdpa_program_sasaran`;

CREATE TABLE `sakip_pdpa_program_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Nm_SasaranProgramOPD` varchar(200) NOT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_TW1` double DEFAULT NULL,
  `Target_TW2` double DEFAULT NULL,
  `Target_TW3` double DEFAULT NULL,
  `Target_TW4` double DEFAULT NULL,
  `Kinerja_TW1` double DEFAULT NULL,
  `Kinerja_TW2` double DEFAULT NULL,
  `Kinerja_TW3` double DEFAULT NULL,
  `Kinerja_TW4` double DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`),
  KEY `FKP_DPA_Bid2` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `DKP_DPA_Program2` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`) REFERENCES `sakip_pdpa_program` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FKP_DPA_Bid2` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_prenja_kegiatan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_prenja_kegiatan`;

CREATE TABLE `sakip_prenja_kegiatan` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) DEFAULT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Nm_KegiatanOPD` varchar(200) DEFAULT NULL,
  `Nm_SasaranKegiatanOPD` varchar(200) DEFAULT NULL,
  `Nm_IndikatorKegiatanOPD` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Kd_SumberDana` varchar(200) DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_N1` double DEFAULT NULL,
  `Akhir` double DEFAULT NULL,
  `Total` double DEFAULT '0',
  `Total_N1` double DEFAULT NULL,
  `IsEplan` tinyint(1) NOT NULL DEFAULT '1',
  `Remarks` varchar(200) DEFAULT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FKP_SUBBID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FKP_Kegiatan_SasaranProg` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) REFERENCES `sakip_prenja_program_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) ON UPDATE CASCADE,
  CONSTRAINT `FKP_SUBBID` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_prenja_kegiatan_grup
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_prenja_kegiatan_grup`;

CREATE TABLE `sakip_prenja_kegiatan_grup` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Nm_GrupKegiatanOPD` varchar(200) NOT NULL DEFAULT '',
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_SUBBID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_prenja_kegiatan_indikator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_prenja_kegiatan_indikator`;

CREATE TABLE `sakip_prenja_kegiatan_indikator` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Kd_SasaranKegiatanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorKegiatanOPD` bigint(10) NOT NULL,
  `Nm_IndikatorKegiatanOPD` varchar(200) DEFAULT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  `Nm_PenanggungJawab` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Akhir` double DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`,`Kd_SasaranKegiatanOPD`,`Kd_IndikatorKegiatanOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FKP_SUBBID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FKP_IndikatorSasaranKeg` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Kd_SasaranKegiatanOPD`) REFERENCES `sakip_prenja_kegiatan_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Kd_SasaranKegiatanOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_prenja_kegiatan_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_prenja_kegiatan_sasaran`;

CREATE TABLE `sakip_prenja_kegiatan_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Kd_SasaranKegiatanOPD` bigint(10) NOT NULL,
  `Nm_SasaranKegiatanOPD` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Akhir` double DEFAULT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`,`Kd_SasaranKegiatanOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FKP_SUBBID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FKP_SasaranKeg` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`) REFERENCES `sakip_prenja_kegiatan` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FKP_SasaranSubbid` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_prenja_program
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_prenja_program`;

CREATE TABLE `sakip_prenja_program` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Nm_ProgramOPD` varchar(200) DEFAULT NULL,
  `Nm_SasaranProgram` varchar(200) DEFAULT NULL,
  `Nm_IndikatorProgram` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_N1` double DEFAULT NULL,
  `IsEplan` tinyint(1) NOT NULL DEFAULT '1',
  `Remarks` varchar(200) DEFAULT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FKP_OPD_IKSASARAN` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`),
  KEY `FKP_BID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `FKP_BID` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON UPDATE CASCADE,
  CONSTRAINT `FKP_OPD_IKSASARAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`) REFERENCES `sakip_mopd_iksasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_prenja_program_indikator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_prenja_program_indikator`;

CREATE TABLE `sakip_prenja_program_indikator` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) NOT NULL,
  `Nm_IndikatorProgramOPD` varchar(200) DEFAULT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  `Nm_PenanggungJawab` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Akhir` double DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_IndikatorProgramOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FKP_Bid6` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `FKP_Bid6` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FKP_IndikatorSasaran` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) REFERENCES `sakip_prenja_program_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_prenja_program_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_prenja_program_sasaran`;

CREATE TABLE `sakip_prenja_program_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Nm_SasaranProgramOPD` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Akhir` double DEFAULT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FKP_OPD_IKSASARAN` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`),
  KEY `FKP_BID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `FKP_SasaranBid` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FKP_SasaranProgram` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`) REFERENCES `sakip_prenja_program` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table ta_kegiatan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ta_kegiatan`;

CREATE TABLE `ta_kegiatan` (
  `Tahun` year(4) NOT NULL,
  `Kd_Urusan` tinyint(4) NOT NULL,
  `Kd_Bidang` tinyint(4) NOT NULL,
  `Kd_Prog` int(11) NOT NULL,
  `Kd_Keg` int(11) NOT NULL,
  `Kd_Unit` tinyint(4) NOT NULL,
  `Kd_Sub` smallint(6) NOT NULL,
  `ID_Prog` smallint(6) DEFAULT NULL,
  `Ket_Kegiatan` varchar(255) DEFAULT NULL,
  `Lokasi` varchar(800) DEFAULT NULL,
  `Kelompok_Sasaran` varchar(255) DEFAULT NULL,
  `Status_Kegiatan` varchar(1) NOT NULL COMMENT '1. baru, 2 lanjutan',
  `Pagu_Anggaran` double DEFAULT NULL,
  `Waktu_Pelaksanaan` varchar(100) DEFAULT NULL,
  `Kd_Sumber` tinyint(4) DEFAULT NULL,
  `Status` int(1) NOT NULL,
  `Keterangan` text NOT NULL,
  `Pagu_Anggaran_Nt1` double DEFAULT NULL,
  `Verifikasi_Bappeda` tinyint(4) DEFAULT NULL,
  `Tanggal_Verifikasi_Bappeda` int(11) DEFAULT NULL,
  `Keterangan_Verifikasi_Bappeda` mediumtext,
  `Kd_Ref` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Prog`,`Kd_Keg`) USING BTREE,
  KEY `Ta_Kegiatan` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Prog`,`Kd_Keg`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table ta_program
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ta_program`;

CREATE TABLE `ta_program` (
  `Tahun` year(4) NOT NULL,
  `Kd_Urusan` tinyint(4) NOT NULL COMMENT 'untuk filter / kode skpd',
  `Kd_Bidang` tinyint(4) NOT NULL COMMENT 'untuk filter / koda skpd',
  `Kd_Unit` tinyint(4) NOT NULL,
  `Kd_Sub` smallint(6) NOT NULL,
  `Kd_Prog` int(11) NOT NULL,
  `ID_Prog` smallint(6) DEFAULT NULL,
  `Ket_Prog` varchar(255) NOT NULL,
  `Tolak_Ukur` varchar(255) DEFAULT NULL,
  `Target_Angka` double DEFAULT NULL,
  `Target_Uraian` varchar(255) DEFAULT NULL,
  `Kd_Urusan1` tinyint(4) DEFAULT NULL COMMENT 'untuk filter per program',
  `Kd_Bidang1` tinyint(4) DEFAULT NULL COMMENT 'untuk filter per program',
  PRIMARY KEY (`Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Prog`),
  KEY `FK_Ta_Program` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Prog`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
