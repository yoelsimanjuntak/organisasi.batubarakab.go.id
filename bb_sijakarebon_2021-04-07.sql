# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.36-MariaDB)
# Database: bb_sijakarebon
# Generation Time: 2021-04-07 07:59:58 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table _postcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postcategories`;

CREATE TABLE `_postcategories` (
  `PostCategoryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryName` varchar(50) NOT NULL,
  `PostCategoryLabel` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`PostCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_postcategories` WRITE;
/*!40000 ALTER TABLE `_postcategories` DISABLE KEYS */;

INSERT INTO `_postcategories` (`PostCategoryID`, `PostCategoryName`, `PostCategoryLabel`)
VALUES
	(1,'Berita','#f56954'),
	(2,'Galeri','#00a65a'),
	(3,'Pengumuman','#f39c12'),
	(5,'Lainnya','#3c8dbc');

/*!40000 ALTER TABLE `_postcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _postimages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postimages`;

CREATE TABLE `_postimages` (
  `PostImageID` bigint(10) NOT NULL AUTO_INCREMENT,
  `PostID` bigint(10) NOT NULL,
  `FileName` varchar(250) NOT NULL,
  `Description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`PostImageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_postimages` WRITE;
/*!40000 ALTER TABLE `_postimages` DISABLE KEYS */;

INSERT INTO `_postimages` (`PostImageID`, `PostID`, `FileName`, `Description`)
VALUES
	(9,1,'1617087488.jpeg',NULL),
	(10,2,'1617185118.jpeg',NULL);

/*!40000 ALTER TABLE `_postimages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_posts`;

CREATE TABLE `_posts` (
  `PostID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryID` int(10) NOT NULL,
  `PostDate` date NOT NULL,
  `PostTitle` varchar(200) NOT NULL,
  `PostSlug` varchar(200) NOT NULL,
  `PostContent` longtext NOT NULL,
  `PostExpiredDate` date NOT NULL,
  `TotalView` int(11) NOT NULL DEFAULT '0',
  `LastViewDate` datetime DEFAULT NULL,
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '1',
  `FileName` varchar(250) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  PRIMARY KEY (`PostID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_posts` WRITE;
/*!40000 ALTER TABLE `_posts` DISABLE KEYS */;

INSERT INTO `_posts` (`PostID`, `PostCategoryID`, `PostDate`, `PostTitle`, `PostSlug`, `PostContent`, `PostExpiredDate`, `TotalView`, `LastViewDate`, `IsSuspend`, `FileName`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,1,'2021-04-06','Melalui Vidcon, Bupati Zahir Pimpin Musrenbang RKPD Batu Bara','melalui-vidcon-bupati-zahir-pimpin-musrenbang-rkpd-batu-bara','<p>Pelaksanaan Musyawarah Perencanaan Pembangunan (Musrenbang) Rencana Kerja Pemerintah Daerah (RKPD) Tahun 2022 dilakukan dengan video conference (vidcon) yang berpusat di Aula Rumah Dinas Bupati Kecamatan Sei Suka, Selasa (30/03/2021).</p>\r\n\r\n<p>Yang disaksikan oleh Unsur FORKOPIMDA Kabupaten Batu Bara, Kesbangpol Provsu, OPD Se-Kabupaten Batu Bara dan disaksikan oleh Kabupaten Asahan, Kabupaten Sergai dan Kabupaten Simalungun.&nbsp;</p>\r\n\r\n<p>Musrenbang dilakukan sesuai dengan Dasar Hukum, UU No. 25 Tahun 2004 tentang Sistem Perencanaan Pembangunan Nasional. UU No. 23 Tahun 2014 Tentang Pemerintahan Daerah.&nbsp;</p>\r\n\r\n<p>Dalam kata sambutan Bupati menyampaikan, pelaksanaan Musrenbang Pemerintah Kabupaten Batu Bara menjalankan amanat surat Gubernur Sumatera Utara No. 050/1899, tanggal 5 Maret 2021 perihal himbauan sebagai tindak lanjut dari Instruksi Menteri Dalam Negeri Republik Indonesia Nomor 01 Tahun 2021.</p>\r\n\r\n<p>Pentingnya diselenggarakan Musrenbang RKPD untuk menyelenggarakan pembangunan Kabupaten Batu Bara dan penyusunan APBD Tahun 2022. Hal ini agar terciptanya &quot; Good Goverment atau Penyelenggaraan Pemerintahan yang baik dan Cleaner Governance atau Pemerintahan yang Bersih dan Bebas dari Korupsi, Kolusi dan Nepotisme Sebagai upaya peningkatan kompetensi Sumber Daya Manusia dan daya saing yang dilakukan secara serius dan berkesinambungan &quot;.</p>\r\n\r\n<p>Dengan tujuan dilaksanakan Musrenbang untuk meningkatkan Kualitas Sumber Daya Manusia, Pembangunan Infrastruktur, menurunkan angka kemiskinan, menumbuhkan perekonomian yang baik, peningkatan lingkungan hidup dan energi, peningkatan tata kelola Pemerintahan, penegakan Hukum serta Pelayanan Publik.</p>\r\n\r\n<p>&quot;Saya berharap kepada Pemerintah Provinsi Sumatera Utara agar dibuka kembali Bantuan Keuangan Provinsi (BKP) ditahun 2022. Hal ini untuk membantu Kabupaten/Kota di Provinsi Sumut dalam pembangunan&quot;. Ungkap Bupati Zahir</p>\r\n\r\n<p><span style=\"font-size:10px\"><em>Sumber:&nbsp;https://batubarakab.go.id/post/melalui-vidcon-bupati-zahir-pimpin-musrenbang-rkpd-batu-bara-1617087488</em></span></p>\r\n','2022-04-06',7,NULL,0,NULL,'admin','2021-04-06 20:43:50','admin','2021-04-06 20:43:50'),
	(2,1,'2021-04-06','Ketua TP PKK Bekerja Sama Dengan Bank Sumut Dalam Digital Perbankan','ketua-tp-pkk-bekerja-sama-dengan-bank-sumut-dalam-digital-perbankan','<p>Bank Sumut Lima Puluh Kabupaten Batubara gelar sosialisasi produk perbankan dan sekaligus menyerahan Quick Response Code Indonesia Standard (QRIS), kepada Batubara Mart, di Aula Rumdis Tanjung Gading Kecamatan Sei Suka,Rabu (31/3/2021)</p>\r\n\r\n<p>Laporan Pimpinan cabang Bank Sumut Batubara M Zaini mengatakan, Penerapan QRIS maka Bank Sumut melaksanakan sosialisasi, kepada kalangan pelaku UMKM yang ada di Kabupaten Batubara di binaan TP PKK Kabupaten Batubara,</p>\r\n\r\n<p>&ldquo;Inilah salah satu produk yang kita tawarkan kepada Batubara Mart, sekarang kita sudah saling memiliki handphone, apalagi kita sudah memiliki aplikasi Sumut mobile dan sudah bisa bertransaksi pembayaran,&rdquo;ujarnya zaini</p>\r\n\r\n<p>Dengan adanya aplikasi sumut mobile kita sudah bisa bertransaksi pembayaran apa saja,seperti pembayaran BPJS, Pajak bermotor dll, kita tidak lagi perlu datang kekantor cukup melalu aplikasi Sumut mobile saja, ini adalah bagian dari evensiensi Bank Sumut perbankan,</p>\r\n\r\n<p>&ldquo;Oleh sebab itu saya mengucapkan terimakasih atas dukungan Pemkab Batubara kepada bank Sumut Lima Puluh yang sudah mendukung bank sumut, Bank Sumut Lima Puluh ini salah satu bank yang dimilik pemkab Batubara,&rdquo;ujarnya Zaini</p>\r\n\r\n<p>Demikian dikatakan Deputi Perwakilan Siantar Bank Indonesia Abdullah Haris mengatakan, sosialisasi ini kebijakan sistem pembayaran terkini, uang elektronik dan QRIS&rsquo; bertujuan untuk edukasi bagi pelaku UMKM terkait sistem pembayaran melalui QRIS dalam mendorong Batubara mart, sistem digital upaya membangkitkan perekonomian di sumut khususnya di Kabupaten Batubara,</p>\r\n\r\n<p>Menurut Abdullah, berbicara mengenai sistem pembayaran, dengan perkembangan teknologi saat ini,kita mau tidak mau harus mengikutinya dengan kemajuan teknologi, dengan sistem pembayaran khususnya pembayaran nontunai seperti mobile banking, e-money dan fintech menandai peralihan tren pembayaran dari sistem tunai menjadi nontunai.</p>\r\n\r\n<p>&ldquo;BI terus bersinergitas kepada Pemerintah Kabupaten/kota dalam mendorong berbasis pembayaran nontunai, ini teknologi terbaru,&rdquo;sebutnya</p>\r\n\r\n<p>Dengan hadirnya QRIS ini, para pengguna aplikasi pembayaran nontunai tidak perlu memiliki terlalu banyak aplikasi, karena hanya dengan memiliki satu aplikasi saja dapat digunakan untuk melakukan pembayaran di berbagai merchant.</p>\r\n\r\n<p>&ldquo;Tentunya ini sangat memudahkan kita, termasuk para pelaku UMKM,&rdquo;sebutnya</p>\r\n\r\n<p>Plt Kadis Koperasi dan UMKM Arip Hanafi menyampaikan sebagai pelaku UMKM dan pendorong perekonomian masyarakat, masyarakat Batu bara. khususnya cara itu sudah kita pikirkan betul-betul dalam setiap program yang di usung oleh dinas koperasi dan ukm.</p>\r\n\r\n<p>&ldquo;Kami sangat mendukung produk kekinian dari pihak bank Sumut seperti QRIS, ini dapat menjadi alat transaksi pembayaran apa saja. Melihat zaman semangkin maju ini dapat kita Implementasikan dalam keseharian kita melalui transaksi mengunakan uang tunai menjadi non tunai dan harus memiliki tabungan di bank Sumut,ujar Arif</p>\r\n\r\n<p>Ketua TP PKK Kabupaten Batubara Ny Maya Indriasari Zahir mengatakan seiring perubahan zaman yang semakin berkembang, termasuk dalam hal bertransaksi. gawai dan internet kini menjadi pilihan para pelaku usaha dalam melakukan transaksi dengan konsumennya. termasuk dalam kemudahan bertransaksi</p>\r\n\r\n<p>&ldquo;Diharapkan kedepannya juga dengan para pelaku UMKM di Batubara, oleh sebab itu kami menerima dengan pintu terbuka kepada pihak bank sumut yang hadir pada hari ini untuk mewujudkan kemudahan bertransaksi tersebut dengan hadirnya produk perbankan bank sumut terhadap para pelaku UMKM di Kabupaten Batubara,&rdquo;</p>\r\n\r\n<p>&ldquo;Seperti QRIS Bank Sumut yang pada hari ini akan dicontohkan oleh Batu Bara MART, dapat memanfaatkan dengan sebaik-baiknya agar UMKM terus maju dan tidak tertinggal, karena sejatinya UMKM adalah penopang perekonomian masyarakat setempat&rdquo;,ujar Ny Maya,</p>\r\n\r\n<p>Selesai acara, Lembaga Pengkajian Pangan Obat-Obatan Dan Kosmetik Majelis Ulama Indonesia Sumatera Utara (LP POM MUI SU). Memberikan Sertifikat Halal Kepada 7 Pelaku Usaha Kabupaten Batu Bara.</p>\r\n\r\n<p><span style=\"font-size:10px\"><em>Sumber:&nbsp;https://batubarakab.go.id/post/ketua-tp-pkk-bekerja-sama-dengan-bank-sumut-dalam-digital-perbankan</em></span></p>\r\n','2022-04-06',2,NULL,0,NULL,'admin','2021-04-06 20:45:05','admin','2021-04-06 20:45:05');

/*!40000 ALTER TABLE `_posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_roles`;

CREATE TABLE `_roles` (
  `RoleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(50) NOT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_roles` WRITE;
/*!40000 ALTER TABLE `_roles` DISABLE KEYS */;

INSERT INTO `_roles` (`RoleID`, `RoleName`)
VALUES
	(1,'Administrator'),
	(2,'Bappeda'),
	(3,'Operator OPD'),
	(4,'Operator Bidang OPD'),
	(5,'Operator Sub Bidang OPD'),
	(6,'Operator Keuangan');

/*!40000 ALTER TABLE `_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_settings`;

CREATE TABLE `_settings` (
  `SettingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`SettingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_settings` WRITE;
/*!40000 ALTER TABLE `_settings` DISABLE KEYS */;

INSERT INTO `_settings` (`SettingID`, `SettingLabel`, `SettingName`, `SettingValue`)
VALUES
	(1,'SETTING_WEB_NAME','SETTING_WEB_NAME','SI JAKA REBON'),
	(2,'SETTING_WEB_DESC','SETTING_WEB_DESC','Sistem Informasi Analisis Jabatan, Akuntabilitas Kinerja, dan Reformasi Birokrasi Berbasis Online'),
	(3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','-'),
	(4,'SETTING_ORG_NAME','SETTING_ORG_NAME','BAGIAN ORGANISASI\nSETDA KAB. BATU BARA'),
	(5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','Jl. Perintis Kemerdekaan No. 164, Lima Puluh Kota'),
	(6,'SETTING_ORG_LAT','SETTING_ORG_LAT',''),
	(7,'SETTING_ORG_LONG','SETTING_ORG_LONG',''),
	(8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','(0622)96765'),
	(9,'SETTING_ORG_FAX','SETTING_ORG_FAX','-'),
	(10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','pemkab@batubarakab.go.id'),
	(11,'SETTING_WEB_API_FOOTERLINK','SETTING_WEB_API_FOOTERLINK','-'),
	(12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','logo.png'),
	(13,'SETTING_WEB_SKIN_CLASS','SETTING_WEB_SKIN_CLASS','skin-green-light'),
	(14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','loader-128x/Preloader_1.gif'),
	(15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.0');

/*!40000 ALTER TABLE `_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _userinformation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_userinformation`;

CREATE TABLE `_userinformation` (
  `UserName` varchar(50) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `CompanyID` varchar(200) DEFAULT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `IdentityNo` varchar(50) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `ReligionID` int(10) DEFAULT NULL,
  `Gender` tinyint(1) DEFAULT NULL,
  `Address` text,
  `PhoneNumber` varchar(50) DEFAULT NULL,
  `EducationID` int(10) DEFAULT NULL,
  `UniversityName` varchar(50) DEFAULT NULL,
  `FacultyName` varchar(50) DEFAULT NULL,
  `MajorName` varchar(50) DEFAULT NULL,
  `IsGraduated` tinyint(1) NOT NULL DEFAULT '0',
  `GraduatedDate` date DEFAULT NULL,
  `YearOfExperience` int(10) DEFAULT NULL,
  `RecentPosition` varchar(250) DEFAULT NULL,
  `RecentSalary` double DEFAULT NULL,
  `ExpectedSalary` double DEFAULT NULL,
  `CVFilename` varchar(250) DEFAULT NULL,
  `ImageFilename` varchar(250) DEFAULT NULL,
  `RegisteredDate` date DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_userinformation` WRITE;
/*!40000 ALTER TABLE `_userinformation` DISABLE KEYS */;

INSERT INTO `_userinformation` (`UserName`, `Email`, `CompanyID`, `Name`, `IdentityNo`, `BirthDate`, `ReligionID`, `Gender`, `Address`, `PhoneNumber`, `EducationID`, `UniversityName`, `FacultyName`, `MajorName`, `IsGraduated`, `GraduatedDate`, `YearOfExperience`, `RecentPosition`, `RecentSalary`, `ExpectedSalary`, `CVFilename`, `ImageFilename`, `RegisteredDate`)
VALUES
	('admin','yoelrolas@gmail.com',NULL,'Administrator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-17'),
	('tt.admin.bag.adpem','tt.admin.bag.adpem@sitalakbajakun.tebingtinggikota.go.id','4.1.1.1.5','OPERATOR BAGIAN ADMINISTRASI PEMBANGUNAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-17'),
	('tt.admin.bag.ekosda','tt.admin.bag.ekosda@sitalakbajakun.tebingtinggikota.go.id','4.1.1.1.4','OPERATOR BAGIAN EKONOMI & SDA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-17'),
	('tt.admin.bag.hukum','tt.admin.bag.hukum@sitalakbajakun.tebingtinggikota.go.id','4.1.1.1.3','OPERATOR BAGIAN HUKUM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-17'),
	('tt.admin.bag.kesra','tt.admin.bag.kesra@sitalakbajakun.tebingtinggikota.go.id','4.1.1.1.6','OPERATOR BAGIAN KESRA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-17'),
	('tt.admin.bag.organisasi','tt.admin.bag.organisasi@sitalakbajakun.tebingtinggikota.go.id','4.1.1.1.1','OPERATOR BAGIAN ORGANISASI',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-17'),
	('tt.admin.bag.pbj','tt.admin.bag.pbj@sitalakbajakun.tebingtinggikota.go.id','4.1.1.1.9','OPERATOR BAGIAN PENGADAAN BARANG & JASA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-17'),
	('tt.admin.bag.pemerintahan','tt.admin.bag.pemerintahan@sitalakbajakun.tebingtinggikota.go.id','4.1.1.1.2','OPERATOR BAGIAN PEMERINTAHAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-17'),
	('tt.admin.bag.prokom','tt.admin.bag.prokom@sitalakbajakun.tebingtinggikota.go.id','4.1.1.1.8','OPERATOR BAGIAN PROTOKOL & KOMUNIKASI PIMPINAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-17'),
	('tt.admin.bag.umum','tt.admin.bag.umum@sitalakbajakun.tebingtinggikota.go.id','4.1.1.1.7','OPERATOR BAGIAN UMUM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-17'),
	('tt.admin.bajenis','tt.admin.bajenis@sitalakbajakun.tebingtinggikota.go.id','4.1.1.4','OPERATOR KEC. BAJENIS',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.bappeda','tt.admin.bappeda@sitalakbajakun.tebingtinggikota.go.id','4.3.1.1','OPERATOR BAPPEDA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.bkd','tt.admin.bkd@sitalakbajakun.tebingtinggikota.go.id','4.5.1.1','OPERATOR BKD',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.bpbd','tt.admin.bpbd@sitalakbajakun.tebingtinggikota.go.id','1.5.1.4','OPERATOR BPBD',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.bpkpad','tt.admin.bpkpad@sitalakbajakun.tebingtinggikota.go.id','4.4.1.1','OPERATOR BPKPAD',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.dinkes','tt.admin.dinkes@sitalakbajakun.tebingtinggikota.go.id','1.2.1.1','OPERATOR DINAS KESEHATAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.dinsos','tt.admin.dinsos@sitalakbajakun.tebingtinggikota.go.id','1.6.1.1','OPERATOR DINAS SOSIAL',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.disdik','tt.admin.disdik@sitalakbajakun.tebingtinggikota.go.id','1.1.1.1','OPERATOR DINAS PENDIDIKAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.dishub','tt.admin.dishub@sitalakbajakun.tebingtinggikota.go.id','2.9.1.1','OPERATOR DINAS PERHUBUNGAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.diskominfo','tt.admin.diskominfo@sitalakbajakun.tebingtinggikota.go.id','2.10.1.1','OPERATOR DINAS KOMUNIKASI DAN INFORMATIKA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.disnaker','tt.admin.disnaker@sitalakbajakun.tebingtinggikota.go.id','2.1.1.1','OPERATOR DINAS KETENAGAKERJAAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.disperindag','tt.admin.disperindag@sitalakbajakun.tebingtinggikota.go.id','2.11.1.1','OPERATOR DINAS PERDAGANGAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.dispora','tt.admin.dispora@sitalakbajakun.tebingtinggikota.go.id','2.13.1.1','OPERATOR DINAS PEMUDA DAN OLAHRAGA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.dlh','tt.admin.dlh@sitalakbajakun.tebingtinggikota.go.id','2.5.1.1','OPERATOR DINAS LINGKUNGAN HIDUP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.dpmk','tt.admin.dpmk@sitalakbajakun.tebingtinggikota.go.id','2.7.1.1','OPERATOR DINAS PEMERDAYAAN MASYARAKAT KELURAHAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.dpmptsp','tt.admin.dpmptsp@sitalakbajakun.tebingtinggikota.go.id','2.12.1.1','OPERATOR DINAS PENANAMAN MODAL & PPTSP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.dukcapil','tt.admin.dukcapil@sitalakbajakun.tebingtinggikota.go.id','2.6.1.1','OPERATOR DINAS KEPENDUDUKAN DAN PENCATATAN SIPIL',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.inspektorat','tt.admin.inspektorat@sitalakbajakun.tebingtinggikota.go.id','4.2.1.1','OPERATOR INSPEKTORAT',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.kesbangpollinmas','tt.admin.kesbangpollinmas@sitalakbajakun.tebingtinggikota.go.id','4.1.1.3','OPERATOR BADAN KESBANG POL',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.ketapangp','tt.admin.ketapangp@sitalakbajakun.tebingtinggikota.go.id','2.3.1.1','OPERATOR DINAS KETAHAN PANGAN DAN PERTANIAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.padanghilir','tt.admin.padanghilir@sitalakbajakun.tebingtinggikota.go.id','4.1.1.6','OPERATOR KEC. PADANG HILIR',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.padanghulu','tt.admin.padanghulu@sitalakbajakun.tebingtinggikota.go.id','4.1.1.7','OPERATOR KEC. PADANG HULU',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.perkimsih','tt.admin.perkimsih@sitalakbajakun.tebingtinggikota.go.id','1.4.1.1','OPERATOR DINAS PERKIMSIH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.perpustakaan','tt.admin.perpustakaan@sitalakbajakun.tebingtinggikota.go.id','2.17.1.1','OPERATOR DINAS PERPUSTAKAAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.ppappkb','tt.admin.ppappkb@sitalakbajakun.tebingtinggikota.go.id','2.8.1.1','OPERATOR DINAS PPAPP & KB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.pupr','tt.admin.pupr@sitalakbajakun.tebingtinggikota.go.id','1.3.1.1','OPERATOR DINAS PEKERJAAN UMUM DAN PENATAAN RUANG',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.rambutan','tt.admin.rambutan@sitalakbajakun.tebingtinggikota.go.id','4.1.1.5','OPERATOR KEC. RAMBUTAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.satpolpp','tt.admin.satpolpp@sitalakbajakun.tebingtinggikota.go.id','1.5.1.1','OPERATOR SATPOL PP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.sekwan','tt.admin.sekwan@sitalakbajakun.tebingtinggikota.go.id','4.1.1.2','OPERATOR SEKRETARIAT DPRD',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.setdako','tt.admin.setdako@sitalakbajakun.tebingtinggikota.go.id','4.1.1.1','OPERATOR SEKRETARIAT DAERAH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.admin.tebingtinggikota','tt.admin.tebingtinggikota@sitalakbajakun.tebingtinggikota.go.id','4.1.1.8','OPERATOR KEC. TEBING TINGGI KOTA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.bajenis','tt.anjab.bajenis@tebingtinggikota.go.id','4.1.1.1.10','KEC. BAJENIS',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.bappeda','tt.anjab.bappeda@tebingtinggikota.go.id','4.3.1.1','BAPPEDA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.bkd','tt.anjab.bkd@tebingtinggikota.go.id','4.5.1.1','BKD',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.bpbd','tt.anjab.bpbd@tebingtinggikota.go.id','1.5.1.4','BPBD',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.bpkpad','tt.anjab.bpkpad@tebingtinggikota.go.id','4.4.1.1','BPKPAD',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.dinkes','tt.anjab.dinkes@tebingtinggikota.go.id','1.2.1.1','DINAS KESEHATAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.dinsos','tt.anjab.dinsos@tebingtinggikota.go.id','1.6.1.1','DINAS SOSIAL',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.disdik','tt.anjab.disdik@tebingtinggikota.go.id','1.1.1.1','DINAS PENDIDIKAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.dishub','tt.anjab.dishub@tebingtinggikota.go.id','2.9.1.1','DINAS PERHUBUNGAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.diskominfo','tt.anjab.diskominfo@tebingtinggikota.go.id','2.10.1.1','DINAS KOMUNIKASI DAN INFORMATIKA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.disnaker','tt.anjab.disnaker@tebingtinggikota.go.id','2.1.1.1','DINAS KETENAGAKERJAAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.disperindag','tt.anjab.disperindag@tebingtinggikota.go.id','2.11.1.1','DINAS PERDAGANGAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.dispora','tt.anjab.dispora@tebingtinggikota.go.id','2.13.1.1','DINAS PEMUDA DAN OLAHRAGA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.dlh','tt.anjab.dlh@tebingtinggikota.go.id','2.5.1.1','DINAS LINGKUNGAN HIDUP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.dpmk','tt.anjab.dpmk@tebingtinggikota.go.id','2.7.1.1','DINAS PEMERDAYAAN MASYARAKAT KELURAHAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.dpmptsp','tt.anjab.dpmptsp@tebingtinggikota.go.id','2.12.1.1','DINAS PENANAMAN MODAL & PPTSP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.dukcapil','tt.anjab.dukcapil@tebingtinggikota.go.id','2.6.1.1','DINAS KEPENDUDUKAN DAN PENCATATAN SIPIL',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.inspektorat','tt.anjab.inspektorat@tebingtinggikota.go.id','4.2.1.1','INSPEKTORAT',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.kesbangpollinmas','tt.anjab.kesbangpollinmas@tebingtinggikota.go.id','4.1.1.3','BADAN KESBANGPOLLINMAS',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.ketapangp','tt.anjab.ketapangp@tebingtinggikota.go.id','2.3.1.1','DINAS KETAHAN PANGAN DAN PERTANIAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.padanghilir','tt.anjab.padanghilir@tebingtinggikota.go.id','4.1.1.1.12','KEC. PADANG HILIR',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.padanghulu','tt.anjab.padanghulu@tebingtinggikota.go.id','4.1.1.1.13','KEC. PADANG HULU',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.perkimsih','tt.anjab.perkimsih@tebingtinggikota.go.id','1.4.1.1','DINAS PERKIMSIH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.perpustakaan','tt.anjab.perpustakaan@tebingtinggikota.go.id','2.17.1.1','DINAS PERPUSTAKAAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.ppappkb','tt.anjab.ppappkb@tebingtinggikota.go.id','2.8.1.1','DINAS PPAPP & KB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.pupr','tt.anjab.pupr@tebingtinggikota.go.id','1.3.1.1','DINAS PEKERJAAN UMUM DAN PENATAAN RUANG',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.rambutan','tt.anjab.rambutan@tebingtinggikota.go.id','4.1.1.1.11','KEC. RAMBUTAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.satpolpp','tt.anjab.satpolpp@tebingtinggikota.go.id','1.5.1.1','SATPOL PP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.sekwan','tt.anjab.sekwan@tebingtinggikota.go.id','4.1.1.2','SEKRETARIAT DPRD',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.setdako','tt.anjab.setdako@tebingtinggikota.go.id','4.1.1.1','SEKRETARIAT DAERAH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.anjab.tebingtinggikota','tt.anjab.tebingtinggikota@tebingtinggikota.go.id','4.1.1.1.14','KEC. TEBING TINGGI KOTA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.bajenis','tt.keuangan.bajenis@sitalakbajakun.tebingtinggikota.go.id','4.1.1.4','OPERATOR KEUANGAN KEC. BAJENIS',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.bappeda','tt.keuangan.bappeda@sitalakbajakun.tebingtinggikota.go.id','4.3.1.1','OPERATOR KEUANGAN BAPPEDA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.bkd','tt.keuangan.bkd@sitalakbajakun.tebingtinggikota.go.id','4.5.1.1','OPERATOR KEUANGAN BKD',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.bpbd','tt.keuangan.bpbd@sitalakbajakun.tebingtinggikota.go.id','1.5.1.4','OPERATOR KEUANGAN BPBD',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.bpkpad','tt.keuangan.bpkpad@sitalakbajakun.tebingtinggikota.go.id','4.4.1.1','OPERATOR KEUANGAN BPKPAD',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.dinkes','tt.keuangan.dinkes@sitalakbajakun.tebingtinggikota.go.id','1.2.1.1','OPERATOR KEUANGAN DINAS KESEHATAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.dinsos','tt.keuangan.dinsos@sitalakbajakun.tebingtinggikota.go.id','1.6.1.1','OPERATOR KEUANGAN DINAS SOSIAL',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.disdik','tt.keuangan.disdik@sitalakbajakun.tebingtinggikota.go.id','1.1.1.1','OPERATOR KEUANGAN DINAS PENDIDIKAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.dishub','tt.keuangan.dishub@sitalakbajakun.tebingtinggikota.go.id','2.9.1.1','OPERATOR KEUANGAN DINAS PERHUBUNGAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.diskominfo','tt.keuangan.diskominfo@sitalakbajakun.tebingtinggikota.go.id','2.10.1.1','OPERATOR KEUANGAN DINAS KOMUNIKASI DAN INFORMATIKA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.disnaker','tt.keuangan.disnaker@sitalakbajakun.tebingtinggikota.go.id','2.1.1.1','OPERATOR KEUANGAN DINAS KETENAGAKERJAAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.disperindag','tt.keuangan.disperindag@sitalakbajakun.tebingtinggikota.go.id','2.11.1.1','OPERATOR KEUANGAN DINAS PERDAGANGAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.dispora','tt.keuangan.dispora@sitalakbajakun.tebingtinggikota.go.id','2.13.1.1','OPERATOR KEUANGAN DINAS PEMUDA DAN OLAHRAGA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.dlh','tt.keuangan.dlh@sitalakbajakun.tebingtinggikota.go.id','2.5.1.1','OPERATOR KEUANGAN DINAS LINGKUNGAN HIDUP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.dpmk','tt.keuangan.dpmk@sitalakbajakun.tebingtinggikota.go.id','2.7.1.1','OPERATOR KEUANGAN DINAS PEMERDAYAAN MASYARAKAT KELURAHAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.dpmptsp','tt.keuangan.dpmptsp@sitalakbajakun.tebingtinggikota.go.id','2.12.1.1','OPERATOR KEUANGAN DINAS PENANAMAN MODAL & PPTSP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.dukcapil','tt.keuangan.dukcapil@sitalakbajakun.tebingtinggikota.go.id','2.6.1.1','OPERATOR KEUANGAN DINAS KEPENDUDUKAN DAN PENCATATAN SIPIL',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.inspektorat','tt.keuangan.inspektorat@sitalakbajakun.tebingtinggikota.go.id','4.2.1.1','OPERATOR KEUANGAN INSPEKTORAT',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.kesbangpollinmas','tt.keuangan.kesbangpollinmas@sitalakbajakun.tebingtinggikota.go.id','4.1.1.3','OPERATOR KEUANGAN BADAN KESBANG POL',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.ketapangp','tt.keuangan.ketapangp@sitalakbajakun.tebingtinggikota.go.id','2.3.1.1','OPERATOR KEUANGAN DINAS KETAHAN PANGAN DAN PERTANIAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.padanghilir','tt.keuangan.padanghilir@sitalakbajakun.tebingtinggikota.go.id','4.1.1.6','OPERATOR KEUANGAN KEC. PADANG HILIR',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.padanghulu','tt.keuangan.padanghulu@sitalakbajakun.tebingtinggikota.go.id','4.1.1.7','OPERATOR KEUANGAN KEC. PADANG HULU',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.perkimsih','tt.keuangan.perkimsih@sitalakbajakun.tebingtinggikota.go.id','1.4.1.1','OPERATOR KEUANGAN DINAS PERKIMSIH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.perpustakaan','tt.keuangan.perpustakaan@sitalakbajakun.tebingtinggikota.go.id','2.17.1.1','OPERATOR KEUANGAN DINAS PERPUSTAKAAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.ppappkb','tt.keuangan.ppappkb@sitalakbajakun.tebingtinggikota.go.id','2.8.1.1','OPERATOR KEUANGAN DINAS PPAPP & KB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.pupr','tt.keuangan.pupr@sitalakbajakun.tebingtinggikota.go.id','1.3.1.1','OPERATOR KEUANGAN DINAS PEKERJAAN UMUM DAN PENATAAN RUANG',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.rambutan','tt.keuangan.rambutan@sitalakbajakun.tebingtinggikota.go.id','4.1.1.5','OPERATOR KEUANGAN KEC. RAMBUTAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.satpolpp','tt.keuangan.satpolpp@sitalakbajakun.tebingtinggikota.go.id','1.5.1.1','OPERATOR KEUANGAN SATPOL PP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.sekwan','tt.keuangan.sekwan@sitalakbajakun.tebingtinggikota.go.id','4.1.1.2','OPERATOR KEUANGAN SEKRETARIAT DPRD',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.setdako','tt.keuangan.setdako@sitalakbajakun.tebingtinggikota.go.id','4.1.1.1','OPERATOR KEUANGAN SEKRETARIAT DAERAH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.keuangan.tebingtinggikota','tt.keuangan.tebingtinggikota@sitalakbajakun.tebingtinggikota.go.id','4.1.1.8','OPERATOR KEUANGAN KEC. TEBING TINGGI KOTA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('tt.organisasi','organisasi@tebingtinggikota.go.id','4.1.1.1','Operator Bag. Organisasi',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-17'),
	('tt.organisasi.keuangan','keuangan@tebingtinggikota.go.id','4.1.1.1','Operator Keungan Bag. Organisasi',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-17');

/*!40000 ALTER TABLE `_userinformation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_users`;

CREATE TABLE `_users` (
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_users` WRITE;
/*!40000 ALTER TABLE `_users` DISABLE KEYS */;

INSERT INTO `_users` (`UserName`, `Password`, `RoleID`, `IsSuspend`, `LastLogin`, `LastLoginIP`)
VALUES
	('admin','3798e989b41b858040b8b69aa6f2ce90',1,0,'2021-04-07 08:39:05','::1'),
	('tt.admin.bag.adpem','827ccb0eea8a706c4c34a16891f84e7b',4,0,'2020-11-16 08:22:50','192.168.50.52'),
	('tt.admin.bag.ekosda','827ccb0eea8a706c4c34a16891f84e7b',4,0,'2021-03-03 12:29:25','192.168.33.22'),
	('tt.admin.bag.hukum','827ccb0eea8a706c4c34a16891f84e7b',4,0,'2021-03-04 10:51:27','192.168.35.245'),
	('tt.admin.bag.kesra','827ccb0eea8a706c4c34a16891f84e7b',4,0,'2021-03-02 10:57:36','114.5.146.43'),
	('tt.admin.bag.organisasi','827ccb0eea8a706c4c34a16891f84e7b',4,0,'2021-03-02 12:11:05','192.168.37.95'),
	('tt.admin.bag.pbj','827ccb0eea8a706c4c34a16891f84e7b',4,0,'2021-01-18 16:01:53','192.168.50.3'),
	('tt.admin.bag.pemerintahan','827ccb0eea8a706c4c34a16891f84e7b',4,0,'2021-03-02 10:18:05','192.168.37.16'),
	('tt.admin.bag.prokom','827ccb0eea8a706c4c34a16891f84e7b',4,0,'2021-03-04 13:02:30','192.168.43.42'),
	('tt.admin.bag.umum','a8784208bdd1f3566ff61c0be875ec40',4,0,'2021-03-02 08:35:47','192.168.32.10'),
	('tt.admin.bajenis','078563f337ec6d6fedf131ddc857db19',3,0,'2021-03-04 15:23:52','192.168.3.117'),
	('tt.admin.bappeda','ac594884e345c4b6eb26809a02263251',3,0,'2021-03-05 08:10:10','192.168.59.17'),
	('tt.admin.bkd','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-03-03 15:40:24','110.137.27.180'),
	('tt.admin.bpbd','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-03-02 14:25:03','192.168.63.27'),
	('tt.admin.bpkpad','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-03-04 09:14:53','192.168.85.124'),
	('tt.admin.dinkes','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-03-04 13:38:25','192.168.96.4'),
	('tt.admin.dinsos','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-03-03 14:15:25','180.251.72.173'),
	('tt.admin.disdik','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-03-02 10:44:16','192.168.37.70'),
	('tt.admin.dishub','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-03-04 10:48:10','10.0.139.254'),
	('tt.admin.diskominfo','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-03-05 08:09:26','10.0.21.26'),
	('tt.admin.disnaker','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-03-03 10:37:43','180.241.47.81'),
	('tt.admin.disperindag','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-03-04 14:46:53','180.241.44.85'),
	('tt.admin.dispora','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-03-04 14:16:48','10.0.139.254'),
	('tt.admin.dlh','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-03-04 10:25:44','192.168.67.3'),
	('tt.admin.dpmk','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-03-05 08:24:00','::1'),
	('tt.admin.dpmptsp','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-03-02 11:31:28','192.168.37.83'),
	('tt.admin.dukcapil','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-03-04 15:25:29','116.206.31.40'),
	('tt.admin.inspektorat','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-02-02 08:05:49','192.168.83.7'),
	('tt.admin.kesbangpollinmas','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-03-03 11:27:26','192.168.79.3'),
	('tt.admin.ketapangp','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-03-04 09:30:21','192.168.84.18'),
	('tt.admin.padanghilir','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-01-31 10:30:57','114.125.6.18'),
	('tt.admin.padanghulu','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-03-02 10:19:40','114.125.56.240'),
	('tt.admin.perkimsih','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-03-03 14:33:26','36.76.144.247'),
	('tt.admin.perpustakaan','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-03-04 11:34:12','36.68.126.88'),
	('tt.admin.ppappkb','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-03-03 11:27:52','180.241.47.148'),
	('tt.admin.pupr','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-03-03 11:25:40','192.168.76.8'),
	('tt.admin.rambutan','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-03-03 15:51:02','192.168.99.11'),
	('tt.admin.satpolpp','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-03-01 15:26:18','192.168.58.78'),
	('tt.admin.sekwan','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-03-02 11:56:14','192.168.41.95'),
	('tt.admin.setdako','827ccb0eea8a706c4c34a16891f84e7b',3,0,'2021-03-04 15:14:48','192.168.50.73'),
	('tt.admin.tebingtinggikota','f5e8d19401d8672da7c53d6d0abc1707',3,0,'2021-03-02 10:22:51','192.168.37.69'),
	('tt.anjab.bajenis','e10adc3949ba59abbe56e057f20f883e',4,0,'2020-08-18 11:11:19','180.241.44.119'),
	('tt.anjab.bappeda','e10adc3949ba59abbe56e057f20f883e',3,0,'2020-10-27 09:38:31','103.76.23.245'),
	('tt.anjab.bkd','e10adc3949ba59abbe56e057f20f883e',3,0,'2020-08-06 14:50:39','110.137.27.180'),
	('tt.anjab.bpbd','e10adc3949ba59abbe56e057f20f883e',3,0,'2020-08-18 15:12:00','116.206.38.50'),
	('tt.anjab.bpkpad','e10adc3949ba59abbe56e057f20f883e',3,0,'2021-03-02 09:27:04','192.168.37.90'),
	('tt.anjab.dinkes','e10adc3949ba59abbe56e057f20f883e',3,0,'2020-12-14 12:06:08','192.168.96.22'),
	('tt.anjab.dinsos','e10adc3949ba59abbe56e057f20f883e',3,0,'2020-11-26 11:15:30','180.241.44.92'),
	('tt.anjab.disdik','e10adc3949ba59abbe56e057f20f883e',3,0,'2020-08-24 16:34:27','114.79.4.45'),
	('tt.anjab.dishub','e10adc3949ba59abbe56e057f20f883e',3,0,'2020-09-17 09:53:31','192.168.77.5'),
	('tt.anjab.diskominfo','e10adc3949ba59abbe56e057f20f883e',3,0,'2021-02-15 12:36:35','180.241.47.23'),
	('tt.anjab.disnaker','e10adc3949ba59abbe56e057f20f883e',3,0,'2020-09-03 11:07:17','36.68.124.234'),
	('tt.anjab.disperindag','e10adc3949ba59abbe56e057f20f883e',3,0,'2020-08-31 08:45:35','120.188.35.64'),
	('tt.anjab.dispora','e10adc3949ba59abbe56e057f20f883e',3,0,'2020-08-25 09:32:19','10.0.139.254'),
	('tt.anjab.dlh','e10adc3949ba59abbe56e057f20f883e',3,0,'2020-08-21 12:56:42','114.125.15.149'),
	('tt.anjab.dpmk','e10adc3949ba59abbe56e057f20f883e',3,0,'2021-01-27 12:22:30','116.206.38.52'),
	('tt.anjab.dpmptsp','e10adc3949ba59abbe56e057f20f883e',3,0,'2020-08-07 09:46:21','192.168.80.56'),
	('tt.anjab.dukcapil','e10adc3949ba59abbe56e057f20f883e',3,0,'2020-07-17 12:50:58','36.76.162.178'),
	('tt.anjab.inspektorat','e10adc3949ba59abbe56e057f20f883e',3,0,'2020-09-17 13:17:49','180.241.46.153'),
	('tt.anjab.kesbangpollinmas','e10adc3949ba59abbe56e057f20f883e',3,0,'2021-02-15 16:17:16','182.1.63.114'),
	('tt.anjab.ketapangp','e10adc3949ba59abbe56e057f20f883e',3,0,'2020-08-03 10:18:40','192.168.84.135'),
	('tt.anjab.padanghilir','e10adc3949ba59abbe56e057f20f883e',4,0,'2021-02-15 16:25:53','182.1.61.134'),
	('tt.anjab.padanghulu','e10adc3949ba59abbe56e057f20f883e',4,0,'2021-03-02 14:17:42','180.241.45.107'),
	('tt.anjab.perkimsih','e10adc3949ba59abbe56e057f20f883e',3,0,'2020-08-10 08:36:54','192.168.78.95'),
	('tt.anjab.perpustakaan','e10adc3949ba59abbe56e057f20f883e',3,0,'2021-02-16 10:47:21','192.168.32.11'),
	('tt.anjab.ppappkb','e10adc3949ba59abbe56e057f20f883e',3,0,'2021-02-25 10:09:31','192.168.97.87'),
	('tt.anjab.pupr','e10adc3949ba59abbe56e057f20f883e',3,0,'2021-01-19 08:58:43','192.168.76.9'),
	('tt.anjab.rambutan','e10adc3949ba59abbe56e057f20f883e',4,0,'2020-09-22 10:41:51','192.168.99.1'),
	('tt.anjab.satpolpp','e10adc3949ba59abbe56e057f20f883e',3,0,'2020-08-13 14:40:19','192.168.58.252'),
	('tt.anjab.sekwan','e10adc3949ba59abbe56e057f20f883e',3,0,'2020-07-14 09:53:55','116.206.31.55'),
	('tt.anjab.setdako','e10adc3949ba59abbe56e057f20f883e',3,0,'2021-03-02 10:20:02','192.168.37.71'),
	('tt.anjab.tebingtinggikota','e10adc3949ba59abbe56e057f20f883e',4,0,'2021-02-25 09:50:28','180.241.46.78'),
	('tt.keuangan.bajenis','827ccb0eea8a706c4c34a16891f84e7b',6,0,NULL,NULL),
	('tt.keuangan.bappeda','827ccb0eea8a706c4c34a16891f84e7b',6,0,NULL,NULL),
	('tt.keuangan.bkd','827ccb0eea8a706c4c34a16891f84e7b',6,0,NULL,NULL),
	('tt.keuangan.bpbd','827ccb0eea8a706c4c34a16891f84e7b',6,0,NULL,NULL),
	('tt.keuangan.bpkpad','827ccb0eea8a706c4c34a16891f84e7b',6,0,NULL,NULL),
	('tt.keuangan.dinkes','827ccb0eea8a706c4c34a16891f84e7b',6,0,NULL,NULL),
	('tt.keuangan.dinsos','827ccb0eea8a706c4c34a16891f84e7b',6,0,NULL,NULL),
	('tt.keuangan.disdik','827ccb0eea8a706c4c34a16891f84e7b',6,0,NULL,NULL),
	('tt.keuangan.dishub','827ccb0eea8a706c4c34a16891f84e7b',6,0,NULL,NULL),
	('tt.keuangan.diskominfo','827ccb0eea8a706c4c34a16891f84e7b',6,0,NULL,NULL),
	('tt.keuangan.disnaker','827ccb0eea8a706c4c34a16891f84e7b',6,0,NULL,NULL),
	('tt.keuangan.disperindag','827ccb0eea8a706c4c34a16891f84e7b',6,0,NULL,NULL),
	('tt.keuangan.dispora','827ccb0eea8a706c4c34a16891f84e7b',6,0,'2020-03-18 04:03:41','192.168.47.32'),
	('tt.keuangan.dlh','827ccb0eea8a706c4c34a16891f84e7b',6,0,NULL,NULL),
	('tt.keuangan.dpmk','827ccb0eea8a706c4c34a16891f84e7b',6,0,'2020-11-03 14:36:48','192.168.81.44'),
	('tt.keuangan.dpmptsp','827ccb0eea8a706c4c34a16891f84e7b',6,0,NULL,NULL),
	('tt.keuangan.dukcapil','827ccb0eea8a706c4c34a16891f84e7b',6,0,NULL,NULL),
	('tt.keuangan.inspektorat','827ccb0eea8a706c4c34a16891f84e7b',6,0,NULL,NULL),
	('tt.keuangan.kesbangpollinmas','827ccb0eea8a706c4c34a16891f84e7b',6,0,NULL,NULL),
	('tt.keuangan.ketapangp','827ccb0eea8a706c4c34a16891f84e7b',6,0,NULL,NULL),
	('tt.keuangan.padanghilir','827ccb0eea8a706c4c34a16891f84e7b',6,0,NULL,NULL),
	('tt.keuangan.padanghulu','827ccb0eea8a706c4c34a16891f84e7b',6,0,NULL,NULL),
	('tt.keuangan.perkimsih','827ccb0eea8a706c4c34a16891f84e7b',6,0,NULL,NULL),
	('tt.keuangan.perpustakaan','827ccb0eea8a706c4c34a16891f84e7b',6,0,NULL,NULL),
	('tt.keuangan.ppappkb','827ccb0eea8a706c4c34a16891f84e7b',6,0,'2020-06-14 09:35:36','114.125.31.191'),
	('tt.keuangan.pupr','827ccb0eea8a706c4c34a16891f84e7b',6,0,NULL,NULL),
	('tt.keuangan.rambutan','827ccb0eea8a706c4c34a16891f84e7b',6,0,NULL,NULL),
	('tt.keuangan.satpolpp','827ccb0eea8a706c4c34a16891f84e7b',6,0,NULL,NULL),
	('tt.keuangan.sekwan','827ccb0eea8a706c4c34a16891f84e7b',6,0,NULL,NULL),
	('tt.keuangan.setdako','827ccb0eea8a706c4c34a16891f84e7b',6,0,'2020-03-17 13:50:28','114.125.26.53'),
	('tt.keuangan.tebingtinggikota','827ccb0eea8a706c4c34a16891f84e7b',6,0,NULL,NULL),
	('tt.organisasi','e10adc3949ba59abbe56e057f20f883e',3,1,'2020-03-17 08:19:24','192.168.33.31'),
	('tt.organisasi.keuangan','e10adc3949ba59abbe56e057f20f883e',6,1,'2020-02-21 09:16:32','192.168.33.10');

/*!40000 ALTER TABLE `_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ref_sub_unit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ref_sub_unit`;

CREATE TABLE `ref_sub_unit` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Nm_Sub_Unit` varchar(255) NOT NULL,
  `Nm_Pimpinan` varchar(255) DEFAULT NULL,
  `Nm_Kop1` varchar(255) DEFAULT NULL,
  `Nm_Kop2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`) USING BTREE,
  UNIQUE KEY `Uniq` (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ref_sub_unit` WRITE;
/*!40000 ALTER TABLE `ref_sub_unit` DISABLE KEYS */;

INSERT INTO `ref_sub_unit` (`Uniq`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Nm_Sub_Unit`, `Nm_Pimpinan`, `Nm_Kop1`, `Nm_Kop2`)
VALUES
	(1,1,1,1,1,'Dinas Pendidikan','Drs. H. PARDAMEAN SIREGAR, MAP',NULL,NULL),
	(2,1,2,1,1,'Dinas Kesehatan','Dr. H. NANANG FITRA AULIA, Sp. PK',NULL,NULL),
	(3,1,3,1,1,'Dinas Pekerjaan Umum Dan Penataan Ruang','Hj. RUSMIATY HARAHAP, ST',NULL,NULL),
	(4,1,4,1,1,'Dinas Perkimsih','H. MUHAMMAD HASBIE ASHSHIDDIQI, MM., Msi.',NULL,NULL),
	(5,1,5,1,1,'Satpol PP','MANAHAN GUNTUR HARAHAP, S.STP, M.Si',NULL,NULL),
	(6,1,5,1,4,'BPBD',NULL,NULL,NULL),
	(7,1,6,1,1,'Dinas Sosial','SAHBANA',NULL,NULL),
	(8,2,1,1,1,'Dinas Ketenagakerjaan','Ir. IBOY HUTAPEA',NULL,NULL),
	(9,2,3,1,1,'Dinas Ketahan Pangan Dan Pertanian','MARIMBUN MARPAUNG, SP., M.Si',NULL,NULL),
	(10,2,5,1,1,'Dinas Lingkungan Hidup','IDAM KHALID, SKM, M.Kes',NULL,NULL),
	(11,2,6,1,1,'Dinas Kependudukan Dan Pencatatan Sipil','MUHAMMAD FACHRY, S.STP, M.AP',NULL,NULL),
	(12,2,7,1,1,'Dinas Pemberdayaan Masyarakat Kelurahan','Dra. SRI WAHYUNI',NULL,NULL),
	(13,2,8,1,1,'Dinas PPAPP dan KB','HJ. NINA ZAHARA MZ, SH., M.AP',NULL,NULL),
	(14,2,9,1,1,'Dinas Perhubungan','SYAPRIN EFENDI HARAHAP, SH',NULL,NULL),
	(15,2,10,1,1,'Dinas Komunikasi Dan Informatika','DEDI PARULIAN SIAGIAN, S.STP',NULL,NULL),
	(16,2,11,1,1,'Dinas Perdagangan','GUL BAKHRI SIREGAR, SIP, M.Si',NULL,NULL),
	(17,2,12,1,1,'Dinas Penanaman Modal dan PPTSP','SURYA DARMA, SH',NULL,NULL),
	(18,2,13,1,1,'Dinas Pemuda Dan Olahraga',NULL,NULL,NULL),
	(19,2,17,1,1,'Dinas Perpustakaan','Drs. KHAIRIL ANWAR, M.Si',NULL,NULL),
	(20,4,1,1,1,'Sekretariat Daerah Kota','MUHAMMAD DIMIYATHI, S.Sos, M. TP',NULL,NULL),
	(21,4,1,1,2,'Sekretariat DPRD','MUHAMMAD SAAT NASUTION, SH',NULL,NULL),
	(22,4,1,1,3,'Badan Kesatuan Bangsa, Politik dan Perlindungan Masyarakat','ZUBIR HUSNI HARAHAP, SH','Jl. Gunung Agung Tebing Tinggi 20615','Telepon: 0621-325515, Fax: 0621-325342'),
	(23,4,1,1,4,'Kec. Bajenis','ZULIMANSYAH, SH',NULL,NULL),
	(24,4,1,1,5,'Kec. Rambutan',NULL,NULL,NULL),
	(25,4,1,1,6,'Kec. Padang Hilir','RAMADHAN BARQAH PULUNGAN, S.IP',NULL,NULL),
	(26,4,1,1,7,'Kec. Padang Hulu','DENI HANDIKA SIREGAR, SE., M.Si',NULL,NULL),
	(27,4,1,1,8,'Kec. Tebing Tinggi Kota','MANDA YULIAN, S.STP',NULL,NULL),
	(28,4,2,1,1,'Inspektorat','H. KAMLAN, SH, MM',NULL,NULL),
	(29,4,3,1,1,'BAPPEDA','ERWIN SUHERI DAMANIK',NULL,NULL),
	(30,4,4,1,1,'BPKPAD','JEFFRI SEMBIRING, SE, MM',NULL,NULL),
	(31,4,5,1,1,'BKD','SYAIFUL FAHRI, SP., M.Si',NULL,NULL);

/*!40000 ALTER TABLE `ref_sub_unit` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_dpa_kegiatan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_dpa_kegiatan`;

CREATE TABLE `sakip_dpa_kegiatan` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Nm_KegiatanOPD` varchar(200) NOT NULL,
  `Is_Renja` tinyint(1) NOT NULL,
  `Total` double NOT NULL,
  `Budget` double DEFAULT NULL,
  `Pergeseran` double DEFAULT NULL,
  `Budget_TW1` double DEFAULT NULL,
  `Budget_TW2` double DEFAULT NULL,
  `Budget_TW3` double DEFAULT NULL,
  `Budget_TW4` double DEFAULT NULL,
  `Anggaran_TW1` double DEFAULT NULL,
  `Anggaran_TW2` double DEFAULT NULL,
  `Anggaran_TW3` double DEFAULT NULL,
  `Anggaran_TW4` double DEFAULT NULL,
  `Kd_SumberDana` varchar(200) NOT NULL,
  `Create_By` varchar(200) NOT NULL,
  `Create_Date` datetime NOT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`),
  KEY `FK_Subbid2` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FK_DPA_SasaranProgram2` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) REFERENCES `sakip_dpa_program_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_Subbid2` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_dpa_kegiatan_grup
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_dpa_kegiatan_grup`;

CREATE TABLE `sakip_dpa_kegiatan_grup` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Nm_GrupKegiatanOPD` varchar(200) NOT NULL DEFAULT '',
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_SUBBID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_dpa_kegiatan_indikator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_dpa_kegiatan_indikator`;

CREATE TABLE `sakip_dpa_kegiatan_indikator` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Kd_SasaranKegiatanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorKegiatanOPD` bigint(10) NOT NULL,
  `Nm_IndikatorKegiatanOPD` varchar(200) NOT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  `Nm_PenanggungJawab` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_TW1` double DEFAULT NULL,
  `Target_TW2` double DEFAULT NULL,
  `Target_TW3` double DEFAULT NULL,
  `Target_TW4` double DEFAULT NULL,
  `Kinerja_TW1` double DEFAULT NULL,
  `Kinerja_TW2` double DEFAULT NULL,
  `Kinerja_TW3` double DEFAULT NULL,
  `Kinerja_TW4` double DEFAULT NULL,
  `Anggaran_TW1` double DEFAULT NULL,
  `Anggaran_TW2` double DEFAULT NULL,
  `Anggaran_TW3` double DEFAULT NULL,
  `Anggaran_TW4` double DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`,`Kd_SasaranKegiatanOPD`,`Kd_IndikatorKegiatanOPD`),
  CONSTRAINT `FK_DPA_SasaranKeg` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Kd_SasaranKegiatanOPD`) REFERENCES `sakip_dpa_kegiatan_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Kd_SasaranKegiatanOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_dpa_kegiatan_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_dpa_kegiatan_sasaran`;

CREATE TABLE `sakip_dpa_kegiatan_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Kd_SasaranKegiatanOPD` bigint(10) NOT NULL,
  `Nm_SasaranKegiatanOPD` varchar(200) NOT NULL,
  `Kd_Satuan` varchar(50) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_TW1` double DEFAULT NULL,
  `Target_TW2` double DEFAULT NULL,
  `Target_TW3` double DEFAULT NULL,
  `Target_TW4` double DEFAULT NULL,
  `Budget` double DEFAULT NULL,
  `Budget_TW1` double DEFAULT NULL,
  `Budget_TW2` double DEFAULT NULL,
  `Budget_TW3` double DEFAULT NULL,
  `Budget_TW4` double DEFAULT NULL,
  `Kinerja_TW1` double DEFAULT NULL,
  `Kinerja_TW2` double DEFAULT NULL,
  `Kinerja_TW3` double DEFAULT NULL,
  `Kinerja_TW4` double DEFAULT NULL,
  `Anggaran_TW1` double DEFAULT NULL,
  `Anggaran_TW2` double DEFAULT NULL,
  `Anggaran_TW3` double DEFAULT NULL,
  `Anggaran_TW4` double DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`,`Kd_SasaranKegiatanOPD`),
  KEY `FK_DPA_Subbid` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FK_DPA_Kegiatan` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`) REFERENCES `sakip_dpa_kegiatan` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_DPA_Subbid` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_dpa_program
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_dpa_program`;

CREATE TABLE `sakip_dpa_program` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Nm_ProgramOPD` varchar(200) NOT NULL,
  `Is_Renja` tinyint(1) NOT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`),
  KEY `FK_DPA_Bid` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `FK_DPA_Bid` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_dpa_program_indikator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_dpa_program_indikator`;

CREATE TABLE `sakip_dpa_program_indikator` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) NOT NULL,
  `Nm_IndikatorProgramOPD` varchar(200) NOT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  `Nm_PenanggungJawab` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_TW1` double DEFAULT NULL,
  `Target_TW2` double DEFAULT NULL,
  `Target_TW3` double DEFAULT NULL,
  `Target_TW4` double DEFAULT NULL,
  `Kinerja_TW1` double DEFAULT NULL,
  `Kinerja_TW2` double DEFAULT NULL,
  `Kinerja_TW3` double DEFAULT NULL,
  `Kinerja_TW4` double DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_IndikatorProgramOPD`),
  CONSTRAINT `FK_DPA_SasaranProgram` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) REFERENCES `sakip_dpa_program_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_dpa_program_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_dpa_program_sasaran`;

CREATE TABLE `sakip_dpa_program_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Nm_SasaranProgramOPD` varchar(200) NOT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_TW1` double DEFAULT NULL,
  `Target_TW2` double DEFAULT NULL,
  `Target_TW3` double DEFAULT NULL,
  `Target_TW4` double DEFAULT NULL,
  `Kinerja_TW1` double DEFAULT NULL,
  `Kinerja_TW2` double DEFAULT NULL,
  `Kinerja_TW3` double DEFAULT NULL,
  `Kinerja_TW4` double DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`),
  KEY `FK_DPA_Bid2` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `DK_DPA_Program2` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`) REFERENCES `sakip_dpa_program` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_DPA_Bid2` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_individu_indikator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_individu_indikator`;

CREATE TABLE `sakip_individu_indikator` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) unsigned NOT NULL,
  `Kd_Misi` bigint(10) unsigned NOT NULL,
  `Kd_Tujuan` bigint(10) unsigned NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) unsigned NOT NULL,
  `Kd_Sasaran` bigint(10) unsigned NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) unsigned NOT NULL,
  `Kd_Tahun` bigint(10) unsigned NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) unsigned NOT NULL,
  `Kd_SasaranOPD` bigint(10) unsigned NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) unsigned NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) unsigned NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) unsigned NOT NULL,
  `Kd_SasaranSubbidang` bigint(10) unsigned NOT NULL,
  `Kd_SasaranIndividu` bigint(10) unsigned NOT NULL,
  `Kd_IndikatorIndividu` bigint(10) unsigned NOT NULL,
  `Nm_SasaranIndividu` varchar(255) NOT NULL DEFAULT '',
  `Nm_IndikatorIndividu` varchar(255) NOT NULL DEFAULT '',
  `Nm_Target` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_SasaranProgramOPD`,`Kd_IndikatorProgramOPD`,`Kd_SasaranSubbidang`,`Kd_SasaranIndividu`,`Kd_IndikatorIndividu`) USING BTREE,
  KEY `Uniq` (`Uniq`),
  KEY `FK_SUBBID2` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FK_Indikator_Sasaran` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_SasaranProgramOPD`, `Kd_IndikatorProgramOPD`, `Kd_SasaranSubbidang`) REFERENCES `sakip_individu_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_SasaranProgramOPD`, `Kd_IndikatorProgramOPD`, `Kd_SasaranSubbidang`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_individu_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_individu_sasaran`;

CREATE TABLE `sakip_individu_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) unsigned NOT NULL,
  `Kd_Misi` bigint(10) unsigned NOT NULL,
  `Kd_Tujuan` bigint(10) unsigned NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) unsigned NOT NULL,
  `Kd_Sasaran` bigint(10) unsigned NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) unsigned NOT NULL,
  `Kd_Tahun` bigint(10) unsigned NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) unsigned NOT NULL,
  `Kd_SasaranOPD` bigint(10) unsigned NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) unsigned NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) unsigned NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) unsigned NOT NULL,
  `Kd_SasaranSubbidang` bigint(10) unsigned NOT NULL,
  `Kd_SasaranIndividu` bigint(10) unsigned NOT NULL,
  `Nm_Jabatan` varchar(50) NOT NULL DEFAULT '',
  `Nm_Pegawai` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_SasaranProgramOPD`,`Kd_IndikatorProgramOPD`,`Kd_SasaranSubbidang`,`Kd_SasaranIndividu`) USING BTREE,
  KEY `Uniq` (`Uniq`),
  KEY `FK_Individu_Subbid` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FK_Individu_Subbid` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_mbid
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mbid`;

CREATE TABLE `sakip_mbid` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Nm_Bid` varchar(200) DEFAULT NULL,
  `Nm_Kabid` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  KEY `Uniq` (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_mbid_indikator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mbid_indikator`;

CREATE TABLE `sakip_mbid_indikator` (
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) DEFAULT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) NOT NULL,
  `Nm_IndikatorProgramOPD` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_SasaranProgramOPD`,`Kd_IndikatorProgramOPD`),
  KEY `FK_BID3` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `FK_BID3` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_BID_SASARAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_SasaranProgramOPD`) REFERENCES `sakip_mbid_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_SasaranProgramOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_mbid_program
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mbid_program`;

CREATE TABLE `sakip_mbid_program` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Nm_ProgramOPD` varchar(200) DEFAULT NULL,
  `Nm_SasaranProgram` varchar(200) DEFAULT NULL,
  `Nm_IndikatorProgram` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_N1` double DEFAULT NULL,
  `IsEplan` tinyint(1) NOT NULL DEFAULT '1',
  `Remarks` varchar(200) DEFAULT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_OPD_IKSASARAN` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`),
  KEY `FK_BID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `FK_BID` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_OPD_IKSASARAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`) REFERENCES `sakip_mopd_iksasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_mbid_program_indikator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mbid_program_indikator`;

CREATE TABLE `sakip_mbid_program_indikator` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) NOT NULL,
  `Nm_IndikatorProgramOPD` varchar(200) DEFAULT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  `Nm_PenanggungJawab` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Akhir` double DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_IndikatorProgramOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_Bid6` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `FK_Bid6` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_IndikatorSasaran` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) REFERENCES `sakip_mbid_program_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_mbid_program_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mbid_program_sasaran`;

CREATE TABLE `sakip_mbid_program_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Nm_SasaranProgramOPD` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Akhir` double DEFAULT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_OPD_IKSASARAN` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`),
  KEY `FK_BID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `FK_SasaranBid` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SasaranProgram` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`) REFERENCES `sakip_mbid_program` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_mbid_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mbid_sasaran`;

CREATE TABLE `sakip_mbid_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) DEFAULT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Nm_SasaranProgramOPD` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_SasaranProgramOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_BID2` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  KEY `FK_OPD_IKSASARAN_TO_BID_SASARAN` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`),
  CONSTRAINT `FK_BID2` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_OPD_IKSASARAN_TO_BID_SASARAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`) REFERENCES `sakip_mopd_iksasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_mopd_file
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mopd_file`;

CREATE TABLE `sakip_mopd_file` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_Type` varchar(50) NOT NULL,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) DEFAULT NULL,
  `Nm_Keterangan` varchar(200) DEFAULT NULL,
  `Nm_File` varchar(200) NOT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `Uniq` (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_mopd_iksasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mopd_iksasaran`;

CREATE TABLE `sakip_mopd_iksasaran` (
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) DEFAULT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Nm_IndikatorSasaranOPD` varchar(500) DEFAULT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  `Nm_PenanggungJawab` varchar(200) DEFAULT NULL,
  `Nm_Target` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`),
  CONSTRAINT `FK_OPD_SASARAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`) REFERENCES `sakip_mopd_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_mopd_iksasaran_capaian
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mopd_iksasaran_capaian`;

CREATE TABLE `sakip_mopd_iksasaran_capaian` (
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Target` varchar(200) DEFAULT NULL,
  `Target_TW1` varchar(200) DEFAULT NULL,
  `Target_TW2` varchar(200) DEFAULT NULL,
  `Target_TW3` varchar(200) DEFAULT NULL,
  `Target_TW4` varchar(200) DEFAULT NULL,
  `Realisasi` varchar(200) DEFAULT NULL,
  `Realisasi_TW1` varchar(200) DEFAULT NULL,
  `Realisasi_TW2` varchar(200) DEFAULT NULL,
  `Realisasi_TW3` varchar(200) DEFAULT NULL,
  `Realisasi_TW4` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_mopd_iktujuan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mopd_iktujuan`;

CREATE TABLE `sakip_mopd_iktujuan` (
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) DEFAULT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Nm_IndikatorTujuanOPD` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`),
  KEY `FK_OPD_TUJUAN` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_TujuanOPD`),
  CONSTRAINT `FK_OPD_TUJUAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`) REFERENCES `sakip_mopd_tujuan` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_mopd_lakip
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mopd_lakip`;

CREATE TABLE `sakip_mopd_lakip` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) DEFAULT NULL,
  `Kd_Tahun` bigint(10) DEFAULT NULL,
  `Kd_Urusan` bigint(10) DEFAULT NULL,
  `Kd_Bidang` bigint(10) DEFAULT NULL,
  `Kd_Unit` bigint(10) DEFAULT NULL,
  `Kd_Sub` bigint(10) DEFAULT NULL,
  `Nm_FormLakip` text,
  `Create_By` varchar(50) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_mopd_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mopd_sasaran`;

CREATE TABLE `sakip_mopd_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) DEFAULT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Nm_SasaranOPD` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`),
  KEY `Uniq` (`Uniq`),
  CONSTRAINT `FK_OPD_IKTUJUAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`) REFERENCES `sakip_mopd_iktujuan` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_mopd_tujuan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mopd_tujuan`;

CREATE TABLE `sakip_mopd_tujuan` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) DEFAULT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Nm_TujuanOPD` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`),
  KEY `Uniq` (`Uniq`),
  CONSTRAINT `FK_PEMDA_IKSASARAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`) REFERENCES `sakip_mpmd_iksasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_mpemda
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mpemda`;

CREATE TABLE `sakip_mpemda` (
  `Kd_Pemda` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Prov` bigint(10) NOT NULL,
  `Kd_Kab` bigint(10) NOT NULL,
  `Kd_Tahun_From` bigint(10) NOT NULL,
  `Kd_Tahun_To` bigint(10) NOT NULL,
  `Nm_Kab` varchar(200) DEFAULT NULL,
  `Nm_Pejabat` varchar(200) DEFAULT NULL,
  `Nm_Posisi` varchar(200) DEFAULT NULL,
  `Nm_Visi` varchar(200) DEFAULT NULL,
  `Nm_Alamat_Kab` varchar(200) DEFAULT NULL,
  `Nm_Ket_Periode` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Prov`,`Kd_Kab`,`Kd_Tahun_From`,`Kd_Tahun_To`),
  KEY `Kd_Pemda` (`Kd_Pemda`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_mpemda` WRITE;
/*!40000 ALTER TABLE `sakip_mpemda` DISABLE KEYS */;

INSERT INTO `sakip_mpemda` (`Kd_Pemda`, `Kd_Prov`, `Kd_Kab`, `Kd_Tahun_From`, `Kd_Tahun_To`, `Nm_Kab`, `Nm_Pejabat`, `Nm_Posisi`, `Nm_Visi`, `Nm_Alamat_Kab`, `Nm_Ket_Periode`)
VALUES
	(1,12,16,2019,2024,'Batubara','Ir. H. Zahir, M. AP','Bupati','Menjadikan Masyarakat Kabupaten Batu Bara Masyarakat Industri yang Sejahtera, Mandiri dan Berbudaya',NULL,NULL);

/*!40000 ALTER TABLE `sakip_mpemda` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_mpmd_iksasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mpmd_iksasaran`;

CREATE TABLE `sakip_mpmd_iksasaran` (
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Nm_IndikatorSasaran` varchar(200) DEFAULT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`),
  CONSTRAINT `FK_PEMDA_SASARAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`) REFERENCES `sakip_mpmd_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_mpmd_iksasaran` WRITE;
/*!40000 ALTER TABLE `sakip_mpmd_iksasaran` DISABLE KEYS */;

INSERT INTO `sakip_mpmd_iksasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Nm_IndikatorSasaran`, `Nm_Formula`, `Nm_SumberData`)
VALUES
	(1,1,1,1,1,1,'Opini BPK terhadap Laporan Keuangan','Penilaian opini yang di keluarkan oleh BPK terhadap laporan\nkeuangan daerah','BPK'),
	(1,1,1,1,1,2,'Tingkat Akuntabilitas Kinerja','Hasil Penilaian KEMENPAN RB','KEMENPAN RB'),
	(1,1,1,1,2,1,'Indeks Kepuasan Masyarakat (IKM)','(Total dari Nilai Persepsi per Unsur / Total unsur yang terisi) X Nilai Penimbang','Bagian Organisasi'),
	(1,2,1,1,1,1,'PDRB Per Kapita (juta rupiah)','PDRB / Penduduk Pertengahan tahun','BPS'),
	(1,2,1,1,1,2,'Inflasi','(nilai pada 1 tahun berikutnya - nilai inflasi pada tahun berjalan) / nilai inflasi pada tahun berjalan X 100%','BPS'),
	(1,2,1,1,1,3,'Nilai Investasi (milyar rupiah)','Jumlah nilai investasi berskala nasional (PMDN/PMA)','DPMPPTSP'),
	(1,3,1,1,1,1,'Angka Melek Huruf','(Jumlah penduduk (usia diatas 15 tahun) yang bisa\nmenulis pada tahun t / Jumlah penduduk usia 15 tahun keatas) X 100','Dinas Pendidikan'),
	(1,3,1,1,1,2,'Rata-rata Lama Sekolah','Kombinasi antara partisipasi sekolah, jenjang pendidikan yang\nsedang dijalani, kelas yang diduduki dan pendidikan yang\nditamatkan.','Dinas Pendidikan'),
	(1,3,1,1,1,3,'Nilai Rata-rata Ujian Akhir Nasional','(Jumlah Nilai UAN / Jumlah Peserta) X 100%','Dinas Pendidikan'),
	(1,3,1,1,2,1,'Angka Harapan Hidup','Rata-rata banyaknya tahun yang ditempuh penduduk yang masih hidup sampai umur tertentu','BPS'),
	(1,3,2,1,1,1,'Tingkat Pengangguran Terbuka','(Jumlah penganggur terbuka usia angkatan kerja /Jumlah penduduk angkatan kerja) X 100%','Dinas Ketenagakerjaan'),
	(1,3,2,1,1,2,'Rata-rata pendapatan masyarakat','Rata-rata pendapatan masyarakat','BPS'),
	(1,4,1,1,1,1,'Pasar yang berkualitas','',''),
	(1,4,1,1,1,2,'Presentase jalan dalam kondisi mantap','(panjang jalan kondisi sedang  dan baik / total panjang jalan kota) x 100%','Dinas pupr'),
	(1,4,2,1,1,1,'Indeks Kualitas Air','Hasil Pengukuruan Indeks kualitas Air','Dinas Lingkungan Hidup'),
	(1,4,2,1,1,2,'Indeks Kuliatas Udara','Hasil Pengukuruan Indeks kualitas udara','Dinas Lingkungan Hidup'),
	(1,4,2,1,1,3,'Tutupan Lahan (ha)','Hasil Pengukuruan Tutupan Lahan','Dinas Lingkungan Hidup');

/*!40000 ALTER TABLE `sakip_mpmd_iksasaran` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_mpmd_iksasaran_capaian
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mpmd_iksasaran_capaian`;

CREATE TABLE `sakip_mpmd_iksasaran_capaian` (
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Target` varchar(200) DEFAULT NULL,
  `Target_TW1` varchar(200) DEFAULT NULL,
  `Target_TW2` varchar(200) DEFAULT NULL,
  `Target_TW3` varchar(200) DEFAULT NULL,
  `Target_TW4` varchar(200) DEFAULT NULL,
  `Realisasi` varchar(200) DEFAULT NULL,
  `Realisasi_TW1` varchar(200) DEFAULT NULL,
  `Realisasi_TW2` varchar(200) DEFAULT NULL,
  `Realisasi_TW3` varchar(200) DEFAULT NULL,
  `Realisasi_TW4` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`),
  CONSTRAINT `sakip_mpmd_iksasaran_capaian_ibfk_1` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`) REFERENCES `sakip_mpmd_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_mpmd_iksasaran_capaian` WRITE;
/*!40000 ALTER TABLE `sakip_mpmd_iksasaran_capaian` DISABLE KEYS */;

INSERT INTO `sakip_mpmd_iksasaran_capaian` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Target`, `Target_TW1`, `Target_TW2`, `Target_TW3`, `Target_TW4`, `Realisasi`, `Realisasi_TW1`, `Realisasi_TW2`, `Realisasi_TW3`, `Realisasi_TW4`)
VALUES
	(1,1,1,1,1,1,2020,'WTP','-','-','-','-','WTP','-','-','-','-'),
	(1,1,1,1,1,2,2020,'B','-','-','-','-',NULL,'-','-','-','-'),
	(1,1,1,1,2,1,2020,'3.75','3.75','3.75','3.75','3.75','3,45','3.52','3.54','3,46','3,45'),
	(1,2,1,1,1,1,2020,'36','-','-','-','-',NULL,'-','-','-','-'),
	(1,2,1,1,1,2,2020,'2.05','-','-','-','-','2,78','-','-','-','-'),
	(1,2,1,1,1,3,2020,'193,123','-','-','-','-','3.220.943.628.467','-','-','-','-'),
	(1,3,1,1,1,1,2020,'99.73%','-','99.81%','-','-','99,83%','-','99.65%','-','-'),
	(1,3,1,1,1,2,2020,'11.52 Tahun','-','13 Tahun','-','-','10,69 tahun','-','-10.28 Tahun','-','-'),
	(1,3,1,1,1,3,2020,'58.62','-','70.55','-','-','85,15','-','-','-','-'),
	(1,3,1,1,2,1,2020,'70.88 Tahun','-','-','-','-','70,87 tahun','-','-','-','-'),
	(1,3,2,1,1,1,2020,'6.10%','-','-','-','-','9,98 %','-','-','-','-'),
	(1,3,2,1,1,2,2020,'2.454.089','-','-','-','-',NULL,'-','-','-','-'),
	(1,4,1,1,1,1,2020,'85%','85  %','85 %','85  %','85  %',NULL,'78,57 %','78,57%','78,57 %',NULL),
	(1,4,1,1,1,2,2020,'68%','60%','65%','66%','67%',NULL,'75,13%','75,02%','75,02%',NULL),
	(1,4,2,1,1,1,2020,'100','-','-','-','-','96,33','-','-','-','-'),
	(1,4,2,1,1,2,2020,'85','-','-','-','-','77,6','-','-','-','-'),
	(1,4,2,1,1,3,2020,'248.775','-','-','-','-','47,28','-','-','-','-');

/*!40000 ALTER TABLE `sakip_mpmd_iksasaran_capaian` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_mpmd_iktujuan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mpmd_iktujuan`;

CREATE TABLE `sakip_mpmd_iktujuan` (
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Nm_IndikatorTujuan` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`),
  CONSTRAINT `FK_TUJUAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`) REFERENCES `sakip_mpmd_tujuan` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_mpmd_iktujuan` WRITE;
/*!40000 ALTER TABLE `sakip_mpmd_iktujuan` DISABLE KEYS */;

INSERT INTO `sakip_mpmd_iktujuan` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Nm_IndikatorTujuan`)
VALUES
	(1,1,1,1,'Indeks Reformasi Birokrasi Kota Tebing Tinggi'),
	(1,2,1,1,'Pertumbuhan Ekonomi'),
	(1,3,1,1,'Indeks Pembangunan Manusia (IPM)'),
	(1,3,2,1,'Angka Kemiskinan'),
	(1,4,1,1,'Fasilitas Umum yang Berkualitas'),
	(1,4,2,1,'Indeks Kualitas Lingkungan Hidup (IKLH)');

/*!40000 ALTER TABLE `sakip_mpmd_iktujuan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_mpmd_misi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mpmd_misi`;

CREATE TABLE `sakip_mpmd_misi` (
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Nm_Misi` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`),
  CONSTRAINT `FK_PEMDA` FOREIGN KEY (`Kd_Pemda`) REFERENCES `sakip_mpemda` (`Kd_Pemda`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_mpmd_misi` WRITE;
/*!40000 ALTER TABLE `sakip_mpmd_misi` DISABLE KEYS */;

INSERT INTO `sakip_mpmd_misi` (`Kd_Pemda`, `Kd_Misi`, `Nm_Misi`)
VALUES
	(1,1,'Meningkatkan Pelayanan Aparatur Pemerintah atas Pelayanan Publik dan Investasi'),
	(1,2,'Meningkatkan Jumlah dan Kualitas Infrastruktur dan Sarana Prasarana Pendukung Pertumbuhan Industri dan Perekonomian Masyarakat'),
	(1,3,'Mewujudkan Masyarakat yang Produktif, Inovatif dan Berbudaya'),
	(1,4,'Mewujudkan Industri Berbasis Sumber Daya Unggulan Kabupaten Batu Bara'),
	(1,5,'Meningkatkan Pemasaran Hasil Industri, Pertanian, dan Perikanan Secara Meluas Memanfaatkan Teknologi Berkembang'),
	(1,6,'Meningkatkan Kolaborasi Industri, Lembaga Pendidikan, dan Pemerintahan Kabupaten Batu Bara'),
	(1,7,'Meningkatkan Kualitas Pendidikan, Kesehatan, dan Spiritual Masyarakat'),
	(1,8,'Meningkatkan Peran Serta Seluruh Elemen Masyarakat dalam Pembangunan Kabupaten Batu Bara');

/*!40000 ALTER TABLE `sakip_mpmd_misi` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_mpmd_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mpmd_sasaran`;

CREATE TABLE `sakip_mpmd_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Nm_Sasaran` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`),
  KEY `Uniq` (`Uniq`),
  CONSTRAINT `FK_PEMDA_IKTUJUAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`) REFERENCES `sakip_mpmd_iktujuan` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_mpmd_sasaran` WRITE;
/*!40000 ALTER TABLE `sakip_mpmd_sasaran` DISABLE KEYS */;

INSERT INTO `sakip_mpmd_sasaran` (`Uniq`, `Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Nm_Sasaran`)
VALUES
	(1,1,1,1,1,1,'Meningkatnya Tata Kelola Pemerintahan yang Bersih dan Akuntabel'),
	(3,1,1,1,1,2,'Meningkatnya Tata Kelola Pemerintahan yang Melayani'),
	(4,1,2,1,1,1,'Meningkatnya iklim investasi daerah'),
	(5,1,3,1,1,1,'Meningkatnya akses dan layanan PAUD dan pendidikan dasar yang bermutu'),
	(6,1,3,1,1,2,'Meningkatkan mutu pelayanan kesehatan yang bermutu dan sesuai standar'),
	(7,1,3,2,1,1,'Meningkatnya kualitas sosial masyarakat'),
	(8,1,4,1,1,1,'Meningkatnya infrastruktur, sarana dan prasarana perkotaan yang berkualitas dan berkelanjutan'),
	(9,1,4,2,1,1,'Meningkatnya kualitas lingkungan hidup');

/*!40000 ALTER TABLE `sakip_mpmd_sasaran` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_mpmd_tujuan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_mpmd_tujuan`;

CREATE TABLE `sakip_mpmd_tujuan` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Nm_Tujuan` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`),
  KEY `Uniq` (`Uniq`),
  CONSTRAINT `FK_MISI` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`) REFERENCES `sakip_mpmd_misi` (`Kd_Pemda`, `Kd_Misi`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_mpmd_tujuan` WRITE;
/*!40000 ALTER TABLE `sakip_mpmd_tujuan` DISABLE KEYS */;

INSERT INTO `sakip_mpmd_tujuan` (`Uniq`, `Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Nm_Tujuan`)
VALUES
	(1,1,1,1,'Meningkatkan Tata Kelola Pemerintahan yang Bersih dan Melayani'),
	(2,1,2,1,'Meningkatkan Investasi Daerah untuk Kesejahteraan Masyarakat'),
	(3,1,3,1,'Kota Tebing Tinggi sebagai Pusat Pelayanan Pendidikan dan Kesehatan'),
	(4,1,3,2,'Meningkatkan Derajat Kehidupan Masyarakat'),
	(5,1,4,1,'Meningkatnya Kualitas Sarana Prasarana Perkotaan'),
	(6,1,4,2,'Perilaku Masyarakat Berwawasan Lingkungan');

/*!40000 ALTER TABLE `sakip_mpmd_tujuan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_msatuan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_msatuan`;

CREATE TABLE `sakip_msatuan` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Satuan` varchar(50) NOT NULL,
  `Nm_Satuan` varchar(50) NOT NULL,
  `Create_By` varchar(200) NOT NULL,
  `Create_Date` datetime NOT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `sakip_msatuan` WRITE;
/*!40000 ALTER TABLE `sakip_msatuan` DISABLE KEYS */;

INSERT INTO `sakip_msatuan` (`Uniq`, `Kd_Satuan`, `Nm_Satuan`, `Create_By`, `Create_Date`, `Edit_By`, `Edit_Date`)
VALUES
	(1,'%','Persen','admin','2019-03-14 23:59:59',NULL,NULL),
	(2,'Nilai','Nilai','admin','2019-03-14 23:59:59',NULL,NULL),
	(3,'Indeks','Indeks','admin','2019-03-14 23:59:59',NULL,NULL),
	(4,'Unit','Unit','admin','2019-03-14 23:59:59','admin','2019-08-27 02:23:25'),
	(5,'ORG','Orang','admin','2019-03-14 23:59:59',NULL,NULL),
	(6,'OP','Opini','admin','2019-03-14 23:59:59',NULL,NULL),
	(7,'KEL','Kelompok','admin','2019-03-14 23:59:59',NULL,NULL),
	(8,'CABOR','Cabor','admin','2019-03-14 23:59:59',NULL,NULL),
	(9,'TIM','Tim','admin','2019-03-14 23:59:59',NULL,NULL),
	(10,'Dokumen','Dokumen','admin','2019-03-14 23:59:59','admin','2019-08-27 02:23:33'),
	(11,'Jasa','Jasa','admin','2019-03-14 23:59:59',NULL,NULL),
	(12,'Item','Item','admin','2019-03-14 23:59:59',NULL,NULL),
	(13,'Jenis','Jenis','admin','2019-03-14 23:59:59',NULL,NULL),
	(14,'Bahan Bacaan','Bahan Bacaan','admin','2019-03-14 23:59:59',NULL,NULL),
	(15,'Kebutuhan','Kebutuhan','admin','2019-03-14 23:59:59',NULL,NULL),
	(16,'Kali','Kali','admin','2019-03-14 23:59:59',NULL,NULL),
	(17,'Lembaga','Lembaga','admin','2019-03-14 23:59:59',NULL,NULL),
	(18,'Paket','Paket','admin','2019-03-14 23:59:59',NULL,NULL),
	(19,'Gugus & IGTK','Gugus & IGTK','admin','2019-03-14 23:59:59',NULL,NULL),
	(20,'Ruang','Ruang','admin','2019-03-14 23:59:59',NULL,NULL),
	(21,'KK-DATADIK PAUD','KK-DATADIK PAUD','admin','2019-03-14 23:59:59',NULL,NULL),
	(22,'LS','LS','admin','2019-03-14 23:59:59',NULL,NULL),
	(23,'Siswa','Siswa','admin','2019-03-14 23:59:59',NULL,NULL),
	(24,'Mapel','Mapel','admin','2019-03-14 23:59:59',NULL,NULL),
	(25,'Mata Lomba','Mata Lomba','admin','2019-03-14 23:59:59',NULL,NULL),
	(26,'Kegiatan','Kegiatan','admin','2019-03-14 23:59:59',NULL,NULL),
	(27,'Buku','Buku','admin','2019-03-14 23:59:59',NULL,NULL),
	(28,'Sekolah','Sekolah','admin','2019-03-14 23:59:59',NULL,NULL),
	(29,'Lokasi','Lokasi','admin','2019-03-14 23:59:59',NULL,NULL),
	(30,'Exemplar','Exemplar','admin','2019-03-14 23:59:59',NULL,NULL),
	(31,'Set','Set','admin','2019-03-14 23:59:59',NULL,NULL),
	(32,'Event','Event','admin','2019-03-14 23:59:59',NULL,NULL),
	(33,'Keping','Keping','admin','2019-03-14 23:59:59',NULL,NULL),
	(34,'Aplikasi','Aplikasi','admin','2019-03-14 23:59:59',NULL,NULL),
	(35,'Pagelaran','Pagelaran','admin','2019-03-14 23:59:59',NULL,NULL),
	(36,'Wadah','Wadah','admin','2019-03-14 23:59:59',NULL,NULL),
	(37,'Buah','Buah','admin','2019-03-14 23:59:59',NULL,NULL),
	(38,'Desa','Desa','admin','2019-03-14 23:59:59',NULL,NULL),
	(39,'Batas Perwilayahan','Batas Perwilayahan','admin','2019-03-14 23:59:59',NULL,NULL),
	(40,'Produk Hukum','Produk Hukum','admin','2019-03-14 23:59:59',NULL,NULL),
	(41,'WP','WP','admin','2019-03-14 23:59:59',NULL,NULL),
	(42,'Rekening','Rekening','admin','2019-03-15 08:49:13',NULL,NULL),
	(43,'OH','OH','admin','2019-03-15 08:49:23',NULL,NULL),
	(44,'Ha','Hektare','admin','2019-03-19 11:01:43','admin','2019-08-27 02:23:47'),
	(45,'Ton','Ton','admin','2019-03-19 11:01:59',NULL,NULL),
	(47,'Batang','Batang','admin','2019-03-19 11:02:30',NULL,NULL),
	(48,'Komoditi','Komoditi','admin','2019-03-19 11:02:51',NULL,NULL),
	(49,'Poktan','Poktan','admin','2019-03-19 12:15:56',NULL,NULL),
	(51,'Km','Kilometer','admin','2019-03-20 12:03:37',NULL,NULL),
	(52,'Penangkar','Penangkar','admin','2019-03-20 12:51:18',NULL,NULL),
	(53,'WTP','Wajar Tanpa Pengecualian','admin','2019-03-20 12:56:22',NULL,NULL),
	(54,'WDP','Wajar Dengan Pengecualian','admin','2019-03-20 12:56:37',NULL,NULL),
	(55,'Disclaimer','Disclaimer','admin','2019-03-20 12:57:04',NULL,NULL),
	(56,'Level','Level','admin','2019-03-20 12:57:14',NULL,NULL),
	(57,'Laporan','Laporan','admin','2019-03-20 12:57:25',NULL,NULL),
	(58,'Rencana Aksi','Rencana Aksi','admin','2019-03-20 12:57:41',NULL,NULL),
	(59,'OPD','OPD','admin','2019-03-20 12:57:59','admin','2019-08-22 09:09:23'),
	(60,'Koperasi','Koperasi','admin','2019-03-21 04:26:35',NULL,NULL),
	(61,'UMKM','UMKM','admin','2019-03-21 04:26:48',NULL,NULL),
	(62,'IKM','IKM','admin','2019-03-21 04:27:32',NULL,NULL),
	(63,'Pasar','Pasar','admin','2019-03-21 04:28:21',NULL,NULL),
	(64,'Rumah Tangga','Rumah Tangga','admin','2019-03-21 05:07:57',NULL,NULL),
	(65,'Meter','Meter','admin','2019-03-21 05:08:06',NULL,NULL),
	(66,'65','kecamatan','admin','2019-05-06 09:53:05',NULL,NULL),
	(67,'Bulan','Bulan','admin','2019-07-16 03:55:03',NULL,NULL),
	(68,'Unit Kerja','Unit Kerja','admin','2019-08-15 03:49:22',NULL,NULL),
	(69,'Perpustakaan','Perpustakaan','admin','2019-08-15 03:49:33',NULL,NULL),
	(72,'Ekor','Ekor','admin','2019-08-15 03:49:54',NULL,NULL),
	(74,'Hari','Hari','admin','2019-08-19 07:13:55',NULL,NULL),
	(75,'Bungkus','Bungkus','admin','2019-08-19 07:14:08',NULL,NULL),
	(76,'Tilang','Tilang','admin','2019-08-19 07:14:24',NULL,NULL),
	(77,'Perusahaan','Perusahaan','admin','2019-08-19 07:14:33',NULL,NULL),
	(78,'Organisasi','Organisasi','admin','2019-08-20 07:02:57',NULL,NULL),
	(79,'DI','Daerah Irigasi','admin','2019-08-20 07:03:07',NULL,NULL),
	(80,'Peserta','Peserta','admin','2019-08-20 11:05:19',NULL,NULL),
	(81,'kg','kg','admin','2019-08-21 03:43:21',NULL,NULL),
	(82,'kg/kapita/tahun','kg/kapita/tahun','admin','2019-08-21 03:46:01',NULL,NULL),
	(83,'Produk','Produk','admin','2019-08-21 04:14:14',NULL,NULL),
	(84,'Boot/Stand','Boot/Stand','admin','2019-08-21 04:15:10',NULL,NULL),
	(85,'Tepat Waktu','Tepat Waktu','admin','2019-08-21 08:07:00',NULL,NULL),
	(86,'SP2D','SP2D','admin','2019-08-21 08:07:22',NULL,NULL),
	(87,'Temuan','Temuan','admin','2019-08-21 08:07:29',NULL,NULL),
	(88,'Rp','Rupiah','admin','2019-08-21 08:07:55',NULL,NULL),
	(89,'RTP','RTP','admin','2019-08-21 09:11:11',NULL,NULL),
	(90,'Pedagang','Pedagang','admin','2019-08-21 09:29:02',NULL,NULL),
	(91,'Faskes','Fasilitas Kesehatan','admin','2019-08-21 09:36:01',NULL,NULL),
	(92,'PPKBD dan Sub PPKKBD','PPKBD dan Sub PPKKBD','admin','2019-08-21 09:55:14',NULL,NULL),
	(93,'Bangunan','Bangunan','admin','2019-08-21 10:00:22',NULL,NULL),
	(94,'Kepala Keluarga','Kepala Keluarga (KK)','admin','2019-08-21 10:04:27',NULL,NULL),
	(95,'Skor','Skor','admin','2019-08-21 11:20:46',NULL,NULL),
	(96,'Angka','Angka','admin','2019-08-21 11:20:57',NULL,NULL),
	(97,'Kampung KB','Kampung KB','admin','2019-08-21 11:32:27',NULL,NULL),
	(98,'Informasi','Informasi','admin','2019-08-22 02:41:47',NULL,NULL),
	(99,'Meter Persegi','Meter Persegi','admin','2019-08-22 05:20:17',NULL,NULL),
	(100,'KWT','KWT','admin','2019-08-22 07:56:32',NULL,NULL),
	(101,'Poktan','Poktan','admin','2019-08-22 08:13:52',NULL,NULL),
	(102,'Predikat','Predikat','admin','2019-08-22 09:09:04',NULL,NULL),
	(103,'Kasus','Kasus','admin','2019-08-27 09:29:28',NULL,NULL),
	(104,'Responden','Responden','admin','2019-08-27 10:18:45',NULL,NULL),
	(105,'Sertifikat','Sertifikat','admin','2019-08-27 11:36:15',NULL,NULL),
	(106,'Pelaku usaha dan atau kegiatan','Pelaku usaha dan atau kegiatan','admin','2019-08-27 12:52:54',NULL,NULL),
	(107,'Judul','Judul','admin','2019-08-28 00:39:02',NULL,NULL),
	(108,'Peraturan','Peraturan','admin','2019-08-28 01:09:45',NULL,NULL),
	(109,'Titik','Titik','admin','2019-08-28 04:53:05',NULL,NULL),
	(110,'Puskesmas','Puskesmas','admin','2019-08-28 04:56:10',NULL,NULL),
	(111,'Titik','Titik','admin','2019-08-28 05:27:58',NULL,NULL),
	(112,'Media Informasi','Media Informasi','admin','2019-08-28 07:43:41',NULL,NULL),
	(113,'Wajib Pajak','Wajib Pajak','admin','2019-08-28 09:50:29',NULL,NULL),
	(114,'SPPT','SPPT','admin','2019-08-28 09:51:15',NULL,NULL),
	(115,'Balai Penyuluhan','Balai Penyuluhan','admin','2019-08-28 12:08:03',NULL,NULL),
	(116,'Rumah','Rumah','admin','2019-08-29 04:37:30',NULL,NULL),
	(117,'Sampel','Sampel','admin','2019-08-30 03:05:53',NULL,NULL),
	(118,'Leaflet/baliho','Leaflet/baliho','admin','2019-08-30 03:06:12',NULL,NULL),
	(119,'Pemotong','Pemotong','admin','2019-08-30 03:24:07',NULL,NULL),
	(120,'Dosis','Dosis','admin','2019-08-30 04:25:40',NULL,NULL),
	(121,'Tahun','Tahun','admin','2020-05-26 14:23:20',NULL,NULL);

/*!40000 ALTER TABLE `sakip_msatuan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_msubbid
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_msubbid`;

CREATE TABLE `sakip_msubbid` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Nm_Subbid` varchar(200) DEFAULT NULL,
  `Nm_Kasubbid` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  KEY `Uniq` (`Uniq`),
  CONSTRAINT `FK_BID4` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_msubbid_indikator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_msubbid_indikator`;

CREATE TABLE `sakip_msubbid_indikator` (
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_SasaranSubbidang` bigint(10) NOT NULL,
  `Kd_IndikatorSubbidang` bigint(10) NOT NULL,
  `Nm_IndikatorSubbidang` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_SasaranProgramOPD`,`Kd_IndikatorProgramOPD`,`Kd_Subbid`,`Kd_SasaranSubbidang`,`Kd_IndikatorSubbidang`),
  KEY `FK_SUBBID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FK_SUBBID5` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_SUBBID_SASARAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_SasaranProgramOPD`, `Kd_IndikatorProgramOPD`, `Kd_Subbid`, `Kd_SasaranSubbidang`) REFERENCES `sakip_msubbid_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_SasaranProgramOPD`, `Kd_IndikatorProgramOPD`, `Kd_Subbid`, `Kd_SasaranSubbidang`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_msubbid_kegiatan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_msubbid_kegiatan`;

CREATE TABLE `sakip_msubbid_kegiatan` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) DEFAULT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Nm_KegiatanOPD` varchar(200) DEFAULT NULL,
  `Nm_SasaranKegiatanOPD` varchar(200) DEFAULT NULL,
  `Nm_IndikatorKegiatanOPD` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Kd_SumberDana` varchar(200) DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_N1` double DEFAULT NULL,
  `Akhir` double DEFAULT NULL,
  `Total` double DEFAULT '0',
  `Total_N1` double DEFAULT NULL,
  `IsEplan` tinyint(1) NOT NULL DEFAULT '1',
  `Remarks` varchar(200) DEFAULT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_SUBBID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FK_Kegiatan_SasaranProg` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) REFERENCES `sakip_mbid_program_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) ON UPDATE CASCADE,
  CONSTRAINT `FK_SUBBID` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_msubbid_kegiatan_grup
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_msubbid_kegiatan_grup`;

CREATE TABLE `sakip_msubbid_kegiatan_grup` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Nm_GrupKegiatanOPD` varchar(200) NOT NULL DEFAULT '',
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_SUBBID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_msubbid_kegiatan_indikator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_msubbid_kegiatan_indikator`;

CREATE TABLE `sakip_msubbid_kegiatan_indikator` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Kd_SasaranKegiatanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorKegiatanOPD` bigint(10) NOT NULL,
  `Nm_IndikatorKegiatanOPD` varchar(200) DEFAULT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  `Nm_PenanggungJawab` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Akhir` double DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`,`Kd_SasaranKegiatanOPD`,`Kd_IndikatorKegiatanOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_SUBBID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FK_IndikatorSasaranKeg` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Kd_SasaranKegiatanOPD`) REFERENCES `sakip_msubbid_kegiatan_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Kd_SasaranKegiatanOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_msubbid_kegiatan_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_msubbid_kegiatan_sasaran`;

CREATE TABLE `sakip_msubbid_kegiatan_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Kd_SasaranKegiatanOPD` bigint(10) NOT NULL,
  `Nm_SasaranKegiatanOPD` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Akhir` double DEFAULT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`,`Kd_SasaranKegiatanOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_SUBBID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FK_SasaranKeg` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`) REFERENCES `sakip_msubbid_kegiatan` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SasaranSubbid` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_msubbid_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_msubbid_sasaran`;

CREATE TABLE `sakip_msubbid_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_SasaranSubbidang` bigint(10) NOT NULL,
  `Nm_SasaranSubbidang` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_SasaranProgramOPD`,`Kd_IndikatorProgramOPD`,`Kd_Subbid`,`Kd_SasaranSubbidang`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_SUBBID2` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FK_BID_INDIKATOR` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_SasaranProgramOPD`, `Kd_IndikatorProgramOPD`) REFERENCES `sakip_mbid_indikator` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_SasaranProgramOPD`, `Kd_IndikatorProgramOPD`) ON UPDATE CASCADE,
  CONSTRAINT `FK_SUBBID4` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_msumberdana
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_msumberdana`;

CREATE TABLE `sakip_msumberdana` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_SumberDana` varchar(50) NOT NULL,
  `Nm_SumberDana` varchar(50) NOT NULL,
  `Create_By` varchar(200) NOT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sakip_msumberdana` WRITE;
/*!40000 ALTER TABLE `sakip_msumberdana` DISABLE KEYS */;

INSERT INTO `sakip_msumberdana` (`Uniq`, `Kd_SumberDana`, `Nm_SumberDana`, `Create_By`, `Create_Date`, `Edit_By`, `Edit_Date`)
VALUES
	(1,'DAU','DAU','admin','2019-03-14 19:00:54',NULL,NULL),
	(2,'DAK','DAK','admin','2019-03-14 19:01:09','admin','2019-03-14 19:03:30'),
	(3,'PAD','PAD','admin','2019-03-14 19:01:35',NULL,NULL),
	(4,'DAU & DAK','DAU & DAK','admin','2019-08-28 05:28:44',NULL,NULL);

/*!40000 ALTER TABLE `sakip_msumberdana` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sakip_pdpa_kegiatan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_pdpa_kegiatan`;

CREATE TABLE `sakip_pdpa_kegiatan` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Nm_KegiatanOPD` varchar(200) NOT NULL,
  `Is_Renja` tinyint(1) NOT NULL,
  `Total` double NOT NULL,
  `Budget` double DEFAULT NULL,
  `Budget_TW1` double DEFAULT NULL,
  `Budget_TW2` double DEFAULT NULL,
  `Budget_TW3` double DEFAULT NULL,
  `Budget_TW4` double DEFAULT NULL,
  `Anggaran_TW1` double DEFAULT NULL,
  `Anggaran_TW2` double DEFAULT NULL,
  `Anggaran_TW3` double DEFAULT NULL,
  `Anggaran_TW4` double DEFAULT NULL,
  `Kd_SumberDana` varchar(200) NOT NULL,
  `Create_By` varchar(200) NOT NULL,
  `Create_Date` datetime NOT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`),
  KEY `FKP_Subbid2` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FKP_DPA_SasaranProgram2` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) REFERENCES `sakip_pdpa_program_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FKP_Subbid2` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_pdpa_kegiatan_grup
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_pdpa_kegiatan_grup`;

CREATE TABLE `sakip_pdpa_kegiatan_grup` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Nm_GrupKegiatanOPD` varchar(200) NOT NULL DEFAULT '',
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_SUBBID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_pdpa_kegiatan_indikator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_pdpa_kegiatan_indikator`;

CREATE TABLE `sakip_pdpa_kegiatan_indikator` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Kd_SasaranKegiatanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorKegiatanOPD` bigint(10) NOT NULL,
  `Nm_IndikatorKegiatanOPD` varchar(200) NOT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  `Nm_PenanggungJawab` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_TW1` double DEFAULT NULL,
  `Target_TW2` double DEFAULT NULL,
  `Target_TW3` double DEFAULT NULL,
  `Target_TW4` double DEFAULT NULL,
  `Kinerja_TW1` double DEFAULT NULL,
  `Kinerja_TW2` double DEFAULT NULL,
  `Kinerja_TW3` double DEFAULT NULL,
  `Kinerja_TW4` double DEFAULT NULL,
  `Anggaran_TW1` double DEFAULT NULL,
  `Anggaran_TW2` double DEFAULT NULL,
  `Anggaran_TW3` double DEFAULT NULL,
  `Anggaran_TW4` double DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`,`Kd_SasaranKegiatanOPD`,`Kd_IndikatorKegiatanOPD`),
  CONSTRAINT `FKP_DPA_SasaranKeg` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Kd_SasaranKegiatanOPD`) REFERENCES `sakip_pdpa_kegiatan_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Kd_SasaranKegiatanOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_pdpa_kegiatan_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_pdpa_kegiatan_sasaran`;

CREATE TABLE `sakip_pdpa_kegiatan_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Kd_SasaranKegiatanOPD` bigint(10) NOT NULL,
  `Nm_SasaranKegiatanOPD` varchar(200) NOT NULL,
  `Kd_Satuan` varchar(50) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_TW1` double DEFAULT NULL,
  `Target_TW2` double DEFAULT NULL,
  `Target_TW3` double DEFAULT NULL,
  `Target_TW4` double DEFAULT NULL,
  `Budget` double DEFAULT NULL,
  `Budget_TW1` double DEFAULT NULL,
  `Budget_TW2` double DEFAULT NULL,
  `Budget_TW3` double DEFAULT NULL,
  `Budget_TW4` double DEFAULT NULL,
  `Kinerja_TW1` double DEFAULT NULL,
  `Kinerja_TW2` double DEFAULT NULL,
  `Kinerja_TW3` double DEFAULT NULL,
  `Kinerja_TW4` double DEFAULT NULL,
  `Anggaran_TW1` double DEFAULT NULL,
  `Anggaran_TW2` double DEFAULT NULL,
  `Anggaran_TW3` double DEFAULT NULL,
  `Anggaran_TW4` double DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`,`Kd_SasaranKegiatanOPD`),
  KEY `FKP_DPA_Subbid` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FKP_DPA_Kegiatan` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`) REFERENCES `sakip_pdpa_kegiatan` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FKP_DPA_Subbid` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_pdpa_program
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_pdpa_program`;

CREATE TABLE `sakip_pdpa_program` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Nm_ProgramOPD` varchar(200) NOT NULL,
  `Is_Renja` tinyint(1) NOT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`),
  KEY `FKP_DPA_Bid` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `FKP_DPA_Bid` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_pdpa_program_indikator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_pdpa_program_indikator`;

CREATE TABLE `sakip_pdpa_program_indikator` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) NOT NULL,
  `Nm_IndikatorProgramOPD` varchar(200) NOT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  `Nm_PenanggungJawab` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_TW1` double DEFAULT NULL,
  `Target_TW2` double DEFAULT NULL,
  `Target_TW3` double DEFAULT NULL,
  `Target_TW4` double DEFAULT NULL,
  `Kinerja_TW1` double DEFAULT NULL,
  `Kinerja_TW2` double DEFAULT NULL,
  `Kinerja_TW3` double DEFAULT NULL,
  `Kinerja_TW4` double DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_IndikatorProgramOPD`),
  CONSTRAINT `FKP_DPA_SasaranProgram` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) REFERENCES `sakip_pdpa_program_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_pdpa_program_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_pdpa_program_sasaran`;

CREATE TABLE `sakip_pdpa_program_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Nm_SasaranProgramOPD` varchar(200) NOT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_TW1` double DEFAULT NULL,
  `Target_TW2` double DEFAULT NULL,
  `Target_TW3` double DEFAULT NULL,
  `Target_TW4` double DEFAULT NULL,
  `Kinerja_TW1` double DEFAULT NULL,
  `Kinerja_TW2` double DEFAULT NULL,
  `Kinerja_TW3` double DEFAULT NULL,
  `Kinerja_TW4` double DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`),
  KEY `FKP_DPA_Bid2` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `DKP_DPA_Program2` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`) REFERENCES `sakip_pdpa_program` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FKP_DPA_Bid2` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_prenja_kegiatan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_prenja_kegiatan`;

CREATE TABLE `sakip_prenja_kegiatan` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) DEFAULT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Nm_KegiatanOPD` varchar(200) DEFAULT NULL,
  `Nm_SasaranKegiatanOPD` varchar(200) DEFAULT NULL,
  `Nm_IndikatorKegiatanOPD` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Kd_SumberDana` varchar(200) DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_N1` double DEFAULT NULL,
  `Akhir` double DEFAULT NULL,
  `Total` double DEFAULT '0',
  `Total_N1` double DEFAULT NULL,
  `IsEplan` tinyint(1) NOT NULL DEFAULT '1',
  `Remarks` varchar(200) DEFAULT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FKP_SUBBID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FKP_Kegiatan_SasaranProg` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) REFERENCES `sakip_prenja_program_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) ON UPDATE CASCADE,
  CONSTRAINT `FKP_SUBBID` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_prenja_kegiatan_grup
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_prenja_kegiatan_grup`;

CREATE TABLE `sakip_prenja_kegiatan_grup` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Nm_GrupKegiatanOPD` varchar(200) NOT NULL DEFAULT '',
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_SUBBID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_prenja_kegiatan_indikator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_prenja_kegiatan_indikator`;

CREATE TABLE `sakip_prenja_kegiatan_indikator` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Kd_SasaranKegiatanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorKegiatanOPD` bigint(10) NOT NULL,
  `Nm_IndikatorKegiatanOPD` varchar(200) DEFAULT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  `Nm_PenanggungJawab` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Akhir` double DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`,`Kd_SasaranKegiatanOPD`,`Kd_IndikatorKegiatanOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FKP_SUBBID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FKP_IndikatorSasaranKeg` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Kd_SasaranKegiatanOPD`) REFERENCES `sakip_prenja_kegiatan_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Kd_SasaranKegiatanOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_prenja_kegiatan_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_prenja_kegiatan_sasaran`;

CREATE TABLE `sakip_prenja_kegiatan_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Kd_SasaranKegiatanOPD` bigint(10) NOT NULL,
  `Nm_SasaranKegiatanOPD` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Akhir` double DEFAULT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`,`Kd_SasaranKegiatanOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FKP_SUBBID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FKP_SasaranKeg` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`) REFERENCES `sakip_prenja_kegiatan` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FKP_SasaranSubbid` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_prenja_program
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_prenja_program`;

CREATE TABLE `sakip_prenja_program` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Nm_ProgramOPD` varchar(200) DEFAULT NULL,
  `Nm_SasaranProgram` varchar(200) DEFAULT NULL,
  `Nm_IndikatorProgram` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_N1` double DEFAULT NULL,
  `IsEplan` tinyint(1) NOT NULL DEFAULT '1',
  `Remarks` varchar(200) DEFAULT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FKP_OPD_IKSASARAN` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`),
  KEY `FKP_BID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `FKP_BID` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON UPDATE CASCADE,
  CONSTRAINT `FKP_OPD_IKSASARAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`) REFERENCES `sakip_mopd_iksasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_prenja_program_indikator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_prenja_program_indikator`;

CREATE TABLE `sakip_prenja_program_indikator` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) NOT NULL,
  `Nm_IndikatorProgramOPD` varchar(200) DEFAULT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  `Nm_PenanggungJawab` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Akhir` double DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_IndikatorProgramOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FKP_Bid6` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `FKP_Bid6` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FKP_IndikatorSasaran` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) REFERENCES `sakip_prenja_program_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table sakip_prenja_program_sasaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sakip_prenja_program_sasaran`;

CREATE TABLE `sakip_prenja_program_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Nm_SasaranProgramOPD` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Akhir` double DEFAULT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FKP_OPD_IKSASARAN` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`),
  KEY `FKP_BID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `FKP_SasaranBid` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FKP_SasaranProgram` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`) REFERENCES `sakip_prenja_program` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table ta_kegiatan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ta_kegiatan`;

CREATE TABLE `ta_kegiatan` (
  `Tahun` year(4) NOT NULL,
  `Kd_Urusan` tinyint(4) NOT NULL,
  `Kd_Bidang` tinyint(4) NOT NULL,
  `Kd_Prog` int(11) NOT NULL,
  `Kd_Keg` int(11) NOT NULL,
  `Kd_Unit` tinyint(4) NOT NULL,
  `Kd_Sub` smallint(6) NOT NULL,
  `ID_Prog` smallint(6) DEFAULT NULL,
  `Ket_Kegiatan` varchar(255) DEFAULT NULL,
  `Lokasi` varchar(800) DEFAULT NULL,
  `Kelompok_Sasaran` varchar(255) DEFAULT NULL,
  `Status_Kegiatan` varchar(1) NOT NULL COMMENT '1. baru, 2 lanjutan',
  `Pagu_Anggaran` double DEFAULT NULL,
  `Waktu_Pelaksanaan` varchar(100) DEFAULT NULL,
  `Kd_Sumber` tinyint(4) DEFAULT NULL,
  `Status` int(1) NOT NULL,
  `Keterangan` text NOT NULL,
  `Pagu_Anggaran_Nt1` double DEFAULT NULL,
  `Verifikasi_Bappeda` tinyint(4) DEFAULT NULL,
  `Tanggal_Verifikasi_Bappeda` int(11) DEFAULT NULL,
  `Keterangan_Verifikasi_Bappeda` mediumtext,
  `Kd_Ref` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Prog`,`Kd_Keg`) USING BTREE,
  KEY `Ta_Kegiatan` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Prog`,`Kd_Keg`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table ta_program
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ta_program`;

CREATE TABLE `ta_program` (
  `Tahun` year(4) NOT NULL,
  `Kd_Urusan` tinyint(4) NOT NULL COMMENT 'untuk filter / kode skpd',
  `Kd_Bidang` tinyint(4) NOT NULL COMMENT 'untuk filter / koda skpd',
  `Kd_Unit` tinyint(4) NOT NULL,
  `Kd_Sub` smallint(6) NOT NULL,
  `Kd_Prog` int(11) NOT NULL,
  `ID_Prog` smallint(6) DEFAULT NULL,
  `Ket_Prog` varchar(255) NOT NULL,
  `Tolak_Ukur` varchar(255) DEFAULT NULL,
  `Target_Angka` double DEFAULT NULL,
  `Target_Uraian` varchar(255) DEFAULT NULL,
  `Kd_Urusan1` tinyint(4) DEFAULT NULL COMMENT 'untuk filter per program',
  `Kd_Bidang1` tinyint(4) DEFAULT NULL COMMENT 'untuk filter per program',
  PRIMARY KEY (`Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Prog`),
  KEY `FK_Ta_Program` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Prog`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
