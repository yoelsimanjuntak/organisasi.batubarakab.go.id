<?php

$rpemda = $this->db
->where(COL_PMDISAKTIF, 1)
->get(TBL_SAKIPV2_PEMDA)
->row_array();

$rOptSkpd = $this->db
->where(COL_SKPDISAKTIF, 1)
->order_by(COL_SKPDURUSAN, 'asc')
->order_by(COL_SKPDBIDANG, 'asc')
->order_by(COL_SKPDUNIT, 'asc')
->order_by(COL_SKPDSUBUNIT, 'asc')
->get(TBL_SAKIPV2_SKPD)
->result_array();

$getSkpd = '';
$getRenstra = '';
$getDPA = '';
$isDPAPerubahan = '';

if(!empty($_GET['idSKPD'])) $getSkpd = $_GET['idSKPD'];
else if(!empty($rOptSkpd)) $getSkpd = $rOptSkpd[0][COL_SKPDID];

$rOptRenstra = array();
if(!empty($getSkpd)) {
  $rOptRenstra = $this->db
  ->where(COL_IDPEMDA, $rpemda[COL_PMDID])
  ->where(COL_IDSKPD, $getSkpd)
  //->where(COL_RENSTRAISAKTIF, 1)
  ->order_by(COL_RENSTRAISAKTIF, 'desc')
  ->order_by(COL_RENSTRATAHUN, 'desc')
  ->order_by(COL_RENSTRAID, 'desc')
  ->get(TBL_SAKIPV2_SKPD_RENSTRA)
  ->result_array();
}

if(!empty($_GET['idRenstra'])) $getRenstra = $_GET['idRenstra'];
else if(!empty($rOptRenstra)) $getRenstra = $rOptRenstra[0][COL_RENSTRAID];

$getDPA = null;
if(!empty($_GET['idDPA'])) {
  $getDPA = $_GET['idDPA'];
} else if(!empty($rOptDpa) && $rOptDpa[0][COL_DPAISAKTIF]==1) {
  $getDPA = $rOptDpa[0][COL_DPAID];
}

$rRenstra = array();
$rSasaran = array();
if(!empty($getSkpd) && !empty($getRenstra)) {
  $rRenstra = $this->db
  ->where(COL_RENSTRAID, $getRenstra)
  ->get(TBL_SAKIPV2_SKPD_RENSTRA)
  ->row_array();

  $rSasaran = $this->db
  ->join(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN,TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.".".COL_IDTUJUAN,"left")
  ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.".".COL_IDRENSTRA,"left")
  ->where(COL_IDSKPD, $getSkpd)
  ->where(COL_IDRENSTRA, $getRenstra)
  ->order_by(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANNO)
  ->order_by(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.'.'.COL_SASARANNO)
  ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN)
  ->result_array();
}

$rOptDpa = array();
if(!empty($getRenstra)) {
  $rOptDpa = $this->db
  ->where(COL_IDRENSTRA, $getRenstra)
  ->where(COL_DPAISAKTIF, 1)
  ->order_by(COL_DPAISAKTIF, 'desc')
  ->order_by(COL_DPATAHUN,'desc')
  ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
  ->result_array();
}

$getDPA = null;
$rDpa = array();
if(!empty($_GET['idDPA'])) {
  $getDPA = $_GET['idDPA'];
} else if(!empty($rOptDpa) && $rOptDpa[0][COL_DPAISAKTIF]==1) {
  $getDPA = $rOptDpa[0][COL_DPAID];
}

if(!empty($getDPA)) {
  $rDpa = $this->db
  ->where(COL_DPAID, $getDPA)
  ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
  ->row_array();
}
?>
<div class="content-header">
</div>
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header p-0">
            <table class="table mb-0">
              <thead>
                <tr>
                  <td>
                    <div class="form-group row mb-0">
                      <label class="control-label col-lg-3">PERIODE PEMERINTAHAN :</label>
                      <div class="col-lg-8">
                        <p class="font-italic font-weight-bold mb-0" style="line-height: 2; text-decoration: underline">
                          <?=$rpemda[COL_PMDTAHUNMULAI].' - '.$rpemda[COL_PMDTAHUNAKHIR].' '.strtoupper($rpemda[COL_PMDPEJABAT]).' & '.strtoupper($rpemda[COL_PMDPEJABATWAKIL])?>
                        </p>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="control-label col-lg-3">SKPD :</label>
                      <div class="col-lg-8">
                        <select class="form-control" name="filterSkpd">
                          <?php
                          foreach($rOptSkpd as $opt) {
                            $isSelected = '';
                            if(!empty($getSkpd) && $opt[COL_SKPDID]==$getSkpd) {
                              $isSelected='selected';
                            }
                            ?>
                            <option value="<?=site_url('site/home/sakip').'?idSKPD='.$opt[COL_SKPDID]?>" <?=$isSelected?>>
                              <?=strtoupper($opt[COL_SKPDNAMA])?>
                            </option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </td>
                </tr>
              </thead>
            </table>
          </div>
          <div class="card-body">
            <h6 class="text-center"><?=!empty($rRenstra)?'<strong>'.$rRenstra[COL_RENSTRAURAIAN].'</strong>':'<span class="font-italic">BELUM ADA DATA</span>'?></h6>
            <table class="table table-bordered text-sm mb-5">
              <tr>
                <th>SASARAN / INDIKATOR</th>
                <th>SATUAN</th>
                <th>TARGET</th>
              </tr>
              <?php
              foreach($rSasaran as $r) {
                $rindikator = $this->db
                ->where(COL_IDSASARAN, $r[COL_SASARANID])
                ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARANDET)
                ->result_array();
                ?>
                <tr>
                  <td colspan="3" class="font-weight-bold"><?=$r[COL_SASARANURAIAN]?></td>
                </tr>
                <?php
                foreach($rindikator as $i) {
                  ?>
                  <tr>
                    <td class="font-italic pl-4"><?=$i[COL_SSRINDIKATORURAIAN]?></td>
                    <td class="font-italic"><?=$i[COL_SSRINDIKATORSATUAN]?></td>
                    <td class="font-italic text-center"><?=$i[COL_SSRINDIKATORTARGET]?></td>
                  </tr>
                  <?php
                }
              }
              ?>
            </table>

            <?php
            if(!empty($rDpa)) {
              ?>
              <h6 class="text-center"><strong><?=$rDpa[COL_DPAURAIAN]?></strong></h6>
              <?php
              $rprogram = $this->db
              ->select('*')
              ->where(COL_IDDPA, $getDPA)
              ->order_by(COL_PROGRAMKODE)
              ->get(TBL_SAKIPV2_BID_PROGRAM)
              ->result_array();
              ?>
              <table class="table table-bordered text-sm">
                <tr>
                  <th>PROGRAM / KEGIATAN</th>
                  <th>INDIKATOR</th>
                  <th>SATUAN</th>
                  <th>TARGET</th>
                </tr>
                <?php
                foreach($rprogram as $r) {
                  $rsasaran = $this->db
                  ->where(COL_IDPROGRAM, $r[COL_PROGRAMID])
                  ->get(TBL_SAKIPV2_BID_PROGSASARAN)
                  ->row_array();

                  $rkegiatan = $this->db
                  ->where(COL_IDPROGRAM, $r[COL_PROGRAMID])
                  ->get(TBL_SAKIPV2_BID_KEGIATAN)
                  ->result_array();
                  ?>
                  <tr>
                    <td class="font-weight-bold"><?=$r[COL_PROGRAMURAIAN]?></td>
                    <td class="font-weight-bold text-justify"><?=!empty($rsasaran)?$rsasaran[COL_SASARANINDIKATOR]:'-'?></td>
                    <td class="font-weight-bold"><?=!empty($rsasaran)?$rsasaran[COL_SASARANSATUAN]:'-'?></td>
                    <td class="font-weight-bold text-center"><?=!empty($rsasaran)?$rsasaran[COL_SASARANTARGET]:'-'?></td>
                  </tr>
                  <?php
                  foreach($rkegiatan as $k) {
                    $rsasarankeg = $this->db
                    ->where(COL_IDKEGIATAN, $k[COL_KEGIATANID])
                    ->get(TBL_SAKIPV2_BID_KEGSASARAN)
                    ->row_array();
                    ?>
                    <tr>
                      <td class="pl-4 font-italic"><?=$k[COL_KEGIATANURAIAN]?></td>
                      <td class="pl-4 font-italic text-justify"><?=!empty($rsasarankeg)?$rsasarankeg[COL_SASARANINDIKATOR]:'-'?></td>
                      <td class="font-italic"><?=!empty($rsasarankeg)?$rsasarankeg[COL_SASARANSATUAN]:'-'?></td>
                      <td class="font-italic text-center"><?=!empty($rsasarankeg)?$rsasarankeg[COL_SASARANTARGET]:'-'?></td>
                    </tr>
                    <?php
                  }
                }
                ?>
              </table>
              <?php
            }
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('select[name=filterRenstra],select[name=filterSkpd],select[name=filterDPA],input[name=isDPAPerubahan]').change(function(){
    var url = $(this).val();
    location.href = url;
  });
});
</script>
