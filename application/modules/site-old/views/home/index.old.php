<style>
.todo-list>li:hover {
    background-color: #ccc;
}
.carousel-container {
  min-height: 300px;
}

.badge-misi {
  font-size: 11pt !important;
  font-weight: 400;
  position: absolute;
  left: 50% !important;
  top: -8px;
  margin: 0 !important;
}

#section-data .card-inner {
  transition: transform .2s;
}
#section-data .card-inner:hover {
  transform: scale(1.1);
  z-index: 1;
}
.timeline::before {
  display: none;
}
#section-data .content-header-bg::after {
  background: url(<?=MY_IMAGEURL.'footer-map-bg.png'?>) no-repeat scroll left top / 100% auto;
  content: "";
  height: 100%;
  left: 0;
  opacity: 0.1;
  position: absolute;
  top: 0;
  width: 100%;
  z-index: 1;
}
#tbl-data-kec th {
  vertical-align: middle;
  text-align: center;
}
</style>
<?php
$carouselDir =  scandir(MY_IMAGEPATH.'slide/');
$carouselArr = array();
foreach (scandir(MY_IMAGEPATH.'slide/') as $file) {
  if(strpos(mime_content_type(MY_IMAGEPATH.'slide/'.$file), 'image') !== false) {
    $carouselArr[MY_IMAGEURL.'slide/'.$file] = filemtime(MY_IMAGEPATH.'slide/'. $file); //$carouselArr[] = MY_IMAGEURL.'slide/'.$file;
  }
}
arsort($carouselArr);
$carouselArr = array_keys($carouselArr);

$carousel2Dir =  scandir(MY_IMAGEPATH.'slide2/');
$carousel2Arr = array();
foreach (scandir(MY_IMAGEPATH.'slide2/') as $file) {
  if(strpos(mime_content_type(MY_IMAGEPATH.'slide2/'.$file), 'image') !== false) {
    $carousel2Arr[MY_IMAGEURL.'slide2/'.$file] = filemtime(MY_IMAGEPATH.'slide2/'. $file); //$carouselArr[] = MY_IMAGEURL.'slide/'.$file;
  }
}
arsort($carousel2Arr);
$carousel2Arr = array_keys($carousel2Arr);

?>
<div class="content-wrapper mt-0">
  <div class="hero-container" style="background-image: url('<?=MY_IMAGEURL.'hero-bg.png'?>') !important; background-size: cover; background-position-y: top">
    <!--<span class="hero-wave"></span>-->
    <div class="hero-content">
      <div class="row">
        <div class="col-sm-4">
          <div class="hero-item text-left">
            <h4 class="font-weight-bold" style="color: #FFFF02 !important"><?=nl2br($this->setting_org_name)?><br /><small class="font-italic text-white"><?=nl2br($this->setting_web_desc)?></small></h4>
          </div>
        </div>
        <div class="col-sm-8">
          <!-- SLIDER -->
          <?php
          if(!empty($carouselArr)) {
            ?>
            <div id="carouselMain" class="carousel slide carousel-fade" data-ride="carousel" style="border: 4px solid #FFFF02; border-radius: .25rem; box-shadow: 0 0 5px #00000020, 0 3px 5px rgba(0,0,0,.2);">
              <div class="carousel-inner">
                <?php
                for($i=0; $i<count($carouselArr); $i++) {
                  ?>
                  <div class="carousel-item <?=$i==0?'active':''?>">
                    <!--<img class="d-block" src="<?=$carouselArr[$i]?>" style="height: 200px">-->
                    <div class="d-block w-100" style="background: url('<?=$carouselArr[$i]?>'); background-size: cover; background-repeat: no-repeat; height: 300px">
                    </div>
                  </div>
                  <?php
                }
                ?>
              </div>
              <a class="carousel-control-prev" href="#carouselMain" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselMain" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
              <ol class="carousel-indicators" style="transform: translateY(50px);">
                <?php
                for($i=0; $i<count($carouselArr); $i++) {
                  ?>
                  <li data-target="#carouselMain" data-slide-to="<?=$i?>" <?=$i==0?'class="active"':''?>></li>
                  <?php
                }
                ?>
              </ol>
              <div class="d-block pt-2" style="background: #FFFF02 !important;">
                <marquee class="font-weight-bold text-dark">SELAMAT DATANG DI <?=$this->setting_web_name?> (<?=strtoupper($this->setting_web_desc)?>)</marquee>
              </div>
            </div>
            <?php
          }
          ?>
          <!-- SLIDER -->
        </div>
      </div>
    </div>
  </div>
  <div class="content pt-3">
    <div class="container">
      <div class="row">
        <div class="col-sm-8">
          <div class="card card-danger d-none">
            <div class="card-header">
              <h5 class="card-title font-weight-bold m-0">VISI & MISI PEMERINTAH</h5>
            </div>
            <div class="card-body p-0">
              <?php
              $this->db->where(COL_KD_TAHUN_FROM." <=", date("Y"));
              $this->db->where(COL_KD_TAHUN_TO." >=", date("Y"));
              $this->db->order_by(COL_KD_TAHUN_FROM, "desc");
              $rperiod = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();
              $this->db->where(COL_KD_TAHUN_FROM." <=", date("Y"));
              $this->db->where(COL_KD_TAHUN_TO." >=", date("Y"));
              $this->db->order_by(COL_KD_TAHUN_FROM, "desc");
              $rperiod = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();
              $rmisi = $this->db
              ->where(COL_KD_PEMDA, !empty($rperiod[COL_KD_PEMDA])?$rperiod[COL_KD_PEMDA]:-999)
              ->order_by(COL_KD_MISI, "asc")
              ->get(TBL_SAKIP_MPMD_MISI)
              ->result_array();
              $rtujuan = $this->db
              ->where(COL_KD_PEMDA, !empty($rperiod[COL_KD_PEMDA])?$rperiod[COL_KD_PEMDA]:-999)
              ->order_by(COL_KD_MISI)
              ->order_by(COL_KD_TUJUAN)
              ->get(TBL_SAKIP_MPMD_TUJUAN)
              ->result_array();
              $riktujuan = $this->db
              ->where(COL_KD_PEMDA, !empty($rperiod[COL_KD_PEMDA])?$rperiod[COL_KD_PEMDA]:-999)
              ->order_by(COL_KD_MISI)
              ->order_by(COL_KD_TUJUAN)
              ->order_by(COL_KD_INDIKATORTUJUAN)
              ->get(TBL_SAKIP_MPMD_IKTUJUAN)
              ->result_array();
              $rsasaran = $this->db
              ->where(COL_KD_PEMDA, !empty($rperiod[COL_KD_PEMDA])?$rperiod[COL_KD_PEMDA]:-999)
              ->order_by(COL_KD_MISI)
              ->order_by(COL_KD_TUJUAN)
              ->order_by(COL_KD_INDIKATORTUJUAN)
              ->order_by(COL_KD_SASARAN)
              ->get(TBL_SAKIP_MPMD_SASARAN)
              ->result_array();
              $riksasaran = $this->db
              ->where(COL_KD_PEMDA, !empty($rperiod[COL_KD_PEMDA])?$rperiod[COL_KD_PEMDA]:-999)
              ->order_by(COL_KD_MISI)
              ->order_by(COL_KD_TUJUAN)
              ->order_by(COL_KD_INDIKATORTUJUAN)
              ->order_by(COL_KD_SASARAN)
              ->order_by(COL_KD_INDIKATORSASARAN)
              ->get(TBL_SAKIP_MPMD_IKSASARAN)
              ->result_array();
              $sumrka = $this->db
              ->select_sum(COL_TOTAL)
              ->where(COL_KD_PEMDA, !empty($rperiod[COL_KD_PEMDA])?$rperiod[COL_KD_PEMDA]:-999)
              ->where(COL_KD_TAHUN, date('Y'))
              ->get(TBL_SAKIP_MSUBBID_KEGIATAN)
              ->row_array();
              $sumdpa = $this->db
              ->select_sum(COL_BUDGET)
              ->where(COL_KD_PEMDA, !empty($rperiod[COL_KD_PEMDA])?$rperiod[COL_KD_PEMDA]:-999)
              ->where(COL_KD_TAHUN, date('Y'))
              ->get(TBL_SAKIP_DPA_KEGIATAN)
              ->row_array();
              ?>
              <div class="row p-2">
                <div class="col-lg-12 mb-2 pt-0 pb-2 pl-3 pr-3" style="border-bottom: 1px solid #dedede">
                  PERIODE PEMERINTAHAN :<br />
                  <b><?=$rperiod?$rperiod[COL_KD_TAHUN_FROM]." s.d ".$rperiod[COL_KD_TAHUN_TO]:"-"?></b>
                </div>
                <div class="col-lg-12 mb-2 pt-0 pb-2 pl-3 pr-3" style="border-bottom: 1px solid #dedede">
                  KEPALA DAERAH :<br />
                  <b><?=$rperiod?strtoupper($rperiod[COL_NM_PEJABAT]):"-"?></b>
                </div>
                <div class="col-lg-12 pt-0 pb-2 pl-3 pr-3" style="border-bottom: 1px solid #dedede">
                  VISI :<br />
                  <b><?=$rperiod?strtoupper($rperiod[COL_NM_VISI]):"-"?></b>
                </div>
              </div>
              <div class="row pt-0 pb-2 pr-2 pl-2">
                <div class="col-3 pt-0 pb-2 pl-3 pr-3" style="border-bottom: 1px solid #dedede">
                  MISI
                </div>
                <div class="col-3 pt-0 pb-2 pl-3 pr-3 text-right" style="border-bottom: 1px solid #dedede">
                  <a href="#" data-toggle="modal" data-target="#modal-misi">
                    <span class="badge bg-default" style="background: #FFFF02; color: #000"><?=number_format(count($rmisi))?></span>
                  </a>
                </div>
                <div class="col-6 pt-0 pb-2 pl-3 pr-3" style="border-bottom: 1px solid #dedede"></div>
              </div>
              <div class="row pt-0 pb-2 pr-2 pl-2">
                <div class="col-3 pt-0 pb-2 pl-3 pr-3" style="border-bottom: 1px solid #dedede">
                  TUJUAN
                </div>
                <div class="col-3 pt-0 pb-2 pl-3 pr-3 text-right" style="border-bottom: 1px solid #dedede">
                  <a href="#" data-toggle="modal" data-target="#modal-tujuan">
                    <span class="badge bg-default" style="background: #FFFF02; color: #000"><?=number_format(count($rtujuan))?></span>
                  </a>
                </div>
                <div class="col-6 pt-0 pb-2 pl-3 pr-3" style="border-bottom: 1px solid #dedede">
                  INDIKATOR TUJUAN
                  <a href="#" class="pull-right" data-toggle="modal" data-target="#modal-tujuan">
                    <span class="badge bg-default" style="background: #FFFF02; color: #000"><?=number_format(count($riktujuan))?></span>
                  </a>
                </div>
              </div>
              <div class="row pt-0 pb-2 pr-2 pl-2">
                <div class="col-3 pt-0 pb-2 pl-3 pr-3">
                  SASARAN
                </div>
                <div class="col-3 pt-0 pb-2 pl-3 pr-3 text-right">
                  <a href="#" data-toggle="modal" data-target="#modal-sasaran">
                    <span class="badge bg-default" style="background: #FFFF02; color: #000"><?=number_format(count($rsasaran))?></span>
                  </a>
                </div>
                <div class="col-6 pt-0 pb-2 pl-3 pr-3">
                  INDIKATOR SASARAN
                  <a href="#" class="pull-right" data-toggle="modal" data-target="#modal-sasaran">
                    <span class="badge bg-default" style="background: #FFFF02; color: #000"><?=number_format(count($riksasaran))?></span>
                  </a>
                </div>
              </div>
              <!--<div class="row pt-0 pb-2 pr-2 pl-2">
                <div class="col-3 pt-0 pb-2 pl-3 pr-3" style="border-bottom: 1px solid #dedede">
                  TOTAL RKA <?=date('Y')?>
                </div>
                <div class="col-9 pt-0 pb-2 pl-3 pr-3 text-right" style="border-bottom: 1px solid #dedede">
                  <span class="badge bg-success">Rp. <?=number_format($sumrka[COL_TOTAL])?></span>
                </div>
              </div>
              <div class="row pt-0 pb-2 pr-2 pl-2">
                <div class="col-3 pt-0 pb-2 pl-3 pr-3">
                  TOTAL DPA <?=date('Y')?>
                </div>
                <div class="col-9 pt-0 pb-2 pl-3 pr-3 text-right">
                  <span class="badge bg-success text-md">Rp. <?=number_format($sumdpa[COL_BUDGET])?></span>
                </div>
              </div>-->
            </div>
          </div>
          <div class="card card-danger">
            <div class="card-header">
              <h5 class="card-title font-weight-bold m-0">BERITA TERKINI</h5>
            </div>
            <div class="card-body pb-0">
              <?php
              if(!empty($berita)) {
                $n=0;
                foreach($berita as $b) {
                  $n++;
                  $strippedcontent = strip_tags($b[COL_POSTCONTENT]);
                  $img = $this->db->where(COL_POSTID, $b[COL_POSTID])->get(TBL__POSTIMAGES)->row_array();
                  ?>
                  <div class="d-block pb-3 mb-3" <?=$n<count($berita)?'style="border-bottom: 1px solid #dedede"':''?>>
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="img" style="
                        height: 150px;
                        width: 100%;
                        background-image: url('<?=!empty($img)?MY_UPLOADURL.$img[COL_FILENAME]:MY_IMAGEURL.'no-image.png'?>');
                        background-size: cover;
                        background-repeat: no-repeat;
                        background-position: center;
                        border: 4px solid #000;
                        ">
                        </div>
                      </div>
                      <div class="col-sm-8">
                        <a href="<?=site_url('site/home/page/'.$b[COL_POSTSLUG])?>"><h5 class="text-red"><?=$b[COL_POSTTITLE]?></h5></a>
                        <p class="mb-0">
                          <small class="text-muted"><?=date('d-m-Y H:i', strtotime($b[COL_CREATEDON]))?></small>
                          <small class="text-muted float-right">dilihat <strong><?=$b[COL_TOTALVIEW]?></strong> kali</small>
                        </p>
                        <p class="mb-2">
                          <?=strlen($strippedcontent) > 200 ? substr($strippedcontent, 0, 200) . "..." : $strippedcontent ?>
                        </p>
                      </div>
                    </div>
                  </div>
                  <?php
                }
                ?>
                <?php
              } else {
                echo '<p>Belum ada data.</p>';
              }
              ?>
            </div>
            <div class="card-footer">
              <div class="row">
                <div class="col-sm-12">
                  <a href="<?=site_url('site/home/post/1')?>" class="btn btn-block btn-danger font-weight-bold" style="color: #FFFF02">LIHAT SEMUA</a>
                </div>
              </div>
            </div>
          </div>
          <div class="card card-danger">
            <div class="card-header">
              <h5 class="card-title font-weight-bold m-0">PELAYANAN PUBLIK</h5>
            </div>
            <div class="card-body p-0">
              <?php
              if(!empty($carousel2Arr)) {
                ?>
                <div id="carouselSecondary" class="carousel slide carousel-fade" data-ride="carousel">
                  <div class="carousel-inner">
                    <?php
                    for($i=0; $i<count($carousel2Arr); $i++) {
                      ?>
                      <div class="carousel-item <?=$i==0?'active':''?>">
                        <!--<img class="d-block" src="<?=$carousel2Arr[$i]?>" style="height: 200px">-->
                        <div class="d-block w-100" style="background: url('<?=$carousel2Arr[$i]?>'); background-size: cover; background-repeat: no-repeat; height: 800px">
                        </div>
                      </div>
                      <?php
                    }
                    ?>
                  </div>
                  <a class="carousel-control-prev" href="#carouselSecondary" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselSecondary" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                  <ol class="carousel-indicators">
                    <?php
                    for($i=0; $i<count($carousel2Arr); $i++) {
                      ?>
                      <li data-target="#carouselSecondary" data-slide-to="<?=$i?>" <?=$i==0?'class="active"':''?>></li>
                      <?php
                    }
                    ?>
                  </ol>
                </div>
                <?php
              }
              ?>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="card card-danger">
            <div class="card-header">
              <h5 class="card-title font-weight-bold m-0">PORTAL APLIKASI</h5>
            </div>
          </div>
          <div class="small-box bg-danger">
            <div class="inner text-left" style="width: 80% !important; min-height: 100px">
              <h5 class="font-weight-bold" style="color: #FFFF02">E-SAKIP</h5>
              <p class="font-italic">Sistem Akuntabilitas Kinerja Pemerintah Daerah berbasis Online</p>
            </div>
            <div class="icon d-block">
              <i class="fas fa-globe"></i>
            </div>
            <a href="<?=site_url('sakip/user/login')?>" class="small-box-footer font-weight-bold" style="color: #FFFF02 !important">
              MASUK&nbsp;&nbsp;<i class="fas fa-arrow-right"></i>
            </a>
          </div>
          <div class="small-box bg-danger">
            <div class="inner text-left" style="width: 80% !important; min-height: 100px">
              <h5 class="font-weight-bold" style="color: #FFFF02">SI SUKMA</h5>
              <p class="font-italic">Sistem Informasi Survey Kepuasan Masyarakat</p>
            </div>
            <div class="icon d-block">
              <i class="fas fa-box-ballot"></i>
            </div>
            <a href="<?=site_url().'skm'?>" class="small-box-footer font-weight-bold" style="color: #FFFF02 !important">
              MASUK&nbsp;&nbsp;<i class="fas fa-arrow-right"></i>
            </a>
          </div>
          <div class="small-box bg-danger">
            <div class="inner text-left" style="width: 80% !important; min-height: 100px">
              <h5 class="font-weight-bold" style="color: #FFFF02">CMS</h5>
              <p class="font-italic">Content Management System</p>
            </div>
            <div class="icon d-block">
              <i class="fas fa-user-cog"></i>
            </div>
            <a href="<?=site_url('site/user/login')?>" class="small-box-footer font-weight-bold" style="color: #FFFF02 !important">
              MASUK&nbsp;&nbsp;<i class="fas fa-arrow-right"></i>
            </a>
          </div>
          <div class="card">
            <div class="card-body p-0">
              <script type="text/javascript" src="https://widget.kominfo.go.id/gpr-widget-kominfo.min.js"></script>
              <div id="gpr-kominfo-widget-container"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-misi" aria-modal="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">MISI</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body p-0">
        <table class="table table-striped" width="100%">
          <tbody>
            <?php
            foreach ($rmisi as $m) {
              echo '<tr><td>'.$m[COL_KD_MISI].'</td><td>'.$m[COL_NM_MISI].'</td></tr>';
            }
            ?>
            <tr></tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-tujuan" aria-modal="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">TUJUAN</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body p-0">
        <table class="table table-striped" width="100%">
          <tbody>
            <?php
            $n=1;
            foreach ($rtujuan as $t) {
              echo '<tr><td><strong>'.$n.'</strong></td><td colspan="2"><strong>'.$t[COL_NM_TUJUAN].'</strong></td></tr>';
              foreach($riktujuan as $ikt) {
                if($ikt[COL_KD_MISI]==$t[COL_KD_MISI] && $ikt[COL_KD_TUJUAN]==$t[COL_KD_TUJUAN]) {
                  echo '<tr><td></td><td style="width: 100px; white-space: nowrap">INDIKATOR '.$ikt[COL_KD_INDIKATORTUJUAN].'</td><td>'.$ikt[COL_NM_INDIKATORTUJUAN].'</td></tr>';
                }
              }
              $n++;
            }
            ?>
            <tr></tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-sasaran" aria-modal="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">SASARAN</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body p-0">
        <table class="table table-striped" width="100%">
          <tbody>
            <?php
            $n=1;
            foreach ($rsasaran as $t) {
              echo '<tr><td><strong>'.$n.'</strong></td><td colspan="2"><strong>'.$t[COL_NM_SASARAN].'</strong></td></tr>';
              foreach($riksasaran as $ikt) {
                if($ikt[COL_KD_MISI]==$t[COL_KD_MISI] && $ikt[COL_KD_TUJUAN]==$t[COL_KD_TUJUAN] && $ikt[COL_KD_SASARAN]==$t[COL_KD_SASARAN]) {
                  echo '<tr><td></td><td style="width: 100px; white-space: nowrap">INDIKATOR '.$ikt[COL_KD_INDIKATORSASARAN].'</td><td>'.$ikt[COL_NM_INDIKATORSASARAN].'</td></tr>';
                }
              }
              $n++;
            }
            ?>
            <tr></tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/ekko-lightbox/ekko-lightbox.min.js"></script>
<script type="text/javascript">
var lat = 3.326288;
var lon = 99.156685;
function getWilayah() {
    $.getJSON('https://ibnux.github.io/BMKG-importer/cuaca/wilayah.json', function (data) {
        var items = [];
        var jml = data.length;

        //hitung jarak
        for (n = 0; n < jml; n++) {
            data[n].jarak = distance(lat, lon, data[n].lat, data[n].lon, 'K');
        }

        //urutkan berdasarkan jarak
        data.sort(urutkanJarak);

        //setelah dapat jarak,  ambil 5 terdekat
        for (n = 0; n < jml; n++) {
            items.push('<li class="list-group-item d-flex justify-content-between align-items-center">' + data[n].propinsi +
                ', ' + data[n].kota + ', ' + data[n].kecamatan + '<span class="badge badge-primary badge-pill">' + data[n].jarak.toFixed(2) + ' km</span></li>');
            if (n > 4) break
        };
        $('#judulTerdekat').html("Jarak terdekat dari " + lat + "," + lon);
        $('#wilayahTerdekat').html(items.join(""));
        $('#judulCuaca').html(data[0].propinsi +
                ', ' + data[0].kota + ', ' + data[0].kecamatan + ' ' + data[0].jarak.toFixed(2)+" km");
        getCuaca(data[0].id);
    });
}

function getCuaca(idWilayah) {
    $.getJSON('https://ibnux.github.io/BMKG-importer/cuaca/'+idWilayah+'.json', function (data) {
        var items = [];
        data = jQuery.grep(data, function(n) {
          return (moment(n.jamCuaca).format("YYYY-MM-DD") == '<?=date('Y-m-d')?>' && moment(n.jamCuaca).format("HH") != '00');
        });
        var jml = data.length;

        //setelah dapat jarak,  ambil 5 terdekat
        for (n = 0; n < jml; n++) {
          items.push('<div class=\"card bg-light\">'+
          '<div class=\"card-body\">'+
          '<div class=\"row\">'+
          '<div class=\"col-8 text-sm\">'+
                '<h2 class=\"lead\"><b>'+data[n].cuaca+'</b></h2>'+
                  '<div class=\"row\">'+
                  '<div class=\"col-sm-6 text-right\">Waktu :</div>'+
                    '<div class=\"col-sm-6 font-weight-bold\">'+moment(data[n].jamCuaca).format("HH:mm")+'</div>'+
                    '</div>'+
                  '<div class=\"row\">'+
                  '<div class=\"col-sm-6 text-right\">Temperatur :</div>'+
                    '<div class=\"col-sm-6\">'+data[n].tempC+' C, '+data[n].tempF+' F</div>'+
                    '</div>'+
                  '<div class=\"row\">'+
                  '<div class=\"col-sm-6 text-right\">Kelembapan :</div>'+
                    '<div class=\"col-sm-6\">'+data[n].humidity+'</div>'+
                    '</div>'+
                  '</div>'+
                '<div class=\"col-4 text-center\">'+
                '<img src=\"https://ibnux.github.io/BMKG-importer/icon/'+data[n].kodeCuaca+'.png\" alt=\"\" class=\"img-fluid\">'+
                  '</div>'+
                '</div>'+
              '</div>'+
            '</div>');
            if (n > 4) break
        };
        $('#forecast').html(items.join(""));
    });
}

// https://www.htmlgoodies.com/beyond/javascript/calculate-the-distance-between-two-points-in-your-web-apps.html
function distance(lat1, lon1, lat2, lon2) {
    var radlat1 = Math.PI * lat1 / 180
    var radlat2 = Math.PI * lat2 / 180
    var theta = lon1 - lon2
    var radtheta = Math.PI * theta / 180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist)
    dist = dist * 180 / Math.PI
    dist = dist * 60 * 1.1515
    return Math.round((dist * 1.609344) * 1000) / 1000;
}

function urutkanJarak(a, b) {
    if (a['jarak'] === b['jarak']) {
        return 0;
    }
    else {
        return (a['jarak'] < b['jarak']) ? -1 : 1;
    }
}
getWilayah();

$(document).ready(function() {
  /*$('#forecast').load('<?=site_url('site/ajax/get-weather')?>', function(){

  });*/
});
$(document).on('click', '[data-toggle="lightbox"]', function(event) {
  event.preventDefault();
  $(this).ekkoLightbox({
    alwaysShowClose: true
  });
});
(function ($) {
  var navbar = $('.navbar');
  var lastScrollTop = 0;

  $(window).scroll(function () {
      var st = $(this).scrollTop();
      // Scroll down
      if (st > lastScrollTop) {
          //navbar.fadeOut();
          navbar.removeClass('navbar-dark bg-transparent').addClass('navbar-dark bg-header');
      }
      // Scroll up but still lower than 200 (change that to whatever suits your need)
      else if(st < lastScrollTop && st > 200) {
          //navbar.fadeIn();
          navbar.removeClass('navbar-dark bg-transparent').addClass('navbar-dark bg-header');
      }
      // Reached top
      else {
          navbar.removeClass('navbar-dark bg-header').addClass('navbar-dark bg-transparent');
      }
      lastScrollTop = st;
  }).trigger('scroll');
})(jQuery);
</script>
