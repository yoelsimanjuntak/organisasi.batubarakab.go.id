<div class="content-wrapper mt-0">
  <div class="hero-container" style="padding: 60px 20px !important; background-image: url('<?=MY_IMAGEURL.'hero-bg.png'?>') !important; background-size: cover; background-position-y: top">
    <!--<span class="hero-wave"></span>-->
    <div class="hero-content">
      <div class="row">
        <div class="col-sm-12">
          <div class="hero-item text-center" style="transform: translateY(20px) !important">
            <h4 class="font-weight-bold text-white">
              <small class="font-weight-bold" style="color: #ffeb3b"><?=strtoupper($data[COL_POSTCATEGORYNAME])?></small><br />
              <?=$data[COL_POSTTITLE]?>
            </h4>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="content pt-3">
    <div class="container">
      <div class="row">
        <div class="col-sm-8">
          <div class="card card-outline card-danger">
            <div class="card-body">
              <?php
              $files = $this->db->where(COL_POSTID, $data[COL_POSTID])->get(TBL__POSTIMAGES)->result_array();
              if(!empty($files)) {
                if(count($files) > 1) {
                  ?>
                  <div class="row mb-2">
                    <?php
                    foreach($files as $f) {
                      if(strpos(mime_content_type(MY_UPLOADPATH.$f[COL_FILENAME]), 'image') !== false) {
                        ?>
                        <div class="col-12 col-sm-6 col-md-6 d-flex align-items-stretch p-2">
                          <div href="<?=MY_UPLOADURL.$f[COL_FILENAME]?>"
                          data-toggle="lightbox"
                          data-title="<?=$data[COL_POSTTITLE]?>"
                          data-gallery="gallery"
                          style="background: url('<?=MY_UPLOADURL.$f[COL_FILENAME]?>');
                                  background-size: cover;
                                  background-repeat: no-repeat;
                                  background-position: center;
                                  width: 100%;
                                  min-height: 300px;
                                  cursor: pointer;">
                          </div>
                        </div>
                        <?php
                      } else {
                        ?>
                        <div class="col-12 col-sm-12 col-md-12 mb-3 d-flex align-items-stretch">
                          <embed src="<?=MY_UPLOADURL.$f[COL_FILENAME]?>" width="100%" height="600" />
                        </div>
                        <?php
                      }
                      ?>
                    <?php
                    }
                    ?>
                  </div>
                  <?php
                } else {
                  ?>
                  <div class="row mb-2">
                    <div class="col-12 text-center">
                      <div href="<?=MY_UPLOADURL.$files[0][COL_FILENAME]?>"
                      data-toggle="lightbox"
                      data-title="<?=$data[COL_POSTTITLE]?>"
                      data-gallery="gallery"
                      style="background: url('<?=MY_UPLOADURL.$files[0][COL_FILENAME]?>');
                              background-size: cover;
                              background-repeat: no-repeat;
                              background-position: center;
                              width: 100%;
                              min-height: 300px;
                              cursor: pointer;">
                      </div>
                    </div>
                  </div>
                  <?php
                }
              }
              ?>
              <?=$data[COL_POSTCONTENT]?>
              <div class="row bt-1">
                <p class="font-sm text-muted">
                  <i class="fad fa-user"></i>&nbsp;&nbsp;<?=$data[COL_NAME]?><br  />
                  <i class="fad fa-calendar"></i>&nbsp;&nbsp;<?=date('d-m-Y H:i', strtotime($data[COL_CREATEDON]))?>
                </p>
              </div>
            </div>
            <div class="card-footer">
                <div id="disqus_thread"></div>
                <script>

                    /**
                     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                    /*
                     */
                    var disqus_config = function () {
                        this.page.url = '<?=site_url('site/home/page/'.$data[COL_POSTSLUG])?>';  // Replace PAGE_URL with your page's canonical URL variable
                        this.page.identifier = '<?=$data[COL_POSTSLUG]?>'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                    };
                    (function() { // DON'T EDIT BELOW THIS LINE
                        var d = document, s = d.createElement('script');
                        s.src = '<?=$this->setting_web_disqus_url?>';
                        s.setAttribute('data-timestamp', +new Date());
                        (d.head || d.body).appendChild(s);
                    })();
                </script>
                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="card">
            <div class="card-body p-0">
              <script type="text/javascript" src="https://widget.kominfo.go.id/gpr-widget-kominfo.min.js"></script>
              <div id="gpr-kominfo-widget-container"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/ekko-lightbox/ekko-lightbox.min.js"></script>
<script>
$(document).ready(function() {
  $(document).on('click', '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox({
      alwaysShowClose: true
    });
  });
});
(function ($) {
  var navbar = $('.navbar');
  var lastScrollTop = 0;

  $(window).scroll(function () {
      var st = $(this).scrollTop();
      // Scroll down
      if (st > lastScrollTop) {
          //navbar.fadeOut();
          navbar.removeClass('navbar-dark bg-transparent').addClass('navbar-dark bg-header');
      }
      // Scroll up but still lower than 200 (change that to whatever suits your need)
      else if(st < lastScrollTop && st > 200) {
          //navbar.fadeIn();
          navbar.removeClass('navbar-dark bg-transparent').addClass('navbar-dark bg-header');
      }
      // Reached top
      else {
          navbar.removeClass('navbar-dark bg-header').addClass('navbar-dark bg-transparent');
      }
      lastScrollTop = st;
  }).trigger('scroll');
})(jQuery);
</script>
