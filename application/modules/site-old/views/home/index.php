<section class="slider">
  <div class="hero-slider">
    <!-- Start Single Slider -->

    <!-- End Single Slider -->
    <!-- Start Single Slider -->
    <div class="single-slider" style="background-image:url('<?=base_url()?>assets/themes/mediplus-lite/img/slider-main.jpg')">
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <div class="text">
              <h1>SELAMAT <span>DATANG</span></h1>
              <p style="font-size: 18pt !important; line-height: 1.5 !important">di Portal Informasi dan Aplikasi <br /><strong>BAGIAN <span style="color: #E71414 !important">ORGANISASI</span> SETDA KAB. BATU BARA</strong></p>
              <div class="button">
                <!--<a href="#" class="btn">Profil</a>-->
                <a href="#contact-us" class="btn primary">Hubungi Kami</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Start End Slider -->
    <!-- Start Single Slider -->
    <div class="single-slider" style="background-image:url('<?=base_url()?>assets/themes/mediplus-lite/img/slider-main.jpg')">
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <div class="text">
              <h1>SELAMAT <span>DATANG</span></h1>
              <p style="font-size: 18pt !important; line-height: 1.5 !important">di Portal Informasi dan Aplikasi <br /><strong>BAGIAN <span style="color: #E71414 !important">ORGANISASI</span> SETDA KAB. BATU BARA</strong></p>
              <div class="button">
                <!--<a href="#" class="btn">Profil</a>-->
                <a href="#contact-us" class="btn primary">Hubungi Kami</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Single Slider -->
  </div>
</section>
<section class="schedule">
  <div class="container">
    <div class="schedule-inner">
      <div class="row" style="display: flex !important; align-items: stretch !important">
        <div class="col-12 col-sm-12 col-md-4" style="display: flex !important; align-items: stretch !important">
          <!-- single-schedule -->
          <div class="single-schedule" style="display: flex; flex-direction: column;; width: 100%">
            <div class="inner" style="height: 100%">
              <div class="icon">
                <i class="far fa-building"></i>
              </div>
              <div class="single-content">
                <!--<span>Lorem Amet</span>-->
                <h4>PROFIL</h4>
                <ul class="time-sidual">
                  <li class="day"><a href="" data-toggle="modal" data-target="#modalVisiMisi" style="margin-top: 0 !important">Visi & Misi</a></li>
                  <li class="day"><a href="" data-toggle="modal" data-target="#modalKataSambutan" style="margin-top: 0 !important">Motto</a></li>
                  <li class="day"><a href="" data-toggle="modal" data-target="#modalMaklumat" style="margin-top: 0 !important">Maklumat Pelayanan</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-12 col-md-4" style="display: flex !important; align-items: stretch !important">
          <!-- single-schedule -->
          <div class="single-schedule" style="display: flex; flex-direction: column;; width: 100%">
            <div class="inner" style="height: 100%">
              <div class="icon">
                <i class="far fa-map-signs"></i>
              </div>
              <div class="single-content">
                <!--<span>Lorem Amet</span>-->
                <h4>UNIT KERJA</h4>
                <ul class="time-sidual">
                  <li class="day"><a href="" data-toggle="modal" data-target="#modalSubbagPelayananPublik" style="margin-top: 0 !important">Pelayanan Publik & Tata Laksana</a></li>
                  <li class="day"><a href="" data-toggle="modal" data-target="#modalSubbagKelembagaan" style="margin-top: 0 !important">Kelembagaan & Analisis Jabatan</a></li>
                  <li class="day"><a href="" data-toggle="modal" data-target="#modalSubbagKinerja" style="margin-top: 0 !important">Kinerja & Reformasi Birokrasi</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-12 col-md-4" style="display: flex !important; align-items: stretch !important">
          <!-- single-schedule -->
          <div class="single-schedule" style="display: flex; flex-direction: column;; width: 100%">
            <div class="inner" style="height: 100%">
              <div class="icon">
                <i class="far fa-globe"></i>
              </div>
              <div class="single-content">
                <!--<span>Donec luctus</span>-->
                <h4>PORTAL APLIKASI</h4>
                <ul class="time-sidual">
                  <li class="day"><a href="" value=""data-toggle="modal" data-target="#modalESAKIP" style="margin-top: 0 !important">E-SAKIP</a><span></span></li>
                  <li class="day"><a href="" data-toggle="modal" data-target="#modalSISUKMA" style="margin-top: 0 !important">SI SUKMA</a><span></span></li>
                  <li class="day"><a href="" value=""data-toggle="modal" data-target="#modalRB" style="margin-top: 0 !important">REFORMASI BIROKRASI</a><span></span></li>
                  <li class="day"><a href="" data-toggle="modal" data-target="#modalCMS" style="margin-top: 0 !important">CMS</a><span></span></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php
$this->db->where(COL_KD_TAHUN_FROM." <=", date("Y"));
$this->db->where(COL_KD_TAHUN_TO." >=", date("Y"));
$this->db->order_by(COL_KD_TAHUN_FROM, "desc");
$rperiod = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();
$rmisi = $this->db
->where(COL_KD_PEMDA, !empty($rperiod[COL_KD_PEMDA])?$rperiod[COL_KD_PEMDA]:-999)
->order_by(COL_KD_MISI, "asc")
->get(TBL_SAKIP_MPMD_MISI)
->result_array();
$rtujuan = $this->db
->where(COL_KD_PEMDA, !empty($rperiod[COL_KD_PEMDA])?$rperiod[COL_KD_PEMDA]:-999)
->order_by(COL_KD_MISI)
->order_by(COL_KD_TUJUAN)
->get(TBL_SAKIP_MPMD_TUJUAN)
->result_array();
$riktujuan = $this->db
->where(COL_KD_PEMDA, !empty($rperiod[COL_KD_PEMDA])?$rperiod[COL_KD_PEMDA]:-999)
->order_by(COL_KD_MISI)
->order_by(COL_KD_TUJUAN)
->order_by(COL_KD_INDIKATORTUJUAN)
->get(TBL_SAKIP_MPMD_IKTUJUAN)
->result_array();
$rsasaran = $this->db
->where(COL_KD_PEMDA, !empty($rperiod[COL_KD_PEMDA])?$rperiod[COL_KD_PEMDA]:-999)
->order_by(COL_KD_MISI)
->order_by(COL_KD_TUJUAN)
->order_by(COL_KD_INDIKATORTUJUAN)
->order_by(COL_KD_SASARAN)
->get(TBL_SAKIP_MPMD_SASARAN)
->result_array();
$riksasaran = $this->db
->where(COL_KD_PEMDA, !empty($rperiod[COL_KD_PEMDA])?$rperiod[COL_KD_PEMDA]:-999)
->order_by(COL_KD_MISI)
->order_by(COL_KD_TUJUAN)
->order_by(COL_KD_INDIKATORTUJUAN)
->order_by(COL_KD_SASARAN)
->order_by(COL_KD_INDIKATORSASARAN)
->get(TBL_SAKIP_MPMD_IKSASARAN)
->result_array();
if(!empty($rperiod)) {
  ?>
  <section class="why-choose section" style="padding-top: 0 !important">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="section-title">
            <h2>PEMERINTAH DAERAH KAB. BATU BARA<br /><small>PERIODE <?=$rperiod?$rperiod[COL_KD_TAHUN_FROM]." - ".$rperiod[COL_KD_TAHUN_TO]:"-"?></small></h2>

            <img src="<?=base_url()?>assets/themes/mediplus-lite/img/section-div.png" style="width: 80px;" alt="#">
          </div>
        </div>
      </div>
      <div class="row" style="margin-bottom: 50px !important">
        <div class="col-lg-6 col-12">
          <div class="choose-left">
            <h3>Visi</h3>
            <p style="text-align: center">
              <img src="<?=base_url()?>assets/themes/mediplus-lite/img/bupati.png" style="width: 400px; height: auto" /><br /><br />
              <strong style="font-style:italic"><?=$rperiod[COL_NM_VISI]?></strong>
            </p>
          </div>
        </div>
        <div class="col-lg-6 col-12">
          <div class="choose-left">
            <h3>Misi</h3>
            <div class="row">
              <div class="col-lg-6">
                <ul class="list">
                  <?php
                  for($i=0; $i<count($rmisi)/2;$i++) {
                    ?>
                    <li><?=$rmisi[$i][COL_NM_MISI]?></li>
                    <?php
                  }
                  ?>
                </ul>
              </div>
              <div class="col-lg-6">
                <ul class="list">
                  <?php
                  for($n=$i; $n<count($rmisi);$n++) {
                    ?>
                    <li><?=$rmisi[$n][COL_NM_MISI]?></li>
                    <?php
                  }
                  ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div id="fun-facts" class="fun-facts section overlay" style="background:url(<?=base_url()?>assets/themes/mediplus-lite/img/section-bg-sakip.jpeg) !important">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-md-6 col-12">
          <!-- Start Single Fun -->
          <div class="single-fun">
            <a href="#" data-toggle="modal" data-target="#modal-tujuan"><i class="far fa-map"></i></a>
            <div class="content">
              <span class="counter"><?=number_format(count($rtujuan))?></span>
              <p>TUJUAN</p>
            </div>
          </div>
          <!-- End Single Fun -->
        </div>
        <div class="col-lg-3 col-md-6 col-12">
          <!-- Start Single Fun -->
          <div class="single-fun">
            <a href="#" data-toggle="modal" data-target="#modal-tujuan"><i class="far fa-list"></i></a>
            <div class="content">
              <span class="counter"><?=number_format(count($riktujuan))?></span>
              <p>INDIKATOR TUJUAN</p>
            </div>
          </div>
          <!-- End Single Fun -->
        </div>
        <div class="col-lg-3 col-md-6 col-12">
          <!-- Start Single Fun -->
          <div class="single-fun">
            <a href="#" data-toggle="modal" data-target="#modal-sasaran"><i class="far fa-bullseye-arrow"></i></a>
            <div class="content">
              <span class="counter"><?=number_format(count($rsasaran))?></span>
              <p>SASARAN</p>
            </div>
          </div>
          <!-- End Single Fun -->
        </div>
        <div class="col-lg-3 col-md-6 col-12">
          <!-- Start Single Fun -->
          <div class="single-fun">
            <a href="#" data-toggle="modal" data-target="#modal-sasaran"><i class="far fa-list"></i></a>
            <div class="content">
              <span class="counter"><?=number_format(count($riksasaran))?></span>
              <p>INDIKATOR SASARAN</p>
            </div>
          </div>
          <!-- End Single Fun -->
        </div>
      </div>
    </div>
  </div>
  <?php
}
?>

<?php
if(!empty($berita)) {
  ?>
  <section class="blog section" id="blog" style="background: #f9f9f9 !important">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="section-title">
            <h2>BERITA TERKINI</h2>
            <img src="<?=base_url()?>assets/themes/mediplus-lite/img/section-div.png" style="width: 80px;" alt="#">
          </div>
        </div>
      </div>
      <div class="row">
        <?php
        $n=0;
        foreach($berita as $b) {
          $n++;
          $strippedcontent = strip_tags($b[COL_POSTCONTENT]);
          $img = $this->db->where(COL_ISTHUMBNAIL,1)->where(COL_POSTID, $b[COL_POSTID])->get(TBL__POSTIMAGES)->row_array();
          ?>
          <div class="col-lg-4 col-md-6 col-12" style="padding-top: 20px!important">
            <div class="single-news">
              <div class="news-head">
                <div style="
                height: 250px;
                width: 100%;
                background-image: url('<?=!empty($img)?MY_UPLOADURL.$img[COL_IMGPATH]:MY_IMAGEURL.'no-image.png'?>');
                background-size: cover;
                background-repeat: no-repeat;
                background-position: center;
                ">
                </div>
              </div>
              <div class="news-body">
                <div class="news-content">
                  <div class="date"><?=date('d-m-Y', strtotime($b[COL_CREATEDON]))?></div>
                  <h2>
                    <a href="<?=site_url('site/home/page/'.$b[COL_POSTSLUG])?>"><h5 class="text-red"><?=$b[COL_POSTTITLE]?></h5></a>
                  </h2>
                  <p class="text" style="text-transform: none !important">
                    <?=strlen($strippedcontent) > 150 ? substr($strippedcontent, 0, 150) . "..." : $strippedcontent ?>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <?php
        }
        ?>
      </div>
    </div>
  </section>
  <?php
}
?>

<?php
if(!empty($galeri)) {
  ?>
  <section class="portfolio section" style="background: #FFF !important">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="section-title">
            <h2>PROFIL PEGAWAI</h2>
            <img src="<?=base_url()?>assets/themes/mediplus-lite/img/section-div.png" style="width: 80px;" alt="#">
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 col-12">
          <div id="owl-pegawai" class="owl-carousel galeri-slider">
            <?php
            foreach($galeri as $r) {
              $rfiles = $this->db
              ->where(COL_POSTID, $r[COL_POSTID])
              ->get(TBL__POSTIMAGES)
              ->result_array();
              foreach($rfiles as $f) {
                ?>
                <div class="single-pf" style="width: 300px; height: 300px; background-image: url(<?=MY_UPLOADURL.$f[COL_IMGPATH]?>); background-size: cover">
                  <a href="<?=site_url('site/ajax/popup-galeri/'.$f[COL_POSTIMAGEID])?>" class="btn btn-popup-galeri">LIHAT</a>
                </div>
                <?php
              }
            }
            ?>

          </div>
        </div>
      </div>
    </div>
  </section>
  <?php
}
?>

<section class="appointment" id="contact-us" style="background: #f9f9f9 !important; padding-top: 170px !important">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="section-title">
          <h2>HUBUNGI KAMI</h2>
          <img src="<?=base_url()?>assets/themes/mediplus-lite/img/section-div.png" style="width: 80px;" alt="#">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6 col-md-12 col-12">
        <form class="form" action="#">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-12">
              <div class="form-group">
                <input name="name" type="text" placeholder="Nama Lengkap">
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-12">
              <div class="form-group">
                <input name="email" type="email" placeholder="Email">
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-12">
              <div class="form-group">
                <input name="phone" type="text" placeholder="No. Telp / HP">
              </div>
            </div>
            <div class="col-lg-12 col-md-12 col-12">
              <div class="form-group">
                <textarea name="message" placeholder="Tuliskan pesan / catatan anda disini.."></textarea>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-5 col-md-4 col-12">
              <div class="form-group">
                <div class="button">
                  <button type="submit" class="btn" style="background: #E71414 !important">SUBMIT</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="col-lg-6 col-md-12 ">
        <div class="appointment-image">
          <img src="<?=base_url()?>assets/themes/mediplus-lite/img/contact-us.png" alt="#">
        </div>
      </div>
    </div>
  </div>
</section>
<div id="modalSubbagPelayananPublik" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Subbag Pelayanan Publik & Tata Laksana</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-12">
            <h6>JENIS LAYANAN</h6>
            <ul class="list" style="padding-left: 10px; margin-bottom: 20px">
              <li>Penyusunan Standar Operasional Prosedur Administrasi Pemerintah (SOP AP)</li>
              <li>Penyusunan Standar Pelayanan Perangkat Daerah</li>
              <li>Penyusunan Peta Proses Bisnis Instansi Pemerintah</li>
              <li>Penyusunan Survei Kepuasan Masyarakat</li>
              <li>Konsultasi Tata Naskah Dinas</li>
              <li>Penggunaan Pakaian Dinas</li>
            </ul>

            <h6>PROSEDUR</h6>
            <ol style="padding-left: 20px;">
              <li style="padding-left: 10px;">Tamu datang ke Bagian Organisasi Setda Kab. Batu Bara</li>
              <li style="padding-left: 10px;">Tamu melakukan registrasi / mengisi Buku Tamu</li>
              <li style="padding-left: 10px;">Tamu melakukan konsultasi dengan Kepala Subbag Pelayanan Publik & Tata Laksana sesuai kebutuhan</li>
              <li style="padding-left: 10px;">Jika konsultasi dianggap cukup oleh tamu maka konsultasi dianggap selesai</li>
              <li style="padding-left: 10px;">Jika konsultasi dianggap belum cukup maka Kepala Subbag Pelayanan Publik & Tata Laksana akan meminta saran kepada Kepala Bagian Organisasi untuk disampaikan kepada tamu</li>
            </ol>

            <h6>JAM PELAYANAN</h6>
            <div class="row" style="margin-bottom: 25px">
              <div class="col-sm-12 col-12">
                <table class="table table-bordered" style="margin-top: 10px;">
                  <thead style="font-weight: bold">
                    <tr>
                      <td style="vertical-align: middle !important">HARI</td>
                      <td style="vertical-align: middle !important">JAM PELAYANAN</td>
                      <td style="vertical-align: middle !important">JAM ISTIRAHAT</td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td style="vertical-align: middle !important; padding: .25rem .75rem !important">Senin s.d Kamis</td>
                      <td style="vertical-align: middle !important; padding: .25rem .75rem !important">08.00 - 16.15</td>
                      <td style="vertical-align: middle !important; padding: .25rem .75rem !important">12.15 - 12.30</td>
                    </tr>
                    <tr>
                      <td style="vertical-align: middle !important; padding: .25rem .75rem !important">Jum'at</td>
                      <td style="vertical-align: middle !important; padding: .25rem .75rem !important">08.00 - 14.30</td>
                      <td style="vertical-align: middle !important; padding: .25rem .75rem !important">12.30 - 13.00</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <h6>BIAYA <strong style="color: #E71414 !important">GRATIS</strong></h6>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn primary" data-dismiss="modal" style="background: #E71414 !important">TUTUP</button>
      </div>
    </div>
  </div>
</div>
<div id="modalSubbagKelembagaan" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Subbag Kelembagaan dan Analisis Jabatan</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-12">
            <h6>JENIS LAYANAN</h6>
            <ul class="list" style="padding-left: 10px; margin-bottom: 20px">
              <li>Penyusunan ANJAB & ABK</li>
              <li>Penyusunan Evaluasi Jabatan (EVJAB)</li>
              <li>Penyusunan Tugas Pokok dan Fungsi</li>
              <li>Penyusunan Penilaian Perangkat Daerah</li>
              <li>Penataan Perangkat Daerah</li>
            </ul>

            <h6>PROSEDUR</h6>
            <ol style="padding-left: 20px;">
              <li style="padding-left: 10px;">Tamu datang ke Bagian Organisasi Setda Kab. Batu Bara</li>
              <li style="padding-left: 10px;">Tamu melakukan registrasi / mengisi Buku Tamu</li>
              <li style="padding-left: 10px;">Tamu melakukan konsultasi dengan Kepala Subbag Kelembagaan dan Analisis Jabatan sesuai kebutuhan</li>
              <li style="padding-left: 10px;">Jika konsultasi dianggap cukup oleh tamu maka konsultasi dianggap selesai</li>
              <li style="padding-left: 10px;">Jika konsultasi dianggap belum cukup maka Kepala Subbag Kelembagaan dan Analisis Jabatan akan meminta saran kepada Kepala Bagian Organisasi untuk disampaikan kepada tamu</li>
            </ol>

            <h6>JAM PELAYANAN</h6>
            <div class="row" style="margin-bottom: 25px">
              <div class="col-sm-12 col-12">
                <table class="table table-bordered" style="margin-top: 10px;">
                  <thead style="font-weight: bold">
                    <tr>
                      <td style="vertical-align: middle !important">HARI</td>
                      <td style="vertical-align: middle !important">JAM PELAYANAN</td>
                      <td style="vertical-align: middle !important">JAM ISTIRAHAT</td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td style="vertical-align: middle !important; padding: .25rem .75rem !important">Senin s.d Kamis</td>
                      <td style="vertical-align: middle !important; padding: .25rem .75rem !important">08.00 - 16.15</td>
                      <td style="vertical-align: middle !important; padding: .25rem .75rem !important">12.15 - 12.30</td>
                    </tr>
                    <tr>
                      <td style="vertical-align: middle !important; padding: .25rem .75rem !important">Jum'at</td>
                      <td style="vertical-align: middle !important; padding: .25rem .75rem !important">08.00 - 14.30</td>
                      <td style="vertical-align: middle !important; padding: .25rem .75rem !important">12.30 - 13.00</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <h6>BIAYA <strong style="color: #E71414 !important">GRATIS</strong></h6>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn primary" data-dismiss="modal" style="background: #E71414 !important">TUTUP</button>
      </div>
    </div>
  </div>
</div>
<div id="modalSubbagKinerja" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Subbag Kinerja dan Reformasi Birokrasi</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-12">
            <h6>JENIS LAYANAN</h6>
            <ul class="list" style="padding-left: 10px; margin-bottom: 20px">
              <li>Penyusunan Roadmap Reformasi Birokrasi</li>
              <li>Penyusunan Laporan Kinerja</li>
            </ul>

            <h6>PROSEDUR</h6>
            <ol style="padding-left: 20px;">
              <li style="padding-left: 10px;">Tamu datang ke Bagian Organisasi Setda Kab. Batu Bara</li>
              <li style="padding-left: 10px;">Tamu melakukan registrasi / mengisi Buku Tamu</li>
              <li style="padding-left: 10px;">Tamu melakukan konsultasi dengan Kepala Subbag Kinerja dan Reformasi Birokrasi sesuai kebutuhan</li>
              <li style="padding-left: 10px;">Jika konsultasi dianggap cukup oleh tamu maka konsultasi dianggap selesai</li>
              <li style="padding-left: 10px;">Jika konsultasi dianggap belum cukup maka Kepala Subbag Kinerja dan Reformasi Birokrasi akan meminta saran kepada Kepala Bagian Organisasi untuk disampaikan kepada tamu</li>
            </ol>

            <h6>JAM PELAYANAN</h6>
            <div class="row" style="margin-bottom: 25px">
              <div class="col-sm-12 col-12">
                <table class="table table-bordered" style="margin-top: 10px;">
                  <thead style="font-weight: bold">
                    <tr>
                      <td style="vertical-align: middle !important">HARI</td>
                      <td style="vertical-align: middle !important">JAM PELAYANAN</td>
                      <td style="vertical-align: middle !important">JAM ISTIRAHAT</td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td style="vertical-align: middle !important; padding: .25rem .75rem !important">Senin s.d Kamis</td>
                      <td style="vertical-align: middle !important; padding: .25rem .75rem !important">08.00 - 16.15</td>
                      <td style="vertical-align: middle !important; padding: .25rem .75rem !important">12.15 - 12.30</td>
                    </tr>
                    <tr>
                      <td style="vertical-align: middle !important; padding: .25rem .75rem !important">Jum'at</td>
                      <td style="vertical-align: middle !important; padding: .25rem .75rem !important">08.00 - 14.30</td>
                      <td style="vertical-align: middle !important; padding: .25rem .75rem !important">12.30 - 13.00</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <h6>BIAYA <strong style="color: #E71414 !important">GRATIS</strong></h6>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn primary" data-dismiss="modal" style="background: #E71414 !important">TUTUP</button>
      </div>
    </div>
  </div>
</div>
<div id="modalESAKIP" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">E-SAKIP</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-12">
            <p style="text-align: justify">
              E-SAKIP adalah sistem informasi / aplikasi elektronik berbasis web yang bertujuan untuk memudahkan proses pemantauan dan pengendalian kinerja dalam rangka meningkatkan akuntabilitas dan kinerja unit kerja. Aplikasi ini menghimpun data sistematik terkait pelaporan kinerja serta menampilkan proses perencanaan kinerja, penganggaran kinerja, keterkaitan kegiatan / sub kegiatan dalam pencapaian target kinerja, dan monitoring serta evaluasi pencapaian kinerja dan keuangan.
            </p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <a href="https://bagianorganisasi.batubarakab.go.id/sakipv2/user/login.jsp" class="btn primary" style="background: #E71414 !important; color: #fff !important">MASUK <i class="fas fa-sign-in-alt"></i></a>
      </div>
    </div>
  </div>
</div>
<div id="modalRB" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Reformasi Birokrasi</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-12">
            <p style="text-align: justify">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <a href="<?=site_url('rb/user/login')?>" class="btn primary" style="background: #E71414 !important; color: #fff !important">MASUK <i class="fas fa-sign-in-alt"></i></a>
      </div>
    </div>
  </div>
</div>
<div id="modalSISUKMA" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">SI SUKMA</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-12">
            <p style="text-align: justify">
              SI SUKMA (Sistem Informasi Survei Kepuasan Masyarakat) merupakan platform Survei Kepuasan Masyarakat yang membantu kegiatan pengukuran secara komprehensif tentang tingkat kepuasan masyarakat terhadap kualitas layanan yang diberikan oleh penyelenggara pelayanan publik dengan tujuan untuk mengetahui kelemahan atau kekurangan dari masing-masing unsur dalam penyelenggara pelayanan publik secara periodik.
            </p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <a href="https://bagianorganisasi.batubarakab.go.id/skm/" class="btn primary" style="background: #E71414 !important; color: #fff !important">MASUK <i class="fas fa-sign-in-alt"></i></a>
      </div>
    </div>
  </div>
</div>
<div id="modalCMS" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">CMS</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-12">
            <p style="text-align: justify">
              CMS (Content Management System) merupakan portal khusus administrator yang digunakan untuk mengelola informasi publik (berita, infogratis, data strategis, dll) yang akan ditayangkan melalui website Bagian Organisasi Setda Kab. Batu Bara.
            </p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <a href="https://bagianorganisasi.batubarakab.go.id/site/user/login.jsp" class="btn primary" style="background: #E71414 !important; color: #fff !important">MASUK <i class="fas fa-sign-in-alt"></i></a>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-tujuan" aria-modal="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">TUJUAN</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body p-0">
        <table class="table table-striped" width="100%" style="margin-bottom: 0 !important">
          <tbody>
            <?php
            $n=1;
            foreach ($rtujuan as $t) {
              echo '<tr><td><strong>'.$n.'</strong></td><td colspan="2"><strong>'.$t[COL_NM_TUJUAN].'</strong></td></tr>';
              foreach($riktujuan as $ikt) {
                if($ikt[COL_KD_MISI]==$t[COL_KD_MISI] && $ikt[COL_KD_TUJUAN]==$t[COL_KD_TUJUAN]) {
                  echo '<tr><td></td><td style="width: 100px; white-space: nowrap">INDIKATOR '.$ikt[COL_KD_INDIKATORTUJUAN].'</td><td>'.$ikt[COL_NM_INDIKATORTUJUAN].'</td></tr>';
                }
              }
              $n++;
            }
            ?>
            <tr></tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-sasaran" aria-modal="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">SASARAN</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body p-0">
        <table class="table table-striped" width="100%" style="margin-bottom: 0 !important">
          <tbody>
            <?php
            $n=1;
            foreach ($rsasaran as $t) {
              echo '<tr><td><strong>'.$n.'</strong></td><td colspan="2"><strong>'.$t[COL_NM_SASARAN].'</strong></td></tr>';
              foreach($riksasaran as $ikt) {
                if($ikt[COL_KD_MISI]==$t[COL_KD_MISI] && $ikt[COL_KD_TUJUAN]==$t[COL_KD_TUJUAN] && $ikt[COL_KD_SASARAN]==$t[COL_KD_SASARAN]) {
                  echo '<tr><td></td><td style="width: 100px; white-space: nowrap">INDIKATOR '.$ikt[COL_KD_INDIKATORSASARAN].'</td><td>'.$ikt[COL_NM_INDIKATORSASARAN].'</td></tr>';
                }
              }
              $n++;
            }
            ?>
            <tr></tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-galeri" aria-modal="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">GALERI</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body p-0">
        <p class="p-2">MEMUAT...</p>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalKataSambutan" aria-modal="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Motto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body p-0">
        <img src="<?=base_url()?>assets/themes/mediplus-lite/img/home-04-motto.png" style="width: 100%" />
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalMaklumat" aria-modal="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Maklumat Pelayanan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body p-0">
        <img src="<?=base_url()?>assets/themes/mediplus-lite/img/home-01-maklumat.png" style="width: 100%" />
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalVisiMisi" aria-modal="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Visi & Misi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body p-0">
        <img src="<?=base_url()?>assets/themes/mediplus-lite/img/home-03-visimisi.png" style="width: 100%" />
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $("#modal-galeri").on('hidden.bs.modal', function(){
    $('.modal-title', $(this)).html('GALERI');
    $('.modal-body', $(this)).html('<p class="p-2">MEMUAT...</p>');
  });
  var owlGaleri = $('.galeri-slider');
  owlGaleri.owlCarousel({
    autoplay:true,
    autoplayTimeout:4000,
    margin:15,
    smartSpeed:300,
    autoplayHoverPause:true,
    loop:true,
    nav:true,
    dots:false,
    responsive:{
      300: {
        items:1,
      },
      480: {
        items:2,
      },
      768: {
        items:2,
      },
      1170: {
        items:4,
      },
    },
    onInitialized: function(){
      $('.btn-popup-galeri', owlGaleri).click(function(){
        var mod = $('#modal-galeri').modal('show');
        var href = $(this).attr('href');

        $.ajax({
          url: href
        }).done(function(data) {
          var dat = JSON.parse(data);
          $('.modal-body', mod).html(dat.body);
          $('.modal-title', mod).html(dat.title);
        });
        return false;
      });
    }
  });
});
</script>
