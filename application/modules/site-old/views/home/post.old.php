<div class="content-wrapper mt-0">
  <div class="hero-container" style="padding: 60px 20px !important; background-image: url('<?=MY_IMAGEURL.'hero-bg.png'?>') !important; background-size: cover; background-position-y: top">
    <!--<span class="hero-wave"></span>-->
    <div class="hero-content">
      <div class="row">
        <div class="col-sm-12">
          <div class="hero-item text-center" style="transform: translateY(20px) !important">
            <h4 class="font-weight-bold text-white">
              <?=strtoupper($title)?>
            </h4>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="content pt-3">
    <div class="container">
      <div class="row">
        <div class="col-sm-8">
          <?php
          if(!empty($res)) {
            ?>
            <div class="row d-flex align-items-stretch">
              <?php
              foreach($res as $r) {
                $strippedcontent = strip_tags($r[COL_POSTCONTENT]);
                $img = $this->db->where(COL_POSTID, $r[COL_POSTID])->get(TBL__POSTIMAGES)->row_array();
                ?>
                <div class="col-12 col-sm-12 col-md-6 d-flex align-items-stretch">
                  <div class="card card-outline card-danger">
                    <div class="card-header">
                      <h3 class="card-title"><?=$r[COL_POSTTITLE]?></h3>
                    </div>
                    <div class="card-body p-0">
                      <div style="
                      width: 100%;
                      height: 25vh;
                      background-image: url('<?=!empty($img)?MY_UPLOADURL.$img[COL_FILENAME]:MY_IMAGEURL.'no-image.png'?>');
                      background-size: cover;
                      background-repeat: no-repeat;
                      background-position: center;
                      ">
                      </div>
                      <p class="p-3 mb-0"><?=strlen($strippedcontent) > 200 ? substr($strippedcontent, 0, 200) . "..." : $strippedcontent ?></p>
                    </div>
                    <div class="card-footer text-right">
                      <a href="<?=site_url('site/home/page/'.$r[COL_POSTSLUG])?>" class="text-danger">SELENGKAPNYA</a>
                    </div>
                  </div>
                </div>
                <?php
              }
              ?>
            </div>
            <div class="row d-flex pt-3 mb-3 align-items-stretch">
              <div class="col-sm-6 text-right">
                <?php
                if($start==0) {
                  ?>
                  <button type="button" class="btn btn-default" disabled>
                    <i class="far fa-chevron-left"></i>&nbsp;SEBELUMNYA
                  </button>
                  <?php
                } else {
                  ?>
                  <a href="?start=<?=$start-10?>" class="btn btn-default"><i class="far fa-chevron-left"></i>&nbsp;SEBELUMNYA</a>
                  <?php
                }
                ?>
              </div>
              <div class="col-sm-6 text-left">
                <?php
                if($start+10>$count) {
                  ?>
                  <button type="button" class="btn btn-default" disabled>
                    SELANJUTNYA&nbsp;<i class="far fa-chevron-right"></i>
                  </button>
                  <?php
                } else {
                  ?>
                  <a href="?start=<?=$start+10?>" class="btn btn-default">SELANJUTNYA&nbsp;<i class="far fa-chevron-right"></i></a>
                  <?php
                }
                ?>
              </div>
            </div>

            <?php
          } else {
            ?>
            <p class="text-center">Tidak ada data.</p>
            <?php
          }
          ?>
        </div>
        <div class="col-sm-4">
          <div class="card card-outline card-danger">
            <div class="card-header">
              <h5 class="card-title">PENCARIAN</h5>
            </div>
            <div class="card-body">
              <form method="get" action="<?=current_url()?>" class="form-horizontal">
                <div class="form-group">
                  <label>Tanggal</label>
                  <div class="row">
                    <div class="col-sm-6">
                      <input type="text" class="form-control" name="dateFrom" placeholder="Dari" value="<?=!empty($_GET['dateFrom'])?$_GET['dateFrom']:''?>" />
                    </div>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" name="dateTo" placeholder="Sampai" value="<?=!empty($_GET['dateTo'])?$_GET['dateTo']:''?>" />
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label>Kata Kunci</label>
                  <input type="text" class="form-control" name="keyword" placeholder="Keyword" value="<?=!empty($_GET['keyword'])?$_GET['keyword']:''?>" />
                </div>
                <div class="form-group">
                  <input type="hidden" name="start" value="<?=$start?>" />
                  <button type="submit" class="btn btn-block btn-danger">CARI</button>
                </div>
              </form>
            </div>
          </div>
          <div class="card">
            <div class="card-body p-0">
              <script type="text/javascript" src="https://widget.kominfo.go.id/gpr-widget-kominfo.min.js"></script>
              <div id="gpr-kominfo-widget-container"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
(function ($) {
  var navbar = $('.navbar');
  var lastScrollTop = 0;

  $(window).scroll(function () {
      var st = $(this).scrollTop();
      // Scroll down
      if (st > lastScrollTop) {
          //navbar.fadeOut();
          navbar.removeClass('navbar-dark bg-transparent').addClass('navbar-light bg-white');
      }
      // Scroll up but still lower than 200 (change that to whatever suits your need)
      else if(st < lastScrollTop && st > 200) {
          //navbar.fadeIn();
          navbar.removeClass('navbar-dark bg-transparent').addClass('navbar-light bg-white');
      }
      // Reached top
      else {
          navbar.removeClass('navbar-light bg-white').addClass('navbar-dark bg-transparent');
      }
      lastScrollTop = st;
  }).trigger('scroll');
})(jQuery);
</script>
