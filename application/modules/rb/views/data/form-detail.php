<?php
$arrPeriod = array();
if(isset($data)) {
  $arrPeriod = explode(",", $data[COL_PERIODTARGET]);
}
?>
<form id="form-detail" method="post" action="<?=current_url()?>">
<div class="modal-header">
  <h5 class="modal-title"><?=$mode=='edit'?'UBAH':'TAMBAH'?> RENCANA AKSI</h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true"><i class="fa fa-close"></i></span>
  </button>
</div>
<div class="modal-body">
  <div class="row">
    <div class="col-sm-12">
      <div class="form-group">
        <label>KEGIATAN</label>
        <textarea name="<?=COL_NMTAHAPAN?>" class="form-control" placeholder="Tahapan / Kegiatan"><?=isset($data)?$data[COL_NMTAHAPAN]:''?></textarea>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label>INDIKATOR</label>
            <textarea name="<?=COL_NMINDIKATOR?>" class="form-control" placeholder="Indikator"><?=isset($data)?$data[COL_NMINDIKATOR]:''?></textarea>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label>OUTPUT</label>
            <textarea name="<?=COL_NMOUTPUT?>" class="form-control" placeholder="Output Kegiatan"><?=isset($data)?$data[COL_NMOUTPUT]:''?></textarea>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label>TARGET</label>
            <input type="text" class="form-control" name="<?=COL_NMTARGET?>" placeholder="Target" value="<?=isset($data)?$data[COL_NMTARGET]:''?>" />
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label>SATUAN</label>
            <input type="text" class="form-control" name="<?=COL_NMSATUAN?>" placeholder="Satuan" value="<?=isset($data)?$data[COL_NMSATUAN]:''?>" />
          </div>
        </div>
      </div>
      <div class="form-group">
        <label>WAKTU PELAKSANAAN</label>
        <div class="row">
          <div class="col-sm-3">
            <label><input type="checkbox" name="Period[]" value="1" <?=!empty($arrPeriod)&&in_array("1", $arrPeriod)?'checked':''?> />&nbsp;&nbsp;JAN</label><br />
            <label><input type="checkbox" name="Period[]" value="2" <?=!empty($arrPeriod)&&in_array("2", $arrPeriod)?'checked':''?> />&nbsp;&nbsp;FEB</label><br />
            <label><input type="checkbox" name="Period[]" value="3" <?=!empty($arrPeriod)&&in_array("3", $arrPeriod)?'checked':''?> />&nbsp;&nbsp;MAR</label><br />
          </div>
          <div class="col-sm-3">
            <label><input type="checkbox" name="Period[]" value="4" <?=!empty($arrPeriod)&&in_array("4", $arrPeriod)?'checked':''?> />&nbsp;&nbsp;APR</label><br />
            <label><input type="checkbox" name="Period[]" value="5" <?=!empty($arrPeriod)&&in_array("5", $arrPeriod)?'checked':''?> />&nbsp;&nbsp;MEI</label><br />
            <label><input type="checkbox" name="Period[]" value="6" <?=!empty($arrPeriod)&&in_array("6", $arrPeriod)?'checked':''?> />&nbsp;&nbsp;JUN</label><br />
          </div>
          <div class="col-sm-3">
            <label><input type="checkbox" name="Period[]" value="7" <?=!empty($arrPeriod)&&in_array("7", $arrPeriod)?'checked':''?> />&nbsp;&nbsp;JUL</label><br />
            <label><input type="checkbox" name="Period[]" value="8" <?=!empty($arrPeriod)&&in_array("8", $arrPeriod)?'checked':''?> />&nbsp;&nbsp;AGT</label><br />
            <label><input type="checkbox" name="Period[]" value="9" <?=!empty($arrPeriod)&&in_array("9", $arrPeriod)?'checked':''?> />&nbsp;&nbsp;SEP</label><br />
          </div>
          <div class="col-sm-3">
            <label><input type="checkbox" name="Period[]" value="10" <?=!empty($arrPeriod)&&in_array("10", $arrPeriod)?'checked':''?> />&nbsp;&nbsp;OKT</label><br />
            <label><input type="checkbox" name="Period[]" value="11" <?=!empty($arrPeriod)&&in_array("11", $arrPeriod)?'checked':''?> />&nbsp;&nbsp;NOV</label><br />
            <label><input type="checkbox" name="Period[]" value="12" <?=!empty($arrPeriod)&&in_array("12", $arrPeriod)?'checked':''?> />&nbsp;&nbsp;DES</label><br />
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
<div class="modal-footer">
  <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
  <button type="submit" class="btn btn-primary btn-sm"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
</div>
</form>
