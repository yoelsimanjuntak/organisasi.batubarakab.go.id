<?php
$ruser = GetLoggedUser();
$rperubahan = $this->db
->where(COL_IDRENJA, $data[COL_UNIQ])
->order_by(COL_UNIQ)
->get(TBL_RB_RENJAPERUBAHAN)
->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=$title?></h3>
      </div>
      <div class="col-sm-6 text-right">
        <a href="<?=site_url('rb/data/index/'.strtolower($data[COL_NMTYPE]))?>" class="btn btn-sm btn-secondary"><i class="far fa-arrow-circle-left"></i> KEMBALI</a>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <?php
        $no=1;
        foreach($rperubahan as $d) {
          $arrKegiatan = array();
          if(!empty($d[COL_NMKEGIATAN])) {
            $arrKegiatan = explode(";", $d[COL_NMKEGIATAN]);
          }

          $rtahapan = $this->db
          ->where(COL_IDRENJA, $data[COL_UNIQ])
          ->where(COL_IDPERUBAHAN, $d[COL_UNIQ])
          ->get(TBL_RB_RENJADET)
          ->result_array();
          ?>
          <div class="card collapsed-card" data-id="<?=$d[COL_UNIQ]?>">
            <div class="card-header">
              <h5 class="card-title"><?=$no?>. <strong><?=$d[COL_NMPERUBAHAN]?></strong></h5>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                </button>
              </div>
            </div>
            <div class="card-body p-0">
              <div class="row">
                <div class="col-sm-12">
                  <?php
                  if(!empty($arrKegiatan)) {
                    ?>
                    <ul class="font-italic mt-2">
                      <?php
                      foreach($arrKegiatan as $k) {
                        ?>
                        <li><?=$k?></li>
                        <?php
                      }
                      ?>
                    </ul>
                    <?php
                  }
                  ?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <table class="table table-bordered tbl-det mb-0" style="border-right: none !important; border-left: none !important" style="width: 100%">
                    <thead class="text-sm bg-teal">
                      <tr>
                        <th rowspan="2" class="text-center" style="vertical-align: middle">RENCANA AKSI</th>
                        <th rowspan="2" class="text-center" style="vertical-align: middle">INDIKATOR</th>
                        <th rowspan="2" class="text-center" style="vertical-align: middle">OUTPUT</th>
                        <th rowspan="2" class="text-center" style="vertical-align: middle">TARGET</th>
                        <th rowspan="2" class="text-center" style="vertical-align: middle">SATUAN</th>
                        <th colspan="12" class="text-center" style="vertical-align: middle">WAKTU PELAKSANAAN</th>
                        <th rowspan="2" class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap; text-align: center; padding-left: .75rem !important; padding-right: .75rem !important">OPSI</th>
                      </tr>
                      <tr>
                        <?php
                        for($i=1;$i<=12;$i++) {
                          ?>
                          <th style="width: 10px; white-space: nowrap; padding-left: .75rem !important; padding-right: .75rem !important"><?=str_pad($i,2,'0', STR_PAD_LEFT)?></th>
                          <?php
                        }
                        ?>
                      </tr>
                    </thead>
                    <tbody class="text-sm">

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="card-footer">
              <?php
              if($ruser[COL_ROLEID]!=ROLEGUEST) {
                ?>
<button type="button" class="btn btn-primary btn-add-detail btn-sm" data-url="<?=site_url('rb/data/detail-add/'.$d[COL_UNIQ])?>"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH</button>
                <?php
              }
              ?>

              <button type="button" class="btn btn-secondary btn-refresh-detail btn-sm" data-url="<?=site_url('rb/data/detail-load/'.$d[COL_UNIQ])?>">
                <i class="far fa-refresh"></i>&nbsp;REFRESH
              </button>
            </div>

          </div>
          <?php
          $no++;
        }
        ?>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-detail" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">

    </div>
  </div>
</div>
<script type="text/javascript">
var modalDetail = $("#modal-detail");

$(document).ready(function(){
  $('.btn-refresh-detail').click(function(){
    var url = $(this).data('url');
    var card = $(this).closest('.card');
    var overlay = $('<div class="overlay"><i class="far fa-2x fa-refresh fa-spin"></i></div>');
    if(url) {
      card.append(overlay);
      $('.tbl-det>tbody', card).load(url, function(){
        overlay.remove();

        $('.btn-edit-detail', card).click(function(){
          var url = $(this).data('url');
          var overlay = $('<div class="overlay"><i class="far fa-2x fa-refresh fa-spin"></i></div>');
          if(url) {
            $('.modal-content', modalDetail).load(url, function(response, status, xhr){
              if(status == "error") {
                toastr.error('MAAF, TELAH TERJADI KESALAHAN SERVER');
                return false;
              }

              modalDetail.modal('show');
              $('form', modalDetail).validate({
                submitHandler: function(form) {
                  var btnSubmit = $('button[type=submit]', form);
                  btnSubmit.attr('disabled', true);
                  $(form).ajaxSubmit({
                    dataType: 'json',
                    type : 'post',
                    success: function(res) {
                      if(res.error != 0) {
                        toastr.error(res.error);
                      } else {
                        toastr.success(res.success);
                        $('.btn-refresh-detail', card).click();
                        modalDetail.modal('hide');
                      }
                    },
                    error: function() {
                      toastr.error('SERVER ERROR');
                    },
                    complete: function() {
                      btnSubmit.attr('disabled', false);
                    }
                  });

                  return false;
                }
              });
            });
          }
        });

        $('.btn-delete-detail', card).click(function(){
          var url = $(this).data('url');
          var overlay = $('<div class="overlay"><i class="far fa-2x fa-refresh fa-spin"></i></div>');
          if(url) {
            if(confirm('Apakah anda yakin menghapus kegiatan ini?')) {
              $.ajax({
                url: url,
                type: 'GET',
                success: function(data){
                  data = JSON.parse(data);
                  if(data.error==0) toastr.success(data.success);
                  else toastr.error(data.error);
                },
                error: function(data) {
                  toastr.error('MAAF, TELAH TERJADI KESALAHAN SERVER');
                  return false;
                },
                complete: function(data) {
                  $('.btn-refresh-detail', card).click();
                }
              });
            }
          }
        });
      });
    }
  }).trigger('click');

  $('.btn-add-detail').click(function(){
    var url = $(this).data('url');
    var card = $(this).closest('.card');
    var overlay = $('<div class="overlay"><i class="far fa-2x fa-refresh fa-spin"></i></div>');
    if(url) {
      $('.modal-content', modalDetail).load(url, function(response, status, xhr){
        if(status == "error") {
          toastr.error('MAAF, TELAH TERJADI KESALAHAN SERVER');
          return false;
        }

        modalDetail.modal('show');
        $('form', modalDetail).validate({
          submitHandler: function(form) {
            var btnSubmit = $('button[type=submit]', form);
            btnSubmit.attr('disabled', true);
            $(form).ajaxSubmit({
              dataType: 'json',
              type : 'post',
              success: function(res) {
                if(res.error != 0) {
                  toastr.error(res.error);
                } else {
                  toastr.success(res.success);
                  $('.btn-refresh-detail', card).click();
                  modalDetail.modal('hide');
                }
              },
              error: function() {
                toastr.error('SERVER ERROR');
              },
              complete: function() {
                btnSubmit.attr('disabled', false);
              }
            });

            return false;
          }
        });
      });
    }
  });
});
</script>
