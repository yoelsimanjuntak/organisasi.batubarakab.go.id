<form id="form-editor" method="post" action="#">
<div class="modal-header">
  <h5 class="modal-title">ENTRI MONEV</h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true"><i class="fa fa-close"></i></span>
  </button>
</div>
<div class="modal-body">
  <div class="form-group">
    <label>CAPAIAN <small><?=!empty($rtahapan)?'('.$rtahapan[COL_NMSATUAN].')':''?></small></label>
    <input type="text" name="<?=COL_MONEVCAPAIAN?>" class="form-control" value="<?=!empty($data)?$data[COL_MONEVCAPAIAN]:''?>"  />
  </div>
  <div class="form-group">
    <label>CATATAN</label>
    <textarea name="<?=COL_MONEVKETERANGAN?>" class="form-control"><?=!empty($data)?$data[COL_MONEVKETERANGAN]:''?></textarea>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
  <button type="submit" class="btn btn-outline-primary btn-ok"><i class="far fa-arrow-circle-right"></i>&nbsp;LANJUT</button>
</div>
</form>
