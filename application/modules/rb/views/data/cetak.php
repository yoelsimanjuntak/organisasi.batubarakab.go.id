<html>
<head>
  <title><?=$title?></title>
  <style>
  body {
    font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
  }
  table {
    width: 100%;
    border-collapse: collapse;
    margin-bottom: 0 !important;
  }
  table, th, td {
    border: 1px solid black;
  }
  th, td {
    padding: 5px;
  }
  </style>
</head>
<body>
  <table width="100%" style="border: 0 !important">
    <tr>
      <td colspan="2" style="text-align: center; vertical-align: middle; border: 0 !important">
        <h4>RENCANA KERJA REFORMASI BIROKRASI</h4>
      </td>
    </tr>
  </table>
  <hr />
  <br />
  <?php
  if ($data[COL_NMTYPE]=='INSTANSI') {
    ?>
    <table width="100%" style="border: 0 !important; font-size: 10pt !important">
      <tr>
        <td style="vertical-align: top; width: 100px; white-space: nowrap; border: 0 !important">TAHUN</td>
        <td style="vertical-align: top; width: 10px; border: 0 !important">:</td>
        <td style="vertical-align: top; font-weight: bold; border: 0 !important"><?=$data[COL_TAHUN]?></td>
      </tr>
      <tr>
        <td style="vertical-align: top; width: 100px; white-space: nowrap; border: 0 !important">JUDUL</td>
        <td style="vertical-align: top; width: 10px; border: 0 !important">:</td>
        <td style="vertical-align: top; font-weight: bold; border: 0 !important"><?=$data[COL_NMKETERANGAN]?></td>
      </tr>
    </table>
    <?php
  } else {
    ?>
    <table width="100%" style="border: 0 !important; font-size: 10pt !important">
      <tr>
        <td style="vertical-align: top; width: 200px; white-space: nowrap; border: 0 !important">TAHUN</td>
        <td style="vertical-align: top; width: 10px; border: 0 !important">:</td>
        <td style="vertical-align: top; font-weight: bold; border: 0 !important"><?=$data[COL_TAHUN]?></td>
      </tr>
      <tr>
        <td style="vertical-align: top; width: 200px; white-space: nowrap; border: 0 !important">UNIT KERJA</td>
        <td style="vertical-align: top; width: 10px; border: 0 !important">:</td>
        <td style="vertical-align: top; font-weight: bold; border: 0 !important"><?=!empty($data[COL_SKPDNAMA])?$data[COL_SKPDNAMA]:$this->setting_org_name?></td>
      </tr>
    </table>
    <?php
  }
  ?>
  <br />
  <table width="100%" border="1" style="font-size: 10pt !important; margin-bottom: 10px">
    <?php
    $no=1;
    foreach($det as $d) {
      $rtahap = $this->db
      ->where(COL_IDPERUBAHAN, $d[COL_UNIQ])
      ->get(TBL_RB_RENJADET)
      ->result_array();
      ?>
      <tr>
        <td style="font-weight: bold" colspan="7"><?=$no.'. '.$d[COL_NMPERUBAHAN]?></td>
      </tr>
      <tr>
        <td style="vertical-align: top; width: 10px; white-space: nowrap; font-size: 8pt; font-weight: bold; font-style: italic">NO.</td>
        <td style="vertical-align: top; font-size: 8pt; font-weight: bold; font-style: italic">RENCANA AKSI</td>
        <td style="vertical-align: top; width: 200px; font-size: 8pt; font-weight: bold; font-style: italic">INDIKATOR</td>
        <td style="vertical-align: top; width: 50px; font-size: 8pt; font-weight: bold; font-style: italic">OUTPUT</td>
        <td style="vertical-align: top; width: 10px; white-space: nowrap;  font-size: 8pt; font-weight: bold; font-style: italic">TARGET</td>
        <td style="vertical-align: top; width: 10px; white-space: nowrap;  font-size: 8pt; font-weight: bold; font-style: italic">SATUAN</td>
        <td style="vertical-align: top; font-size: 8pt; font-weight: bold; font-style: italic">PELAKSANAAN (BULAN)</td>
      </tr>
      <?php
      $not=1;
      foreach ($rtahap as $t) {
        $period = explode(",", $t[COL_PERIODTARGET]);
        $period = implode(", ", $period);
        ?>
        <tr>
          <td style="vertical-align: top; width: 10px; white-space: nowrap; font-size: 8pt; text-align: right; font-style: italic"><?=$not.'.'?></td>
          <td style="vertical-align: top; font-size: 8pt; font-style: italic;"><?=$t[COL_NMTAHAPAN]?></td>
          <td style="vertical-align: top; font-size: 8pt; font-style: italic;"><?=$t[COL_NMINDIKATOR]?></td>
          <td style="vertical-align: top; font-size: 8pt; font-style: italic;"><?=$t[COL_NMOUTPUT]?></td>
          <td style="white-space: nowrap; vertical-align: top; font-size: 8pt; text-align: right; font-style: italic"><?=$t[COL_NMTARGET]?></td>
          <td style="white-space: nowrap; vertical-align: top; font-size: 8pt; font-style: italic;"><?=strtoupper($t[COL_NMSATUAN])?></td>
          <td style="vertical-align: top; font-size: 8pt; font-style: italic;"><?=$period?></td>
        </tr>
        <?php
        $not++;
      }
      $no++;
    }
    ?>
  </table>
</body>
</html>
