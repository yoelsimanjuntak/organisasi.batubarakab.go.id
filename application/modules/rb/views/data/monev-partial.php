<?php
$arrKegiatan = array();
if(!empty($rdata[COL_NMKEGIATAN])) {
  $arrKegiatan = explode(";", $rdata[COL_NMKEGIATAN]);
}

$rtahapan = $this->db
->where(COL_IDRENJA, $rdata[COL_IDRENJA])
->where(COL_IDPERUBAHAN, $rdata[COL_UNIQ])
->get(TBL_RB_RENJADET)
->result_array();
?>
<div class="row">
  <div class="col-sm-12">
    <table class="table table-bordered tbl-det mb-0" style="border-right: none !important; border-left: none !important; width: 100%">
      <thead class="text-sm">
        <tr>
          <!--<th style="width: 10px; white-space: nowrap">#</th>-->
          <th>RENCANA AKSI</th>
          <th>OUTPUT</th>
          <th style="width: 10px; white-space: nowrap">SATUAN</th>
          <th style="width: 10px; white-space: nowrap">TARGET</th>
          <th style="width: 10px; white-space: nowrap">CAPAIAN</th>
          <th>CATATAN</th>
          <th style="width: 10px; white-space: nowrap">AKSI</th>
        </tr>
      </thead>
      <tbody class="text-sm">
        <?php
        if(!empty($rtahapan)) {
          foreach($rtahapan as $r) {
            $txtPeriod = $r[COL_PERIODTARGET];
            $arrPeriod = array();
            if(!empty($txtPeriod)) {
              $arrPeriod = explode(",", $txtPeriod);
            }

            $rmonev = $this->db
            ->where(COL_IDTAHAPAN, $r[COL_UNIQ])
            ->where(COL_MONEVPERIOD, $period)
            ->get(TBL_RB_RENJAMONEV)
            ->row_array();

            if(!in_array($period, $arrPeriod)) continue;

            ?>
            <tr <?=!in_array($period, $arrPeriod)?'class="text-muted"':''?>>
              <!--<td style="text-align: center; vertical-align: middle"><?=in_array($period, $arrPeriod)?'<i class="far fa-check-circle"></i>':'<i class="far fa-times-circle"></i>'?></td>-->
              <td><?=$r[COL_NMTAHAPAN]?></td>
              <td><?=$r[COL_NMOUTPUT]?></td>
              <td style="text-align: left; vertical-align: middle; white-space: nowrap"><?=!empty($r[COL_NMSATUAN])?strtoupper($r[COL_NMSATUAN]):'-'?></td>
              <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=!empty($r[COL_NMTARGET])?$r[COL_NMTARGET]:'-'?></td>
              <td style="text-align: center; vertical-align: middle">
                <?=!empty($rmonev)?$rmonev[COL_MONEVCAPAIAN]:'-'?>
              </td>
              <td style="text-align: left; vertical-align: middle">
                <?=!empty($rmonev)?$rmonev[COL_MONEVKETERANGAN]:'-'?>
              </td>
              <td style="text-align: center; vertical-align: middle; white-space: nowrap">
                <a href="<?=site_url('rb/data/monev-form/'.$r[COL_UNIQ].'/'.$period)?>" class="btn btn-xs btn-outline-primary btn-entry"><i class="far fa-pencil"></i>&nbsp;ENTRI</a>
              </td>
            </tr>
            <?php
          }
        } else {
          ?>

          <?php
        }
        ?>
      </tbody>
    </table>
  </div>
</div>
