<?php
class Data extends MY_Controller {
  function __construct() {
      parent::__construct();
  }

  public function index($tipe='unit') {
    $ruser = GetLoggedUser();
    $data['title'] = 'Renja RB '.strtoupper($tipe);
    $data['tipe'] = $tipe;
    $this->template->load('main', 'rb/data/index', $data);
  }

  public function index_load($tipe='unit') {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $tahun = !empty($_POST['Tahun'])?$_POST['Tahun']:null;
    $stat = !empty($_POST['Status'])?$_POST['Status']:null;
    /*$IdSupplier = !empty($_POST['idSupplier'])?$_POST['idSupplier']:null;
    $dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    $dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');*/

    $ruser = GetLoggedUser();
    $orderdef = array(COL_TAHUN=>'desc');
    $orderables = array(null,COL_TAHUN,COL_NMTYPE,null,COL_CREATEDON);
    $cols = array(COL_TAHUN,COL_NMTYPE,COL_NMKETERANGAN);

    if($tipe=='unit') {
      $orderables = array(null,COL_TAHUN,COL_NMTYPE,COL_SKPDNAMA,null,COL_CREATEDON);
      $cols = array(COL_TAHUN,COL_NMTYPE,COL_SKPDNAMA,COL_NMKETERANGAN);

      $this->db->join(TBL_SAKIPV2_SKPD.' opd','opd.'.COL_SKPDID." = ".TBL_RB_RENJA.".".COL_SKPDID,"left");

      if($ruser[COL_ROLEID]!=ROLEADMIN&&$ruser[COL_ROLEID]!=ROLEGUEST) {
        $this->db->where(TBL_RB_RENJA.'.'.COL_SKPDID, $ruser[COL_SKPDID]);
      }
    }


    $queryAll = $this->db
    ->where(COL_NMTYPE, $tipe)
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_RB_RENJA.".".COL_CREATEDBY,"left")
    ->get(TBL_RB_RENJA);

    $i = 0;
    foreach($cols as $item){
      if($item == COL_CREATEDBY) $item = TBL_RB_RENJA.'.'.COL_CREATEDBY;
      else if($item == COL_SKPDNAMA) $item = TBL_SAKIPV2_SKPD.'.'.COL_SKPDNAMA;
      else if($item == COL_SKPDID) $item = TBL_SAKIPV2_SKPD.'.'.COL_SKPDID;
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($tahun)) {
      $this->db->where(TBL_RB_RENJA.'.'.COL_TAHUN, $tahun);
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
        $order = $orderdef;
        $this->db->order_by(key($order), $order[key($order)]);
    }

    if($tipe=='unit') {
      $this->db->select('rb_renja.*, opd.SkpdNama, uc.Name as Nm_CreatedBy');
      $this->db->join(TBL_SAKIPV2_SKPD.' opd','opd.'.COL_SKPDID." = ".TBL_RB_RENJA.".".COL_SKPDID,"left");

      if($ruser[COL_ROLEID]!=ROLEADMIN&&$ruser[COL_ROLEID]!=ROLEGUEST) {
        $this->db->where(TBL_RB_RENJA.'.'.COL_SKPDID, $ruser[COL_SKPDID]);
      }
    } else {
      $this->db->select('rb_renja.*, uc.Name as Nm_CreatedBy');
    }

    $q = $this->db
    ->select('rb_renja.*, uc.UserName')
    ->where(COL_NMTYPE, $tipe)
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_RB_RENJA.".".COL_CREATEDBY,"left")
    ->join(TBL_SAKIPV2_SKPD,TBL_SAKIPV2_SKPD.'.'.COL_SKPDID." = ".TBL_RB_RENJA.".".COL_SKPDID,"left")
    ->order_by(TBL_RB_RENJA.".".COL_TAHUN, 'desc')
    ->order_by(TBL_RB_RENJA.".".COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL_RB_RENJA, FALSE);
    //echo $q;
    //exit();
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      if($tipe=='unit') {
        $link = '-';
        if($ruser[COL_ROLEID] != ROLEGUEST) {
          $link = @
          '<a href="'.site_url('rb/data/delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-danger btn-action"><i class="fas fa-trash"></i>&nbsp;HAPUS</a>&nbsp;'.
          '<a href="'.site_url('rb/data/form/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-primary"><i class="fas fa-file-edit"></i>&nbsp;RINCIAN</a>&nbsp;'.
          '<a href="'.site_url('rb/data/monev/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-info"><i class="fas fa-file-check"></i>&nbsp;MONEV</a>&nbsp;'.
          '<a href="'.site_url('rb/data/cetak/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-success" target="_blank"><i class="fas fa-print"></i>&nbsp;CETAK</a>';
        } else {
          $link = @
          '<a href="'.site_url('rb/data/form/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-primary"><i class="fas fa-search"></i>&nbsp;RINCIAN</a>&nbsp;'.
          '<a href="'.site_url('rb/data/cetak/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-success" target="_blank"><i class="fas fa-print"></i>&nbsp;CETAK</a>';
        }
        $data[] = array(
          $link,
          $r[COL_TAHUN],
          $r[COL_NMTYPE],
          $r[COL_SKPDNAMA],
          $r[COL_NMKETERANGAN],
          date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
        );
      } else {
        $link = '-';
        if($ruser[COL_ROLEID] != ROLEGUEST) {
          $link = @
          '<a href="'.site_url('rb/data/delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-danger btn-action"><i class="fas fa-trash"></i>&nbsp;HAPUS</a>&nbsp;'.
          '<a href="'.site_url('rb/data/form/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-primary"><i class="fas fa-file-edit"></i>&nbsp;RINCIAN</a>&nbsp;'.
          '<a href="'.site_url('rb/data/monev/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-info"><i class="fas fa-file-check"></i>&nbsp;MONEV</a>&nbsp;'.
          '<a href="'.site_url('rb/data/cetak/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-success" target="_blank"><i class="fas fa-print"></i>&nbsp;CETAK</a>';
        } else {
          $link = @
          '<a href="'.site_url('rb/data/form/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-primary"><i class="fas fa-search"></i>&nbsp;RINCIAN</a>&nbsp;'.
          '<a href="'.site_url('rb/data/cetak/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-success" target="_blank"><i class="fas fa-print"></i>&nbsp;CETAK</a>';
        }
        $data[] = array(
          $link,
          $r[COL_TAHUN],
          $r[COL_NMTYPE],
          $r[COL_NMKETERANGAN],
          date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
        );
      }

    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function add($tipe='unit') {
    $ruser = GetLoggedUser();
    $ropd = array();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      $ropd = $this->db
      ->where(COL_SKPDID, $ruser[COL_SKPDID])
      ->get(TBL_SAKIPV2_SKPD)
      ->row_array();
    }

    $data['ropd'] = $ropd;
    $data['tipe'] = $tipe;

    if(!empty($_POST)) {
      $rperubahan = $this->db
      ->get(TBL_RB_MPERUBAHAN)
      ->result_array();
      if(empty($rperubahan)) {
        ShowJsonError('Parameter tidak valid!');
        exit();
      }

      $det = array();
      $dat = array(
        COL_NMKETERANGAN=>$this->input->post(COL_NMKETERANGAN),
        COL_NMTYPE=>$tipe,
        COL_TAHUN=>$this->input->post(COL_TAHUN),
        COL_SKPDID=>$this->input->post(COL_SKPDID),

        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );


      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_RB_RENJA, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        $renjaId = $this->db->insert_id();
        foreach($rperubahan as $d) {
          $det[] = array(
            COL_IDRENJA=>$renjaId,
            COL_NMPERUBAHAN=>$d[COL_NMPERUBAHAN],
            COL_NMINDIKATOR=>$d[COL_NMINDIKATOR],
            COL_NMKEGIATAN=>$d[COL_NMKEGIATAN]
          );
        }

        $res = $this->db->insert_batch(TBL_RB_RENJAPERUBAHAN, $det);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('LKE berhasil ditambahkan.');

      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }

    } else {
      $this->load->view('rb/data/add', $data);
    }

  }

  public function detail_add($id) {
    $dat['mode'] = 'add';
    $data = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_RB_RENJAPERUBAHAN)
    ->row_array();

    if(empty($data)) {
      show_error('Parameter tidak valid');
      exit();
    }

    if(!empty($_POST)) {
      $period = "";
      if(!empty($_POST['Period'])) {
        $period = implode(",", $this->input->post('Period'));
      }

      $dat_ = array(
        COL_IDRENJA=>$data[COL_IDRENJA],
        COL_IDPERUBAHAN=>$id,
        COL_NMTAHAPAN=>$this->input->post(COL_NMTAHAPAN),
        COL_NMINDIKATOR=>$this->input->post(COL_NMINDIKATOR),
        COL_NMOUTPUT=>$this->input->post(COL_NMOUTPUT),
        COL_NMTARGET=>$this->input->post(COL_NMTARGET),
        COL_NMSATUAN=>$this->input->post(COL_NMSATUAN),
        COL_PERIODTARGET=>$period
      );

      $res = $this->db->insert(TBL_RB_RENJADET, $dat_);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('RENCANA AKSI BERHASIL DITAMBAHKAN!');
      exit();
    } else {
      $this->load->view('rb/data/form-detail', $dat);
    }
  }

  public function detail_edit($id) {
    $dat['mode'] = 'edit';
    $data = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_RB_RENJADET)
    ->row_array();

    if(empty($data)) {
      show_error('Parameter tidak valid');
      exit();
    }

    $dat['data'] = $data;

    if(!empty($_POST)) {
      $period = "";
      if(!empty($_POST['Period'])) {
        $period = implode(",", $this->input->post('Period'));
      }

      $dat_ = array(
        COL_NMTAHAPAN=>$this->input->post(COL_NMTAHAPAN),
        COL_NMINDIKATOR=>$this->input->post(COL_NMINDIKATOR),
        COL_NMOUTPUT=>$this->input->post(COL_NMOUTPUT),
        COL_NMTARGET=>$this->input->post(COL_NMTARGET),
        COL_NMSATUAN=>$this->input->post(COL_NMSATUAN),
        COL_PERIODTARGET=>$period
      );

      $res = $this->db->where(COL_UNIQ, $id)->update(TBL_RB_RENJADET, $dat_);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('RENCANA AKSI BERHASIL DIPERBARUI!');
      exit();
    } else {
      $this->load->view('rb/data/form-detail', $dat);
    }
  }

  public function detail_delete($id) {
    $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_RB_RENJADET);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('RENCANA AKSI BERHASIL DIHAPUS!');
    exit();
  }

  public function detail_load($id){
    $ruser = GetLoggedUser();
    $rdet = $this->db
    ->where(COL_IDPERUBAHAN, $id)
    ->get(TBL_RB_RENJADET)
    ->result_array();

    $row = '';
    if(empty($rdet)) {
      echo '<tr><td colspan="18" class="text-center">BELUM ADA DATA</td></tr>';
      exit();
    }

    foreach($rdet as $r) {
      $period = explode(",", $r[COL_PERIODTARGET]);
      $period_ = '';
      for($i=1; $i<=12; $i++) {
        if(in_array($i, $period)) {
          $period_ .= '<td style="white-space: nowrap; vertical-align: middle; text-align: center; padding-left: .75rem; padding-right: .75rem"><i class="far fa-check-circle"></i></td>';
        }else {
          $period_ .= '<td style="white-space: nowrap; vertical-align: middle; text-align: center; padding-left: .75rem; padding-right: .75rem">-</td>';
        }
      }
      $opt = "<td style='width: 10px; white-space: nowrap; vertical-align: middle; text-align: center; padding-left: .75rem !important; padding-right: .75rem !important'></td>";
      if($ruser[COL_ROLEID]!=ROLEGUEST) {
        $opt = @"
        <td style='width: 10px; white-space: nowrap; vertical-align: middle; text-align: center; padding-left: .75rem !important; padding-right: .75rem !important'>
          <button type='button' class='btn btn-xs btn-primary btn-edit-detail' data-url='".site_url('rb/data/detail-edit/'.$r[COL_UNIQ])."'><i class='far fa-pencil'></i></button>&nbsp;
          <button type='button' class='btn btn-xs btn-danger btn-delete-detail' data-url='".site_url('rb/data/detail-delete/'.$r[COL_UNIQ])."'><i class='far fa-trash'></i></button>
        </td>
        ";
      }
      $row .= @"
      <tr>
        <td style='vertical-align: middle;'>".$r[COL_NMTAHAPAN]."</td>
        <td style='vertical-align: middle;'>".$r[COL_NMINDIKATOR]."</td>
        <td style='vertical-align: middle;'>".$r[COL_NMOUTPUT]."</td>
        <td style='white-space: nowrap; vertical-align: middle;' class='text-right'>".$r[COL_NMTARGET]."</td>
        <td style='white-space: nowrap; vertical-align: middle;'>".strtoupper($r[COL_NMSATUAN])."</td>
        $period_.$opt
      </tr>
      ";
    }
    echo $row;
  }

  public function form($id) {
    $ruser = GetLoggedUser();
    $data['title'] = 'Form Renja RB';
    $rdata = $this->db
    ->join(TBL_SAKIPV2_SKPD.' opd','opd.'.COL_SKPDID." = ".TBL_RB_RENJA.".".COL_SKPDID,"left")
    ->where(COL_UNIQ, $id)
    ->get(TBL_RB_RENJA)
    ->row_array();

    if(!empty($_POST)) {
      $idPerubahan = $this->input->post(COL_IDPERUBAHAN);
      $arrTahapan = $this->input->post('ArrTahapan');

      $datTahapan = array();
      $dat = array(
        COL_NMOUTPUT=>$this->input->post(COL_NMOUTPUT),
        COL_NMPENANGGUNGJAWAB=>$this->input->post(COL_NMPENANGGUNGJAWAB),
        COL_NMKRITERIA=>$this->input->post(COL_NMKRITERIA)
      );

      if($arrTahapan) {
        $arrTahapan = json_decode($arrTahapan);
        if(is_array($arrTahapan)) {
          foreach($arrTahapan as $r) {
            $datTahapan[] = array(
              COL_IDRENJA=>$id,
              COL_IDPERUBAHAN=>$idPerubahan,
              COL_NMTAHAPAN=>$r->Uraian,
              COL_NMTARGET=>$r->Target,
              COL_NMSATUAN=>$r->Satuan,
              COL_PERIODTARGET=>$r->Period
            );
          }
        }
      }

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_IDRENJA, $id)->where(COL_UNIQ, $idPerubahan)->update(TBL_RB_RENJAPERUBAHAN, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        $res = $this->db->where(COL_IDRENJA, $id)->where(COL_IDPERUBAHAN, $idPerubahan)->delete(TBL_RB_RENJADET);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        if(!empty($datTahapan)) {
          $res = $this->db->insert_batch(TBL_RB_RENJADET, $datTahapan);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Data berhasil diperbarui.');

      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }
    } else {
      if(empty($rdata)) {
        show_error('Parameter tidak valid!');
        exit();
      }
      $data['title'] = 'Form Renja RB '.$rdata[COL_NMTYPE].' TA. '.$rdata[COL_TAHUN].' <small class="text-sm font-weight-bold" style="font-size: 14pt">'.(!empty($rdata[COL_SKPDNAMA])?' '.$rdata[COL_SKPDNAMA]:'').'</small>';
      $data['data'] = $rdata;
      $data['id'] = $id;
      $this->template->load('main', 'rb/data/form', $data);
    }
  }

  public function monev($id) {
    $ruser = GetLoggedUser();
    $data['title'] = 'Form Monev RB';
    $rdata = $this->db
    ->join(TBL_SAKIPV2_SKPD.' opd','opd.'.COL_SKPDID." = ".TBL_RB_RENJA.".".COL_SKPDID,"left")
    ->where(COL_UNIQ, $id)
    ->get(TBL_RB_RENJA)
    ->row_array();

    if(!empty($_POST)) {

    } else {
      if(empty($rdata)) {
        show_error('Parameter tidak valid!');
        exit();
      }
      $period = $this->input->get('Period');
      if(empty($period)) {
        redirect(current_url().'?Period=1');
      }

      $data['title'] = 'Form Monev RB '.$rdata[COL_NMTYPE].' TA. '.$rdata[COL_TAHUN].' <small class="text-sm font-weight-bold" style="font-size: 14pt">'.(!empty($rdata[COL_SKPDNAMA])?' '.$rdata[COL_SKPDNAMA]:'').'</small>';
      $data['data'] = $rdata;
      $data['id'] = $id;
      $this->template->load('main', 'rb/data/monev', $data);
    }
  }

  public function monev_partial($id, $period) {
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_RB_RENJAPERUBAHAN)->row_array();
    $this->load->view('rb/data/monev-partial', array('rdata'=>$rdata, 'period'=>$period));
  }

  public function monev_form($id, $period) {
    $rtahapan = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_RB_RENJADET)
    ->row_array();

    $data = $this->db
    ->where(COL_IDTAHAPAN, $id)
    ->where(COL_MONEVPERIOD, $period)
    ->get(TBL_RB_RENJAMONEV)
    ->row_array();
    if(!empty($_POST)) {
      $res = $this->db
      ->where(COL_IDTAHAPAN, $id)
      ->where(COL_MONEVPERIOD, $period)
      ->delete(TBL_RB_RENJAMONEV);

      $res = $this->db->insert(TBL_RB_RENJAMONEV, array(
        COL_IDRENJA=>$rtahapan[COL_IDRENJA],
        COL_IDTAHAPAN=>$id,
        COL_MONEVPERIOD=>$period,
        COL_MONEVKETERANGAN=>$this->input->post(COL_MONEVKETERANGAN),
        COL_MONEVCAPAIAN=>$this->input->post(COL_MONEVCAPAIAN)
      ));
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('Entri data monev berhasil!');
      exit();
    } else {
      $this->load->view('rb/data/monev-form', array('rtahapan'=>$rtahapan, 'data'=>$data));
    }
  }

  public function delete($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_RB_RENJA)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_RB_RENJA);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('BERHASIL DIHAPUS');
  }

  public function cetak($id) {
    $rdata = $this->db
    ->join(TBL_SAKIPV2_SKPD.' opd','opd.'.COL_SKPDID." = ".TBL_RB_RENJA.".".COL_SKPDID,"left")
    ->where(COL_UNIQ, $id)
    ->get(TBL_RB_RENJA)
    ->row_array();

    if(empty($rdata)) {
      show_error('Parameter tidak valid!');
      exit();
    }

    $rdet = $this->db
    ->where(COL_IDRENJA, $rdata[COL_UNIQ])
    ->get(TBL_RB_RENJAPERUBAHAN)
    ->result_array();

    $title = (!empty($rdata[COL_SKPDNAMA])?'RENCANA AKSI '.$rdata[COL_SKPDNAMA].' TAHUN '.$rdata[COL_TAHUN]:strtoupper($rdata[COL_NMKETERANGAN]).' TAHUN '.$rdata[COL_TAHUN]);

    $this->load->library('Mypdf');
    $mpdf = new Mypdf('','A4-L');

    $html = $this->load->view('data/cetak', array('data'=>$rdata, 'det'=>$rdet, 'title'=>$title), TRUE);
    //echo $html;
    //exit();
    $mpdf->pdf->use_kwt = true;
    $mpdf->pdf->SetTitle($title);
    $mpdf->pdf->SetFooter('Dicetak melalui aplikasi '.$this->setting_web_name);
    $mpdf->pdf->SetWatermarkImage(MY_IMAGEURL.$this->setting_web_logo, 0.1, array(100,100));
    $mpdf->pdf->showWatermarkImage = true;
    $mpdf->pdf->WriteHTML($html);
    $mpdf->pdf->Output($title.'.pdf', 'I');
  }
}
?>
