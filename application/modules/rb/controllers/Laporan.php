<?php
class Laporan extends MY_Controller {
  function __construct() {
      parent::__construct();
  }

  public function index($tipe) {
    $ruser = GetLoggedUser();
    $data['title'] = 'Laporan Monitoring dan Evaluasi RB Tahunan';
    $data['tipe'] = $tipe;
    $this->template->load('main', 'rb/laporan/index', $data);
  }
}
