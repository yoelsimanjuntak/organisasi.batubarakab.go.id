<form id="form-kegiatan" action="<?=current_url()?>">
  <div class="form-group">
    <label>KODE</label>
    <div class="row">
      <div class="col-lg-5">
        <input type="text" class="form-control" name="<?=COL_KEGIATANKODE?>" placeholder="KODE NOMENKLATUR KEGIATAN" value="<?=!empty($data)?$data[COL_KEGIATANKODE]:''?>" required />
      </div>
    </div>
  </div>
  <div class="form-group">
    <label>NOMENKLATUR</label>
    <textarea class="form-control" name="<?=COL_KEGIATANURAIAN?>" placeholder="NOMENKLATUR KEGIATAN" required><?=!empty($data)?$data[COL_KEGIATANURAIAN]:''?></textarea>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function(){
  $('#form-kegiatan').validate({
    submitHandler: function(form) {
      var modal = $(form).closest('modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>
