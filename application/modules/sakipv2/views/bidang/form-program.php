<form id="form-program" action="<?=current_url()?>">
  <input type="hidden" name="<?=COL_IDBID?>" value="<?=!empty($def[COL_IDBID])?$def[COL_IDBID]:(!empty($data[COL_IDBID])?$data[COL_IDBID]:'')?>" />
  <input type="hidden" name="<?=COL_IDDPA?>" value="<?=!empty($def[COL_IDDPA])?$def[COL_IDDPA]:(!empty($data[COL_IDDPA])?$data[COL_IDDPA]:'')?>" />
  <input type="hidden" name="<?=COL_IDSASARANSKPD?>" value="<?=!empty($def[COL_IDSASARANSKPD])?$def[COL_IDSASARANSKPD]:(!empty($data[COL_IDSASARANSKPD])?$data[COL_IDSASARANSKPD]:'')?>" />
  <div class="form-group">
    <label>KODE</label>
    <div class="row">
      <div class="col-lg-5">
        <input type="text" class="form-control" name="<?=COL_PROGRAMKODE?>" placeholder="KODE NOMENKLATUR PROGRAM" value="<?=!empty($data)?$data[COL_PROGRAMKODE]:''?>" required />
      </div>
    </div>
  </div>
  <div class="form-group">
    <label>NOMENKLATUR</label>
    <textarea class="form-control" name="<?=COL_PROGRAMURAIAN?>" placeholder="NOMENKLATUR PROGRAM" required><?=!empty($data)?$data[COL_PROGRAMURAIAN]:''?></textarea>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function(){
  $('#form-program').validate({
    submitHandler: function(form) {
      var modal = $(form).closest('modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>
