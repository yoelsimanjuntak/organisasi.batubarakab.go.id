<form id="form-bidang" action="<?=current_url()?>">
  <div class="form-group">
    <label>NAMA BIDANG / BAGIAN</label>
    <input type="text" class="form-control" placeholder="NAMA BIDANG / BAGIAN" name="<?=COL_BIDNAMA?>" value="<?=!empty($data)?$data[COL_BIDNAMA]:''?>" />
  </div>
  <div class="form-group">
    <label>NAMA PIMPINAN</label>
    <input type="text" class="form-control" name="<?=COL_BIDNAMAPIMPINAN?>" placeholder="NAMA PIMPINAN BIDANG" value="<?=!empty($data)?$data[COL_BIDNAMAPIMPINAN]:''?>" required />
  </div>
  <div class="form-group">
    <label>NAMA JABATAN</label>
    <input type="text" class="form-control" name="<?=COL_BIDNAMAJABATAN?>" placeholder="NAMA JABATAN PIMPINAN" value="<?=!empty($data)?$data[COL_BIDNAMAJABATAN]:''?>" required />
  </div>
</form>
<script type="text/javascript">
$(document).ready(function(){
  $('#form-bidang').validate({
    submitHandler: function(form) {
      var modal = $(form).closest('modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });

      return false;
    }
  });
});
</script>
