<?php

$rOptBidang = $this->db
->where(COL_IDRENSTRA, $rbidang[COL_IDRENSTRA])
->order_by(COL_BIDISAKTIF,'desc')
->order_by(COL_BIDNAMA,'asc')
->get(TBL_SAKIPV2_BID)
->result_array();

$rOptDpa = $this->db
->where(COL_IDRENSTRA, $rbidang[COL_IDRENSTRA])
->order_by(COL_DPAISAKTIF, 'desc')
->order_by(COL_DPATAHUN,'desc')
->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
->result_array();

$getDPA = null;
if(!empty($_GET['idDPA'])) {
  $getDPA = $_GET['idDPA'];
} else if(!empty($rOptDpa) && $rOptDpa[0][COL_DPAISAKTIF]==1) {
  $getDPA = $rOptDpa[0][COL_DPAID];
}

$rOptSasaranSKPD = $this->db
->join(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN,TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.".".COL_IDTUJUAN,"left")
->where(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_IDRENSTRA, $rbidang[COL_IDRENSTRA])
->order_by(COL_SASARANNO, 'asc')
->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN)
->result_array();

$getSasaran = null;
if(!empty($_GET['idSasaran'])) {
  $getSasaran = $_GET['idSasaran'];
} else if(!empty($rOptSasaranSKPD)) {
  $getSasaran = $rOptSasaranSKPD[0][COL_SASARANID];
}

$rProgram = array();
if(!empty($getSasaran) && !empty($getDPA)) {
  $rProgram = $this->db
  ->where(COL_IDBID, $rbidang[COL_BIDID])
  ->where(COL_IDSASARANSKPD, $getSasaran)
  ->where(COL_IDDPA, $getDPA)
  ->order_by(COL_PROGRAMKODE, 'asc')
  ->get(TBL_SAKIPV2_BID_PROGRAM)
  ->result_array();
}

$arrFungsi = array();
$arrIKU = array();
if(!empty($rbidang[COL_BIDFUNGSI])) $arrFungsi = json_decode($rbidang[COL_BIDFUNGSI]);
if(!empty($rbidang[COL_BIDIKU])) $arrIKU = json_decode($rbidang[COL_BIDIKU]);
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?=$title?></h3>
      </div>
      <?php
      if(!empty($navs)) {
        ?>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <?php
            foreach($navs as $n) {
              if(!empty($n['link'])) {
                ?>
                <li class="breadcrumb-item"><a href="<?=$n['link']?>"><?=$n['text']?></a></li>
                <?php
              } else {
                ?>
                <li class="breadcrumb-item active"><?=$n['text']?></li>
                <?php
              }
            }
            ?>
          </ol>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-outline card-secondary">
          <?php
          if(!empty($subtitle)) {
            ?>
            <div class="card-header">
              <h4 class="card-title"><?=$subtitle?></h4>
            </div>
            <?php
          }
          ?>
          <div class="card-body p-0">
            <table class="table">
              <tbody>
                <tr>
                  <td class="text-right" style="width: 150px; white-space: nowrap; vertical-align: middle">NOMENKLATUR</td>
                  <td style="width: 10px; white-space: nowrap; vertical-align: middle">:</td>
                  <td>
                    <select class="form-control" name="filterBidang">
                      <?php
                      foreach($rOptBidang as $opt) {
                        ?>
                        <option value="<?=site_url('sakipv2/bidang/index').'?opr=detail-bidang&id='.$opt[COL_BIDID]?>" <?=$opt[COL_BIDID]==$rbidang[COL_BIDID]?'selected':''?>>
                          <?=strtoupper($opt[COL_BIDNAMA])?>
                        </option>
                        <?php
                      }
                      ?>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td class="text-right" style="width: 150px; white-space: nowrap;">TUGAS POKOK</td>
                  <td style="width: 10px; white-space: nowrap;">:</td>
                  <td class="font-weight-bold font-italic">
                    <?=!empty($rbidang[COL_BIDTUGASPOKOK])?$rbidang[COL_BIDTUGASPOKOK]:'(KOSONG)'?>
                  </td>
                </tr>
                <tr>
                  <td class="text-right" style="width: 150px; white-space: nowrap;">FUNGSI</td>
                  <td style="width: 10px; white-space: nowrap;">:</td>
                  <td class="font-weight-bold">
                    <?php
                    if(!empty($arrFungsi)) {
                      ?>
                      <ul class="todo-list ui-sortable mt-2" data-widget="todo-list">
                        <?php
                        foreach($arrFungsi as $r) {
                          ?>
                          <li>
                            <span class="text"><?=strtoupper($r)?></span>
                          </li>
                          <?php
                        }
                        ?>
                      </ul>
                      <?php
                    } else {
                      echo '<span class="font-italic">(KOSONG)</span>';
                    }
                    ?>
                  </td>
                </tr>
                <tr>
                  <td class="text-right" style="width: 150px; white-space: nowrap;">INDIKATOR KINERJA UTAMA</td>
                  <td style="width: 10px; white-space: nowrap;">:</td>
                  <td class="font-weight-bold">
                    <?php
                    if(!empty($arrIKU)) {
                      ?>
                      <ul class="todo-list ui-sortable mt-2" data-widget="todo-list">
                        <?php
                        $idx=0;
                        foreach($arrIKU as $r) {
                          ?>
                          <li>
                            <div class="row">
                              <div class="col-lg-10">
                                <span class="text"><?=strtoupper($r->Uraian)?></span>
                              </div>
                              <div class="col-lg-2">
                                <small class="font-italic">TARGET : <strong class="pull-right font-italic"><?=$r->Target.' ('.strtoupper($r->Satuan).')'?></strong></small>
                              </div>

                            </div>

                          </li>
                          <?php
                          $idx++;
                        }
                        ?>
                      </ul>
                      <?php
                    } else {
                      echo '<span class="font-italic">(KOSONG)</span>';
                    }
                    ?>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="card-footer d-block">
            <div class="row">
              <div class="col-lg-12 text-center">
                <button type="button" id="btnFormIKU" class="btn btn-primary font-weight-bold"><i class="far fa-edit"></i>&nbsp;UPDATE DATA</button>
              </div>
            </div>
          </div>
        </div>
        <?php
        if(!empty($getDPA)) {
          ?>
          <div class="card card-outline card-secondary">
            <div class="card-header">
              <h4 class="card-title">DAFTAR PROGRAM & KEGIATAN</h4>
            </div>
            <div class="card-body p-0">
              <table class="table table-bordered text-sm">
                <thead>
                  <tr>
                    <td colspan="5">
                      <div class="form-group row">
                        <label class="control-label col-lg-2">DPA SKPD</label>
                        <div class="col-lg-10">
                          <select class="form-control" name="filterDPA">
                            <?php
                            foreach($rOptDpa as $opt) {
                              ?>
                              <option value="<?=site_url('sakipv2/bidang/index').'?opr=detail-bidang&id='.$rbidang[COL_BIDID].'&idDPA='.$opt[COL_DPAID]?>" <?=$opt[COL_DPAID]==$getDPA?'selected':''?>>
                                <?=$opt[COL_DPATAHUN].' - '.strtoupper($opt[COL_DPAURAIAN])?>
                              </option>
                              <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="control-label col-lg-2">SASARAN SKPD</label>
                        <div class="col-lg-10">
                          <select class="form-control" name="filterDPA">
                            <?php
                            foreach($rOptSasaranSKPD as $opt) {
                              ?>
                              <option value="<?=site_url('sakipv2/bidang/index').'?opr=detail-bidang&id='.$rbidang[COL_BIDID].'&idDPA='.$getDPA.'&idSasaran='.$opt[COL_SASARANID]?>" <?=$opt[COL_SASARANID]==$getSasaran?'selected':''?>>
                                <?=$opt[COL_SASARANNO].'. '.strtoupper($opt[COL_SASARANURAIAN])?>
                              </option>
                              <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <tr>
                      <th>KODE</th>
                      <th>NOMENKLATUR</th>
                      <th style="white-space: nowrap">JLH. SASARAN</th>
                      <th>ANGGARAN</th>
                      <th style="white-space: nowrap; text-align: center !important">AKSI</th>
                    </tr>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $sumPaguProgram = 0;
                  if(!empty($rProgram)) {
                    foreach($rProgram as $r) {
                      $rSasaran = $this->db
                      ->where(COL_IDPROGRAM,$r[COL_PROGRAMID])
                      ->get(TBL_SAKIPV2_BID_PROGSASARAN)
                      ->result_array();

                      $rKegiatan = $this->db
                      ->where(COL_IDPROGRAM,$r[COL_PROGRAMID])
                      ->get(TBL_SAKIPV2_BID_KEGIATAN)
                      ->result_array();

                      $rPaguProgram = $this->db
                      ->join(TBL_SAKIPV2_BID_KEGIATAN,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANID." = ".TBL_SAKIPV2_SUBBID_SUBKEGIATAN.".".COL_IDKEGIATAN,"left")
                      ->where(COL_IDPROGRAM,$r[COL_PROGRAMID])
                      ->select_sum(COL_SUBKEGPAGU)
                      ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
                      ->row_array();

                      $sumPaguProgram += $rPaguProgram[COL_SUBKEGPAGU];
                      ?>
                      <tr>
                        <td style="width: 10px; white-space: nowrap"><?=$r[COL_PROGRAMKODE]?></td>
                        <td><?=strtoupper($r[COL_PROGRAMURAIAN])?></td>
                        <td class="text-right"><?=number_format(count($rSasaran))?></td>
                        <td class="text-right"><?=number_format($rPaguProgram[COL_SUBKEGPAGU])?></td>
                        <td style="white-space: nowrap">
                          <a href="<?=site_url('sakipv2/bidang/ajax-form-program/edit/'.$r[COL_PROGRAMID])?>" data-toggle="tooltip" data-placement="bottom" title="UBAH" class="btn btn-primary btn-xs btn-edit-program"><i class="far fa-edit"></i></a>
                          <a href="<?=site_url('sakipv2/bidang/ajax-change-program/delete/'.$r[COL_PROGRAMID])?>" data-toggle="tooltip" data-placement="bottom" title="HAPUS" data-prompt="PROGRAM tidak dapat dihapus jika masih terdapat data KEGIATAN terkait di dalamnya." class="btn btn-danger btn-xs btn-change-program"><i class="far fa-times-circle"></i></a>
                          <a href="<?=site_url('sakipv2/bidang/ajax-form-sasaran/sasaran-prog/'.$r[COL_PROGRAMID])?>" data-toggle="tooltip" data-placement="bottom" title="SASARAN PROGRAM" class="btn btn-success btn-xs btn-change-progsasaran"><i class="far fa-bullseye-arrow"></i></a>
                          <a href="<?=site_url('sakipv2/bidang/ajax-form-program/add-kegiatan/'.$r[COL_PROGRAMID])?>" data-toggle="tooltip" data-placement="bottom" title="TAMBAH KEGIATAN" class="btn btn-info btn-xs btn-edit-progkegiatan"><i class="far fa-plus-circle"></i></a>
                        </td>
                      </tr>
                      <?php
                      if(!empty($rKegiatan)) {
                        foreach($rKegiatan as $k) {
                          $rSasaranKeg = $this->db
                          ->where(COL_IDKEGIATAN,$k[COL_KEGIATANID])
                          ->get(TBL_SAKIPV2_BID_KEGSASARAN)
                          ->result_array();

                          $rpagu = $this->db
                          ->where(COL_IDKEGIATAN,$k[COL_KEGIATANID])
                          ->select_sum(COL_SUBKEGPAGU)
                          ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
                          ->row_array();
                          ?>
                          <tr class="text-success">
                            <td style="width: 10px; white-space: nowrap"><?=$k[COL_KEGIATANKODE]?></td>
                            <td class="pl-4"><?=strtoupper($k[COL_KEGIATANURAIAN])?></td>
                            <td class="text-right"><?=number_format(count($rSasaranKeg))?></td>
                            <td class="text-right"><?=number_format($rpagu[COL_SUBKEGPAGU])?></td>
                            <td style="white-space: nowrap;">
                              <a href="<?=site_url('sakipv2/bidang/ajax-form-program/edit-kegiatan/'.$k[COL_KEGIATANID])?>" data-toggle="tooltip" data-placement="bottom" title="UBAH" class="btn btn-primary btn-xs btn-edit-progkegiatan"><i class="far fa-edit"></i></a>
                              <a href="<?=site_url('sakipv2/bidang/ajax-change-program/delete-kegiatan/'.$k[COL_KEGIATANID])?>" data-toggle="tooltip" data-placement="bottom" title="HAPUS" data-prompt="KEGIATAN tidak dapat dihapus jika masih terdapat data SUB KEGIATAN terkait di dalamnya." class="btn btn-danger btn-xs btn-change-progkegiatan"><i class="far fa-times-circle"></i></a>
                              <a href="<?=site_url('sakipv2/bidang/ajax-form-sasaran/sasaran-keg/'.$k[COL_KEGIATANID])?>" data-toggle="tooltip" data-placement="bottom" title="SASARAN KEGIATAN" class="btn btn-success btn-xs btn-change-kegsasaran"><i class="far fa-bullseye-arrow"></i></a>
                            </td>
                          </tr>
                          <?php
                        }
                      } else {
                        ?>
                        <tr>
                          <td colspan="5">
                            <p class="text-center text-sm font-italic mb-0">(BELUM ADA KEGIATAN)</p>
                          </td>
                        </tr>
                        <?php
                      }
                    }
                  } else {
                    ?>
                    <tr>
                      <td colspan="5">
                        <p class="text-center font-italic mb-0">
                          BELUM ADA DATA
                        </p>
                      </td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
                <tfoot>
                  <tr>
                    <th colspan="3"><p class="text-right font-italic mb-0">TOTAL</p></th>
                    <th><p class="text-right font-italic mb-0"><?=number_format($sumPaguProgram)?></p></th>
                    <th></th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <div class="card-footer">
              <a href="<?=site_url('sakipv2/bidang/ajax-form-program/add/'.$rbidang[COL_BIDID].'/'.$getDPA.'/'.$getSasaran)?>" class="btn btn-primary btn-add-program font-weight-bold"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH PROGRAM</a>
            </div>
          </div>
          <?php
        }
        ?>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modalFormProgram" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">FORM PROGRAM SKPD</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalFormKegiatan" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">FORM KEGIATAN SKPD</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalFormSasaran" role="dialog">
  <div class="modal-dialog modal-lg" style="max-width: 1000px !important">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">DAFTAR SASARAN</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalFormIKU" role="dialog">
  <div class="modal-dialog modal-lg" style="max-width: 1000px !important">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">FORM TUPOKSI & IKU</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form-iku" action="<?=site_url('sakipv2/bidang/ajax-form-bidang/edit/'.$rbidang[COL_BIDID])?>" method="POST">
            <input type="hidden" name=<?=COL_BIDNAMA?> value="<?=$rbidang[COL_BIDNAMA]?>" />
            <input type="hidden" name=<?=COL_BIDNAMAPIMPINAN?> value="<?=$rbidang[COL_BIDNAMAPIMPINAN]?>" />
            <div class="form-group">
              <label>TUGAS POKOK</label>
              <textarea class="form-control" name="<?=COL_BIDTUGASPOKOK?>" placeholder="URAIAN TUGAS POKOK" required><?=$rbidang[COL_BIDTUGASPOKOK]?></textarea>
            </div>
            <div class="form-group">
              <label>FUNGSI</label>
              <table id="tbl-fungsi" class="table table-bordered">
                <thead class="bg-default">
                  <tr>
                    <th>URAIAN FUNGSI</th>
                    <th class="text-center" style="width: 10px; white-space: nowrap">
                      <button type="button" id="btn-add-fungsi" class="btn btn-sm btn-primary" style="font-weight: bold"><i class="far fa-plus-circle"></i></button>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(!empty($arrFungsi)) {
                    foreach($arrFungsi as $f) {
                      ?>
                      <tr>
                        <td>
                          <textarea class="form-control" name="FungsiUraian[]" placeholder="URAIAN FUNGSI"><?=$f?></textarea>
                        </td>
                        <td>
                          <button type="button" class="btn btn-sm btn-danger btn-del-fungsi" style="font-weight: bold"><i class="fa fa-minus"></i></button>
                        </td>
                      </tr>
                      <?php
                    }
                  } else {
                    ?>
                    <tr class="empty">
                      <td colspan="6">
                        <p class="font-italic mb-0 text-center">BELUM ADA DATA</p>
                      </td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>

            <div class="form-group">
              <label>INDIKATOR KINERJA UTAMA</label>
              <table id="tbl-iku" class="table table-bordered">
                <thead class="bg-default">
                  <tr>
                    <th>INDIKATOR</th>
                    <th>SUMBER DATA</th>
                    <th>FORMULASI</th>
                    <th style="width: 120px; white-space: nowrap">SATUAN</th>
                    <th style="width: 120px; white-space: nowrap">TARGET</th>
                    <th class="text-center" style="width: 10px; white-space: nowrap">
                      <button type="button" id="btn-add-iku" class="btn btn-sm btn-primary" style="font-weight: bold"><i class="far fa-plus-circle"></i></button>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(!empty($arrIKU)) {
                    foreach($arrIKU as $iku) {
                      ?>
                      <tr>
                        <td>
                          <textarea class="form-control" name="IKUUraian[]" placeholder="URAIAN INDIKATOR"><?=$iku->Uraian?></textarea>
                        </td>
                        <td>
                          <textarea class="form-control" name="IKUSumberData[]" placeholder="SUMBER DATA"><?=$iku->SumberData?></textarea>
                        </td>
                        <td>
                          <textarea class="form-control" name="IKUFormulasi[]" placeholder="FORMULASI"><?=$iku->Formulasi?></textarea>
                        </td>
                        <td>
                          <select class="form-control" name="IKUSatuan[]"><?=GetCombobox("SELECT * FROM ".TBL_SAKIP_MSATUAN." ORDER BY ".COL_NM_SATUAN, COL_NM_SATUAN, COL_NM_SATUAN, $iku->Satuan)?></select>
                        </td>
                        <td>
                          <input type="text" class="form-control" name="IKUTarget[]" value="<?=$iku->Target?>" />
                        </td>
                        <td>
                          <button type="button" class="btn btn-sm btn-danger btn-del-iku" style="font-weight: bold"><i class="fa fa-minus"></i></button>
                        </td>
                      </tr>
                      <?php
                    }
                  } else {
                    ?>
                    <tr class="empty">
                      <td colspan="6">
                        <p class="font-italic mb-0 text-center">BELUM ADA DATA</p>
                      </td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </form>
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var modalFormProgram = $('#modalFormProgram');
var modalFormKegiatan = $('#modalFormKegiatan');
var modalFormSasaran = $('#modalFormSasaran');
var modalFormIKU = $('#modalFormIKU');

function addRowIKU() {
  var html =' ';
  html += '<tr>';
  html += '<td>';
  html += '<textarea class="form-control" name="IKUUraian[]" placeholder="URAIAN INDIKATOR"></textarea>';
  html += '</td>';
  html += '<td>';
  html += '<textarea class="form-control" name="IKUSumberData[]" placeholder="SUMBER DATA"></textarea>';
  html += '</td>';
  html += '<td>';
  html += '<textarea class="form-control" name="IKUFormulasi[]" placeholder="FORMULASI"></textarea>';
  html += '</td>';
  html += '<td>';
  html += '<select class="form-control" name="IKUSatuan[]"><?=GetCombobox("SELECT * FROM ".TBL_SAKIP_MSATUAN." ORDER BY ".COL_NM_SATUAN, COL_NM_SATUAN, COL_NM_SATUAN)?></select>';
  html += '</td>';
  html += '<td>';
  html += '<input type="text" class="form-control" name="IKUTarget[]" />';
  html += '</td>';
  html += '<td>';
  html += '<button type="button" class="btn btn-sm btn-danger btn-del-iku" style="font-weight: bold"><i class="fa fa-minus"></i></button>';
  html += '</td>';
  html += '</tr>';

  var empEl = $('tr.empty', $('tbody', $('#tbl-iku')));
  if(empEl) {
    $('tr.empty', $('tbody', $('#tbl-iku'))).remove();
  }

  $('tbody', $('#tbl-iku')).append(html);
  $('.btn-del-iku', $('tbody', $('#tbl-iku'))).unbind('click').click(function() {
    var row = $(this).closest('tr');
    row.remove();
  });
  $("select", $('tbody tr:last', $('#tbl-iku'))).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
}

function addRowFungsi() {
  var html =' ';
  html += '<tr>';
  html += '<td>';
  html += '<textarea class="form-control" name="FungsiUraian[]" placeholder="URAIAN FUNGSI"></textarea>';
  html += '</td>';
  html += '<td>';
  html += '<button type="button" class="btn btn-sm btn-danger btn-del-fungsi" style="font-weight: bold"><i class="fa fa-minus"></i></button>';
  html += '</td>';
  html += '</tr>';

  var empEl = $('tr.empty', $('tbody', $('#tbl-fungsi')));
  if(empEl) {
    $('tr.empty', $('tbody', $('#tbl-fungsi'))).remove();
  }

  $('tbody', $('#tbl-fungsi')).append(html);
  $('.btn-del-fungsi', $('tbody', $('#tbl-fungsi'))).unbind('click').click(function() {
    var row = $(this).closest('tr');
    row.remove();
  });
  $("select", $('tbody tr:last', $('#tbl-fungsi'))).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
}

$(document).ready(function(){
  $('select[name=filterBidang], select[name=filterDPA]').change(function(){
    var url = $(this).val();
    location.href = url;
  });

  $('#btnTambahTupoksi, #btnTambahIKU, .btn-edit-tupoksi, .btn-edit-iku').click(function(){
    var url = $(this).attr('href');
    var val = $(this).data('val');
    var label = $(this).data('label');
    swal({
      closeOnClickOutside: true,
      buttons: ['BATAL','SUBMIT'],
      text: label,
      content: {
        element: "input",
        attributes: {
          placeholder: label,
          type: "text",
          value: (val||'')
        }
      },
    }).then(function(val){
      if(val) {
        $.ajax({
          url: url,
          method: "POST",
          dataType: "json",
          data: {
            Uraian: val
          }
        }).success(function(res) {
          if(res.error) {
            swal({
              title: 'ERROR',
              text: res.error,
              icon: 'error',
              buttons:false
            });
          } else {
            location.reload();
          }
        }).fail(function() {
          swal({
            title: 'SERVER ERROR',
            text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
            icon: 'error',
            buttons:false
          });
        }).done(function() {

        });
      }
    });

    return false;
  });

  $('.btn-del-tupoksi, .btn-del-iku').click(function() {
    var url = $(this).attr('href');
    swal({
      title: "APAKAH ANDA YAKIN?",
      icon: "warning",
      buttons: [
        'BATAL',
        'YAKIN'
      ],
    }).then(function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          url: url,
          method: "GET",
          dataType: "json"
        }).success(function(res) {
          if(res.error) {
            swal({
              title: 'ERROR',
              text: res.error,
              icon: 'error',
              buttons:false
            });
          } else {
            location.reload();
          }
        }).fail(function() {
          swal({
            title: 'SERVER ERROR',
            text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
            icon: 'error',
            buttons:false
          });
        }).done(function() {

        });

      } else {

      }
    })
    return false;
  });

  modalFormProgram.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalFormProgram).empty();
  });

  $('.btn-add-program, .btn-edit-program').click(function() {
    var url = $(this).attr('href');
    modalFormProgram.modal('show');
    $('.modal-body', modalFormProgram).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modalFormProgram).load(url, function(){
      $('button[type=submit]', modalFormProgram).unbind('click').click(function(){
        $('form', modalFormProgram).submit();
      });
      $("select", modalFormProgram).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
    });
    return false;
  });

  $('.btn-change-program, .btn-change-progkegiatan').click(function() {
    var url = $(this).attr('href');
    var prompt = $(this).data('prompt');
    swal({
      title: "APAKAH ANDA YAKIN?",
      text: prompt,
      icon: "warning",
      buttons: [
        'BATAL',
        'YAKIN'
      ],
    }).then(function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          url: url,
          method: "GET",
          dataType: "json"
        }).success(function(res) {
          if(res.error) {
            swal({
              title: 'ERROR',
              text: res.error,
              icon: 'error',
              buttons:false
            });
          } else {
            location.reload();
          }
        }).fail(function() {
          swal({
            title: 'SERVER ERROR',
            text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
            icon: 'error',
            buttons:false
          });
        }).done(function() {

        });

      } else {

      }
    })
    return false;
  });

  modalFormKegiatan.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalFormKegiatan).empty();
  });

  $('.btn-add-progkegiatan, .btn-edit-progkegiatan').click(function() {
    var url = $(this).attr('href');
    modalFormKegiatan.modal('show');
    $('.modal-body', modalFormKegiatan).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modalFormKegiatan).load(url, function(){
      $('button[type=submit]', modalFormKegiatan).unbind('click').click(function(){
        $('form', modalFormKegiatan).submit();
      });
    });
    return false;
  });

  modalFormSasaran.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalFormSasaran).empty();
  });

  $('.btn-change-kegsasaran, .btn-change-progsasaran').click(function() {
    var url = $(this).attr('href');
    modalFormSasaran.modal('show');
    $('.modal-body', modalFormSasaran).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modalFormSasaran).load(url, function(){
      $('button[type=submit]', modalFormSasaran).unbind('click').click(function(){
        $('form', modalFormSasaran).submit();
      });
    });
    return false;
  });

  $('#btnFormIKU').click(function() {
    var url = $(this).attr('href');
    modalFormIKU.modal('show');

    $('.btn-del-iku, .btn-del-fungsi', modalFormIKU).unbind('click').click(function() {
      var row = $(this).closest('tr');
      row.remove();
    });
    $("select", modalFormIKU).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
    $('button[type=submit]', modalFormIKU).unbind('click').click(function(){
      $('form', modalFormIKU).submit();
    });
  });

  $('#btn-add-iku', modalFormIKU).click(function() {
    addRowIKU();
  });
  $('#btn-add-fungsi', modalFormIKU).click(function() {
    addRowFungsi();
  });

  $('#form-iku').validate({
    submitHandler: function(form) {
      var modal = $(form).closest('modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>
