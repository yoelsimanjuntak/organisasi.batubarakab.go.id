<?php
$ruser = GetLoggedUser();

$rOptSkpd = $this->db
->where(COL_SKPDISAKTIF, 1)
->order_by(COL_SKPDURUSAN, 'asc')
->order_by(COL_SKPDBIDANG, 'asc')
->order_by(COL_SKPDUNIT, 'asc')
->order_by(COL_SKPDSUBUNIT, 'asc')
->get(TBL_SAKIPV2_SKPD)
->result_array();

$getIdSasaranPmd = '';
$getSkpd = '';
$getRenstra = '';
$getDPA = '';

if(!empty($_GET['idSasaranPmd'])) $getIdSasaranPmd = $_GET['idSasaranPmd'];

if(!empty($_GET['idSKPD'])) $getSkpd = $_GET['idSKPD'];
else if(!empty($rOptSkpd)) $getSkpd = $rOptSkpd[0][COL_SKPDID];

if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEGUEST) {
  $getSkpd=$ruser[COL_SKPDID];
}

$rOptRenstra = array();
if(!empty($getSkpd)) {
  $rOptRenstra = $this->db
  ->where(COL_IDPEMDA, $rpemda[COL_PMDID])
  ->where(COL_IDSKPD, $getSkpd)
  //->where(COL_RENSTRAISAKTIF, 1)
  ->order_by(COL_RENSTRAISAKTIF, 'desc')
  ->order_by(COL_RENSTRATAHUN, 'desc')
  ->order_by(COL_RENSTRAID, 'desc')
  ->get(TBL_SAKIPV2_SKPD_RENSTRA)
  ->result_array();
}

if(!empty($_GET['idRenstra'])) $getRenstra = $_GET['idRenstra'];
else if(!empty($rOptRenstra)) $getRenstra = $rOptRenstra[0][COL_RENSTRAID];

$rOptDpa = array();
if(!empty($getRenstra)) {
  $rOptDpa = $this->db
  ->where(COL_IDRENSTRA, $getRenstra)
  ->where(COL_DPAISAKTIF, 1)
  ->order_by(COL_DPAISAKTIF, 'desc')
  ->order_by(COL_DPATAHUN,'desc')
  ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
  ->result_array();
}

$getDPA = null;
if(!empty($_GET['idDPA'])) {
  $getDPA = $_GET['idDPA'];
} else if(!empty($rOptDpa) && $rOptDpa[0][COL_DPAISAKTIF]==1) {
  $getDPA = $rOptDpa[0][COL_DPAID];
}

$rSasaran = array();
if(!empty($getSkpd) && !empty($getRenstra)) {
  if(!empty($getIdSasaranPmd)) {
    $this->db->where(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.'.'.COL_IDSASARANPMD, $getIdSasaranPmd);
  }
  
  $rSasaran = $this->db
  ->join(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN,TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.".".COL_IDTUJUAN,"left")
  ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.".".COL_IDRENSTRA,"left")
  ->where(COL_IDSKPD, $getSkpd)
  ->where(COL_IDRENSTRA, $getRenstra)
  ->order_by(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANNO)
  ->order_by(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.'.'.COL_SASARANNO)
  ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN)
  ->result_array();
}
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?=$title?></h3>
      </div>
      <?php
      if(!empty($navs)) {
        ?>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <?php
            foreach($navs as $n) {
              if(!empty($n['link'])) {
                ?>
                <li class="breadcrumb-item"><a href="<?=$n['link']?>"><?=$n['text']?></a></li>
                <?php
              } else {
                ?>
                <li class="breadcrumb-item active"><?=$n['text']?></li>
                <?php
              }
            }
            ?>
          </ol>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-outline card-secondary">
          <div class="card-body p-0">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <td>
                    <div class="form-group row mb-0">
                      <label class="control-label col-lg-2">PERIODE PEMERINTAHAN :</label>
                      <div class="col-lg-10">
                        <p class="font-italic font-weight-bold mb-0" style="line-height: 2; text-decoration: underline">
                          <?=$rpemda[COL_PMDTAHUNMULAI].' - '.$rpemda[COL_PMDTAHUNAKHIR].' '.strtoupper($rpemda[COL_PMDPEJABAT])?>
                        </p>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="control-label col-lg-2">SKPD :</label>
                      <div class="col-lg-10">
                        <?php
                        if($ruser[COL_ROLEID] == ROLEADMIN || $ruser[COL_ROLEID] == ROLEGUEST) {
                          ?>
                          <select class="form-control" name="filterSkpd">
                            <?php
                            foreach($rOptSkpd as $opt) {
                              $isSelected = '';
                              if(!empty($getSkpd) && $opt[COL_SKPDID]==$getSkpd) {
                                $isSelected='selected';
                              }
                              ?>
                              <option value="<?=site_url('sakipv2/laporan/dpa').'?idSKPD='.$opt[COL_SKPDID]?>" <?=$isSelected?>>
                                <?=$opt[COL_SKPDURUSAN].'.'.$opt[COL_SKPDBIDANG].'.'.$opt[COL_SKPDUNIT].'.'.$opt[COL_SKPDSUBUNIT].' - '.strtoupper($opt[COL_SKPDNAMA])?>
                              </option>
                              <?php
                            }
                            ?>
                          </select>
                          <?php
                        } else {
                          $ropd = $this->db
                          ->where(COL_SKPDID, $ruser[COL_SKPDID])
                          ->get(TBL_SAKIPV2_SKPD)
                          ->row_array();
                          ?>
                          <p class="font-italic font-weight-bold mb-0" style="line-height: 2; text-decoration: underline">
                            <?=strtoupper($ropd[COL_SKPDNAMA])?>
                          </p>
                          <?php
                        }
                        ?>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="control-label col-lg-2">RENSTRA SKPD :</label>
                      <div class="col-lg-10">
                        <select class="form-control" name="filterRenstra">
                          <?php
                          foreach($rOptRenstra as $opt) {
                            $isSelected = '';
                            if(!empty($getRenstra) && $opt[COL_RENSTRAID]==$getRenstra) {
                              $isSelected='selected';
                            }
                            ?>
                            <option value="<?=site_url('sakipv2/laporan/dpa').'?idSKPD='.$getSkpd.'&idRenstra='.$opt[COL_RENSTRAID]?>" <?=$isSelected?>>
                              <?=$opt[COL_RENSTRATAHUN].' - '.strtoupper($opt[COL_RENSTRAURAIAN])?>
                            </option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="control-label col-lg-2">DPA SKPD :</label>
                      <div class="col-lg-10">
                        <select class="form-control" name="filterDPA">
                          <?php
                          foreach($rOptDpa as $opt) {
                            ?>
                            <option value="<?=site_url('sakipv2/laporan/dpa').'?idSKPD='.$getSkpd.'&idRenstra='.$getRenstra.'&idDPA='.$opt[COL_DPAID]?>" <?=$opt[COL_DPAID]==$getDPA?'selected':''?>>
                              <?=$opt[COL_DPATAHUN].' - '.strtoupper($opt[COL_DPAURAIAN])?>
                            </option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </td>
                </tr>
              </thead>
            </table>
          </div>
        </div>
        <div class="card card-outline card-secondary">
          <div class="card-body p-0">
            <div class="table-responsive">
              <table class="table table-bordered text-sm mb-0">
                <thead>
                  <tr>
                    <th>KODE</th>
                    <th>PROGRAM / KEGIATAN / SUB KEGIATAN</th>
                    <th>INDIKATOR /  TARGET</th>
                    <th>ANGGARAN</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $sumTotal = 0;
                  if(!empty($rSasaran)) {
                    foreach($rSasaran as $s) {
                      $rprogram = array();
                      if(!empty($getDPA)) {
                        $rprogram = $this->db
                        ->select('*, (select sum(sakipv2_subbid_subkegiatan.SubkegPagu) from sakipv2_subbid_subkegiatan left join sakipv2_bid_kegiatan keg on keg.KegiatanId = sakipv2_subbid_subkegiatan.IdKegiatan where keg.IdProgram = sakipv2_bid_program.ProgramId) as ProgPagu')
                        ->where(COL_IDSASARANSKPD, $s[COL_SASARANID])
                        ->where(COL_IDDPA, $getDPA)
                        ->get(TBL_SAKIPV2_BID_PROGRAM)
                        ->result_array();
                       }
                      ?>
                      <tr>
                        <td colspan="4" class="font-italic font-weight-bold"><?=strtoupper($s[COL_SASARANURAIAN])?></td>
                      </tr>
                      <?php
                      if(!empty($rprogram)) {
                        foreach($rprogram as $p) {
                          $rPrgIndikator = $this->db
                          ->where(COL_IDPROGRAM, $p[COL_PROGRAMID])
                          ->get(TBL_SAKIPV2_BID_PROGSASARAN)
                          ->result_array();
                          $htmlIndikator = '';
                          foreach ($rPrgIndikator as $i) {
                            $htmlIndikator .= '<p class="text-sm font-italic mb-0">'.strtoupper($i[COL_SASARANINDIKATOR]).' / '.$i[COL_SASARANTARGET].' ('.$i[COL_SASARANSATUAN].')'.'</p>';
                          }

                          $rkegiatan = $this->db
                          ->select('*, (select sum(sakipv2_subbid_subkegiatan.SubkegPagu) from sakipv2_subbid_subkegiatan where sakipv2_subbid_subkegiatan.IdKegiatan = sakipv2_bid_kegiatan.KegiatanId) as KegPagu')
                          ->where(COL_IDPROGRAM, $p[COL_PROGRAMID])
                          ->get(TBL_SAKIPV2_BID_KEGIATAN)
                          ->result_array();
                          ?>
                          <tr class="text-green">
                            <td style="width: 100px; white-space: nowrap"><?=$p[COL_PROGRAMKODE]?></td>
                            <td><?=strtoupper($p[COL_PROGRAMURAIAN])?></td>
                            <td><?=$htmlIndikator?></td>
                            <td style="width: 100px; white-space: nowrap" class="text-right"><?=number_format($p['ProgPagu'])?></td>
                          </tr>
                          <?php
                          foreach($rkegiatan as $k) {
                            $rkegiatansub = $this->db
                            ->where(COL_IDKEGIATAN, $k[COL_KEGIATANID])
                            ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
                            ->result_array();

                            $rKegIndikator = $this->db
                            ->where(COL_IDKEGIATAN, $k[COL_KEGIATANID])
                            ->get(TBL_SAKIPV2_BID_KEGSASARAN)
                            ->result_array();
                            $htmlIndikator = '';
                            foreach ($rKegIndikator as $i) {
                              $htmlIndikator .= '<p class="text-sm font-italic mb-0">'.strtoupper($i[COL_SASARANINDIKATOR]).' / '.$i[COL_SASARANTARGET].' ('.$i[COL_SASARANSATUAN].')'.'</p>';
                            }
                            ?>
                            <tr class="text-blue">
                              <td style="width: 100px; white-space: nowrap"><?=$k[COL_KEGIATANKODE]?></td>
                              <td><?=strtoupper($k[COL_KEGIATANURAIAN])?></td>
                              <td><?=$htmlIndikator?></td>
                              <td style="width: 100px; white-space: nowrap" class="text-right"><?=number_format($k['KegPagu'])?></td>
                            </tr>
                            <?php
                            foreach($rkegiatansub as $sk) {
                              $rSubkegIndikator = $this->db
                              ->where(COL_IDSUBKEG, $sk[COL_SUBKEGID])
                              ->get(TBL_SAKIPV2_SUBBID_SUBKEGSASARAN)
                              ->result_array();
                              $htmlIndikator = '';
                              foreach ($rSubkegIndikator as $i) {
                                $htmlIndikator .= '<p class="text-sm font-italic mb-0">'.strtoupper($i[COL_SASARANINDIKATOR]).' / '.$i[COL_SASARANTARGET].' ('.$i[COL_SASARANSATUAN].')'.'</p>';
                              }
                              ?>
                              <tr class="text-gray">
                                <td style="width: 100px; white-space: nowrap"><?=$sk[COL_SUBKEGKODE]?></td>
                                <td><?=strtoupper($sk[COL_SUBKEGURAIAN])?></td>
                                <td><?=$htmlIndikator?></td>
                                <td style="width: 100px; white-space: nowrap" class="text-right"><?=number_format($sk[COL_SUBKEGPAGU])?></td>
                              </tr>
                              <?php
                            }
                          }
                          $sumTotal += $p['ProgPagu'];
                        }
                      }
                    }
                  } else {
                    ?>
                    <tr>
                      <td colspan="3">
                        <p class="font-italic mb-0">(KOSONG)</p>
                      </td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
                <tfoot>
                  <td colspan="3" class="text-right">
                    <p class="font-italic font-weight-bold mb-0">TOTAL</p>
                  </td>
                  <td style="padding-right: 1.5rem !important">
                    <p class="font-italic font-weight-bold mb-0 text-right"><?=number_format($sumTotal)?></p>
                  </td>
                </tfoot>
              </table>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
  $('select[name=filterRenstra],select[name=filterSkpd],select[name=filterDPA]').change(function(){
    var url = $(this).val();
    location.href = url;
  });
});
</script>
