<?php
$rskpd = $this->db
->where(COL_SKPDID, $idSKPD)
->get(TBL_SAKIPV2_SKPD)
->row_array();

$rrenstra = $this->db
->where(COL_RENSTRAID, $idRenstra)
->get(TBL_SAKIPV2_SKPD_RENSTRA)
->row_array();

$arrFungsi = array();
$arrIKU = array();
if(!empty($rrenstra)) {
  $arrFungsi = json_decode($rrenstra[COL_RENSTRAFUNGSI]);
  $arrIKU = json_decode($rrenstra[COL_RENSTRAIKU]);
}
?>
<?php
if(!empty($isCetak) && $isCetak==1) {
  ?>
  <style>
  table {
    width: 100%;
    border-collapse: collapse;
    margin-bottom: 0 !important;
  }
  table, th, td {
    border: 1px solid black !important;
  }
  th, td {
    padding: 10px;
  }
  </style>
  <?php
} else {
  ?>
  <style>
  th, td {
    padding: 10px !important;
  }
  .card-body.p-0 .table tbody>tr>td:first-of-type, .card-body.p-0 .table tbody>tr>th:first-of-type, .card-body.p-0 .table thead>tr>td:first-of-type, .card-body.p-0 .table thead>tr>th:first-of-type, .pl-4, .px-4 {
    padding-left: 10px !important;
  }
  h5{
    margin-top: 50px !important;
  }
  </style>
  <div class="row p-3">
    <div class="col-lg-12 text-right">
      <a href="<?=site_url('sakipv2/laporan/index/skpd-iku-cetak').'?idSKPD='.$idSKPD.'&idRenstra='.$idRenstra?>" class="btn btn-outline-primary btn-sm" target="_blank">
        <i class="far fa-print"></i>&nbsp;&nbsp;CETAK
      </a>
    </div>
  </div>
  <?php
}
?>
<h5 style="font-weight: 300; text-align: center; margin-top: 10px; margin-bottom: 10px; font-size: 12pt !important">
  INDIKATOR KINERJA UTAMA (IKU)<br />
  <strong><?=strtoupper(!empty($rskpd)?$rskpd[COL_SKPDNAMA]:'-')?></strong><br />
  <?='TAHUN '.$rrenstra[COL_RENSTRATAHUN]?>
</h5>
<hr />
<table class="table table-bordered" style="font-size: 10pt !important; border: none !important">
  <tbody style="font-size: 10pt !important">
    <tr>
      <td colspan="5" style="border: none !important">
        NAMA ORGANISASI:<br />
        <strong><?=strtoupper(!empty($rskpd)?$rskpd[COL_SKPDNAMA]:'-')?></strong>
      </td>
    </tr>
    <tr>
      <td colspan="5" style="border: none !important">
        TUGAS POKOK:<br />
        <strong style="font-style: italic"><?=!empty($rrenstra)?$rrenstra[COL_RENSTRATUGASPOKOK]:'-'?></strong>
      </td>
    </tr>
  </tbody>
</table>
<div style="padding-left: .75rem; padding-right: 1.5rem; font-size: 10pt !important">
  <h5 style="margin-bottom: 0 !important; margin-top: 0 !important; font-size: 10pt !important; font-weight: normal">FUNGSI:</h5>
  <?php
  if(!empty($arrFungsi)) {
    ?>
    <ul style="margin-top: 0 important">
      <?php
      foreach($arrFungsi as $f) {
        ?>
        <li style="text-align: justify"><?=$f?></li>
        <?php
      }
      ?>
    </ul>
    <?php
  } else {
    echo '-';
  }
  ?>
</div>
<br />
<table class="table table-bordered" style="font-size: 10pt !important">
  <thead>
    <tr style="background-color: #ffc1075e">
      <th style="width: 100px; white-space: nowrap">NO.</th>
      <th>INDIKATOR KINERJA UTAMA</th>
      <th>FORMULASI</th>
      <th>SUMBER DATA</th>
      <th>TARGET</th>
    </tr>
  </thead>
  <tbody>

    <?php
    $no=1;
    if(!empty($arrIKU)) {
      $no=1;
      foreach($arrIKU as $iku) {
        ?>
        <tr>
          <td style="text-align: right; width: 10px; white-space: nowrap"><?=$no?>.</td>
          <td style="font-style: italic"><?=strtoupper($iku->Uraian)?></td>
          <td style="font-style: italic"><?=strtoupper($iku->Formulasi)?></td>
          <td style="font-style: italic"><?=$iku->SumberData?></td>
          <td style="font-style: italic"><?=$iku->Target.' '.$iku->Satuan?></td>
        </tr>
        <?php
        $no++;
      }
    } else {
      ?>
      <tr>
        <td colspan="5">
          <p style="font-style: italic; text-align: center; margin-bottom: 0 !important">BELUM ADA DATA</p>
        </td>
      </tr>
      <?php
    }
    ?>
  </tbody>
</table>
<?php
$rbidang = array();
if(!empty($rrenstra)) {
  $rbidang = $this->db
  ->where(COL_IDRENSTRA, $idRenstra)
  ->get(TBL_SAKIPV2_BID)
  ->result_array();
}

foreach($rbidang as $bid) {
  $arrFungsi = array();
  $arrIKU = array();
  if(!empty($bid[COL_BIDFUNGSI])) {
    $arrFungsi = json_decode($bid[COL_BIDFUNGSI]);
  }
  if(!empty($bid[COL_BIDIKU])) {
    $arrIKU = json_decode($bid[COL_BIDIKU]);
  }

  $rsubbidang = $this->db
  ->where(COL_IDBID, $bid[COL_BIDID])
  ->get(TBL_SAKIPV2_SUBBID)
  ->result_array();
  ?>
  <pagebreak></pagebreak>
  <h5 style="font-weight: 300; text-align: center; margin-bottom: 10px; font-size: 12pt !important">
    INDIKATOR KINERJA UTAMA (IKU)<br />
    <strong><?=strtoupper(!empty($bid)?$bid[COL_BIDNAMA]:'-')?></strong><br />
    <?='TAHUN '.$rrenstra[COL_RENSTRATAHUN]?>
  </h5>
  <hr />
  <table class="table table-bordered" style="font-size: 10pt !important; border: none !important">
    <tbody style="font-size: 10pt !important">
      <tr>
        <td colspan="5" style="border: none !important">
          TUGAS POKOK:<br />
          <strong style="font-style: italic"><?=!empty($bid[COL_BIDTUGASPOKOK])?$bid[COL_BIDTUGASPOKOK]:'-'?></strong>
        </td>
      </tr>
    </tbody>
  </table>
  <div style="padding-left: .75rem; padding-right: 1.5rem; font-size: 10pt !important">
    <h5 style="margin-bottom: 0 !important; margin-top: 0 !important; font-size: 10pt !important; font-weight: normal">FUNGSI:</h5>
    <?php
    if(!empty($arrFungsi)) {
      ?>
      <ul style="margin-top: 0 !important">
        <?php
        foreach($arrFungsi as $f) {
          ?>
          <li style="text-align: justify"><?=$f?></li>
          <?php
        }
        ?>
      </ul>
      <?php
    } else {
      echo '-';
    }
    ?>
  </div>
  <br />
  <table class="table table-bordered" style="font-size: 10pt !important">
    <thead>
      <tr style="background-color: #ffc1075e">
        <th style="width: 100px; white-space: nowrap">NO.</th>
        <th>INDIKATOR KINERJA UTAMA</th>
        <th>FORMULASI</th>
        <th>SUMBER DATA</th>
        <th>TARGET</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $no=1;
      if(!empty($arrIKU)) {
        foreach($arrIKU as $iku) {
          ?>
          <tr>
            <td style="text-align: right; width: 10px; white-space: nowrap"><?=$no?>.</td>
            <td style="font-style: italic"><?=strtoupper($iku->Uraian)?></td>
            <td style="font-style: italic"><?=strtoupper($iku->Formulasi)?></td>
            <td style="font-style: italic"><?=$iku->SumberData?></td>
            <td style="font-style: italic"><?=$iku->Target.' '.$iku->Satuan?></td>
          </tr>
          <?php
          $no++;
        }
      } else {
        ?>
        <tr>
          <td colspan="5">
            <p style="font-style: italic; text-align: center; margin-bottom: 0 !important">BELUM ADA DATA</p>
          </td>
        </tr>
        <?php
      }
      ?>
    </tbody>
  </table>
  <?php
  foreach($rsubbidang as $subbid) {
    $arrFungsi = array();
    $arrIKU = array();
    if(!empty($subbid[COL_SUBBIDFUNGSI])) {
      $arrFungsi = json_decode($subbid[COL_SUBBIDFUNGSI]);
    }
    if(!empty($subbid[COL_SUBBIDIKU])) {
      $arrIKU = json_decode($subbid[COL_SUBBIDIKU]);
    }

    $rpelaksana = $this->db
    ->where(COL_IDSUBBID, $subbid[COL_SUBBIDID])
    ->get(TBL_SAKIPV2_SUBBID_PELAKSANA)
    ->result_array();
    ?>
    <pagebreak></pagebreak>
    <h5 style="font-weight: 300; text-align: center; margin-bottom: 10px; font-size: 12pt !important">
      INDIKATOR KINERJA UTAMA (IKU)<br />
      <strong><?=strtoupper(!empty($subbid)?$subbid[COL_SUBBIDNAMA]:'-')?></strong><br />
      <?='TAHUN '.$rrenstra[COL_RENSTRATAHUN]?>
    </h5>
    <hr />
    <table class="table table-bordered" style="font-size: 10pt !important; border: 0 !important">
      <tbody style="font-size: 10pt !important">
        <tr>
          <td colspan="5" style="border: 0 !important">
            TUGAS POKOK:<br />
            <strong style="font-style: italic"><?=!empty($subbid[COL_SUBBIDTUGASPOKOK])?$subbid[COL_SUBBIDTUGASPOKOK]:'-'?></strong>
          </td>
        </tr>
      </tbody>
    </table>
    <div style="padding-left: .75rem; padding-right: 1.5rem; font-size: 10pt !important">
      <h5 style="margin-bottom: 0 !important; margin-top: 0 !important; font-size: 10pt !important; font-weight: normal">FUNGSI:</h5>
      <?php
      if(!empty($arrFungsi)) {
        ?>
        <ul style="margin-top: 0 !important">
          <?php
          foreach($arrFungsi as $f) {
            ?>
            <li style="text-align: justify"><?=$f?></li>
            <?php
          }
          ?>
        </ul>
        <?php
      } else {
        echo '-';
      }
      ?>
    </div>
    <br />
    <table class="table table-bordered" style="font-size: 10pt !important">
      <thead>
        <tr style="background-color: #ffc1075e">
          <th style="width: 100px; white-space: nowrap">NO.</th>
          <th>INDIKATOR KINERJA UTAMA</th>
          <th>FORMULASI</th>
          <th>SUMBER DATA</th>
          <th>TARGET</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $no=1;
        if(!empty($arrIKU)) {
          foreach($arrIKU as $iku) {
            ?>
            <tr>
              <td style="text-align: right"><?=$no?></td>
              <td style="font-style: italic"><?=strtoupper($iku->Uraian)?></td>
              <td style="font-style: italic"><?=strtoupper($iku->Formulasi)?></td>
              <td style="font-style: italic"><?=$iku->SumberData?></td>
              <td style="font-style: italic"><?=$iku->Target.' '.$iku->Satuan?></td>
            </tr>
            <?php
            $no++;
          }
        } else {
          ?>
          <tr>
            <td colspan="5">
              <p style="font-style: italic; text-align: center; margin-bottom: 0 !important">BELUM ADA DATA</p>
            </td>
          </tr>
          <?php
        }
        ?>
      </tbody>
    </table>
    <?php

    foreach($rpelaksana as $pel) {
      $arrFungsi = array();
      $arrIKU = array();
      if(!empty($pel[COL_PLSFUNGSI])) {
        $arrFungsi = json_decode($pel[COL_PLSFUNGSI]);
      }
      if(!empty($pel[COL_PLSFUNGSI])) {
        $arrIKU = json_decode($pel[COL_PLSIKU]);
      }
      ?>
      <pagebreak></pagebreak>
      <h5 style="font-weight: 300; text-align: center; margin-bottom: 10px; font-size: 12pt !important">
        INDIKATOR KINERJA INDIVIDU (IKI)<br />
        <strong><?=strtoupper(!empty($pel)?$pel[COL_PLSNAMA]:'-')?></strong><br />
        <?='TAHUN '.$rrenstra[COL_RENSTRATAHUN]?>
      </h5>
      <hr />
      <table class="table table-bordered" style="font-size: 10pt !important; border: 0 !important">
        <tbody style="font-size: 10pt !important">
          <tr>
            <td colspan="5" style="border: 0 !important">
              TUGAS POKOK:<br />
              <strong style="font-style: italic"><?=!empty($pel[COL_PLSTUGASPOKOK])?$pel[COL_PLSTUGASPOKOK]:'-'?></strong>
            </td>
          </tr>
        </tbody>
      </table>
      <div style="padding-left: .75rem; padding-right: 1.5rem; font-size: 10pt !important">
        <h5 style="margin-bottom: 0 !important; margin-top: 0 !important; font-size: 10pt !important; font-weight: normal">FUNGSI:</h5>
        <?php
        if(!empty($arrFungsi)) {
          ?>
          <ul style="margin-top: 0 !important">
            <?php
            foreach($arrFungsi as $f) {
              ?>
              <li style="text-align: justify"><?=$f?></li>
              <?php
            }
            ?>
          </ul>
          <?php
        } else {
          echo '-';
        }
        ?>
      </div>
      <br />
      <table class="table table-bordered" style="font-size: 10pt !important">
        <thead>
          <tr style="background-color: #ffc1075e">
            <th style="width: 100px; white-space: nowrap">NO.</th>
            <th>INDIKATOR KINERJA UTAMA</th>
            <th>FORMULASI</th>
            <th>SUMBER DATA</th>
            <th>TARGET</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $no=1;
          if(!empty($arrIKU)) {
            foreach($arrIKU as $iku) {
              ?>
              <tr>
                <td style="text-align: right"><?=$no?></td>
                <td style="font-style: italic"><?=strtoupper($iku->Uraian)?></td>
                <td style="font-style: italic"><?=strtoupper($iku->Formulasi)?></td>
                <td style="font-style: italic"><?=$iku->SumberData?></td>
                <td style="font-style: italic"><?=$iku->Target.' '.$iku->Satuan?></td>
              </tr>
              <?php
              $no++;
            }
          } else {
            ?>
            <tr>
              <td colspan="5">
                <p style="font-style: italic; text-align: center; margin-bottom: 0 !important">BELUM ADA DATA</p>
              </td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
      <?php
    }
  }
}
?>
