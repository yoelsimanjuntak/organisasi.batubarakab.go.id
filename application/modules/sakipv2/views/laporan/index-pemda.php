<?php
$rOptPmd = $this->db
->order_by(COL_PMDISAKTIF,'desc')
->order_by(COL_PMDTAHUNMULAI,'desc')
->get(TBL_SAKIPV2_PEMDA)
->result_array();
$getPmd = null;
if(!empty($_GET['idPmd'])) $getPmd = $_GET['idPmd'];
else if(!empty($rOptPmd)) $getPmd = $rOptPmd[0][COL_PMDID];

?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?=$title?></h3>
      </div>
      <?php
      if(!empty($navs)) {
        ?>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <?php
            foreach($navs as $n) {
              if(!empty($n['link'])) {
                ?>
                <li class="breadcrumb-item"><a href="<?=$n['link']?>"><?=$n['text']?></a></li>
                <?php
              } else {
                ?>
                <li class="breadcrumb-item active"><?=$n['text']?></li>
                <?php
              }
            }
            ?>
          </ol>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-outline card-secondary">
          <div class="card-header p-0 border-0">
            <table class="table table-bordered mb-0">
              <thead>
                <tr>
                  <td>
                    <div class="form-group row mb-0">
                      <label class="control-label col-lg-2">PERIODE PEMERINTAHAN :</label>
                      <div class="col-lg-10">
                        <select class="form-control" name="idPmd">
                          <?php
                          foreach($rOptPmd as $opt) {
                            ?>
                            <option value="<?=site_url('sakipv2/laporan/index/'.$page).'?idPmd='.$opt[COL_PMDID]?>" <?=$opt[COL_PMDID]==$getPmd?'selected':''?>>
                              <?=$opt[COL_PMDTAHUNMULAI].' s.d '.$opt[COL_PMDTAHUNMULAI].' - '.strtoupper($opt[COL_PMDPEJABAT])?>
                            </option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </td>
                </tr>
              </thead>
            </table>
          </div>
          <div class="card-body p-0">
            <?=$this->load->view('sakipv2/laporan/'.$page, array('idPmd'=>$getPmd))?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
  $('select[name=idPmd]').change(function(){
    var url = $(this).val();
    location.href = url;
  });
});
</script>
