<?php
$rmisi = $this->db
->where(COL_IDPMD, $idPmd)
->get(TBL_SAKIPV2_PEMDA_MISI)
->result_array();

$rpemda = $this->db
->where(COL_PMDID, $idPmd)
->get(TBL_SAKIPV2_PEMDA)
->row_array();

$arrMisi = array();
foreach($rmisi as $m) {
    $arrKabTujuan = array();
    $this->db->join(TBL_SAKIPV2_PEMDA_MISI,TBL_SAKIPV2_PEMDA_MISI.'.'.COL_MISIID." = ".TBL_SAKIPV2_PEMDA_TUJUAN.".".COL_IDMISI,"inner");
    $this->db->where(TBL_SAKIPV2_PEMDA_TUJUAN.'.'.COL_IDMISI, $m[COL_MISIID]);
    $this->db->order_by(TBL_SAKIPV2_PEMDA_TUJUAN.".".COL_TUJUANNO, 'asc');
    $rkab_tujuan = $this->db->get(TBL_SAKIPV2_PEMDA_TUJUAN)->result_array();

    foreach($rkab_tujuan as $tujuankab) {
        $arrKabIkTujuan = array();
        $kab_iktujuan = $this->db
            ->where(COL_IDTUJUAN, $tujuankab[COL_TUJUANID])
            ->get(TBL_SAKIPV2_PEMDA_TUJUANDET)
            ->result_array();

        $htmlTujuan = "<p class='node-name'>TUJUAN ".$tujuankab[COL_MISINO].".".$tujuankab[COL_TUJUANNO]."</p><p class='node-title'>".strtoupper($tujuankab[COL_TUJUANURAIAN])."</p>";
        $iktujuan_ = "";
        if(count($kab_iktujuan) > 0) {
            $iktujuan_ .= "<p class='node-title' style='margin: 0 !important'><strong>INDIKATOR :</strong></p><ul style='margin-left: 0px; padding-left: 15px;'>";
            foreach($kab_iktujuan as $ikt) {
                $iktujuan_ .= "<li>".strtoupper($ikt[COL_TUJINDIKATORURAIAN])."</li>";
            }
            $iktujuan_ .= "</ul>";
        }
        $htmlTujuan .= $iktujuan_;

        $arrKabSasaran = array();
        $kab_sasaran = $this->db
            ->join(TBL_SAKIPV2_PEMDA_TUJUAN,TBL_SAKIPV2_PEMDA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_PEMDA_SASARAN.".".COL_IDTUJUAN,"inner")
            ->join(TBL_SAKIPV2_PEMDA_MISI,TBL_SAKIPV2_PEMDA_MISI.'.'.COL_MISIID." = ".TBL_SAKIPV2_PEMDA_TUJUAN.".".COL_IDMISI,"inner")
            ->where(TBL_SAKIPV2_PEMDA_SASARAN.'.'.COL_IDTUJUAN, $tujuankab[COL_TUJUANID])
            ->order_by(TBL_SAKIPV2_PEMDA_SASARAN.".".COL_SASARANNO, 'asc')
            ->get(TBL_SAKIPV2_PEMDA_SASARAN)
            ->result_array();

        foreach($kab_sasaran as $skab) {
            $arrKabIkSasaran = array();
            $kab_iksasaran = $this->db
                ->where(COL_IDSASARAN, $skab[COL_SASARANID])
                ->get(TBL_SAKIPV2_PEMDA_SASARANDET)
                ->result_array();

            $htmlSasaran = "<p class='node-name'>SASARAN ".$skab[COL_MISINO].".".$skab[COL_TUJUANNO].".".$skab[COL_SASARANNO]."</p><p class='node-title'>".strtoupper($skab[COL_SASARANURAIAN])."</p>";
            $iksasaran_ = "";
            if(count($kab_iksasaran) > 0) {
                $iksasaran_ .= "<p class='node-title' style='margin: 0 !important'><strong>INDIKATOR :</strong></p><ul style='margin-left: 0px; padding-left: 15px;'>";
                foreach($kab_iksasaran as $iks) {
                    $iksasaran_ .= "<li>".strtoupper($iks[COL_SSRINDIKATORURAIAN])."</li>";
                }
                $iksasaran_ .= "</ul>";
            }
            $htmlSasaran .= $iksasaran_;

            $arrSasaranOPD = array();
            $sasaranOPD = $this->db
            ->select(TBL_SAKIPV2_SKPD.'.'.COL_SKPDID.','.TBL_SAKIPV2_SKPD.'.'.COL_SKPDNAMA)
            ->join(TBL_SAKIPV2_PEMDA_SASARAN,TBL_SAKIPV2_PEMDA_SASARAN.'.'.COL_SASARANID." = ".TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.".".COL_IDSASARANPMD,"inner")
            ->join(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN,TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.".".COL_IDTUJUAN,"inner")
            ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.".".COL_IDRENSTRA,"inner")
            ->join(TBL_SAKIPV2_SKPD,TBL_SAKIPV2_SKPD.'.'.COL_SKPDID." = ".TBL_SAKIPV2_SKPD_RENSTRA.".".COL_IDSKPD,"inner")
            ->where(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_IDTUJUANPMD, $tujuankab[COL_TUJUANID])
            ->where(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.'.'.COL_IDSASARANPMD, $skab[COL_SASARANID])
            ->where(TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAISAKTIF, 1)
            ->group_by(TBL_SAKIPV2_SKPD.'.'.COL_SKPDID, TBL_SAKIPV2_SKPD.'.'.COL_SKPDNAMA)
            ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN)
            ->result_array();

            $htmlSasaranOPD = "<p class='node-name'>OPD PENANGGUNG JAWAB</p>";
            if(count($sasaranOPD) > 0) {
              $htmlSasaranOPD .= "<ul style='padding-left: 1rem'>";
              foreach($sasaranOPD as $i_) {
                $_idSKPD=$i_[COL_SKPDID];
                $_idSasaranPmd=$skab[COL_SASARANID];
                //$htmlSasaranOPD .= "<li><a href='".site_url('sakipv2/laporan/index/skpd-cascading').'?idSKPD='.$i_[COL_SKPDID]."' target='_blank'>".$i_[COL_SKPDNAMA]."</a></li>";
                $htmlSasaranOPD .= "<li><a href='javascript:showSwalSKPD($_idSKPD, $_idSasaranPmd)'>".$i_[COL_SKPDNAMA]."</a></li>";
              }
              $htmlSasaranOPD .= "</ul>";
            } else {
              $htmlSasaranOPD .= "<p class='node-title' style='font-style: italic; text-align: center'>(KOSONG)</p>";
            }
            $arrSasaranOPD[] = array("innerHTML" => $htmlSasaranOPD,
                "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                "HTMLclass" => "bg-white"
            );

            $arrKabSasaran[] = array(
                "innerHTML" => $htmlSasaran,
                "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                "children" => $arrSasaranOPD,
                "HTMLclass" => "bg-orange node-wide"
            );
        }

        $arrKabTujuan[] = array(
            "innerHTML" => $htmlTujuan,
            "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
            "children" => $arrKabSasaran,
            "HTMLclass" => "bg-lime node-wide"
        );
    }

    $arrMisi[] = array(
        "text" => array("name"=> "MISI ".$m[COL_MISINO], "title"=> strtoupper($m[COL_MISIURAIAN])),
        "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
        "children" => $arrKabTujuan,
        "HTMLclass" => "bg-aqua"
    );
}
$nodes = array(
    "text" => array("name"=> "VISI", "title"=> $rpemda[COL_PMDVISI]),
    "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
    "children" => $arrMisi,
    "HTMLclass" => "bg-fuchsia"
);
?>
<script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/raphael.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/treant/Treant.js"></script>
<link href="<?=base_url()?>assets/treant/Treant.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="<?=base_url()?>assets/treant/vendor/perfect-scrollbar/perfect-scrollbar.css">
<script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
<style>
  .nodeExample1 {
      border: 1px solid #000;
      padding : 0px !important;
      width : 25vh !important;
      font-size: 8pt;
      color: #000 !important;
  }
  .node-wide {
      width : 32vh !important;
  }
  .nodeExample1 .node-name {
      font-weight: bold;
      margin: 0 0 5px !important;
      border-bottom: 1px solid #000;
      padding: 2px;
  }
  .nodeExample1 .node-title {
      #text-align: justify;
      padding: 2px;
  }
  .chart {
      overflow: auto;
  }
  .bg-greenlight {
      background-color: #c4dd39 !important;
  }
  .node-title {
      padding: 5px !important;
  }
  .nodeExample1 ul {
      margin-left: 5px !important;
  }
  .bg-lime {
    background-color: #01ff7054 !important;
  }
  .bg-teal {
    background-color: #39cccc66 !important;
  }
  .bg-aqua {
    background-color: #00c0ef61 !important;
  }
  .bg-orange {
    background-color: #ff851b73 !important;
  }
  .bg-fuchsia {
    background-color: #f012be73 !important;
  }
</style>

<div style="padding: 1rem; text-align: right">
<a id="btn-download" style="display: none" download="SITALAKBAJAKUN - CASCADING PEMERINTAH DAERAH PERIODE <?=$rpemda[COL_PMDTAHUNMULAI].' s.d '.$rpemda[COL_PMDTAHUNAKHIR].' ('.strtoupper($rpemda[COL_PMDPEJABAT]).')'?>.jpg" class="btn btn-primary btn-sm" href="">DOWNLOAD</a>
</div>
<div id="chart">
<h4 style="text-align: center; font-size: 14pt !important; margin-top: 10px !important">
  CASCADING KINERJA<br />
  <strong>KABUPATEN BATU BARA</strong><br />
  <small style="font-style: italic">Dicetak melalui aplikasi <strong><?=$this->setting_web_name.' - '.$this->setting_web_desc?></strong></small>
</h4><hr />
  <div class="chart" id="basic-example">

  </div>
</div>
<div id="canvas" style="display: none">

</div>
<script src="<?=base_url()?>assets/js/html2canvas.min.js"></script>
<script>
function showSwalSKPD(idSKPD, idSasaranPmd) {
  const link1 = document.createElement('a');
  const link2 = document.createElement('a');
  link1.innerHTML = 'CASCADING SKPD';
  link1.className = 'btn btn-block btn-primary';
  link1.target = '_blank';
  link1.href = "<?=site_url('sakipv2/laporan/index/skpd-cascading')?>?idSKPD="+idSKPD;
  link2.innerHTML = 'DPA SKPD';
  link2.className = 'btn btn-block btn-success';
  link2.target = '_blank';
  link2.href = "<?=site_url('sakipv2/laporan/dpa')?>?idSKPD="+idSKPD+"&idSasaranPmd="+idSasaranPmd;

  const container = document.createElement("div");
  // You could also use container.innerHTML to set the content.
  container.append(link1);
  container.append(link2);

  swal("PILIH NAVIGASI", {
    buttons: false,
    content: container
  });
}
  var chart_config = {
      chart: {
          container: "#basic-example",
          scrollbar: "fancy",
          //animateOnInit: true,
          rootOrientation:  'WEST', // NORTH || EAST || WEST || SOUTH
          connectors: {
              type: "step",
              style: {
                  "stroke-width": 1
              }
          },
          node: {
              HTMLclass: 'nodeExample1'
          },
          nodeAlign: 'TOP',
          hideRootNode: false
          /*animation: {
           nodeAnimation: "easeOutBounce",
           nodeSpeed: 700,
           connectorsAnimation: "bounce",
           connectorsSpeed: 700
           }*/
      },
      nodeStructure: <?=json_encode($nodes)?>
  };
  new Treant( chart_config );
  html2canvas(document.querySelector("#chart"), {scale: 1.75}).then(canvas => {
      document.getElementById("canvas").appendChild(canvas);
      var img = canvas.toDataURL("image/jpg");
      $("#btn-download").attr("href", img).show();
  });
</script>
