<?php
$rOptPmd = $this->db
->order_by(COL_PMDISAKTIF,'desc')
->order_by(COL_PMDTAHUNMULAI,'desc')
->get(TBL_SAKIPV2_PEMDA)
->result_array();

$getPmd = null;
if(!empty($_GET['idPmd'])) $getPmd = $_GET['idPmd'];
else if(!empty($rOptPmd)) $getPmd = $rOptPmd[0][COL_PMDID];

if(empty($getPmd)) {
  show_error('PARAMETER TIDAK VALID');
  exit();
}

$rpemda = $this->db
->where(COL_PMDID, $getPmd)
->get(TBL_SAKIPV2_PEMDA)
->row_array();

$rsasaran = $this->db
->join(TBL_SAKIPV2_PEMDA_TUJUAN,TBL_SAKIPV2_PEMDA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_PEMDA_SASARAN.".".COL_IDTUJUAN,"inner")
->join(TBL_SAKIPV2_PEMDA_MISI,TBL_SAKIPV2_PEMDA_MISI.'.'.COL_MISIID." = ".TBL_SAKIPV2_PEMDA_TUJUAN.".".COL_IDMISI,"inner")
->where(COL_IDPMD, $rpemda[COL_PMDID])
->order_by(COL_MISINO)
->order_by(COL_TUJUANNO)
->order_by(COL_SASARANNO)
->get(TBL_SAKIPV2_PEMDA_SASARAN)
->result_array();

$thn = date('Y');
$qprogram = @"
select prg.*, sum(subkeg.SubkegPagu) as Pagu
from sakipv2_bid_program prg
left join sakipv2_skpd_renstra_dpa dpa on dpa.DPAId=prg.IdDPA
left join sakipv2_bid_kegiatan keg on keg.IdProgram=prg.ProgramId
left join sakipv2_subbid_subkegiatan subkeg on subkeg.IdKegiatan=keg.KegiatanId
where dpa.DPATahun = $thn
group by prg.ProgramKode
order by prg.ProgramKode
";
$rprogram = $this->db
->query($qprogram)
->result_array();

if(!empty($isCetak) && $isCetak==1) {

} else {
  ?>
  <div class="row p-3">
    <div class="col-lg-12 text-center">
      <a href="<?=site_url('sakipv2/laporan/index/pemda-pk-cetak').'?idPmd='.$idPmd?>" class="btn btn-outline-primary btn-sm" target="_blank">
        <i class="far fa-print"></i>&nbsp;&nbsp;CETAK
      </a>
    </div>
  </div>
  <?php
}
?>
<?php
if(!empty($isCetak) && $isCetak==1) {
  ?>
  <html>
  <head>
    <style>
    body {
      font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
    }
    th, td {
      padding: 5px;
    }
    table {
      width: 100%;
      border-collapse: collapse;
    }
    table, th, td {
      /*border: 1px solid black !important;*/
    }
    </style>
  </head>
  <body style="padding-top: 40px">
    <p style="text-align: center">
      <img src="<?=MY_IMAGEURL.'logo-garuda.png'?>" style="height: 100px" />
    </p>
    <p style="text-align: center; font-weight: bold">
      PERJANJIAN KINERJA TAHUN <?=date('Y')?>
    </p>

    <table width="100%">
      <tr>
        <td colspan="2" style="text-align: justify; vertical-align: top">
          <p>Dalam rangka mewujudkan manajemen pemerintahan yang efektif, transparan, dan akuntabel serta berorientasi pada hasil, yang bertanda tangan dibawah ini:</p><br />
          <table>
            <tr>
              <td style="padding-left: 25px; width: 10px; white-space: nowrap">Nama</td>
              <td style="width:60px; text-align: right">:</td>
              <td style="font-weight: bold"><?=$rpemda[COL_PMDPEJABAT]?></td>
            </tr>
            <tr>
              <td style="padding-left: 25px; width: 10px; white-space: nowrap">Jabatan</td>
              <td style="width:60px; text-align: right">:</td>
              <td style="font-weight: bold">WALI KABUPATEN BATU BARA</td>
            </tr>
          </table>
          <br />
          <p style="margin-top: 15px">
            berjanji akan mewujudkan target kinerja yang seharusnya sesuai lampiran perjanjian ini, dalam rangka mencapai target kinerja jangka menengah seperti yang telah ditetapkan dalam dokumen perencanaan.
          </p>
          <br />
          <p>
            Keberhasilan dan kegagalan pencapaian target kinerja tersebut menjadi tanggung jawab kami.
          </p>
        </td>
      </tr>
    </table>
    <br />
    <br />
    <table width="100%">
      <tr>
        <td style="width: 50%"></td>
        <td style="text-align: center">
          <strong>
            Tebing Tinggi,
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <?=date('Y')?>
          </strong>
        </td>
      </tr>
      <tr>
          <td style="text-align: center"></td>
          <td style="text-align: center"><strong>WALI KABUPATEN BATU BARA</strong></td>
      </tr>
      <tr>
          <td colspan="2">
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
          </td>
      </tr>
      <tr>
          <td style="text-align: center"></td>
          <td style="text-align: center"><strong><?=$rpemda[COL_PMDPEJABAT]?></strong></td>
      </tr>
    </table>
    <pagebreak></pagebreak>
    <p style="text-align: center; font-weight: bold">
      PERJANJIAN KINERJA<br />PEMERINTAH KABUPATEN BATU BARA TAHUN <?=date('Y')?>
    </p>
    <table>
      <tr>
        <td style="padding-left: 25px; width: 10px; white-space: nowrap">Provinsi / Kota</td>
        <td style="width:60px; text-align: right">:</td>
        <td style="font-weight: bold">SUMATERA UTARA / TEBING TINGGI</td>
      </tr>
      <tr>
        <td style="padding-left: 25px; width: 10px; white-space: nowrap">Tahun Anggaran</td>
        <td style="width:60px; text-align: right">:</td>
        <td style="font-weight: bold"><?=date('Y')?></td>
      </tr>
    </table>
    <br />
    <table style="border: 1px solid #000; border-spacing: 0" border="1" width="100%">
      <thead>
        <tr style="background: #dedede">
            <th>No.</th>
            <th>SASARAN STRATEGIS</th>
            <th>INDIKATOR</th>
            <th>SATUAN</th>
            <th>TARGET</th>
        </tr>
        <tr style="background: #dedede">
            <th>(1)</th>
            <th>(2)</th>
            <th>(3)</th>
            <th>(4)</th>
            <th>(5)</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $nos=1;
        foreach($rsasaran as $s) {
          $rindikator = $this->db
          ->where(COL_IDSASARAN, $s[COL_SASARANID])
          ->get(TBL_SAKIPV2_PEMDA_SASARANDET)
          ->result_array();
          $rowspan = !empty($rindikator)&&count($rindikator)>1?count($rindikator):0;
          ?>
          <tr>
            <td style="width: 10px; white-space: nowrap; text-align: right; vertical-align: top" <?=$rowspan>0?"rowspan=$rowspan":""?>>
              <?=$nos?>.
            </td>
            <td style="vertical-align: top" <?=$rowspan>0?"rowspan=$rowspan":""?>>
              <?=$s[COL_SASARANURAIAN]?>
            </td>
            <td style="vertical-align: top">
              <?=!empty($rindikator)?strtoupper($rindikator[0][COL_SSRINDIKATORURAIAN]):'-'?>
            </td>
            <td style="white-space: nowrap; vertical-align: top">
              <?=!empty($rindikator)&&!empty($rindikator[0][COL_SSRINDIKATORSATUAN])?$rindikator[0][COL_SSRINDIKATORSATUAN]:'-'?>
            </td>
            <td style="white-space: nowrap; vertical-align: top">
              <?=!empty($rindikator)&&!empty($rindikator[0][COL_SSRINDIKATORTARGET])?$rindikator[0][COL_SSRINDIKATORTARGET]:'-'?>
            </td>
          </tr>
          <?php
          if(!empty($rindikator)&&count($rindikator)>1) {
            for($i=1; $i<count($rindikator); $i++) {
              ?>
              <tr>
                <td style="vertical-align: top">
                  <?=!empty($rindikator)?strtoupper($rindikator[$i][COL_SSRINDIKATORURAIAN]):'-'?>
                </td>
                <td style="white-space: nowrap; vertical-align: top">
                  <?=!empty($rindikator)&&!empty($rindikator[$i][COL_SSRINDIKATORSATUAN])?$rindikator[$i][COL_SSRINDIKATORSATUAN]:'-'?>
                </td>
                <td style="white-space: nowrap; vertical-align: top">
                  <?=!empty($rindikator)&&!empty($rindikator[$i][COL_SSRINDIKATORTARGET])?$rindikator[$i][COL_SSRINDIKATORTARGET]:'-'?>
                </td>
              </tr>
              <?php
            }
          }
          $nos++;
        }
        ?>
        <tr style="border: none">
          <td colspan="5">
            <table width="100%" style="margin-top: 50px !important">
              <tr>
                  <td style="width: 50%"></td>
                  <td style="text-align: center"><strong>WALI KABUPATEN BATU BARA</strong></td>
              </tr>
              <tr>
                  <td colspan="2">
                      <br />
                      <br />
                      <br />
                      <br />
                      <br />
                      <br />
                  </td>
              </tr>
              <tr>
                  <td style="text-align: center"></td>
                  <td style="text-align: center"><strong><?=$rpemda[COL_PMDPEJABAT]?></strong></td>
              </tr>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
    <pagebreak></pagebreak>
    <table style="border: 1px solid #000; border-spacing: 0" border="1" width="100%">
      <thead>
        <tr style="background: #dedede">
            <th>No.</th>
            <th>PROGRAM</th>
            <th>ANGGARAN (Rp.)</th>
        </tr>
        <tr style="background: #dedede">
            <th>(1)</th>
            <th>(2)</th>
            <th>(3)</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $nop=1;
        $sumPrg=0;
        foreach($rprogram as $prg) {
          ?>
          <tr>
            <td style="width: 10px; white-space: nowrap; text-align: right; vertical-align: top">
              <?=$nop?>.
            </td>
            <td style="vertical-align: top">
              <?=strtoupper(trim($prg[COL_PROGRAMURAIAN]))?>
            </td>
            <td style="vertical-align: middle; text-align: right;">
              <?=number_format($prg['Pagu'])?>
            </td>
          </tr>
          <?php
          $nop++;
          $sumPrg+=$prg['Pagu'];
        }
        ?>
        <tr style="background: #dedede">
          <td style="text-align: right; vertical-align: top; font-weight: bold; font-style: italic" colspan="2">
            TOTAL
          </td>
          <td style="vertical-align: middle; text-align: right; font-style: italic">
            <strong><?=number_format($sumPrg)?></strong>
          </td>
        </tr>
        <tr style="border: none">
          <td colspan="3">
            <table width="100%" style="margin-top: 50px !important">
              <tr>
                  <td style="width: 50%"></td>
                  <td style="text-align: center"><strong>WALI KABUPATEN BATU BARA</strong></td>
              </tr>
              <tr>
                  <td colspan="2">
                      <br />
                      <br />
                      <br />
                      <br />
                      <br />
                      <br />
                  </td>
              </tr>
              <tr>
                  <td style="text-align: center"></td>
                  <td style="text-align: center"><strong><?=$rpemda[COL_PMDPEJABAT]?></strong></td>
              </tr>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
  </body>
  <?php
}
?>
