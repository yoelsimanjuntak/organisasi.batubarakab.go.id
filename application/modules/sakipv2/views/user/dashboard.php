<?php
$ruser = GetLoggedUser();
$rskpd = array();
if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEGUEST) {
  $rskpd = $this->db->where(COL_SKPDID, $ruser[COL_SKPDID])->get(TBL_SAKIPV2_SKPD)->row_array();
}

$rpemda = $this->db
->where(COL_PMDISAKTIF, 1)
->order_by(COL_PMDTAHUNMULAI, 'desc')
->get(TBL_SAKIPV2_PEMDA)
->row_array();

$rmisi = array();
$rtujuan = array();
$rsasaran = array();
if(!empty($rpemda)) {
  $rmisi = $this->db
  ->where(COL_IDPMD, $rpemda[COL_PMDID])
  ->order_by(COL_MISINO, 'asc')
  ->get(TBL_SAKIPV2_PEMDA_MISI)
  ->result_array();

  $rtujuan = $this->db
  ->join(TBL_SAKIPV2_PEMDA_MISI,TBL_SAKIPV2_PEMDA_MISI.'.'.COL_MISIID." = ".TBL_SAKIPV2_PEMDA_TUJUAN.".".COL_IDMISI,"left")
  ->where(COL_IDPMD, $rpemda[COL_PMDID])
  ->order_by(COL_TUJUANNO, 'asc')
  ->get(TBL_SAKIPV2_PEMDA_TUJUAN)
  ->result_array();

  $rsasaran = $this->db
  ->join(TBL_SAKIPV2_PEMDA_TUJUAN,TBL_SAKIPV2_PEMDA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_PEMDA_SASARAN.".".COL_IDTUJUAN,"left")
  ->join(TBL_SAKIPV2_PEMDA_MISI,TBL_SAKIPV2_PEMDA_MISI.'.'.COL_MISIID." = ".TBL_SAKIPV2_PEMDA_TUJUAN.".".COL_IDMISI,"left")
  ->where(COL_IDPMD, $rpemda[COL_PMDID])
  ->order_by(COL_SASARANNO, 'asc')
  ->get(TBL_SAKIPV2_PEMDA_SASARAN)
  ->result_array();
}
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?=$title?></h3>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <?php
          if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEGUEST) {
            ?>
            <li class="breadcrumb-item active font-italic"><?=strtoupper($rskpd[COL_SKPDNAMA])?></li>
            <?php
          }
          ?>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <?php
      if($ruser[COL_ROLEID]==ROLEADMIN || $ruser[COL_ROLEID]==ROLEGUEST) {
        $rbidang = array();
        $rsubbidang = array();
        $rskpd = $this->db
        ->where(COL_SKPDISAKTIF, 1)
        ->order_by(COL_SKPDNAMA, 'asc')
        ->get(TBL_SAKIPV2_SKPD)
        ->result_array();

        if(!empty($rpemda)) {
          $rbidang = $this->db
          ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_BID.".".COL_IDRENSTRA,"left")
          ->where(COL_IDPEMDA, $rpemda[COL_PMDID])
          ->where(COL_BIDISAKTIF, 1)
          ->order_by(COL_BIDNAMA, 'asc')
          ->get(TBL_SAKIPV2_BID)
          ->result_array();

          $rsubbidang = $this->db
          ->join(TBL_SAKIPV2_BID,TBL_SAKIPV2_BID.'.'.COL_BIDID." = ".TBL_SAKIPV2_SUBBID.".".COL_IDBID,"left")
          ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_BID.".".COL_IDRENSTRA,"left")
          ->where(COL_IDPEMDA, $rpemda[COL_PMDID])
          ->where(COL_SUBBIDISAKTIF, 1)
          ->order_by(COL_SUBBIDNAMA, 'asc')
          ->get(TBL_SAKIPV2_SUBBID)
          ->result_array();
        }

        ?>
        <div class="col-lg-4 col-4">
          <div class="small-box bg-success">
            <div class="inner">
              <h5 class="font-weight-bold">PERIODE PEMERINTAHAN</h5>
              <p class="font-italic text-sm"><?=!empty($rpemda)?'<span style="text-decoration: underline">TH. '.$rpemda[COL_PMDTAHUNMULAI].' - '.$rpemda[COL_PMDTAHUNAKHIR].'</span><br />'.$rpemda[COL_PMDPEJABAT].'<br />'.$rpemda[COL_PMDPEJABATWAKIL]:'(KOSONG)'?></p>
            </div>
            <div class="icon">
              <i class="far fa-university"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-4">
          <div class="small-box bg-primary">
            <div class="inner">
              <h5 class="font-weight-bold">RPJMD</h5>
              <p class="font-italic text-sm"><?=!empty($rpemda)?'<strong>'.number_format(count($rmisi)).'</strong> MISI<br /><strong>'.number_format(count($rtujuan)).'</strong> TUJUAN<br /><strong>'.number_format(count($rsasaran)).'</strong> SASARAN':'(KOSONG)'?></p>
            </div>
            <div class="icon">
              <i class="far fa-building"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-4">
          <div class="small-box bg-indigo">
            <div class="inner">
              <h5 class="font-weight-bold">STRUKTUR ORGANISASI</h5>
              <p class="font-italic text-sm"><?=!empty($rpemda)?'<strong>'.number_format(count($rskpd)).'</strong> SKPD<br /><strong>'.number_format(count($rbidang)).'</strong> BIDANG / BAGIAN<br /><strong>'.number_format(count($rsubbidang)).'</strong> SUB BIDANG / SUB BAGIAN / SEKSI':'(KOSONG)'?></p>
            </div>
            <div class="icon">
              <i class="far fa-sign"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-12 col-12">
          <div class="card card-warning">
            <div class="card-header">
              <h4 class="card-title font-weight-bold" style="text-decoration: underline">REKAPITULASI KINERJA SKPD</h4>
            </div>
            <div class="card-body">
              <table class="table table-bordered table-condensed table-responsive text-sm" id="tbl-opd">
                <thead>
                  <tr>
                    <th>SKPD</th>
                    <th>RENSTRA</th>
                    <th>DPA</th>
                    <th>PAGU (Rp.)</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  foreach($rskpd as $opd) {
                    $rdpa = array();
                    $ctujuan=0;
                    $csasaran=0;
                    $cprogram=0;
                    $ckegiatan=0;
                    $csubkeg=0;
                    $rpagu=null;

                    $rrenstra = $this->db
                    ->where(COL_RENSTRAISAKTIF,1)
                    ->where(COL_IDSKPD,$opd[COL_SKPDID])
                    ->where(COL_IDPEMDA,$rpemda[COL_PMDID])
                    ->order_by(COL_RENSTRATAHUN, 'desc')
                    ->get(TBL_SAKIPV2_SKPD_RENSTRA)
                    ->row_array();

                    if(!empty($rrenstra)) {
                      $rdpa = $this->db
                      ->where(COL_DPAISAKTIF, 1)
                      ->where(COL_IDRENSTRA,$rrenstra[COL_RENSTRAID])
                      ->order_by(COL_DPATAHUN, 'desc')
                      ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
                      ->row_array();

                      $ctujuan = $this->db
                      ->where(COL_IDRENSTRA,$rrenstra[COL_RENSTRAID])
                      ->count_all_results(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN);

                      $csasaran = $this->db
                      ->join(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN,TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.".".COL_IDTUJUAN,"left")
                      ->where(COL_IDRENSTRA,$rrenstra[COL_RENSTRAID])
                      ->count_all_results(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN);
                    }
                    if(!empty($rdpa)) {
                      $cprogram = $this->db
                      ->where(COL_IDDPA,$rdpa[COL_DPAID])
                      ->count_all_results(TBL_SAKIPV2_BID_PROGRAM);

                      $ckegiatan = $this->db
                      ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
                      ->where(COL_IDDPA,$rdpa[COL_DPAID])
                      ->count_all_results(TBL_SAKIPV2_BID_KEGIATAN);

                      $csubkeg = $this->db
                      ->join(TBL_SAKIPV2_BID_KEGIATAN,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANID." = ".TBL_SAKIPV2_SUBBID_SUBKEGIATAN.".".COL_IDKEGIATAN,"left")
                      ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
                      ->where(COL_IDDPA,$rdpa[COL_DPAID])
                      ->count_all_results(TBL_SAKIPV2_SUBBID_SUBKEGIATAN);

                      $rpagu = $this->db
                      ->select_sum(COL_SUBKEGPAGU)
                      ->join(TBL_SAKIPV2_BID_KEGIATAN,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANID." = ".TBL_SAKIPV2_SUBBID_SUBKEGIATAN.".".COL_IDKEGIATAN,"left")
                      ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
                      ->where(COL_IDDPA,$rdpa[COL_DPAID])
                      ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
                      ->row_array();
                    }
                    ?>
                    <tr>
                      <td><?=strtoupper($opd[COL_SKPDNAMA])?></td>
                      <td style="white-space: nowrap">
                        <p class="text-sm mb-0">
                          <strong style="text-decoration: underline"><?=!empty($rrenstra)?'TH. '.$rrenstra[COL_RENSTRATAHUN].' - '.strtoupper($rrenstra[COL_RENSTRAURAIAN]):'-'?></strong><br />
                          <?php
                          if(!empty($rrenstra)) {
                            ?>
                            <a href="<?=site_url('sakipv2/skpd/index').'?opr=detail-renstra&id='.$rrenstra[COL_RENSTRAID]?>" target="_blank"><strong><?=number_format($ctujuan)?></strong></a> TUJUAN /
                            <a href="<?=site_url('sakipv2/skpd/index').'?opr=detail-renstra&id='.$rrenstra[COL_RENSTRAID]?>" target="_blank"><strong><?=number_format($csasaran)?></strong></a> SASARAN
                            <?php
                          }
                          ?>
                        </p>
                      </td>
                      <td style="white-space: nowrap">
                        <p class="text-sm mb-0">
                          <strong style="text-decoration: underline"><?=!empty($rdpa)?'TH. '.$rdpa[COL_DPATAHUN].' - '.strtoupper($rdpa[COL_DPAURAIAN]):'-'?></strong><br />
                          <?php
                          if(!empty($rrenstra)) {
                            ?>
                            <a href="<?=site_url('sakipv2/skpd/index').'?opr=detail-renstra&id='.$rrenstra[COL_RENSTRAID]?>" target="_blank"><strong><?=number_format($cprogram)?></strong></a> PROGRAM /
                            <a href="<?=site_url('sakipv2/skpd/index').'?opr=detail-renstra&id='.$rrenstra[COL_RENSTRAID]?>" target="_blank"><strong><?=number_format($ckegiatan)?></strong></a> KEGIATAN /
                            <a href="<?=site_url('sakipv2/skpd/index').'?opr=detail-renstra&id='.$rrenstra[COL_RENSTRAID]?>" target="_blank"><strong><?=number_format($csubkeg)?></strong></a> SUB KEGIATAN
                            <?php
                          }
                          ?>
                        </p>
                      </td>
                      <td style="white-space: nowrap">
                        <p class="mb-0 text-right">
                          <?=number_format(!empty($rpagu)?$rpagu[COL_SUBKEGPAGU]:0)?>
                        </p>
                      </td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <?php
      } else {
        $rrenstra = array();
        $rdpa = array();
        $rprogram = array();
        $rkegiatan = array();
        $rsubkeg = array();
        $rtujuanskpd = array();
        $rsasaranskpd = array();
        if(!empty($rpemda)) {
          $rrenstra = $this->db
          ->where(COL_IDSKPD, $ruser[COL_SKPDID])
          ->where(COL_IDPEMDA, $rpemda[COL_PMDID])
          ->where(COL_RENSTRAISAKTIF, 1)
          ->order_by(COL_RENSTRATAHUN, 'desc')
          ->get(TBL_SAKIPV2_SKPD_RENSTRA)
          ->row_array();

          if(!empty($rrenstra)) {
            $rtujuanskpd = $this->db
            ->where(COL_IDRENSTRA, $rrenstra[COL_RENSTRAID])
            ->order_by(COL_TUJUANNO, 'asc')
            ->get(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN)
            ->result_array();

            $rsasaranskpd = $this->db
            ->join(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN,TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.".".COL_IDTUJUAN,"left")
            ->where(COL_IDRENSTRA, $rrenstra[COL_RENSTRAID])
            ->order_by(COL_TUJUANNO, 'asc')
            ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN)
            ->result_array();

            $rdpa = $this->db
            ->where(COL_IDRENSTRA, $rrenstra[COL_RENSTRAID])
            ->where(COL_DPAISAKTIF, 1)
            ->order_by(COL_DPATAHUN, 'desc')
            ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
            ->row_array();
            if(!empty($rdpa)) {
              $rprogram = $this->db
              ->where(COL_IDDPA, $rdpa[COL_DPAID])
              ->get(TBL_SAKIPV2_BID_PROGRAM)
              ->result_array();

              $rkegiatan = $this->db
              ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
              ->where(COL_IDDPA, $rdpa[COL_DPAID])
              ->get(TBL_SAKIPV2_BID_KEGIATAN)
              ->result_array();

              $rsubkeg = $this->db
              ->join(TBL_SAKIPV2_BID_KEGIATAN,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANID." = ".TBL_SAKIPV2_SUBBID_SUBKEGIATAN.".".COL_IDKEGIATAN,"left")
              ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
              ->where(COL_IDDPA, $rdpa[COL_DPAID])
              ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
              ->result_array();
            }
          }
        }
        ?>
        <div class="col-lg-4 col-4">
          <div class="small-box bg-success">
            <div class="inner">
              <h5 class="font-weight-bold">PERIODE PEMERINTAHAN</h5>
              <p class="font-italic text-sm"><?=!empty($rpemda)?'<span style="text-decoration: underline">TH. '.$rpemda[COL_PMDTAHUNMULAI].' - '.$rpemda[COL_PMDTAHUNAKHIR].'</span><br />'.$rpemda[COL_PMDPEJABAT].'<br />'.$rpemda[COL_PMDPEJABATWAKIL].'<br />&nbsp;':'(KOSONG)'?></p>
            </div>
            <div class="icon">
              <i class="far fa-university"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-4">
          <div class="small-box bg-primary">
            <div class="inner">
              <h5 class="font-weight-bold">RENSTRA AKTIF</h5>
              <p class="font-italic text-sm">
                <?=!empty($rrenstra)?'<span style="text-decoration: underline">TH. '.$rrenstra[COL_RENSTRATAHUN].' - '.$rrenstra[COL_RENSTRAURAIAN].'</span><br />
                                        <strong>'.number_format(count($rtujuanskpd)).'</strong> TUJUAN<br />
                                        <strong>'.number_format(count($rsasaranskpd)).'</strong> SASARAN<br />&nbsp;':'(KOSONG)'?>
              </p>
            </div>
            <div class="icon">
              <i class="far fa-building"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-4">
          <div class="small-box bg-indigo">
            <div class="inner">
              <h5 class="font-weight-bold">DPA AKTIF</h5>
              <p class="font-italic text-sm">
                <?=!empty($rrenstra)?'<span style="text-decoration: underline">TH. '.$rdpa[COL_DPATAHUN].' - '.$rdpa[COL_DPAURAIAN].'</span><br />
                                      <strong>'.number_format(count($rprogram)).'</strong> PROGRAM<br />
                                      <strong>'.number_format(count($rkegiatan)).'</strong> KEGIATAN<br />
                                      <strong>'.number_format(count($rsubkeg)).'</strong> SUB KEGIATAN':'(KOSONG)'?>
              </p>
            </div>
            <div class="icon">
              <i class="far fa-file"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-12 col-12">
          <div class="card card-warning">
            <div class="card-header">
              <h4 class="card-title font-weight-bold" style="text-decoration: underline">REKAPITULASI KINERJA SKPD</h4>
            </div>
            <div class="card-body p-0">
              <table class="table table-bordered table-condensed text-sm">
                <thead>
                  <tr>
                    <th>UNIT KERJA</th>
                    <th>DPA</th>
                    <th>PAGU (Rp.)</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                $rbid=array();
                $rsubbid=array();
                if(!empty($rrenstra)) {
                  $rbid = $this->db
                  ->where(COL_IDRENSTRA, $rrenstra[COL_RENSTRAID])
                  ->get(TBL_SAKIPV2_BID)
                  ->result_array();

                  $rsubbid = $this->db
                  ->join(TBL_SAKIPV2_BID,TBL_SAKIPV2_BID.'.'.COL_BIDID." = ".TBL_SAKIPV2_SUBBID.".".COL_IDBID,"left")
                  ->where(COL_IDRENSTRA, $rrenstra[COL_RENSTRAID])
                  ->get(TBL_SAKIPV2_SUBBID)
                  ->result_array();
                }
                foreach($rbid as $bid) {
                  $cprogram=0;
                  $ckegiatan=0;
                  $csubkeg=0;
                  $rpagu=null;
                  if(!empty($rdpa)) {
                    $cprogram = $this->db
                    ->where(COL_IDDPA,$rdpa[COL_DPAID])
                    ->where(COL_IDBID,$bid[COL_BIDID])
                    ->count_all_results(TBL_SAKIPV2_BID_PROGRAM);

                    $ckegiatan = $this->db
                    ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
                    ->where(COL_IDDPA,$rdpa[COL_DPAID])
                    ->where(COL_IDBID,$bid[COL_BIDID])
                    ->count_all_results(TBL_SAKIPV2_BID_KEGIATAN);

                    $rpagu = $this->db
                    ->select_sum(COL_SUBKEGPAGU)
                    ->join(TBL_SAKIPV2_BID_KEGIATAN,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANID." = ".TBL_SAKIPV2_SUBBID_SUBKEGIATAN.".".COL_IDKEGIATAN,"left")
                    ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
                    ->where(COL_IDDPA,$rdpa[COL_DPAID])
                    ->where(COL_IDBID,$bid[COL_BIDID])
                    ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
                    ->row_array();
                  }
                  ?>
                  <tr>
                    <td><?=strtoupper($bid[COL_BIDNAMA])?></td>
                    <td style="width: 10px; white-space: nowrap">
                      <p class="text-sm text-right mb-0">
                        <?php
                        if(!empty($rdpa)) {
                          ?>
                          <a href="<?=site_url('sakipv2/bidang/index').'?opr=detail-bidang&id='.$bid[COL_BIDID]?>" target="_blank"><strong><?=number_format($cprogram)?></strong></a> PROGRAM /
                          <a href="<?=site_url('sakipv2/bidang/index').'?opr=detail-bidang&id='.$bid[COL_BIDID]?>" target="_blank"><strong><?=number_format($ckegiatan)?></strong></a> KEGIATAN
                          <?php
                        }
                        ?>
                      </p>
                    </td>
                    <td style="white-space: nowrap">
                      <p class="mb-0 text-right">
                        <?=number_format(!empty($rpagu)?$rpagu[COL_SUBKEGPAGU]:0)?>
                      </p>
                    </td>
                  </tr>
                <?php
                }

                foreach($rsubbid as $sub) {
                  $ckegiatan=0;
                  $rpagu=null;

                  $csubkeg = $this->db
                  ->join(TBL_SAKIPV2_BID_KEGIATAN,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANID." = ".TBL_SAKIPV2_SUBBID_SUBKEGIATAN.".".COL_IDKEGIATAN,"left")
                  ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
                  ->where(COL_IDDPA,$rdpa[COL_DPAID])
                  ->where(COL_IDSUBBID,$sub[COL_SUBBIDID])
                  ->count_all_results(TBL_SAKIPV2_SUBBID_SUBKEGIATAN);

                  $rpagu = $this->db
                  ->select_sum(COL_SUBKEGPAGU)
                  ->join(TBL_SAKIPV2_BID_KEGIATAN,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANID." = ".TBL_SAKIPV2_SUBBID_SUBKEGIATAN.".".COL_IDKEGIATAN,"left")
                  ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
                  ->where(COL_IDDPA,$rdpa[COL_DPAID])
                  ->where(COL_IDSUBBID,$sub[COL_SUBBIDID])
                  ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
                  ->row_array();
                  ?>
                  <tr>
                    <td><?=strtoupper($sub[COL_SUBBIDNAMA])?></td>
                    <td style="width: 10px; white-space: nowrap">
                      <p class="text-sm text-right mb-0">
                        <?php
                        if(!empty($rdpa)) {
                          ?>
                          <a href="<?=site_url('sakipv2/subbidang/index').'?opr=detail-subbidang&id='.$sub[COL_SUBBIDID]?>" target="_blank"><strong><?=number_format($csubkeg)?></strong></a> SUB KEGIATAN
                          <?php
                        }
                        ?>
                      </p>
                    </td>
                    <td style="white-space: nowrap">
                      <p class="mb-0 text-right">
                        <?=number_format(!empty($rpagu)?$rpagu[COL_SUBKEGPAGU]:0)?>
                      </p>
                    </td>
                  </tr>
                  <?php
                }
                ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <?php
      }
      ?>

    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function() {
    var dataTable = $('#tbl-opd').dataTable({
      "autoWidth" : false,
      "iDisplayLength": 10,
      "aLengthMenu": [[10, 50, 100, -1], [10, 50, 100, "Semua"]],
    });
});
</script>
