<?php
$rOptKegiatan = array();
if(!empty($def[COL_IDDPA]) && !empty($def[COL_IDBID])) {
  $rOptKegiatan = $this->db
  ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
  ->where(COL_IDDPA, $def[COL_IDDPA])
  ->where(COL_IDBID, $def[COL_IDBID])
  ->get(TBL_SAKIPV2_BID_KEGIATAN)
  ->result_array();
} else if(!empty($data[COL_IDDPA]) && !empty($data[COL_IDBID])) {
  $rOptKegiatan = $this->db
  ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
  ->where(COL_IDDPA, $data[COL_IDDPA])
  ->where(COL_IDBID, $data[COL_IDBID])
  ->get(TBL_SAKIPV2_BID_KEGIATAN)
  ->result_array();
}
?>
<form id="form-subkegiatan" action="<?=current_url()?>">
  <input type="hidden" name="<?=COL_IDSUBBID?>" value="<?=!empty($def[COL_IDSUBBID])?$def[COL_IDSUBBID]:(!empty($data[COL_IDSUBBID])?$data[COL_IDSUBBID]:'')?>" />
  <div class="form-group">
    <label>KEGIATAN</label>
    <select name="<?=COL_IDKEGIATAN?>" class="form-control" style="width: 100%">
      <?php
      foreach($rOptKegiatan as $opt) {
        ?>
        <option value="<?=$opt[COL_KEGIATANID]?>" <?=!empty($data)&&$data[COL_KEGIATANID]==$opt[COL_KEGIATANID]?'selected':''?>><?=$opt[COL_KEGIATANKODE].' - '.$opt[COL_KEGIATANURAIAN]?></option>
        <?php
      }
      ?>
    </select>
  </div>
  <div class="form-group">
    <label>KODE</label>
    <div class="row">
      <div class="col-lg-5">
        <input type="text" class="form-control" name="<?=COL_SUBKEGKODE?>" placeholder="KODE NOMENKLATUR SUB KEGIATAN" value="<?=!empty($data)?$data[COL_SUBKEGKODE]:''?>" required />
      </div>
    </div>
  </div>
  <div class="form-group">
    <label>NOMENKLATUR</label>
    <textarea class="form-control" name="<?=COL_SUBKEGURAIAN?>" placeholder="NOMENKLATUR SUB KEGIATAN" required><?=!empty($data)?$data[COL_SUBKEGURAIAN]:''?></textarea>
  </div>
  <div class="form-group">
    <label>PAGU</label>
    <div class="row">
      <div class="col-lg-5">
        <input type="text" class="form-control uang text-right" name="<?=COL_SUBKEGPAGU?>" placeholder="JUMLAH PAGU" value="<?=!empty($data)?$data[COL_SUBKEGPAGU]:''?>" required />
      </div>
    </div>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function(){
  $('#form-subkegiatan').validate({
    submitHandler: function(form) {
      var modal = $(form).closest('modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>
