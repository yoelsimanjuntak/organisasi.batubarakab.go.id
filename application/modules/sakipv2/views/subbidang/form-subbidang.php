<form id="form-subbidang" action="<?=current_url()?>">
  <div class="form-group">
    <label>BIDANG / BAGIAN</label>
    <select class="form-control" name="<?=COL_IDBID?>" style="width: 100%">
      <?php
      foreach($rOptBidang as $opt) {
        ?>
        <option value="<?=$opt[COL_BIDID]?>" <?=!empty($data)&&$opt[COL_BIDID]==$data[COL_IDBID]?'selected':''?>>
          <?=strtoupper($opt[COL_BIDNAMA])?>
        </option>
        <?php
      }
      ?>
    </select>
  </div>
  <div class="form-group">
    <label>NAMA SUB BIDANG / SUB BAGIAN / SEKSI</label>
    <input type="text" class="form-control" placeholder="NAMA SUB BIDANG / SUB BAGIAN / SEKSI" name="<?=COL_SUBBIDNAMA?>" value="<?=!empty($data)?$data[COL_SUBBIDNAMA]:''?>" />
  </div>
  <div class="form-group">
    <label>NAMA PIMPINAN</label>
    <input type="text" class="form-control" name="<?=COL_SUBBIDNAMAPIMPINAN?>" placeholder="NAMA PIMPINAN SUB BIDANG" value="<?=!empty($data)?$data[COL_SUBBIDNAMAPIMPINAN]:''?>" required />
  </div>
  <div class="form-group">
    <label>NAMA JABATAN</label>
    <input type="text" class="form-control" name="<?=COL_SUBBIDNAMAJABATAN?>" placeholder="NAMA JABATAN PIMPINAN" value="<?=!empty($data)?$data[COL_SUBBIDNAMAJABATAN]:''?>" required />
  </div>
</form>
<script type="text/javascript">
$(document).ready(function(){
  $('#form-subbidang').validate({
    submitHandler: function(form) {
      var modal = $(form).closest('modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });

      return false;
    }
  });
});
</script>
