<?php
$rOptTujuan = $this->db
->where(COL_IDMISI, $rtujuan[COL_MISIID])
->order_by(COL_TUJUANNO)
->get(TBL_SAKIPV2_PEMDA_TUJUAN)
->result_array();

$rIndikatorTujuan = $this->db
->where(COL_IDTUJUAN, $rtujuan[COL_TUJUANID])
->get(TBL_SAKIPV2_PEMDA_TUJUANDET)
->result_array();

$rsasaran = $this->db
->where(COL_IDTUJUAN, $rtujuan[COL_TUJUANID])
->order_by(COL_SASARANNO, 'asc')
->get(TBL_SAKIPV2_PEMDA_SASARAN)
->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?=$title?></h3>
      </div>
      <?php
      if(!empty($navs)) {
        ?>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <?php
            foreach($navs as $n) {
              if(!empty($n['link'])) {
                ?>
                <li class="breadcrumb-item"><a href="<?=$n['link']?>"><?=$n['text']?></a></li>
                <?php
              } else {
                ?>
                <li class="breadcrumb-item active"><?=$n['text']?></li>
                <?php
              }
            }
            ?>
          </ol>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-outline card-secondary">
          <?php
          if(!empty($subtitle)) {
            ?>
            <div class="card-header">
              <h4 class="card-title"><?=$subtitle?></h4>
            </div>
            <?php
          }
          ?>
          <div class="card-body p-0">
            <table class="table">
              <tbody>
                <tr>
                  <td class="text-right"  style="width: 150px; white-space: nowrap">PERIODE</td>
                  <td style="width: 10px; white-space: nowrap">:</td>
                  <td>
                    <a href="<?=site_url('sakipv2/pemda/index').'?opr=detail-periode&id='.$rtujuan[COL_PMDID]?>">
                      <strong><?=$rtujuan[COL_PMDTAHUNMULAI]?> - <?=$rtujuan[COL_PMDTAHUNAKHIR]?>&nbsp; (<?=strtoupper($rtujuan[COL_PMDPEJABAT])?>)</strong>
                    </a>
                  </td>
                </tr>
                <tr>
                  <td class="text-right"  style="width: 150px; white-space: nowrap">MISI</td>
                  <td style="width: 10px; white-space: nowrap">:</td>
                  <td>
                    <a href="<?=site_url('sakipv2/pemda/index').'?opr=detail-misi&id='.$rtujuan[COL_MISIID]?>">
                      <strong><?=$rtujuan[COL_MISINO].'. '.$rtujuan[COL_MISIURAIAN]?></strong>
                    </a>
                  </td>
                </tr>
                <tr>
                  <td class="text-right" style="width: 150px; white-space: nowrap; vertical-align: middle">TUJUAN</td>
                  <td style="width: 10px; white-space: nowrap; vertical-align: middle">:</td>
                  <td>
                    <select class="form-control" name="filterTujuan">
                      <?php
                      foreach($rOptTujuan as $opt) {
                        ?>
                        <option value="<?=site_url('sakipv2/pemda/index').'?opr=detail-tujuan&id='.$opt[COL_TUJUANID]?>" <?=$opt[COL_TUJUANID]==$rtujuan[COL_TUJUANID]?'selected':''?>><?=$opt[COL_TUJUANNO].'. '.strtoupper($opt[COL_TUJUANURAIAN])?></option>
                        <?php
                      }
                      ?>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td class="text-right" style="width: 150px; white-space: nowrap">INDIKATOR TUJUAN</td>
                  <td style="width: 10px; white-space: nowrap">:</td>
                  <td>
                    <p class="mb-0">
                      <a href="<?=site_url('sakipv2/pemda/ajax-form-indikatortujuan/add/'.$rtujuan[COL_TUJUANID])?>" class="btn btn-primary btn-xs font-weight-bold" id="btnTambahIndikator"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH INDIKATOR</a>
                    </p>
                    <?php
                    if(!empty($rIndikatorTujuan)) {
                      ?>
                      <ul class="todo-list ui-sortable mt-2" data-widget="todo-list">
                        <?php
                        foreach($rIndikatorTujuan as $r) {
                          ?>
                          <li>
                            <div class="d-inline mr-2">
                              <a href="<?=site_url('sakipv2/pemda/ajax-form-indikatortujuan/delete/'.$r[COL_TUJINDIKATORID])?>" class="btn btn-danger btn-xs btn-del-indikator"><i class="far fa-times-circle"></i></a>
                              <a href="<?=site_url('sakipv2/pemda/ajax-form-indikatortujuan/edit/'.$r[COL_TUJINDIKATORID])?>" data-val="<?=$r[COL_TUJINDIKATORURAIAN]?>" class="btn btn-success btn-xs btn-edit-indikator"><i class="far fa-edit"></i></a>
                            </div>
                            <span class="text"><?=strtoupper($r[COL_TUJINDIKATORURAIAN])?></span>

                          </li>
                          <?php
                        }
                        ?>
                      </ul>
                      <?php
                    }
                    ?>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

        <div class="card card-outline card-secondary">
          <div class="card-header">
            <h4 class="card-title">DAFTAR SASARAN</h4>
          </div>
          <div class="card-body p-0">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th style="width: 10px; white-space: nowrap">NO</th>
                  <th>URAIAN SASARAN / INDIKATOR</th>
                  <th>SUMBER DATA</th>
                  <th>FORMULASI</th>
                  <th>SATUAN</th>
                  <th>TARGET</th>
                  <th style="width: 100px; white-space: nowrap">AKSI</th>
                </tr>
              </thead>
              <tbody>
                <?php
                if(!empty($rsasaran)) {
                  foreach($rsasaran as $r) {
                    $rIndikatorSasaran = $this->db
                    ->where(COL_IDSASARAN, $r[COL_SASARANID])
                    ->get(TBL_SAKIPV2_PEMDA_SASARANDET)
                    ->result_array();
                    ?>
                    <tr>
                      <td style="width: 10px; white-space: nowrap" <?=count($rIndikatorSasaran)>0?'rowspan="'.(count($rIndikatorSasaran)+1).'"':''?>><?=$r[COL_SASARANNO]?></td>
                      <td colspan="5"><?=strtoupper($r[COL_SASARANURAIAN])?></td>
                      <td class="text-center" style="white-space: nowrap" <?=count($rIndikatorSasaran)>0?'rowspan="'.(count($rIndikatorSasaran)+1).'"':''?>>
                        <a href="<?=site_url('sakipv2/pemda/ajax-form-sasaran/edit/'.$r[COL_IDTUJUAN].'/'.$r[COL_SASARANID])?>" data-toggle="tooltip" data-placement="bottom" title="UBAH" class="btn btn-primary btn-sm btn-edit-sasaran"><i class="far fa-edit"></i></a>
                        <a href="<?=site_url('sakipv2/pemda/ajax-change-sasaran/delete/'.$r[COL_SASARANID])?>" data-toggle="tooltip" data-placement="bottom" title="HAPUS" data-prompt="Menghapus data sasaran akan otomatis menghapus data INDIKATOR terkait." class="btn btn-danger btn-sm btn-change-sasaran"><i class="far fa-times-circle"></i></a>
                        <!--<a href="<?=site_url('sakipv2/pemda/index').'?opr=detail-sasaran&id='.$r[COL_SASARANID]?>" data-toggle="tooltip" data-placement="bottom" title="TELUSURI" class="btn btn-info btn-sm"><i class="far fa-search"></i></a>-->
                      </td>
                    </tr>
                    <?php
                    foreach($rIndikatorSasaran as $i) {
                      ?>
                      <tr class="text-sm">
                        <td class="font-italic" style="padding: .75rem !important"><?=strtoupper($i[COL_SSRINDIKATORURAIAN])?></td>
                        <td class="font-italic"><?=!empty($i[COL_SSRINDIKATORSUMBERDATA])?$i[COL_SSRINDIKATORSUMBERDATA]:'(kosong)'?></td>
                        <td class="font-italic"><?=!empty($i[COL_SSRINDIKATORFORMULASI])?$i[COL_SSRINDIKATORFORMULASI]:'(kosong)'?></td>
                        <td class="font-italic"><?=!empty($i[COL_SSRINDIKATORSATUAN])?$i[COL_SSRINDIKATORSATUAN]:'(kosong)'?></td>
                        <td class="font-italic text-right"><?=!empty($i[COL_SSRINDIKATORTARGET])?$i[COL_SSRINDIKATORTARGET]:'-'?></td>
                      </tr>
                      <?php
                    }
                  }
                } else {
                  ?>
                  <tr>
                    <td colspan="3">
                      <p class="text-center font-italic mb-0">
                        BELUM ADA DATA
                      </p>
                    </td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </div>
          <div class="card-footer">
            <a href="<?=site_url('sakipv2/pemda/ajax-form-sasaran/add/'.$rtujuan[COL_TUJUANID])?>" class="btn btn-primary btn-add-sasaran font-weight-bold"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH SASARAN</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modalFormSasaran" role="dialog">
  <div class="modal-dialog modal-lg" style="max-width: 1000px !important">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">FORM SASARAN PEMERINTAH DAERAH</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var modalFormSasaran = $('#modalFormSasaran');
$(document).ready(function() {
  $('select[name=filterTujuan]').change(function(){
    var url = $(this).val();
    location.href = url;
  });

  $('#btnTambahIndikator, .btn-edit-indikator').click(function(){
    var url = $(this).attr('href');
    var val = $(this).data('val');
    swal({
      closeOnClickOutside: true,
      buttons: ['BATAL','SUBMIT'],
      text: "INDIKATOR TUJUAN",
      content: {
        element: "input",
        attributes: {
          placeholder: "URAIAN INDIKATOR",
          type: "text",
          value: (val||'')
        }
      },
    }).then(function(val){
      if(val) {
        $.ajax({
          url: url,
          method: "POST",
          dataType: "json",
          data: {
            TujIndikatorUraian: val
          }
        }).success(function(res) {
          if(res.error) {
            swal({
              title: 'ERROR',
              text: res.error,
              icon: 'error',
              buttons:false
            });
          } else {
            location.reload();
          }
        }).fail(function() {
          swal({
            title: 'SERVER ERROR',
            text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
            icon: 'error',
            buttons:false
          });
        }).done(function() {

        });
      }
    });

    return false;
  });

  $('.btn-del-indikator').click(function() {
    var url = $(this).attr('href');
    swal({
      title: "APAKAH ANDA YAKIN?",
      icon: "warning",
      buttons: [
        'BATAL',
        'YAKIN'
      ],
    }).then(function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          url: url,
          method: "GET",
          dataType: "json"
        }).success(function(res) {
          if(res.error) {
            swal({
              title: 'ERROR',
              text: res.error,
              icon: 'error',
              buttons:false
            });
          } else {
            location.reload();
          }
        }).fail(function() {
          swal({
            title: 'SERVER ERROR',
            text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
            icon: 'error',
            buttons:false
          });
        }).done(function() {

        });

      } else {

      }
    })
    return false;
  });

  modalFormSasaran.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalFormSasaran).empty();
  });

  $('.btn-add-sasaran, .btn-edit-sasaran').click(function() {
    var url = $(this).attr('href');
    modalFormSasaran.modal('show');
    $('.modal-body', modalFormSasaran).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modalFormSasaran).load(url, function(){
      $("select", modalFormSasaran).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
      $('button[type=submit]', modalFormSasaran).unbind('click').click(function(){
        $('form', modalFormSasaran).submit();
      });
    });
    return false;
  });

  $('.btn-change-sasaran').click(function() {
    var url = $(this).attr('href');
    var prompt = $(this).data('prompt');
    swal({
      title: "APAKAH ANDA YAKIN?",
      text: prompt,
      icon: "warning",
      buttons: [
        'BATAL',
        'YAKIN'
      ],
    }).then(function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          url: url,
          method: "GET",
          dataType: "json"
        }).success(function(res) {
          if(res.error) {
            swal({
              title: 'ERROR',
              text: res.error,
              icon: 'error',
              buttons:false
            });
          } else {
            location.reload();
          }
        }).fail(function() {
          swal({
            title: 'SERVER ERROR',
            text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
            icon: 'error',
            buttons:false
          });
        }).done(function() {

        });

      } else {

      }
    })
    return false;
  });
});
</script>
