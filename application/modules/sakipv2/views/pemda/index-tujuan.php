<?php
$rOptMisi = $this->db
->where(COL_IDPMD, $rperiod[COL_PMDID])
->order_by(COL_MISINO)
->get(TBL_SAKIPV2_PEMDA_MISI)
->result_array();

$rtujuan = $this->db
->where(COL_IDMISI, $rmisi[COL_MISIID])
->order_by(COL_TUJUANNO)
->get(TBL_SAKIPV2_PEMDA_TUJUAN)
->result_array();

$arrIKU = array();
if(!empty($rmisi[COL_MISIIKU])) {
  $arrIKU = json_decode($rmisi[COL_MISIIKU]);
}
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?=$title?></h3>
      </div>
      <?php
      if(!empty($navs)) {
        ?>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <?php
            foreach($navs as $n) {
              if(!empty($n['link'])) {
                ?>
                <li class="breadcrumb-item"><a href="<?=$n['link']?>"><?=$n['text']?></a></li>
                <?php
              } else {
                ?>
                <li class="breadcrumb-item active"><?=$n['text']?></li>
                <?php
              }
            }
            ?>
          </ol>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-outline card-secondary">
          <?php
          if(!empty($subtitle)) {
            ?>
            <div class="card-header">
              <h4 class="card-title"><?=$subtitle?></h4>
            </div>
            <?php
          }
          ?>
          <div class="card-body p-0">
            <table class="table table-bordered">
              <table class="table table-striped">
                <tbody>
                  <tr>
                    <td class="text-right"  style="width: 150px; white-space: nowrap">PERIODE</td>
                    <td style="width: 10px; white-space: nowrap">:</td>
                    <td>
                      <a href="<?=site_url('sakipv2/pemda/index').'?opr=detail-periode&id='.$rperiod[COL_PMDID]?>">
                        <strong><?=$rperiod[COL_PMDTAHUNMULAI]?> - <?=$rperiod[COL_PMDTAHUNAKHIR]?>&nbsp; (<?=strtoupper($rperiod[COL_PMDPEJABAT])?>)</strong>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td class="text-right" style="width: 150px; white-space: nowrap">MISI</td>
                    <td style="width: 10px; white-space: nowrap">:</td>
                    <td>
                      <select class="form-control" name="filterMisi">
                        <?php
                        foreach($rOptMisi as $opt) {
                          ?>
                          <option value="<?=site_url('sakipv2/pemda/index').'?opr=detail-misi&id='.$opt[COL_MISIID]?>" <?=$opt[COL_MISIID]==$rmisi[COL_MISIID]?'selected':''?>><?=$opt[COL_MISINO].'. '.strtoupper($opt[COL_MISIURAIAN])?></option>
                          <?php
                        }
                        ?>
                      </select>
                    </td>
                  </tr>
                  <tr class="bg-white">
                    <td class="text-right" style="width: 200px; white-space: nowrap">INDIKATOR KINERJA UTAMA</td>
                    <td style="width: 10px; white-space: nowrap">:</td>
                    <td>
                      <p class="mb-0">
                        <button type="button" class="btn btn-primary btn-xs font-weight-bold" id="btnFormIKU"><i class="far fa-cog"></i>&nbsp;KELOLA IKU</button>
                      </p>
                      <?php
                      if(!empty($arrIKU)) {
                        ?>
                        <ul class="todo-list ui-sortable mt-2" data-widget="todo-list">
                          <?php
                          $idx=0;
                          foreach($arrIKU as $r) {
                            ?>
                            <li>
                              <div class="row">
                                <div class="col-lg-10">
                                  <span class="text"><?=strtoupper($r->IKUUraian)?></span>
                                </div>
                                <div class="col-lg-2">
                                  <small class="font-italic">TARGET : <strong class="pull-right font-italic"><?=$r->IKUTarget?></strong></small>
                                </div>

                              </div>

                            </li>
                            <?php
                            $idx++;
                          }
                          ?>
                        </ul>
                        <?php
                      }
                      ?>
                    </td>
                  </tr>
                </tbody>
              </table>
            </table>
          </div>
        </div>

        <div class="card card-outline card-secondary">
          <div class="card-header">
            <h4 class="card-title">DAFTAR TUJUAN</h4>
          </div>
          <div class="card-body p-0">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th style="width: 10px; white-space: nowrap">NO</th>
                  <th>URAIAN TUJUAN</th>
                  <th style="width: 100px; white-space: nowrap">AKSI</th>
                </tr>
              </thead>
              <tbody>
                <?php
                if(!empty($rtujuan)) {
                  foreach($rtujuan as $r) {
                    ?>
                    <tr>
                      <td style="width: 10px; white-space: nowrap"><?=$r[COL_TUJUANNO]?></td>
                      <td><?=strtoupper($r[COL_TUJUANURAIAN])?></td>
                      <td class="text-center" style="white-space: nowrap">
                        <a href="<?=site_url('sakipv2/pemda/ajax-form-tujuan/edit/'.$r[COL_IDMISI].'/'.$r[COL_TUJUANID])?>" data-toggle="tooltip" data-placement="bottom" title="UBAH" class="btn btn-primary btn-sm btn-edit-misi"><i class="far fa-edit"></i></a>
                        <a href="<?=site_url('sakipv2/pemda/ajax-change-tujuan/delete/'.$r[COL_TUJUANID])?>" data-toggle="tooltip" data-placement="bottom" title="HAPUS" data-prompt="Menghapus data tujuan akan otomatis menghapus data INDIKATOR dan SASARAN terkait." class="btn btn-danger btn-sm btn-change-misi"><i class="far fa-times-circle"></i></a>
                        <a href="<?=site_url('sakipv2/pemda/index').'?opr=detail-tujuan&id='.$r[COL_TUJUANID]?>" data-toggle="tooltip" data-placement="bottom" title="TELUSURI" class="btn btn-info btn-sm"><i class="far fa-search"></i></a>
                      </td>
                    </tr>
                    <?php
                  }
                } else {
                  ?>
                  <tr>
                    <td colspan="3">
                      <p class="text-center font-italic mb-0">
                        BELUM ADA DATA
                      </p>
                    </td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </div>
          <div class="card-footer">
            <a href="<?=site_url('sakipv2/pemda/ajax-form-tujuan/add/'.$rmisi[COL_MISIID])?>" class="btn btn-primary btn-add-misi font-weight-bold"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH TUJUAN</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modalFormTujuan" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">FORM TUJUAN PEMERINTAH DAERAH</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalFormIKU" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">FORM IKU</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form-iku" action="<?=site_url('sakipv2/pemda/ajax-form-iku/'.$rmisi[COL_MISIID])?>" method="POST">
            <input type="hidden" name="IKU" />
            <table id="tbl-iku" class="table table-bordered">
              <thead class="bg-default">
                <tr>
                  <th>INDIKATOR</th>
                  <th>FORMULASI</th>
                  <th style="width: 120px; white-space: nowrap">SATUAN</th>
                  <th style="width: 120px; white-space: nowrap">TARGET</th>
                  <th class="text-center" style="width: 10px; white-space: nowrap">
                    <button type="button" id="btn-add-iku" class="btn btn-sm btn-primary" style="font-weight: bold"><i class="far fa-plus-circle"></i></button>
                  </th>
                </tr>
              </thead>
              <tbody>
                <?php
                if(!empty($arrIKU)) {
                  foreach($arrIKU as $iku) {
                    ?>
                    <tr>
                      <td>
                        <textarea class="form-control" name="IKUUraian[]" placeholder="URAIAN INDIKATOR"><?=$iku->IKUUraian?></textarea>
                      </td>
                      <td>
                        <textarea class="form-control" name="IKUFormulasi[]" placeholder="FORMULASI"><?=$iku->IKUFormulasi?></textarea>
                      </td>
                      <td>
                        <select class="form-control" name="IKUSatuan[]"><?=GetCombobox("SELECT * FROM ".TBL_SAKIP_MSATUAN." ORDER BY ".COL_NM_SATUAN, COL_NM_SATUAN, COL_NM_SATUAN, $iku->IKUSatuan)?></select>
                      </td>
                      <td>
                        <input type="text" class="form-control" name="IKUTarget[]" value="<?=$iku->IKUTarget?>" />
                      </td>
                      <td>
                        <button type="button" class="btn btn-sm btn-danger btn-del-iku" style="font-weight: bold"><i class="fa fa-minus"></i></button>
                      </td>
                    </tr>
                    <?php
                  }
                } else {
                  ?>
                  <tr class="empty">
                    <td colspan="6">
                      <p class="font-italic mb-0 text-center">BELUM ADA DATA</p>
                    </td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </form>
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var modalFormTujuan = $('#modalFormTujuan');
var modalFormIKU = $('#modalFormIKU');

function addRowIKU() {
  var html =' ';
  html += '<tr>';
  html += '<td>';
  html += '<textarea class="form-control" name="IKUUraian[]" placeholder="URAIAN INDIKATOR"></textarea>';
  html += '</td>';
  html += '<td>';
  html += '<textarea class="form-control" name="IKUFormulasi[]" placeholder="FORMULASI"></textarea>';
  html += '</td>';
  html += '<td>';
  html += '<select class="form-control" name="IKUSatuan[]"><?=GetCombobox("SELECT * FROM ".TBL_SAKIP_MSATUAN." ORDER BY ".COL_NM_SATUAN, COL_NM_SATUAN, COL_NM_SATUAN)?></select>';
  html += '</td>';
  html += '<td>';
  html += '<input type="text" class="form-control" name="IKUTarget[]" />';
  html += '</td>';
  html += '<td>';
  html += '<button type="button" class="btn btn-sm btn-danger btn-del-iku" style="font-weight: bold"><i class="fa fa-minus"></i></button>';
  html += '</td>';
  html += '</tr>';

  var empEl = $('tr.empty', $('tbody', $('#tbl-iku')));
  if(empEl) {
    $('tr.empty', $('tbody', $('#tbl-iku'))).remove();
  }

  $('tbody', $('#tbl-iku')).append(html);
  $('.btn-del-iku', $('tbody', $('#tbl-iku'))).unbind('click').click(function() {
    var row = $(this).closest('tr');
    row.remove();
  });
  $("select", $('tbody tr:last', $('#tbl-iku'))).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
}

$(document).ready(function() {
  $('select[name=filterMisi]').change(function(){
    var url = $(this).val();
    location.href = url;
  });

  modalFormTujuan.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalFormTujuan).empty();
  });

  $('.btn-add-misi, .btn-edit-misi').click(function() {
    var url = $(this).attr('href');
    modalFormTujuan.modal('show');
    $('.modal-body', modalFormTujuan).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modalFormTujuan).load(url, function(){
      $('button[type=submit]', modalFormTujuan).unbind('click').click(function(){
        $('form', modalFormTujuan).submit();
      });
    });
    return false;
  });

  $('.btn-change-misi').click(function() {
    var url = $(this).attr('href');
    var prompt = $(this).data('prompt');
    swal({
      title: "APAKAH ANDA YAKIN?",
      text: prompt,
      icon: "warning",
      buttons: [
        'BATAL',
        'YAKIN'
      ],
    }).then(function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          url: url,
          method: "GET",
          dataType: "json"
        }).success(function(res) {
          if(res.error) {
            swal({
              title: 'ERROR',
              text: res.error,
              icon: 'error',
              buttons:false
            });
          } else {
            location.reload();
          }
        }).fail(function() {
          swal({
            title: 'SERVER ERROR',
            text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
            icon: 'error',
            buttons:false
          });
        }).done(function() {

        });

      } else {

      }
    })
    return false;
  });

  $('#btn-add-iku', modalFormIKU).click(function() {
    addRowIKU();
  });

  $('#form-iku').validate({
    submitHandler: function(form) {
      var modal = $(form).closest('modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });

  $('#btnFormIKU').click(function() {
    var url = $(this).attr('href');
    modalFormIKU.modal('show');

    $('.btn-del-iku', modalFormIKU).unbind('click').click(function() {
      var row = $(this).closest('tr');
      row.remove();
    });
    $("select", modalFormIKU).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
    $('button[type=submit]', modalFormIKU).unbind('click').click(function(){
      $('form', modalFormIKU).submit();
    });
  });
});
</script>
