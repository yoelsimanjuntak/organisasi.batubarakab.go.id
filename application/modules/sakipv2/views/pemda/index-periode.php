<?php
$rperiode = $this->db
->order_by(COL_PMDTAHUNMULAI, 'desc')
->get(TBL_SAKIPV2_PEMDA)
->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?=$title?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-outline card-secondary">
          <?php
          if(!empty($subtitle)) {
            ?>
            <div class="card-header">
              <h4 class="card-title"><?=$subtitle?></h4>
            </div>
            <?php
          }
          ?>
          <div class="card-body p-0">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th style="text-align: center !important">PERIODE</th>
                  <th>KEPALA DAERAH</th>
                  <th>VISI</th>
                  <th style="white-space: nowrap; text-align: center !important">STATUS</th>
                  <th style="white-space: nowrap; text-align: center !important">AKSI</th>
                </tr>
              </thead>
              <tbody>
                <?php
                if(!empty($rperiode)) {
                  foreach($rperiode as $r) {
                    ?>
                    <tr>
                      <td class="text-center" style="width: 100px; white-space: nowrap">
                        <?=$r[COL_PMDTAHUNMULAI].' - '.$r[COL_PMDTAHUNAKHIR]?>
                      </td>
                      <td style="white-space: nowrap">
                        <?=$r[COL_PMDPEJABAT]?>
                      </td>
                      <td class="font-italic">
                        <?=$r[COL_PMDVISI]?>
                      </td>
                      <td style="white-space: nowrap;">
                        <?=$r[COL_PMDISAKTIF]==1?'<span class="badge bg-success">AKTIF</span>':'<span class="badge bg-secondary">INAKTIF</span>'?>
                      </td>
                      <td class="text-center" style="white-space: nowrap">
                        <a href="<?=site_url('sakipv2/pemda/ajax-form-periode/edit/'.$r[COL_PMDID])?>" data-toggle="tooltip" data-placement="bottom" title="UBAH" class="btn btn-primary btn-sm btn-edit-periode"><i class="far fa-edit"></i></a>
                        <?php
                        if($r[COL_PMDISAKTIF]==1) {
                          ?>
                          <button type="button" class="btn btn-secondary btn-sm" disabled><i class="far fa-check-circle"></i></button>
                          <?php
                        } else {
                          ?>
                          <a href="<?=site_url('sakipv2/pemda/ajax-change-periode/activate/'.$r[COL_PMDID])?>" data-toggle="tooltip" data-placement="bottom" title="AKTIFKAN" data-prompt="Mengaktifkan data periode pemerintahan akan otomatis me-nonaktifkan data periode pemerintahan lainnya." class="btn btn-success btn-sm btn-change-periode"><i class="far fa-check-circle"></i></a>
                          <?php
                        }
                        ?>
                        <a href="<?=site_url('sakipv2/pemda/ajax-change-periode/delete/'.$r[COL_PMDID])?>" data-toggle="tooltip" data-placement="bottom" title="HAPUS" data-prompt="Menghapus data periode pemerintahan akan otomatis menghapus data RPMJD yang sudah ada." class="btn btn-danger btn-sm btn-change-periode"><i class="far fa-times-circle"></i></a>
                        <a href="<?=site_url('sakipv2/pemda/index').'?opr=detail-periode&id='.$r[COL_PMDID]?>" data-toggle="tooltip" data-placement="bottom" title="TELUSURI" class="btn btn-info btn-sm"><i class="far fa-search"></i></a>
                      </td>
                    </tr>
                    <?php
                  }
                } else {
                  ?>
                  <tr>
                    <td colspan="5">
                      <p class="text-center font-italic mb-0">
                        BELUM ADA DATA
                      </p>
                    </td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </div>
          <div class="card-footer">
            <a href="<?=site_url('sakipv2/pemda/ajax-form-periode/add')?>" class="btn btn-primary btn-add-periode font-weight-bold"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH PERIODE</a>
          </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modalFormPeriode" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Form Periode Pemerintahan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var modalFormPeriode = $('#modalFormPeriode');
$(document).ready(function() {
  modalFormPeriode.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalFormPeriode).empty();
  });

  $('.btn-add-periode, .btn-edit-periode').click(function() {
    var url = $(this).attr('href');
    modalFormPeriode.modal('show');
    $('.modal-body', modalFormPeriode).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modalFormPeriode).load(url, function(){
      $('button[type=submit]', modalFormPeriode).unbind('click').click(function(){
        $('form', modalFormPeriode).submit();
      });
    });
    return false;
  });

  $('.btn-change-periode').click(function() {
    var url = $(this).attr('href');
    var prompt = $(this).data('prompt');
    swal({
      title: "APAKAH ANDA YAKIN?",
      text: prompt,
      icon: "warning",
      buttons: [
        'BATAL',
        'YAKIN'
      ],
    }).then(function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          url: url,
          method: "GET",
          dataType: "json"
        }).success(function(res) {
          if(res.error) {
            swal({
              title: 'ERROR',
              text: res.error,
              icon: 'error',
              buttons:false
            });
          } else {
            location.reload();
          }
        }).fail(function() {
          swal({
            title: 'SERVER ERROR',
            text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
            icon: 'error',
            buttons:false
          });
        }).done(function() {

        });

      } else {

      }
    })
    return false;
  });
});
</script>
