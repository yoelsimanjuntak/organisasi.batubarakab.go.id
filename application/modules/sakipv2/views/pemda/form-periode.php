<form id="form-periode" action="<?=current_url()?>">
  <div class="form-group row">
    <label class="control-label col-lg-3">TAHUN</label>
    <div class="col-lg-5">
      <div class="input-group">
        <input type="number" class="form-control" name="<?=COL_PMDTAHUNMULAI?>" placeholder="MULAI" value="<?=!empty($data)?$data[COL_PMDTAHUNMULAI]:''?>" required />
        <div class="input-group-prepend input-group-append">
          <div class="input-group-text font-weight-bold">s.d</div>
        </div>
        <input type="number" class="form-control" name="<?=COL_PMDTAHUNAKHIR?>" placeholder="AKHIR" value="<?=!empty($data)?$data[COL_PMDTAHUNAKHIR]:''?>" required />
      </div>
    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-lg-3">KEPALA DAERAH</label>
    <div class="col-lg-8">
      <input type="text" class="form-control" name="<?=COL_PMDPEJABAT?>" placeholder="NAMA LENGKAP" value="<?=!empty($data)?$data[COL_PMDPEJABAT]:''?>" required />
    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-lg-3">WAKIL KEPALA DAERAH</label>
    <div class="col-lg-8">
      <input type="text" class="form-control" name="<?=COL_PMDPEJABATWAKIL?>" placeholder="NAMA LENGKAP" value="<?=!empty($data)?$data[COL_PMDPEJABATWAKIL]:''?>" required />
    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-lg-3">VISI</label>
    <div class="col-lg-8">
      <textarea class="form-control" name="<?=COL_PMDVISI?>" placeholder="VISI KEPALA DAERAH" required><?=!empty($data)?$data[COL_PMDVISI]:''?></textarea>
    </div>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function(){
  $('#form-periode').validate({
    submitHandler: function(form) {
      var modal = $(form).closest('modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });

      return false;
    }
  });
});
</script>
