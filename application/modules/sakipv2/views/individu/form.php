<?php
$rPlsTupoksi = array();
$rPlsIKU = array();
if(!empty($data[COL_PLSFUNGSI])) {
  $rPlsTupoksi = json_decode($data[COL_PLSFUNGSI]);
}

if(!empty($data[COL_PLSIKU])) {
  $rPlsIKU = json_decode($data[COL_PLSIKU]);
}
?>
<form id="form-individu" action="<?=current_url()?>">
  <div class="form-group">
    <label>UNIT KERJA</label>
    <select class="form-control" name="<?=COL_IDSUBBID?>" style="width: 100%">
      <?php
      foreach($rOptSubbidang as $opt) {
        ?>
        <option value="<?=$opt[COL_SUBBIDID]?>" <?=!empty($data)&&$opt[COL_SUBBIDID]==$data[COL_IDSUBBID]?'selected':''?>>
          <?=strtoupper($opt[COL_SUBBIDNAMA])?>
        </option>
        <?php
      }
      ?>
    </select>
  </div>
  <div class="form-group">
    <label>NOMENKLATUR</label>
    <input type="text" class="form-control" placeholder="NOMENKLATUR JAB. PELAKSANA" name="<?=COL_PLSNAMA?>" value="<?=!empty($data)?$data[COL_PLSNAMA]:''?>" />
  </div>
  <div class="form-group">
    <label>NAMA PEGAWAI</label>
    <input type="text" class="form-control" placeholder="NAMA PEGAWAI" name="<?=COL_PLSNAMAPEGAWAI?>" value="<?=!empty($data)?$data[COL_PLSNAMAPEGAWAI]:''?>" />
  </div>

  <div class="form-group">
    <label>TUGAS POKOK</label>
    <textarea class="form-control" name="<?=COL_PLSTUGASPOKOK?>" placeholder="URAIAN TUGAS POKOK" required><?=!empty($data)?$data[COL_PLSTUGASPOKOK]:''?></textarea>
  </div>

  <table id="tbl-tupoksi" class="table table-bordered text-sm">
    <thead class="bg-default">
      <tr>
        <th>SASARAN</th>
        <th class="text-center" style="width: 10px; white-space: nowrap">
          <button type="button" id="btn-add-tupoksi" class="btn btn-xs btn-primary" style="font-weight: bold"><i class="far fa-plus-circle"></i></button>
        </th>
      </tr>
    </thead>
    <tbody>
      <?php
      if(!empty($rPlsTupoksi)) {
        foreach($rPlsTupoksi as $r) {
          ?>
          <tr>
            <td>
              <textarea class="form-control" name="FungsiUraian[]" placeholder="URAIAN SASARAN"><?=$r?></textarea>
            </td>
            <td>
              <button type="button" class="btn btn-xs btn-danger btn-del-tupoksi" style="font-weight: bold"><i class="fa fa-minus"></i></button>
            </td>
          </tr>
          <?php
        }
      } else {
        ?>
        <tr class="empty">
          <td colspan="2">
            <p class="text-center font-italic mb-0">BELUM ADA DATA</p>
          </td>
        </tr>
        <?php
      }
      ?>
    </tbody>
  </table>

  <div class="form-group">
    <label>INDIKATOR KINERJA INDIVIDU</label>
    <table id="tbl-iku" class="table table-bordered text-sm">
      <thead class="bg-default">
        <tr>
          <th>INDIKATOR</th>
          <th>SUMBER DATA</th>
          <th>FORMULASI</th>
          <th style="width: 120px; white-space: nowrap">SATUAN</th>
          <th style="width: 120px; white-space: nowrap">TARGET</th>
          <th class="text-center" style="width: 10px; white-space: nowrap">
            <button type="button" id="btn-add-iku" class="btn btn-xs btn-primary" style="font-weight: bold"><i class="far fa-plus-circle"></i></button>
          </th>
        </tr>
      </thead>
      <tbody>
        <?php
        if(!empty($rPlsIKU)) {
          foreach($rPlsIKU as $iku) {
            ?>
            <tr>
              <td>
                <textarea class="form-control" name="IKUUraian[]" placeholder="URAIAN INDIKATOR"><?=$iku->Uraian?></textarea>
              </td>
              <td>
                <textarea class="form-control" name="IKUSumberData[]" placeholder="SUMBER DATA"><?=$iku->SumberData?></textarea>
              </td>
              <td>
                <textarea class="form-control" name="IKUFormulasi[]" placeholder="FORMULASI"><?=$iku->Formulasi?></textarea>
              </td>
              <td>
                <select class="form-control" name="IKUSatuan[]"><?=GetCombobox("SELECT * FROM ".TBL_SAKIP_MSATUAN." ORDER BY ".COL_NM_SATUAN, COL_NM_SATUAN, COL_NM_SATUAN, $iku->Satuan)?></select>
              </td>
              <td>
                <input type="text" class="form-control" name="IKUTarget[]" value="<?=$iku->Target?>" />
              </td>
              <td>
                <button type="button" class="btn btn-xs btn-danger btn-del-iku" style="font-weight: bold"><i class="fa fa-minus"></i></button>
              </td>
            </tr>
            <?php
          }
        } else {
          ?>
          <tr class="empty">
            <td colspan="6">
              <p class="font-italic mb-0 text-center">BELUM ADA DATA</p>
            </td>
          </tr>
          <?php
        }
        ?>
      </tbody>
    </table>
  </div>
</form>
<script type="text/javascript">
function addRowTupoksi() {
  var html =' ';
  html += '<tr>';
  html += '<td>';
  html += '<textarea class="form-control" name="FungsiUraian[]" placeholder="URAIAN SASARAN"></textarea>';
  html += '</td>';
  html += '<td>';
  html += '<button type="button" class="btn btn-xs btn-danger btn-del-tupoksi" style="font-weight: bold"><i class="fa fa-minus"></i></button>';
  html += '</td>';
  html += '</tr>';

  var empEl = $('tr.empty', $('tbody', $('#tbl-tupoksi')));
  if(empEl) {
    $('tr.empty', $('tbody', $('#tbl-tupoksi'))).remove();
  }

  $('tbody', $('#tbl-tupoksi')).append(html);
  $('.btn-del-tupoksi', $('tbody', $('#tbl-tupoksi'))).unbind('click').click(function() {
    var row = $(this).closest('tr');
    row.remove();
  });
  $("select", $('tbody tr:last', $('#tbl-tupoksi'))).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
}

function addRowIKU() {
  var html =' ';
  html += '<tr>';
  html += '<td>';
  html += '<textarea class="form-control" name="IKUUraian[]" placeholder="URAIAN INDIKATOR"></textarea>';
  html += '</td>';
  html += '<td>';
  html += '<textarea class="form-control" name="IKUSumberData[]" placeholder="SUMBER DATA"></textarea>';
  html += '</td>';
  html += '<td>';
  html += '<textarea class="form-control" name="IKUFormulasi[]" placeholder="FORMULASI"></textarea>';
  html += '</td>';
  html += '<td>';
  html += '<select class="form-control" name="IKUSatuan[]"><?=GetCombobox("SELECT * FROM ".TBL_SAKIP_MSATUAN." ORDER BY ".COL_NM_SATUAN, COL_NM_SATUAN, COL_NM_SATUAN)?></select>';
  html += '</td>';
  html += '<td>';
  html += '<input type="text" class="form-control" name="IKUTarget[]" />';
  html += '</td>';
  html += '<td>';
  html += '<button type="button" class="btn btn-xs btn-danger btn-del-iku" style="font-weight: bold"><i class="fa fa-minus"></i></button>';
  html += '</td>';
  html += '</tr>';

  var empEl = $('tr.empty', $('tbody', $('#tbl-iku')));
  if(empEl) {
    $('tr.empty', $('tbody', $('#tbl-iku'))).remove();
  }

  $('tbody', $('#tbl-iku')).append(html);
  $('.btn-del-iku', $('tbody', $('#tbl-iku'))).unbind('click').click(function() {
    var row = $(this).closest('tr');
    row.remove();
  });
  $("select", $('tbody tr:last', $('#tbl-iku'))).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
}

$(document).ready(function(){
  $('#btn-add-tupoksi', $('#tbl-tupoksi')).click(function() {
    addRowTupoksi();
  });
  $('#btn-add-iku', $('#tbl-iku')).click(function() {
    addRowIKU();
  });
  $('.btn-del-tupoksi', $('tbody', $('#tbl-tupoksi'))).unbind('click').click(function() {
    var row = $(this).closest('tr');
    row.remove();
  });
  $('.btn-del-iku', $('tbody', $('#tbl-iku'))).unbind('click').click(function() {
    var row = $(this).closest('tr');
    row.remove();
  });

  $('#form-individu').validate({
    submitHandler: function(form) {
      var modal = $(form).closest('modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });

      return false;
    }
  });
});
</script>
