<form id="form-periode" action="<?=current_url()?>">
  <div class="form-group">
    <label>NAMA SKPD</label>
    <input type="text" class="form-control" placeholder="NAMA SKPD" name="<?=COL_SKPDNAMA?>" value="<?=!empty($data)?$data[COL_SKPDNAMA]:''?>" />
  </div>
  <div class="form-group">
    <label>NAMA PIMPINAN</label>
    <input type="text" class="form-control" name="<?=COL_SKPDNAMAPIMPINAN?>" placeholder="NAMA PIMPINAN SKPD" value="<?=!empty($data)?$data[COL_SKPDNAMAPIMPINAN]:''?>" required />
  </div>
  <div class="form-group">
    <label>NAMA JABATAN PIMPINAN</label>
    <input type="text" class="form-control" name="<?=COL_SKPDNAMAJABATAN?>" placeholder="NAMA JABATAN PIMPINAN SKPD" value="<?=!empty($data)?$data[COL_SKPDNAMAJABATAN]:''?>" required />
  </div>
  <div class="form-group">
    <label>INFO KOP SKPD</label>
    <textarea class="form-control" name="<?=COL_SKPDKOP?>" placeholder="CONTOH: Jl. Ahmad Yani No. 15, Kec. Aman Sentosa; Telp: 021-123456; Fax: 021-654321"><?=!empty($data)?$data[COL_SKPDKOP]:''?></textarea>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function(){
  $('#form-periode').validate({
    submitHandler: function(form) {
      var modal = $(form).closest('modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });

      return false;
    }
  });
});
</script>
