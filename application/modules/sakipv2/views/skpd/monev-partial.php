<?php
if(empty($idSKPD) || empty($idRenstra) || empty($idDPA)) {
  ?>
  <p class="text-center font-italic font-weight-bold text-danger mb-0">
    DATA TIDAK DITEMUKAN
  </p>
  <?php
  exit();
}

$rpemda = $this->db
->where(COL_PMDISAKTIF, 1)
->get(TBL_SAKIPV2_PEMDA)
->row_array();
if(empty($rpemda)) {
  ?>
  <p class="text-center font-italic font-weight-bold text-danger mb-0">
    PERIODE PEMERINTAHAN AKTIF TIDAK DITEMUKAN
  </p>
  <?php
}

$rskpd = $this->db
->where(COL_SKPDID, $idSKPD)
->get(TBL_SAKIPV2_SKPD)
->row_array();
if(empty($rskpd)) {
  ?>
  <p class="text-center font-italic font-weight-bold text-danger mb-0">
    SKPD TIDAK DITEMUKAN
  </p>
  <?php
}

$rdpa = $this->db
->where(COL_DPAID, $idDPA)
->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
->row_array();
if(empty($rskpd)) {
  ?>
  <p class="text-center font-italic font-weight-bold text-danger mb-0">
    DPA TIDAK DITEMUKAN
  </p>
  <?php
}

$rsasaran = $this->db
->join(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN,TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.".".COL_IDTUJUAN,"left")
->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.".".COL_IDRENSTRA,"left")
->where(COL_IDPEMDA, $rpemda[COL_PMDID])
->where(COL_IDSKPD, $idSKPD)
->where(COL_IDRENSTRA, $idRenstra)
->order_by(COL_TUJUANNO)
->order_by(COL_SASARANNO)
->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN)
->result_array();

$rsubkeg = $this->db
->join(TBL_SAKIPV2_BID_KEGIATAN,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANID." = ".TBL_SAKIPV2_SUBBID_SUBKEGIATAN.".".COL_IDKEGIATAN,"left")
->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
->join(TBL_SAKIPV2_SKPD_RENSTRA_DPA,TBL_SAKIPV2_SKPD_RENSTRA_DPA.'.'.COL_DPAID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDDPA,"left")
->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_DPA.".".COL_IDRENSTRA,"left")
->where(COL_IDSKPD, $idSKPD)
->where(COL_IDDPA, $idDPA)
->order_by(COL_IDBID)
->order_by(COL_SUBKEGKODE)
->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
->result_array();
?>

<?php
if(!empty($modCetak)) {
  ?>
  <style>
  body {
    font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
  }
  th {
    background: #ffc107 !important;
  }
  th, td {
    font-size: 10pt !important;
    padding: 5px 10px !important;
    max-width: 200px !important;
  }
  table {
    width: 100%;
    border-collapse: collapse;
  }
  table, th, td {
    border: 1px solid black !important;
  }
  </style>
  <?php

} else {
  ?>
  <style>
  td, th {
    font-size: 10pt !important;
    padding: 5px 10px !important;
  }
  .card-body.p-0 .table tbody>tr>td:first-of-type,
  .card-body.p-0 .table tbody>tr>th:first-of-type,
  .card-body.p-0 .table thead>tr>td:first-of-type,
  .card-body.p-0 .table thead>tr>th:first-of-type {
    font-size: 10pt !important;
    padding: 5px 10px !important;
  }
  td a {
    text-decoration-line: underline !important;
    text-decoration-style: dotted !important;
  }
  </style>
  <?php
}
?>
<h5 style="text-align: center; margin-top: 2rem; margin-bottom: 0 !important">
  <?=$title?><br />
  <?=strtoupper($rskpd[COL_SKPDNAMA])?><br />
  TAHUN <?=$rdpa[COL_DPATAHUN]?>
</h5>
<hr />
<?php
if(empty($modCetak)) {
  ?>
  <p class="font-weight-bold font-italic pl-3 pr-3 mb-0">
    PETUNJUK: <span class="text-danger">Klik pada nilai / angka di kolom RENCANA AKSI dan REALISASI untuk mengubah nilai / angka.</span>
  </p>
  <?php
}
?>
<div class="row" style="margin-top: 1rem">
  <div class="col-lg-12">
    <table class="table table-bordered" autosize="1">
      <thead>
        <tr>
          <th rowspan="<?=!empty($modCetak)?'2':'3'?>" style="width: 10px; white-space: nowrap; vertical-align: middle">NO. / KODE REKENING</th>
          <th rowspan="<?=!empty($modCetak)?'2':'3'?>" style="vertical-align: middle">PROGRAM / KEGIATAN / SUB KEGIATAN</th>
          <th rowspan="<?=!empty($modCetak)?'2':'3'?>" style="vertical-align: middle">INDIKATOR KINERJA</th>
          <th rowspan="<?=!empty($modCetak)?'2':'3'?>" style="width: 10px; white-space: nowrap; vertical-align: middle">SATUAN</th>
          <th rowspan="<?=!empty($modCetak)?'2':'3'?>" style="width: 10px; white-space: nowrap; vertical-align: middle; text-align: center">TARGET<br /><?=$rdpa[COL_DPATAHUN]?></th>
          <th colspan="<?=!empty($modCetak)?'4':'8'?>" style="vertical-align: middle; text-align: center">
            <?=!empty($modCetak)?($modCetak=='RENCANA'?'RENCANA AKSI':'REALISASI'):'RENCANA AKSI & REALISASI'?>
          </th>
        </tr>
        <tr>
          <th <?=!empty($modCetak)?'':'colspan="2"'?> style="text-align: center">TW-I</th>
          <th <?=!empty($modCetak)?'':'colspan="2"'?> style="text-align: center">TW-II</th>
          <th <?=!empty($modCetak)?'':'colspan="2"'?> style="text-align: center">TW-III</th>
          <th <?=!empty($modCetak)?'':'colspan="2"'?> style="text-align: center">TW-IV</th>
        </tr>
        <?php
        if(empty($modCetak)) {
          ?>
          <tr>
            <th class="bg-primary disabled" style="text-align: center">TARGET</th>
            <th class="bg-success disabled" style="text-align: center">REALISASI</th>
            <th class="bg-primary disabled" style="text-align: center">TARGET</th>
            <th class="bg-success disabled" style="text-align: center">REALISASI</th>
            <th class="bg-primary disabled" style="text-align: center">TARGET</th>
            <th class="bg-success disabled" style="text-align: center">REALISASI</th>
            <th class="bg-primary disabled" style="text-align: center">TARGET</th>
            <th class="bg-success disabled" style="text-align: center">REALISASI</th>
          </tr>
          <?php
        }
        ?>
      </thead>
      <tbody>
        <?php
        if(!empty($rsasaran)) {
          foreach($rsasaran as $s) {
            $rdet = $this->db
            ->where(COL_IDSASARAN, $s[COL_SASARANID])
            ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARANDET)
            ->result_array();

            $rprogram = $this->db
            ->join(TBL_SAKIPV2_SKPD_RENSTRA_DPA,TBL_SAKIPV2_SKPD_RENSTRA_DPA.'.'.COL_DPAID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDDPA,"left")
            ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_DPA.".".COL_IDRENSTRA,"left")
            ->where(COL_IDSASARANSKPD, $s[COL_SASARANID])
            ->where(COL_IDRENSTRA, $idRenstra)
            ->where(COL_IDDPA, $idDPA)
            ->order_by(COL_IDBID)
            ->order_by(COL_PROGRAMKODE)
            ->get(TBL_SAKIPV2_BID_PROGRAM)
            ->result_array();

            $rprogramInd = $this->db
            ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_PROGSASARAN.".".COL_IDPROGRAM,"left")
            ->join(TBL_SAKIPV2_SKPD_RENSTRA_DPA,TBL_SAKIPV2_SKPD_RENSTRA_DPA.'.'.COL_DPAID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDDPA,"left")
            ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_DPA.".".COL_IDRENSTRA,"left")
            ->where(COL_IDSASARANSKPD, $s[COL_SASARANID])
            ->where(COL_IDRENSTRA, $idRenstra)
            ->where(COL_IDDPA, $idDPA)
            ->count_all_results(TBL_SAKIPV2_BID_PROGSASARAN);

            $rkegiatan = $this->db
            ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
            ->join(TBL_SAKIPV2_SKPD_RENSTRA_DPA,TBL_SAKIPV2_SKPD_RENSTRA_DPA.'.'.COL_DPAID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDDPA,"left")
            ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_DPA.".".COL_IDRENSTRA,"left")
            ->where(COL_IDSASARANSKPD, $s[COL_SASARANID])
            ->where(COL_IDRENSTRA, $idRenstra)
            ->where(COL_IDDPA, $idDPA)
            ->count_all_results(TBL_SAKIPV2_BID_KEGIATAN);

            $rkegiatanInd = $this->db
            ->join(TBL_SAKIPV2_BID_KEGIATAN,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANID." = ".TBL_SAKIPV2_BID_KEGSASARAN.".".COL_IDKEGIATAN,"left")
            ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
            ->join(TBL_SAKIPV2_SKPD_RENSTRA_DPA,TBL_SAKIPV2_SKPD_RENSTRA_DPA.'.'.COL_DPAID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDDPA,"left")
            ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_DPA.".".COL_IDRENSTRA,"left")
            ->where(COL_IDSASARANSKPD, $s[COL_SASARANID])
            ->where(COL_IDRENSTRA, $idRenstra)
            ->where(COL_IDDPA, $idDPA)
            ->count_all_results(TBL_SAKIPV2_BID_KEGSASARAN);
            $noInd = 1;

            $rowspan = count($rdet);
            ?>
            <tr style="background: #dedede; font-style: italic">
              <td <?=$rowspan>1?'rowspan="'.$rowspan.'"':''?> colspan="2" style="font-style: italic; font-weight: bold; vertical-align: middle"><?=strtoupper($s[COL_SASARANURAIAN])?></td>
              <?php
              if(!empty($rdet)) {
                $rval = $this->db
                ->where(COL_IDSASARANINDIKATOR, $rdet[0][COL_SSRINDIKATORID])
                ->where(COL_MONEVTAHUN, $rdpa[COL_DPATAHUN])
                ->order_by(COL_UNIQ, 'desc')
                ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARANMONEV)
                ->row_array();

                $valTargetTW1 = !empty($rval[COL_MONEVTARGETTW1])&&$rval[COL_MONEVTARGETTW1]!=null?$rval[COL_MONEVTARGETTW1]:'';
                $txtTargetTW1 = !empty($rval[COL_MONEVTARGETTW1])&&$rval[COL_MONEVTARGETTW1]!=null?$rval[COL_MONEVTARGETTW1]:'N/A';
                $hrefTargetTW1 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVTARGETTW1.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[0][COL_SSRINDIKATORID]).'" data-value="'.$valTargetTW1.'">'.$txtTargetTW1.'</a>';

                $valTargetTW2 = !empty($rval[COL_MONEVTARGETTW2])&&$rval[COL_MONEVTARGETTW2]!=null?$rval[COL_MONEVTARGETTW2]:'';
                $txtTargetTW2 = !empty($rval[COL_MONEVTARGETTW2])&&$rval[COL_MONEVTARGETTW2]!=null?$rval[COL_MONEVTARGETTW2]:'N/A';
                $hrefTargetTW2 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVTARGETTW2.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[0][COL_SSRINDIKATORID]).'" data-value="'.$valTargetTW2.'">'.$txtTargetTW2.'</a>';

                $valTargetTW3 = !empty($rval[COL_MONEVTARGETTW3])&&$rval[COL_MONEVTARGETTW3]!=null?$rval[COL_MONEVTARGETTW3]:'';
                $txtTargetTW3 = !empty($rval[COL_MONEVTARGETTW3])&&$rval[COL_MONEVTARGETTW3]!=null?$rval[COL_MONEVTARGETTW3]:'N/A';
                $hrefTargetTW3 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVTARGETTW3.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[0][COL_SSRINDIKATORID]).'" data-value="'.$valTargetTW3.'">'.$txtTargetTW3.'</a>';

                $valTargetTW4 = !empty($rval[COL_MONEVTARGETTW4])&&$rval[COL_MONEVTARGETTW4]!=null?$rval[COL_MONEVTARGETTW4]:'';
                $txtTargetTW4 = !empty($rval[COL_MONEVTARGETTW4])&&$rval[COL_MONEVTARGETTW4]!=null?$rval[COL_MONEVTARGETTW4]:'N/A';
                $hrefTargetTW4 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVTARGETTW4.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[0][COL_SSRINDIKATORID]).'" data-value="'.$valTargetTW4.'">'.$txtTargetTW4.'</a>';

                $valRealTW1 = !empty($rval[COL_MONEVREALISASITW1])&&$rval[COL_MONEVREALISASITW1]!=null?$rval[COL_MONEVREALISASITW1]:'';
                $txtRealTW1 = !empty($rval[COL_MONEVREALISASITW1])&&$rval[COL_MONEVREALISASITW1]!=null?$rval[COL_MONEVREALISASITW1]:'N/A';
                $hrefRealTW1 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVREALISASITW1.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[0][COL_SSRINDIKATORID]).'" data-value="'.$valRealTW1.'">'.$txtRealTW1.'</a>';

                $valRealTW2 = !empty($rval[COL_MONEVREALISASITW2])&&$rval[COL_MONEVREALISASITW2]!=null?$rval[COL_MONEVREALISASITW2]:'';
                $txtRealTW2 = !empty($rval[COL_MONEVREALISASITW2])&&$rval[COL_MONEVREALISASITW2]!=null?$rval[COL_MONEVREALISASITW2]:'N/A';
                $hrefRealTW2 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVREALISASITW2.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[0][COL_SSRINDIKATORID]).'" data-value="'.$valRealTW2.'">'.$txtRealTW2.'</a>';

                $valRealTW3 = !empty($rval[COL_MONEVREALISASITW3])&&$rval[COL_MONEVREALISASITW3]!=null?$rval[COL_MONEVREALISASITW3]:'';
                $txtRealTW3 = !empty($rval[COL_MONEVREALISASITW3])&&$rval[COL_MONEVREALISASITW3]!=null?$rval[COL_MONEVREALISASITW3]:'N/A';
                $hrefRealTW3 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVREALISASITW3.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[0][COL_SSRINDIKATORID]).'" data-value="'.$valRealTW3.'">'.$txtRealTW3.'</a>';

                $valRealTW4 = !empty($rval[COL_MONEVREALISASITW4])&&$rval[COL_MONEVREALISASITW4]!=null?$rval[COL_MONEVREALISASITW4]:'';
                $txtRealTW4 = !empty($rval[COL_MONEVREALISASITW4])&&$rval[COL_MONEVREALISASITW4]!=null?$rval[COL_MONEVREALISASITW4]:'N/A';
                $hrefRealTW4 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVREALISASITW4.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[0][COL_SSRINDIKATORID]).'" data-value="'.$valRealTW4.'">'.$txtRealTW4.'</a>';
                ?>
                <td style="vertical-align: top;"><?=strtoupper($rdet[0][COL_SSRINDIKATORURAIAN])?></td>
                <td style="vertical-align: middle; white-space: nowrap"><?=strtoupper($rdet[0][COL_SSRINDIKATORSATUAN])?></td>
                <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=strtoupper($rdet[0][COL_SSRINDIKATORTARGET])?></td>
                <?php
                if(!empty($modCetak) && $modCetak=='RENCANA') {
                  ?>
                  <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW1:$txtTargetTW1?></td>
                  <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW2:$txtTargetTW2?></td>
                  <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW3:$txtTargetTW3?></td>
                  <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW4:$txtTargetTW4?></td>
                  <?php
                } else if(!empty($modCetak) && $modCetak=='REALISASI') {
                  ?>
                  <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW1:$txtRealTW1?></td>
                  <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW2:$txtRealTW2?></td>
                  <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW3:$txtRealTW3?></td>
                  <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW4:$txtRealTW4?></td>
                  <?php
                } else {
                  ?>
                  <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW1:$txtTargetTW1?></td>
                  <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW1:$txtRealTW1?></td>
                  <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW2:$txtTargetTW2?></td>
                  <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW2:$txtRealTW2?></td>
                  <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW3:$txtTargetTW3?></td>
                  <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW3:$txtRealTW3?></td>
                  <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW4:$txtTargetTW4?></td>
                  <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW4:$txtRealTW4?></td>
                  <?php
                }
                ?>
                <?php
              } else {
                ?>
                <td colspan="11" style="text-align: center; vertical-align: middle; white-space: nowrap">(KOSONG)</td>
                <?php
              }
              ?>
            </tr>
            <?php
            if(!empty($rdet)&&count($rdet)>1) {
              for($i=1; $i<count($rdet); $i++) {
                $rval = $this->db
                ->where(COL_IDSASARANINDIKATOR, $rdet[$i][COL_SSRINDIKATORID])
                ->where(COL_MONEVTAHUN, $rdpa[COL_DPATAHUN])
                ->order_by(COL_UNIQ, 'desc')
                ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARANMONEV)
                ->row_array();

                $valTargetTW1 = !empty($rval[COL_MONEVTARGETTW1])&&$rval[COL_MONEVTARGETTW1]!=null?$rval[COL_MONEVTARGETTW1]:'';
                $txtTargetTW1 = !empty($rval[COL_MONEVTARGETTW1])&&$rval[COL_MONEVTARGETTW1]!=null?$rval[COL_MONEVTARGETTW1]:'N/A';
                $hrefTargetTW1 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVTARGETTW1.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[$i][COL_SSRINDIKATORID]).'" data-value="'.$valTargetTW1.'">'.$txtTargetTW1.'</a>';

                $valTargetTW2 = !empty($rval[COL_MONEVTARGETTW2])&&$rval[COL_MONEVTARGETTW2]!=null?$rval[COL_MONEVTARGETTW2]:'';
                $txtTargetTW2 = !empty($rval[COL_MONEVTARGETTW2])&&$rval[COL_MONEVTARGETTW2]!=null?$rval[COL_MONEVTARGETTW2]:'N/A';
                $hrefTargetTW2 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVTARGETTW2.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[$i][COL_SSRINDIKATORID]).'" data-value="'.$valTargetTW2.'">'.$txtTargetTW2.'</a>';

                $valTargetTW3 = !empty($rval[COL_MONEVTARGETTW3])&&$rval[COL_MONEVTARGETTW3]!=null?$rval[COL_MONEVTARGETTW3]:'';
                $txtTargetTW3 = !empty($rval[COL_MONEVTARGETTW3])&&$rval[COL_MONEVTARGETTW3]!=null?$rval[COL_MONEVTARGETTW3]:'N/A';
                $hrefTargetTW3 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVTARGETTW3.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[$i][COL_SSRINDIKATORID]).'" data-value="'.$valTargetTW3.'">'.$txtTargetTW3.'</a>';

                $valTargetTW4 = !empty($rval[COL_MONEVTARGETTW4])&&$rval[COL_MONEVTARGETTW4]!=null?$rval[COL_MONEVTARGETTW4]:'';
                $txtTargetTW4 = !empty($rval[COL_MONEVTARGETTW4])&&$rval[COL_MONEVTARGETTW4]!=null?$rval[COL_MONEVTARGETTW4]:'N/A';
                $hrefTargetTW4 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVTARGETTW4.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[$i][COL_SSRINDIKATORID]).'" data-value="'.$valTargetTW4.'">'.$txtTargetTW4.'</a>';

                $valRealTW1 = !empty($rval[COL_MONEVREALISASITW1])&&$rval[COL_MONEVREALISASITW1]!=null?$rval[COL_MONEVREALISASITW1]:'';
                $txtRealTW1 = !empty($rval[COL_MONEVREALISASITW1])&&$rval[COL_MONEVREALISASITW1]!=null?$rval[COL_MONEVREALISASITW1]:'N/A';
                $hrefRealTW1 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVREALISASITW1.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[$i][COL_SSRINDIKATORID]).'" data-value="'.$valRealTW1.'">'.$txtRealTW1.'</a>';

                $valRealTW2 = !empty($rval[COL_MONEVREALISASITW2])&&$rval[COL_MONEVREALISASITW2]!=null?$rval[COL_MONEVREALISASITW2]:'';
                $txtRealTW2 = !empty($rval[COL_MONEVREALISASITW2])&&$rval[COL_MONEVREALISASITW2]!=null?$rval[COL_MONEVREALISASITW2]:'N/A';
                $hrefRealTW2 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVREALISASITW2.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[$i][COL_SSRINDIKATORID]).'" data-value="'.$valRealTW2.'">'.$txtRealTW2.'</a>';

                $valRealTW3 = !empty($rval[COL_MONEVREALISASITW3])&&$rval[COL_MONEVREALISASITW3]!=null?$rval[COL_MONEVREALISASITW3]:'';
                $txtRealTW3 = !empty($rval[COL_MONEVREALISASITW3])&&$rval[COL_MONEVREALISASITW3]!=null?$rval[COL_MONEVREALISASITW3]:'N/A';
                $hrefRealTW3 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVREALISASITW3.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[$i][COL_SSRINDIKATORID]).'" data-value="'.$valRealTW3.'">'.$txtRealTW3.'</a>';

                $valRealTW4 = !empty($rval[COL_MONEVREALISASITW4])&&$rval[COL_MONEVREALISASITW4]!=null?$rval[COL_MONEVREALISASITW4]:'';
                $txtRealTW4 = !empty($rval[COL_MONEVREALISASITW4])&&$rval[COL_MONEVREALISASITW4]!=null?$rval[COL_MONEVREALISASITW4]:'N/A';
                $hrefRealTW4 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVREALISASITW4.'/'.$rdpa[COL_DPATAHUN].'/'.$rdet[$i][COL_SSRINDIKATORID]).'" data-value="'.$valRealTW4.'">'.$txtRealTW4.'</a>';

                ?>
                <tr style="background: #dedede; font-style: italic">
                  <td style="vertical-align: top"><?=strtoupper($rdet[$i][COL_SSRINDIKATORURAIAN])?></td>
                  <td style="vertical-align: middle; white-space: nowrap"><?=strtoupper($rdet[$i][COL_SSRINDIKATORSATUAN])?></td>
                  <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=strtoupper($rdet[$i][COL_SSRINDIKATORTARGET])?></td>
                  <?php
                  if(!empty($modCetak) && $modCetak=='RENCANA') {
                    ?>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW1:$txtTargetTW1?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW2:$txtTargetTW2?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW3:$txtTargetTW3?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW4:$txtTargetTW4?></td>
                    <?php
                  } else if(!empty($modCetak) && $modCetak=='REALISASI') {
                    ?>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW1:$txtRealTW1?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW2:$txtRealTW2?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW3:$txtRealTW3?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW4:$txtRealTW4?></td>
                    <?php
                  } else {
                    ?>
                    <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW1:$txtTargetTW1?></td>
                    <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW1:$txtRealTW1?></td>
                    <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW2:$txtTargetTW2?></td>
                    <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW2:$txtRealTW2?></td>
                    <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW3:$txtTargetTW3?></td>
                    <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW3:$txtRealTW3?></td>
                    <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW4:$txtTargetTW4?></td>
                    <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW4:$txtRealTW4?></td>
                    <?php
                  }
                  ?>
                </tr>
                <?php
              }
            }

            foreach($rprogram as $prg) {
              $rPrgInd = $this->db
              ->where(COL_IDPROGRAM, $prg[COL_PROGRAMID])
              ->order_by(COL_SASARANNO)
              ->get(TBL_SAKIPV2_BID_PROGSASARAN)
              ->result_array();

              $rkegiatan = $this->db
              ->where(COL_IDPROGRAM, $prg[COL_PROGRAMID])
              ->order_by(COL_KEGIATANKODE)
              ->get(TBL_SAKIPV2_BID_KEGIATAN)
              ->result_array();
              ?>
              <tr>
                <td <?=!empty($rPrgInd)&&count($rPrgInd)>1?'rowspan="'.(count($rPrgInd)).'"':''?> style="vertical-align: top"><?=$prg[COL_PROGRAMKODE]?></td>
                <td <?=!empty($rPrgInd)&&count($rPrgInd)>1?'rowspan="'.(count($rPrgInd)).'"':''?> style="vertical-align: top"><?=strtoupper($prg[COL_PROGRAMURAIAN])?></td>
                <?php
                if(!empty($rPrgInd)) {
                  $valTargetTW1 = !empty($rPrgInd[0][COL_SASARANTARGETTW1])&&$rPrgInd[0][COL_SASARANTARGETTW1]!=null?$rPrgInd[0][COL_SASARANTARGETTW1]:'';
                  $txtTargetTW1 = !empty($rPrgInd[0][COL_SASARANTARGETTW1])&&$rPrgInd[0][COL_SASARANTARGETTW1]!=null?$rPrgInd[0][COL_SASARANTARGETTW1]:'N/A';
                  $hrefTargetTW1 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaranprog/'.COL_SASARANTARGETTW1.'/'.$rPrgInd[0][COL_SASARANID]).'" data-value="'.$valTargetTW1.'">'.$txtTargetTW1.'</a>';

                  $valTargetTW2 = !empty($rPrgInd[0][COL_SASARANTARGETTW2])&&$rPrgInd[0][COL_SASARANTARGETTW2]!=null?$rPrgInd[0][COL_SASARANTARGETTW2]:'';
                  $txtTargetTW2 = !empty($rPrgInd[0][COL_SASARANTARGETTW2])&&$rPrgInd[0][COL_SASARANTARGETTW2]!=null?$rPrgInd[0][COL_SASARANTARGETTW2]:'N/A';
                  $hrefTargetTW2 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaranprog/'.COL_SASARANTARGETTW2.'/'.$rPrgInd[0][COL_SASARANID]).'" data-value="'.$valTargetTW2.'">'.$txtTargetTW2.'</a>';

                  $valTargetTW3 = !empty($rPrgInd[0][COL_SASARANTARGETTW3])&&$rPrgInd[0][COL_SASARANTARGETTW3]!=null?$rPrgInd[0][COL_SASARANTARGETTW3]:'';
                  $txtTargetTW3 = !empty($rPrgInd[0][COL_SASARANTARGETTW3])&&$rPrgInd[0][COL_SASARANTARGETTW3]!=null?$rPrgInd[0][COL_SASARANTARGETTW3]:'N/A';
                  $hrefTargetTW3 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaranprog/'.COL_SASARANTARGETTW3.'/'.$rPrgInd[0][COL_SASARANID]).'" data-value="'.$valTargetTW3.'">'.$txtTargetTW3.'</a>';

                  $valTargetTW4 = !empty($rPrgInd[0][COL_SASARANTARGETTW4])&&$rPrgInd[0][COL_SASARANTARGETTW4]!=null?$rPrgInd[0][COL_SASARANTARGETTW4]:'';
                  $txtTargetTW4 = !empty($rPrgInd[0][COL_SASARANTARGETTW4])&&$rPrgInd[0][COL_SASARANTARGETTW4]!=null?$rPrgInd[0][COL_SASARANTARGETTW4]:'N/A';
                  $hrefTargetTW4 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaranprog/'.COL_SASARANTARGETTW4.'/'.$rPrgInd[0][COL_SASARANID]).'" data-value="'.$valTargetTW4.'">'.$txtTargetTW4.'</a>';

                  $valRealTW1 = !empty($rPrgInd[0][COL_SASARANREALISASITW1])&&$rPrgInd[0][COL_SASARANREALISASITW1]!=null?$rPrgInd[0][COL_SASARANREALISASITW1]:'';
                  $txtRealTW1 = !empty($rPrgInd[0][COL_SASARANREALISASITW1])&&$rPrgInd[0][COL_SASARANREALISASITW1]!=null?$rPrgInd[0][COL_SASARANREALISASITW1]:'N/A';
                  $hrefRealTW1 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaranprog/'.COL_SASARANREALISASITW1.'/'.$rPrgInd[0][COL_SASARANID]).'" data-value="'.$valRealTW1.'">'.$txtRealTW1.'</a>';

                  $valRealTW2 = !empty($rPrgInd[0][COL_SASARANREALISASITW2])&&$rPrgInd[0][COL_SASARANREALISASITW2]!=null?$rPrgInd[0][COL_SASARANREALISASITW2]:'';
                  $txtRealTW2 = !empty($rPrgInd[0][COL_SASARANREALISASITW2])&&$rPrgInd[0][COL_SASARANREALISASITW2]!=null?$rPrgInd[0][COL_SASARANREALISASITW2]:'N/A';
                  $hrefRealTW2 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaranprog/'.COL_SASARANREALISASITW2.'/'.$rPrgInd[0][COL_SASARANID]).'" data-value="'.$valRealTW2.'">'.$txtRealTW2.'</a>';

                  $valRealTW3 = !empty($rPrgInd[0][COL_SASARANREALISASITW3])&&$rPrgInd[0][COL_SASARANREALISASITW3]!=null?$rPrgInd[0][COL_SASARANREALISASITW3]:'';
                  $txtRealTW3 = !empty($rPrgInd[0][COL_SASARANREALISASITW3])&&$rPrgInd[0][COL_SASARANREALISASITW3]!=null?$rPrgInd[0][COL_SASARANREALISASITW3]:'N/A';
                  $hrefRealTW3 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaranprog/'.COL_SASARANREALISASITW3.'/'.$rPrgInd[0][COL_SASARANID]).'" data-value="'.$valRealTW3.'">'.$txtRealTW3.'</a>';

                  $valRealTW4 = !empty($rPrgInd[0][COL_SASARANREALISASITW4])&&$rPrgInd[0][COL_SASARANREALISASITW4]!=null?$rPrgInd[0][COL_SASARANREALISASITW4]:'';
                  $txtRealTW4 = !empty($rPrgInd[0][COL_SASARANREALISASITW4])&&$rPrgInd[0][COL_SASARANREALISASITW4]!=null?$rPrgInd[0][COL_SASARANREALISASITW4]:'N/A';
                  $hrefRealTW4 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaranprog/'.COL_SASARANREALISASITW4.'/'.$rPrgInd[0][COL_SASARANID]).'" data-value="'.$valRealTW4.'">'.$txtRealTW4.'</a>';
                  ?>
                  <td style="vertical-align: top"><?=strtoupper($rPrgInd[0][COL_SASARANINDIKATOR])?></td>
                  <td style="vertical-align: middle; white-space: nowrap"><?=!empty($rPrgInd)?strtoupper($rPrgInd[0][COL_SASARANSATUAN]):'-'?></td>
                  <td style="text-align: center; vertical-align: middle;; white-space: nowrap"><?=!empty($rPrgInd)?strtoupper($rPrgInd[0][COL_SASARANTARGET]):'-'?></td>
                  <?php
                  if(!empty($modCetak) && $modCetak=='RENCANA') {
                    ?>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW1:$txtTargetTW1?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW2:$txtTargetTW2?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW3:$txtTargetTW3?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW4:$txtTargetTW4?></td>
                    <?php
                  } else if(!empty($modCetak) && $modCetak=='REALISASI') {
                    ?>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW1:$txtRealTW1?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW2:$txtRealTW2?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW3:$txtRealTW3?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW4:$txtRealTW4?></td>
                    <?php
                  } else {
                    ?>
                    <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW1:$txtTargetTW1?></td>
                    <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW1:$txtRealTW1?></td>
                    <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW2:$txtTargetTW2?></td>
                    <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW2:$txtRealTW2?></td>
                    <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW3:$txtTargetTW3?></td>
                    <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW3:$txtRealTW3?></td>
                    <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW4:$txtTargetTW4?></td>
                    <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW4:$txtRealTW4?></td>
                    <?php
                  }
                  ?>
                  <?php
                } else {
                  ?>
                  <td colspan="11" style="text-align: center; vertical-align: middle; white-space: nowrap">(KOSONG)</td>
                  <?php
                }
                ?>
              </tr>
              <?php
              if(!empty($rPrgInd) && count($rPrgInd)>1) {
                for($i=1; $i<count($rPrgInd); $i++) {
                  $valTargetTW1 = !empty($rPrgInd[$i][COL_SASARANTARGETTW1])&&$rPrgInd[$i][COL_SASARANTARGETTW1]!=null?$rPrgInd[$i][COL_SASARANTARGETTW1]:'';
                  $txtTargetTW1 = !empty($rPrgInd[$i][COL_SASARANTARGETTW1])&&$rPrgInd[$i][COL_SASARANTARGETTW1]!=null?$rPrgInd[$i][COL_SASARANTARGETTW1]:'N/A';
                  $hrefTargetTW1 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaranprog/'.COL_SASARANTARGETTW1.'/'.$rPrgInd[$i][COL_SASARANID]).'" data-value="'.$valTargetTW1.'">'.$txtTargetTW1.'</a>';

                  $valTargetTW2 = !empty($rPrgInd[$i][COL_SASARANTARGETTW2])&&$rPrgInd[$i][COL_SASARANTARGETTW2]!=null?$rPrgInd[$i][COL_SASARANTARGETTW2]:'';
                  $txtTargetTW2 = !empty($rPrgInd[$i][COL_SASARANTARGETTW2])&&$rPrgInd[$i][COL_SASARANTARGETTW2]!=null?$rPrgInd[$i][COL_SASARANTARGETTW2]:'N/A';
                  $hrefTargetTW2 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaranprog/'.COL_SASARANTARGETTW2.'/'.$rPrgInd[$i][COL_SASARANID]).'" data-value="'.$valTargetTW2.'">'.$txtTargetTW2.'</a>';

                  $valTargetTW3 = !empty($rPrgInd[$i][COL_SASARANTARGETTW3])&&$rPrgInd[$i][COL_SASARANTARGETTW3]!=null?$rPrgInd[$i][COL_SASARANTARGETTW3]:'';
                  $txtTargetTW3 = !empty($rPrgInd[$i][COL_SASARANTARGETTW3])&&$rPrgInd[$i][COL_SASARANTARGETTW3]!=null?$rPrgInd[$i][COL_SASARANTARGETTW3]:'N/A';
                  $hrefTargetTW3 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaranprog/'.COL_SASARANTARGETTW3.'/'.$rPrgInd[$i][COL_SASARANID]).'" data-value="'.$valTargetTW3.'">'.$txtTargetTW3.'</a>';

                  $valTargetTW4 = !empty($rPrgInd[$i][COL_SASARANTARGETTW4])&&$rPrgInd[$i][COL_SASARANTARGETTW4]!=null?$rPrgInd[$i][COL_SASARANTARGETTW4]:'';
                  $txtTargetTW4 = !empty($rPrgInd[$i][COL_SASARANTARGETTW4])&&$rPrgInd[$i][COL_SASARANTARGETTW4]!=null?$rPrgInd[$i][COL_SASARANTARGETTW4]:'N/A';
                  $hrefTargetTW4 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaranprog/'.COL_SASARANTARGETTW4.'/'.$rPrgInd[$i][COL_SASARANID]).'" data-value="'.$valTargetTW4.'">'.$txtTargetTW4.'</a>';

                  $valRealTW1 = !empty($rPrgInd[$i][COL_SASARANREALISASITW1])&&$rPrgInd[$i][COL_SASARANREALISASITW1]!=null?$rPrgInd[$i][COL_SASARANREALISASITW1]:'';
                  $txtRealTW1 = !empty($rPrgInd[$i][COL_SASARANREALISASITW1])&&$rPrgInd[$i][COL_SASARANREALISASITW1]!=null?$rPrgInd[$i][COL_SASARANREALISASITW1]:'N/A';
                  $hrefRealTW1 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaranprog/'.COL_SASARANREALISASITW1.'/'.$rPrgInd[$i][COL_SASARANID]).'" data-value="'.$valRealTW1.'">'.$txtRealTW1.'</a>';

                  $valRealTW2 = !empty($rPrgInd[$i][COL_SASARANREALISASITW2])&&$rPrgInd[$i][COL_SASARANREALISASITW2]!=null?$rPrgInd[$i][COL_SASARANREALISASITW2]:'';
                  $txtRealTW2 = !empty($rPrgInd[$i][COL_SASARANREALISASITW2])&&$rPrgInd[$i][COL_SASARANREALISASITW2]!=null?$rPrgInd[$i][COL_SASARANREALISASITW2]:'N/A';
                  $hrefRealTW2 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaranprog/'.COL_SASARANREALISASITW2.'/'.$rPrgInd[$i][COL_SASARANID]).'" data-value="'.$valRealTW2.'">'.$txtRealTW2.'</a>';

                  $valRealTW3 = !empty($rPrgInd[$i][COL_SASARANREALISASITW3])&&$rPrgInd[$i][COL_SASARANREALISASITW3]!=null?$rPrgInd[$i][COL_SASARANREALISASITW3]:'';
                  $txtRealTW3 = !empty($rPrgInd[$i][COL_SASARANREALISASITW3])&&$rPrgInd[$i][COL_SASARANREALISASITW3]!=null?$rPrgInd[$i][COL_SASARANREALISASITW3]:'N/A';
                  $hrefRealTW3 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaranprog/'.COL_SASARANREALISASITW3.'/'.$rPrgInd[$i][COL_SASARANID]).'" data-value="'.$valRealTW3.'">'.$txtRealTW3.'</a>';

                  $valRealTW4 = !empty($rPrgInd[$i][COL_SASARANREALISASITW4])&&$rPrgInd[$i][COL_SASARANREALISASITW4]!=null?$rPrgInd[$i][COL_SASARANREALISASITW4]:'';
                  $txtRealTW4 = !empty($rPrgInd[$i][COL_SASARANREALISASITW4])&&$rPrgInd[$i][COL_SASARANREALISASITW4]!=null?$rPrgInd[$i][COL_SASARANREALISASITW4]:'N/A';
                  $hrefRealTW4 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaranprog/'.COL_SASARANREALISASITW4.'/'.$rPrgInd[$i][COL_SASARANID]).'" data-value="'.$valRealTW4.'">'.$txtRealTW4.'</a>';
                  ?>
                  <tr>
                    <td style="vertical-align: top"><?=strtoupper($rPrgInd[$i][COL_SASARANINDIKATOR])?></td>
                    <td style="vertical-align: middle; white-space: nowrap"><?=strtoupper($rPrgInd[$i][COL_SASARANSATUAN])?></td>
                    <td style="text-align: center; vertical-align: middle;; white-space: nowrap"><?=strtoupper($rPrgInd[$i][COL_SASARANTARGET])?></td>
                    <?php
                    if(!empty($modCetak) && $modCetak=='RENCANA') {
                      ?>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW1:$txtTargetTW1?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW2:$txtTargetTW2?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW3:$txtTargetTW3?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW4:$txtTargetTW4?></td>
                      <?php
                    } else if(!empty($modCetak) && $modCetak=='REALISASI') {
                      ?>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW1:$txtRealTW1?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW2:$txtRealTW2?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW3:$txtRealTW3?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW4:$txtRealTW4?></td>
                      <?php
                    } else {
                      ?>
                      <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW1:$txtTargetTW1?></td>
                      <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW1:$txtRealTW1?></td>
                      <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW2:$txtTargetTW2?></td>
                      <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW2:$txtRealTW2?></td>
                      <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW3:$txtTargetTW3?></td>
                      <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW3:$txtRealTW3?></td>
                      <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW4:$txtTargetTW4?></td>
                      <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW4:$txtRealTW4?></td>
                      <?php
                    }
                    ?>
                  </tr>
                  <?php
                }
              }

              foreach($rkegiatan as $keg) {
                $rKegInd = $this->db
                ->where(COL_IDKEGIATAN, $keg[COL_KEGIATANID])
                ->order_by(COL_SASARANNO)
                ->get(TBL_SAKIPV2_BID_KEGSASARAN)
                ->result_array();

                $rsubkeg = $this->db
                ->where(COL_IDKEGIATAN, $keg[COL_KEGIATANID])
                ->order_by(COL_SUBKEGKODE)
                ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
                ->result_array();
                ?>
                <tr>
                  <td <?=!empty($rKegInd)&&count($rKegInd)>1?'rowspan="'.(count($rKegInd)).'"':''?> style="vertical-align: top"><?=$keg[COL_KEGIATANKODE]?></td>
                  <td <?=!empty($rKegInd)&&count($rKegInd)>1?'rowspan="'.(count($rKegInd)).'"':''?> style="vertical-align: top"><?=strtoupper($keg[COL_KEGIATANURAIAN])?></td>
                  <?php
                  if(!empty($rKegInd)) {
                    $valTargetTW1 = !empty($rKegInd[0][COL_SASARANTARGETTW1])&&$rKegInd[0][COL_SASARANTARGETTW1]!=null?$rKegInd[0][COL_SASARANTARGETTW1]:'';
                    $txtTargetTW1 = !empty($rKegInd[0][COL_SASARANTARGETTW1])&&$rKegInd[0][COL_SASARANTARGETTW1]!=null?$rKegInd[0][COL_SASARANTARGETTW1]:'N/A';
                    $hrefTargetTW1 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasarankeg/'.COL_SASARANTARGETTW1.'/'.$rKegInd[0][COL_SASARANID]).'" data-value="'.$valTargetTW1.'">'.$txtTargetTW1.'</a>';

                    $valTargetTW2 = !empty($rKegInd[0][COL_SASARANTARGETTW2])&&$rKegInd[0][COL_SASARANTARGETTW2]!=null?$rKegInd[0][COL_SASARANTARGETTW2]:'';
                    $txtTargetTW2 = !empty($rKegInd[0][COL_SASARANTARGETTW2])&&$rKegInd[0][COL_SASARANTARGETTW2]!=null?$rKegInd[0][COL_SASARANTARGETTW2]:'N/A';
                    $hrefTargetTW2 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasarankeg/'.COL_SASARANTARGETTW2.'/'.$rKegInd[0][COL_SASARANID]).'" data-value="'.$valTargetTW2.'">'.$txtTargetTW2.'</a>';

                    $valTargetTW3 = !empty($rKegInd[0][COL_SASARANTARGETTW3])&&$rKegInd[0][COL_SASARANTARGETTW3]!=null?$rKegInd[0][COL_SASARANTARGETTW3]:'';
                    $txtTargetTW3 = !empty($rKegInd[0][COL_SASARANTARGETTW3])&&$rKegInd[0][COL_SASARANTARGETTW3]!=null?$rKegInd[0][COL_SASARANTARGETTW3]:'N/A';
                    $hrefTargetTW3 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasarankeg/'.COL_SASARANTARGETTW3.'/'.$rKegInd[0][COL_SASARANID]).'" data-value="'.$valTargetTW3.'">'.$txtTargetTW3.'</a>';

                    $valTargetTW4 = !empty($rKegInd[0][COL_SASARANTARGETTW4])&&$rKegInd[0][COL_SASARANTARGETTW4]!=null?$rKegInd[0][COL_SASARANTARGETTW4]:'';
                    $txtTargetTW4 = !empty($rKegInd[0][COL_SASARANTARGETTW4])&&$rKegInd[0][COL_SASARANTARGETTW4]!=null?$rKegInd[0][COL_SASARANTARGETTW4]:'N/A';
                    $hrefTargetTW4 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasarankeg/'.COL_SASARANTARGETTW4.'/'.$rKegInd[0][COL_SASARANID]).'" data-value="'.$valTargetTW4.'">'.$txtTargetTW4.'</a>';

                    $valRealTW1 = !empty($rKegInd[0][COL_SASARANREALISASITW1])&&$rKegInd[0][COL_SASARANREALISASITW1]!=null?$rKegInd[0][COL_SASARANREALISASITW1]:'';
                    $txtRealTW1 = !empty($rKegInd[0][COL_SASARANREALISASITW1])&&$rKegInd[0][COL_SASARANREALISASITW1]!=null?$rKegInd[0][COL_SASARANREALISASITW1]:'N/A';
                    $hrefRealTW1 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasarankeg/'.COL_SASARANREALISASITW1.'/'.$rKegInd[0][COL_SASARANID]).'" data-value="'.$valRealTW1.'">'.$txtRealTW1.'</a>';

                    $valRealTW2 = !empty($rKegInd[0][COL_SASARANREALISASITW2])&&$rKegInd[0][COL_SASARANREALISASITW2]!=null?$rKegInd[0][COL_SASARANREALISASITW2]:'';
                    $txtRealTW2 = !empty($rKegInd[0][COL_SASARANREALISASITW2])&&$rKegInd[0][COL_SASARANREALISASITW2]!=null?$rKegInd[0][COL_SASARANREALISASITW2]:'N/A';
                    $hrefRealTW2 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasarankeg/'.COL_SASARANREALISASITW2.'/'.$rKegInd[0][COL_SASARANID]).'" data-value="'.$valRealTW2.'">'.$txtRealTW2.'</a>';

                    $valRealTW3 = !empty($rKegInd[0][COL_SASARANREALISASITW3])&&$rKegInd[0][COL_SASARANREALISASITW3]!=null?$rKegInd[0][COL_SASARANREALISASITW3]:'';
                    $txtRealTW3 = !empty($rKegInd[0][COL_SASARANREALISASITW3])&&$rKegInd[0][COL_SASARANREALISASITW3]!=null?$rKegInd[0][COL_SASARANREALISASITW3]:'N/A';
                    $hrefRealTW3 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasarankeg/'.COL_SASARANREALISASITW3.'/'.$rKegInd[0][COL_SASARANID]).'" data-value="'.$valRealTW3.'">'.$txtRealTW3.'</a>';

                    $valRealTW4 = !empty($rKegInd[0][COL_SASARANREALISASITW4])&&$rKegInd[0][COL_SASARANREALISASITW4]!=null?$rKegInd[0][COL_SASARANREALISASITW4]:'';
                    $txtRealTW4 = !empty($rKegInd[0][COL_SASARANREALISASITW4])&&$rKegInd[0][COL_SASARANREALISASITW4]!=null?$rKegInd[0][COL_SASARANREALISASITW4]:'N/A';
                    $hrefRealTW4 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasarankeg/'.COL_SASARANREALISASITW4.'/'.$rKegInd[0][COL_SASARANID]).'" data-value="'.$valRealTW4.'">'.$txtRealTW4.'</a>';
                    ?>
                    <td style="vertical-align: top"><?=strtoupper($rKegInd[0][COL_SASARANINDIKATOR])?></td>
                    <td style="vertical-align: middle; white-space: nowrap"><?=!empty($rKegInd)?strtoupper($rKegInd[0][COL_SASARANSATUAN]):'-'?></td>
                    <td style="text-align: center; vertical-align: middle;; white-space: nowrap"><?=!empty($rKegInd)?strtoupper($rKegInd[0][COL_SASARANTARGET]):'-'?></td>
                    <?php
                    if(!empty($modCetak) && $modCetak=='RENCANA') {
                      ?>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW1:$txtTargetTW1?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW2:$txtTargetTW2?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW3:$txtTargetTW3?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW4:$txtTargetTW4?></td>
                      <?php
                    } else if(!empty($modCetak) && $modCetak=='REALISASI') {
                      ?>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW1:$txtRealTW1?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW2:$txtRealTW2?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW3:$txtRealTW3?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW4:$txtRealTW4?></td>
                      <?php
                    } else {
                      ?>
                      <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW1:$txtTargetTW1?></td>
                      <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW1:$txtRealTW1?></td>
                      <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW2:$txtTargetTW2?></td>
                      <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW2:$txtRealTW2?></td>
                      <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW3:$txtTargetTW3?></td>
                      <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW3:$txtRealTW3?></td>
                      <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW4:$txtTargetTW4?></td>
                      <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW4:$txtRealTW4?></td>
                      <?php
                    }
                    ?>
                    <?php
                  } else {
                    ?>
                    <td colspan="11" style="text-align: center; vertical-align: middle; white-space: nowrap">(KOSONG)</td>
                    <?php
                  }
                  ?>
                </tr>
                <?php
                if(!empty($rKegInd) && count($rKegInd)>1) {
                  for($i=1; $i<count($rKegInd); $i++) {
                    $valTargetTW1 = !empty($rKegInd[$i][COL_SASARANTARGETTW1])&&$rKegInd[$i][COL_SASARANTARGETTW1]!=null?$rKegInd[$i][COL_SASARANTARGETTW1]:'';
                    $txtTargetTW1 = !empty($rKegInd[$i][COL_SASARANTARGETTW1])&&$rKegInd[$i][COL_SASARANTARGETTW1]!=null?$rKegInd[$i][COL_SASARANTARGETTW1]:'N/A';
                    $hrefTargetTW1 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasarankeg/'.COL_SASARANTARGETTW1.'/'.$rKegInd[$i][COL_SASARANID]).'" data-value="'.$valTargetTW1.'">'.$txtTargetTW1.'</a>';

                    $valTargetTW2 = !empty($rKegInd[$i][COL_SASARANTARGETTW2])&&$rKegInd[$i][COL_SASARANTARGETTW2]!=null?$rKegInd[$i][COL_SASARANTARGETTW2]:'';
                    $txtTargetTW2 = !empty($rKegInd[$i][COL_SASARANTARGETTW2])&&$rKegInd[$i][COL_SASARANTARGETTW2]!=null?$rKegInd[$i][COL_SASARANTARGETTW2]:'N/A';
                    $hrefTargetTW2 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasarankeg/'.COL_SASARANTARGETTW2.'/'.$rKegInd[$i][COL_SASARANID]).'" data-value="'.$valTargetTW2.'">'.$txtTargetTW2.'</a>';

                    $valTargetTW3 = !empty($rKegInd[$i][COL_SASARANTARGETTW3])&&$rKegInd[$i][COL_SASARANTARGETTW3]!=null?$rKegInd[$i][COL_SASARANTARGETTW3]:'';
                    $txtTargetTW3 = !empty($rKegInd[$i][COL_SASARANTARGETTW3])&&$rKegInd[$i][COL_SASARANTARGETTW3]!=null?$rKegInd[$i][COL_SASARANTARGETTW3]:'N/A';
                    $hrefTargetTW3 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasarankeg/'.COL_SASARANTARGETTW3.'/'.$rKegInd[$i][COL_SASARANID]).'" data-value="'.$valTargetTW3.'">'.$txtTargetTW3.'</a>';

                    $valTargetTW4 = !empty($rKegInd[$i][COL_SASARANTARGETTW4])&&$rKegInd[$i][COL_SASARANTARGETTW4]!=null?$rKegInd[$i][COL_SASARANTARGETTW4]:'';
                    $txtTargetTW4 = !empty($rKegInd[$i][COL_SASARANTARGETTW4])&&$rKegInd[$i][COL_SASARANTARGETTW4]!=null?$rKegInd[$i][COL_SASARANTARGETTW4]:'N/A';
                    $hrefTargetTW4 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasarankeg/'.COL_SASARANTARGETTW4.'/'.$rKegInd[$i][COL_SASARANID]).'" data-value="'.$valTargetTW4.'">'.$txtTargetTW4.'</a>';

                    $valRealTW1 = !empty($rKegInd[$i][COL_SASARANREALISASITW1])&&$rKegInd[$i][COL_SASARANREALISASITW1]!=null?$rKegInd[$i][COL_SASARANREALISASITW1]:'';
                    $txtRealTW1 = !empty($rKegInd[$i][COL_SASARANREALISASITW1])&&$rKegInd[$i][COL_SASARANREALISASITW1]!=null?$rKegInd[$i][COL_SASARANREALISASITW1]:'N/A';
                    $hrefRealTW1 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasarankeg/'.COL_SASARANREALISASITW1.'/'.$rKegInd[$i][COL_SASARANID]).'" data-value="'.$valRealTW1.'">'.$txtRealTW1.'</a>';

                    $valRealTW2 = !empty($rKegInd[$i][COL_SASARANREALISASITW2])&&$rKegInd[$i][COL_SASARANREALISASITW2]!=null?$rKegInd[$i][COL_SASARANREALISASITW2]:'';
                    $txtRealTW2 = !empty($rKegInd[$i][COL_SASARANREALISASITW2])&&$rKegInd[$i][COL_SASARANREALISASITW2]!=null?$rKegInd[$i][COL_SASARANREALISASITW2]:'N/A';
                    $hrefRealTW2 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasarankeg/'.COL_SASARANREALISASITW2.'/'.$rKegInd[$i][COL_SASARANID]).'" data-value="'.$valRealTW2.'">'.$txtRealTW2.'</a>';

                    $valRealTW3 = !empty($rKegInd[$i][COL_SASARANREALISASITW3])&&$rKegInd[$i][COL_SASARANREALISASITW3]!=null?$rKegInd[$i][COL_SASARANREALISASITW3]:'';
                    $txtRealTW3 = !empty($rKegInd[$i][COL_SASARANREALISASITW3])&&$rKegInd[$i][COL_SASARANREALISASITW3]!=null?$rKegInd[$i][COL_SASARANREALISASITW3]:'N/A';
                    $hrefRealTW3 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasarankeg/'.COL_SASARANREALISASITW3.'/'.$rKegInd[$i][COL_SASARANID]).'" data-value="'.$valRealTW3.'">'.$txtRealTW3.'</a>';

                    $valRealTW4 = !empty($rKegInd[$i][COL_SASARANREALISASITW4])&&$rKegInd[$i][COL_SASARANREALISASITW4]!=null?$rKegInd[$i][COL_SASARANREALISASITW4]:'';
                    $txtRealTW4 = !empty($rKegInd[$i][COL_SASARANREALISASITW4])&&$rKegInd[$i][COL_SASARANREALISASITW4]!=null?$rKegInd[$i][COL_SASARANREALISASITW4]:'N/A';
                    $hrefRealTW4 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasarankeg/'.COL_SASARANREALISASITW4.'/'.$rKegInd[$i][COL_SASARANID]).'" data-value="'.$valRealTW4.'">'.$txtRealTW4.'</a>';
                    ?>
                    <tr>
                      <td style="vertical-align: top"><?=strtoupper($rKegInd[$i][COL_SASARANINDIKATOR])?></td>
                      <td style="vertical-align: middle; white-space: nowrap"><?=strtoupper($rKegInd[$i][COL_SASARANSATUAN])?></td>
                      <td style="text-align: center; vertical-align: middle;; white-space: nowrap"><?=strtoupper($rKegInd[$i][COL_SASARANTARGET])?></td>
                      <?php
                      if(!empty($modCetak) && $modCetak=='RENCANA') {
                        ?>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW1:$txtTargetTW1?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW2:$txtTargetTW2?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW3:$txtTargetTW3?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW4:$txtTargetTW4?></td>
                        <?php
                      } else if(!empty($modCetak) && $modCetak=='REALISASI') {
                        ?>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW1:$txtRealTW1?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW2:$txtRealTW2?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW3:$txtRealTW3?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW4:$txtRealTW4?></td>
                        <?php
                      } else {
                        ?>
                        <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW1:$txtTargetTW1?></td>
                        <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW1:$txtRealTW1?></td>
                        <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW2:$txtTargetTW2?></td>
                        <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW2:$txtRealTW2?></td>
                        <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW3:$txtTargetTW3?></td>
                        <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW3:$txtRealTW3?></td>
                        <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW4:$txtTargetTW4?></td>
                        <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW4:$txtRealTW4?></td>
                        <?php
                      }
                      ?>
                    </tr>
                    <?php
                  }
                }

                foreach($rsubkeg as $sub) {
                  $rSubKegInd = $this->db
                  ->where(COL_IDSUBKEG, $sub[COL_SUBKEGID])
                  ->order_by(COL_SASARANNO)
                  ->get(TBL_SAKIPV2_SUBBID_SUBKEGSASARAN)
                  ->result_array();
                  ?>
                  <tr>
                    <td <?=!empty($rSubKegInd)&&count($rSubKegInd)>1?'rowspan="'.(count($rSubKegInd)).'"':''?> style="vertical-align: top"><?=$sub[COL_SUBKEGKODE]?></td>
                    <td <?=!empty($rSubKegInd)&&count($rSubKegInd)>1?'rowspan="'.(count($rSubKegInd)).'"':''?> style="vertical-align: top"><?=strtoupper($sub[COL_SUBKEGURAIAN])?></td>
                    <?php
                    if(!empty($rSubKegInd)) {
                      $valTargetTW1 = !empty($rSubKegInd[0][COL_SASARANTARGETTW1])&&$rSubKegInd[0][COL_SASARANTARGETTW1]!=null?$rSubKegInd[0][COL_SASARANTARGETTW1]:'';
                      $txtTargetTW1 = !empty($rSubKegInd[0][COL_SASARANTARGETTW1])&&$rSubKegInd[0][COL_SASARANTARGETTW1]!=null?$rSubKegInd[0][COL_SASARANTARGETTW1]:'N/A';
                      $hrefTargetTW1 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaransubkeg/'.COL_SASARANTARGETTW1.'/'.$rSubKegInd[0][COL_SASARANID]).'" data-value="'.$valTargetTW1.'">'.$txtTargetTW1.'</a>';

                      $valTargetTW2 = !empty($rSubKegInd[0][COL_SASARANTARGETTW2])&&$rSubKegInd[0][COL_SASARANTARGETTW2]!=null?$rSubKegInd[0][COL_SASARANTARGETTW2]:'';
                      $txtTargetTW2 = !empty($rSubKegInd[0][COL_SASARANTARGETTW2])&&$rSubKegInd[0][COL_SASARANTARGETTW2]!=null?$rSubKegInd[0][COL_SASARANTARGETTW2]:'N/A';
                      $hrefTargetTW2 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaransubkeg/'.COL_SASARANTARGETTW2.'/'.$rSubKegInd[0][COL_SASARANID]).'" data-value="'.$valTargetTW2.'">'.$txtTargetTW2.'</a>';

                      $valTargetTW3 = !empty($rSubKegInd[0][COL_SASARANTARGETTW3])&&$rSubKegInd[0][COL_SASARANTARGETTW3]!=null?$rSubKegInd[0][COL_SASARANTARGETTW3]:'';
                      $txtTargetTW3 = !empty($rSubKegInd[0][COL_SASARANTARGETTW3])&&$rSubKegInd[0][COL_SASARANTARGETTW3]!=null?$rSubKegInd[0][COL_SASARANTARGETTW3]:'N/A';
                      $hrefTargetTW3 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaransubkeg/'.COL_SASARANTARGETTW3.'/'.$rSubKegInd[0][COL_SASARANID]).'" data-value="'.$valTargetTW3.'">'.$txtTargetTW3.'</a>';

                      $valTargetTW4 = !empty($rSubKegInd[0][COL_SASARANTARGETTW4])&&$rSubKegInd[0][COL_SASARANTARGETTW4]!=null?$rSubKegInd[0][COL_SASARANTARGETTW4]:'';
                      $txtTargetTW4 = !empty($rSubKegInd[0][COL_SASARANTARGETTW4])&&$rSubKegInd[0][COL_SASARANTARGETTW4]!=null?$rSubKegInd[0][COL_SASARANTARGETTW4]:'N/A';
                      $hrefTargetTW4 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaransubkeg/'.COL_SASARANTARGETTW4.'/'.$rSubKegInd[0][COL_SASARANID]).'" data-value="'.$valTargetTW4.'">'.$txtTargetTW4.'</a>';

                      $valRealTW1 = !empty($rSubKegInd[0][COL_SASARANREALISASITW1])&&$rSubKegInd[0][COL_SASARANREALISASITW1]!=null?$rSubKegInd[0][COL_SASARANREALISASITW1]:'';
                      $txtRealTW1 = !empty($rSubKegInd[0][COL_SASARANREALISASITW1])&&$rSubKegInd[0][COL_SASARANREALISASITW1]!=null?$rSubKegInd[0][COL_SASARANREALISASITW1]:'N/A';
                      $hrefRealTW1 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaransubkeg/'.COL_SASARANREALISASITW1.'/'.$rSubKegInd[0][COL_SASARANID]).'" data-value="'.$valRealTW1.'">'.$txtRealTW1.'</a>';

                      $valRealTW2 = !empty($rSubKegInd[0][COL_SASARANREALISASITW2])&&$rSubKegInd[0][COL_SASARANREALISASITW2]!=null?$rSubKegInd[0][COL_SASARANREALISASITW2]:'';
                      $txtRealTW2 = !empty($rSubKegInd[0][COL_SASARANREALISASITW2])&&$rSubKegInd[0][COL_SASARANREALISASITW2]!=null?$rSubKegInd[0][COL_SASARANREALISASITW2]:'N/A';
                      $hrefRealTW2 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaransubkeg/'.COL_SASARANREALISASITW2.'/'.$rSubKegInd[0][COL_SASARANID]).'" data-value="'.$valRealTW2.'">'.$txtRealTW2.'</a>';

                      $valRealTW3 = !empty($rSubKegInd[0][COL_SASARANREALISASITW3])&&$rSubKegInd[0][COL_SASARANREALISASITW3]!=null?$rSubKegInd[0][COL_SASARANREALISASITW3]:'';
                      $txtRealTW3 = !empty($rSubKegInd[0][COL_SASARANREALISASITW3])&&$rSubKegInd[0][COL_SASARANREALISASITW3]!=null?$rSubKegInd[0][COL_SASARANREALISASITW3]:'N/A';
                      $hrefRealTW3 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaransubkeg/'.COL_SASARANREALISASITW3.'/'.$rSubKegInd[0][COL_SASARANID]).'" data-value="'.$valRealTW3.'">'.$txtRealTW3.'</a>';

                      $valRealTW4 = !empty($rSubKegInd[0][COL_SASARANREALISASITW4])&&$rSubKegInd[0][COL_SASARANREALISASITW4]!=null?$rSubKegInd[0][COL_SASARANREALISASITW4]:'';
                      $txtRealTW4 = !empty($rSubKegInd[0][COL_SASARANREALISASITW4])&&$rSubKegInd[0][COL_SASARANREALISASITW4]!=null?$rSubKegInd[0][COL_SASARANREALISASITW4]:'N/A';
                      $hrefRealTW4 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaransubkeg/'.COL_SASARANREALISASITW4.'/'.$rSubKegInd[0][COL_SASARANID]).'" data-value="'.$valRealTW4.'">'.$txtRealTW4.'</a>';
                      ?>
                      <td style="vertical-align: top"><?=strtoupper($rSubKegInd[0][COL_SASARANINDIKATOR])?></td>
                      <td style="vertical-align: middle; white-space: nowrap"><?=!empty($rSubKegInd)?strtoupper($rSubKegInd[0][COL_SASARANSATUAN]):'-'?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=!empty($rSubKegInd)?strtoupper($rSubKegInd[0][COL_SASARANTARGET]):'-'?></td>
                      <?php
                      if(!empty($modCetak) && $modCetak=='RENCANA') {
                        ?>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW1:$txtTargetTW1?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW2:$txtTargetTW2?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW3:$txtTargetTW3?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW4:$txtTargetTW4?></td>
                        <?php
                      } else if(!empty($modCetak) && $modCetak=='REALISASI') {
                        ?>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW1:$txtRealTW1?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW2:$txtRealTW2?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW3:$txtRealTW3?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW4:$txtRealTW4?></td>
                        <?php
                      } else {
                        ?>
                        <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW1:$txtTargetTW1?></td>
                        <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW1:$txtRealTW1?></td>
                        <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW2:$txtTargetTW2?></td>
                        <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW2:$txtRealTW2?></td>
                        <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW3:$txtTargetTW3?></td>
                        <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW3:$txtRealTW3?></td>
                        <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW4:$txtTargetTW4?></td>
                        <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW4:$txtRealTW4?></td>
                        <?php
                      }
                      ?>
                      <?php
                    } else {
                      ?>
                      <td colspan="11" style="text-align: center; vertical-align: middle; white-space: nowrap">(KOSONG)</td>
                      <?php
                    }
                    ?>
                  </tr>
                  <?php
                  if(!empty($rSubKegInd) && count($rSubKegInd)>1) {
                    for($i=1; $i<count($rSubKegInd); $i++) {
                      $valTargetTW1 = !empty($rSubKegInd[$i][COL_SASARANTARGETTW1])&&$rSubKegInd[$i][COL_SASARANTARGETTW1]!=null?$rSubKegInd[$i][COL_SASARANTARGETTW1]:'';
                      $txtTargetTW1 = !empty($rSubKegInd[$i][COL_SASARANTARGETTW1])&&$rSubKegInd[$i][COL_SASARANTARGETTW1]!=null?$rSubKegInd[$i][COL_SASARANTARGETTW1]:'N/A';
                      $hrefTargetTW1 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaransubkeg/'.COL_SASARANTARGETTW1.'/'.$rSubKegInd[$i][COL_SASARANID]).'" data-value="'.$valTargetTW1.'">'.$txtTargetTW1.'</a>';

                      $valTargetTW2 = !empty($rSubKegInd[$i][COL_SASARANTARGETTW2])&&$rSubKegInd[$i][COL_SASARANTARGETTW2]!=null?$rSubKegInd[$i][COL_SASARANTARGETTW2]:'';
                      $txtTargetTW2 = !empty($rSubKegInd[$i][COL_SASARANTARGETTW2])&&$rSubKegInd[$i][COL_SASARANTARGETTW2]!=null?$rSubKegInd[$i][COL_SASARANTARGETTW2]:'N/A';
                      $hrefTargetTW2 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaransubkeg/'.COL_SASARANTARGETTW2.'/'.$rSubKegInd[$i][COL_SASARANID]).'" data-value="'.$valTargetTW2.'">'.$txtTargetTW2.'</a>';

                      $valTargetTW3 = !empty($rSubKegInd[$i][COL_SASARANTARGETTW3])&&$rSubKegInd[$i][COL_SASARANTARGETTW3]!=null?$rSubKegInd[$i][COL_SASARANTARGETTW3]:'';
                      $txtTargetTW3 = !empty($rSubKegInd[$i][COL_SASARANTARGETTW3])&&$rSubKegInd[$i][COL_SASARANTARGETTW3]!=null?$rSubKegInd[$i][COL_SASARANTARGETTW3]:'N/A';
                      $hrefTargetTW3 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaransubkeg/'.COL_SASARANTARGETTW3.'/'.$rSubKegInd[$i][COL_SASARANID]).'" data-value="'.$valTargetTW3.'">'.$txtTargetTW3.'</a>';

                      $valTargetTW4 = !empty($rSubKegInd[$i][COL_SASARANTARGETTW4])&&$rSubKegInd[$i][COL_SASARANTARGETTW4]!=null?$rSubKegInd[$i][COL_SASARANTARGETTW4]:'';
                      $txtTargetTW4 = !empty($rSubKegInd[$i][COL_SASARANTARGETTW4])&&$rSubKegInd[$i][COL_SASARANTARGETTW4]!=null?$rSubKegInd[$i][COL_SASARANTARGETTW4]:'N/A';
                      $hrefTargetTW4 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaransubkeg/'.COL_SASARANTARGETTW4.'/'.$rSubKegInd[$i][COL_SASARANID]).'" data-value="'.$valTargetTW4.'">'.$txtTargetTW4.'</a>';

                      $valRealTW1 = !empty($rSubKegInd[$i][COL_SASARANREALISASITW1])&&$rSubKegInd[$i][COL_SASARANREALISASITW1]!=null?$rSubKegInd[$i][COL_SASARANREALISASITW1]:'';
                      $txtRealTW1 = !empty($rSubKegInd[$i][COL_SASARANREALISASITW1])&&$rSubKegInd[$i][COL_SASARANREALISASITW1]!=null?$rSubKegInd[$i][COL_SASARANREALISASITW1]:'N/A';
                      $hrefRealTW1 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaransubkeg/'.COL_SASARANREALISASITW1.'/'.$rSubKegInd[$i][COL_SASARANID]).'" data-value="'.$valRealTW1.'">'.$txtRealTW1.'</a>';

                      $valRealTW2 = !empty($rSubKegInd[$i][COL_SASARANREALISASITW2])&&$rSubKegInd[$i][COL_SASARANREALISASITW2]!=null?$rSubKegInd[$i][COL_SASARANREALISASITW2]:'';
                      $txtRealTW2 = !empty($rSubKegInd[$i][COL_SASARANREALISASITW2])&&$rSubKegInd[$i][COL_SASARANREALISASITW2]!=null?$rSubKegInd[$i][COL_SASARANREALISASITW2]:'N/A';
                      $hrefRealTW2 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaransubkeg/'.COL_SASARANREALISASITW2.'/'.$rSubKegInd[$i][COL_SASARANID]).'" data-value="'.$valRealTW2.'">'.$txtRealTW2.'</a>';

                      $valRealTW3 = !empty($rSubKegInd[$i][COL_SASARANREALISASITW3])&&$rSubKegInd[$i][COL_SASARANREALISASITW3]!=null?$rSubKegInd[$i][COL_SASARANREALISASITW3]:'';
                      $txtRealTW3 = !empty($rSubKegInd[$i][COL_SASARANREALISASITW3])&&$rSubKegInd[$i][COL_SASARANREALISASITW3]!=null?$rSubKegInd[$i][COL_SASARANREALISASITW3]:'N/A';
                      $hrefRealTW3 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaransubkeg/'.COL_SASARANREALISASITW3.'/'.$rSubKegInd[$i][COL_SASARANID]).'" data-value="'.$valRealTW3.'">'.$txtRealTW3.'</a>';

                      $valRealTW4 = !empty($rSubKegInd[$i][COL_SASARANREALISASITW4])&&$rSubKegInd[$i][COL_SASARANREALISASITW4]!=null?$rSubKegInd[$i][COL_SASARANREALISASITW4]:'';
                      $txtRealTW4 = !empty($rSubKegInd[$i][COL_SASARANREALISASITW4])&&$rSubKegInd[$i][COL_SASARANREALISASITW4]!=null?$rSubKegInd[$i][COL_SASARANREALISASITW4]:'N/A';
                      $hrefRealTW4 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaransubkeg/'.COL_SASARANREALISASITW4.'/'.$rSubKegInd[$i][COL_SASARANID]).'" data-value="'.$valRealTW4.'">'.$txtRealTW4.'</a>';
                      ?>
                      <tr>
                        <td style="vertical-align: top"><?=strtoupper($rSubKegInd[$i][COL_SASARANINDIKATOR])?></td>
                        <td style="vertical-align: middle; white-space: nowrap"><?=$rSubKegInd[$i][COL_SASARANSATUAN]?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$rSubKegInd[$i][COL_SASARANTARGET]?></td>
                        <?php
                        if(!empty($modCetak) && $modCetak=='RENCANA') {
                          ?>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW1:$txtTargetTW1?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW2:$txtTargetTW2?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW3:$txtTargetTW3?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW4:$txtTargetTW4?></td>
                          <?php
                        } else if(!empty($modCetak) && $modCetak=='REALISASI') {
                          ?>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW1:$txtRealTW1?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW2:$txtRealTW2?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW3:$txtRealTW3?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW4:$txtRealTW4?></td>
                          <?php
                        } else {
                          ?>
                          <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW1:$txtTargetTW1?></td>
                          <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW1:$txtRealTW1?></td>
                          <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW2:$txtTargetTW2?></td>
                          <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW2:$txtRealTW2?></td>
                          <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW3:$txtTargetTW3?></td>
                          <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW3:$txtRealTW3?></td>
                          <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW4:$txtTargetTW4?></td>
                          <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW4:$txtRealTW4?></td>
                          <?php
                        }
                        ?>
                      </tr>
                      <?php
                    }
                  }
                }
              }
            }
          }
        } else {
          ?>
          <tr>
            <td colspan="15">
              <p style="text-align: center; font-style: italic; margin-bottom: 0 !important">
                (KOSONG)
              </p>
            </td>
          </tr>
          <?php
        }
        ?>
      </tbody>
    </table>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){

});
</script>
