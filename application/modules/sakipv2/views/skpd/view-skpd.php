<?php
$ruser = GetLoggedUser();
$rOptSkpd = $this->db
->where(COL_SKPDISAKTIF, 1)
->order_by(COL_SKPDURUSAN, 'asc')
->order_by(COL_SKPDBIDANG, 'asc')
->order_by(COL_SKPDUNIT, 'asc')
->order_by(COL_SKPDSUBUNIT, 'asc')
->get(TBL_SAKIPV2_SKPD)
->result_array();

$rOptPemda = $this->db
->where(COL_PMDISAKTIF, 1)
->order_by(COL_PMDTAHUNMULAI, 'desc')
->get(TBL_SAKIPV2_PEMDA)
->result_array();

$rpemda = $this->db
->where(COL_PMDISAKTIF, 1)
->order_by(COL_PMDTAHUNMULAI, 'desc')
->get(TBL_SAKIPV2_PEMDA)
->row_array();

if(!empty($_GET['idPmd'])) {
  $rpemda = $this->db
  ->where(COL_PMDID, $_GET['idPmd'])
  ->get(TBL_SAKIPV2_PEMDA)
  ->row_array();
}

$rrenstra = array();
if(!empty($rpemda)) {
  $rrenstra = $this->db
  ->where(COL_IDPEMDA, $rpemda[COL_PMDID])
  ->where(COL_IDSKPD, $ropd[COL_SKPDID])
  ->order_by(COL_RENSTRAISAKTIF, 'desc')
  ->order_by(COL_RENSTRATAHUN, 'desc')
  ->order_by(COL_RENSTRAID, 'desc')
  ->get(TBL_SAKIPV2_SKPD_RENSTRA)
  ->result_array();
}
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?=$title?></h3>
      </div>
      <?php
      if(!empty($navs)) {
        ?>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <?php
            foreach($navs as $n) {
              if(!empty($n['link'])) {
                ?>
                <li class="breadcrumb-item"><a href="<?=$n['link']?>"><?=$n['text']?></a></li>
                <?php
              } else {
                ?>
                <li class="breadcrumb-item active"><?=$n['text']?></li>
                <?php
              }
            }
            ?>
          </ol>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-outline card-secondary">
          <?php
          if(!empty($subtitle)) {
            ?>
            <div class="card-header">
              <h4 class="card-title"><?=$subtitle?></h4>
            </div>
            <?php
          }
          ?>
          <div class="card-body p-0">
            <table class="table">
              <tbody>
                <tr>
                  <td class="text-right" style="width: 150px; white-space: nowrap; vertical-align: middle">NAMA SKPD</td>
                  <td style="width: 10px; white-space: nowrap; vertical-align: middle">:</td>
                  <td class="font-weight-bold">
                    <?php
                    if($ruser[COL_ROLEID] == ROLEADMIN) {
                      ?>
                      <select class="form-control" name="filterSkpd">
                        <?php
                        foreach($rOptSkpd as $opt) {
                          ?>
                          <option value="<?=site_url('sakipv2/skpd/index').'?opr=detail-skpd&id='.$opt[COL_SKPDID]?>" <?=$opt[COL_SKPDID]==$ropd[COL_SKPDID]?'selected':''?>>
                            <?=$opt[COL_SKPDURUSAN].'.'.$opt[COL_SKPDBIDANG].'.'.$opt[COL_SKPDUNIT].'.'.$opt[COL_SKPDSUBUNIT].' - '.strtoupper($opt[COL_SKPDNAMA])?>
                          </option>
                          <?php
                        }
                        ?>
                      </select>
                      <?php
                    } else {
                      echo strtoupper($ropd[COL_SKPDNAMA]);
                    }
                    ?>
                  </td>
                </tr>
                <tr>
                  <td class="text-right" style="width: 150px; white-space: nowrap; vertical-align: middle">INFO KOP SKPD</td>
                  <td style="width: 10px; white-space: nowrap; vertical-align: middle">:</td>
                  <td class="font-weight-bold">
                    <?=!empty($ropd[COL_SKPDKOP])?$ropd[COL_SKPDKOP]:'<span class="text-sm text-danger font-italic">(KOSONG)</span>'?>
                  </td>
                </tr>
                <tr>
                  <td class="text-right" style="width: 150px; white-space: nowrap; vertical-align: middle">NAMA PIMPINAN</td>
                  <td style="width: 10px; white-space: nowrap; vertical-align: middle">:</td>
                  <td class="font-weight-bold">
                    <?=!empty($ropd[COL_SKPDNAMAPIMPINAN])?$ropd[COL_SKPDNAMAPIMPINAN]:'<span class="text-sm text-danger font-italic">(KOSONG)</span>'?>
                  </td>
                </tr>
                <tr>
                  <td class="text-right" style="width: 150px; white-space: nowrap; vertical-align: middle">NAMA JABATAN PIMPINAN</td>
                  <td style="width: 10px; white-space: nowrap; vertical-align: middle">:</td>
                  <td class="font-weight-bold">
                    <?=!empty($ropd[COL_SKPDNAMAJABATAN])?$ropd[COL_SKPDNAMAJABATAN]:'KEPALA '.strtoupper($ropd[COL_SKPDNAMA])?>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="card-footer d-block">
            <div class="row">
              <div class="col-lg-12 text-center">
                <a href="<?=site_url('sakipv2/skpd/ajax-form-skpd/edit/'.$ropd[COL_SKPDID])?>" class="btn btn-primary btn-edit-skpd font-weight-bold"><i class="far fa-edit"></i>&nbsp;UPDATE SKPD</a>
              </div>
            </div>
          </div>
        </div>
        <div class="card card-outline card-secondary">
          <div class="card-header">
            <h4 class="card-title">
              RENSTRA PERIODE&nbsp;
              <select name="filterPmd" class="no-select2 font-weight-bold" style="padding-right: 1rem">
                <?php
                foreach($rOptPemda as $opt) {
                  $isSelected = '';
                  if(!empty($_GET['idPmd']) && $opt[COL_PMDID]==$_GET['idPmd']) {
                    $isSelected = 'selected';
                  } else if(empty($_GET['idPmd']) && $opt[COL_PMDISAKTIF]==1) {
                    $isSelected = 'selected';
                  }
                  ?>
                  <option value="<?=site_url('sakipv2/skpd/index').'?opr=detail-skpd&id='.$ropd[COL_SKPDID].'&idPmd='.$opt[COL_PMDID]?>" <?=$isSelected?>>
                    <?=$opt[COL_PMDTAHUNMULAI].' s.d '.$opt[COL_PMDTAHUNAKHIR].' - '.strtoupper($opt[COL_PMDPEJABAT])?>
                  </option>
                  <?php
                }
                ?>
              </select>
            </h4>
          </div>
          <div class="card-body p-0">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th style="width: 100px; white-space: nowrap; text-align: center !important">TAHUN</th>
                  <th>KETERANGAN</th>
                  <th style="width: 100px; white-space: nowrap; text-align: center !important">STATUS</th>
                  <th style="width: 100px; white-space: nowrap; text-align: center !important">AKSI</th>
                </tr>
              </thead>
              <tbody>
                <?php
                if(!empty($rrenstra)) {
                  foreach($rrenstra as $r) {
                    ?>
                    <tr>
                      <td class="text-center" style="width: 100px; white-space: nowrap">
                        <?=$r[COL_RENSTRATAHUN]?>
                      </td>
                      <td>
                        <?=$r[COL_RENSTRAURAIAN]?>
                      </td>
                      <td style="white-space: nowrap;">
                        <?=$r[COL_RENSTRAISAKTIF]==1?'<span class="badge bg-success">AKTIF</span>':'<span class="badge bg-secondary">INAKTIF</span>'?>
                      </td>
                      <td class="text-center" style="white-space: nowrap">
                        <a href="<?=site_url('sakipv2/skpd/ajax-form-renstra/edit/'.$r[COL_RENSTRAID])?>" data-toggle="tooltip" data-placement="bottom" title="UBAH" class="btn btn-primary btn-sm btn-edit-renstra"><i class="far fa-edit"></i></a>
                        <?php
                        if($r[COL_RENSTRAISAKTIF]==1) {
                          ?>
                          <button type="button" class="btn btn-secondary btn-sm" disabled><i class="far fa-check-circle"></i></button>
                          <?php
                        } else {
                          ?>
                          <a href="<?=site_url('sakipv2/skpd/ajax-change-renstra/activate/'.$r[COL_RENSTRAID])?>" data-toggle="tooltip" data-placement="bottom" title="AKTIFKAN" data-prompt="Mengaktifkan data RENSTRA SKPD akan otomatis me-NONAKTIF-kan data RENSTRA SKPD lainnya yang berstatus AKTIF." class="btn btn-success btn-sm btn-change-renstra"><i class="far fa-check-circle"></i></a>
                          <?php
                        }
                        ?>
                        <a href="<?=site_url('sakipv2/skpd/ajax-change-renstra/delete/'.$r[COL_RENSTRAID])?>" data-toggle="tooltip" data-placement="bottom" title="HAPUS" data-prompt="RENSTRA tidak dapat dihapus jika masih terdapat data turunan terkait (TUJUAN, SASARAN, PROGRAM, KEGIATAN, dll) di dalamnya." class="btn btn-danger btn-sm btn-change-renstra"><i class="far fa-times-circle"></i></a>
                        <a href="<?=site_url('sakipv2/skpd/index').'?opr=detail-renstra&id='.$r[COL_RENSTRAID]?>" data-toggle="tooltip" data-placement="bottom" title="TELUSURI" class="btn btn-info btn-sm"><i class="far fa-search"></i></a>
                      </td>
                    </tr>
                    <?php
                  }
                } else {
                  ?>
                  <tr>
                    <td colspan="4">
                      <p class="text-center font-italic mb-0">
                        BELUM ADA DATA
                      </p>
                    </td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </div>
          <?php
          if(!empty($rpemda)) {
            ?>
            <div class="card-footer">
              <a href="<?=site_url('sakipv2/skpd/ajax-form-renstra/add/'.$rpemda[COL_PMDID].'/'.$ropd[COL_SKPDID])?>" class="btn btn-primary btn-add-renstra font-weight-bold"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH RENSTRA</a>
            </div>
            <?php
          }
          ?>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modalFormSKPD" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"><?=strtoupper($ropd[COL_SKPDNAMA])?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalFormRenstra" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Form RENSTRA SKPD</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var modalFormSKPD = $('#modalFormSKPD');
var modalFormRenstra = $('#modalFormRenstra');
$(document).ready(function(){
  $('select[name=filterSkpd]').change(function(){
    var url = $(this).val();
    location.href = url;
  });

  $('select[name=filterPmd]').change(function(){
    var url = $(this).val();
    location.href = url;
  });

  modalFormSKPD.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalFormSKPD).empty();
  });
  modalFormRenstra.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalFormRenstra).empty();
  });

  $('.btn-edit-skpd').click(function() {
    var url = $(this).attr('href');
    modalFormSKPD.modal('show');
    $('.modal-body', modalFormSKPD).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modalFormSKPD).load(url, function(){
      $('button[type=submit]', modalFormSKPD).unbind('click').click(function(){
        $('form', modalFormSKPD).submit();
      });
    });
    return false;
  });

  $('.btn-add-renstra, .btn-edit-renstra').click(function() {
    var url = $(this).attr('href');
    modalFormRenstra.modal('show');
    $('.modal-body', modalFormRenstra).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modalFormRenstra).load(url, function(){
      $('button[type=submit]', modalFormRenstra).unbind('click').click(function(){
        $('form', modalFormRenstra).submit();
      });
    });
    return false;
  });

  $('.btn-change-renstra').click(function() {
    var url = $(this).attr('href');
    var prompt = $(this).data('prompt');
    swal({
      title: "APAKAH ANDA YAKIN?",
      text: prompt,
      icon: "warning",
      buttons: [
        'BATAL',
        'YAKIN'
      ],
    }).then(function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          url: url,
          method: "GET",
          dataType: "json"
        }).success(function(res) {
          if(res.error) {
            swal({
              title: 'ERROR',
              text: res.error,
              icon: 'error',
              buttons:false
            });
          } else {
            location.reload();
          }
        }).fail(function() {
          swal({
            title: 'SERVER ERROR',
            text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
            icon: 'error',
            buttons:false
          });
        }).done(function() {

        });

      } else {

      }
    })
    return false;
  });
});
</script>
