<?php
$rOptSasaran = $this->db
->where(COL_IDTUJUAN, $rsasaran[COL_IDTUJUAN])
->order_by(COL_SASARANNO)
->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN)
->result_array();

$rIndikatorSasaran = $this->db
->where(COL_IDSASARAN, $rsasaran[COL_SASARANID])
->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARANDET)
->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-4">
        <h3 class="m-0 font-weight-light"><?=$title?></h3>
      </div>
      <?php
      if(!empty($navs)) {
        ?>
        <div class="col-sm-8">
          <ol class="breadcrumb float-sm-right">
            <?php
            foreach($navs as $n) {
              if(!empty($n['link'])) {
                ?>
                <li class="breadcrumb-item"><a href="<?=$n['link']?>"><?=$n['text']?></a></li>
                <?php
              } else {
                ?>
                <li class="breadcrumb-item active"><?=$n['text']?></li>
                <?php
              }
            }
            ?>
          </ol>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-outline card-secondary">
          <?php
          if(!empty($subtitle)) {
            ?>
            <div class="card-header">
              <h4 class="card-title"><?=$subtitle?></h4>
            </div>
            <?php
          }
          ?>
          <div class="card-body p-0">
            <table class="table">
              <table class="table">
                <tbody>
                  <tr>
                    <td class="text-right"  style="width: 150px; white-space: nowrap">RENSTRA</td>
                    <td style="width: 10px; white-space: nowrap">:</td>
                    <td>
                      <a href="<?=site_url('sakipv2/skpd/index').'?opr=detail-renstra&id='.$rsasaran[COL_RENSTRAID]?>">
                        <strong><?=$rsasaran[COL_RENSTRATAHUN]?> - <?=$rsasaran[COL_RENSTRAURAIAN]?></strong>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td class="text-right"  style="width: 150px; white-space: nowrap">TUJUAN SKPD</td>
                    <td style="width: 10px; white-space: nowrap">:</td>
                    <td>
                      <a href="<?=site_url('sakipv2/skpd/index').'?opr=detail-tujuan&id='.$rsasaran[COL_TUJUANID]?>">
                        <strong><?=$rsasaran[COL_TUJUANNO]?>. <?=strtoupper($rsasaran[COL_TUJUANURAIAN])?></strong>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td class="text-right" style="width: 150px; white-space: nowrap; vertical-align: middle">SASARAN SKPD</td>
                    <td style="width: 10px; white-space: nowrap; vertical-align: middle">:</td>
                    <td>
                      <select class="form-control" name="filterSasaran">
                        <?php
                        foreach($rOptSasaran as $opt) {
                          ?>
                          <option value="<?=site_url('sakipv2/skpd/index').'?opr=detail-sasaran&id='.$opt[COL_SASARANID]?>" <?=$opt[COL_SASARANID]==$rsasaran[COL_SASARANID]?'selected':''?>><?=$opt[COL_SASARANNO].'. '.strtoupper($opt[COL_SASARANURAIAN])?></option>
                          <?php
                        }
                        ?>
                      </select>
                    </td>
                  </tr>
                  <tr>
                    <td class="text-right"  style="width: 150px; white-space: nowrap">SASARAN PEM. DAERAH</td>
                    <td style="width: 10px; white-space: nowrap">:</td>
                    <td>
                      <div class="row">
                        <div class="col-lg-1">MISI :</div>
                        <div class="col-lg-10"><?=$rsasaran[COL_MISINO]?>. <strong><?=strtoupper($rsasaran[COL_MISIURAIAN])?></strong></div>
                      </div>
                      <div class="row">
                        <div class="col-lg-1">TUJUAN :</div>
                        <div class="col-lg-10"><?=$rsasaran['PmdTujuanNo']?>. <strong><?=strtoupper($rsasaran['PmdTujuanUraian'])?></strong></div>
                      </div>
                      <div class="row">
                        <div class="col-lg-1">SASARAN :</div>
                        <div class="col-lg-10"><?=$rsasaran['PmdSasaranNo']?>. <strong><?=strtoupper($rsasaran['PmdSasaranUraian'])?></strong></div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td class="text-right" style="width: 150px; white-space: nowrap">INDIKATOR SASARAN</td>
                    <td style="width: 10px; white-space: nowrap">:</td>
                    <td>
                      <p class="mb-0">
                        <a href="<?=site_url('sakipv2/skpd/ajax-form-indikatorsasaran/add/'.$rsasaran[COL_SASARANID])?>" class="btn btn-primary btn-xs font-weight-bold" id="btnTambahIndikator"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH INDIKATOR</a>
                      </p>
                      <?php
                      if(!empty($rIndikatorSasaran)) {
                        ?>
                        <ul class="todo-list ui-sortable mt-2" data-widget="todo-list">
                          <?php
                          foreach($rIndikatorSasaran as $r) {
                            ?>
                            <li>
                              <div class="d-inline mr-2">
                                <a href="<?=site_url('sakipv2/skpd/ajax-form-indikatorsasaran/delete/'.$r[COL_SSRINDIKATORID])?>" class="btn btn-danger btn-xs btn-del-indikator"><i class="far fa-times-circle"></i></a>
                                <a href="<?=site_url('sakipv2/skpd/ajax-form-indikatorsasaran/edit/'.$r[COL_SSRINDIKATORID])?>" data-val="<?=$r[COL_SSRINDIKATORURAIAN]?>" class="btn btn-success btn-xs btn-edit-indikator"><i class="far fa-edit"></i></a>
                              </div>
                              <span class="text"><?=strtoupper($r[COL_SSRINDIKATORURAIAN])?></span>
                            </li>
                            <?php
                          }
                          ?>
                        </ul>
                        <?php
                      }
                      ?>
                    </td>
                  </tr>
                </tbody>
              </table>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function() {
  $('select[name=filterSasaran]').change(function(){
    var url = $(this).val();
    location.href = url;
  });

  $('#btnTambahIndikator, .btn-edit-indikator').click(function(){
    var url = $(this).attr('href');
    var val = $(this).data('val');
    swal({
      closeOnClickOutside: true,
      buttons: ['BATAL','SUBMIT'],
      text: "INDIKATOR SASARAN",
      content: {
        element: "input",
        attributes: {
          placeholder: "URAIAN INDIKATOR",
          type: "text",
          value: (val||'')
        }
      },
    }).then(function(val){
      if(val) {
        $.ajax({
          url: url,
          method: "POST",
          dataType: "json",
          data: {
            SsrIndikatorUraian: val
          }
        }).success(function(res) {
          if(res.error) {
            swal({
              title: 'ERROR',
              text: res.error,
              icon: 'error',
              buttons:false
            });
          } else {
            location.reload();
          }
        }).fail(function() {
          swal({
            title: 'SERVER ERROR',
            text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
            icon: 'error',
            buttons:false
          });
        }).done(function() {

        });
      }
    });

    return false;
  });

  $('.btn-del-indikator').click(function() {
    var url = $(this).attr('href');
    swal({
      title: "APAKAH ANDA YAKIN?",
      icon: "warning",
      buttons: [
        'BATAL',
        'YAKIN'
      ],
    }).then(function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          url: url,
          method: "GET",
          dataType: "json"
        }).success(function(res) {
          if(res.error) {
            swal({
              title: 'ERROR',
              text: res.error,
              icon: 'error',
              buttons:false
            });
          } else {
            location.reload();
          }
        }).fail(function() {
          swal({
            title: 'SERVER ERROR',
            text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
            icon: 'error',
            buttons:false
          });
        }).done(function() {

        });

      } else {

      }
    })
    return false;
  });
});
</script>
