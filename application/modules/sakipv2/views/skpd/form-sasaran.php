<?php
$rrenstra = $this->db
->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.".".COL_IDRENSTRA,"left")
->where(COL_TUJUANID, $idTujuan)
->get(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN)
->row_array();

$rOptSasaran = array();
if(!empty($rrenstra)) {
  $rOptSasaran = $this->db
  ->join(TBL_SAKIPV2_PEMDA_TUJUAN,TBL_SAKIPV2_PEMDA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_PEMDA_SASARAN.".".COL_IDTUJUAN,"left")
  ->join(TBL_SAKIPV2_PEMDA_MISI,TBL_SAKIPV2_PEMDA_MISI.'.'.COL_MISIID." = ".TBL_SAKIPV2_PEMDA_TUJUAN.".".COL_IDMISI,"left")
  ->where(TBL_SAKIPV2_PEMDA_MISI.'.'.COL_IDPMD, $rrenstra[COL_IDPEMDA])
  ->where(TBL_SAKIPV2_PEMDA_TUJUAN.'.'.COL_TUJUANID, $rrenstra[COL_IDTUJUANPMD])
  ->order_by(COL_MISINO)
  ->order_by(COL_SASARANNO)
  ->get(TBL_SAKIPV2_PEMDA_SASARAN)
  ->result_array();
}
?>
<form id="form-sasaran" action="<?=current_url()?>">
  <div class="form-group">
    <label>SASARAN PEM. DAERAH</label>
    <select class="form-control" name="<?=COL_IDSASARANPMD?>" style="width: 100%">
      <?php
      foreach($rOptSasaran as $opt) {
        ?>
        <option value="<?=$opt[COL_SASARANID]?>" <?=!empty($data)&&$data[COL_IDSASARANPMD]==$opt[COL_SASARANID]?'selected':''?>><?='Misi '.$opt[COL_MISINO].': Sasaran '.$opt[COL_SASARANNO].' - '.strtoupper($opt[COL_SASARANURAIAN])?></option>
        <?php
      }
      ?>
    </select>
  </div>
  <div class="form-group">
    <div class="row">
      <div class="col-lg-2">
        <label>NO</label>
        <input type="number" class="form-control" name="<?=COL_SASARANNO?>" placeholder="NO." value="<?=!empty($data)?$data[COL_SASARANNO]:''?>" required />
      </div>
      <div class="col-lg-10">
        <label>URAIAN</label>
        <textarea class="form-control" name="<?=COL_SASARANURAIAN?>" placeholder="URAIAN SASARAN" required><?=!empty($data)?$data[COL_SASARANURAIAN]:''?></textarea>
      </div>
    </div>
  </div>
  <table id="tbl-sasaran" class="table table-bordered">
    <thead class="bg-default">
      <tr>
        <th>INDIKATOR</th>
        <th>SUMBER DATA</th>
        <th>FORMULASI</th>
        <th style="width: 120px; white-space: nowrap">SATUAN</th>
        <th style="width: 120px; white-space: nowrap">TARGET</th>
        <th class="text-center" style="width: 10px; white-space: nowrap">
          <button type="button" id="btn-add-sasaran" class="btn btn-sm btn-primary" style="font-weight: bold"><i class="far fa-plus-circle"></i></button>
        </th>
      </tr>
    </thead>
    <tbody>
      <?php
      if(!empty($rIndikatorSasaran)) {
        foreach($rIndikatorSasaran as $r) {
          ?>
          <tr>
            <td>
              <textarea class="form-control" name="SsrIndikatorUraian[]" placeholder="INDIKATOR KEBERHASILAN"><?=strtoupper($r[COL_SSRINDIKATORURAIAN])?></textarea>
            </td>
            <td>
              <textarea class="form-control" name="SsrIndikatorSumberData[]" placeholder="SUMBER DATA"><?=strtoupper($r[COL_SSRINDIKATORSUMBERDATA])?></textarea>
            </td>
            <td>
              <textarea class="form-control" name="SsrIndikatorFormulasi[]" placeholder="FORMULASI"><?=strtoupper($r[COL_SSRINDIKATORFORMULASI])?></textarea>
            </td>
            <td>
              <select class="form-control" name="SsrIndikatorSatuan[]">
                <?=GetCombobox("SELECT * FROM ".TBL_SAKIP_MSATUAN." ORDER BY ".COL_NM_SATUAN, COL_NM_SATUAN, COL_NM_SATUAN, $r[COL_SSRINDIKATORSATUAN])?>
              </select>
            </td>
            <td>
              <input type="text" class="form-control" name="SsrIndikatorTarget[]" value="<?=$r[COL_SSRINDIKATORTARGET]?>" />
            </td>
            <td>
              <button type="button" class="btn btn-sm btn-danger btn-del-sasaran" style="font-weight: bold"><i class="fa fa-minus"></i></button>
            </td>
          </tr>
          <?php
        }
      } else {
        ?>
        <tr class="empty">
          <td colspan="6">
            <p class="text-center font-italic mb-0">BELUM ADA DATA</p>
          </td>
        </tr>
        <?php
      }
      ?>
    </tbody>
  </table>
</form>
<script type="text/javascript">
function addRowSasaran() {
  var html =' ';
  html += '<tr>';
  html += '<td>';
  html += '<textarea class="form-control" name="SsrIndikatorUraian[]" placeholder="URAIAN INDIKATOR"></textarea>';
  html += '</td>';
  html += '<td>';
  html += '<textarea class="form-control" name="SsrIndikatorSumberData[]" placeholder="SUMBER DATA"></textarea>';
  html += '</td>';
  html += '<td>';
  html += '<textarea class="form-control" name="SsrIndikatorFormulasi[]" placeholder="FORMULASI"></textarea>';
  html += '</td>';
  html += '<td>';
  html += '<select class="form-control" name="SsrIndikatorSatuan[]"><?=GetCombobox("SELECT * FROM ".TBL_SAKIP_MSATUAN." ORDER BY ".COL_NM_SATUAN, COL_NM_SATUAN, COL_NM_SATUAN)?></select>';
  html += '</td>';
  html += '<td>';
  html += '<input type="text" class="form-control" name="SsrIndikatorTarget[]" />';
  html += '</td>';
  html += '<td>';
  html += '<button type="button" class="btn btn-sm btn-danger btn-del-sasaran" style="font-weight: bold"><i class="fa fa-minus"></i></button>';
  html += '</td>';
  html += '</tr>';

  var empEl = $('tr.empty', $('tbody', $('#tbl-iku')));
  if(empEl) {
    $('tr.empty', $('tbody', $('#tbl-sasaran'))).remove();
  }

  $('tbody', $('#tbl-sasaran')).append(html);
  $('.btn-del-sasaran', $('tbody', $('#tbl-sasaran'))).unbind('click').click(function() {
    var row = $(this).closest('tr');
    row.remove();
  });
  $("select", $('tbody tr:last', $('#tbl-sasaran'))).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
}

$(document).ready(function(){
  $('#btn-add-sasaran', $('#form-sasaran')).click(function() {
    addRowSasaran();
  });

  $('.btn-del-sasaran', $('#form-sasaran')).unbind('click').click(function() {
    var row = $(this).closest('tr');
    row.remove();
  });

  $('#form-sasaran').validate({
    submitHandler: function(form) {
      var modal = $(form).closest('modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>
