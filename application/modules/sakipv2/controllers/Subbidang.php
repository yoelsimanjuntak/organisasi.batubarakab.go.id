<?php
class Subbidang extends MY_Controller {
  function __construct() {
      parent::__construct();
      if(!IsLogin()) {
        redirect(site_url());
      }
  }

  public function showErrUnathorized() {
    $act = $this->router->fetch_method();
    if($this->input->is_ajax_request() && strpos($act, "_form")===false) {
      ShowJsonError('MAAF, ANDA TIDAK MEMILIKI HAK AKSES.');
      exit();
    }else{
      echo 'MAAF, ANDA TIDAK MEMILIKI HAK AKSES.';
      exit();
    }
  }

  public function index() {
    $ruser = GetLoggedUser();
    $data['title'] = 'Kinerja Sub Bidang / Sub Bagian / Seksi';
    $opr = !empty($_GET['opr'])?$_GET['opr']:'';
    $id = !empty($_GET['id'])?$_GET['id']:'';

    switch ($opr) {
      case 'detail-subbidang':
        $data['subtitle'] = 'DETIL SUB BIDANG / SUB BAGIAN / SEKSI';
        $data['rsubbidang'] = $rsubbidang = $this->db
        ->join(TBL_SAKIPV2_BID,TBL_SAKIPV2_BID.'.'.COL_BIDID." = ".TBL_SAKIPV2_SUBBID.".".COL_IDBID,"left")
        ->where(COL_SUBBIDID, $id)
        ->get(TBL_SAKIPV2_SUBBID)
        ->row_array();
        if(empty($rsubbidang)) {
          show_error('PARAMETER TIDAK VALID!');
          exit();
        }
        $data['navs'] = array(
          array('text'=>'DAFTAR SUB BIDANG / SUB BAGIAN / SEKSI', 'link'=>site_url('sakipv2/subbidang/index')),
          array('text'=>strtoupper($rsubbidang[COL_SUBBIDNAMA]))
        );
        $this->template->load('main', 'sakipv2/subbidang/view-subbidang', $data);
      break;
      default:
        $data['subtitle'] = 'DAFTAR SUB BIDANG / SUB BAGIAN / SEKSI';
        $this->template->load('main', 'sakipv2/subbidang/index-subbidang', $data);
      break;
    }
  }

  public function ajax_form_subbidang($mode, $id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    $data['mode'] = $mode;
    if(!empty($_POST)) {
      if($mode=='add') {
        if(empty($id)) {
          ShowJsonError('PARAMETER TIDAK VALID!');
          exit();
        }

        $this->db->trans_begin();
        try {
          $rec = array(
            COL_IDBID=>$this->input->post(COL_IDBID),
            COL_SUBBIDNAMA=>$this->input->post(COL_SUBBIDNAMA),
            COL_SUBBIDNAMAPIMPINAN=>$this->input->post(COL_SUBBIDNAMAPIMPINAN),
            COL_SUBBIDNAMAJABATAN=>$this->input->post(COL_SUBBIDNAMAJABATAN),

            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );

          $res = $this->db->insert(TBL_SAKIPV2_SUBBID, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      } else if($mode=='edit') {
        $this->db->trans_begin();
        try {
          if(empty($id)) {
            throw new Exception('PARAMETER TIDAK VALID');
          }

          $rec = array(
            COL_IDBID=>$this->input->post(COL_IDBID),
            COL_SUBBIDNAMA=>$this->input->post(COL_SUBBIDNAMA),
            COL_SUBBIDNAMAPIMPINAN=>$this->input->post(COL_SUBBIDNAMAPIMPINAN),
            COL_SUBBIDNAMAJABATAN=>$this->input->post(COL_SUBBIDNAMAJABATAN),

            COL_UPDATEDBY=>$ruser[COL_USERNAME],
            COL_UPDATEDON=>date('Y-m-d H:i:s')
          );

          if(!empty($this->input->post(COL_SUBBIDTUGASPOKOK))) {
            $rec[COL_SUBBIDTUGASPOKOK] = $this->input->post(COL_SUBBIDTUGASPOKOK);
          }

          $arrFungsi = array();
          $arrIKU = array();
          $fungsiUraian = $this->input->post('FungsiUraian');
          $IKUUraian = $this->input->post('IKUUraian');
          $IKUSumberData = $this->input->post('IKUSumberData');
          $IKUFormulasi = $this->input->post('IKUFormulasi');
          $IKUSatuan = $this->input->post('IKUSatuan');
          $IKUTarget = $this->input->post('IKUTarget');

          if(is_array($fungsiUraian)) {
            for($i = 0; $i<count($fungsiUraian); $i++) {
              $arrFungsi[] = $fungsiUraian[$i];
            }
          }
          if(is_array($IKUUraian)) {
            for($i = 0; $i<count($IKUUraian); $i++) {
              $arrIKU[] = array(
                'Uraian'=>$IKUUraian[$i],
                'SumberData'=>$IKUSumberData[$i],
                'Formulasi'=>$IKUFormulasi[$i],
                'Satuan'=>$IKUSatuan[$i],
                'Target'=>$IKUTarget[$i]
              );
            }
          }



          if(!empty($arrFungsi)) {
            $rec[COL_SUBBIDFUNGSI]=json_encode($arrFungsi);
          }
          if(!empty($arrIKU)) {
            $rec[COL_SUBBIDIKU]=json_encode($arrIKU);
          }

          $res = $this->db->where(COL_SUBBIDID, $id)->update(TBL_SAKIPV2_SUBBID, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      }
    } else {
      if($mode=='edit') {
        $rdata = $this->db
        ->join(TBL_SAKIPV2_BID,TBL_SAKIPV2_BID.'.'.COL_BIDID." = ".TBL_SAKIPV2_SUBBID.".".COL_IDBID,"left")
        ->where(COL_SUBBIDID, $id)
        ->get(TBL_SAKIPV2_SUBBID)
        ->row_array();
        if(empty($rdata)) {
          echo '<p class="text-center text-danger font-weight-bold">PARAMETER TIDAK VALID!</p>';
          exit();
        }
        $data['data'] = $rdata;
        $data['rOptBidang'] = $this->db
        ->where(COL_IDRENSTRA, $rdata[COL_IDRENSTRA])
        ->order_by(COL_BIDISAKTIF,'desc')
        ->order_by(COL_BIDNAMA,'asc')
        ->get(TBL_SAKIPV2_BID)
        ->result_array();
      } else {
        $data['rOptBidang'] = $this->db
        ->where(COL_IDRENSTRA, $id)
        ->order_by(COL_BIDISAKTIF,'desc')
        ->order_by(COL_BIDNAMA,'asc')
        ->get(TBL_SAKIPV2_BID)
        ->result_array();
      }
      $this->load->view('sakipv2/subbidang/form-subbidang', $data);
    }
  }

  public function ajax_form_iku($mode, $id, $idx=null)  {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    $uraianIKU = $this->input->post('Uraian');

    if($mode == 'add-tupoksi' || $mode == 'add-iku') {
      if(empty($uraianIKU)) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $rsubbidang = $this->db
      ->where(COL_SUBBIDID, $id)
      ->get(TBL_SAKIPV2_SUBBID)
      ->row_array();
      if(empty($rsubbidang)) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $arrCurrJSON = array();
      if($mode == 'add-tupoksi') {
        if(!empty($rsubbidang[COL_SUBBIDTUPOKSI])) {
          $arrCurrJSON = json_decode($rsubbidang[COL_SUBBIDTUPOKSI]);
        }
      } else if($mode == 'add-iku') {
        if(!empty($rsubbidang[COL_SUBBIDIKU])) {
          $arrCurrJSON = json_decode($rsubbidang[COL_SUBBIDIKU]);
        }
      }

      $arrCurrJSON[] = $uraianIKU;

      $rec = array();
      if($mode == 'add-tupoksi') {
        $rec = array(COL_SUBBIDTUPOKSI=>json_encode($arrCurrJSON));
      } else if($mode == 'add-iku') {
        $rec = array(COL_SUBBIDIKU=>json_encode($arrCurrJSON));
      }

      $res = $this->db->where(COL_SUBBIDID, $id)->update(TBL_SAKIPV2_SUBBID, $rec);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('INPUT DATA BERHASIL');
      exit();

    } else if($mode == 'edit-tupoksi' || $mode == 'edit-iku') {
      if(empty($uraianIKU)) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }
      if($idx==null) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $rsubbidang = $this->db
      ->where(COL_SUBBIDID, $id)
      ->get(TBL_SAKIPV2_SUBBID)
      ->row_array();
      if(empty($rsubbidang)) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $arrCurrJSON = array();
      if($mode == 'edit-tupoksi') {
        if(!empty($rsubbidang[COL_SUBBIDTUPOKSI])) {
          $arrCurrJSON = json_decode($rsubbidang[COL_SUBBIDTUPOKSI]);
        }
      } else if($mode == 'edit-iku') {
        if(!empty($rsubbidang[COL_SUBBIDIKU])) {
          $arrCurrJSON = json_decode($rsubbidang[COL_SUBBIDIKU]);
        }
      }

      if(isset($arrCurrJSON[$idx])) {
        $arrCurrJSON[$idx] = $uraianIKU;
      }

      $rec = array();
      if($mode == 'edit-tupoksi') {
        $rec = array(COL_SUBBIDTUPOKSI=>json_encode($arrCurrJSON));
      } else if($mode == 'edit-iku') {
        $rec = array(COL_SUBBIDIKU=>json_encode($arrCurrJSON));
      }

      $res = $this->db->where(COL_SUBBIDID, $id)->update(TBL_SAKIPV2_SUBBID, $rec);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('INPUT DATA BERHASIL');
      exit();

    } else if($mode == 'delete-tupoksi' || $mode == 'delete-iku') {
      if($idx==null) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $rsubbidang = $this->db
      ->where(COL_SUBBIDID, $id)
      ->get(TBL_SAKIPV2_SUBBID)
      ->row_array();
      if(empty($rsubbidang)) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $arrCurrJSON = array();
      if($mode == 'delete-tupoksi') {
        if(!empty($rsubbidang[COL_SUBBIDTUPOKSI])) {
          $arrCurrJSON = json_decode($rsubbidang[COL_SUBBIDTUPOKSI]);
        }
      } else if($mode == 'delete-iku') {
        if(!empty($rsubbidang[COL_SUBBIDIKU])) {
          $arrCurrJSON = json_decode($rsubbidang[COL_SUBBIDIKU]);
        }
      }

      if(isset($arrCurrJSON[$idx])) {
        array_splice($arrCurrJSON,$idx,1);
      }

      $rec = array();
      if($mode == 'delete-tupoksi') {
        $rec = array(COL_SUBBIDTUPOKSI=>json_encode($arrCurrJSON));
      } else if($mode == 'delete-iku') {
        $rec = array(COL_SUBBIDIKU=>json_encode($arrCurrJSON));
      }

      $res = $this->db->where(COL_SUBBIDID, $id)->update(TBL_SAKIPV2_SUBBID, $rec);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('PERUBAHAN DATA BERHASIL');
      exit();
    }
  }

  public function ajax_form_subkegiatan($mode, $id, $idDPA=null) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    $data['mode'] = $mode;
    if(!empty($_POST)) {
      if($mode=='add') {
        if(empty($id) || empty($idDPA)) {
          ShowJsonError('PARAMETER TIDAK VALID!');
          exit();
        }

        $this->db->trans_begin();
        try {
          $rec = array(
            COL_IDSUBBID=>$this->input->post(COL_IDSUBBID),
            COL_IDKEGIATAN=>$this->input->post(COL_IDKEGIATAN),
            COL_SUBKEGKODE=>$this->input->post(COL_SUBKEGKODE),
            COL_SUBKEGURAIAN=>$this->input->post(COL_SUBKEGURAIAN),
            COL_SUBKEGPAGU=>toNum($this->input->post(COL_SUBKEGPAGU)),

            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );

          $res = $this->db->insert(TBL_SAKIPV2_SUBBID_SUBKEGIATAN, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      } else if($mode=='edit') {
        $this->db->trans_begin();
        try {
          if(empty($id)) {
            throw new Exception('PARAMETER TIDAK VALID');
          }

          $rec = array(
            COL_IDSUBBID=>$this->input->post(COL_IDSUBBID),
            COL_IDKEGIATAN=>$this->input->post(COL_IDKEGIATAN),
            COL_SUBKEGKODE=>$this->input->post(COL_SUBKEGKODE),
            COL_SUBKEGURAIAN=>$this->input->post(COL_SUBKEGURAIAN),
            COL_SUBKEGPAGU=>toNum($this->input->post(COL_SUBKEGPAGU)),

            COL_UPDATEDBY=>$ruser[COL_USERNAME],
            COL_UPDATEDON=>date('Y-m-d H:i:s')
          );

          $res = $this->db->where(COL_SUBKEGID, $id)->update(TBL_SAKIPV2_SUBBID_SUBKEGIATAN, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      }
    } else {
      if($mode=='edit') {
        $rdata = $this->db
        ->join(TBL_SAKIPV2_BID_KEGIATAN,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANID." = ".TBL_SAKIPV2_SUBBID_SUBKEGIATAN.".".COL_IDKEGIATAN,"left")
        ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
        ->join(TBL_SAKIPV2_SUBBID,TBL_SAKIPV2_SUBBID.'.'.COL_SUBBIDID." = ".TBL_SAKIPV2_SUBBID_SUBKEGIATAN.".".COL_IDSUBBID,"left")
        ->where(COL_SUBKEGID, $id)
        ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
        ->row_array();
        if(empty($rdata)) {
          echo '<p class="text-center text-danger font-weight-bold">PARAMETER TIDAK VALID!</p>';
          exit();
        }
        $data['data'] = $rdata;
      } else if($mode=='add') {
        $data['def'] = array(
          COL_IDSUBBID=>$id,
          COL_IDDPA=>$idDPA
        );

        $rsubbidang = $this->db->where(COL_SUBBIDID, $id)->get(TBL_SAKIPV2_SUBBID)->row_array();
        if(!empty($rsubbidang)) {
          $data['def'][COL_IDBID] = $rsubbidang[COL_IDBID];
        }
      }

      if($mode=='add' || $mode=='edit') {
        $this->load->view('sakipv2/subbidang/form-subkegiatan', $data);
      }
    }
  }

  public function ajax_change_subkegiatan($mode, $id)  {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    if($mode=='delete') {
      $res = $this->db->where(COL_SUBKEGID, $id)->delete(TBL_SAKIPV2_SUBBID_SUBKEGIATAN);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }
    } else {
      ShowJsonError('PARAMETER TIDAK VALID');
      exit();
    }

    ShowJsonSuccess('BERHASIL MENGUBAH DATA');
    exit();
  }

  public function ajax_form_sasaran($mode, $id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    $data['mode'] = $mode;
    if(!empty($_POST)) {
      $ruser = GetLoggedUser();
      if($mode=='sasaran-subkeg') {
        if(empty($id)) {
          ShowJsonError('PARAMETER TIDAK VALID!');
          exit();
        }

        $this->db->trans_begin();
        try {
          $arrSasaranNo = $this->input->post(COL_SASARANNO);
          $arrSasaranUraian = $this->input->post(COL_SASARANURAIAN);
          $arrSasaranIndikator = $this->input->post(COL_SASARANINDIKATOR);
          $arrSasaranSatuan = $this->input->post(COL_SASARANSATUAN);
          $arrSasaranTarget = $this->input->post(COL_SASARANTARGET);
          $arrSasaran = [];

          if(is_array($arrSasaranNo)) {
            for($i = 0; $i<count($arrSasaranNo); $i++) {
              $arrSasaran[] = array(
                COL_IDSUBKEG => $id,
                COL_SASARANNO => $arrSasaranNo[$i],
                COL_SASARANURAIAN => $arrSasaranUraian[$i],
                COL_SASARANINDIKATOR => $arrSasaranIndikator[$i],
                COL_SASARANSATUAN => $arrSasaranSatuan[$i],
                COL_SASARANTARGET => $arrSasaranTarget[$i],

                COL_CREATEDON=>date('Y-m-d H:i:s'),
                COL_CREATEDBY=>$ruser[COL_USERNAME]
              );
            }
          }

          $res = $this->db->where(COL_IDSUBKEG, $id)->delete(TBL_SAKIPV2_SUBBID_SUBKEGSASARAN);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }
          if(!empty($arrSasaran)) {
            $res = $this->db->insert_batch(TBL_SAKIPV2_SUBBID_SUBKEGSASARAN, $arrSasaran);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception($err['message']);
            }
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      }
    } else {
      $data['rsasaran'] = $this->db->where(COL_IDSUBKEG, $id)->get(TBL_SAKIPV2_SUBBID_SUBKEGSASARAN)->result_array();
      $this->load->view('sakipv2/subbidang/form-sasaran', $data);
    }
  }

  public function ajax_change_subbidang($mode, $id)  {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    if($mode=='delete') {
      $res = $this->db->where(COL_SUBBIDID, $id)->delete(TBL_SAKIPV2_SUBBID);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }
    } else  if($mode=='activate') {
      $this->db->trans_begin();
      $res = $this->db->where(COL_SUBBIDID, $id)->update(TBL_SAKIPV2_SUBBID, array(COL_SUBBIDISAKTIF=>1));
      if(!$res) {
        $this->db->trans_rollback();
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }
      $this->db->trans_commit();
    } else  if($mode=='suspend') {
      $this->db->trans_begin();
      $res = $this->db->where(COL_SUBBIDID, $id)->update(TBL_SAKIPV2_SUBBID, array(COL_SUBBIDISAKTIF=>0));
      if(!$res) {
        $this->db->trans_rollback();
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }
      $this->db->trans_commit();
    } else {
      ShowJsonError('PARAMETER TIDAK VALID');
      exit();
    }

    ShowJsonSuccess('BERHASIL MENGUBAH DATA');
    exit();
  }
}
?>
