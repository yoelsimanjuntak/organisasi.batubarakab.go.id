<?php
class Individu extends MY_Controller {
  function __construct() {
      parent::__construct();
      if(!IsLogin()) {
        redirect(site_url());
      }
  }

  public function showErrUnathorized() {
    $act = $this->router->fetch_method();
    if($this->input->is_ajax_request() && strpos($act, "_form")===false) {
      ShowJsonError('MAAF, ANDA TIDAK MEMILIKI HAK AKSES.');
      exit();
    }else{
      echo 'MAAF, ANDA TIDAK MEMILIKI HAK AKSES.';
      exit();
    }
  }

  public function index() {
    $ruser = GetLoggedUser();
    $data['title'] = 'Kinerja Individu';
    $opr = !empty($_GET['opr'])?$_GET['opr']:'';
    $id = !empty($_GET['id'])?$_GET['id']:'';

    switch ($opr) {
      case 'detail-individu':
        $data['subtitle'] = 'DETIL KINERJA INDIVIDU';
        $data['rpelaksana'] = $rpelaksana = $this->db
        ->where(COL_PLSID, $id)
        ->get(TBL_SAKIPV2_SUBBID_PELAKSANA)
        ->row_array();
        if(empty($rpelaksana)) {
          show_error('PARAMETER TIDAK VALID!');
          exit();
        }
        $data['navs'] = array(
          array('text'=>'DAFTAR JAB. PELAKSANA / INDIVIDU', 'link'=>site_url('sakipv2/individu/index')),
          array('text'=>strtoupper($rpelaksana[COL_PLSNAMA]))
        );
        $this->template->load('main', 'sakipv2/individu/view-jabatan', $data);
      break;

      default:
        $data['subtitle'] = 'DAFTAR NOMENKLATUR';
        $this->template->load('main', 'sakipv2/individu/index', $data);
      break;
    }
  }

  public function ajax_form_individu($mode, $id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    $data['mode'] = $mode;
    if(!empty($_POST)) {
      if($mode=='add') {
        if(empty($id)) {
          ShowJsonError('PARAMETER TIDAK VALID!');
          exit();
        }

        $this->db->trans_begin();
        try {
          $rec = array(
            COL_IDSUBBID=>$this->input->post(COL_IDSUBBID),
            COL_PLSNAMA=>$this->input->post(COL_PLSNAMA),
            COL_PLSNAMAPEGAWAI=>$this->input->post(COL_PLSNAMAPEGAWAI),

            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );

          if(!empty($this->input->post(COL_PLSTUGASPOKOK))) {
            $rec[COL_PLSTUGASPOKOK] = $this->input->post(COL_PLSTUGASPOKOK);
          }

          $arrFungsi = array();
          $arrIKU = array();
          $fungsiUraian = $this->input->post('FungsiUraian');
          $IKUUraian = $this->input->post('IKUUraian');
          $IKUSumberData = $this->input->post('IKUSumberData');
          $IKUFormulasi = $this->input->post('IKUFormulasi');
          $IKUSatuan = $this->input->post('IKUSatuan');
          $IKUTarget = $this->input->post('IKUTarget');

          for($i = 0; $i<count($fungsiUraian); $i++) {
            $arrFungsi[] = $fungsiUraian[$i];
          }
          for($i = 0; $i<count($IKUUraian); $i++) {
            $arrIKU[] = array(
              'Uraian'=>$IKUUraian[$i],
              'SumberData'=>$IKUSumberData[$i],
              'Formulasi'=>$IKUFormulasi[$i],
              'Satuan'=>$IKUSatuan[$i],
              'Target'=>$IKUTarget[$i]
            );
          }

          if(!empty($arrFungsi)) {
            $rec[COL_PLSFUNGSI]=json_encode($arrFungsi);
          }
          if(!empty($arrIKU)) {
            $rec[COL_PLSIKU]=json_encode($arrIKU);
          }

          $res = $this->db->insert(TBL_SAKIPV2_SUBBID_PELAKSANA, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      } else if($mode=='edit') {
        $this->db->trans_begin();
        try {
          if(empty($id)) {
            throw new Exception('PARAMETER TIDAK VALID');
          }

          $rec = array(
            COL_IDSUBBID=>$this->input->post(COL_IDSUBBID),
            COL_PLSNAMA=>$this->input->post(COL_PLSNAMA),
            COL_PLSNAMAPEGAWAI=>$this->input->post(COL_PLSNAMAPEGAWAI),

            COL_UPDATEDBY=>$ruser[COL_USERNAME],
            COL_UPDATEDON=>date('Y-m-d H:i:s')
          );

          if(!empty($this->input->post(COL_PLSTUGASPOKOK))) {
            $rec[COL_PLSTUGASPOKOK] = $this->input->post(COL_PLSTUGASPOKOK);
          }

          $arrFungsi = array();
          $arrIKU = array();
          $fungsiUraian = $this->input->post('FungsiUraian');
          $IKUUraian = $this->input->post('IKUUraian');
          $IKUSumberData = $this->input->post('IKUSumberData');
          $IKUFormulasi = $this->input->post('IKUFormulasi');
          $IKUSatuan = $this->input->post('IKUSatuan');
          $IKUTarget = $this->input->post('IKUTarget');

          for($i = 0; $i<count($fungsiUraian); $i++) {
            $arrFungsi[] = $fungsiUraian[$i];
          }
          for($i = 0; $i<count($IKUUraian); $i++) {
            $arrIKU[] = array(
              'Uraian'=>$IKUUraian[$i],
              'SumberData'=>$IKUSumberData[$i],
              'Formulasi'=>$IKUFormulasi[$i],
              'Satuan'=>$IKUSatuan[$i],
              'Target'=>$IKUTarget[$i]
            );
          }

          if(!empty($arrFungsi)) {
            $rec[COL_PLSFUNGSI]=json_encode($arrFungsi);
          }
          if(!empty($arrIKU)) {
            $rec[COL_PLSIKU]=json_encode($arrIKU);
          }

          $res = $this->db->where(COL_PLSID, $id)->update(TBL_SAKIPV2_SUBBID_PELAKSANA, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      }
    } else {
      if($mode=='edit') {
        $rdata = $this->db
        ->join(TBL_SAKIPV2_SUBBID,TBL_SAKIPV2_SUBBID.'.'.COL_SUBBIDID." = ".TBL_SAKIPV2_SUBBID_PELAKSANA.".".COL_IDSUBBID,"left")
        ->join(TBL_SAKIPV2_BID,TBL_SAKIPV2_BID.'.'.COL_BIDID." = ".TBL_SAKIPV2_SUBBID.".".COL_IDBID,"left")
        ->where(COL_PLSID, $id)
        ->get(TBL_SAKIPV2_SUBBID_PELAKSANA)
        ->row_array();
        if(empty($rdata)) {
          echo '<p class="text-center text-danger font-weight-bold">PARAMETER TIDAK VALID!</p>';
          exit();
        }
        $data['data'] = $rdata;
        $data['rOptSubbidang'] = $this->db
        ->join(TBL_SAKIPV2_BID,TBL_SAKIPV2_BID.'.'.COL_BIDID." = ".TBL_SAKIPV2_SUBBID.".".COL_IDBID,"left")
        ->where(COL_IDRENSTRA, $rdata[COL_IDRENSTRA])
        ->order_by(COL_SUBBIDISAKTIF,'desc')
        ->order_by(COL_SUBBIDNAMA,'asc')
        ->get(TBL_SAKIPV2_SUBBID)
        ->result_array();
      } else {
        $data['rOptSubbidang'] = $this->db
        ->join(TBL_SAKIPV2_BID,TBL_SAKIPV2_BID.'.'.COL_BIDID." = ".TBL_SAKIPV2_SUBBID.".".COL_IDBID,"left")
        ->where(COL_IDRENSTRA, $id)
        ->order_by(COL_SUBBIDISAKTIF,'desc')
        ->order_by(COL_SUBBIDNAMA,'asc')
        ->get(TBL_SAKIPV2_SUBBID)
        ->result_array();
      }
      $this->load->view('sakipv2/individu/form', $data);
    }
  }

  public function ajax_change_individu($mode, $id)  {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }
    
    if($mode=='delete') {
      $res = $this->db->where(COL_PLSID, $id)->delete(TBL_SAKIPV2_SUBBID_PELAKSANA);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }
    } else {
      ShowJsonError('PARAMETER TIDAK VALID');
      exit();
    }

    ShowJsonSuccess('BERHASIL MENGUBAH DATA');
    exit();
  }
}
?>
