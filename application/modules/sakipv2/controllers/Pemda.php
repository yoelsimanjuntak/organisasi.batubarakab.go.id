<?php
class Pemda extends MY_Controller {
  function __construct() {
      parent::__construct();
      if(!IsLogin()) {
        redirect(site_url());
      }
  }

  public function showErrUnathorized() {
    $act = $this->router->fetch_method();
    if($this->input->is_ajax_request() && strpos($act, "_form")===false) {
      ShowJsonError('MAAF, ANDA TIDAK MEMILIKI HAK AKSES.');
      exit();
    }else{
      echo 'MAAF, ANDA TIDAK MEMILIKI HAK AKSES.';
      exit();
    }
  }

  public function index() {
    $data['title'] = 'Kinerja Pemerintah Daerah';
    $opr = !empty($_GET['opr'])?$_GET['opr']:'';
    $id = !empty($_GET['id'])?$_GET['id']:'';

    switch ($opr) {
      case 'detail-periode':
      $data['subtitle'] = 'DETIL PERIODE';
      $data['navs'] = array(
        array('text'=>'DAFTAR PERIODE', 'link'=>site_url('sakipv2/pemda/index')),
        array('text'=>'DETIL PERIODE')
      );

      $rperiod = $this->db
      ->where(COL_PMDID, $id)
      ->get(TBL_SAKIPV2_PEMDA)
      ->row_array();
      if(empty($rperiod)) {
        show_error('PARAMETER TIDAK VALID!');
        exit();
      }

      $data['rperiod'] = $rperiod;
      $this->template->load('main', 'sakipv2/pemda/index-misi', $data);
      break;

      case 'detail-misi':
      $rmisi = $this->db
      ->where(COL_MISIID, $id)
      ->get(TBL_SAKIPV2_PEMDA_MISI)
      ->row_array();
      if(empty($rmisi)) {
        show_error('PARAMETER TIDAK VALID!');
        exit();
      }

      $rperiod = $this->db
      ->where(COL_PMDID, $rmisi[COL_IDPMD])
      ->get(TBL_SAKIPV2_PEMDA)
      ->row_array();
      if(empty($rperiod)) {
        show_error('PARAMETER TIDAK VALID!');
        exit();
      }

      $data['subtitle'] = 'DETIL MISI';
      $data['navs'] = array(
        array('text'=>'DAFTAR PERIODE', 'link'=>site_url('sakipv2/pemda/index')),
        array('text'=>$rperiod[COL_PMDTAHUNMULAI].' - '.$rperiod[COL_PMDTAHUNAKHIR], 'link'=>site_url('sakipv2/pemda/index').'?opr=detail-periode&id='.$rperiod[COL_PMDID]),
        array('text'=>'MISI '.$rmisi[COL_MISINO])
      );
      $data['rmisi'] = $rmisi;
      $data['rperiod'] = $rperiod;
      $this->template->load('main', 'sakipv2/pemda/index-tujuan', $data);
      break;

      case 'detail-tujuan':
      $rtujuan = $this->db
      ->select('sakipv2_pemda_tujuan.*, sakipv2_pemda_misi.MisiId, sakipv2_pemda_misi.MisiNo, sakipv2_pemda_misi.MisiUraian, sakipv2_pemda.PmdId, sakipv2_pemda.PmdTahunMulai, sakipv2_pemda.PmdTahunAkhir, sakipv2_pemda.PmdPejabat')
      ->join(TBL_SAKIPV2_PEMDA_MISI,TBL_SAKIPV2_PEMDA_MISI.'.'.COL_MISIID." = ".TBL_SAKIPV2_PEMDA_TUJUAN.".".COL_IDMISI,"left")
      ->join(TBL_SAKIPV2_PEMDA,TBL_SAKIPV2_PEMDA.'.'.COL_PMDID." = ".TBL_SAKIPV2_PEMDA_MISI.".".COL_IDPMD,"left")
      ->where(COL_TUJUANID, $id)
      ->get(TBL_SAKIPV2_PEMDA_TUJUAN)
      ->row_array();
      if(empty($rtujuan)) {
        show_error('PARAMETER TIDAK VALID!');
        exit();
      }

      $data['subtitle'] = 'DETIL TUJUAN';
      $data['navs'] = array(
        array('text'=>'DAFTAR PERIODE', 'link'=>site_url('sakipv2/pemda/index')),
        array('text'=>$rtujuan[COL_PMDTAHUNMULAI].' - '.$rtujuan[COL_PMDTAHUNAKHIR], 'link'=>site_url('sakipv2/pemda/index').'?opr=detail-periode&id='.$rtujuan[COL_PMDID]),
        array('text'=>'MISI '.$rtujuan[COL_MISINO], 'link'=>site_url('sakipv2/pemda/index').'?opr=detail-misi&id='.$rtujuan[COL_MISIID]),
        array('text'=>'TUJUAN '.$rtujuan[COL_TUJUANNO])
      );
      $data['rtujuan'] = $rtujuan;
      $this->template->load('main', 'sakipv2/pemda/view-tujuan', $data);
      break;

      case 'detail-sasaran':
      $rsasaran = $this->db
      ->select('sakipv2_pemda_sasaran.*,sakipv2_pemda_tujuan.TujuanId,sakipv2_pemda_tujuan.TujuanNo,sakipv2_pemda_tujuan.TujuanUraian,sakipv2_pemda_misi.MisiId,
                  sakipv2_pemda_misi.MisiNo,
                  sakipv2_pemda_misi.MisiUraian,
                  sakipv2_pemda.PmdId,
                  sakipv2_pemda.PmdTahunMulai,
                  sakipv2_pemda.PmdTahunAkhir,
                  sakipv2_pemda.PmdPejabat')
      ->join(TBL_SAKIPV2_PEMDA_TUJUAN,TBL_SAKIPV2_PEMDA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_PEMDA_SASARAN.".".COL_IDTUJUAN,"left")
      ->join(TBL_SAKIPV2_PEMDA_MISI,TBL_SAKIPV2_PEMDA_MISI.'.'.COL_MISIID." = ".TBL_SAKIPV2_PEMDA_TUJUAN.".".COL_IDMISI,"left")
      ->join(TBL_SAKIPV2_PEMDA,TBL_SAKIPV2_PEMDA.'.'.COL_PMDID." = ".TBL_SAKIPV2_PEMDA_MISI.".".COL_IDPMD,"left")
      ->where(COL_SASARANID, $id)
      ->get(TBL_SAKIPV2_PEMDA_SASARAN)
      ->row_array();
      if(empty($rsasaran)) {
        show_error('PARAMETER TIDAK VALID!');
        exit();
      }

      $data['subtitle'] = 'DETIL SASARAN';
      $data['navs'] = array(
        array('text'=>'DAFTAR PERIODE', 'link'=>site_url('sakipv2/pemda/index')),
        array('text'=>$rsasaran[COL_PMDTAHUNMULAI].' - '.$rsasaran[COL_PMDTAHUNAKHIR], 'link'=>site_url('sakipv2/pemda/index').'?opr=detail-periode&id='.$rsasaran[COL_PMDID]),
        array('text'=>'MISI '.$rsasaran[COL_MISINO], 'link'=>site_url('sakipv2/pemda/index').'?opr=detail-misi&id='.$rsasaran[COL_MISIID]),
        array('text'=>'TUJUAN '.$rsasaran[COL_TUJUANNO], 'link'=>site_url('sakipv2/pemda/index').'?opr=detail-tujuan&id='.$rsasaran[COL_TUJUANID]),
        array('text'=>'SASARAN '.$rsasaran[COL_SASARANNO])
      );
      $data['rsasaran'] = $rsasaran;
      $this->template->load('main', 'sakipv2/pemda/view-sasaran', $data);
      break;

      default:
        $data['subtitle'] = 'PERIODE PEMERINTAHAN';
        $this->template->load('main', 'sakipv2/pemda/index-periode', $data);
        break;
    }
  }

  public function ajax_form_periode($mode, $id=null) {
    $data['mode'] = $mode;
    $ruser = GetLoggedUser();

    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      $this->showErrUnathorized();
    }

    if(!empty($_POST)) {
      if($mode=='add') {
        $this->db->trans_begin();
        try {
          $rec = array(
            COL_PMDTAHUNMULAI=>$this->input->post(COL_PMDTAHUNMULAI),
            COL_PMDTAHUNAKHIR=>$this->input->post(COL_PMDTAHUNAKHIR),
            COL_PMDPEJABAT=>$this->input->post(COL_PMDPEJABAT),
            COL_PMDPEJABATWAKIL=>$this->input->post(COL_PMDPEJABATWAKIL),
            COL_PMDVISI=>$this->input->post(COL_PMDVISI),

            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );

          $res = $this->db->update(TBL_SAKIPV2_PEMDA, array(COL_PMDISAKTIF=>0));
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $res = $this->db->insert(TBL_SAKIPV2_PEMDA, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      } else if($mode=='edit') {
        $this->db->trans_begin();
        try {
          if(empty($id)) {
            throw new Exception('PARAMETER TIDAK VALID');
          }

          $rec = array(
            COL_PMDTAHUNMULAI=>$this->input->post(COL_PMDTAHUNMULAI),
            COL_PMDTAHUNAKHIR=>$this->input->post(COL_PMDTAHUNAKHIR),
            COL_PMDPEJABAT=>$this->input->post(COL_PMDPEJABAT),
            COL_PMDPEJABATWAKIL=>$this->input->post(COL_PMDPEJABATWAKIL),
            COL_PMDVISI=>$this->input->post(COL_PMDVISI),

            COL_UPDATEDBY=>$ruser[COL_USERNAME],
            COL_UPDATEDON=>date('Y-m-d H:i:s')
          );

          $res = $this->db->where(COL_PMDID, $id)->update(TBL_SAKIPV2_PEMDA, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      }
    } else {
      if($mode=='edit') {
        $rdata = $this->db->where(COL_PMDID, $id)->get(TBL_SAKIPV2_PEMDA)->row_array();
        if(empty($rdata)) {
          echo '<p class="text-center text-danger font-weight-bold">PARAMETER TIDAK VALID!</p>';
          exit();
        }
        $data['data'] = $rdata;
      }
      $this->load->view('sakipv2/pemda/form-periode', $data);
    }
  }

  public function ajax_change_periode($mode, $id)  {
    $ruser=GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      $this->showErrUnathorized();
    }

    if($mode=='delete') {
      $res = $this->db->where(COL_PMDID, $id)->delete(TBL_SAKIPV2_PEMDA);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      $rexist = $this->db
      ->order_by(COL_PMDISAKTIF,'asc')
      ->order_by(COL_PMDTAHUNMULAI,'desc')
      ->get(TBL_SAKIPV2_PEMDA)
      ->row_array();

      if(!empty($rexist) && $rexist[COL_PMDISAKTIF]==0) {
        $res = $this->db->where(COL_PMDID, $rexist[COL_PMDID])->update(TBL_SAKIPV2_PEMDA, array(COL_PMDISAKTIF=>1));
        if(!$res) {
          $this->db->trans_rollback();
          $err = $this->db->error();
          ShowJsonError($err['message']);
          exit();
        }
      }
    } else  if($mode=='activate') {
      $this->db->trans_begin();
      $res = $this->db->where(COL_PMDID.'!=', $id)->update(TBL_SAKIPV2_PEMDA, array(COL_PMDISAKTIF=>0));
      if(!$res) {
        $this->db->trans_rollback();
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }
      $res = $this->db->where(COL_PMDID, $id)->update(TBL_SAKIPV2_PEMDA, array(COL_PMDISAKTIF=>1));
      if(!$res) {
        $this->db->trans_rollback();
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }
      $this->db->trans_commit();
    } else {
      ShowJsonSuccess('PARAMETER TIDAK VALID');
      exit();
    }

    ShowJsonSuccess('BERHASIL MENGUBAH DATA');
    exit();
  }

  public function ajax_form_misi($mode, $idPmd, $id=null) {
    $data['mode'] = $mode;
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      $this->showErrUnathorized();
    }

    if(!empty($_POST)) {
      if($mode=='add') {
        $this->db->trans_begin();
        try {
          $rec = array(
            COL_IDPMD=>$idPmd,
            COL_MISINO=>$this->input->post(COL_MISINO),
            COL_MISIURAIAN=>$this->input->post(COL_MISIURAIAN),

            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );

          $res = $this->db->insert(TBL_SAKIPV2_PEMDA_MISI, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      } else if($mode=='edit') {
        $this->db->trans_begin();
        try {
          if(empty($id)) {
            throw new Exception('PARAMETER TIDAK VALID');
          }

          $rec = array(
            COL_MISINO=>$this->input->post(COL_MISINO),
            COL_MISIURAIAN=>$this->input->post(COL_MISIURAIAN),

            COL_UPDATEDBY=>$ruser[COL_USERNAME],
            COL_UPDATEDON=>date('Y-m-d H:i:s')
          );

          $res = $this->db->where(COL_MISIID, $id)->update(TBL_SAKIPV2_PEMDA_MISI, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      }
    } else {
      if($mode=='edit') {
        $rdata = $this->db->where(COL_MISIID, $id)->get(TBL_SAKIPV2_PEMDA_MISI)->row_array();
        if(empty($rdata)) {
          echo '<p class="text-center text-danger font-weight-bold">PARAMETER TIDAK VALID!</p>';
          exit();
        }
        $data['data'] = $rdata;
      }
      $this->load->view('sakipv2/pemda/form-misi', $data);
    }
  }

  public function ajax_change_misi($mode, $id)  {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      $this->showErrUnathorized();
    }

    if($mode=='delete') {
      $res = $this->db->where(COL_MISIID, $id)->delete(TBL_SAKIPV2_PEMDA_MISI);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }
    }

    ShowJsonSuccess('BERHASIL MENGUBAH DATA');
    exit();
  }

  public function ajax_form_tujuan($mode, $idMisi, $id=null) {
    $data['mode'] = $mode;
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      $this->showErrUnathorized();
    }

    if(!empty($_POST)) {
      $ruser = GetLoggedUser();
      if($mode=='add') {
        $this->db->trans_begin();
        try {
          $rec = array(
            COL_IDMISI=>$idMisi,
            COL_TUJUANNO=>$this->input->post(COL_TUJUANNO),
            COL_TUJUANURAIAN=>$this->input->post(COL_TUJUANURAIAN),

            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );

          $res = $this->db->insert(TBL_SAKIPV2_PEMDA_TUJUAN, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      } else if($mode=='edit') {
        $this->db->trans_begin();
        try {
          if(empty($id)) {
            throw new Exception('PARAMETER TIDAK VALID');
          }

          $rec = array(
            COL_TUJUANNO=>$this->input->post(COL_TUJUANNO),
            COL_TUJUANURAIAN=>$this->input->post(COL_TUJUANURAIAN),

            COL_UPDATEDBY=>$ruser[COL_USERNAME],
            COL_UPDATEDON=>date('Y-m-d H:i:s')
          );

          $res = $this->db->where(COL_TUJUANID, $id)->update(TBL_SAKIPV2_PEMDA_TUJUAN, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      }
    } else {
      if($mode=='edit') {
        $rdata = $this->db->where(COL_TUJUANID, $id)->get(TBL_SAKIPV2_PEMDA_TUJUAN)->row_array();
        if(empty($rdata)) {
          echo '<p class="text-center text-danger font-weight-bold">PARAMETER TIDAK VALID!</p>';
          exit();
        }
        $data['data'] = $rdata;
      }
      $this->load->view('sakipv2/pemda/form-tujuan', $data);
    }
  }

  public function ajax_change_tujuan($mode, $id)  {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      $this->showErrUnathorized();
    }

    if($mode=='delete') {
      $res = $this->db->where(COL_TUJUANID, $id)->delete(TBL_SAKIPV2_PEMDA_TUJUAN);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }
    }

    ShowJsonSuccess('BERHASIL MENGUBAH DATA');
    exit();
  }

  public function ajax_form_indikatortujuan($mode, $id)  {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      $this->showErrUnathorized();
    }

    $uraianIndikator = $this->input->post(COL_TUJINDIKATORURAIAN);

    if($mode == 'add') {
      if(empty($uraianIndikator)) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $rec = array(
        COL_IDTUJUAN=>$id,
        COL_TUJINDIKATORURAIAN=> $uraianIndikator,
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $res = $this->db->insert(TBL_SAKIPV2_PEMDA_TUJUANDET, $rec);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('INPUT DATA BERHASIL');
      exit();
    } else if($mode == 'edit') {
      if(empty($uraianIndikator)) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $rec = array(
        COL_TUJINDIKATORURAIAN=> $uraianIndikator,
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $res = $this->db->where(COL_TUJINDIKATORID, $id)->update(TBL_SAKIPV2_PEMDA_TUJUANDET, $rec);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('INPUT DATA BERHASIL');
      exit();
    } else if($mode == 'delete') {
      $res = $this->db->where(COL_TUJINDIKATORID, $id)->delete(TBL_SAKIPV2_PEMDA_TUJUANDET);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('HAPUS DATA BERHASIL');
      exit();
    }
  }

  public function ajax_form_sasaran($mode, $idTujuan, $id=null) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      $this->showErrUnathorized();
    }

    $data['mode'] = $mode;
    if(!empty($_POST)) {
      if($mode=='add') {
        $this->db->trans_begin();
        try {
          $rec = array(
            COL_IDTUJUAN=>$idTujuan,
            COL_SASARANNO=>$this->input->post(COL_SASARANNO),
            COL_SASARANURAIAN=>$this->input->post(COL_SASARANURAIAN),

            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );
          $arrUraian = $this->input->post('SsrIndikatorUraian');
          $arrSumberData = $this->input->post('SsrIndikatorSumberData');
          $arrFormulasi = $this->input->post('SsrIndikatorFormulasi');
          $arrSatuan = $this->input->post('SsrIndikatorSatuan');
          $arrTarget = $this->input->post('SsrIndikatorTarget');
          $arrIndikator = [];

          $res = $this->db->insert(TBL_SAKIPV2_PEMDA_SASARAN, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $idSasaran = $this->db->insert_id();
          for($i = 0; $i<count($arrUraian); $i++) {
            $arrIndikator[] = array(
              COL_IDSASARAN=>$idSasaran,
              'SsrIndikatorUraian'=>$arrUraian[$i],
              'SsrIndikatorSumberData'=>$arrSumberData[$i],
              'SsrIndikatorFormulasi'=>$arrFormulasi[$i],
              'SsrIndikatorSatuan'=>$arrSatuan[$i],
              'SsrIndikatorTarget'=>$arrTarget[$i],

              COL_CREATEDBY=>$ruser[COL_USERNAME],
              COL_CREATEDON=>date('Y-m-d H:i:s')
            );
          }

          if(!empty($arrIndikator)) {
            $res = $this->db->insert_batch(TBL_SAKIPV2_PEMDA_SASARANDET, $arrIndikator);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception($err['message']);
            }
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      } else if($mode=='edit') {
        $this->db->trans_begin();
        try {
          if(empty($id)) {
            throw new Exception('PARAMETER TIDAK VALID');
          }

          $rec = array(
            COL_SASARANNO=>$this->input->post(COL_SASARANNO),
            COL_SASARANURAIAN=>$this->input->post(COL_SASARANURAIAN),

            COL_UPDATEDBY=>$ruser[COL_USERNAME],
            COL_UPDATEDON=>date('Y-m-d H:i:s')
          );

          $arrUraian = $this->input->post('SsrIndikatorUraian');
          $arrSumberData = $this->input->post('SsrIndikatorSumberData');
          $arrFormulasi = $this->input->post('SsrIndikatorFormulasi');
          $arrSatuan = $this->input->post('SsrIndikatorSatuan');
          $arrTarget = $this->input->post('SsrIndikatorTarget');
          $arrIndikator = [];

          for($i = 0; $i<count($arrUraian); $i++) {
            $arrIndikator[] = array(
              COL_IDSASARAN=>$id,
              'SsrIndikatorUraian'=>$arrUraian[$i],
              'SsrIndikatorSumberData'=>$arrSumberData[$i],
              'SsrIndikatorFormulasi'=>$arrFormulasi[$i],
              'SsrIndikatorSatuan'=>$arrSatuan[$i],
              'SsrIndikatorTarget'=>$arrTarget[$i],

              COL_CREATEDBY=>$ruser[COL_USERNAME],
              COL_CREATEDON=>date('Y-m-d H:i:s')
            );
          }

          if(!empty($arrIndikator)) {
            $res = $this->db->where(COL_IDSASARAN, $id)->delete(TBL_SAKIPV2_PEMDA_SASARANDET);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception($err['message']);
            }

            $res = $this->db->insert_batch(TBL_SAKIPV2_PEMDA_SASARANDET, $arrIndikator);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception($err['message']);
            }
          }

          $res = $this->db->where(COL_SASARANID, $id)->update(TBL_SAKIPV2_PEMDA_SASARAN, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      }
    } else {
      if($mode=='edit') {
        $rdata = $this->db->where(COL_SASARANID, $id)->get(TBL_SAKIPV2_PEMDA_SASARAN)->row_array();
        if(empty($rdata)) {
          echo '<p class="text-center text-danger font-weight-bold">PARAMETER TIDAK VALID!</p>';
          exit();
        }
        $rIndikatorSasaran = $this->db->where(COL_IDSASARAN, $id)->get(TBL_SAKIPV2_PEMDA_SASARANDET)->result_array();
        $data['data'] = $rdata;
        $data['rIndikatorSasaran'] = $rIndikatorSasaran;
      }
      $this->load->view('sakipv2/pemda/form-sasaran', $data);
    }
  }

  public function ajax_change_sasaran($mode, $id)  {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      $this->showErrUnathorized();
    }

    if($mode=='delete') {
      $res = $this->db->where(COL_SASARANID, $id)->delete(TBL_SAKIPV2_PEMDA_SASARAN);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }
    }

    ShowJsonSuccess('BERHASIL MENGUBAH DATA');
    exit();
  }

  public function ajax_form_indikatorsasaran($mode, $id)  {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      $this->showErrUnathorized();
    }

    $uraianIndikator = $this->input->post(COL_SSRINDIKATORURAIAN);

    if($mode == 'add') {
      if(empty($uraianIndikator)) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $rec = array(
        COL_IDSASARAN=>$id,
        COL_SSRINDIKATORURAIAN=> $uraianIndikator,
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $res = $this->db->insert(TBL_SAKIPV2_PEMDA_SASARANDET, $rec);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('INPUT DATA BERHASIL');
      exit();
    } else if($mode == 'edit') {
      if(empty($uraianIndikator)) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $rec = array(
        COL_SSRINDIKATORURAIAN=> $uraianIndikator,
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $res = $this->db->where(COL_SSRINDIKATORID, $id)->update(TBL_SAKIPV2_PEMDA_SASARANDET, $rec);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('INPUT DATA BERHASIL');
      exit();
    } else if($mode == 'delete') {
      $res = $this->db->where(COL_SSRINDIKATORID, $id)->delete(TBL_SAKIPV2_PEMDA_SASARANDET);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('HAPUS DATA BERHASIL');
      exit();
    }
  }

  public function ajax_form_iku($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      $this->showErrUnathorized();
    }

    if(!empty($_POST)) {
      $ruser = GetLoggedUser();
      if(empty($id)) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $this->db->trans_begin();
      try {
        $arrUraian = $this->input->post('IKUUraian');
        $arrFormulasi = $this->input->post('IKUFormulasi');
        $arrSatuan = $this->input->post('IKUSatuan');
        $arrTarget = $this->input->post('IKUTarget');
        $arrIKU = [];
        for($i = 0; $i<count($arrUraian); $i++) {
          $arrIKU[] = array(
            'IKUUraian'=>$arrUraian[$i],
            'IKUFormulasi'=>$arrFormulasi[$i],
            'IKUSatuan'=>$arrSatuan[$i],
            'IKUTarget'=>$arrTarget[$i]
          );
        }

        if(!empty($arrIKU)) {
          $res = $this->db
          ->where(COL_MISIID, $id)
          ->update(TBL_SAKIPV2_PEMDA_MISI, array(
            COL_MISIIKU=>json_encode($arrIKU),
            COL_UPDATEDBY=>$ruser[COL_USERNAME],
            COL_UPDATEDON=>date('Y-m-d H:i:s')
          ));
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }
        } else {
          $res = $this->db
          ->where(COL_MISIID, $id)
          ->update(TBL_SAKIPV2_PEMDA_MISI, array(
            COL_MISIIKU=>NULL,
            COL_UPDATEDBY=>$ruser[COL_USERNAME],
            COL_UPDATEDON=>date('Y-m-d H:i:s')
          ));
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess('INPUT DATA BERHASIL');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }
    }
  }

  public function monev() {
    $data['title'] = 'Target & Capaian Pemerintah Daerah';
    $this->template->load('main', 'sakipv2/pemda/monev', $data);
  }

  public function monev_data($idPmd, $modeCetak='') {
    $data = array(
      'idPmd'=>$idPmd,
      'modCetak'=>$modeCetak
    );
    $data['title'] = 'TARGET & CAPAIAN PEMERINTAH DAERAH';

    if($modeCetak=='RENCANA' || $modeCetak=='REALISASI') {
      if($modeCetak=='RENCANA') $data['title'] = 'RENCANA AKSI';
      else if($modeCetak=='REALISASI') $data['title'] = 'REALISASI KINERJA';

      $this->load->library('Mypdf');
      $mpdf = new Mypdf('utf-8', 'A4-L', 0,'',15,15,30,16);

      $html = $this->load->view('sakipv2/pemda/monev-partial', $data, TRUE);
      $htmlLogo = MY_IMAGEURL.'logo.png';
      $htmlYear = date('Y');
      $htmlTitle = $data['title'].' PEMERINTAH DAERAH';
      $htmlHeader = @"
      <table style=\"border: none !important\">
        <tr>
          <td rowspan=\"2\" style=\"border: none !important; padding: 0 !important; width: 30px; white-space: nowrap; vertical-align: top\"><img src=\"$htmlLogo\" style=\"width: 25px\" /></td>
          <td style=\"border: none !important; padding: 0 !important; font-size: 10pt; font-weight: bold\">$htmlTitle</td>
        </tr>
        <tr>
          <td style=\"border: none !important; padding: 0 !important; font-size: 10pt; font-weight: bold\">KABUPATEN BATU BARA TAHUN $htmlYear</td>
        </tr>
      </table>
      <hr />
      ";
      //echo $html;
      //return;
      $mpdf->pdf->SetTitle($data['title']);
      $mpdf->pdf->SetHTMLHeader($htmlHeader);
      $mpdf->pdf->SetFooter('Dicetak melalui aplikasi '.$this->setting_web_name.' pada '.date('d-m-Y H:i'));
      $mpdf->pdf->WriteHTML($html);
      $mpdf->pdf->Output('SITALAKBAJAKUN - '.$data['title'].' '.date('YmdHis').'.pdf', 'I');
    } else {
      $this->load->view('sakipv2/pemda/monev-partial', $data);
    }
  }

  public function monev_ajax_change_sasaran($mode, $dpaTahun, $idSasaranIndikator) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      $this->showErrUnathorized();
    }

    $val = $this->input->post('value');
    if($mode==COL_MONEVTARGET ||
        $mode==COL_MONEVTARGETTW1 ||
        $mode==COL_MONEVTARGETTW2 ||
        $mode==COL_MONEVTARGETTW3 ||
        $mode==COL_MONEVTARGETTW4 ||
        $mode==COL_MONEVREALISASI ||
        $mode==COL_MONEVREALISASITW1 ||
        $mode==COL_MONEVREALISASITW2 ||
        $mode==COL_MONEVREALISASITW3 ||
        $mode==COL_MONEVREALISASITW4
      ) {
      $rval = $this->db
      ->where(COL_IDSASARANINDIKATOR, $idSasaranIndikator)
      ->where(COL_MONEVTAHUN, $dpaTahun)
      ->order_by(COL_UNIQ, 'desc')
      ->get(TBL_SAKIPV2_PEMDA_SASARANMONEV)
      ->row_array();
      if(!empty($rval)) {
        $res = $this->db
        ->where(COL_UNIQ, $rval[COL_UNIQ])
        ->update(TBL_SAKIPV2_PEMDA_SASARANMONEV, array($mode=>$val, COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s')));
        if(!$res) {
          $err = $this->db->error();
          ShowJsonError($err['message']);
          exit();
        }
      } else {
        $dat = array(
          COL_IDSASARANINDIKATOR=>$idSasaranIndikator,
          COL_MONEVTAHUN=>$dpaTahun,
          $mode=>$val,

          COL_CREATEDBY=>$ruser[COL_USERNAME],
          COL_CREATEDON=>date('Y-m-d H:i:s')
        );
        $res = $this->db
        ->insert(TBL_SAKIPV2_PEMDA_SASARANMONEV, $dat);
        if(!$res) {
          $err = $this->db->error();
          ShowJsonError($err['message']);
          exit();
        }
      }

      ShowJsonSuccess($this->db->last_query());
      exit();
    }
  }
}
