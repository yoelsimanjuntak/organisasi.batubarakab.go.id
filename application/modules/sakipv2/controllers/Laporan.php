<?php
class Laporan extends MY_Controller {
  function __construct() {
      parent::__construct();
      if(!IsLogin()) {
        redirect(site_url());
      }
  }

  public function index($page) {
    $ruser = GetLoggedUser();
    $data['page'] = $page;
    switch ($page) {
      case 'pemda-cascading':
        $data['title'] = 'Cascading Pemerintah Daerah';
        $this->template->load('main', 'sakipv2/laporan/index-pemda', $data);
      break;

      case 'pemda-iku':
        $data['title'] = 'Indikator Kinerja Utama (IKU) Pemerintah Daerah';
        $this->template->load('main', 'sakipv2/laporan/index-pemda', $data);
      break;

      case 'pemda-iku-cetak':
        $rOptPmd = $this->db
        ->order_by(COL_PMDISAKTIF,'desc')
        ->order_by(COL_PMDTAHUNMULAI,'desc')
        ->get(TBL_SAKIPV2_PEMDA)
        ->result_array();

        $getPmd = null;
        if(!empty($_GET['idPmd'])) $getPmd = $_GET['idPmd'];
        else if(!empty($rOptPmd)) $getPmd = $rOptPmd[0][COL_PMDID];

        if(empty($getPmd)) {
          show_error('PARAMETER TIDAK VALID');
          exit();
        }

        $rpemda = $this->db
        ->where(COL_PMDID, $getPmd)
        ->get(TBL_SAKIPV2_PEMDA)
        ->row_array();

        $data['idPmd'] = $getPmd;
        $data['isCetak'] = 1;

        $this->load->library('Mypdf');
        $mpdf = new Mypdf('utf-8', 'A4-L', 0,'',15,15,30,16);

        $html = $this->load->view('sakipv2/laporan/pemda-iku', $data, TRUE);
        $htmlLogo = MY_IMAGEURL.'logo.png';
        $htmlTitle = 'INDIKATOR KINERJA';
        $htmlHeader = @"
        <table style=\"border: none !important\">
          <tr>
            <td rowspan=\"2\" style=\"border: none !important; padding: 0 !important; width: 30px; white-space: nowrap; vertical-align: top\"><img src=\"$htmlLogo\" style=\"width: 25px\" /></td>
            <td style=\"border: none !important; padding: 0 !important; font-size: 10pt; font-weight: bold\">$htmlTitle</td>
          </tr>
          <tr>
            <td style=\"border: none !important; padding: 0 !important; font-size: 10pt; font-weight: bold\">PEMERINTAH KABUPATEN BATU BARA</td>
          </tr>
        </table>
        <hr />
        ";
        //echo $html;
        //return;
        $mpdf->pdf->SetTitle('Indikator Kinerja Utama (IKU) Pemerintah Daerah');
        $mpdf->pdf->SetHTMLHeader($htmlHeader);
        $mpdf->pdf->SetFooter('Dicetak melalui aplikasi '.$this->setting_web_name.' pada '.date('d-m-Y H:i'));
        $mpdf->pdf->WriteHTML($html);
        $mpdf->pdf->Output('SITALAKBAJAKUN - IKU PEMERINTAH DAERAH PERIODE '.$rpemda[COL_PMDTAHUNMULAI].' s.d '.$rpemda[COL_PMDTAHUNAKHIR].' ('.strtoupper($rpemda[COL_PMDPEJABAT]).')'.'.pdf', 'I');
      break;

      case 'pemda-pk':
        $data['title'] = 'Perjanjian Kinerja Pemerintah Daerah';
        $this->template->load('main', 'sakipv2/laporan/index-pemda', $data);
      break;

      case 'pemda-pk-cetak':
        $rOptPmd = $this->db
        ->order_by(COL_PMDISAKTIF,'desc')
        ->order_by(COL_PMDTAHUNMULAI,'desc')
        ->get(TBL_SAKIPV2_PEMDA)
        ->result_array();

        $getPmd = null;
        if(!empty($_GET['idPmd'])) $getPmd = $_GET['idPmd'];
        else if(!empty($rOptPmd)) $getPmd = $rOptPmd[0][COL_PMDID];

        if(empty($getPmd)) {
          show_error('PARAMETER TIDAK VALID');
          exit();
        }

        $rpemda = $this->db
        ->where(COL_PMDID, $getPmd)
        ->get(TBL_SAKIPV2_PEMDA)
        ->row_array();

        $data['idPmd'] = $getPmd;
        $data['isCetak'] = 1;
        $data['rpemda'] = $rpemda;

        $this->load->library('Mypdf');
        $mpdf = new Mypdf('','A4',0,'',15,15,30,16);

        $html = $this->load->view('sakipv2/laporan/pemda-pk', $data, TRUE);
        $htmlLogo = MY_IMAGEURL.'logo.png';
        $htmlYear = date('Y');
        $htmlHeader = @"
        <table>
          <tr>
            <td rowspan=\"2\" style=\"padding: 0 !important; width: 30px; white-space: nowrap; vertical-align: top\"><img src=\"$htmlLogo\" style=\"width: 25px\" /></td>
            <td style=\"padding: 0 !important; font-size: 10pt; font-weight: bold\">PERJANJIAN KINERJA PEMERINTAH DAERAH</td>
          </tr>
          <tr>
            <td style=\"padding: 0 !important; font-size: 10pt; font-weight: bold\">KABUPATEN BATU BARA TAHUN $htmlYear</td>
          </tr>
        </table>
        <hr />
        ";
        //echo $html;
        //return;
        //$mpdf->pdf->SetHeader(' PERJANJIAN KINERJA PEMERINTAH DAERAH<br />KABUPATEN BATU BARA TAHUN '.date('Y'));
        $mpdf->pdf->use_kwt = true;
        $mpdf->pdf->SetHTMLHeader($htmlHeader);
        $mpdf->pdf->SetFooter('Dicetak melalui aplikasi '.$this->setting_web_name);
        $mpdf->pdf->WriteHTML($html);
        $mpdf->pdf->SetTitle('Perjanjian Kinerja Pemerintah Daerah');
        $mpdf->pdf->Output('SITALAKBAJAKUN - IKU PEMERINTAH DAERAH PERIODE '.$rpemda[COL_PMDTAHUNMULAI].' s.d '.$rpemda[COL_PMDTAHUNAKHIR].' ('.strtoupper($rpemda[COL_PMDPEJABAT]).')'.'.pdf', 'I');
      break;

      case 'skpd-cascading':
        $data['title'] = 'Cascading Kinerja SKPD';
        $data['isFilterDPA'] = 1;
        $this->template->load('main', 'sakipv2/laporan/index-skpd', $data);
      break;

      case 'skpd-iku':
        $data['title'] = 'Indikator Kinerja Utama (IKU) SKPD';
        $this->template->load('main', 'sakipv2/laporan/index-skpd', $data);
      break;

      case 'skpd-iku-cetak':
        $rpemda = $this->db
        ->where(COL_PMDISAKTIF, 1)
        ->get(TBL_SAKIPV2_PEMDA)
        ->row_array();

        $rOptSkpd = $this->db
        ->where(COL_SKPDISAKTIF, 1)
        ->order_by(COL_SKPDURUSAN, 'asc')
        ->order_by(COL_SKPDBIDANG, 'asc')
        ->order_by(COL_SKPDUNIT, 'asc')
        ->order_by(COL_SKPDSUBUNIT, 'asc')
        ->get(TBL_SAKIPV2_SKPD)
        ->result_array();

        $getSkpd = '';
        $getRenstra = '';
        $getDPA = '';

        if(!empty($_GET['idSKPD'])) $getSkpd = $_GET['idSKPD'];
        else if(!empty($rOptSkpd)) $getSkpd = $rOptSkpd[0][COL_SKPDID];

        if($ruser[COL_ROLEID]!=ROLEADMIN) {
          $getSkpd=$ruser[COL_SKPDID];
        }

        $rOptRenstra = array();
        if(!empty($getSkpd)) {
          $rOptRenstra = $this->db
          ->where(COL_IDPEMDA, $rpemda[COL_PMDID])
          ->where(COL_IDSKPD, $getSkpd)
          //->where(COL_RENSTRAISAKTIF, 1)
          ->order_by(COL_RENSTRAISAKTIF, 'desc')
          ->order_by(COL_RENSTRATAHUN, 'desc')
          ->order_by(COL_RENSTRAID, 'desc')
          ->get(TBL_SAKIPV2_SKPD_RENSTRA)
          ->result_array();
        }

        if(!empty($_GET['idRenstra'])) $getRenstra = $_GET['idRenstra'];
        else if(!empty($rOptRenstra)) $getRenstra = $rOptRenstra[0][COL_RENSTRAID];

        $data['idSKPD'] = $getSkpd;
        $data['idRenstra'] = $getRenstra;
        $data['isCetak'] = 1;

        $rskpd = $this->db
        ->where(COL_SKPDID, $getSkpd)
        ->get(TBL_SAKIPV2_SKPD)
        ->row_array();

        $this->load->library('Mypdf');
        $mpdf = new Mypdf('utf-8', 'A4-L', 0,'',15,15,30,16);

        $html = $this->load->view('sakipv2/laporan/skpd-iku', $data, TRUE);
        $htmlLogo = MY_IMAGEURL.'logo.png';
        $htmlTitle = 'INDIKATOR KINERJA'.(!empty($rskpd)?' '.strtoupper($rskpd[COL_SKPDNAMA]):'');
        $htmlHeader = @"
        <table style=\"border: none !important\">
          <tr>
            <td rowspan=\"2\" style=\"border: none !important; padding: 0 !important; width: 30px; white-space: nowrap; vertical-align: top\"><img src=\"$htmlLogo\" style=\"width: 25px\" /></td>
            <td style=\"border: none !important; padding: 0 !important; font-size: 10pt; font-weight: bold\">$htmlTitle</td>
          </tr>
          <tr>
            <td style=\"border: none !important; padding: 0 !important; font-size: 10pt; font-weight: bold\">PEMERINTAH KABUPATEN BATU BARA</td>
          </tr>
        </table>
        <hr />
        ";
        $mpdf->pdf->SetTitle('Indikator Kinerja Utama (IKU) '.(!empty($rskpd)?$rskpd[COL_SKPDNAMA]:''));
        $mpdf->pdf->SetHTMLHeader($htmlHeader);
        $mpdf->pdf->SetFooter('Dicetak melalui aplikasi '.$this->setting_web_name);
        //echo $html;
        //return;
        $mpdf->pdf->WriteHTML($html);
        $mpdf->pdf->use_kwt = true;
        $mpdf->pdf->Output('SITALAKBAJAKUN - IKU PERANGKAT DAERAH.pdf', 'I');
      break;

      case 'skpd-pk':
        $data['title'] = 'Perjanjian Kinerja';
        $data['isFilterDPA'] = 1;
        $this->template->load('main', 'sakipv2/laporan/index-skpd', $data);
      break;

      case 'skpd-pk-cetak':
        $rpemda = $this->db
        ->where(COL_PMDISAKTIF, 1)
        ->get(TBL_SAKIPV2_PEMDA)
        ->row_array();

        $rOptSkpd = $this->db
        ->where(COL_SKPDISAKTIF, 1)
        ->order_by(COL_SKPDURUSAN, 'asc')
        ->order_by(COL_SKPDBIDANG, 'asc')
        ->order_by(COL_SKPDUNIT, 'asc')
        ->order_by(COL_SKPDSUBUNIT, 'asc')
        ->get(TBL_SAKIPV2_SKPD)
        ->result_array();

        $getSkpd = '';
        $getRenstra = '';
        $getDPA = '';

        if(!empty($_GET['idSKPD'])) $getSkpd = $_GET['idSKPD'];
        else if(!empty($rOptSkpd)) $getSkpd = $rOptSkpd[0][COL_SKPDID];

        if($ruser[COL_ROLEID]!=ROLEADMIN) {
          $getSkpd=$ruser[COL_SKPDID];
        }

        $rOptRenstra = array();
        if(!empty($getSkpd)) {
          $rOptRenstra = $this->db
          ->where(COL_IDPEMDA, $rpemda[COL_PMDID])
          ->where(COL_IDSKPD, $getSkpd)
          //->where(COL_RENSTRAISAKTIF, 1)
          ->order_by(COL_RENSTRAISAKTIF, 'desc')
          ->order_by(COL_RENSTRATAHUN, 'desc')
          ->order_by(COL_RENSTRAID, 'desc')
          ->get(TBL_SAKIPV2_SKPD_RENSTRA)
          ->result_array();
        }

        if(!empty($_GET['idRenstra'])) $getRenstra = $_GET['idRenstra'];
        else if(!empty($rOptRenstra)) $getRenstra = $rOptRenstra[0][COL_RENSTRAID];

        $rOptDpa = array();
        if(!empty($getRenstra)) {
          $rOptDpa = $this->db
          ->where(COL_IDRENSTRA, $getRenstra)
          ->where(COL_DPAISAKTIF, 1)
          ->order_by(COL_DPAISAKTIF, 'desc')
          ->order_by(COL_DPATAHUN,'desc')
          ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
          ->result_array();
        }

        $getDPA = null;
        if(!empty($_GET['idDPA'])) {
          $getDPA = $_GET['idDPA'];
        } else if(!empty($rOptDpa) && $rOptDpa[0][COL_DPAISAKTIF]==1) {
          $getDPA = $rOptDpa[0][COL_DPAID];
        }

        $data['idPmd'] = $rpemda[COL_PMDID];
        $data['idSKPD'] = $getSkpd;
        $data['idRenstra'] = $getRenstra;
        $data['idDPA'] = $getDPA;
        $data['isCetak'] = 1;
        $data['isDPAPerubahan'] = isset($_GET['isDPAPerubahan'])&&$_GET['isDPAPerubahan']?1:0;
        $rskpd = $this->db
        ->where(COL_SKPDID, $getSkpd)
        ->get(TBL_SAKIPV2_SKPD)
        ->row_array();

        $this->load->library('Mypdf');
        $mpdf = new Mypdf();

        $html = $this->load->view('sakipv2/laporan/skpd-pk', $data, TRUE);
        //echo $html;
        //return;
        $mpdf->pdf->WriteHTML($html);
        $mpdf->pdf->use_kwt = true;
        $mpdf->pdf->SetTitle('Perjanjian Kinerja '.(!empty($rskpd)?$rskpd[COL_SKPDNAMA]:''));
        $mpdf->pdf->Output('SITALAKBAJAKUN - PERJANJIAN KINERJA '.(!empty($rskpd)?$rskpd[COL_SKPDNAMA]:'').'.pdf', 'I');
      break;
    }

  }

  public function dpa() {
    $data['title'] = 'Laporan - DPA';

    if(!empty($_GET['idSasaranPmd'])) {
      $data['rpemda'] = $this->db
      ->select('sakipv2_pemda.*')
      ->join(TBL_SAKIPV2_PEMDA_TUJUAN,TBL_SAKIPV2_PEMDA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_PEMDA_SASARAN.".".COL_IDTUJUAN,"inner")
      ->join(TBL_SAKIPV2_PEMDA_MISI,TBL_SAKIPV2_PEMDA_MISI.'.'.COL_MISIID." = ".TBL_SAKIPV2_PEMDA_TUJUAN.".".COL_IDMISI,"inner")
      ->join(TBL_SAKIPV2_PEMDA,TBL_SAKIPV2_PEMDA.'.'.COL_PMDID." = ".TBL_SAKIPV2_PEMDA_MISI.".".COL_IDPMD,"inner")
      ->where(COL_SASARANID, $_GET['idSasaranPmd'])
      ->get(TBL_SAKIPV2_PEMDA_SASARAN)
      ->row_array();
    } else {
      $data['rpemda'] = $this->db
      ->where(COL_PMDISAKTIF, 1)
      ->get(TBL_SAKIPV2_PEMDA)
      ->row_array();
    }
    $this->template->load('main', 'sakipv2/laporan/dpa', $data);
  }
}
?>
