<?php
class User extends MY_Controller {
  function __construct() {
      parent::__construct();
      //$this->load->library('encrypt');
      $this->load->helper('captcha');
      $this->load->library('user_agent');
      $this->load->model('muser');
  }

  public function login() {
    if(IsLogin()) {
      redirect('sakipv2/user/dashboard');
    }
    if(!empty($_POST)) {
      $this->form_validation->set_rules(array(
        array(
          'field' => 'UserName',
          'label' => 'UserName',
          'rules' => 'required'
        ),
        array(
          'field' => 'Password',
          'label' => 'Password',
          'rules' => 'required'
        )
      ));
      if($this->form_validation->run()) {
        $this->load->model('muser');
        $username = $this->input->post(COL_USERNAME);
        $password = $this->input->post(COL_PASSWORD);

        if($this->muser->authenticate($username, $password)) {
          if($this->muser->IsSuspend($username)) {
            ShowJsonError('MAAF, AKUN INI DI SUSPEND. SILAKAN HUBUNGI ADMINISTRATOR.');
            return;
          }

          $userdetails = $this->muser->getdetails($username);
          if($userdetails[COL_ROLEID] != ROLEADMIN && $userdetails[COL_ROLEID] != ROLEGUEST) {
            $arrOPD = explode('.', $userdetails[COL_COMPANYID]);
            $rskpd = $this->db
            ->where(array(COL_SKPDURUSAN=>$arrOPD[0], COL_SKPDBIDANG=>$arrOPD[1], COL_SKPDUNIT=>$arrOPD[2], COL_SKPDSUBUNIT=>$arrOPD[3]))
            ->get(TBL_SAKIPV2_SKPD)
            ->row_array();
            if(empty($rskpd)) {
              ShowJsonError('LOGIN GAGAL. UNIT KERJA TIDAK DITEMUKAN.');
              exit();
            }

            $userdetails = array_merge($userdetails, array(COL_SKPDID=>$rskpd[COL_SKPDID]));
          }

          $this->db->where(COL_USERNAME, $username);
          $this->db->update(TBL__USERS, array(COL_LASTLOGIN=>date('Y-m-d H:i:s'), COL_LASTLOGINIP=>$this->input->ip_address()));

          SetLoginSession($userdetails);
          ShowJsonSuccess('LOGIN BERHASIL', array('redirect'=>site_url('sakipv2/user/dashboard')));
        } else {
          ShowJsonError('USERNAME / PASSWORD TIDAK VALID.');
        }
      } else {
        ShowJsonError('USERNAME / PASSWORD TIDAK VALID.');
      }
    } else if(!empty($_GET)) {
      $this->load->model('muser');
      $username = $this->input->get('uname');
      $password = $this->input->get('passwd');
      $this->db->where("(".TBL__USERS.".".COL_USERNAME." = '".$username."') AND ".TBL__USERS.".".COL_PASSWORD." = '".$password."'");

      $res = $this->db->get(TBL__USERS)->row_array();
      if($res){
        $userdetails = $this->muser->getdetails($username);
        if($userdetails[COL_ROLEID] != ROLEADMIN && $userdetails[COL_ROLEID] != ROLEGUEST) {
          $arrOPD = explode('.', $userdetails[COL_COMPANYID]);
          $rskpd = $this->db
          ->where(array(COL_SKPDURUSAN=>$arrOPD[0], COL_SKPDBIDANG=>$arrOPD[1], COL_SKPDUNIT=>$arrOPD[2], COL_SKPDSUBUNIT=>$arrOPD[3]))
          ->get(TBL_SAKIPV2_SKPD)
          ->row_array();
          if(empty($rskpd)) {
            ShowJsonError('LOGIN GAGAL. UNIT KERJA TIDAK DITEMUKAN.');
            exit();
          }

          $userdetails = array_merge($userdetails, array(COL_SKPDID=>$rskpd[COL_SKPDID]));
        }

        $this->db->where(COL_USERNAME, $username);
        $this->db->update(TBL__USERS, array(COL_LASTLOGIN=>date('Y-m-d H:i:s'), COL_LASTLOGINIP=>$this->input->ip_address()));

        SetLoginSession($userdetails);
        redirect('sakipv2/user/dashboard');
      }else{
        $this->load->view('sakipv2/user/login');
      }

    } else {
      $this->load->view('sakipv2/user/login');
    }
  }

  function Logout(){
    UnsetLoginSession();
    redirect(site_url());
  }
  function Dashboard() {
    if(!IsLogin()) {
      redirect('sakipv2/user/login');
    }

    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEGUEST && empty($ruser[COL_SKPDID])) {
      redirect(site_url('sakipv2/user/logout'));
    }
    $data['title'] = 'DASHBOARD';

    $this->template->load('main', 'sakipv2/user/dashboard', $data);
  }

  public function changepassword() {
    $data['data'] = array();
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $this->form_validation->set_rules(array(
        array(
          'field' => 'NewPassword',
          'label' => 'NewPassword',
          'rules' => 'required|min_length[5]',
          'errors' => array('min_length' => 'Password minimal terdiri dari 5 karakter.')
        ),
        array(
          'field' => 'ConfirmPassword',
          'label' => 'Repeat ConfirmPassword',
          'rules' => 'required|matches[NewPassword]',
          'errors' => array('matches' => 'Kolom Konfirmasi Password wajib sama dengan Password Baru.')
        )
      ));

      if($ruser[COL_PASSWORD] != md5($this->input->post('OldPassword'))) {
        ShowJsonError('Password Lama tidak valid.');
        return false;
      }

      if($this->form_validation->run()) {
        $res = $this->db
        ->where(COL_USERNAME, $ruser[COL_USERNAME])
        ->update(TBL__USERS, array(COL_PASSWORD=>md5($this->input->post('NewPassword'))));
        if(!$res) {
          $err = $this->db->error();
          ShowJsonError($err['message']);
          return false;
        }

        ShowJsonSuccess('Password berhasil diperbarui.');
        return false;
      } else {
        ShowJsonError(validation_errors());
        return false;
      }
    } else {
      $this->load->view('sakipv2/user/changepassword', $data);
    }
  }
}
