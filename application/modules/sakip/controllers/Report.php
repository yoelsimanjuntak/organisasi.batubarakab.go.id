<?php
include_once(APPPATH."third_party/PhpOffice/PhpWord/Autoloader.php");

use PhpOffice\PhpWord\Autoloader;
Autoloader::register();
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 3/3/2019
 * Time: 10:31 AM
 */
class Report extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin() || (GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEBAPPEDA && GetLoggedUser()[COL_ROLEID] != ROLEKADIS && GetLoggedUser()[COL_ROLEID] != ROLEKEUANGAN)) {
            redirect('sakip/user/dashboard');
        }
    }

    function tc27() {
        $ruser = GetLoggedUser();
        $dat['title'] = "Laporan TC. 27";
        $dat['edit'] = false;
        $dat['cetak'] = $cetak = $this->input->post("cetak");

        $this->db->where(COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(COL_KD_TAHUN_TO." >=", date("Y"));
        $this->db->order_by(COL_KD_TAHUN_FROM, 'desc');
        $dat['rpemda'] = $rpemda = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();
        if(!empty($_POST)) {
            $dat['rpemda'] = $rpemda = $this->db->where(COL_KD_PEMDA, $this->input->post(COL_KD_PEMDA))->get(TBL_SAKIP_MPEMDA)->row_array();

            $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_PEMDA,"inner");
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_PEMDA, $this->input->post(COL_KD_PEMDA));
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_URUSAN, $this->input->post(COL_KD_URUSAN));
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_BIDANG, $this->input->post(COL_KD_BIDANG));
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_UNIT, $this->input->post(COL_KD_UNIT));
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SUB, $this->input->post(COL_KD_SUB));
            $this->db->group_by(array(COL_KD_TUJUANOPD));
            $this->db->order_by(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_TUJUANOPD, 'asc');
            $rtujuan = $this->db->get(TBL_SAKIP_MOPD_TUJUAN)->result_array();
            $dat['tujuan'] = $rtujuan;
            $dat['edit'] = true;
            $dat['data'] = array(
                COL_KD_TAHUN_FROM => count($rtujuan) > 0 ? $rtujuan[0][COL_KD_TAHUN_FROM] : "",
                COL_KD_TAHUN_TO => count($rtujuan) > 0 ? $rtujuan[0][COL_KD_TAHUN_TO] : "",
                COL_NM_PEJABAT => count($rtujuan) > 0 ? $rtujuan[0][COL_NM_PEJABAT] : "",
                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB)
            );
        }

        $nmSub = "";
        $strOPD = explode('.', $ruser[COL_COMPANYID]);
        $eplandb = $this->load->database("eplan", true);
        if($dat['edit']) {
            $eplandb->where(COL_KD_URUSAN, $dat['data'][COL_KD_URUSAN]);
            $eplandb->where(COL_KD_BIDANG, $dat['data'][COL_KD_BIDANG]);
            $eplandb->where(COL_KD_UNIT, $dat['data'][COL_KD_UNIT]);
            $eplandb->where(COL_KD_SUB, $dat['data'][COL_KD_SUB]);
            $subunit = $eplandb->get("ref_sub_unit")->row_array();
            if($subunit) {
                $nmSub = $subunit["Nm_Sub_Unit"];
            }
        }
        if($ruser[COL_ROLEID] == ROLEKADIS) {
            $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
            $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
            $eplandb->where(COL_KD_UNIT, $strOPD[2]);
            $eplandb->where(COL_KD_SUB, $strOPD[3]);
            $subunit = $eplandb->get("ref_sub_unit")->row_array();
            if($subunit) {
                $nmSub = $subunit["Nm_Sub_Unit"];
            }
        }
        $dat['strOPD'] = $strOPD;
        $dat['nmSub'] = $nmSub;
        $dat['eplandb'] = $eplandb;

        if($cetak) $this->load->view('report/tc27_partial', $dat);
        else $this->load->view('report/tc27', $dat);
    }

    function renja() {
        $ruser = GetLoggedUser();
        $dat['title'] = "Resume Renja";
        $dat['edit'] = false;
        $dat['cetak'] = $cetak = $this->input->post("cetak");

        if(!empty($_POST)) {
            $this->db->select(TBL_SAKIP_MBID_PROGRAM.".*,".TBL_SAKIP_MOPD_TUJUAN.".".COL_NM_TUJUANOPD.",".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_NM_INDIKATORTUJUANOPD.",".TBL_SAKIP_MOPD_SASARAN.".".COL_NM_SASARANOPD.",".TBL_SAKIP_MOPD_IKSASARAN.".".COL_NM_INDIKATORSASARANOPD.",".
                "(select GROUP_CONCAT(Nm_IndikatorProgramOPD separator ', ') from sakip_mbid_program_indikator
                    where
                        Kd_Urusan = sakip_mbid_program.Kd_Urusan
                        and Kd_Bidang = sakip_mbid_program.Kd_Bidang
                        and Kd_Unit = sakip_mbid_program.Kd_Unit
                        and Kd_Sub = sakip_mbid_program.Kd_Sub
                        and Kd_Bid = sakip_mbid_program.Kd_Bid
                        and Kd_Pemda = sakip_mbid_program.Kd_Pemda
                        and Kd_Misi = sakip_mbid_program.Kd_Misi
                        and Kd_Tujuan = sakip_mbid_program.Kd_Tujuan
                        and Kd_IndikatorTujuan = sakip_mbid_program.Kd_IndikatorTujuan
                        and Kd_Sasaran = sakip_mbid_program.Kd_Sasaran
                        and Kd_IndikatorSasaran = sakip_mbid_program.Kd_IndikatorSasaran
                        and Kd_TujuanOPD = sakip_mbid_program.Kd_TujuanOPD
                        and Kd_IndikatorTujuanOPD = sakip_mbid_program.Kd_IndikatorTujuanOPD
                        and Kd_SasaranOPD = sakip_mbid_program.Kd_SasaranOPD
                        and Kd_IndikatorSasaranOPD = sakip_mbid_program.Kd_IndikatorSasaranOPD
                        and Kd_ProgramOPD = sakip_mbid_program.Kd_ProgramOPD
                        and Kd_Tahun = sakip_mbid_program.Kd_Tahun
                ) as NmIndikatorProgram,
                GROUP_CONCAT(DISTINCT(sakip_msubbid_kegiatan.Kd_SumberDana) separator ', ') as KdSumberDanaProgram,
                sum(Total) as Total_Prg,
                sum(Total_N1) as TotalN1_Prg");
            $this->db->join(TBL_SAKIP_MOPD_TUJUAN,
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD
                ,"left");
            $this->db->join(TBL_SAKIP_MOPD_IKTUJUAN,
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD
                ,"left");
            $this->db->join(TBL_SAKIP_MOPD_SASARAN,
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARANOPD
                ,"left");
            $this->db->join(TBL_SAKIP_MOPD_IKSASARAN,
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARANOPD
                ,"left");
            $this->db->join(TBL_SAKIP_MSUBBID_KEGIATAN,
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_BID." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BID." AND ".

                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARANOPD." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARANOPD." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PROGRAMOPD." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TAHUN
                ,"left");
            /*$this->db->join(TBL_SAKIP_MBID_PROGRAM_INDIKATOR,
                TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_BID." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BID." AND ".

                TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARANOPD." AND ".
                TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARANOPD." AND ".
                TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PROGRAMOPD." AND ".
                TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TAHUN
                ,"left");*/
            $this->db->where(TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TAHUN, $this->input->post(COL_KD_TAHUN));
            $this->db->where(TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_URUSAN, $this->input->post(COL_KD_URUSAN));
            $this->db->where(TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_BIDANG, $this->input->post(COL_KD_BIDANG));
            $this->db->where(TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_UNIT, $this->input->post(COL_KD_UNIT));
            $this->db->where(TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SUB, $this->input->post(COL_KD_SUB));
            $this->db->order_by(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PROGRAMOPD, 'asc');
            $this->db->group_by(array(
                TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TAHUN,
                TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA,
                /*TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI,
                TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN,
                TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN,
                TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN,
                TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN,*/
                TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN,
                TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG,
                TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT,
                TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB,
                TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD,
                TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD,
                TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARANOPD,
                TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARANOPD,
                TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BID,
                TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PROGRAMOPD));
            $rprogram = $this->db->get(TBL_SAKIP_MBID_PROGRAM)->result_array();
            //echo $this->db->last_query();
            //return;
            $dat['program'] = $rprogram;
            $dat['edit'] = true;
            $dat['data'] = array(
                COL_KD_TAHUN => count($rprogram) > 0 ? $rprogram[0][COL_KD_TAHUN] : date("Y"),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB)
            );
        }

        $nmSub = "";
        $strOPD = explode('.', $ruser[COL_COMPANYID]);
        $eplandb = $this->load->database("eplan", true);
        if($dat['edit']) {
            $eplandb->where(COL_KD_URUSAN, $dat['data'][COL_KD_URUSAN]);
            $eplandb->where(COL_KD_BIDANG, $dat['data'][COL_KD_BIDANG]);
            $eplandb->where(COL_KD_UNIT, $dat['data'][COL_KD_UNIT]);
            $eplandb->where(COL_KD_SUB, $dat['data'][COL_KD_SUB]);
            $subunit = $eplandb->get("ref_sub_unit")->row_array();
            if($subunit) {
                $nmSub = $subunit["Nm_Sub_Unit"];
            }
        }
        if($ruser[COL_ROLEID] == ROLEKADIS) {
            $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
            $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
            $eplandb->where(COL_KD_UNIT, $strOPD[2]);
            $eplandb->where(COL_KD_SUB, $strOPD[3]);
            $subunit = $eplandb->get("ref_sub_unit")->row_array();
            if($subunit) {
                $nmSub = $subunit["Nm_Sub_Unit"];
            }
        }
        $dat['strOPD'] = $strOPD;
        $dat['nmSub'] = $nmSub;
        $dat['eplandb'] = $eplandb;

        if($cetak) $this->load->view('report/renja_partial', $dat);
        else $this->load->view('report/renja', $dat);
    }

    function rka() {
        $ruser = GetLoggedUser();
        $dat['title'] = "RKA";
        $dat['edit'] = false;
        $dat['cetak'] = $cetak = $this->input->post("cetak");

        if(!empty($_POST)) {
            $q = @"
            SELECT
            sakip_msubbid_kegiatan.*,
            sakip_mbid_program.*,
            sakip_mbid_program.Kd_Tahun,
            sakip_mbid_program.Nm_ProgramOPD,
            sakip_mbid_program_sasaran.Kd_SasaranProgramOPD,
            #sakip_mbid_program_sasaran.Kd_IndikatorProgramOPD,
            #sakip_mbid_program_sasaran.Kd_Satuan AS Satuan1,
            #sakip_mbid_program_sasaran.Target AS Target1,
            0 AS Total1,
            sakip_msubbid_kegiatan.Nm_KegiatanOPD,
            sakip_msubbid_kegiatan_indikator.Kd_SasaranKegiatanOPD,
            sakip_msubbid_kegiatan_indikator.Kd_IndikatorKegiatanOPD,
            sakip_msubbid_kegiatan_indikator.Nm_IndikatorKegiatanOPD,
            sakip_msubbid_kegiatan_indikator.Kd_Satuan AS Satuan2,
            sakip_msubbid_kegiatan_indikator.Target AS Target2,
            sakip_msubbid_kegiatan.Total AS Budget2,
            sakip_msubbid_kegiatan.Total AS Total2,
            (SELECT SUM(sakip_msubbid_kegiatan.Total) FROM `sakip_msubbid_kegiatan`
                WHERE `sakip_msubbid_kegiatan`.`Kd_Urusan` = `sakip_mbid_program_sasaran`.`Kd_Urusan`
                AND `sakip_msubbid_kegiatan`.`Kd_Bidang` = `sakip_mbid_program_sasaran`.`Kd_Bidang`
                AND `sakip_msubbid_kegiatan`.`Kd_Unit` = `sakip_mbid_program_sasaran`.`Kd_Unit`
                AND `sakip_msubbid_kegiatan`.`Kd_Sub` = `sakip_mbid_program_sasaran`.`Kd_Sub`
                #AND `sakip_msubbid_kegiatan`.`Kd_Bid` = `sakip_mbid_program_sasaran`.`Kd_Bid`
                AND `sakip_msubbid_kegiatan`.`Kd_Pemda` = `sakip_mbid_program_sasaran`.`Kd_Pemda`
                AND `sakip_msubbid_kegiatan`.`Kd_Misi` = `sakip_mbid_program_sasaran`.`Kd_Misi`
                AND `sakip_msubbid_kegiatan`.`Kd_Tujuan` = `sakip_mbid_program_sasaran`.`Kd_Tujuan`
                AND `sakip_msubbid_kegiatan`.`Kd_IndikatorTujuan` = `sakip_mbid_program_sasaran`.`Kd_IndikatorTujuan`
                AND `sakip_msubbid_kegiatan`.`Kd_Sasaran` = `sakip_mbid_program_sasaran`.`Kd_Sasaran`
                AND `sakip_msubbid_kegiatan`.`Kd_IndikatorSasaran` = `sakip_mbid_program_sasaran`.`Kd_IndikatorSasaran`
                AND `sakip_msubbid_kegiatan`.`Kd_TujuanOPD` = `sakip_mbid_program_sasaran`.`Kd_TujuanOPD`
                AND `sakip_msubbid_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_mbid_program_sasaran`.`Kd_IndikatorTujuanOPD`
                AND `sakip_msubbid_kegiatan`.`Kd_SasaranOPD` = `sakip_mbid_program_sasaran`.`Kd_SasaranOPD`
                AND `sakip_msubbid_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_mbid_program_sasaran`.`Kd_IndikatorSasaranOPD`
                AND `sakip_msubbid_kegiatan`.`Kd_ProgramOPD` = `sakip_mbid_program_sasaran`.`Kd_ProgramOPD`
                AND `sakip_msubbid_kegiatan`.`Kd_Tahun` = `sakip_mbid_program_sasaran`.`Kd_Tahun`
                AND `sakip_msubbid_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_mbid_program_sasaran`.`Kd_SasaranProgramOPD`
            ) AS sum_program_total,
            (SELECT SUM(sakip_msubbid_kegiatan.Total) FROM `sakip_msubbid_kegiatan`
                WHERE `sakip_msubbid_kegiatan`.`Kd_Urusan` = `sakip_msubbid_kegiatan`.`Kd_Urusan`
                AND `sakip_msubbid_kegiatan`.`Kd_Bidang` = `sakip_msubbid_kegiatan`.`Kd_Bidang`
                AND `sakip_msubbid_kegiatan`.`Kd_Unit` = `sakip_msubbid_kegiatan`.`Kd_Unit`
                AND `sakip_msubbid_kegiatan`.`Kd_Sub` = `sakip_msubbid_kegiatan`.`Kd_Sub`
                AND `sakip_msubbid_kegiatan`.`Kd_Bid` = `sakip_msubbid_kegiatan`.`Kd_Bid`
                AND `sakip_msubbid_kegiatan`.`Kd_Pemda` = `sakip_msubbid_kegiatan`.`Kd_Pemda`
                AND `sakip_msubbid_kegiatan`.`Kd_Misi` = `sakip_msubbid_kegiatan`.`Kd_Misi`
                AND `sakip_msubbid_kegiatan`.`Kd_Tujuan` = `sakip_msubbid_kegiatan`.`Kd_Tujuan`
                AND `sakip_msubbid_kegiatan`.`Kd_IndikatorTujuan` = `sakip_msubbid_kegiatan`.`Kd_IndikatorTujuan`
                AND `sakip_msubbid_kegiatan`.`Kd_Sasaran` = `sakip_msubbid_kegiatan`.`Kd_Sasaran`
                AND `sakip_msubbid_kegiatan`.`Kd_IndikatorSasaran` = `sakip_msubbid_kegiatan`.`Kd_IndikatorSasaran`
                AND `sakip_msubbid_kegiatan`.`Kd_TujuanOPD` = `sakip_msubbid_kegiatan`.`Kd_TujuanOPD`
                AND `sakip_msubbid_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_msubbid_kegiatan`.`Kd_IndikatorTujuanOPD`
                AND `sakip_msubbid_kegiatan`.`Kd_SasaranOPD` = `sakip_msubbid_kegiatan`.`Kd_SasaranOPD`
                AND `sakip_msubbid_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_msubbid_kegiatan`.`Kd_IndikatorSasaranOPD`
                AND `sakip_msubbid_kegiatan`.`Kd_ProgramOPD` = `sakip_msubbid_kegiatan`.`Kd_ProgramOPD`
                AND `sakip_msubbid_kegiatan`.`Kd_Tahun` = `sakip_msubbid_kegiatan`.`Kd_Tahun`
                AND `sakip_msubbid_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_msubbid_kegiatan`.`Kd_SasaranProgramOPD`
                AND `sakip_msubbid_kegiatan`.`Kd_KegiatanOPD` = `sakip_msubbid_kegiatan`.`Kd_KegiatanOPD`
            ) AS sum_kegiatan_total,
            (SELECT COUNT(*) FROM `sakip_mbid_program_sasaran`
                left join `sakip_msubbid_kegiatan` on
                  `sakip_msubbid_kegiatan`.`Kd_Urusan` = `sakip_mbid_program_sasaran`.`Kd_Urusan`
                  AND `sakip_msubbid_kegiatan`.`Kd_Bidang` = `sakip_mbid_program_sasaran`.`Kd_Bidang`
                  AND `sakip_msubbid_kegiatan`.`Kd_Unit` = `sakip_mbid_program_sasaran`.`Kd_Unit`
                  AND `sakip_msubbid_kegiatan`.`Kd_Sub` = `sakip_mbid_program_sasaran`.`Kd_Sub`
                  AND `sakip_msubbid_kegiatan`.`Kd_Bid` = `sakip_mbid_program_sasaran`.`Kd_Bid`
                  AND `sakip_msubbid_kegiatan`.`Kd_Pemda` = `sakip_mbid_program_sasaran`.`Kd_Pemda`
                  AND `sakip_msubbid_kegiatan`.`Kd_Misi` = `sakip_mbid_program_sasaran`.`Kd_Misi`
                  AND `sakip_msubbid_kegiatan`.`Kd_Tujuan` = `sakip_mbid_program_sasaran`.`Kd_Tujuan`
                  AND `sakip_msubbid_kegiatan`.`Kd_IndikatorTujuan` = `sakip_mbid_program_sasaran`.`Kd_IndikatorTujuan`
                  AND `sakip_msubbid_kegiatan`.`Kd_Sasaran` = `sakip_mbid_program_sasaran`.`Kd_Sasaran`
                  AND `sakip_msubbid_kegiatan`.`Kd_IndikatorSasaran` = `sakip_mbid_program_sasaran`.`Kd_IndikatorSasaran`
                  AND `sakip_msubbid_kegiatan`.`Kd_TujuanOPD` = `sakip_mbid_program_sasaran`.`Kd_TujuanOPD`
                  AND `sakip_msubbid_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_mbid_program_sasaran`.`Kd_IndikatorTujuanOPD`
                  AND `sakip_msubbid_kegiatan`.`Kd_SasaranOPD` = `sakip_mbid_program_sasaran`.`Kd_SasaranOPD`
                  AND `sakip_msubbid_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_mbid_program_sasaran`.`Kd_IndikatorSasaranOPD`
                  AND `sakip_msubbid_kegiatan`.`Kd_ProgramOPD` = `sakip_mbid_program_sasaran`.`Kd_ProgramOPD`
                  AND `sakip_msubbid_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_mbid_program_sasaran`.`Kd_SasaranProgramOPD`
                  AND `sakip_msubbid_kegiatan`.`Kd_Tahun` = `sakip_mbid_program_sasaran`.`Kd_Tahun`
                left join `sakip_msubbid_kegiatan_indikator` on
                  `sakip_msubbid_kegiatan_indikator`.`Kd_Urusan` = `sakip_msubbid_kegiatan`.`Kd_Urusan`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_Bidang` = `sakip_msubbid_kegiatan`.`Kd_Bidang`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_Unit` = `sakip_msubbid_kegiatan`.`Kd_Unit`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_Sub` = `sakip_msubbid_kegiatan`.`Kd_Sub`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_Bid` = `sakip_msubbid_kegiatan`.`Kd_Bid`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_Subbid` = `sakip_msubbid_kegiatan`.`Kd_Subbid`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_Pemda` = `sakip_msubbid_kegiatan`.`Kd_Pemda`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_Misi` = `sakip_msubbid_kegiatan`.`Kd_Misi`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_Tujuan` = `sakip_msubbid_kegiatan`.`Kd_Tujuan`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_IndikatorTujuan` = `sakip_msubbid_kegiatan`.`Kd_IndikatorTujuan`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_Sasaran` = `sakip_msubbid_kegiatan`.`Kd_Sasaran`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_IndikatorSasaran` = `sakip_msubbid_kegiatan`.`Kd_IndikatorSasaran`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_TujuanOPD` = `sakip_msubbid_kegiatan`.`Kd_TujuanOPD`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_msubbid_kegiatan`.`Kd_IndikatorTujuanOPD`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_SasaranOPD` = `sakip_msubbid_kegiatan`.`Kd_SasaranOPD`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_msubbid_kegiatan`.`Kd_IndikatorSasaranOPD`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_ProgramOPD` = `sakip_msubbid_kegiatan`.`Kd_ProgramOPD`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_SasaranProgramOPD` = `sakip_msubbid_kegiatan`.`Kd_SasaranProgramOPD`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_KegiatanOPD` = `sakip_msubbid_kegiatan`.`Kd_KegiatanOPD`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_Tahun` = `sakip_msubbid_kegiatan`.`Kd_Tahun`
                WHERE `sakip_mbid_program_sasaran`.`Kd_Urusan` = `sakip_mbid_program`.`Kd_Urusan`
                AND `sakip_mbid_program_sasaran`.`Kd_Bidang` = `sakip_mbid_program`.`Kd_Bidang`
                AND `sakip_mbid_program_sasaran`.`Kd_Unit` = `sakip_mbid_program`.`Kd_Unit`
                AND `sakip_mbid_program_sasaran`.`Kd_Sub` = `sakip_mbid_program`.`Kd_Sub`
                AND `sakip_mbid_program_sasaran`.`Kd_Pemda` = `sakip_mbid_program`.`Kd_Pemda`
                AND `sakip_mbid_program_sasaran`.`Kd_Misi` = `sakip_mbid_program`.`Kd_Misi`
                AND `sakip_mbid_program_sasaran`.`Kd_Tujuan` = `sakip_mbid_program`.`Kd_Tujuan`
                AND `sakip_mbid_program_sasaran`.`Kd_IndikatorTujuan` = `sakip_mbid_program`.`Kd_IndikatorTujuan`
                AND `sakip_mbid_program_sasaran`.`Kd_Sasaran` = `sakip_mbid_program`.`Kd_Sasaran`
                AND `sakip_mbid_program_sasaran`.`Kd_IndikatorSasaran` = `sakip_mbid_program`.`Kd_IndikatorSasaran`
                AND `sakip_mbid_program_sasaran`.`Kd_TujuanOPD` = `sakip_mbid_program`.`Kd_TujuanOPD`
                AND `sakip_mbid_program_sasaran`.`Kd_IndikatorTujuanOPD` = `sakip_mbid_program`.`Kd_IndikatorTujuanOPD`
                AND `sakip_mbid_program_sasaran`.`Kd_SasaranOPD` = `sakip_mbid_program`.`Kd_SasaranOPD`
                AND `sakip_mbid_program_sasaran`.`Kd_IndikatorSasaranOPD` = `sakip_mbid_program`.`Kd_IndikatorSasaranOPD`
                AND `sakip_mbid_program_sasaran`.`Kd_Bid` = `sakip_mbid_program`.`Kd_Bid`
                AND `sakip_mbid_program_sasaran`.`Kd_ProgramOPD` = `sakip_mbid_program`.`Kd_ProgramOPD`
                AND `sakip_mbid_program_sasaran`.`Kd_Tahun` = `sakip_mbid_program`.`Kd_Tahun`
            ) AS count_program_span,
            (SELECT COUNT(*) FROM `sakip_msubbid_kegiatan`
                left join `sakip_msubbid_kegiatan_indikator` on
                  `sakip_msubbid_kegiatan_indikator`.`Kd_Urusan` = `sakip_msubbid_kegiatan`.`Kd_Urusan`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_Bidang` = `sakip_msubbid_kegiatan`.`Kd_Bidang`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_Unit` = `sakip_msubbid_kegiatan`.`Kd_Unit`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_Sub` = `sakip_msubbid_kegiatan`.`Kd_Sub`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_Bid` = `sakip_msubbid_kegiatan`.`Kd_Bid`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_Subbid` = `sakip_msubbid_kegiatan`.`Kd_Subbid`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_Pemda` = `sakip_msubbid_kegiatan`.`Kd_Pemda`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_Misi` = `sakip_msubbid_kegiatan`.`Kd_Misi`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_Tujuan` = `sakip_msubbid_kegiatan`.`Kd_Tujuan`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_IndikatorTujuan` = `sakip_msubbid_kegiatan`.`Kd_IndikatorTujuan`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_Sasaran` = `sakip_msubbid_kegiatan`.`Kd_Sasaran`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_IndikatorSasaran` = `sakip_msubbid_kegiatan`.`Kd_IndikatorSasaran`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_TujuanOPD` = `sakip_msubbid_kegiatan`.`Kd_TujuanOPD`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_msubbid_kegiatan`.`Kd_IndikatorTujuanOPD`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_SasaranOPD` = `sakip_msubbid_kegiatan`.`Kd_SasaranOPD`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_msubbid_kegiatan`.`Kd_IndikatorSasaranOPD`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_ProgramOPD` = `sakip_msubbid_kegiatan`.`Kd_ProgramOPD`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_Tahun` = `sakip_msubbid_kegiatan`.`Kd_Tahun`
                  AND `sakip_msubbid_kegiatan_indikator`.`Kd_KegiatanOPD` = `sakip_msubbid_kegiatan`.`Kd_KegiatanOPD`
                WHERE `sakip_msubbid_kegiatan`.`Kd_Urusan` = `sakip_mbid_program`.`Kd_Urusan`
                AND `sakip_msubbid_kegiatan`.`Kd_Bidang` = `sakip_mbid_program`.`Kd_Bidang`
                AND `sakip_msubbid_kegiatan`.`Kd_Unit` = `sakip_mbid_program`.`Kd_Unit`
                AND `sakip_msubbid_kegiatan`.`Kd_Sub` = `sakip_mbid_program`.`Kd_Sub`
                AND `sakip_msubbid_kegiatan`.`Kd_Pemda` = `sakip_mbid_program`.`Kd_Pemda`
                AND `sakip_msubbid_kegiatan`.`Kd_Misi` = `sakip_mbid_program`.`Kd_Misi`
                AND `sakip_msubbid_kegiatan`.`Kd_Tujuan` = `sakip_mbid_program`.`Kd_Tujuan`
                AND `sakip_msubbid_kegiatan`.`Kd_IndikatorTujuan` = `sakip_mbid_program`.`Kd_IndikatorTujuan`
                AND `sakip_msubbid_kegiatan`.`Kd_Sasaran` = `sakip_mbid_program`.`Kd_Sasaran`
                AND `sakip_msubbid_kegiatan`.`Kd_IndikatorSasaran` = `sakip_mbid_program`.`Kd_IndikatorSasaran`
                AND `sakip_msubbid_kegiatan`.`Kd_TujuanOPD` = `sakip_mbid_program`.`Kd_TujuanOPD`
                AND `sakip_msubbid_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_mbid_program`.`Kd_IndikatorTujuanOPD`
                AND `sakip_msubbid_kegiatan`.`Kd_SasaranOPD` = `sakip_mbid_program`.`Kd_SasaranOPD`
                AND `sakip_msubbid_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_mbid_program`.`Kd_IndikatorSasaranOPD`
                AND `sakip_msubbid_kegiatan`.`Kd_Bid` = `sakip_mbid_program`.`Kd_Bid`
                AND `sakip_msubbid_kegiatan`.`Kd_ProgramOPD` = `sakip_mbid_program`.`Kd_ProgramOPD`
                AND `sakip_msubbid_kegiatan`.`Kd_Tahun` = `sakip_mbid_program`.`Kd_Tahun`
                AND `sakip_msubbid_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_mbid_program_sasaran`.`Kd_SasaranProgramOPD`
            ) AS count_sasaranprog_span,
            (SELECT COUNT(*) FROM `sakip_msubbid_kegiatan_indikator`
                WHERE `sakip_msubbid_kegiatan_indikator`.`Kd_Urusan` = `sakip_msubbid_kegiatan`.`Kd_Urusan`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_Bidang` = `sakip_msubbid_kegiatan`.`Kd_Bidang`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_Unit` = `sakip_msubbid_kegiatan`.`Kd_Unit`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_Sub` = `sakip_msubbid_kegiatan`.`Kd_Sub`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_Bid` = `sakip_msubbid_kegiatan`.`Kd_Bid`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_Subbid` = `sakip_msubbid_kegiatan`.`Kd_Subbid`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_Pemda` = `sakip_msubbid_kegiatan`.`Kd_Pemda`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_Misi` = `sakip_msubbid_kegiatan`.`Kd_Misi`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_Tujuan` = `sakip_msubbid_kegiatan`.`Kd_Tujuan`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_IndikatorTujuan` = `sakip_msubbid_kegiatan`.`Kd_IndikatorTujuan`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_Sasaran` = `sakip_msubbid_kegiatan`.`Kd_Sasaran`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_IndikatorSasaran` = `sakip_msubbid_kegiatan`.`Kd_IndikatorSasaran`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_TujuanOPD` = `sakip_msubbid_kegiatan`.`Kd_TujuanOPD`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_msubbid_kegiatan`.`Kd_IndikatorTujuanOPD`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_SasaranOPD` = `sakip_msubbid_kegiatan`.`Kd_SasaranOPD`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_msubbid_kegiatan`.`Kd_IndikatorSasaranOPD`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_ProgramOPD` = `sakip_msubbid_kegiatan`.`Kd_ProgramOPD`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_Tahun` = `sakip_msubbid_kegiatan`.`Kd_Tahun`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_SasaranProgramOPD` = `sakip_msubbid_kegiatan`.`Kd_SasaranProgramOPD`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_KegiatanOPD` = `sakip_msubbid_kegiatan`.`Kd_KegiatanOPD`
            ) AS count_kegiatan_span
            FROM `sakip_mbid_program`
            LEFT JOIN `sakip_mbid_program_sasaran`
                ON `sakip_mbid_program_sasaran`.`Kd_Urusan` = `sakip_mbid_program`.`Kd_Urusan`
                AND `sakip_mbid_program_sasaran`.`Kd_Bidang` = `sakip_mbid_program`.`Kd_Bidang`
                AND `sakip_mbid_program_sasaran`.`Kd_Unit` = `sakip_mbid_program`.`Kd_Unit`
                AND `sakip_mbid_program_sasaran`.`Kd_Sub` = `sakip_mbid_program`.`Kd_Sub`
                AND `sakip_mbid_program_sasaran`.`Kd_Bid` = `sakip_mbid_program`.`Kd_Bid`
                AND `sakip_mbid_program_sasaran`.`Kd_Pemda` = `sakip_mbid_program`.`Kd_Pemda`
                AND `sakip_mbid_program_sasaran`.`Kd_Misi` = `sakip_mbid_program`.`Kd_Misi`
                AND `sakip_mbid_program_sasaran`.`Kd_Tujuan` = `sakip_mbid_program`.`Kd_Tujuan`
                AND `sakip_mbid_program_sasaran`.`Kd_IndikatorTujuan` = `sakip_mbid_program`.`Kd_IndikatorTujuan`
                AND `sakip_mbid_program_sasaran`.`Kd_Sasaran` = `sakip_mbid_program`.`Kd_Sasaran`
                AND `sakip_mbid_program_sasaran`.`Kd_IndikatorSasaran` = `sakip_mbid_program`.`Kd_IndikatorSasaran`
                AND `sakip_mbid_program_sasaran`.`Kd_TujuanOPD` = `sakip_mbid_program`.`Kd_TujuanOPD`
                AND `sakip_mbid_program_sasaran`.`Kd_IndikatorTujuanOPD` = `sakip_mbid_program`.`Kd_IndikatorTujuanOPD`
                AND `sakip_mbid_program_sasaran`.`Kd_SasaranOPD` = `sakip_mbid_program`.`Kd_SasaranOPD`
                AND `sakip_mbid_program_sasaran`.`Kd_IndikatorSasaranOPD` = `sakip_mbid_program`.`Kd_IndikatorSasaranOPD`
                AND `sakip_mbid_program_sasaran`.`Kd_ProgramOPD` = `sakip_mbid_program`.`Kd_ProgramOPD`
                AND `sakip_mbid_program_sasaran`.`Kd_Tahun` = `sakip_mbid_program`.`Kd_Tahun`
            LEFT JOIN `sakip_msubbid_kegiatan`
                ON `sakip_msubbid_kegiatan`.`Kd_Urusan` = `sakip_mbid_program_sasaran`.`Kd_Urusan`
                AND `sakip_msubbid_kegiatan`.`Kd_Bidang` = `sakip_mbid_program_sasaran`.`Kd_Bidang`
                AND `sakip_msubbid_kegiatan`.`Kd_Unit` = `sakip_mbid_program_sasaran`.`Kd_Unit`
                AND `sakip_msubbid_kegiatan`.`Kd_Sub` = `sakip_mbid_program_sasaran`.`Kd_Sub`
                AND `sakip_msubbid_kegiatan`.`Kd_Bid` = `sakip_mbid_program_sasaran`.`Kd_Bid`
                AND `sakip_msubbid_kegiatan`.`Kd_Pemda` = `sakip_mbid_program_sasaran`.`Kd_Pemda`
                AND `sakip_msubbid_kegiatan`.`Kd_Misi` = `sakip_mbid_program_sasaran`.`Kd_Misi`
                AND `sakip_msubbid_kegiatan`.`Kd_Tujuan` = `sakip_mbid_program_sasaran`.`Kd_Tujuan`
                AND `sakip_msubbid_kegiatan`.`Kd_IndikatorTujuan` = `sakip_mbid_program_sasaran`.`Kd_IndikatorTujuan`
                AND `sakip_msubbid_kegiatan`.`Kd_Sasaran` = `sakip_mbid_program_sasaran`.`Kd_Sasaran`
                AND `sakip_msubbid_kegiatan`.`Kd_IndikatorSasaran` = `sakip_mbid_program_sasaran`.`Kd_IndikatorSasaran`
                AND `sakip_msubbid_kegiatan`.`Kd_TujuanOPD` = `sakip_mbid_program_sasaran`.`Kd_TujuanOPD`
                AND `sakip_msubbid_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_mbid_program_sasaran`.`Kd_IndikatorTujuanOPD`
                AND `sakip_msubbid_kegiatan`.`Kd_SasaranOPD` = `sakip_mbid_program_sasaran`.`Kd_SasaranOPD`
                AND `sakip_msubbid_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_mbid_program_sasaran`.`Kd_IndikatorSasaranOPD`
                AND `sakip_msubbid_kegiatan`.`Kd_ProgramOPD` = `sakip_mbid_program_sasaran`.`Kd_ProgramOPD`
                AND `sakip_msubbid_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_mbid_program_sasaran`.`Kd_SasaranProgramOPD`
                AND `sakip_msubbid_kegiatan`.`Kd_Tahun` = `sakip_mbid_program_sasaran`.`Kd_Tahun`
            LEFT JOIN `sakip_msubbid_kegiatan_indikator`
                ON `sakip_msubbid_kegiatan_indikator`.`Kd_Urusan` = `sakip_msubbid_kegiatan`.`Kd_Urusan`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_Bidang` = `sakip_msubbid_kegiatan`.`Kd_Bidang`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_Unit` = `sakip_msubbid_kegiatan`.`Kd_Unit`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_Sub` = `sakip_msubbid_kegiatan`.`Kd_Sub`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_Bid` = `sakip_msubbid_kegiatan`.`Kd_Bid`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_Pemda` = `sakip_msubbid_kegiatan`.`Kd_Pemda`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_Misi` = `sakip_msubbid_kegiatan`.`Kd_Misi`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_Tujuan` = `sakip_msubbid_kegiatan`.`Kd_Tujuan`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_IndikatorTujuan` = `sakip_msubbid_kegiatan`.`Kd_IndikatorTujuan`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_Sasaran` = `sakip_msubbid_kegiatan`.`Kd_Sasaran`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_IndikatorSasaran` = `sakip_msubbid_kegiatan`.`Kd_IndikatorSasaran`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_TujuanOPD` = `sakip_msubbid_kegiatan`.`Kd_TujuanOPD`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_msubbid_kegiatan`.`Kd_IndikatorTujuanOPD`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_SasaranOPD` = `sakip_msubbid_kegiatan`.`Kd_SasaranOPD`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_msubbid_kegiatan`.`Kd_IndikatorSasaranOPD`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_ProgramOPD` = `sakip_msubbid_kegiatan`.`Kd_ProgramOPD`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_SasaranProgramOPD` = `sakip_msubbid_kegiatan`.`Kd_SasaranProgramOPD`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_KegiatanOPD` = `sakip_msubbid_kegiatan`.`Kd_KegiatanOPD`
                AND `sakip_msubbid_kegiatan_indikator`.`Kd_Tahun` = `sakip_msubbid_kegiatan`.`Kd_Tahun`
            WHERE
                `sakip_mbid_program`.`Kd_Tahun` = ?
                AND `sakip_mbid_program`.`Kd_Urusan` = ?
                AND `sakip_mbid_program`.`Kd_Bidang` = ?
                AND `sakip_mbid_program`.`Kd_Unit` = ?
                AND `sakip_mbid_program`.`Kd_Sub` = ?
            /*group by
              sakip_mbid_program.Kd_ProgramOPD,
              sakip_msubbid_kegiatan.Kd_SasaranProgramOPD,
              sakip_msubbid_kegiatan.Kd_KegiatanOPD*/
            ORDER BY
              sakip_mbid_program.Kd_Bid,
              sakip_mbid_program.Kd_ProgramOPD,
              sakip_mbid_program.Uniq,
              sakip_mbid_program_sasaran.Kd_SasaranProgramOPD,
              #sakip_mbid_program_sasaran.Kd_IndikatorProgramOPD,
              sakip_msubbid_kegiatan.Kd_KegiatanOPD,
              sakip_msubbid_kegiatan_indikator.Kd_SasaranKegiatanOPD,
              sakip_msubbid_kegiatan_indikator.Kd_IndikatorKegiatanOPD
            ";
            $params = array(
                $this->input->post(COL_KD_TAHUN),
                $this->input->post(COL_KD_URUSAN),
                $this->input->post(COL_KD_BIDANG),
                $this->input->post(COL_KD_UNIT),
                $this->input->post(COL_KD_SUB),
            );
            $rprogram = $this->db->query($q, $params)->result_array();
            $dat['program'] = $rprogram;
            $dat['edit'] = true;
            $dat['data'] = array(
                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN), //count($rprogram) > 0 ? $rprogram[0][COL_KD_TAHUN] : date("Y"),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB)
            );
        }

        $nmSub = "";
        $strOPD = explode('.', $ruser[COL_COMPANYID]);
        $eplandb = $this->load->database("eplan", true);
        if($dat['edit']) {
            $eplandb->where(COL_KD_URUSAN, $dat['data'][COL_KD_URUSAN]);
            $eplandb->where(COL_KD_BIDANG, $dat['data'][COL_KD_BIDANG]);
            $eplandb->where(COL_KD_UNIT, $dat['data'][COL_KD_UNIT]);
            $eplandb->where(COL_KD_SUB, $dat['data'][COL_KD_SUB]);
            $subunit = $eplandb->get("ref_sub_unit")->row_array();
            if($subunit) {
                $nmSub = $subunit["Nm_Sub_Unit"];
            }
        }
        if($ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEKEUANGAN) {
            $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
            $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
            $eplandb->where(COL_KD_UNIT, $strOPD[2]);
            $eplandb->where(COL_KD_SUB, $strOPD[3]);
            $subunit = $eplandb->get("ref_sub_unit")->row_array();
            if($subunit) {
                $nmSub = $subunit["Nm_Sub_Unit"];
            }
        }
        $dat['strOPD'] = $strOPD;
        $dat['nmSub'] = $nmSub;
        $dat['eplandb'] = $eplandb;

        if($cetak) $this->load->view('report/rka_partial', $dat);
        else $this->load->view('report/rka', $dat);
    }

    function pohonkinerja() {
        $ruser = GetLoggedUser();
        $dat['title'] = "Pohon Kinerja";
        $dat['edit'] = false;
        $dat['SumberData'] = $this->input->get('SumberData');

        $this->db->where(COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(COL_KD_TAHUN_TO." >=", date("Y"));
        $this->db->order_by(COL_KD_TAHUN_FROM, 'desc');
        $dat['rpemda'] = $rpemda = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();

        if (!empty($_GET)) {
            $this->db->where(COL_KD_TAHUN_FROM." <=", $this->input->get(COL_KD_TAHUN));
            $this->db->where(COL_KD_TAHUN_TO." >=", $this->input->get(COL_KD_TAHUN));
            $this->db->order_by(COL_KD_TAHUN_FROM, 'desc');
            $dat['rpemda'] = $rpemda = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();
            if(count($rpemda) == 0) {
                echo "Tahun tidak valid.";
                return;
            }

            $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MPMD_MISI.".".COL_KD_PEMDA,"inner");
            $this->db->where(TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA, $rpemda[COL_KD_PEMDA]);
            $this->db->order_by(TBL_SAKIP_MPMD_MISI.".".COL_KD_MISI, 'asc');
            $rmisi = $this->db->get(TBL_SAKIP_MPMD_MISI)->result_array();
            $dat['misi'] = $rmisi;

            $dat['data'] = array(
                COL_KD_TAHUN_FROM => count($rmisi) > 0 ? $rmisi[0][COL_KD_TAHUN_FROM] : "",
                COL_KD_TAHUN_TO => count($rmisi) > 0 ? $rmisi[0][COL_KD_TAHUN_TO] : "",
                COL_NM_PEJABAT => count($rmisi) > 0 ? $rmisi[0][COL_NM_PEJABAT] : "",
                COL_KD_PEMDA => $rpemda[COL_KD_PEMDA],
                COL_KD_TAHUN => $this->input->get(COL_KD_TAHUN),
                COL_KD_URUSAN => $this->input->get(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->get(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->get(COL_KD_UNIT),
                COL_KD_SUB => $this->input->get(COL_KD_SUB)
            );

            $nmSub = "";
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $eplandb = $this->load->database("eplan", true);
            if($dat['edit']) {
                $eplandb->where(COL_KD_URUSAN, $dat['data'][COL_KD_URUSAN]);
                $eplandb->where(COL_KD_BIDANG, $dat['data'][COL_KD_BIDANG]);
                $eplandb->where(COL_KD_UNIT, $dat['data'][COL_KD_UNIT]);
                $eplandb->where(COL_KD_SUB, $dat['data'][COL_KD_SUB]);
                $subunit = $eplandb->get("ref_sub_unit")->row_array();
                if($subunit) {
                    $nmSub = $subunit["Nm_Sub_Unit"];
                }
            }
            if($ruser[COL_ROLEID] == ROLEKADIS) {
                $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
                $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
                $eplandb->where(COL_KD_UNIT, $strOPD[2]);
                $eplandb->where(COL_KD_SUB, $strOPD[3]);
                $subunit = $eplandb->get("ref_sub_unit")->row_array();
                if($subunit) {
                    $nmSub = $subunit["Nm_Sub_Unit"];
                }
            }
            $dat['strOPD'] = $strOPD;
            $dat['nmSub'] = $nmSub;
            $this->load->view('report/pohonkinerja_', $dat);
        } else {
            $nmSub = "";
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $eplandb = $this->load->database("eplan", true);
            if($dat['edit']) {
                $eplandb->where(COL_KD_URUSAN, $dat['data'][COL_KD_URUSAN]);
                $eplandb->where(COL_KD_BIDANG, $dat['data'][COL_KD_BIDANG]);
                $eplandb->where(COL_KD_UNIT, $dat['data'][COL_KD_UNIT]);
                $eplandb->where(COL_KD_SUB, $dat['data'][COL_KD_SUB]);
                $subunit = $eplandb->get("ref_sub_unit")->row_array();
                if($subunit) {
                    $nmSub = $subunit["Nm_Sub_Unit"];
                }
            }
            if($ruser[COL_ROLEID] == ROLEKADIS) {
                $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
                $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
                $eplandb->where(COL_KD_UNIT, $strOPD[2]);
                $eplandb->where(COL_KD_SUB, $strOPD[3]);
                $subunit = $eplandb->get("ref_sub_unit")->row_array();
                if($subunit) {
                    $nmSub = $subunit["Nm_Sub_Unit"];
                }
            }
            $dat['strOPD'] = $strOPD;
            $dat['nmSub'] = $nmSub;
            $dat['eplandb'] = $eplandb;

            $this->load->view('report/pohonkinerja', $dat);
        }
    }

    function pohonkinerja_kabupaten() {
        $ruser = GetLoggedUser();
        $dat['title'] = "Pohon Kinerja Kabupaten";
        $dat['edit'] = false;

        $this->db->where(COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(COL_KD_TAHUN_TO." >=", date("Y"));
        $this->db->order_by(COL_KD_TAHUN_FROM, 'desc');
        $dat['rpemda'] = $rpemda = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();
        if (!empty($_GET)) {
            $this->db->where(TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA, $this->input->get(COL_KD_PEMDA));
            $this->db->order_by(COL_KD_TAHUN_FROM, 'desc');
            $dat['rpemda'] = $rpemda = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();
            if(count($rpemda) == 0) {
                echo "Tahun tidak valid.";
                return;
            }

            $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MPMD_MISI.".".COL_KD_PEMDA,"inner");
            $this->db->where(TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA, $rpemda[COL_KD_PEMDA]);
            $this->db->order_by(TBL_SAKIP_MPMD_MISI.".".COL_KD_MISI, 'asc');
            if(!empty($this->input->get(COL_KD_MISI))) {
              $this->db->where(COL_KD_MISI, $this->input->get(COL_KD_MISI));
            }
            $rmisi = $this->db->get(TBL_SAKIP_MPMD_MISI)->result_array();
            $dat['misi'] = $rmisi;

            $dat['data'] = array(
                COL_KD_TAHUN_FROM => count($rmisi) > 0 ? $rmisi[0][COL_KD_TAHUN_FROM] : "",
                COL_KD_TAHUN_TO => count($rmisi) > 0 ? $rmisi[0][COL_KD_TAHUN_TO] : "",
                COL_NM_PEJABAT => count($rmisi) > 0 ? $rmisi[0][COL_NM_PEJABAT] : "",
                COL_KD_PEMDA => $rpemda[COL_KD_PEMDA],
                COL_KD_TAHUN => $this->input->get(COL_KD_TAHUN),
                COL_KD_URUSAN => $this->input->get(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->get(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->get(COL_KD_UNIT),
                COL_KD_SUB => $this->input->get(COL_KD_SUB)
            );

            $nmSub = "";
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $eplandb = $this->load->database("eplan", true);
            if($ruser[COL_ROLEID] == ROLEKADIS) {
                $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
                $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
                $eplandb->where(COL_KD_UNIT, $strOPD[2]);
                $eplandb->where(COL_KD_SUB, $strOPD[3]);
                $subunit = $eplandb->get("ref_sub_unit")->row_array();
                if($subunit) {
                    $nmSub = $subunit["Nm_Sub_Unit"];
                }
            } else {
                $eplandb->where(COL_KD_URUSAN, $dat['data'][COL_KD_URUSAN]);
                $eplandb->where(COL_KD_BIDANG, $dat['data'][COL_KD_BIDANG]);
                $eplandb->where(COL_KD_UNIT, $dat['data'][COL_KD_UNIT]);
                $eplandb->where(COL_KD_SUB, $dat['data'][COL_KD_SUB]);
                $subunit = $eplandb->get("ref_sub_unit")->row_array();
                if($subunit) {
                    $nmSub = $subunit["Nm_Sub_Unit"];
                }
            }
            $dat['strOPD'] = $strOPD;
            $dat['nmSub'] = $nmSub;
            $this->load->view('report/pohonkinerja_kab_', $dat);
        } else {
            $nmSub = "";
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $eplandb = $this->load->database("eplan", true);
            if($ruser[COL_ROLEID] == ROLEKADIS) {
                $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
                $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
                $eplandb->where(COL_KD_UNIT, $strOPD[2]);
                $eplandb->where(COL_KD_SUB, $strOPD[3]);
                $subunit = $eplandb->get("ref_sub_unit")->row_array();
                if($subunit) {
                    $nmSub = $subunit["Nm_Sub_Unit"];
                }
            }
            $dat['strOPD'] = $strOPD;
            $dat['nmSub'] = $nmSub;
            $dat['eplandb'] = $eplandb;

            $this->load->view('report/pohonkinerja_kab', $dat);
        }
    }

    function cascading() {
        $ruser = GetLoggedUser();
        $dat['title'] = "Cascading";
        $dat['edit'] = false;
        $this->db->where(COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(COL_KD_TAHUN_TO." >=", date("Y"));
        $this->db->order_by(COL_KD_TAHUN_FROM, 'desc');
        $dat['rpemda'] = $rpemda = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();

        if (!empty($_GET)) {
            $this->db->where(COL_KD_TAHUN_FROM." <=", $this->input->get(COL_KD_TAHUN));
            $this->db->where(COL_KD_TAHUN_TO." >=", $this->input->get(COL_KD_TAHUN));
            $this->db->order_by(COL_KD_TAHUN_FROM, 'desc');
            $dat['rpemda'] = $rpemda = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();
            if(count($rpemda) == 0) {
                echo "Tahun tidak valid.";
                return;
            }

            $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_PEMDA,"inner");
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_PEMDA, $rpemda[COL_KD_PEMDA]);
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_URUSAN, $this->input->get(COL_KD_URUSAN));
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_BIDANG, $this->input->get(COL_KD_BIDANG));
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_UNIT, $this->input->get(COL_KD_UNIT));
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SUB, $this->input->get(COL_KD_SUB));
            $this->db->order_by(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_TUJUANOPD, 'asc');
            $this->db->group_by(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_TUJUANOPD);
            $rtujuan = $this->db->get(TBL_SAKIP_MOPD_TUJUAN)->result_array();
            $dat['rtujuan'] = $rtujuan;

            $dat['data'] = array(
                COL_KD_TAHUN_FROM => count($rtujuan) > 0 ? $rtujuan[0][COL_KD_TAHUN_FROM] : "",
                COL_KD_TAHUN_TO => count($rtujuan) > 0 ? $rtujuan[0][COL_KD_TAHUN_TO] : "",
                COL_NM_PEJABAT => count($rtujuan) > 0 ? $rtujuan[0][COL_NM_PEJABAT] : "",
                COL_KD_PEMDA => $rpemda[COL_KD_PEMDA],
                COL_KD_TAHUN => $this->input->get(COL_KD_TAHUN),
                COL_KD_URUSAN => $this->input->get(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->get(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->get(COL_KD_UNIT),
                COL_KD_SUB => $this->input->get(COL_KD_SUB)
            );

            $nmSub = "";
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $eplandb = $this->load->database("eplan", true);
            if($dat['edit']) {
                $eplandb->where(COL_KD_URUSAN, $dat['data'][COL_KD_URUSAN]);
                $eplandb->where(COL_KD_BIDANG, $dat['data'][COL_KD_BIDANG]);
                $eplandb->where(COL_KD_UNIT, $dat['data'][COL_KD_UNIT]);
                $eplandb->where(COL_KD_SUB, $dat['data'][COL_KD_SUB]);
                $subunit = $eplandb->get("ref_sub_unit")->row_array();
                if($subunit) {
                    $nmSub = $subunit["Nm_Sub_Unit"];
                }
            }
            if($ruser[COL_ROLEID] == ROLEKADIS) {
                $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
                $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
                $eplandb->where(COL_KD_UNIT, $strOPD[2]);
                $eplandb->where(COL_KD_SUB, $strOPD[3]);
                $subunit = $eplandb->get("ref_sub_unit")->row_array();
                if($subunit) {
                    $nmSub = $subunit["Nm_Sub_Unit"];
                }
            }
            $dat['strOPD'] = $strOPD;
            $dat['nmSub'] = $nmSub;
            $this->load->view('report/cascading_', $dat);
        } else {
            $nmSub = "";
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $eplandb = $this->load->database("eplan", true);
            if($dat['edit']) {
                $eplandb->where(COL_KD_URUSAN, $dat['data'][COL_KD_URUSAN]);
                $eplandb->where(COL_KD_BIDANG, $dat['data'][COL_KD_BIDANG]);
                $eplandb->where(COL_KD_UNIT, $dat['data'][COL_KD_UNIT]);
                $eplandb->where(COL_KD_SUB, $dat['data'][COL_KD_SUB]);
                $subunit = $eplandb->get("ref_sub_unit")->row_array();
                if($subunit) {
                    $nmSub = $subunit["Nm_Sub_Unit"];
                }
            }
            if($ruser[COL_ROLEID] == ROLEKADIS) {
                $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
                $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
                $eplandb->where(COL_KD_UNIT, $strOPD[2]);
                $eplandb->where(COL_KD_SUB, $strOPD[3]);
                $subunit = $eplandb->get("ref_sub_unit")->row_array();
                if($subunit) {
                    $nmSub = $subunit["Nm_Sub_Unit"];
                }
            }
            $dat['strOPD'] = $strOPD;
            $dat['nmSub'] = $nmSub;
            $dat['eplandb'] = $eplandb;

            $this->load->view('report/cascading', $dat);
        }
    }

    function cascading_pemda() {
      $ruser = GetLoggedUser();
      $dat['title'] = "Cascading Pemerintah Daerah";
      $dat['edit'] = false;

      $this->db->where(COL_KD_TAHUN_FROM." <=", date("Y"));
      $this->db->where(COL_KD_TAHUN_TO." >=", date("Y"));
      $this->db->order_by(COL_KD_TAHUN_FROM, 'desc');
      $dat['rpemda'] = $rpemda = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();
      if (!empty($_GET)) {
          $this->db->where(TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA, $this->input->get(COL_KD_PEMDA));
          $this->db->order_by(COL_KD_TAHUN_FROM, 'desc');
          $dat['rpemda'] = $rpemda = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();
          if(count($rpemda) == 0) {
              echo "Tahun tidak valid.";
              return;
          }

          $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MPMD_MISI.".".COL_KD_PEMDA,"inner");
          $this->db->where(TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA, $rpemda[COL_KD_PEMDA]);
          $this->db->order_by(TBL_SAKIP_MPMD_MISI.".".COL_KD_MISI, 'asc');
          if(!empty($this->input->get(COL_KD_MISI))) {
            $this->db->where(COL_KD_MISI, $this->input->get(COL_KD_MISI));
          }
          $rmisi = $this->db->get(TBL_SAKIP_MPMD_MISI)->result_array();
          $dat['misi'] = $rmisi;

          $dat['data'] = array(
              COL_KD_TAHUN_FROM => count($rmisi) > 0 ? $rmisi[0][COL_KD_TAHUN_FROM] : "",
              COL_KD_TAHUN_TO => count($rmisi) > 0 ? $rmisi[0][COL_KD_TAHUN_TO] : "",
              COL_NM_PEJABAT => count($rmisi) > 0 ? $rmisi[0][COL_NM_PEJABAT] : "",
              COL_KD_PEMDA => $rpemda[COL_KD_PEMDA],
              COL_KD_TAHUN => $this->input->get(COL_KD_TAHUN),
              COL_KD_URUSAN => $this->input->get(COL_KD_URUSAN),
              COL_KD_BIDANG => $this->input->get(COL_KD_BIDANG),
              COL_KD_UNIT => $this->input->get(COL_KD_UNIT),
              COL_KD_SUB => $this->input->get(COL_KD_SUB)
          );

          $nmSub = "";
          $strOPD = explode('.', $ruser[COL_COMPANYID]);
          $eplandb = $this->load->database("eplan", true);
          if($ruser[COL_ROLEID] == ROLEKADIS) {
              $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
              $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
              $eplandb->where(COL_KD_UNIT, $strOPD[2]);
              $eplandb->where(COL_KD_SUB, $strOPD[3]);
              $subunit = $eplandb->get("ref_sub_unit")->row_array();
              if($subunit) {
                  $nmSub = $subunit["Nm_Sub_Unit"];
              }
          } else {
              $eplandb->where(COL_KD_URUSAN, $dat['data'][COL_KD_URUSAN]);
              $eplandb->where(COL_KD_BIDANG, $dat['data'][COL_KD_BIDANG]);
              $eplandb->where(COL_KD_UNIT, $dat['data'][COL_KD_UNIT]);
              $eplandb->where(COL_KD_SUB, $dat['data'][COL_KD_SUB]);
              $subunit = $eplandb->get("ref_sub_unit")->row_array();
              if($subunit) {
                  $nmSub = $subunit["Nm_Sub_Unit"];
              }
          }
          $dat['strOPD'] = $strOPD;
          $dat['nmSub'] = $nmSub;
          $this->load->view('report/cascadingkab_', $dat);
      } else {
          $nmSub = "";
          $strOPD = explode('.', $ruser[COL_COMPANYID]);
          $eplandb = $this->load->database("eplan", true);
          if($ruser[COL_ROLEID] == ROLEKADIS) {
              $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
              $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
              $eplandb->where(COL_KD_UNIT, $strOPD[2]);
              $eplandb->where(COL_KD_SUB, $strOPD[3]);
              $subunit = $eplandb->get("ref_sub_unit")->row_array();
              if($subunit) {
                  $nmSub = $subunit["Nm_Sub_Unit"];
              }
          }
          $dat['strOPD'] = $strOPD;
          $dat['nmSub'] = $nmSub;
          $dat['eplandb'] = $eplandb;

          $this->load->view('report/cascadingkab', $dat);
      }
    }

    function perjanjiankerja() {
        $ruser = GetLoggedUser();
        $dat['title'] = "Perjanjian Kinerja";
        $dat['edit'] = false;
        $dat['SumberData'] = $SumberData = $this->input->get('SumberData');

        $this->db->where(COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(COL_KD_TAHUN_TO." >=", date("Y"));
        $this->db->order_by(COL_KD_TAHUN_FROM, 'desc');
        $dat['rpemda'] = $rpemda = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();

        if (!empty($_GET)) {
            $this->db->where(COL_KD_TAHUN_FROM." <=", $this->input->get(COL_KD_TAHUN));
            $this->db->where(COL_KD_TAHUN_TO." >=", $this->input->get(COL_KD_TAHUN));
            $this->db->order_by(COL_KD_TAHUN_FROM, 'desc');
            $dat['rpemda'] = $rpemda = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();
            if(count($rpemda) == 0) {
                echo "Tahun tidak valid.";
                return;
            }

            $dat['data'] = array(
                COL_KD_TAHUN_FROM => $rpemda[COL_KD_TAHUN_FROM],
                COL_KD_TAHUN_TO => $rpemda[COL_KD_TAHUN_TO],
                COL_NM_PEJABAT => $rpemda[COL_NM_PEJABAT],
                COL_KD_PEMDA => $rpemda[COL_KD_PEMDA],
                COL_KD_TAHUN => $this->input->get(COL_KD_TAHUN),
                COL_KD_URUSAN => $this->input->get(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->get(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->get(COL_KD_UNIT),
                COL_KD_SUB => $this->input->get(COL_KD_SUB),
                COL_KD_SASARANINDIVIDU => $this->input->get(COL_KD_SASARANINDIVIDU)
            );

            $nmSub = "";
            $kop1 = "";
            $kop2 = "";
            $nmPimpinanSub = "";
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $eplandb = $this->load->database("eplan", true);
            $eplandb->where(COL_KD_URUSAN, !empty($strOPD[0]) ? $strOPD[0] : $this->input->get(COL_KD_URUSAN));
            $eplandb->where(COL_KD_BIDANG, !empty($strOPD[1]) ? $strOPD[1] : $this->input->get(COL_KD_BIDANG));
            $eplandb->where(COL_KD_UNIT, !empty($strOPD[2]) ? $strOPD[2] : $this->input->get(COL_KD_UNIT));
            $eplandb->where(COL_KD_SUB, !empty($strOPD[3]) ? $strOPD[3] : $this->input->get(COL_KD_SUB));
            $subunit = $eplandb->get("ref_sub_unit")->row_array();

            $eplandb->where(COL_KD_URUSAN, !empty($strOPD[0]) ? $strOPD[0] : $this->input->get(COL_KD_URUSAN));
            $eplandb->where(COL_KD_BIDANG, !empty($strOPD[1]) ? $strOPD[1] : $this->input->get(COL_KD_BIDANG));
            $eplandb->where(COL_KD_UNIT, !empty($strOPD[2]) ? $strOPD[2] : $this->input->get(COL_KD_UNIT));
            $eplandb->where(COL_KD_SUB, !empty($strOPD[3]) ? $strOPD[3] : $this->input->get(COL_KD_SUB));
            //$eplandb->where("(Tahun = ".($this->input->get(COL_KD_TAHUN)-1)." or Tahun > ".($this->input->get(COL_KD_TAHUN)-1).")");
            //$eplandb->order_by("Tahun", 'asc');
            $subunit_ = $eplandb->get("ref_sub_unit")->row_array();

            if($subunit) {
              $nmSub = $subunit["Nm_Sub_Unit"];
              $kop1 = $subunit["Nm_Kop1"];
              $kop2 = $subunit["Nm_Kop2"];
            }
            if($subunit_) {
                $nmPimpinanSub = $subunit_["Nm_Pimpinan"];
            }
            $dat['strOPD'] = $strOPD;
            $dat['nmSub'] = $nmSub;
            $dat['kop1'] = $kop1;
            $dat['kop2'] = $kop2;
            $dat['nmPimpinanSub'] = $nmPimpinanSub;
            $dat['eplandb'] = $eplandb;

            $this->load->library('Mypdf');
            $mpdf = new Mypdf();

            $html = $this->load->view('report/perjanjiankerja_', $dat, TRUE);
            //echo $html;
            //return;
            $mpdf->pdf->WriteHTML($html);
            $mpdf->pdf->Output('Perjanjian Kinerja.pdf', 'I');
        } else {
            $nmSub = "";
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $eplandb = $this->load->database("eplan", true);
            if($dat['edit']) {
                $eplandb->where(COL_KD_URUSAN, $dat['data'][COL_KD_URUSAN]);
                $eplandb->where(COL_KD_BIDANG, $dat['data'][COL_KD_BIDANG]);
                $eplandb->where(COL_KD_UNIT, $dat['data'][COL_KD_UNIT]);
                $eplandb->where(COL_KD_SUB, $dat['data'][COL_KD_SUB]);
                $subunit = $eplandb->get("ref_sub_unit")->row_array();
                if($subunit) {
                    $nmSub = $subunit["Nm_Sub_Unit"];
                }
            }
            if($ruser[COL_ROLEID] == ROLEKADIS) {
                $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
                $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
                $eplandb->where(COL_KD_UNIT, $strOPD[2]);
                $eplandb->where(COL_KD_SUB, $strOPD[3]);
                $subunit = $eplandb->get("ref_sub_unit")->row_array();
                if($subunit) {
                    $nmSub = $subunit["Nm_Sub_Unit"];
                }
            }
            $dat['strOPD'] = $strOPD;
            $dat['nmSub'] = $nmSub;
            $dat['eplandb'] = $eplandb;

            $this->load->view('report/perjanjiankerja', $dat);
        }
    }

    function rencana_aksi() {
        $ruser = GetLoggedUser();
        $dat['title'] = "Rencana Aksi";
        $dat['edit'] = false;
        $dat['cetak'] = $cetak = $this->input->post("cetak");
        $dat['SumberData'] = $SumberData = $this->input->post("SumberData");

        if(!empty($_POST)) {
            $q = @"
            SELECT
            sakip_dpa_program.*,
            sakip_dpa_program.Kd_Tahun,
            sakip_dpa_program.Nm_ProgramOPD,
            sakip_dpa_program_sasaran.Kd_SasaranProgramOPD,
            sakip_dpa_program_sasaran.Kd_Satuan AS Satuan1,
            sakip_dpa_program_sasaran.Target AS Target1,
            sakip_dpa_program_sasaran.Target_TW1 AS Program_TW1,
            sakip_dpa_program_sasaran.Target_TW2 AS Program_TW2,
            sakip_dpa_program_sasaran.Target_TW3 AS Program_TW3,
            sakip_dpa_program_sasaran.Target_TW4 AS Program_TW4,
            sakip_dpa_kegiatan.Kd_Subbid,
            sakip_dpa_kegiatan.Kd_KegiatanOPD,
            sakip_dpa_kegiatan.Nm_KegiatanOPD,
            sakip_dpa_kegiatan_indikator.Kd_SasaranKegiatanOPD,
            sakip_dpa_kegiatan_indikator.Kd_IndikatorKegiatanOPD,
            sakip_dpa_kegiatan_indikator.Nm_IndikatorKegiatanOPD,
            sakip_dpa_kegiatan_indikator.Kd_Satuan AS Satuan2,
            sakip_dpa_kegiatan_indikator.Target AS Target2,
            sakip_dpa_kegiatan_indikator.Target_TW1 AS Kegiatan_TW1,
            sakip_dpa_kegiatan_indikator.Target_TW2 AS Kegiatan_TW2,
            sakip_dpa_kegiatan_indikator.Target_TW3 AS Kegiatan_TW3,
            sakip_dpa_kegiatan_indikator.Target_TW4 AS Kegiatan_TW4,
            sakip_dpa_kegiatan.Budget_TW1 AS B_Kegiatan_TW1,
            sakip_dpa_kegiatan.Budget_TW2 AS B_Kegiatan_TW2,
            sakip_dpa_kegiatan.Budget_TW3 AS B_Kegiatan_TW3,
            sakip_dpa_kegiatan.Budget_TW4 AS B_Kegiatan_TW4,
            sakip_dpa_kegiatan.Total AS Total2,
            (SELECT SUM(sakip_dpa_kegiatan.Total) FROM `sakip_dpa_kegiatan`
                WHERE `sakip_dpa_kegiatan`.`Kd_Urusan` = `sakip_dpa_program`.`Kd_Urusan`
                AND `sakip_dpa_kegiatan`.`Kd_Bidang` = `sakip_dpa_program`.`Kd_Bidang`
                AND `sakip_dpa_kegiatan`.`Kd_Unit` = `sakip_dpa_program`.`Kd_Unit`
                AND `sakip_dpa_kegiatan`.`Kd_Sub` = `sakip_dpa_program`.`Kd_Sub`
                AND `sakip_dpa_kegiatan`.`Kd_Pemda` = `sakip_dpa_program`.`Kd_Pemda`
                AND `sakip_dpa_kegiatan`.`Kd_Misi` = `sakip_dpa_program`.`Kd_Misi`
                AND `sakip_dpa_kegiatan`.`Kd_Tujuan` = `sakip_dpa_program`.`Kd_Tujuan`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_dpa_program`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_kegiatan`.`Kd_Sasaran` = `sakip_dpa_program`.`Kd_Sasaran`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_dpa_program`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_kegiatan`.`Kd_TujuanOPD` = `sakip_dpa_program`.`Kd_TujuanOPD`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_program`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_kegiatan`.`Kd_SasaranOPD` = `sakip_dpa_program`.`Kd_SasaranOPD`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_program`.`Kd_IndikatorSasaranOPD`
                AND `sakip_dpa_kegiatan`.`Kd_ProgramOPD` = `sakip_dpa_program`.`Kd_ProgramOPD`
                AND `sakip_dpa_kegiatan`.`Kd_Tahun` = `sakip_dpa_program`.`Kd_Tahun`
            ) AS sum_program_total,
            (SELECT SUM(sakip_dpa_kegiatan.Budget_TW1) FROM `sakip_dpa_kegiatan`
                WHERE `sakip_dpa_kegiatan`.`Kd_Urusan` = `sakip_dpa_program_sasaran`.`Kd_Urusan`
                AND `sakip_dpa_kegiatan`.`Kd_Bidang` = `sakip_dpa_program_sasaran`.`Kd_Bidang`
                AND `sakip_dpa_kegiatan`.`Kd_Unit` = `sakip_dpa_program_sasaran`.`Kd_Unit`
                AND `sakip_dpa_kegiatan`.`Kd_Sub` = `sakip_dpa_program_sasaran`.`Kd_Sub`
                AND `sakip_dpa_kegiatan`.`Kd_Pemda` = `sakip_dpa_program_sasaran`.`Kd_Pemda`
                AND `sakip_dpa_kegiatan`.`Kd_Misi` = `sakip_dpa_program_sasaran`.`Kd_Misi`
                AND `sakip_dpa_kegiatan`.`Kd_Tujuan` = `sakip_dpa_program_sasaran`.`Kd_Tujuan`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_kegiatan`.`Kd_Sasaran` = `sakip_dpa_program_sasaran`.`Kd_Sasaran`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_kegiatan`.`Kd_TujuanOPD` = `sakip_dpa_program_sasaran`.`Kd_TujuanOPD`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_kegiatan`.`Kd_SasaranOPD` = `sakip_dpa_program_sasaran`.`Kd_SasaranOPD`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaranOPD`
                AND `sakip_dpa_kegiatan`.`Kd_ProgramOPD` = `sakip_dpa_program_sasaran`.`Kd_ProgramOPD`
                AND `sakip_dpa_kegiatan`.`Kd_Tahun` = `sakip_dpa_program_sasaran`.`Kd_Tahun`
                AND `sakip_dpa_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_dpa_program_sasaran`.`Kd_SasaranProgramOPD`
            ) AS sum_program_tw1,
            (SELECT SUM(sakip_dpa_kegiatan.Budget_TW2) FROM `sakip_dpa_kegiatan`
                WHERE `sakip_dpa_kegiatan`.`Kd_Urusan` = `sakip_dpa_program_sasaran`.`Kd_Urusan`
                AND `sakip_dpa_kegiatan`.`Kd_Bidang` = `sakip_dpa_program_sasaran`.`Kd_Bidang`
                AND `sakip_dpa_kegiatan`.`Kd_Unit` = `sakip_dpa_program_sasaran`.`Kd_Unit`
                AND `sakip_dpa_kegiatan`.`Kd_Sub` = `sakip_dpa_program_sasaran`.`Kd_Sub`
                AND `sakip_dpa_kegiatan`.`Kd_Pemda` = `sakip_dpa_program_sasaran`.`Kd_Pemda`
                AND `sakip_dpa_kegiatan`.`Kd_Misi` = `sakip_dpa_program_sasaran`.`Kd_Misi`
                AND `sakip_dpa_kegiatan`.`Kd_Tujuan` = `sakip_dpa_program_sasaran`.`Kd_Tujuan`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_kegiatan`.`Kd_Sasaran` = `sakip_dpa_program_sasaran`.`Kd_Sasaran`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_kegiatan`.`Kd_TujuanOPD` = `sakip_dpa_program_sasaran`.`Kd_TujuanOPD`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_kegiatan`.`Kd_SasaranOPD` = `sakip_dpa_program_sasaran`.`Kd_SasaranOPD`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaranOPD`
                AND `sakip_dpa_kegiatan`.`Kd_ProgramOPD` = `sakip_dpa_program_sasaran`.`Kd_ProgramOPD`
                AND `sakip_dpa_kegiatan`.`Kd_Tahun` = `sakip_dpa_program_sasaran`.`Kd_Tahun`
                AND `sakip_dpa_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_dpa_program_sasaran`.`Kd_SasaranProgramOPD`
            ) AS sum_program_tw2,
            (SELECT SUM(sakip_dpa_kegiatan.Budget_TW3) FROM `sakip_dpa_kegiatan`
                WHERE `sakip_dpa_kegiatan`.`Kd_Urusan` = `sakip_dpa_program_sasaran`.`Kd_Urusan`
                AND `sakip_dpa_kegiatan`.`Kd_Bidang` = `sakip_dpa_program_sasaran`.`Kd_Bidang`
                AND `sakip_dpa_kegiatan`.`Kd_Unit` = `sakip_dpa_program_sasaran`.`Kd_Unit`
                AND `sakip_dpa_kegiatan`.`Kd_Sub` = `sakip_dpa_program_sasaran`.`Kd_Sub`
                AND `sakip_dpa_kegiatan`.`Kd_Pemda` = `sakip_dpa_program_sasaran`.`Kd_Pemda`
                AND `sakip_dpa_kegiatan`.`Kd_Misi` = `sakip_dpa_program_sasaran`.`Kd_Misi`
                AND `sakip_dpa_kegiatan`.`Kd_Tujuan` = `sakip_dpa_program_sasaran`.`Kd_Tujuan`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_kegiatan`.`Kd_Sasaran` = `sakip_dpa_program_sasaran`.`Kd_Sasaran`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_kegiatan`.`Kd_TujuanOPD` = `sakip_dpa_program_sasaran`.`Kd_TujuanOPD`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_kegiatan`.`Kd_SasaranOPD` = `sakip_dpa_program_sasaran`.`Kd_SasaranOPD`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaranOPD`
                AND `sakip_dpa_kegiatan`.`Kd_ProgramOPD` = `sakip_dpa_program_sasaran`.`Kd_ProgramOPD`
                AND `sakip_dpa_kegiatan`.`Kd_Tahun` = `sakip_dpa_program_sasaran`.`Kd_Tahun`
                AND `sakip_dpa_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_dpa_program_sasaran`.`Kd_SasaranProgramOPD`
            ) AS sum_program_tw3,
            (SELECT SUM(sakip_dpa_kegiatan.Budget_TW4) FROM `sakip_dpa_kegiatan`
                WHERE `sakip_dpa_kegiatan`.`Kd_Urusan` = `sakip_dpa_program_sasaran`.`Kd_Urusan`
                AND `sakip_dpa_kegiatan`.`Kd_Bidang` = `sakip_dpa_program_sasaran`.`Kd_Bidang`
                AND `sakip_dpa_kegiatan`.`Kd_Unit` = `sakip_dpa_program_sasaran`.`Kd_Unit`
                AND `sakip_dpa_kegiatan`.`Kd_Sub` = `sakip_dpa_program_sasaran`.`Kd_Sub`
                AND `sakip_dpa_kegiatan`.`Kd_Pemda` = `sakip_dpa_program_sasaran`.`Kd_Pemda`
                AND `sakip_dpa_kegiatan`.`Kd_Misi` = `sakip_dpa_program_sasaran`.`Kd_Misi`
                AND `sakip_dpa_kegiatan`.`Kd_Tujuan` = `sakip_dpa_program_sasaran`.`Kd_Tujuan`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_kegiatan`.`Kd_Sasaran` = `sakip_dpa_program_sasaran`.`Kd_Sasaran`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_kegiatan`.`Kd_TujuanOPD` = `sakip_dpa_program_sasaran`.`Kd_TujuanOPD`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_kegiatan`.`Kd_SasaranOPD` = `sakip_dpa_program_sasaran`.`Kd_SasaranOPD`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaranOPD`
                AND `sakip_dpa_kegiatan`.`Kd_ProgramOPD` = `sakip_dpa_program_sasaran`.`Kd_ProgramOPD`
                AND `sakip_dpa_kegiatan`.`Kd_Tahun` = `sakip_dpa_program_sasaran`.`Kd_Tahun`
                AND `sakip_dpa_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_dpa_program_sasaran`.`Kd_SasaranProgramOPD`
            ) AS sum_program_tw4,
            /*(SELECT COUNT(*) FROM `sakip_dpa_kegiatan`
                left join `sakip_dpa_kegiatan_indikator` on
                  `sakip_dpa_kegiatan_indikator`.`Kd_Urusan` = `sakip_dpa_kegiatan`.`Kd_Urusan`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Bidang` = `sakip_dpa_kegiatan`.`Kd_Bidang`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Unit` = `sakip_dpa_kegiatan`.`Kd_Unit`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Sub` = `sakip_dpa_kegiatan`.`Kd_Sub`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Bid` = `sakip_dpa_kegiatan`.`Kd_Bid`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Subbid` = `sakip_dpa_kegiatan`.`Kd_Subbid`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Pemda` = `sakip_dpa_kegiatan`.`Kd_Pemda`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Misi` = `sakip_dpa_kegiatan`.`Kd_Misi`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Tujuan` = `sakip_dpa_kegiatan`.`Kd_Tujuan`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorTujuan` = `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Sasaran` = `sakip_dpa_kegiatan`.`Kd_Sasaran`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorSasaran` = `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_TujuanOPD` = `sakip_dpa_kegiatan`.`Kd_TujuanOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_SasaranOPD` = `sakip_dpa_kegiatan`.`Kd_SasaranOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_ProgramOPD` = `sakip_dpa_kegiatan`.`Kd_ProgramOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Tahun` = `sakip_dpa_kegiatan`.`Kd_Tahun`
                WHERE `sakip_dpa_kegiatan`.`Kd_Urusan` = `sakip_dpa_program`.`Kd_Urusan`
                AND `sakip_dpa_kegiatan`.`Kd_Bidang` = `sakip_dpa_program`.`Kd_Bidang`
                AND `sakip_dpa_kegiatan`.`Kd_Unit` = `sakip_dpa_program`.`Kd_Unit`
                AND `sakip_dpa_kegiatan`.`Kd_Sub` = `sakip_dpa_program`.`Kd_Sub`
                AND `sakip_dpa_kegiatan`.`Kd_Pemda` = `sakip_dpa_program`.`Kd_Pemda`
                AND `sakip_dpa_kegiatan`.`Kd_Misi` = `sakip_dpa_program`.`Kd_Misi`
                AND `sakip_dpa_kegiatan`.`Kd_Tujuan` = `sakip_dpa_program`.`Kd_Tujuan`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_dpa_program`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_kegiatan`.`Kd_Sasaran` = `sakip_dpa_program`.`Kd_Sasaran`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_dpa_program`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_kegiatan`.`Kd_TujuanOPD` = `sakip_dpa_program`.`Kd_TujuanOPD`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_program`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_kegiatan`.`Kd_SasaranOPD` = `sakip_dpa_program`.`Kd_SasaranOPD`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_program`.`Kd_IndikatorSasaranOPD`
                AND `sakip_dpa_kegiatan`.`Kd_ProgramOPD` = `sakip_dpa_program`.`Kd_ProgramOPD`
                AND `sakip_dpa_kegiatan`.`Kd_Tahun` = `sakip_dpa_program`.`Kd_Tahun`
            ) AS count_program_span,*/
            (SELECT COUNT(*) FROM `sakip_dpa_program_sasaran`
                left join `sakip_dpa_kegiatan` on
                  `sakip_dpa_kegiatan`.`Kd_Urusan` = `sakip_dpa_program_sasaran`.`Kd_Urusan`
                  AND `sakip_dpa_kegiatan`.`Kd_Bidang` = `sakip_dpa_program_sasaran`.`Kd_Bidang`
                  AND `sakip_dpa_kegiatan`.`Kd_Unit` = `sakip_dpa_program_sasaran`.`Kd_Unit`
                  AND `sakip_dpa_kegiatan`.`Kd_Sub` = `sakip_dpa_program_sasaran`.`Kd_Sub`
                  AND `sakip_dpa_kegiatan`.`Kd_Bid` = `sakip_dpa_program_sasaran`.`Kd_Bid`
                  AND `sakip_dpa_kegiatan`.`Kd_Pemda` = `sakip_dpa_program_sasaran`.`Kd_Pemda`
                  AND `sakip_dpa_kegiatan`.`Kd_Misi` = `sakip_dpa_program_sasaran`.`Kd_Misi`
                  AND `sakip_dpa_kegiatan`.`Kd_Tujuan` = `sakip_dpa_program_sasaran`.`Kd_Tujuan`
                  AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuan`
                  AND `sakip_dpa_kegiatan`.`Kd_Sasaran` = `sakip_dpa_program_sasaran`.`Kd_Sasaran`
                  AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaran`
                  AND `sakip_dpa_kegiatan`.`Kd_TujuanOPD` = `sakip_dpa_program_sasaran`.`Kd_TujuanOPD`
                  AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuanOPD`
                  AND `sakip_dpa_kegiatan`.`Kd_SasaranOPD` = `sakip_dpa_program_sasaran`.`Kd_SasaranOPD`
                  AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaranOPD`
                  AND `sakip_dpa_kegiatan`.`Kd_ProgramOPD` = `sakip_dpa_program_sasaran`.`Kd_ProgramOPD`
                  AND `sakip_dpa_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_dpa_program_sasaran`.`Kd_SasaranProgramOPD`
                  AND `sakip_dpa_kegiatan`.`Kd_Tahun` = `sakip_dpa_program_sasaran`.`Kd_Tahun`
                left join `sakip_dpa_kegiatan_indikator` on
                  `sakip_dpa_kegiatan_indikator`.`Kd_Urusan` = `sakip_dpa_kegiatan`.`Kd_Urusan`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Bidang` = `sakip_dpa_kegiatan`.`Kd_Bidang`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Unit` = `sakip_dpa_kegiatan`.`Kd_Unit`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Sub` = `sakip_dpa_kegiatan`.`Kd_Sub`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Bid` = `sakip_dpa_kegiatan`.`Kd_Bid`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Subbid` = `sakip_dpa_kegiatan`.`Kd_Subbid`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Pemda` = `sakip_dpa_kegiatan`.`Kd_Pemda`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Misi` = `sakip_dpa_kegiatan`.`Kd_Misi`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Tujuan` = `sakip_dpa_kegiatan`.`Kd_Tujuan`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorTujuan` = `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Sasaran` = `sakip_dpa_kegiatan`.`Kd_Sasaran`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorSasaran` = `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_TujuanOPD` = `sakip_dpa_kegiatan`.`Kd_TujuanOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_SasaranOPD` = `sakip_dpa_kegiatan`.`Kd_SasaranOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_ProgramOPD` = `sakip_dpa_kegiatan`.`Kd_ProgramOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_SasaranProgramOPD` = `sakip_dpa_kegiatan`.`Kd_SasaranProgramOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_KegiatanOPD` = `sakip_dpa_kegiatan`.`Kd_KegiatanOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Tahun` = `sakip_dpa_kegiatan`.`Kd_Tahun`
                WHERE `sakip_dpa_program_sasaran`.`Kd_Urusan` = `sakip_dpa_program`.`Kd_Urusan`
                AND `sakip_dpa_program_sasaran`.`Kd_Bidang` = `sakip_dpa_program`.`Kd_Bidang`
                AND `sakip_dpa_program_sasaran`.`Kd_Unit` = `sakip_dpa_program`.`Kd_Unit`
                AND `sakip_dpa_program_sasaran`.`Kd_Sub` = `sakip_dpa_program`.`Kd_Sub`
                AND `sakip_dpa_program_sasaran`.`Kd_Pemda` = `sakip_dpa_program`.`Kd_Pemda`
                AND `sakip_dpa_program_sasaran`.`Kd_Misi` = `sakip_dpa_program`.`Kd_Misi`
                AND `sakip_dpa_program_sasaran`.`Kd_Tujuan` = `sakip_dpa_program`.`Kd_Tujuan`
                AND `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuan` = `sakip_dpa_program`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_program_sasaran`.`Kd_Sasaran` = `sakip_dpa_program`.`Kd_Sasaran`
                AND `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaran` = `sakip_dpa_program`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_program_sasaran`.`Kd_TujuanOPD` = `sakip_dpa_program`.`Kd_TujuanOPD`
                AND `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_program`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_program_sasaran`.`Kd_SasaranOPD` = `sakip_dpa_program`.`Kd_SasaranOPD`
                AND `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_program`.`Kd_IndikatorSasaranOPD`
                /*AND `sakip_dpa_program_sasaran`.`Kd_Bid` = `sakip_dpa_program`.`Kd_Bid`*/
                AND `sakip_dpa_program_sasaran`.`Kd_ProgramOPD` = `sakip_dpa_program`.`Kd_ProgramOPD`
                AND `sakip_dpa_program_sasaran`.`Kd_Tahun` = `sakip_dpa_program`.`Kd_Tahun`
            ) AS count_program_span,
            (SELECT COUNT(*) FROM `sakip_dpa_kegiatan`
                left join `sakip_dpa_kegiatan_indikator` on
                  `sakip_dpa_kegiatan_indikator`.`Kd_Urusan` = `sakip_dpa_kegiatan`.`Kd_Urusan`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Bidang` = `sakip_dpa_kegiatan`.`Kd_Bidang`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Unit` = `sakip_dpa_kegiatan`.`Kd_Unit`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Sub` = `sakip_dpa_kegiatan`.`Kd_Sub`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Bid` = `sakip_dpa_kegiatan`.`Kd_Bid`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Subbid` = `sakip_dpa_kegiatan`.`Kd_Subbid`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Pemda` = `sakip_dpa_kegiatan`.`Kd_Pemda`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Misi` = `sakip_dpa_kegiatan`.`Kd_Misi`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Tujuan` = `sakip_dpa_kegiatan`.`Kd_Tujuan`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorTujuan` = `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Sasaran` = `sakip_dpa_kegiatan`.`Kd_Sasaran`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorSasaran` = `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_TujuanOPD` = `sakip_dpa_kegiatan`.`Kd_TujuanOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_SasaranOPD` = `sakip_dpa_kegiatan`.`Kd_SasaranOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_ProgramOPD` = `sakip_dpa_kegiatan`.`Kd_ProgramOPD`
                  /*AND `sakip_dpa_kegiatan_indikator`.`Kd_SasaranProgramOPD` = `sakip_dpa_kegiatan`.`Kd_SasaranProgramOPD`*/
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Tahun` = `sakip_dpa_kegiatan`.`Kd_Tahun`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_KegiatanOPD` = `sakip_dpa_kegiatan`.`Kd_KegiatanOPD`
                WHERE `sakip_dpa_kegiatan`.`Kd_Urusan` = `sakip_dpa_program`.`Kd_Urusan`
                AND `sakip_dpa_kegiatan`.`Kd_Bidang` = `sakip_dpa_program`.`Kd_Bidang`
                AND `sakip_dpa_kegiatan`.`Kd_Unit` = `sakip_dpa_program`.`Kd_Unit`
                AND `sakip_dpa_kegiatan`.`Kd_Sub` = `sakip_dpa_program`.`Kd_Sub`
                AND `sakip_dpa_kegiatan`.`Kd_Pemda` = `sakip_dpa_program`.`Kd_Pemda`
                AND `sakip_dpa_kegiatan`.`Kd_Misi` = `sakip_dpa_program`.`Kd_Misi`
                AND `sakip_dpa_kegiatan`.`Kd_Tujuan` = `sakip_dpa_program`.`Kd_Tujuan`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_dpa_program`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_kegiatan`.`Kd_Sasaran` = `sakip_dpa_program`.`Kd_Sasaran`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_dpa_program`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_kegiatan`.`Kd_TujuanOPD` = `sakip_dpa_program`.`Kd_TujuanOPD`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_program`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_kegiatan`.`Kd_SasaranOPD` = `sakip_dpa_program`.`Kd_SasaranOPD`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_program`.`Kd_IndikatorSasaranOPD`
                /*AND `sakip_dpa_kegiatan`.`Kd_Bid` = `sakip_dpa_program`.`Kd_Bid`*/
                AND `sakip_dpa_kegiatan`.`Kd_ProgramOPD` = `sakip_dpa_program`.`Kd_ProgramOPD`
                AND `sakip_dpa_kegiatan`.`Kd_Tahun` = `sakip_dpa_program`.`Kd_Tahun`
                /*AND `sakip_dpa_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_dpa_program_sasaran`.`Kd_SasaranProgramOPD`*/
            ) AS count_sasaranprog_span,
            (SELECT COUNT(*) FROM `sakip_dpa_kegiatan_indikator`
                WHERE `sakip_dpa_kegiatan_indikator`.`Kd_Urusan` = `sakip_dpa_kegiatan`.`Kd_Urusan`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Bidang` = `sakip_dpa_kegiatan`.`Kd_Bidang`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Unit` = `sakip_dpa_kegiatan`.`Kd_Unit`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Sub` = `sakip_dpa_kegiatan`.`Kd_Sub`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Bid` = `sakip_dpa_kegiatan`.`Kd_Bid`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Subbid` = `sakip_dpa_kegiatan`.`Kd_Subbid`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Pemda` = `sakip_dpa_kegiatan`.`Kd_Pemda`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Misi` = `sakip_dpa_kegiatan`.`Kd_Misi`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Tujuan` = `sakip_dpa_kegiatan`.`Kd_Tujuan`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorTujuan` = `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Sasaran` = `sakip_dpa_kegiatan`.`Kd_Sasaran`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorSasaran` = `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_TujuanOPD` = `sakip_dpa_kegiatan`.`Kd_TujuanOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_SasaranOPD` = `sakip_dpa_kegiatan`.`Kd_SasaranOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_ProgramOPD` = `sakip_dpa_kegiatan`.`Kd_ProgramOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Tahun` = `sakip_dpa_kegiatan`.`Kd_Tahun`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_SasaranProgramOPD` = `sakip_dpa_kegiatan`.`Kd_SasaranProgramOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_KegiatanOPD` = `sakip_dpa_kegiatan`.`Kd_KegiatanOPD`
            ) AS count_kegiatan_span
            FROM `sakip_dpa_program`
            LEFT JOIN `sakip_dpa_program_sasaran`
                ON `sakip_dpa_program_sasaran`.`Kd_Urusan` = `sakip_dpa_program`.`Kd_Urusan`
                AND `sakip_dpa_program_sasaran`.`Kd_Bidang` = `sakip_dpa_program`.`Kd_Bidang`
                AND `sakip_dpa_program_sasaran`.`Kd_Unit` = `sakip_dpa_program`.`Kd_Unit`
                AND `sakip_dpa_program_sasaran`.`Kd_Sub` = `sakip_dpa_program`.`Kd_Sub`
                AND `sakip_dpa_program_sasaran`.`Kd_Bid` = `sakip_dpa_program`.`Kd_Bid`
                AND `sakip_dpa_program_sasaran`.`Kd_Pemda` = `sakip_dpa_program`.`Kd_Pemda`
                AND `sakip_dpa_program_sasaran`.`Kd_Misi` = `sakip_dpa_program`.`Kd_Misi`
                AND `sakip_dpa_program_sasaran`.`Kd_Tujuan` = `sakip_dpa_program`.`Kd_Tujuan`
                AND `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuan` = `sakip_dpa_program`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_program_sasaran`.`Kd_Sasaran` = `sakip_dpa_program`.`Kd_Sasaran`
                AND `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaran` = `sakip_dpa_program`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_program_sasaran`.`Kd_TujuanOPD` = `sakip_dpa_program`.`Kd_TujuanOPD`
                AND `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_program`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_program_sasaran`.`Kd_SasaranOPD` = `sakip_dpa_program`.`Kd_SasaranOPD`
                AND `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_program`.`Kd_IndikatorSasaranOPD`
                AND `sakip_dpa_program_sasaran`.`Kd_ProgramOPD` = `sakip_dpa_program`.`Kd_ProgramOPD`
                AND `sakip_dpa_program_sasaran`.`Kd_Tahun` = `sakip_dpa_program`.`Kd_Tahun`
            LEFT JOIN `sakip_dpa_kegiatan`
                ON `sakip_dpa_kegiatan`.`Kd_Urusan` = `sakip_dpa_program_sasaran`.`Kd_Urusan`
                AND `sakip_dpa_kegiatan`.`Kd_Bidang` = `sakip_dpa_program_sasaran`.`Kd_Bidang`
                AND `sakip_dpa_kegiatan`.`Kd_Unit` = `sakip_dpa_program_sasaran`.`Kd_Unit`
                AND `sakip_dpa_kegiatan`.`Kd_Sub` = `sakip_dpa_program_sasaran`.`Kd_Sub`
                AND `sakip_dpa_kegiatan`.`Kd_Bid` = `sakip_dpa_program_sasaran`.`Kd_Bid`
                AND `sakip_dpa_kegiatan`.`Kd_Pemda` = `sakip_dpa_program_sasaran`.`Kd_Pemda`
                AND `sakip_dpa_kegiatan`.`Kd_Misi` = `sakip_dpa_program_sasaran`.`Kd_Misi`
                AND `sakip_dpa_kegiatan`.`Kd_Tujuan` = `sakip_dpa_program_sasaran`.`Kd_Tujuan`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_kegiatan`.`Kd_Sasaran` = `sakip_dpa_program_sasaran`.`Kd_Sasaran`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_kegiatan`.`Kd_TujuanOPD` = `sakip_dpa_program_sasaran`.`Kd_TujuanOPD`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_kegiatan`.`Kd_SasaranOPD` = `sakip_dpa_program_sasaran`.`Kd_SasaranOPD`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaranOPD`
                AND `sakip_dpa_kegiatan`.`Kd_ProgramOPD` = `sakip_dpa_program_sasaran`.`Kd_ProgramOPD`
                AND `sakip_dpa_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_dpa_program_sasaran`.`Kd_SasaranProgramOPD`
                AND `sakip_dpa_kegiatan`.`Kd_Tahun` = `sakip_dpa_program_sasaran`.`Kd_Tahun`
            LEFT JOIN `sakip_dpa_kegiatan_indikator`
                ON `sakip_dpa_kegiatan_indikator`.`Kd_Urusan` = `sakip_dpa_kegiatan`.`Kd_Urusan`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Bidang` = `sakip_dpa_kegiatan`.`Kd_Bidang`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Unit` = `sakip_dpa_kegiatan`.`Kd_Unit`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Sub` = `sakip_dpa_kegiatan`.`Kd_Sub`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Bid` = `sakip_dpa_kegiatan`.`Kd_Bid`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Subbid` = `sakip_dpa_kegiatan`.`Kd_Subbid`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Pemda` = `sakip_dpa_kegiatan`.`Kd_Pemda`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Misi` = `sakip_dpa_kegiatan`.`Kd_Misi`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Tujuan` = `sakip_dpa_kegiatan`.`Kd_Tujuan`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorTujuan` = `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Sasaran` = `sakip_dpa_kegiatan`.`Kd_Sasaran`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorSasaran` = `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_TujuanOPD` = `sakip_dpa_kegiatan`.`Kd_TujuanOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_SasaranOPD` = `sakip_dpa_kegiatan`.`Kd_SasaranOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_ProgramOPD` = `sakip_dpa_kegiatan`.`Kd_ProgramOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_SasaranProgramOPD` = `sakip_dpa_kegiatan`.`Kd_SasaranProgramOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_KegiatanOPD` = `sakip_dpa_kegiatan`.`Kd_KegiatanOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Tahun` = `sakip_dpa_kegiatan`.`Kd_Tahun`
            WHERE
                `sakip_dpa_program`.`Kd_Tahun` = ?
                AND `sakip_dpa_program`.`Kd_Urusan` = ?
                AND `sakip_dpa_program`.`Kd_Bidang` = ?
                AND `sakip_dpa_program`.`Kd_Unit` = ?
                AND `sakip_dpa_program`.`Kd_Sub` = ?
            /*group by
              `sakip_dpa_program`.`Kd_ProgramOPD`,
              `sakip_dpa_program_sasaran`.`Kd_SasaranProgramOPD`,
              `sakip_dpa_kegiatan`.`Kd_KegiatanOPD`,
              `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorKegiatanOPD`*/
            ORDER BY
              sakip_dpa_program.Kd_Urusan,
              sakip_dpa_program.Kd_Bidang,
              sakip_dpa_program.Kd_Unit,
              sakip_dpa_program.Kd_Sub,
              sakip_dpa_program.Kd_Pemda,
              sakip_dpa_program.Kd_Misi,
              sakip_dpa_program.Kd_Tujuan,
              sakip_dpa_program.Kd_IndikatorTujuan,
              sakip_dpa_program.Kd_Sasaran,
              sakip_dpa_program.Kd_IndikatorSasaran,
              sakip_dpa_program.Kd_TujuanOPD,
              sakip_dpa_program.Kd_IndikatorTujuanOPD,
              sakip_dpa_program.Kd_SasaranOPD,
              sakip_dpa_program.Kd_IndikatorSasaranOPD,
              sakip_dpa_program.Kd_ProgramOPD,
              sakip_dpa_program_sasaran.Kd_SasaranProgramOPD,

              sakip_dpa_kegiatan.Kd_KegiatanOPD,
              sakip_dpa_kegiatan_indikator.Kd_IndikatorKegiatanOPD
              /*sakip_dpa_program.Kd_Bid,
              sakip_dpa_kegiatan.Kd_Subbid*/
            ";

            if(!empty($SumberData) && $SumberData == 'PERUBAHAN') {
              $q = @"
              SELECT
            sakip_pdpa_program.*,
            sakip_pdpa_program.Kd_Tahun,
            sakip_pdpa_program.Nm_ProgramOPD,
            sakip_pdpa_program_sasaran.Kd_SasaranProgramOPD,
            sakip_pdpa_program_sasaran.Kd_Satuan AS Satuan1,
            sakip_pdpa_program_sasaran.Target AS Target1,
            sakip_pdpa_program_sasaran.Target_TW1 AS Program_TW1,
            sakip_pdpa_program_sasaran.Target_TW2 AS Program_TW2,
            sakip_pdpa_program_sasaran.Target_TW3 AS Program_TW3,
            sakip_pdpa_program_sasaran.Target_TW4 AS Program_TW4,
            sakip_pdpa_kegiatan.Kd_Subbid,
            sakip_pdpa_kegiatan.Kd_KegiatanOPD,
            sakip_pdpa_kegiatan.Nm_KegiatanOPD,
            sakip_pdpa_kegiatan_indikator.Kd_SasaranKegiatanOPD,
            sakip_pdpa_kegiatan_indikator.Kd_IndikatorKegiatanOPD,
            sakip_pdpa_kegiatan_indikator.Nm_IndikatorKegiatanOPD,
            sakip_pdpa_kegiatan_indikator.Kd_Satuan AS Satuan2,
            sakip_pdpa_kegiatan_indikator.Target AS Target2,
            sakip_pdpa_kegiatan_indikator.Target_TW1 AS Kegiatan_TW1,
            sakip_pdpa_kegiatan_indikator.Target_TW2 AS Kegiatan_TW2,
            sakip_pdpa_kegiatan_indikator.Target_TW3 AS Kegiatan_TW3,
            sakip_pdpa_kegiatan_indikator.Target_TW4 AS Kegiatan_TW4,
            sakip_pdpa_kegiatan.Budget_TW1 AS B_Kegiatan_TW1,
            sakip_pdpa_kegiatan.Budget_TW2 AS B_Kegiatan_TW2,
            sakip_pdpa_kegiatan.Budget_TW3 AS B_Kegiatan_TW3,
            sakip_pdpa_kegiatan.Budget_TW4 AS B_Kegiatan_TW4,
            sakip_pdpa_kegiatan.Total AS Total2,
            (SELECT SUM(sakip_pdpa_kegiatan.Total) FROM `sakip_pdpa_kegiatan`
                WHERE `sakip_pdpa_kegiatan`.`Kd_Urusan` = `sakip_pdpa_program`.`Kd_Urusan`
                AND `sakip_pdpa_kegiatan`.`Kd_Bidang` = `sakip_pdpa_program`.`Kd_Bidang`
                AND `sakip_pdpa_kegiatan`.`Kd_Unit` = `sakip_pdpa_program`.`Kd_Unit`
                AND `sakip_pdpa_kegiatan`.`Kd_Sub` = `sakip_pdpa_program`.`Kd_Sub`
                AND `sakip_pdpa_kegiatan`.`Kd_Pemda` = `sakip_pdpa_program`.`Kd_Pemda`
                AND `sakip_pdpa_kegiatan`.`Kd_Misi` = `sakip_pdpa_program`.`Kd_Misi`
                AND `sakip_pdpa_kegiatan`.`Kd_Tujuan` = `sakip_pdpa_program`.`Kd_Tujuan`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_pdpa_program`.`Kd_IndikatorTujuan`
                AND `sakip_pdpa_kegiatan`.`Kd_Sasaran` = `sakip_pdpa_program`.`Kd_Sasaran`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_pdpa_program`.`Kd_IndikatorSasaran`
                AND `sakip_pdpa_kegiatan`.`Kd_TujuanOPD` = `sakip_pdpa_program`.`Kd_TujuanOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_program`.`Kd_IndikatorTujuanOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_SasaranOPD` = `sakip_pdpa_program`.`Kd_SasaranOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_program`.`Kd_IndikatorSasaranOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_ProgramOPD` = `sakip_pdpa_program`.`Kd_ProgramOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_Tahun` = `sakip_pdpa_program`.`Kd_Tahun`
            ) AS sum_program_total,
            (SELECT SUM(sakip_pdpa_kegiatan.Budget_TW1) FROM `sakip_pdpa_kegiatan`
                WHERE `sakip_pdpa_kegiatan`.`Kd_Urusan` = `sakip_pdpa_program_sasaran`.`Kd_Urusan`
                AND `sakip_pdpa_kegiatan`.`Kd_Bidang` = `sakip_pdpa_program_sasaran`.`Kd_Bidang`
                AND `sakip_pdpa_kegiatan`.`Kd_Unit` = `sakip_pdpa_program_sasaran`.`Kd_Unit`
                AND `sakip_pdpa_kegiatan`.`Kd_Sub` = `sakip_pdpa_program_sasaran`.`Kd_Sub`
                AND `sakip_pdpa_kegiatan`.`Kd_Pemda` = `sakip_pdpa_program_sasaran`.`Kd_Pemda`
                AND `sakip_pdpa_kegiatan`.`Kd_Misi` = `sakip_pdpa_program_sasaran`.`Kd_Misi`
                AND `sakip_pdpa_kegiatan`.`Kd_Tujuan` = `sakip_pdpa_program_sasaran`.`Kd_Tujuan`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorTujuan`
                AND `sakip_pdpa_kegiatan`.`Kd_Sasaran` = `sakip_pdpa_program_sasaran`.`Kd_Sasaran`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorSasaran`
                AND `sakip_pdpa_kegiatan`.`Kd_TujuanOPD` = `sakip_pdpa_program_sasaran`.`Kd_TujuanOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorTujuanOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_SasaranOPD` = `sakip_pdpa_program_sasaran`.`Kd_SasaranOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorSasaranOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_ProgramOPD` = `sakip_pdpa_program_sasaran`.`Kd_ProgramOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_Tahun` = `sakip_pdpa_program_sasaran`.`Kd_Tahun`
                AND `sakip_pdpa_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_pdpa_program_sasaran`.`Kd_SasaranProgramOPD`
            ) AS sum_program_tw1,
            (SELECT SUM(sakip_pdpa_kegiatan.Budget_TW2) FROM `sakip_pdpa_kegiatan`
                WHERE `sakip_pdpa_kegiatan`.`Kd_Urusan` = `sakip_pdpa_program_sasaran`.`Kd_Urusan`
                AND `sakip_pdpa_kegiatan`.`Kd_Bidang` = `sakip_pdpa_program_sasaran`.`Kd_Bidang`
                AND `sakip_pdpa_kegiatan`.`Kd_Unit` = `sakip_pdpa_program_sasaran`.`Kd_Unit`
                AND `sakip_pdpa_kegiatan`.`Kd_Sub` = `sakip_pdpa_program_sasaran`.`Kd_Sub`
                AND `sakip_pdpa_kegiatan`.`Kd_Pemda` = `sakip_pdpa_program_sasaran`.`Kd_Pemda`
                AND `sakip_pdpa_kegiatan`.`Kd_Misi` = `sakip_pdpa_program_sasaran`.`Kd_Misi`
                AND `sakip_pdpa_kegiatan`.`Kd_Tujuan` = `sakip_pdpa_program_sasaran`.`Kd_Tujuan`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorTujuan`
                AND `sakip_pdpa_kegiatan`.`Kd_Sasaran` = `sakip_pdpa_program_sasaran`.`Kd_Sasaran`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorSasaran`
                AND `sakip_pdpa_kegiatan`.`Kd_TujuanOPD` = `sakip_pdpa_program_sasaran`.`Kd_TujuanOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorTujuanOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_SasaranOPD` = `sakip_pdpa_program_sasaran`.`Kd_SasaranOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorSasaranOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_ProgramOPD` = `sakip_pdpa_program_sasaran`.`Kd_ProgramOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_Tahun` = `sakip_pdpa_program_sasaran`.`Kd_Tahun`
                AND `sakip_pdpa_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_pdpa_program_sasaran`.`Kd_SasaranProgramOPD`
            ) AS sum_program_tw2,
            (SELECT SUM(sakip_pdpa_kegiatan.Budget_TW3) FROM `sakip_pdpa_kegiatan`
                WHERE `sakip_pdpa_kegiatan`.`Kd_Urusan` = `sakip_pdpa_program_sasaran`.`Kd_Urusan`
                AND `sakip_pdpa_kegiatan`.`Kd_Bidang` = `sakip_pdpa_program_sasaran`.`Kd_Bidang`
                AND `sakip_pdpa_kegiatan`.`Kd_Unit` = `sakip_pdpa_program_sasaran`.`Kd_Unit`
                AND `sakip_pdpa_kegiatan`.`Kd_Sub` = `sakip_pdpa_program_sasaran`.`Kd_Sub`
                AND `sakip_pdpa_kegiatan`.`Kd_Pemda` = `sakip_pdpa_program_sasaran`.`Kd_Pemda`
                AND `sakip_pdpa_kegiatan`.`Kd_Misi` = `sakip_pdpa_program_sasaran`.`Kd_Misi`
                AND `sakip_pdpa_kegiatan`.`Kd_Tujuan` = `sakip_pdpa_program_sasaran`.`Kd_Tujuan`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorTujuan`
                AND `sakip_pdpa_kegiatan`.`Kd_Sasaran` = `sakip_pdpa_program_sasaran`.`Kd_Sasaran`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorSasaran`
                AND `sakip_pdpa_kegiatan`.`Kd_TujuanOPD` = `sakip_pdpa_program_sasaran`.`Kd_TujuanOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorTujuanOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_SasaranOPD` = `sakip_pdpa_program_sasaran`.`Kd_SasaranOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorSasaranOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_ProgramOPD` = `sakip_pdpa_program_sasaran`.`Kd_ProgramOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_Tahun` = `sakip_pdpa_program_sasaran`.`Kd_Tahun`
                AND `sakip_pdpa_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_pdpa_program_sasaran`.`Kd_SasaranProgramOPD`
            ) AS sum_program_tw3,
            (SELECT SUM(sakip_pdpa_kegiatan.Budget_TW4) FROM `sakip_pdpa_kegiatan`
                WHERE `sakip_pdpa_kegiatan`.`Kd_Urusan` = `sakip_pdpa_program_sasaran`.`Kd_Urusan`
                AND `sakip_pdpa_kegiatan`.`Kd_Bidang` = `sakip_pdpa_program_sasaran`.`Kd_Bidang`
                AND `sakip_pdpa_kegiatan`.`Kd_Unit` = `sakip_pdpa_program_sasaran`.`Kd_Unit`
                AND `sakip_pdpa_kegiatan`.`Kd_Sub` = `sakip_pdpa_program_sasaran`.`Kd_Sub`
                AND `sakip_pdpa_kegiatan`.`Kd_Pemda` = `sakip_pdpa_program_sasaran`.`Kd_Pemda`
                AND `sakip_pdpa_kegiatan`.`Kd_Misi` = `sakip_pdpa_program_sasaran`.`Kd_Misi`
                AND `sakip_pdpa_kegiatan`.`Kd_Tujuan` = `sakip_pdpa_program_sasaran`.`Kd_Tujuan`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorTujuan`
                AND `sakip_pdpa_kegiatan`.`Kd_Sasaran` = `sakip_pdpa_program_sasaran`.`Kd_Sasaran`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorSasaran`
                AND `sakip_pdpa_kegiatan`.`Kd_TujuanOPD` = `sakip_pdpa_program_sasaran`.`Kd_TujuanOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorTujuanOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_SasaranOPD` = `sakip_pdpa_program_sasaran`.`Kd_SasaranOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorSasaranOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_ProgramOPD` = `sakip_pdpa_program_sasaran`.`Kd_ProgramOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_Tahun` = `sakip_pdpa_program_sasaran`.`Kd_Tahun`
                AND `sakip_pdpa_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_pdpa_program_sasaran`.`Kd_SasaranProgramOPD`
            ) AS sum_program_tw4,
            /*(SELECT COUNT(*) FROM `sakip_pdpa_kegiatan`
                left join `sakip_pdpa_kegiatan_indikator` on
                  `sakip_pdpa_kegiatan_indikator`.`Kd_Urusan` = `sakip_pdpa_kegiatan`.`Kd_Urusan`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Bidang` = `sakip_pdpa_kegiatan`.`Kd_Bidang`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Unit` = `sakip_pdpa_kegiatan`.`Kd_Unit`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Sub` = `sakip_pdpa_kegiatan`.`Kd_Sub`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Bid` = `sakip_pdpa_kegiatan`.`Kd_Bid`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Subbid` = `sakip_pdpa_kegiatan`.`Kd_Subbid`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Pemda` = `sakip_pdpa_kegiatan`.`Kd_Pemda`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Misi` = `sakip_pdpa_kegiatan`.`Kd_Misi`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Tujuan` = `sakip_pdpa_kegiatan`.`Kd_Tujuan`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorTujuan` = `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuan`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Sasaran` = `sakip_pdpa_kegiatan`.`Kd_Sasaran`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorSasaran` = `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaran`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_TujuanOPD` = `sakip_pdpa_kegiatan`.`Kd_TujuanOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuanOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_SasaranOPD` = `sakip_pdpa_kegiatan`.`Kd_SasaranOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaranOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_ProgramOPD` = `sakip_pdpa_kegiatan`.`Kd_ProgramOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Tahun` = `sakip_pdpa_kegiatan`.`Kd_Tahun`
                WHERE `sakip_pdpa_kegiatan`.`Kd_Urusan` = `sakip_pdpa_program`.`Kd_Urusan`
                AND `sakip_pdpa_kegiatan`.`Kd_Bidang` = `sakip_pdpa_program`.`Kd_Bidang`
                AND `sakip_pdpa_kegiatan`.`Kd_Unit` = `sakip_pdpa_program`.`Kd_Unit`
                AND `sakip_pdpa_kegiatan`.`Kd_Sub` = `sakip_pdpa_program`.`Kd_Sub`
                AND `sakip_pdpa_kegiatan`.`Kd_Pemda` = `sakip_pdpa_program`.`Kd_Pemda`
                AND `sakip_pdpa_kegiatan`.`Kd_Misi` = `sakip_pdpa_program`.`Kd_Misi`
                AND `sakip_pdpa_kegiatan`.`Kd_Tujuan` = `sakip_pdpa_program`.`Kd_Tujuan`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_pdpa_program`.`Kd_IndikatorTujuan`
                AND `sakip_pdpa_kegiatan`.`Kd_Sasaran` = `sakip_pdpa_program`.`Kd_Sasaran`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_pdpa_program`.`Kd_IndikatorSasaran`
                AND `sakip_pdpa_kegiatan`.`Kd_TujuanOPD` = `sakip_pdpa_program`.`Kd_TujuanOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_program`.`Kd_IndikatorTujuanOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_SasaranOPD` = `sakip_pdpa_program`.`Kd_SasaranOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_program`.`Kd_IndikatorSasaranOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_ProgramOPD` = `sakip_pdpa_program`.`Kd_ProgramOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_Tahun` = `sakip_pdpa_program`.`Kd_Tahun`
            ) AS count_program_span,*/
            (SELECT COUNT(*) FROM `sakip_pdpa_program_sasaran`
                left join `sakip_pdpa_kegiatan` on
                  `sakip_pdpa_kegiatan`.`Kd_Urusan` = `sakip_pdpa_program_sasaran`.`Kd_Urusan`
                  AND `sakip_pdpa_kegiatan`.`Kd_Bidang` = `sakip_pdpa_program_sasaran`.`Kd_Bidang`
                  AND `sakip_pdpa_kegiatan`.`Kd_Unit` = `sakip_pdpa_program_sasaran`.`Kd_Unit`
                  AND `sakip_pdpa_kegiatan`.`Kd_Sub` = `sakip_pdpa_program_sasaran`.`Kd_Sub`
                  AND `sakip_pdpa_kegiatan`.`Kd_Bid` = `sakip_pdpa_program_sasaran`.`Kd_Bid`
                  AND `sakip_pdpa_kegiatan`.`Kd_Pemda` = `sakip_pdpa_program_sasaran`.`Kd_Pemda`
                  AND `sakip_pdpa_kegiatan`.`Kd_Misi` = `sakip_pdpa_program_sasaran`.`Kd_Misi`
                  AND `sakip_pdpa_kegiatan`.`Kd_Tujuan` = `sakip_pdpa_program_sasaran`.`Kd_Tujuan`
                  AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorTujuan`
                  AND `sakip_pdpa_kegiatan`.`Kd_Sasaran` = `sakip_pdpa_program_sasaran`.`Kd_Sasaran`
                  AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorSasaran`
                  AND `sakip_pdpa_kegiatan`.`Kd_TujuanOPD` = `sakip_pdpa_program_sasaran`.`Kd_TujuanOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorTujuanOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_SasaranOPD` = `sakip_pdpa_program_sasaran`.`Kd_SasaranOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorSasaranOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_ProgramOPD` = `sakip_pdpa_program_sasaran`.`Kd_ProgramOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_pdpa_program_sasaran`.`Kd_SasaranProgramOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_Tahun` = `sakip_pdpa_program_sasaran`.`Kd_Tahun`
                left join `sakip_pdpa_kegiatan_indikator` on
                  `sakip_pdpa_kegiatan_indikator`.`Kd_Urusan` = `sakip_pdpa_kegiatan`.`Kd_Urusan`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Bidang` = `sakip_pdpa_kegiatan`.`Kd_Bidang`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Unit` = `sakip_pdpa_kegiatan`.`Kd_Unit`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Sub` = `sakip_pdpa_kegiatan`.`Kd_Sub`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Bid` = `sakip_pdpa_kegiatan`.`Kd_Bid`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Subbid` = `sakip_pdpa_kegiatan`.`Kd_Subbid`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Pemda` = `sakip_pdpa_kegiatan`.`Kd_Pemda`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Misi` = `sakip_pdpa_kegiatan`.`Kd_Misi`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Tujuan` = `sakip_pdpa_kegiatan`.`Kd_Tujuan`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorTujuan` = `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuan`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Sasaran` = `sakip_pdpa_kegiatan`.`Kd_Sasaran`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorSasaran` = `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaran`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_TujuanOPD` = `sakip_pdpa_kegiatan`.`Kd_TujuanOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuanOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_SasaranOPD` = `sakip_pdpa_kegiatan`.`Kd_SasaranOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaranOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_ProgramOPD` = `sakip_pdpa_kegiatan`.`Kd_ProgramOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_SasaranProgramOPD` = `sakip_pdpa_kegiatan`.`Kd_SasaranProgramOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_KegiatanOPD` = `sakip_pdpa_kegiatan`.`Kd_KegiatanOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Tahun` = `sakip_pdpa_kegiatan`.`Kd_Tahun`
                WHERE `sakip_pdpa_program_sasaran`.`Kd_Urusan` = `sakip_pdpa_program`.`Kd_Urusan`
                AND `sakip_pdpa_program_sasaran`.`Kd_Bidang` = `sakip_pdpa_program`.`Kd_Bidang`
                AND `sakip_pdpa_program_sasaran`.`Kd_Unit` = `sakip_pdpa_program`.`Kd_Unit`
                AND `sakip_pdpa_program_sasaran`.`Kd_Sub` = `sakip_pdpa_program`.`Kd_Sub`
                AND `sakip_pdpa_program_sasaran`.`Kd_Pemda` = `sakip_pdpa_program`.`Kd_Pemda`
                AND `sakip_pdpa_program_sasaran`.`Kd_Misi` = `sakip_pdpa_program`.`Kd_Misi`
                AND `sakip_pdpa_program_sasaran`.`Kd_Tujuan` = `sakip_pdpa_program`.`Kd_Tujuan`
                AND `sakip_pdpa_program_sasaran`.`Kd_IndikatorTujuan` = `sakip_pdpa_program`.`Kd_IndikatorTujuan`
                AND `sakip_pdpa_program_sasaran`.`Kd_Sasaran` = `sakip_pdpa_program`.`Kd_Sasaran`
                AND `sakip_pdpa_program_sasaran`.`Kd_IndikatorSasaran` = `sakip_pdpa_program`.`Kd_IndikatorSasaran`
                AND `sakip_pdpa_program_sasaran`.`Kd_TujuanOPD` = `sakip_pdpa_program`.`Kd_TujuanOPD`
                AND `sakip_pdpa_program_sasaran`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_program`.`Kd_IndikatorTujuanOPD`
                AND `sakip_pdpa_program_sasaran`.`Kd_SasaranOPD` = `sakip_pdpa_program`.`Kd_SasaranOPD`
                AND `sakip_pdpa_program_sasaran`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_program`.`Kd_IndikatorSasaranOPD`
                /*AND `sakip_pdpa_program_sasaran`.`Kd_Bid` = `sakip_pdpa_program`.`Kd_Bid`*/
                AND `sakip_pdpa_program_sasaran`.`Kd_ProgramOPD` = `sakip_pdpa_program`.`Kd_ProgramOPD`
                AND `sakip_pdpa_program_sasaran`.`Kd_Tahun` = `sakip_pdpa_program`.`Kd_Tahun`
            ) AS count_program_span,
            (SELECT COUNT(*) FROM `sakip_pdpa_kegiatan`
                left join `sakip_pdpa_kegiatan_indikator` on
                  `sakip_pdpa_kegiatan_indikator`.`Kd_Urusan` = `sakip_pdpa_kegiatan`.`Kd_Urusan`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Bidang` = `sakip_pdpa_kegiatan`.`Kd_Bidang`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Unit` = `sakip_pdpa_kegiatan`.`Kd_Unit`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Sub` = `sakip_pdpa_kegiatan`.`Kd_Sub`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Bid` = `sakip_pdpa_kegiatan`.`Kd_Bid`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Subbid` = `sakip_pdpa_kegiatan`.`Kd_Subbid`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Pemda` = `sakip_pdpa_kegiatan`.`Kd_Pemda`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Misi` = `sakip_pdpa_kegiatan`.`Kd_Misi`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Tujuan` = `sakip_pdpa_kegiatan`.`Kd_Tujuan`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorTujuan` = `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuan`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Sasaran` = `sakip_pdpa_kegiatan`.`Kd_Sasaran`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorSasaran` = `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaran`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_TujuanOPD` = `sakip_pdpa_kegiatan`.`Kd_TujuanOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuanOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_SasaranOPD` = `sakip_pdpa_kegiatan`.`Kd_SasaranOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaranOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_ProgramOPD` = `sakip_pdpa_kegiatan`.`Kd_ProgramOPD`
                  /*AND `sakip_pdpa_kegiatan_indikator`.`Kd_SasaranProgramOPD` = `sakip_pdpa_kegiatan`.`Kd_SasaranProgramOPD`*/
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Tahun` = `sakip_pdpa_kegiatan`.`Kd_Tahun`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_KegiatanOPD` = `sakip_pdpa_kegiatan`.`Kd_KegiatanOPD`
                WHERE `sakip_pdpa_kegiatan`.`Kd_Urusan` = `sakip_pdpa_program`.`Kd_Urusan`
                AND `sakip_pdpa_kegiatan`.`Kd_Bidang` = `sakip_pdpa_program`.`Kd_Bidang`
                AND `sakip_pdpa_kegiatan`.`Kd_Unit` = `sakip_pdpa_program`.`Kd_Unit`
                AND `sakip_pdpa_kegiatan`.`Kd_Sub` = `sakip_pdpa_program`.`Kd_Sub`
                AND `sakip_pdpa_kegiatan`.`Kd_Pemda` = `sakip_pdpa_program`.`Kd_Pemda`
                AND `sakip_pdpa_kegiatan`.`Kd_Misi` = `sakip_pdpa_program`.`Kd_Misi`
                AND `sakip_pdpa_kegiatan`.`Kd_Tujuan` = `sakip_pdpa_program`.`Kd_Tujuan`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_pdpa_program`.`Kd_IndikatorTujuan`
                AND `sakip_pdpa_kegiatan`.`Kd_Sasaran` = `sakip_pdpa_program`.`Kd_Sasaran`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_pdpa_program`.`Kd_IndikatorSasaran`
                AND `sakip_pdpa_kegiatan`.`Kd_TujuanOPD` = `sakip_pdpa_program`.`Kd_TujuanOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_program`.`Kd_IndikatorTujuanOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_SasaranOPD` = `sakip_pdpa_program`.`Kd_SasaranOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_program`.`Kd_IndikatorSasaranOPD`
                /*AND `sakip_pdpa_kegiatan`.`Kd_Bid` = `sakip_pdpa_program`.`Kd_Bid`*/
                AND `sakip_pdpa_kegiatan`.`Kd_ProgramOPD` = `sakip_pdpa_program`.`Kd_ProgramOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_Tahun` = `sakip_pdpa_program`.`Kd_Tahun`
                /*AND `sakip_pdpa_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_pdpa_program_sasaran`.`Kd_SasaranProgramOPD`*/
            ) AS count_sasaranprog_span,
            (SELECT COUNT(*) FROM `sakip_pdpa_kegiatan_indikator`
                WHERE `sakip_pdpa_kegiatan_indikator`.`Kd_Urusan` = `sakip_pdpa_kegiatan`.`Kd_Urusan`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_Bidang` = `sakip_pdpa_kegiatan`.`Kd_Bidang`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_Unit` = `sakip_pdpa_kegiatan`.`Kd_Unit`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_Sub` = `sakip_pdpa_kegiatan`.`Kd_Sub`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_Bid` = `sakip_pdpa_kegiatan`.`Kd_Bid`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_Subbid` = `sakip_pdpa_kegiatan`.`Kd_Subbid`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_Pemda` = `sakip_pdpa_kegiatan`.`Kd_Pemda`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_Misi` = `sakip_pdpa_kegiatan`.`Kd_Misi`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_Tujuan` = `sakip_pdpa_kegiatan`.`Kd_Tujuan`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorTujuan` = `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuan`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_Sasaran` = `sakip_pdpa_kegiatan`.`Kd_Sasaran`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorSasaran` = `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaran`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_TujuanOPD` = `sakip_pdpa_kegiatan`.`Kd_TujuanOPD`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuanOPD`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_SasaranOPD` = `sakip_pdpa_kegiatan`.`Kd_SasaranOPD`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaranOPD`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_ProgramOPD` = `sakip_pdpa_kegiatan`.`Kd_ProgramOPD`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_Tahun` = `sakip_pdpa_kegiatan`.`Kd_Tahun`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_SasaranProgramOPD` = `sakip_pdpa_kegiatan`.`Kd_SasaranProgramOPD`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_KegiatanOPD` = `sakip_pdpa_kegiatan`.`Kd_KegiatanOPD`
            ) AS count_kegiatan_span
            FROM `sakip_pdpa_program`
            LEFT JOIN `sakip_pdpa_program_sasaran`
                ON `sakip_pdpa_program_sasaran`.`Kd_Urusan` = `sakip_pdpa_program`.`Kd_Urusan`
                AND `sakip_pdpa_program_sasaran`.`Kd_Bidang` = `sakip_pdpa_program`.`Kd_Bidang`
                AND `sakip_pdpa_program_sasaran`.`Kd_Unit` = `sakip_pdpa_program`.`Kd_Unit`
                AND `sakip_pdpa_program_sasaran`.`Kd_Sub` = `sakip_pdpa_program`.`Kd_Sub`
                AND `sakip_pdpa_program_sasaran`.`Kd_Bid` = `sakip_pdpa_program`.`Kd_Bid`
                AND `sakip_pdpa_program_sasaran`.`Kd_Pemda` = `sakip_pdpa_program`.`Kd_Pemda`
                AND `sakip_pdpa_program_sasaran`.`Kd_Misi` = `sakip_pdpa_program`.`Kd_Misi`
                AND `sakip_pdpa_program_sasaran`.`Kd_Tujuan` = `sakip_pdpa_program`.`Kd_Tujuan`
                AND `sakip_pdpa_program_sasaran`.`Kd_IndikatorTujuan` = `sakip_pdpa_program`.`Kd_IndikatorTujuan`
                AND `sakip_pdpa_program_sasaran`.`Kd_Sasaran` = `sakip_pdpa_program`.`Kd_Sasaran`
                AND `sakip_pdpa_program_sasaran`.`Kd_IndikatorSasaran` = `sakip_pdpa_program`.`Kd_IndikatorSasaran`
                AND `sakip_pdpa_program_sasaran`.`Kd_TujuanOPD` = `sakip_pdpa_program`.`Kd_TujuanOPD`
                AND `sakip_pdpa_program_sasaran`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_program`.`Kd_IndikatorTujuanOPD`
                AND `sakip_pdpa_program_sasaran`.`Kd_SasaranOPD` = `sakip_pdpa_program`.`Kd_SasaranOPD`
                AND `sakip_pdpa_program_sasaran`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_program`.`Kd_IndikatorSasaranOPD`
                AND `sakip_pdpa_program_sasaran`.`Kd_ProgramOPD` = `sakip_pdpa_program`.`Kd_ProgramOPD`
                AND `sakip_pdpa_program_sasaran`.`Kd_Tahun` = `sakip_pdpa_program`.`Kd_Tahun`
            LEFT JOIN `sakip_pdpa_kegiatan`
                ON `sakip_pdpa_kegiatan`.`Kd_Urusan` = `sakip_pdpa_program_sasaran`.`Kd_Urusan`
                AND `sakip_pdpa_kegiatan`.`Kd_Bidang` = `sakip_pdpa_program_sasaran`.`Kd_Bidang`
                AND `sakip_pdpa_kegiatan`.`Kd_Unit` = `sakip_pdpa_program_sasaran`.`Kd_Unit`
                AND `sakip_pdpa_kegiatan`.`Kd_Sub` = `sakip_pdpa_program_sasaran`.`Kd_Sub`
                AND `sakip_pdpa_kegiatan`.`Kd_Bid` = `sakip_pdpa_program_sasaran`.`Kd_Bid`
                AND `sakip_pdpa_kegiatan`.`Kd_Pemda` = `sakip_pdpa_program_sasaran`.`Kd_Pemda`
                AND `sakip_pdpa_kegiatan`.`Kd_Misi` = `sakip_pdpa_program_sasaran`.`Kd_Misi`
                AND `sakip_pdpa_kegiatan`.`Kd_Tujuan` = `sakip_pdpa_program_sasaran`.`Kd_Tujuan`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorTujuan`
                AND `sakip_pdpa_kegiatan`.`Kd_Sasaran` = `sakip_pdpa_program_sasaran`.`Kd_Sasaran`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorSasaran`
                AND `sakip_pdpa_kegiatan`.`Kd_TujuanOPD` = `sakip_pdpa_program_sasaran`.`Kd_TujuanOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorTujuanOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_SasaranOPD` = `sakip_pdpa_program_sasaran`.`Kd_SasaranOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorSasaranOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_ProgramOPD` = `sakip_pdpa_program_sasaran`.`Kd_ProgramOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_pdpa_program_sasaran`.`Kd_SasaranProgramOPD`
                AND `sakip_pdpa_kegiatan`.`Kd_Tahun` = `sakip_pdpa_program_sasaran`.`Kd_Tahun`
            LEFT JOIN `sakip_pdpa_kegiatan_indikator`
                ON `sakip_pdpa_kegiatan_indikator`.`Kd_Urusan` = `sakip_pdpa_kegiatan`.`Kd_Urusan`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_Bidang` = `sakip_pdpa_kegiatan`.`Kd_Bidang`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_Unit` = `sakip_pdpa_kegiatan`.`Kd_Unit`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_Sub` = `sakip_pdpa_kegiatan`.`Kd_Sub`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_Bid` = `sakip_pdpa_kegiatan`.`Kd_Bid`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_Subbid` = `sakip_pdpa_kegiatan`.`Kd_Subbid`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_Pemda` = `sakip_pdpa_kegiatan`.`Kd_Pemda`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_Misi` = `sakip_pdpa_kegiatan`.`Kd_Misi`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_Tujuan` = `sakip_pdpa_kegiatan`.`Kd_Tujuan`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorTujuan` = `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuan`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_Sasaran` = `sakip_pdpa_kegiatan`.`Kd_Sasaran`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorSasaran` = `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaran`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_TujuanOPD` = `sakip_pdpa_kegiatan`.`Kd_TujuanOPD`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuanOPD`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_SasaranOPD` = `sakip_pdpa_kegiatan`.`Kd_SasaranOPD`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaranOPD`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_ProgramOPD` = `sakip_pdpa_kegiatan`.`Kd_ProgramOPD`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_SasaranProgramOPD` = `sakip_pdpa_kegiatan`.`Kd_SasaranProgramOPD`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_KegiatanOPD` = `sakip_pdpa_kegiatan`.`Kd_KegiatanOPD`
                AND `sakip_pdpa_kegiatan_indikator`.`Kd_Tahun` = `sakip_pdpa_kegiatan`.`Kd_Tahun`
            WHERE
                `sakip_pdpa_program`.`Kd_Tahun` = ?
                AND `sakip_pdpa_program`.`Kd_Urusan` = ?
                AND `sakip_pdpa_program`.`Kd_Bidang` = ?
                AND `sakip_pdpa_program`.`Kd_Unit` = ?
                AND `sakip_pdpa_program`.`Kd_Sub` = ?
            /*group by
              `sakip_pdpa_program`.`Kd_ProgramOPD`,
              `sakip_pdpa_program_sasaran`.`Kd_SasaranProgramOPD`,
              `sakip_pdpa_kegiatan`.`Kd_KegiatanOPD`,
              `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorKegiatanOPD`*/
            ORDER BY
              sakip_pdpa_program.Kd_Urusan,
              sakip_pdpa_program.Kd_Bidang,
              sakip_pdpa_program.Kd_Unit,
              sakip_pdpa_program.Kd_Sub,
              sakip_pdpa_program.Kd_Pemda,
              sakip_pdpa_program.Kd_Misi,
              sakip_pdpa_program.Kd_Tujuan,
              sakip_pdpa_program.Kd_IndikatorTujuan,
              sakip_pdpa_program.Kd_Sasaran,
              sakip_pdpa_program.Kd_IndikatorSasaran,
              sakip_pdpa_program.Kd_TujuanOPD,
              sakip_pdpa_program.Kd_IndikatorTujuanOPD,
              sakip_pdpa_program.Kd_SasaranOPD,
              sakip_pdpa_program.Kd_IndikatorSasaranOPD,
              sakip_pdpa_program.Kd_ProgramOPD,
              sakip_pdpa_program_sasaran.Kd_SasaranProgramOPD,

              sakip_pdpa_kegiatan.Kd_KegiatanOPD,
              sakip_pdpa_kegiatan_indikator.Kd_IndikatorKegiatanOPD
              /*sakip_pdpa_program.Kd_Bid,
              sakip_pdpa_kegiatan.Kd_Subbid*/
              ";
            }
            $params = array(
                $this->input->post(COL_KD_TAHUN),
                $this->input->post(COL_KD_URUSAN),
                $this->input->post(COL_KD_BIDANG),
                $this->input->post(COL_KD_UNIT),
                $this->input->post(COL_KD_SUB),
            );
            $rprogram = $this->db->query($q, $params)->result_array();
            $dat['program'] = $rprogram;
            $dat['edit'] = true;
            $dat['data'] = array(
                COL_KD_TAHUN => count($rprogram) > 0 ? $rprogram[0][COL_KD_TAHUN] : date("Y"),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB)
            );
        }

        $nmSub = "";
        $strOPD = explode('.', $ruser[COL_COMPANYID]);
        $eplandb = $this->load->database("eplan", true);
        if($dat['edit']) {
            $eplandb->where(COL_KD_URUSAN, $dat['data'][COL_KD_URUSAN]);
            $eplandb->where(COL_KD_BIDANG, $dat['data'][COL_KD_BIDANG]);
            $eplandb->where(COL_KD_UNIT, $dat['data'][COL_KD_UNIT]);
            $eplandb->where(COL_KD_SUB, $dat['data'][COL_KD_SUB]);
            $subunit = $eplandb->get("ref_sub_unit")->row_array();
            if($subunit) {
                $nmSub = $subunit["Nm_Sub_Unit"];
            }
        }
        if($ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEKEUANGAN) {
            $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
            $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
            $eplandb->where(COL_KD_UNIT, $strOPD[2]);
            $eplandb->where(COL_KD_SUB, $strOPD[3]);
            $subunit = $eplandb->get("ref_sub_unit")->row_array();
            if($subunit) {
                $nmSub = $subunit["Nm_Sub_Unit"];
            }
        }
        $dat['strOPD'] = $strOPD;
        $dat['nmSub'] = $nmSub;
        $dat['eplandb'] = $eplandb;

        if($cetak) $this->load->view('report/rencana_aksi_partial', $dat);
        else $this->load->view('report/rencana_aksi', $dat);
    }

    function iku_kabupaten() {
        $ruser = GetLoggedUser();
        $dat['title'] = "IKU Kabupaten";
        $dat['edit'] = false;
        $dat['cetak'] = $cetak = $this->input->post("cetak");

        $this->db->where(COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(COL_KD_TAHUN_TO." >=", date("Y"));
        $this->db->order_by(COL_KD_TAHUN_FROM, 'desc');
        $dat['rpemda'] = $rpemda = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();
        if(!empty($_POST)) {
            $dat['rpemda'] = $rpemda = $this->db->where(COL_KD_PEMDA, $this->input->post(COL_KD_PEMDA))->get(TBL_SAKIP_MPEMDA)->row_array();

            $this->db->select(
                TBL_SAKIP_MPMD_SASARAN.".".COL_NM_SASARAN.",".
                TBL_SAKIP_MPMD_IKSASARAN.".*,".
                TBL_SAKIP_MPEMDA.".*,".
                "(SELECT COUNT(*) FROM `sakip_mpmd_iksasaran`
                WHERE `sakip_mpmd_iksasaran`.`Kd_Pemda` = `sakip_mpmd_sasaran`.`Kd_Pemda`
                AND `sakip_mpmd_iksasaran`.`Kd_Misi` = `sakip_mpmd_sasaran`.`Kd_Misi`
                AND `sakip_mpmd_iksasaran`.`Kd_Tujuan` = `sakip_mpmd_sasaran`.`Kd_Tujuan`
                AND `sakip_mpmd_iksasaran`.`Kd_IndikatorTujuan` = `sakip_mpmd_sasaran`.`Kd_IndikatorTujuan`
                AND `sakip_mpmd_iksasaran`.`Kd_Sasaran` = `sakip_mpmd_sasaran`.`Kd_Sasaran`
            ) AS span_sasaran");
            $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_PEMDA,"inner");
            $this->db->join(TBL_SAKIP_MPMD_SASARAN,
                TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_SASARAN
                ,"inner");
            $this->db->where(TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_PEMDA, $this->input->post(COL_KD_PEMDA));
            $this->db->order_by(TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_MISI, 'asc');
            $this->db->order_by(TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_TUJUAN, 'asc');
            $this->db->order_by(TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_INDIKATORTUJUAN, 'asc');
            $this->db->order_by(TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_SASARAN, 'asc');
            $this->db->order_by(TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_INDIKATORSASARAN, 'asc');
            $rsasaran = $this->db->get(TBL_SAKIP_MPMD_IKSASARAN)->result_array();
            $dat['sasaran'] = $rsasaran;
            $dat['edit'] = true;
            $dat['data'] = array(
                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                COL_KD_TAHUN_FROM => count($rsasaran) > 0 ? $rsasaran[0][COL_KD_TAHUN_FROM] : "",
                COL_KD_TAHUN_TO => count($rsasaran) > 0 ? $rsasaran[0][COL_KD_TAHUN_TO] : "",
                COL_NM_PEJABAT => count($rsasaran) > 0 ? $rsasaran[0][COL_NM_PEJABAT] : "",
                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA)
            );
        }

        if($cetak) $this->load->view('report/iku_kab_', $dat);
        else $this->load->view('report/iku_kab', $dat);
    }

    function iku() {
        $ruser = GetLoggedUser();
        $dat['title'] = "Indikator Kinerja Utama";
        $dat['edit'] = false;
        $this->db->where(COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(COL_KD_TAHUN_TO." >=", date("Y"));
        $this->db->order_by(COL_KD_TAHUN_FROM, 'desc');
        $dat['rpemda'] = $rpemda = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();

        if (!empty($_GET)) {
            $this->db->where(COL_KD_TAHUN_FROM." <=", $this->input->get(COL_KD_TAHUN));
            $this->db->where(COL_KD_TAHUN_TO." >=", $this->input->get(COL_KD_TAHUN));
            $this->db->order_by(COL_KD_TAHUN_FROM, 'desc');
            $dat['rpemda'] = $rpemda = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();
            if(count($rpemda) == 0) {
                echo "Tahun tidak valid.";
                return;
            }

            $dat['data'] = array(
                COL_KD_TAHUN_FROM => $rpemda[COL_KD_TAHUN_FROM],
                COL_KD_TAHUN_TO => $rpemda[COL_KD_TAHUN_TO],
                COL_NM_PEJABAT => $rpemda[COL_NM_PEJABAT],
                COL_KD_PEMDA => $rpemda[COL_KD_PEMDA],
                COL_KD_TAHUN => $this->input->get(COL_KD_TAHUN),
                COL_KD_URUSAN => $this->input->get(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->get(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->get(COL_KD_UNIT),
                COL_KD_SUB => $this->input->get(COL_KD_SUB)
            );

            $nmSub = "";
            $nmPimpinanSub = "";
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $eplandb = $this->load->database("eplan", true);
            $eplandb->where(COL_KD_URUSAN, !empty($strOPD[0]) ? $strOPD[0] : $this->input->get(COL_KD_URUSAN));
            $eplandb->where(COL_KD_BIDANG, !empty($strOPD[1]) ? $strOPD[1] : $this->input->get(COL_KD_BIDANG));
            $eplandb->where(COL_KD_UNIT, !empty($strOPD[2]) ? $strOPD[2] : $this->input->get(COL_KD_UNIT));
            $eplandb->where(COL_KD_SUB, !empty($strOPD[3]) ? $strOPD[3] : $this->input->get(COL_KD_SUB));
            $subunit = $eplandb->get("ref_sub_unit")->row_array();

            $eplandb->where(COL_KD_URUSAN, !empty($strOPD[0]) ? $strOPD[0] : $this->input->get(COL_KD_URUSAN));
            $eplandb->where(COL_KD_BIDANG, !empty($strOPD[1]) ? $strOPD[1] : $this->input->get(COL_KD_BIDANG));
            $eplandb->where(COL_KD_UNIT, !empty($strOPD[2]) ? $strOPD[2] : $this->input->get(COL_KD_UNIT));
            $eplandb->where(COL_KD_SUB, !empty($strOPD[3]) ? $strOPD[3] : $this->input->get(COL_KD_SUB));
            //$eplandb->where("(Tahun = ".($this->input->get(COL_KD_TAHUN)-1)." or Tahun > ".($this->input->get(COL_KD_TAHUN)-1).")");
            //$eplandb->order_by("Tahun", 'asc');
            $subunit_ = $eplandb->get("ref_sub_unit")->row_array();

            if($subunit) {
                $nmSub = $subunit["Nm_Sub_Unit"];
            }
            if($subunit_) {
                $nmPimpinanSub = $subunit_["Nm_Pimpinan"];
            }
            $dat['strOPD'] = $strOPD;
            $dat['nmSub'] = $nmSub;
            $dat['nmPimpinanSub'] = $nmPimpinanSub;
            $dat['eplandb'] = $eplandb;

            $this->load->library('Mypdf');
            $mpdf = new Mypdf('utf-8', 'A4-L');

            $html = $this->load->view('report/iku_', $dat, TRUE);
            $mpdf->pdf->WriteHTML($html);
            $mpdf->pdf->Output('IKU.pdf', 'I');
        } else {
            $nmSub = "";
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $eplandb = $this->load->database("eplan", true);
            if($dat['edit']) {
                $eplandb->where(COL_KD_URUSAN, $dat['data'][COL_KD_URUSAN]);
                $eplandb->where(COL_KD_BIDANG, $dat['data'][COL_KD_BIDANG]);
                $eplandb->where(COL_KD_UNIT, $dat['data'][COL_KD_UNIT]);
                $eplandb->where(COL_KD_SUB, $dat['data'][COL_KD_SUB]);
                $subunit = $eplandb->get("ref_sub_unit")->row_array();
                if($subunit) {
                    $nmSub = $subunit["Nm_Sub_Unit"];
                }
            }
            if($ruser[COL_ROLEID] == ROLEKADIS) {
                $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
                $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
                $eplandb->where(COL_KD_UNIT, $strOPD[2]);
                $eplandb->where(COL_KD_SUB, $strOPD[3]);
                $subunit = $eplandb->get("ref_sub_unit")->row_array();
                if($subunit) {
                    $nmSub = $subunit["Nm_Sub_Unit"];
                }
            }
            $dat['strOPD'] = $strOPD;
            $dat['nmSub'] = $nmSub;
            $dat['eplandb'] = $eplandb;

            $this->load->view('report/iku', $dat);
        }
    }

    function dpa() {
        $ruser = GetLoggedUser();
        $dat['title'] = "DPA";
        $dat['edit'] = false;
        $dat['cetak'] = $cetak = $this->input->post("cetak");
        $dat['SumberData'] = $sumberData = $this->input->post("SumberData");

        if(!empty($_POST)) {
            $q = @"
            SELECT
            concat(
            sakip_dpa_kegiatan_indikator.Kd_Pemda,
              sakip_dpa_kegiatan_indikator.Kd_Misi,
              sakip_dpa_kegiatan_indikator.Kd_Tujuan,
              sakip_dpa_kegiatan_indikator.Kd_IndikatorTujuan,
              sakip_dpa_kegiatan_indikator.Kd_Sasaran,
              sakip_dpa_kegiatan_indikator.Kd_IndikatorSasaran,
              sakip_dpa_kegiatan_indikator.Kd_TujuanOPD,
              sakip_dpa_kegiatan_indikator.Kd_IndikatorTujuanOPD,
              sakip_dpa_kegiatan_indikator.Kd_SasaranOPD,
              sakip_dpa_kegiatan_indikator.Kd_IndikatorSasaranOPD,
              sakip_dpa_kegiatan_indikator.Kd_ProgramOPD,
              sakip_dpa_kegiatan_indikator.Kd_SasaranProgramOPD,

              sakip_dpa_kegiatan_indikator.Kd_KegiatanOPD,
              sakip_dpa_kegiatan_indikator.Kd_IndikatorKegiatanOPD,
              sakip_dpa_kegiatan_indikator.Kd_Urusan,
              sakip_dpa_kegiatan_indikator.Kd_Bidang,
              sakip_dpa_kegiatan_indikator.Kd_Unit,
              sakip_dpa_kegiatan_indikator.Kd_Sub,
              sakip_dpa_kegiatan_indikator.Kd_Bid,
              sakip_dpa_kegiatan_indikator.Kd_Subbid
            ) as IDD,
            sakip_dpa_program.Uniq as UNIQ_PRG,
            sakip_dpa_kegiatan_indikator.Uniq as UNIQ_IKKEG,
            sakip_dpa_program_sasaran.Uniq as UNIQ_SPROG,
            sakip_dpa_kegiatan.*,
            sakip_dpa_program.*,
            sakip_dpa_program.Kd_Tahun,
            sakip_dpa_program.Nm_ProgramOPD,
            sakip_dpa_program_sasaran.Kd_SasaranProgramOPD,
            #sakip_dpa_program_sasaran.Kd_IndikatorProgramOPD,
            #sakip_dpa_program_sasaran.Kd_Satuan AS Satuan1,
            #sakip_dpa_program_sasaran.Target AS Target1,
            0 AS Total1,
            sakip_dpa_kegiatan.Nm_KegiatanOPD,
            sakip_dpa_kegiatan_indikator.Kd_SasaranKegiatanOPD,
            sakip_dpa_kegiatan_indikator.Kd_IndikatorKegiatanOPD,
            sakip_dpa_kegiatan_indikator.Nm_IndikatorKegiatanOPD,
            sakip_dpa_kegiatan_indikator.Kd_Satuan AS Satuan2,
            sakip_dpa_kegiatan_indikator.Target AS Target2,
            COALESCE(sakip_dpa_kegiatan.Pergeseran, sakip_dpa_kegiatan.Budget) AS Budget2,
            sakip_dpa_kegiatan.Total AS Total2,
            (SELECT SUM(COALESCE(sakip_dpa_kegiatan.Pergeseran, sakip_dpa_kegiatan.Budget)) FROM `sakip_dpa_kegiatan`
                WHERE `sakip_dpa_kegiatan`.`Kd_Urusan` = `sakip_dpa_program_sasaran`.`Kd_Urusan`
                AND `sakip_dpa_kegiatan`.`Kd_Bidang` = `sakip_dpa_program_sasaran`.`Kd_Bidang`
                AND `sakip_dpa_kegiatan`.`Kd_Unit` = `sakip_dpa_program_sasaran`.`Kd_Unit`
                AND `sakip_dpa_kegiatan`.`Kd_Sub` = `sakip_dpa_program_sasaran`.`Kd_Sub`
                #AND `sakip_dpa_kegiatan`.`Kd_Bid` = `sakip_dpa_program_sasaran`.`Kd_Bid`
                AND `sakip_dpa_kegiatan`.`Kd_Pemda` = `sakip_dpa_program_sasaran`.`Kd_Pemda`
                AND `sakip_dpa_kegiatan`.`Kd_Misi` = `sakip_dpa_program_sasaran`.`Kd_Misi`
                AND `sakip_dpa_kegiatan`.`Kd_Tujuan` = `sakip_dpa_program_sasaran`.`Kd_Tujuan`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_kegiatan`.`Kd_Sasaran` = `sakip_dpa_program_sasaran`.`Kd_Sasaran`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_kegiatan`.`Kd_TujuanOPD` = `sakip_dpa_program_sasaran`.`Kd_TujuanOPD`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_kegiatan`.`Kd_SasaranOPD` = `sakip_dpa_program_sasaran`.`Kd_SasaranOPD`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaranOPD`
                AND `sakip_dpa_kegiatan`.`Kd_ProgramOPD` = `sakip_dpa_program_sasaran`.`Kd_ProgramOPD`
                AND `sakip_dpa_kegiatan`.`Kd_Tahun` = `sakip_dpa_program_sasaran`.`Kd_Tahun`
                AND `sakip_dpa_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_dpa_program_sasaran`.`Kd_SasaranProgramOPD`
            ) AS sum_program_total,
            (SELECT SUM(sakip_dpa_kegiatan.Budget) FROM `sakip_dpa_kegiatan`
                WHERE `sakip_dpa_kegiatan`.`Kd_Urusan` = `sakip_dpa_kegiatan`.`Kd_Urusan`
                AND `sakip_dpa_kegiatan`.`Kd_Bidang` = `sakip_dpa_kegiatan`.`Kd_Bidang`
                AND `sakip_dpa_kegiatan`.`Kd_Unit` = `sakip_dpa_kegiatan`.`Kd_Unit`
                AND `sakip_dpa_kegiatan`.`Kd_Sub` = `sakip_dpa_kegiatan`.`Kd_Sub`
                AND `sakip_dpa_kegiatan`.`Kd_Bid` = `sakip_dpa_kegiatan`.`Kd_Bid`
                AND `sakip_dpa_kegiatan`.`Kd_Pemda` = `sakip_dpa_kegiatan`.`Kd_Pemda`
                AND `sakip_dpa_kegiatan`.`Kd_Misi` = `sakip_dpa_kegiatan`.`Kd_Misi`
                AND `sakip_dpa_kegiatan`.`Kd_Tujuan` = `sakip_dpa_kegiatan`.`Kd_Tujuan`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_kegiatan`.`Kd_Sasaran` = `sakip_dpa_kegiatan`.`Kd_Sasaran`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_kegiatan`.`Kd_TujuanOPD` = `sakip_dpa_kegiatan`.`Kd_TujuanOPD`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_kegiatan`.`Kd_SasaranOPD` = `sakip_dpa_kegiatan`.`Kd_SasaranOPD`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD`
                AND `sakip_dpa_kegiatan`.`Kd_ProgramOPD` = `sakip_dpa_kegiatan`.`Kd_ProgramOPD`
                AND `sakip_dpa_kegiatan`.`Kd_Tahun` = `sakip_dpa_kegiatan`.`Kd_Tahun`
                AND `sakip_dpa_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_dpa_kegiatan`.`Kd_SasaranProgramOPD`
                AND `sakip_dpa_kegiatan`.`Kd_KegiatanOPD` = `sakip_dpa_kegiatan`.`Kd_KegiatanOPD`
            ) AS sum_kegiatan_total,
            (SELECT COUNT(*) FROM `sakip_dpa_program_sasaran`
                left join `sakip_dpa_kegiatan` on
                  `sakip_dpa_kegiatan`.`Kd_Urusan` = `sakip_dpa_program_sasaran`.`Kd_Urusan`
                  AND `sakip_dpa_kegiatan`.`Kd_Bidang` = `sakip_dpa_program_sasaran`.`Kd_Bidang`
                  AND `sakip_dpa_kegiatan`.`Kd_Unit` = `sakip_dpa_program_sasaran`.`Kd_Unit`
                  AND `sakip_dpa_kegiatan`.`Kd_Sub` = `sakip_dpa_program_sasaran`.`Kd_Sub`
                  AND `sakip_dpa_kegiatan`.`Kd_Bid` = `sakip_dpa_program_sasaran`.`Kd_Bid`
                  AND `sakip_dpa_kegiatan`.`Kd_Pemda` = `sakip_dpa_program_sasaran`.`Kd_Pemda`
                  AND `sakip_dpa_kegiatan`.`Kd_Misi` = `sakip_dpa_program_sasaran`.`Kd_Misi`
                  AND `sakip_dpa_kegiatan`.`Kd_Tujuan` = `sakip_dpa_program_sasaran`.`Kd_Tujuan`
                  AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuan`
                  AND `sakip_dpa_kegiatan`.`Kd_Sasaran` = `sakip_dpa_program_sasaran`.`Kd_Sasaran`
                  AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaran`
                  AND `sakip_dpa_kegiatan`.`Kd_TujuanOPD` = `sakip_dpa_program_sasaran`.`Kd_TujuanOPD`
                  AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuanOPD`
                  AND `sakip_dpa_kegiatan`.`Kd_SasaranOPD` = `sakip_dpa_program_sasaran`.`Kd_SasaranOPD`
                  AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaranOPD`
                  AND `sakip_dpa_kegiatan`.`Kd_ProgramOPD` = `sakip_dpa_program_sasaran`.`Kd_ProgramOPD`
                  AND `sakip_dpa_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_dpa_program_sasaran`.`Kd_SasaranProgramOPD`
                  AND `sakip_dpa_kegiatan`.`Kd_Tahun` = `sakip_dpa_program_sasaran`.`Kd_Tahun`
                left join `sakip_dpa_kegiatan_indikator` on
                  `sakip_dpa_kegiatan_indikator`.`Kd_Urusan` = `sakip_dpa_kegiatan`.`Kd_Urusan`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Bidang` = `sakip_dpa_kegiatan`.`Kd_Bidang`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Unit` = `sakip_dpa_kegiatan`.`Kd_Unit`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Sub` = `sakip_dpa_kegiatan`.`Kd_Sub`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Bid` = `sakip_dpa_kegiatan`.`Kd_Bid`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Subbid` = `sakip_dpa_kegiatan`.`Kd_Subbid`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Pemda` = `sakip_dpa_kegiatan`.`Kd_Pemda`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Misi` = `sakip_dpa_kegiatan`.`Kd_Misi`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Tujuan` = `sakip_dpa_kegiatan`.`Kd_Tujuan`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorTujuan` = `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Sasaran` = `sakip_dpa_kegiatan`.`Kd_Sasaran`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorSasaran` = `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_TujuanOPD` = `sakip_dpa_kegiatan`.`Kd_TujuanOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_SasaranOPD` = `sakip_dpa_kegiatan`.`Kd_SasaranOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_ProgramOPD` = `sakip_dpa_kegiatan`.`Kd_ProgramOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_SasaranProgramOPD` = `sakip_dpa_kegiatan`.`Kd_SasaranProgramOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_KegiatanOPD` = `sakip_dpa_kegiatan`.`Kd_KegiatanOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Tahun` = `sakip_dpa_kegiatan`.`Kd_Tahun`
                WHERE `sakip_dpa_program_sasaran`.`Kd_Urusan` = `sakip_dpa_program`.`Kd_Urusan`
                AND `sakip_dpa_program_sasaran`.`Kd_Bidang` = `sakip_dpa_program`.`Kd_Bidang`
                AND `sakip_dpa_program_sasaran`.`Kd_Unit` = `sakip_dpa_program`.`Kd_Unit`
                AND `sakip_dpa_program_sasaran`.`Kd_Sub` = `sakip_dpa_program`.`Kd_Sub`
                AND `sakip_dpa_program_sasaran`.`Kd_Pemda` = `sakip_dpa_program`.`Kd_Pemda`
                AND `sakip_dpa_program_sasaran`.`Kd_Misi` = `sakip_dpa_program`.`Kd_Misi`
                AND `sakip_dpa_program_sasaran`.`Kd_Tujuan` = `sakip_dpa_program`.`Kd_Tujuan`
                AND `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuan` = `sakip_dpa_program`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_program_sasaran`.`Kd_Sasaran` = `sakip_dpa_program`.`Kd_Sasaran`
                AND `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaran` = `sakip_dpa_program`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_program_sasaran`.`Kd_TujuanOPD` = `sakip_dpa_program`.`Kd_TujuanOPD`
                AND `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_program`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_program_sasaran`.`Kd_SasaranOPD` = `sakip_dpa_program`.`Kd_SasaranOPD`
                AND `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_program`.`Kd_IndikatorSasaranOPD`
                /*AND `sakip_dpa_program_sasaran`.`Kd_Bid` = `sakip_dpa_program`.`Kd_Bid`*/
                AND `sakip_dpa_program_sasaran`.`Kd_ProgramOPD` = `sakip_dpa_program`.`Kd_ProgramOPD`
                AND `sakip_dpa_program_sasaran`.`Kd_Tahun` = `sakip_dpa_program`.`Kd_Tahun`
            ) AS count_program_span,
            (SELECT COUNT(*) FROM `sakip_dpa_kegiatan`
                left join `sakip_dpa_kegiatan_indikator` on
                  `sakip_dpa_kegiatan_indikator`.`Kd_Urusan` = `sakip_dpa_kegiatan`.`Kd_Urusan`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Bidang` = `sakip_dpa_kegiatan`.`Kd_Bidang`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Unit` = `sakip_dpa_kegiatan`.`Kd_Unit`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Sub` = `sakip_dpa_kegiatan`.`Kd_Sub`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Bid` = `sakip_dpa_kegiatan`.`Kd_Bid`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Subbid` = `sakip_dpa_kegiatan`.`Kd_Subbid`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Pemda` = `sakip_dpa_kegiatan`.`Kd_Pemda`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Misi` = `sakip_dpa_kegiatan`.`Kd_Misi`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Tujuan` = `sakip_dpa_kegiatan`.`Kd_Tujuan`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorTujuan` = `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Sasaran` = `sakip_dpa_kegiatan`.`Kd_Sasaran`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorSasaran` = `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_TujuanOPD` = `sakip_dpa_kegiatan`.`Kd_TujuanOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_SasaranOPD` = `sakip_dpa_kegiatan`.`Kd_SasaranOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_ProgramOPD` = `sakip_dpa_kegiatan`.`Kd_ProgramOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_SasaranProgramOPD` = `sakip_dpa_kegiatan`.`Kd_SasaranProgramOPD`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_Tahun` = `sakip_dpa_kegiatan`.`Kd_Tahun`
                  AND `sakip_dpa_kegiatan_indikator`.`Kd_KegiatanOPD` = `sakip_dpa_kegiatan`.`Kd_KegiatanOPD`
                WHERE `sakip_dpa_kegiatan`.`Kd_Urusan` = `sakip_dpa_program`.`Kd_Urusan`
                AND `sakip_dpa_kegiatan`.`Kd_Bidang` = `sakip_dpa_program`.`Kd_Bidang`
                AND `sakip_dpa_kegiatan`.`Kd_Unit` = `sakip_dpa_program`.`Kd_Unit`
                AND `sakip_dpa_kegiatan`.`Kd_Sub` = `sakip_dpa_program`.`Kd_Sub`
                AND `sakip_dpa_kegiatan`.`Kd_Pemda` = `sakip_dpa_program`.`Kd_Pemda`
                AND `sakip_dpa_kegiatan`.`Kd_Misi` = `sakip_dpa_program`.`Kd_Misi`
                AND `sakip_dpa_kegiatan`.`Kd_Tujuan` = `sakip_dpa_program`.`Kd_Tujuan`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_dpa_program`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_kegiatan`.`Kd_Sasaran` = `sakip_dpa_program`.`Kd_Sasaran`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_dpa_program`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_kegiatan`.`Kd_TujuanOPD` = `sakip_dpa_program`.`Kd_TujuanOPD`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_program`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_kegiatan`.`Kd_SasaranOPD` = `sakip_dpa_program`.`Kd_SasaranOPD`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_program`.`Kd_IndikatorSasaranOPD`
                AND `sakip_dpa_kegiatan`.`Kd_Bid` = `sakip_dpa_program`.`Kd_Bid`
                AND `sakip_dpa_kegiatan`.`Kd_ProgramOPD` = `sakip_dpa_program`.`Kd_ProgramOPD`
                AND `sakip_dpa_kegiatan`.`Kd_Tahun` = `sakip_dpa_program`.`Kd_Tahun`
                AND `sakip_dpa_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_dpa_program_sasaran`.`Kd_SasaranProgramOPD`
            ) AS count_sasaranprog_span,
            (SELECT COUNT(*) FROM `sakip_dpa_kegiatan_indikator`
                WHERE `sakip_dpa_kegiatan_indikator`.`Kd_Urusan` = `sakip_dpa_kegiatan`.`Kd_Urusan`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Bidang` = `sakip_dpa_kegiatan`.`Kd_Bidang`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Unit` = `sakip_dpa_kegiatan`.`Kd_Unit`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Sub` = `sakip_dpa_kegiatan`.`Kd_Sub`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Bid` = `sakip_dpa_kegiatan`.`Kd_Bid`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Subbid` = `sakip_dpa_kegiatan`.`Kd_Subbid`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Pemda` = `sakip_dpa_kegiatan`.`Kd_Pemda`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Misi` = `sakip_dpa_kegiatan`.`Kd_Misi`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Tujuan` = `sakip_dpa_kegiatan`.`Kd_Tujuan`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorTujuan` = `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Sasaran` = `sakip_dpa_kegiatan`.`Kd_Sasaran`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorSasaran` = `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_TujuanOPD` = `sakip_dpa_kegiatan`.`Kd_TujuanOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_SasaranOPD` = `sakip_dpa_kegiatan`.`Kd_SasaranOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_ProgramOPD` = `sakip_dpa_kegiatan`.`Kd_ProgramOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Tahun` = `sakip_dpa_kegiatan`.`Kd_Tahun`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_SasaranProgramOPD` = `sakip_dpa_kegiatan`.`Kd_SasaranProgramOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_KegiatanOPD` = `sakip_dpa_kegiatan`.`Kd_KegiatanOPD`
            ) AS count_kegiatan_span
            FROM `sakip_dpa_program`
            LEFT JOIN `sakip_dpa_program_sasaran`
                ON `sakip_dpa_program_sasaran`.`Kd_Urusan` = `sakip_dpa_program`.`Kd_Urusan`
                AND `sakip_dpa_program_sasaran`.`Kd_Bidang` = `sakip_dpa_program`.`Kd_Bidang`
                AND `sakip_dpa_program_sasaran`.`Kd_Unit` = `sakip_dpa_program`.`Kd_Unit`
                AND `sakip_dpa_program_sasaran`.`Kd_Sub` = `sakip_dpa_program`.`Kd_Sub`
                AND `sakip_dpa_program_sasaran`.`Kd_Bid` = `sakip_dpa_program`.`Kd_Bid`
                AND `sakip_dpa_program_sasaran`.`Kd_Pemda` = `sakip_dpa_program`.`Kd_Pemda`
                AND `sakip_dpa_program_sasaran`.`Kd_Misi` = `sakip_dpa_program`.`Kd_Misi`
                AND `sakip_dpa_program_sasaran`.`Kd_Tujuan` = `sakip_dpa_program`.`Kd_Tujuan`
                AND `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuan` = `sakip_dpa_program`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_program_sasaran`.`Kd_Sasaran` = `sakip_dpa_program`.`Kd_Sasaran`
                AND `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaran` = `sakip_dpa_program`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_program_sasaran`.`Kd_TujuanOPD` = `sakip_dpa_program`.`Kd_TujuanOPD`
                AND `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_program`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_program_sasaran`.`Kd_SasaranOPD` = `sakip_dpa_program`.`Kd_SasaranOPD`
                AND `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_program`.`Kd_IndikatorSasaranOPD`
                AND `sakip_dpa_program_sasaran`.`Kd_ProgramOPD` = `sakip_dpa_program`.`Kd_ProgramOPD`
                AND `sakip_dpa_program_sasaran`.`Kd_Tahun` = `sakip_dpa_program`.`Kd_Tahun`
            /*LEFT JOIN `sakip_dpa_program_indikator`
                ON `sakip_dpa_program_indikator`.`Kd_Urusan` = `sakip_dpa_program_sasaran`.`Kd_Urusan`
                AND `sakip_dpa_program_indikator`.`Kd_Bidang` = `sakip_dpa_program_sasaran`.`Kd_Bidang`
                AND `sakip_dpa_program_indikator`.`Kd_Unit` = `sakip_dpa_program_sasaran`.`Kd_Unit`
                AND `sakip_dpa_program_indikator`.`Kd_Sub` = `sakip_dpa_program_sasaran`.`Kd_Sub`
                AND `sakip_dpa_program_indikator`.`Kd_Bid` = `sakip_dpa_program_sasaran`.`Kd_Bid`
                AND `sakip_dpa_program_indikator`.`Kd_Pemda` = `sakip_dpa_program_sasaran`.`Kd_Pemda`
                AND `sakip_dpa_program_indikator`.`Kd_Misi` = `sakip_dpa_program_sasaran`.`Kd_Misi`
                AND `sakip_dpa_program_indikator`.`Kd_Tujuan` = `sakip_dpa_program_sasaran`.`Kd_Tujuan`
                AND `sakip_dpa_program_indikator`.`Kd_IndikatorTujuan` = `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_program_indikator`.`Kd_Sasaran` = `sakip_dpa_program_sasaran`.`Kd_Sasaran`
                AND `sakip_dpa_program_indikator`.`Kd_IndikatorSasaran` = `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_program_indikator`.`Kd_TujuanOPD` = `sakip_dpa_program_sasaran`.`Kd_TujuanOPD`
                AND `sakip_dpa_program_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_program_indikator`.`Kd_SasaranOPD` = `sakip_dpa_program_sasaran`.`Kd_SasaranOPD`
                AND `sakip_dpa_program_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaranOPD`
                AND `sakip_dpa_program_indikator`.`Kd_ProgramOPD` = `sakip_dpa_program_sasaran`.`Kd_ProgramOPD`
                AND `sakip_dpa_program_indikator`.`Kd_SasaranProgramOPD` = `sakip_dpa_program_sasaran`.`Kd_SasaranProgramOPD`
                AND `sakip_dpa_program_indikator`.`Kd_Tahun` = `sakip_dpa_program_sasaran`.`Kd_Tahun`*/
            LEFT JOIN `sakip_dpa_kegiatan`
                ON `sakip_dpa_kegiatan`.`Kd_Urusan` = `sakip_dpa_program_sasaran`.`Kd_Urusan`
                AND `sakip_dpa_kegiatan`.`Kd_Bidang` = `sakip_dpa_program_sasaran`.`Kd_Bidang`
                AND `sakip_dpa_kegiatan`.`Kd_Unit` = `sakip_dpa_program_sasaran`.`Kd_Unit`
                AND `sakip_dpa_kegiatan`.`Kd_Sub` = `sakip_dpa_program_sasaran`.`Kd_Sub`
                AND `sakip_dpa_kegiatan`.`Kd_Bid` = `sakip_dpa_program_sasaran`.`Kd_Bid`
                AND `sakip_dpa_kegiatan`.`Kd_Pemda` = `sakip_dpa_program_sasaran`.`Kd_Pemda`
                AND `sakip_dpa_kegiatan`.`Kd_Misi` = `sakip_dpa_program_sasaran`.`Kd_Misi`
                AND `sakip_dpa_kegiatan`.`Kd_Tujuan` = `sakip_dpa_program_sasaran`.`Kd_Tujuan`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_kegiatan`.`Kd_Sasaran` = `sakip_dpa_program_sasaran`.`Kd_Sasaran`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_kegiatan`.`Kd_TujuanOPD` = `sakip_dpa_program_sasaran`.`Kd_TujuanOPD`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_kegiatan`.`Kd_SasaranOPD` = `sakip_dpa_program_sasaran`.`Kd_SasaranOPD`
                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaranOPD`
                AND `sakip_dpa_kegiatan`.`Kd_ProgramOPD` = `sakip_dpa_program_sasaran`.`Kd_ProgramOPD`
                AND `sakip_dpa_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_dpa_program_sasaran`.`Kd_SasaranProgramOPD`
                AND `sakip_dpa_kegiatan`.`Kd_Tahun` = `sakip_dpa_program_sasaran`.`Kd_Tahun`
            LEFT JOIN `sakip_dpa_kegiatan_indikator`
                ON `sakip_dpa_kegiatan_indikator`.`Kd_Urusan` = `sakip_dpa_kegiatan`.`Kd_Urusan`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Bidang` = `sakip_dpa_kegiatan`.`Kd_Bidang`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Unit` = `sakip_dpa_kegiatan`.`Kd_Unit`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Sub` = `sakip_dpa_kegiatan`.`Kd_Sub`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Bid` = `sakip_dpa_kegiatan`.`Kd_Bid`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Subbid` = `sakip_dpa_kegiatan`.`Kd_Subbid`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Pemda` = `sakip_dpa_kegiatan`.`Kd_Pemda`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Misi` = `sakip_dpa_kegiatan`.`Kd_Misi`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Tujuan` = `sakip_dpa_kegiatan`.`Kd_Tujuan`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorTujuan` = `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Sasaran` = `sakip_dpa_kegiatan`.`Kd_Sasaran`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorSasaran` = `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_TujuanOPD` = `sakip_dpa_kegiatan`.`Kd_TujuanOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_SasaranOPD` = `sakip_dpa_kegiatan`.`Kd_SasaranOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_ProgramOPD` = `sakip_dpa_kegiatan`.`Kd_ProgramOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_SasaranProgramOPD` = `sakip_dpa_kegiatan`.`Kd_SasaranProgramOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_KegiatanOPD` = `sakip_dpa_kegiatan`.`Kd_KegiatanOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Tahun` = `sakip_dpa_kegiatan`.`Kd_Tahun`
            /*LEFT JOIN `sakip_dpa_kegiatan_indikator`
                ON `sakip_dpa_kegiatan_indikator`.`Kd_Urusan` = `sakip_dpa_kegiatan_sasaran`.`Kd_Urusan`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Bidang` = `sakip_dpa_kegiatan_sasaran`.`Kd_Bidang`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Unit` = `sakip_dpa_kegiatan_sasaran`.`Kd_Unit`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Sub` = `sakip_dpa_kegiatan_sasaran`.`Kd_Sub`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Bid` = `sakip_dpa_kegiatan_sasaran`.`Kd_Bid`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Pemda` = `sakip_dpa_kegiatan_sasaran`.`Kd_Pemda`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Misi` = `sakip_dpa_kegiatan_sasaran`.`Kd_Misi`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Tujuan` = `sakip_dpa_kegiatan_sasaran`.`Kd_Tujuan`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorTujuan` = `sakip_dpa_kegiatan_sasaran`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Sasaran` = `sakip_dpa_kegiatan_sasaran`.`Kd_Sasaran`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorSasaran` = `sakip_dpa_kegiatan_sasaran`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_TujuanOPD` = `sakip_dpa_kegiatan_sasaran`.`Kd_TujuanOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_kegiatan_sasaran`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_SasaranOPD` = `sakip_dpa_kegiatan_sasaran`.`Kd_SasaranOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_kegiatan_sasaran`.`Kd_IndikatorSasaranOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_ProgramOPD` = `sakip_dpa_kegiatan_sasaran`.`Kd_ProgramOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_SasaranProgramOPD` = `sakip_dpa_kegiatan_sasaran`.`Kd_SasaranProgramOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_KegiatanOPD` = `sakip_dpa_kegiatan_sasaran`.`Kd_KegiatanOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_SasaranKegiatanOPD` = `sakip_dpa_kegiatan_sasaran`.`Kd_SasaranKegiatanOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Tahun` = `sakip_dpa_kegiatan_sasaran`.`Kd_Tahun`*/
            WHERE
                `sakip_dpa_program`.`Kd_Tahun` = ?
                AND `sakip_dpa_program`.`Kd_Urusan` = ?
                AND `sakip_dpa_program`.`Kd_Bidang` = ?
                AND `sakip_dpa_program`.`Kd_Unit` = ?
                AND `sakip_dpa_program`.`Kd_Sub` = ?
                #and `sakip_dpa_program`.`Kd_ProgramOPD`  in (17)
            /*group by
              `sakip_dpa_program`.`Kd_ProgramOPD`,
              `sakip_dpa_program_sasaran`.`Kd_SasaranProgramOPD`,
              `sakip_dpa_kegiatan`.`Kd_KegiatanOPD`,
              `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorKegiatanOPD`*/
            ORDER BY
              sakip_dpa_program.Kd_Pemda,
              sakip_dpa_program.Kd_Misi,
              sakip_dpa_program.Kd_Tujuan,
              sakip_dpa_program.Kd_IndikatorTujuan,
              sakip_dpa_program.Kd_Sasaran,
              sakip_dpa_program.Kd_IndikatorSasaran,
              sakip_dpa_program.Kd_TujuanOPD,
              sakip_dpa_program.Kd_IndikatorTujuanOPD,
              sakip_dpa_program.Kd_SasaranOPD,
              sakip_dpa_program.Kd_IndikatorSasaranOPD,
              sakip_dpa_program.Kd_ProgramOPD,
              sakip_dpa_program_sasaran.Kd_SasaranProgramOPD,

              sakip_dpa_kegiatan.Kd_KegiatanOPD,
              sakip_dpa_kegiatan_indikator.Kd_IndikatorKegiatanOPD,
              sakip_dpa_program.Kd_Urusan,
              sakip_dpa_program.Kd_Bidang,
              sakip_dpa_program.Kd_Unit,
              sakip_dpa_program.Kd_Sub,
              sakip_dpa_program.Kd_Bid,
              sakip_dpa_kegiatan.Kd_Subbid
            ";
            if(!empty($sumberData) && $sumberData == 'PERUBAHAN') {
              $q = @"
              SELECT
              concat(
              sakip_pdpa_kegiatan_indikator.Kd_Pemda,
                sakip_pdpa_kegiatan_indikator.Kd_Misi,
                sakip_pdpa_kegiatan_indikator.Kd_Tujuan,
                sakip_pdpa_kegiatan_indikator.Kd_IndikatorTujuan,
                sakip_pdpa_kegiatan_indikator.Kd_Sasaran,
                sakip_pdpa_kegiatan_indikator.Kd_IndikatorSasaran,
                sakip_pdpa_kegiatan_indikator.Kd_TujuanOPD,
                sakip_pdpa_kegiatan_indikator.Kd_IndikatorTujuanOPD,
                sakip_pdpa_kegiatan_indikator.Kd_SasaranOPD,
                sakip_pdpa_kegiatan_indikator.Kd_IndikatorSasaranOPD,
                sakip_pdpa_kegiatan_indikator.Kd_ProgramOPD,
                sakip_pdpa_kegiatan_indikator.Kd_SasaranProgramOPD,

                sakip_pdpa_kegiatan_indikator.Kd_KegiatanOPD,
                sakip_pdpa_kegiatan_indikator.Kd_IndikatorKegiatanOPD,
                sakip_pdpa_kegiatan_indikator.Kd_Urusan,
                sakip_pdpa_kegiatan_indikator.Kd_Bidang,
                sakip_pdpa_kegiatan_indikator.Kd_Unit,
                sakip_pdpa_kegiatan_indikator.Kd_Sub,
                sakip_pdpa_kegiatan_indikator.Kd_Bid,
                sakip_pdpa_kegiatan_indikator.Kd_Subbid
              ) as IDD,
              sakip_pdpa_program.Uniq as UNIQ_PRG,
              sakip_pdpa_kegiatan_indikator.Uniq as UNIQ_IKKEG,
              sakip_pdpa_program_sasaran.Uniq as UNIQ_SPROG,
              sakip_pdpa_kegiatan.*,
              sakip_pdpa_program.*,
              sakip_pdpa_program.Kd_Tahun,
              sakip_pdpa_program.Nm_ProgramOPD,
              sakip_pdpa_program_sasaran.Kd_SasaranProgramOPD,
              #sakip_pdpa_program_sasaran.Kd_IndikatorProgramOPD,
              #sakip_pdpa_program_sasaran.Kd_Satuan AS Satuan1,
              #sakip_pdpa_program_sasaran.Target AS Target1,
              0 AS Total1,
              sakip_pdpa_kegiatan.Nm_KegiatanOPD,
              sakip_pdpa_kegiatan_indikator.Kd_SasaranKegiatanOPD,
              sakip_pdpa_kegiatan_indikator.Kd_IndikatorKegiatanOPD,
              sakip_pdpa_kegiatan_indikator.Nm_IndikatorKegiatanOPD,
              sakip_pdpa_kegiatan_indikator.Kd_Satuan AS Satuan2,
              sakip_pdpa_kegiatan_indikator.Target AS Target2,
              COALESCE(null, sakip_pdpa_kegiatan.Budget) AS Budget2,
              sakip_pdpa_kegiatan.Total AS Total2,
              (SELECT SUM(COALESCE(null, sakip_pdpa_kegiatan.Budget)) FROM `sakip_pdpa_kegiatan`
                  WHERE `sakip_pdpa_kegiatan`.`Kd_Urusan` = `sakip_pdpa_program_sasaran`.`Kd_Urusan`
                  AND `sakip_pdpa_kegiatan`.`Kd_Bidang` = `sakip_pdpa_program_sasaran`.`Kd_Bidang`
                  AND `sakip_pdpa_kegiatan`.`Kd_Unit` = `sakip_pdpa_program_sasaran`.`Kd_Unit`
                  AND `sakip_pdpa_kegiatan`.`Kd_Sub` = `sakip_pdpa_program_sasaran`.`Kd_Sub`
                  #AND `sakip_pdpa_kegiatan`.`Kd_Bid` = `sakip_pdpa_program_sasaran`.`Kd_Bid`
                  AND `sakip_pdpa_kegiatan`.`Kd_Pemda` = `sakip_pdpa_program_sasaran`.`Kd_Pemda`
                  AND `sakip_pdpa_kegiatan`.`Kd_Misi` = `sakip_pdpa_program_sasaran`.`Kd_Misi`
                  AND `sakip_pdpa_kegiatan`.`Kd_Tujuan` = `sakip_pdpa_program_sasaran`.`Kd_Tujuan`
                  AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorTujuan`
                  AND `sakip_pdpa_kegiatan`.`Kd_Sasaran` = `sakip_pdpa_program_sasaran`.`Kd_Sasaran`
                  AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorSasaran`
                  AND `sakip_pdpa_kegiatan`.`Kd_TujuanOPD` = `sakip_pdpa_program_sasaran`.`Kd_TujuanOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorTujuanOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_SasaranOPD` = `sakip_pdpa_program_sasaran`.`Kd_SasaranOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorSasaranOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_ProgramOPD` = `sakip_pdpa_program_sasaran`.`Kd_ProgramOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_Tahun` = `sakip_pdpa_program_sasaran`.`Kd_Tahun`
                  AND `sakip_pdpa_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_pdpa_program_sasaran`.`Kd_SasaranProgramOPD`
              ) AS sum_program_total,
              (SELECT SUM(sakip_pdpa_kegiatan.Budget) FROM `sakip_pdpa_kegiatan`
                  WHERE `sakip_pdpa_kegiatan`.`Kd_Urusan` = `sakip_pdpa_kegiatan`.`Kd_Urusan`
                  AND `sakip_pdpa_kegiatan`.`Kd_Bidang` = `sakip_pdpa_kegiatan`.`Kd_Bidang`
                  AND `sakip_pdpa_kegiatan`.`Kd_Unit` = `sakip_pdpa_kegiatan`.`Kd_Unit`
                  AND `sakip_pdpa_kegiatan`.`Kd_Sub` = `sakip_pdpa_kegiatan`.`Kd_Sub`
                  AND `sakip_pdpa_kegiatan`.`Kd_Bid` = `sakip_pdpa_kegiatan`.`Kd_Bid`
                  AND `sakip_pdpa_kegiatan`.`Kd_Pemda` = `sakip_pdpa_kegiatan`.`Kd_Pemda`
                  AND `sakip_pdpa_kegiatan`.`Kd_Misi` = `sakip_pdpa_kegiatan`.`Kd_Misi`
                  AND `sakip_pdpa_kegiatan`.`Kd_Tujuan` = `sakip_pdpa_kegiatan`.`Kd_Tujuan`
                  AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuan`
                  AND `sakip_pdpa_kegiatan`.`Kd_Sasaran` = `sakip_pdpa_kegiatan`.`Kd_Sasaran`
                  AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaran`
                  AND `sakip_pdpa_kegiatan`.`Kd_TujuanOPD` = `sakip_pdpa_kegiatan`.`Kd_TujuanOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuanOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_SasaranOPD` = `sakip_pdpa_kegiatan`.`Kd_SasaranOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaranOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_ProgramOPD` = `sakip_pdpa_kegiatan`.`Kd_ProgramOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_Tahun` = `sakip_pdpa_kegiatan`.`Kd_Tahun`
                  AND `sakip_pdpa_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_pdpa_kegiatan`.`Kd_SasaranProgramOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_KegiatanOPD` = `sakip_pdpa_kegiatan`.`Kd_KegiatanOPD`
              ) AS sum_kegiatan_total,
              (SELECT COUNT(*) FROM `sakip_pdpa_program_sasaran`
                  left join `sakip_pdpa_kegiatan` on
                    `sakip_pdpa_kegiatan`.`Kd_Urusan` = `sakip_pdpa_program_sasaran`.`Kd_Urusan`
                    AND `sakip_pdpa_kegiatan`.`Kd_Bidang` = `sakip_pdpa_program_sasaran`.`Kd_Bidang`
                    AND `sakip_pdpa_kegiatan`.`Kd_Unit` = `sakip_pdpa_program_sasaran`.`Kd_Unit`
                    AND `sakip_pdpa_kegiatan`.`Kd_Sub` = `sakip_pdpa_program_sasaran`.`Kd_Sub`
                    AND `sakip_pdpa_kegiatan`.`Kd_Bid` = `sakip_pdpa_program_sasaran`.`Kd_Bid`
                    AND `sakip_pdpa_kegiatan`.`Kd_Pemda` = `sakip_pdpa_program_sasaran`.`Kd_Pemda`
                    AND `sakip_pdpa_kegiatan`.`Kd_Misi` = `sakip_pdpa_program_sasaran`.`Kd_Misi`
                    AND `sakip_pdpa_kegiatan`.`Kd_Tujuan` = `sakip_pdpa_program_sasaran`.`Kd_Tujuan`
                    AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorTujuan`
                    AND `sakip_pdpa_kegiatan`.`Kd_Sasaran` = `sakip_pdpa_program_sasaran`.`Kd_Sasaran`
                    AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorSasaran`
                    AND `sakip_pdpa_kegiatan`.`Kd_TujuanOPD` = `sakip_pdpa_program_sasaran`.`Kd_TujuanOPD`
                    AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorTujuanOPD`
                    AND `sakip_pdpa_kegiatan`.`Kd_SasaranOPD` = `sakip_pdpa_program_sasaran`.`Kd_SasaranOPD`
                    AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorSasaranOPD`
                    AND `sakip_pdpa_kegiatan`.`Kd_ProgramOPD` = `sakip_pdpa_program_sasaran`.`Kd_ProgramOPD`
                    AND `sakip_pdpa_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_pdpa_program_sasaran`.`Kd_SasaranProgramOPD`
                    AND `sakip_pdpa_kegiatan`.`Kd_Tahun` = `sakip_pdpa_program_sasaran`.`Kd_Tahun`
                  left join `sakip_pdpa_kegiatan_indikator` on
                    `sakip_pdpa_kegiatan_indikator`.`Kd_Urusan` = `sakip_pdpa_kegiatan`.`Kd_Urusan`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_Bidang` = `sakip_pdpa_kegiatan`.`Kd_Bidang`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_Unit` = `sakip_pdpa_kegiatan`.`Kd_Unit`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_Sub` = `sakip_pdpa_kegiatan`.`Kd_Sub`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_Bid` = `sakip_pdpa_kegiatan`.`Kd_Bid`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_Subbid` = `sakip_pdpa_kegiatan`.`Kd_Subbid`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_Pemda` = `sakip_pdpa_kegiatan`.`Kd_Pemda`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_Misi` = `sakip_pdpa_kegiatan`.`Kd_Misi`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_Tujuan` = `sakip_pdpa_kegiatan`.`Kd_Tujuan`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorTujuan` = `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuan`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_Sasaran` = `sakip_pdpa_kegiatan`.`Kd_Sasaran`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorSasaran` = `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaran`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_TujuanOPD` = `sakip_pdpa_kegiatan`.`Kd_TujuanOPD`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuanOPD`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_SasaranOPD` = `sakip_pdpa_kegiatan`.`Kd_SasaranOPD`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaranOPD`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_ProgramOPD` = `sakip_pdpa_kegiatan`.`Kd_ProgramOPD`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_SasaranProgramOPD` = `sakip_pdpa_kegiatan`.`Kd_SasaranProgramOPD`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_KegiatanOPD` = `sakip_pdpa_kegiatan`.`Kd_KegiatanOPD`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_Tahun` = `sakip_pdpa_kegiatan`.`Kd_Tahun`
                  WHERE `sakip_pdpa_program_sasaran`.`Kd_Urusan` = `sakip_pdpa_program`.`Kd_Urusan`
                  AND `sakip_pdpa_program_sasaran`.`Kd_Bidang` = `sakip_pdpa_program`.`Kd_Bidang`
                  AND `sakip_pdpa_program_sasaran`.`Kd_Unit` = `sakip_pdpa_program`.`Kd_Unit`
                  AND `sakip_pdpa_program_sasaran`.`Kd_Sub` = `sakip_pdpa_program`.`Kd_Sub`
                  AND `sakip_pdpa_program_sasaran`.`Kd_Pemda` = `sakip_pdpa_program`.`Kd_Pemda`
                  AND `sakip_pdpa_program_sasaran`.`Kd_Misi` = `sakip_pdpa_program`.`Kd_Misi`
                  AND `sakip_pdpa_program_sasaran`.`Kd_Tujuan` = `sakip_pdpa_program`.`Kd_Tujuan`
                  AND `sakip_pdpa_program_sasaran`.`Kd_IndikatorTujuan` = `sakip_pdpa_program`.`Kd_IndikatorTujuan`
                  AND `sakip_pdpa_program_sasaran`.`Kd_Sasaran` = `sakip_pdpa_program`.`Kd_Sasaran`
                  AND `sakip_pdpa_program_sasaran`.`Kd_IndikatorSasaran` = `sakip_pdpa_program`.`Kd_IndikatorSasaran`
                  AND `sakip_pdpa_program_sasaran`.`Kd_TujuanOPD` = `sakip_pdpa_program`.`Kd_TujuanOPD`
                  AND `sakip_pdpa_program_sasaran`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_program`.`Kd_IndikatorTujuanOPD`
                  AND `sakip_pdpa_program_sasaran`.`Kd_SasaranOPD` = `sakip_pdpa_program`.`Kd_SasaranOPD`
                  AND `sakip_pdpa_program_sasaran`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_program`.`Kd_IndikatorSasaranOPD`
                  /*AND `sakip_pdpa_program_sasaran`.`Kd_Bid` = `sakip_pdpa_program`.`Kd_Bid`*/
                  AND `sakip_pdpa_program_sasaran`.`Kd_ProgramOPD` = `sakip_pdpa_program`.`Kd_ProgramOPD`
                  AND `sakip_pdpa_program_sasaran`.`Kd_Tahun` = `sakip_pdpa_program`.`Kd_Tahun`
              ) AS count_program_span,
              (SELECT COUNT(*) FROM `sakip_pdpa_kegiatan`
                  left join `sakip_pdpa_kegiatan_indikator` on
                    `sakip_pdpa_kegiatan_indikator`.`Kd_Urusan` = `sakip_pdpa_kegiatan`.`Kd_Urusan`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_Bidang` = `sakip_pdpa_kegiatan`.`Kd_Bidang`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_Unit` = `sakip_pdpa_kegiatan`.`Kd_Unit`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_Sub` = `sakip_pdpa_kegiatan`.`Kd_Sub`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_Bid` = `sakip_pdpa_kegiatan`.`Kd_Bid`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_Subbid` = `sakip_pdpa_kegiatan`.`Kd_Subbid`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_Pemda` = `sakip_pdpa_kegiatan`.`Kd_Pemda`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_Misi` = `sakip_pdpa_kegiatan`.`Kd_Misi`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_Tujuan` = `sakip_pdpa_kegiatan`.`Kd_Tujuan`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorTujuan` = `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuan`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_Sasaran` = `sakip_pdpa_kegiatan`.`Kd_Sasaran`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorSasaran` = `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaran`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_TujuanOPD` = `sakip_pdpa_kegiatan`.`Kd_TujuanOPD`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuanOPD`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_SasaranOPD` = `sakip_pdpa_kegiatan`.`Kd_SasaranOPD`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaranOPD`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_ProgramOPD` = `sakip_pdpa_kegiatan`.`Kd_ProgramOPD`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_SasaranProgramOPD` = `sakip_pdpa_kegiatan`.`Kd_SasaranProgramOPD`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_Tahun` = `sakip_pdpa_kegiatan`.`Kd_Tahun`
                    AND `sakip_pdpa_kegiatan_indikator`.`Kd_KegiatanOPD` = `sakip_pdpa_kegiatan`.`Kd_KegiatanOPD`
                  WHERE `sakip_pdpa_kegiatan`.`Kd_Urusan` = `sakip_pdpa_program`.`Kd_Urusan`
                  AND `sakip_pdpa_kegiatan`.`Kd_Bidang` = `sakip_pdpa_program`.`Kd_Bidang`
                  AND `sakip_pdpa_kegiatan`.`Kd_Unit` = `sakip_pdpa_program`.`Kd_Unit`
                  AND `sakip_pdpa_kegiatan`.`Kd_Sub` = `sakip_pdpa_program`.`Kd_Sub`
                  AND `sakip_pdpa_kegiatan`.`Kd_Pemda` = `sakip_pdpa_program`.`Kd_Pemda`
                  AND `sakip_pdpa_kegiatan`.`Kd_Misi` = `sakip_pdpa_program`.`Kd_Misi`
                  AND `sakip_pdpa_kegiatan`.`Kd_Tujuan` = `sakip_pdpa_program`.`Kd_Tujuan`
                  AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_pdpa_program`.`Kd_IndikatorTujuan`
                  AND `sakip_pdpa_kegiatan`.`Kd_Sasaran` = `sakip_pdpa_program`.`Kd_Sasaran`
                  AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_pdpa_program`.`Kd_IndikatorSasaran`
                  AND `sakip_pdpa_kegiatan`.`Kd_TujuanOPD` = `sakip_pdpa_program`.`Kd_TujuanOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_program`.`Kd_IndikatorTujuanOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_SasaranOPD` = `sakip_pdpa_program`.`Kd_SasaranOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_program`.`Kd_IndikatorSasaranOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_Bid` = `sakip_pdpa_program`.`Kd_Bid`
                  AND `sakip_pdpa_kegiatan`.`Kd_ProgramOPD` = `sakip_pdpa_program`.`Kd_ProgramOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_Tahun` = `sakip_pdpa_program`.`Kd_Tahun`
                  AND `sakip_pdpa_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_pdpa_program_sasaran`.`Kd_SasaranProgramOPD`
              ) AS count_sasaranprog_span,
              (SELECT COUNT(*) FROM `sakip_pdpa_kegiatan_indikator`
                  WHERE `sakip_pdpa_kegiatan_indikator`.`Kd_Urusan` = `sakip_pdpa_kegiatan`.`Kd_Urusan`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Bidang` = `sakip_pdpa_kegiatan`.`Kd_Bidang`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Unit` = `sakip_pdpa_kegiatan`.`Kd_Unit`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Sub` = `sakip_pdpa_kegiatan`.`Kd_Sub`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Bid` = `sakip_pdpa_kegiatan`.`Kd_Bid`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Subbid` = `sakip_pdpa_kegiatan`.`Kd_Subbid`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Pemda` = `sakip_pdpa_kegiatan`.`Kd_Pemda`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Misi` = `sakip_pdpa_kegiatan`.`Kd_Misi`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Tujuan` = `sakip_pdpa_kegiatan`.`Kd_Tujuan`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorTujuan` = `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuan`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Sasaran` = `sakip_pdpa_kegiatan`.`Kd_Sasaran`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorSasaran` = `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaran`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_TujuanOPD` = `sakip_pdpa_kegiatan`.`Kd_TujuanOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuanOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_SasaranOPD` = `sakip_pdpa_kegiatan`.`Kd_SasaranOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaranOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_ProgramOPD` = `sakip_pdpa_kegiatan`.`Kd_ProgramOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Tahun` = `sakip_pdpa_kegiatan`.`Kd_Tahun`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_SasaranProgramOPD` = `sakip_pdpa_kegiatan`.`Kd_SasaranProgramOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_KegiatanOPD` = `sakip_pdpa_kegiatan`.`Kd_KegiatanOPD`
              ) AS count_kegiatan_span
              FROM `sakip_pdpa_program`
              LEFT JOIN `sakip_pdpa_program_sasaran`
                  ON `sakip_pdpa_program_sasaran`.`Kd_Urusan` = `sakip_pdpa_program`.`Kd_Urusan`
                  AND `sakip_pdpa_program_sasaran`.`Kd_Bidang` = `sakip_pdpa_program`.`Kd_Bidang`
                  AND `sakip_pdpa_program_sasaran`.`Kd_Unit` = `sakip_pdpa_program`.`Kd_Unit`
                  AND `sakip_pdpa_program_sasaran`.`Kd_Sub` = `sakip_pdpa_program`.`Kd_Sub`
                  AND `sakip_pdpa_program_sasaran`.`Kd_Bid` = `sakip_pdpa_program`.`Kd_Bid`
                  AND `sakip_pdpa_program_sasaran`.`Kd_Pemda` = `sakip_pdpa_program`.`Kd_Pemda`
                  AND `sakip_pdpa_program_sasaran`.`Kd_Misi` = `sakip_pdpa_program`.`Kd_Misi`
                  AND `sakip_pdpa_program_sasaran`.`Kd_Tujuan` = `sakip_pdpa_program`.`Kd_Tujuan`
                  AND `sakip_pdpa_program_sasaran`.`Kd_IndikatorTujuan` = `sakip_pdpa_program`.`Kd_IndikatorTujuan`
                  AND `sakip_pdpa_program_sasaran`.`Kd_Sasaran` = `sakip_pdpa_program`.`Kd_Sasaran`
                  AND `sakip_pdpa_program_sasaran`.`Kd_IndikatorSasaran` = `sakip_pdpa_program`.`Kd_IndikatorSasaran`
                  AND `sakip_pdpa_program_sasaran`.`Kd_TujuanOPD` = `sakip_pdpa_program`.`Kd_TujuanOPD`
                  AND `sakip_pdpa_program_sasaran`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_program`.`Kd_IndikatorTujuanOPD`
                  AND `sakip_pdpa_program_sasaran`.`Kd_SasaranOPD` = `sakip_pdpa_program`.`Kd_SasaranOPD`
                  AND `sakip_pdpa_program_sasaran`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_program`.`Kd_IndikatorSasaranOPD`
                  AND `sakip_pdpa_program_sasaran`.`Kd_ProgramOPD` = `sakip_pdpa_program`.`Kd_ProgramOPD`
                  AND `sakip_pdpa_program_sasaran`.`Kd_Tahun` = `sakip_pdpa_program`.`Kd_Tahun`
              LEFT JOIN `sakip_pdpa_kegiatan`
                  ON `sakip_pdpa_kegiatan`.`Kd_Urusan` = `sakip_pdpa_program_sasaran`.`Kd_Urusan`
                  AND `sakip_pdpa_kegiatan`.`Kd_Bidang` = `sakip_pdpa_program_sasaran`.`Kd_Bidang`
                  AND `sakip_pdpa_kegiatan`.`Kd_Unit` = `sakip_pdpa_program_sasaran`.`Kd_Unit`
                  AND `sakip_pdpa_kegiatan`.`Kd_Sub` = `sakip_pdpa_program_sasaran`.`Kd_Sub`
                  AND `sakip_pdpa_kegiatan`.`Kd_Bid` = `sakip_pdpa_program_sasaran`.`Kd_Bid`
                  AND `sakip_pdpa_kegiatan`.`Kd_Pemda` = `sakip_pdpa_program_sasaran`.`Kd_Pemda`
                  AND `sakip_pdpa_kegiatan`.`Kd_Misi` = `sakip_pdpa_program_sasaran`.`Kd_Misi`
                  AND `sakip_pdpa_kegiatan`.`Kd_Tujuan` = `sakip_pdpa_program_sasaran`.`Kd_Tujuan`
                  AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorTujuan`
                  AND `sakip_pdpa_kegiatan`.`Kd_Sasaran` = `sakip_pdpa_program_sasaran`.`Kd_Sasaran`
                  AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorSasaran`
                  AND `sakip_pdpa_kegiatan`.`Kd_TujuanOPD` = `sakip_pdpa_program_sasaran`.`Kd_TujuanOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorTujuanOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_SasaranOPD` = `sakip_pdpa_program_sasaran`.`Kd_SasaranOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_program_sasaran`.`Kd_IndikatorSasaranOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_ProgramOPD` = `sakip_pdpa_program_sasaran`.`Kd_ProgramOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_SasaranProgramOPD` = `sakip_pdpa_program_sasaran`.`Kd_SasaranProgramOPD`
                  AND `sakip_pdpa_kegiatan`.`Kd_Tahun` = `sakip_pdpa_program_sasaran`.`Kd_Tahun`
              LEFT JOIN `sakip_pdpa_kegiatan_indikator`
                  ON `sakip_pdpa_kegiatan_indikator`.`Kd_Urusan` = `sakip_pdpa_kegiatan`.`Kd_Urusan`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Bidang` = `sakip_pdpa_kegiatan`.`Kd_Bidang`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Unit` = `sakip_pdpa_kegiatan`.`Kd_Unit`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Sub` = `sakip_pdpa_kegiatan`.`Kd_Sub`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Bid` = `sakip_pdpa_kegiatan`.`Kd_Bid`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Subbid` = `sakip_pdpa_kegiatan`.`Kd_Subbid`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Pemda` = `sakip_pdpa_kegiatan`.`Kd_Pemda`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Misi` = `sakip_pdpa_kegiatan`.`Kd_Misi`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Tujuan` = `sakip_pdpa_kegiatan`.`Kd_Tujuan`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorTujuan` = `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuan`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Sasaran` = `sakip_pdpa_kegiatan`.`Kd_Sasaran`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorSasaran` = `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaran`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_TujuanOPD` = `sakip_pdpa_kegiatan`.`Kd_TujuanOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuanOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_SasaranOPD` = `sakip_pdpa_kegiatan`.`Kd_SasaranOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaranOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_ProgramOPD` = `sakip_pdpa_kegiatan`.`Kd_ProgramOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_SasaranProgramOPD` = `sakip_pdpa_kegiatan`.`Kd_SasaranProgramOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_KegiatanOPD` = `sakip_pdpa_kegiatan`.`Kd_KegiatanOPD`
                  AND `sakip_pdpa_kegiatan_indikator`.`Kd_Tahun` = `sakip_pdpa_kegiatan`.`Kd_Tahun`
              WHERE
                  `sakip_pdpa_program`.`Kd_Tahun` = ?
                  AND `sakip_pdpa_program`.`Kd_Urusan` = ?
                  AND `sakip_pdpa_program`.`Kd_Bidang` = ?
                  AND `sakip_pdpa_program`.`Kd_Unit` = ?
                  AND `sakip_pdpa_program`.`Kd_Sub` = ?
              ORDER BY
                sakip_pdpa_program.Kd_Pemda,
                sakip_pdpa_program.Kd_Misi,
                sakip_pdpa_program.Kd_Tujuan,
                sakip_pdpa_program.Kd_IndikatorTujuan,
                sakip_pdpa_program.Kd_Sasaran,
                sakip_pdpa_program.Kd_IndikatorSasaran,
                sakip_pdpa_program.Kd_TujuanOPD,
                sakip_pdpa_program.Kd_IndikatorTujuanOPD,
                sakip_pdpa_program.Kd_SasaranOPD,
                sakip_pdpa_program.Kd_IndikatorSasaranOPD,
                sakip_pdpa_program.Kd_ProgramOPD,
                sakip_pdpa_program_sasaran.Kd_SasaranProgramOPD,

                sakip_pdpa_kegiatan.Kd_KegiatanOPD,
                sakip_pdpa_kegiatan_indikator.Kd_IndikatorKegiatanOPD,
                sakip_pdpa_program.Kd_Urusan,
                sakip_pdpa_program.Kd_Bidang,
                sakip_pdpa_program.Kd_Unit,
                sakip_pdpa_program.Kd_Sub,
                sakip_pdpa_program.Kd_Bid,
                sakip_pdpa_kegiatan.Kd_Subbid
              ";
            }
            $params = array(
                $this->input->post(COL_KD_TAHUN),
                $this->input->post(COL_KD_URUSAN),
                $this->input->post(COL_KD_BIDANG),
                $this->input->post(COL_KD_UNIT),
                $this->input->post(COL_KD_SUB),
                /*TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PROGRAMOPD.','.
                //TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_SASARANPROGRAMOPD.' asc,'.
                TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_INDIKATORPROGRAMOPD//.','.
                //TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_KEGIATANOPD.','.
                //TBL_SAKIP_DPA_KEGIATAN_SASARAN.".".COL_KD_SASARANKEGIATANOPD.' asc,'//.
                //TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_INDIKATORKEGIATANOPD.''*/
            );
            $rprogram = $this->db->query($q, $params)->result_array();
            /*$this->db->where(TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_TAHUN, $this->input->post(COL_KD_TAHUN));
            $this->db->where(TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_URUSAN, $this->input->post(COL_KD_URUSAN));
            $this->db->where(TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_BIDANG, $this->input->post(COL_KD_BIDANG));
            $this->db->where(TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_UNIT, $this->input->post(COL_KD_UNIT));
            $this->db->where(TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_SUB, $this->input->post(COL_KD_SUB));
            $this->db->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PROGRAMOPD, 'asc');
            $rprogram = $this->db->get(TBL_SAKIP_DPA_PROGRAM)->result_array();*/
            //$rprogram = $query->result_array();
            $dat['program'] = $rprogram;
            $dat['edit'] = true;
            $dat['data'] = array(
                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN), //count($rprogram) > 0 ? $rprogram[0][COL_KD_TAHUN] : date("Y"),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB)
            );
        }

        $nmSub = "";
        $strOPD = explode('.', $ruser[COL_COMPANYID]);
        $eplandb = $this->load->database("eplan", true);
        if($dat['edit']) {
            $eplandb->where(COL_KD_URUSAN, $dat['data'][COL_KD_URUSAN]);
            $eplandb->where(COL_KD_BIDANG, $dat['data'][COL_KD_BIDANG]);
            $eplandb->where(COL_KD_UNIT, $dat['data'][COL_KD_UNIT]);
            $eplandb->where(COL_KD_SUB, $dat['data'][COL_KD_SUB]);
            $subunit = $eplandb->get("ref_sub_unit")->row_array();
            if($subunit) {
                $nmSub = $subunit["Nm_Sub_Unit"];
            }
        }
        if($ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEKEUANGAN) {
            $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
            $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
            $eplandb->where(COL_KD_UNIT, $strOPD[2]);
            $eplandb->where(COL_KD_SUB, $strOPD[3]);
            $subunit = $eplandb->get("ref_sub_unit")->row_array();
            if($subunit) {
                $nmSub = $subunit["Nm_Sub_Unit"];
            }
        }
        $dat['strOPD'] = $strOPD;
        $dat['nmSub'] = $nmSub;
        $dat['eplandb'] = $eplandb;

        if($cetak) $this->load->view('report/dpa_partial', $dat);
        else $this->load->view('report/dpa', $dat);
    }

    function evaluasi_old() {
        $ruser = GetLoggedUser();
        $dat['title'] = "Evaluasi Kinerja";
        $dat['edit'] = false;
        $dat['cetak'] = $cetak = $this->input->post("cetak");

        if(!empty($_POST)) {
            $this->db->where(COL_KD_TAHUN_FROM." <=", date("Y"));
            $this->db->where(COL_KD_TAHUN_TO." >=", date("Y"));
            $this->db->order_by(COL_KD_TAHUN_FROM, 'desc');
            $dat['rpemda'] = $rpemda = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();

            $params = array(
                $this->input->post(COL_KD_TAHUN),
                $this->input->post(COL_KD_URUSAN),
                $this->input->post(COL_KD_BIDANG),
                $this->input->post(COL_KD_UNIT),
                $this->input->post(COL_KD_SUB)
            );
            $q = @"
            SELECT
            CONCAT(sakip_dpa_program_indikator.Kd_Pemda,'.',
                sakip_dpa_program_indikator.Kd_Misi,'.',
                sakip_dpa_program_indikator.Kd_Tujuan,'.',
                sakip_dpa_program_indikator.Kd_IndikatorTujuan,'.',
                sakip_dpa_program_indikator.Kd_Sasaran,'.',
                sakip_dpa_program_indikator.Kd_IndikatorSasaran,'.',
                sakip_dpa_program_indikator.Kd_TujuanOPD,'.',
                sakip_dpa_program_indikator.Kd_IndikatorTujuanOPD,'.',
                sakip_dpa_program_indikator.Kd_SasaranOPD,'.',
                sakip_dpa_program_indikator.Kd_IndikatorSasaranOPD,'.',
                sakip_dpa_program_indikator.Kd_ProgramOPD,'.',
                sakip_dpa_program_indikator.Kd_Urusan,'.',
                sakip_dpa_program_indikator.Kd_Bidang,'.',
                sakip_dpa_program_indikator.Kd_Unit,'.',
                sakip_dpa_program_indikator.Kd_Sub,'.',
                sakip_dpa_program_indikator.Kd_Bid
            ) AS UNIQ_PRG,
            CONCAT(sakip_dpa_program_indikator.Kd_Pemda,'.',
                sakip_dpa_program_indikator.Kd_Misi,'.',
                sakip_dpa_program_indikator.Kd_Tujuan,'.',
                sakip_dpa_program_indikator.Kd_IndikatorTujuan,'.',
                sakip_dpa_program_indikator.Kd_Sasaran,'.',
                sakip_dpa_program_indikator.Kd_IndikatorSasaran,'.',
                sakip_dpa_program_indikator.Kd_TujuanOPD,'.',
                sakip_dpa_program_indikator.Kd_IndikatorTujuanOPD,'.',
                sakip_dpa_program_indikator.Kd_SasaranOPD
            ) AS UNIQ_SASARAN_OPD,
            sakip_mopd_sasaran.Nm_SasaranOPD,
            sakip_dpa_program.Kd_Tahun,
            sakip_dpa_program.Nm_ProgramOPD,
            (
                SELECT COUNT(*) FROM sakip_dpa_kegiatan_indikator
                WHERE
                    sakip_dpa_kegiatan_indikator.`Kd_Urusan` = sakip_mopd_sasaran.`Kd_Urusan`
                    AND sakip_dpa_kegiatan_indikator.`Kd_Bidang` = sakip_mopd_sasaran.`Kd_Bidang`
                    AND sakip_dpa_kegiatan_indikator.`Kd_Unit` = sakip_mopd_sasaran.`Kd_Unit`
                    AND sakip_dpa_kegiatan_indikator.`Kd_Sub` = sakip_mopd_sasaran.`Kd_Sub`
                    AND sakip_dpa_kegiatan_indikator.`Kd_Pemda` = sakip_mopd_sasaran.`Kd_Pemda`
                    /*AND sakip_dpa_kegiatan_indikator.`Kd_Misi` = sakip_mopd_sasaran.`Kd_Misi`
                    AND sakip_dpa_kegiatan_indikator.`Kd_Tujuan` = sakip_mopd_sasaran.`Kd_Tujuan`
                    AND sakip_dpa_kegiatan_indikator.`Kd_IndikatorTujuan` = sakip_mopd_sasaran.`Kd_IndikatorTujuan`
                    AND sakip_dpa_kegiatan_indikator.`Kd_Sasaran` = sakip_mopd_sasaran.`Kd_Sasaran`
                    AND sakip_dpa_kegiatan_indikator.`Kd_IndikatorSasaran` = sakip_mopd_sasaran.`Kd_IndikatorSasaran`*/
                    AND sakip_dpa_kegiatan_indikator.`Kd_TujuanOPD` = sakip_mopd_sasaran.`Kd_TujuanOPD`
                    AND sakip_dpa_kegiatan_indikator.`Kd_IndikatorTujuanOPD` = sakip_mopd_sasaran.`Kd_IndikatorTujuanOPD`
                    AND sakip_dpa_kegiatan_indikator.`Kd_SasaranOPD` = sakip_mopd_sasaran.`Kd_SasaranOPD`
            ) AS count_sasaranopd_sasarankegiatan,
            (
                SELECT COUNT(*) FROM sakip_dpa_program_indikator
                WHERE
                    sakip_dpa_program_indikator.`Kd_Urusan` = sakip_mopd_sasaran.`Kd_Urusan`
                    AND sakip_dpa_program_indikator.`Kd_Bidang` = sakip_mopd_sasaran.`Kd_Bidang`
                    AND sakip_dpa_program_indikator.`Kd_Unit` = sakip_mopd_sasaran.`Kd_Unit`
                    AND sakip_dpa_program_indikator.`Kd_Sub` = sakip_mopd_sasaran.`Kd_Sub`
                    AND sakip_dpa_program_indikator.`Kd_Pemda` = sakip_mopd_sasaran.`Kd_Pemda`
                    /*AND sakip_dpa_program_indikator.`Kd_Misi` = sakip_mopd_sasaran.`Kd_Misi`
                    AND sakip_dpa_program_indikator.`Kd_Tujuan` = sakip_mopd_sasaran.`Kd_Tujuan`
                    AND sakip_dpa_program_indikator.`Kd_IndikatorTujuan` = sakip_mopd_sasaran.`Kd_IndikatorTujuan`
                    AND sakip_dpa_program_indikator.`Kd_Sasaran` = sakip_mopd_sasaran.`Kd_Sasaran`
                    AND sakip_dpa_program_indikator.`Kd_IndikatorSasaran` = sakip_mopd_sasaran.`Kd_IndikatorSasaran`*/
                    AND sakip_dpa_program_indikator.`Kd_TujuanOPD` = sakip_mopd_sasaran.`Kd_TujuanOPD`
                    AND sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD` = sakip_mopd_sasaran.`Kd_IndikatorTujuanOPD`
                    AND sakip_dpa_program_indikator.`Kd_SasaranOPD` = sakip_mopd_sasaran.`Kd_SasaranOPD`
            ) AS count_sasaranopd_sasaranprogram,
            (
                SELECT COUNT(*) FROM sakip_dpa_kegiatan_indikator
                WHERE
                    sakip_dpa_kegiatan_indikator.`Kd_Urusan` = sakip_dpa_program.`Kd_Urusan`
                    AND sakip_dpa_kegiatan_indikator.`Kd_Bidang` = sakip_dpa_program.`Kd_Bidang`
                    AND sakip_dpa_kegiatan_indikator.`Kd_Unit` = sakip_dpa_program.`Kd_Unit`
                    AND sakip_dpa_kegiatan_indikator.`Kd_Sub` = sakip_dpa_program.`Kd_Sub`
                    AND sakip_dpa_kegiatan_indikator.`Kd_Bid` = sakip_dpa_program.`Kd_Bid`
                    AND sakip_dpa_kegiatan_indikator.`Kd_Misi` = sakip_dpa_program.`Kd_Misi`
                    AND sakip_dpa_kegiatan_indikator.`Kd_Tujuan` = sakip_dpa_program.`Kd_Tujuan`
                    AND sakip_dpa_kegiatan_indikator.`Kd_IndikatorTujuan` = sakip_dpa_program.`Kd_IndikatorTujuan`
                    AND sakip_dpa_kegiatan_indikator.`Kd_Sasaran` = sakip_dpa_program.`Kd_Sasaran`
                    AND sakip_dpa_kegiatan_indikator.`Kd_IndikatorSasaran` = sakip_dpa_program.`Kd_IndikatorSasaran`
                    AND sakip_dpa_kegiatan_indikator.`Kd_TujuanOPD` = sakip_dpa_program.`Kd_TujuanOPD`
                    AND sakip_dpa_kegiatan_indikator.`Kd_IndikatorTujuanOPD` = sakip_dpa_program.`Kd_IndikatorTujuanOPD`
                    AND sakip_dpa_kegiatan_indikator.`Kd_SasaranOPD` = sakip_dpa_program.`Kd_SasaranOPD`
                    AND sakip_dpa_kegiatan_indikator.`Kd_IndikatorSasaranOPD` = sakip_dpa_program.`Kd_IndikatorSasaranOPD`
                    AND sakip_dpa_kegiatan_indikator.`Kd_ProgramOPD` = sakip_dpa_program.`Kd_ProgramOPD`
                    AND sakip_dpa_kegiatan_indikator.`Kd_Tahun` = sakip_dpa_program.`Kd_Tahun`
            ) AS count_program_sasarankegiatan,
            (
                SELECT SUM(sas.Kinerja_TW1 + sas.Kinerja_TW2 + sas.Kinerja_TW3 + sas.Kinerja_TW4) FROM sakip_dpa_program_indikator sas
                WHERE
                    sas.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                    AND sas.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                    AND sas.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                    AND sas.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                    AND sas.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
                    AND sas.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                    AND sas.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                    AND sas.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                    AND sas.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                    AND sas.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                    AND sas.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                    AND sas.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                    AND sas.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
                    AND sas.`Kd_IndikatorSasaranOPD` = sakip_dpa_program_indikator.`Kd_IndikatorSasaranOPD`
                    AND sas.`Kd_ProgramOPD` = sakip_dpa_program_indikator.`Kd_ProgramOPD`
                    AND sas.`Kd_Tahun` >= sakip_mpemda.`Kd_Tahun_From`
                    AND sas.`Kd_Tahun` < sakip_dpa_program_indikator.`Kd_Tahun`
                    AND sas.`Kd_SasaranProgramOPD` = sakip_dpa_program_indikator.`Kd_SasaranProgramOPD`
            ) AS kinerja_akumulasi,
            (
                SELECT SUM(IFNULL(sas.Anggaran_TW1, 0) + IFNULL(sas.Anggaran_TW2, 0) + IFNULL(sas.Anggaran_TW3, 0) + IFNULL(sas.Anggaran_TW4, 0)) FROM sakip_dpa_kegiatan sas
                WHERE
                    sas.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                    AND sas.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                    AND sas.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                    AND sas.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                    AND sas.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
                    AND sas.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                    AND sas.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                    AND sas.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                    AND sas.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                    AND sas.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                    AND sas.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                    AND sas.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                    AND sas.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
                    AND sas.`Kd_IndikatorSasaranOPD` = sakip_dpa_program_indikator.`Kd_IndikatorSasaranOPD`
                    AND sas.`Kd_ProgramOPD` = sakip_dpa_program_indikator.`Kd_ProgramOPD`
                    AND sas.`Kd_Tahun` >= sakip_mpemda.`Kd_Tahun_From`
                    AND sas.`Kd_Tahun` < sakip_dpa_program_indikator.`Kd_Tahun`
                    AND sas.`Kd_SasaranProgramOPD` = sakip_dpa_program_indikator.`Kd_SasaranProgramOPD`
            ) AS anggaran_akumulasi,
            (
                SELECT SUM(IFNULL(sas.Budget, 0)) FROM sakip_dpa_kegiatan sas
                WHERE
                    sas.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                    AND sas.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                    AND sas.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                    AND sas.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                    AND sas.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
                    AND sas.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                    AND sas.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                    AND sas.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                    AND sas.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                    AND sas.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                    AND sas.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                    AND sas.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                    AND sas.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
                    AND sas.`Kd_IndikatorSasaranOPD` = sakip_dpa_program_indikator.`Kd_IndikatorSasaranOPD`
                    AND sas.`Kd_ProgramOPD` = sakip_dpa_program_indikator.`Kd_ProgramOPD`
                    AND sas.`Kd_Tahun` = sakip_dpa_program_indikator.`Kd_Tahun`
                    AND sas.`Kd_SasaranProgramOPD` = sakip_dpa_program_indikator.`Kd_SasaranProgramOPD`
            ) AS budget_program,
            (
                SELECT SUM(IFNULL(sas.Anggaran_TW1, 0)) FROM sakip_dpa_kegiatan sas
                WHERE
                    sas.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                    AND sas.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                    AND sas.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                    AND sas.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                    AND sas.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
                    AND sas.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                    AND sas.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                    AND sas.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                    AND sas.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                    AND sas.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                    AND sas.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                    AND sas.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                    AND sas.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
                    AND sas.`Kd_IndikatorSasaranOPD` = sakip_dpa_program_indikator.`Kd_IndikatorSasaranOPD`
                    AND sas.`Kd_ProgramOPD` = sakip_dpa_program_indikator.`Kd_ProgramOPD`
                    AND sas.`Kd_Tahun` = sakip_dpa_program_indikator.`Kd_Tahun`
                    AND sas.`Kd_SasaranProgramOPD` = sakip_dpa_program_indikator.`Kd_SasaranProgramOPD`
            ) AS anggaran_tw1_program,
            (
                SELECT SUM(IFNULL(sas.Anggaran_TW2, 0)) FROM sakip_dpa_kegiatan sas
                WHERE
                    sas.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                    AND sas.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                    AND sas.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                    AND sas.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                    AND sas.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
                    AND sas.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                    AND sas.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                    AND sas.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                    AND sas.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                    AND sas.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                    AND sas.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                    AND sas.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                    AND sas.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
                    AND sas.`Kd_IndikatorSasaranOPD` = sakip_dpa_program_indikator.`Kd_IndikatorSasaranOPD`
                    AND sas.`Kd_ProgramOPD` = sakip_dpa_program_indikator.`Kd_ProgramOPD`
                    AND sas.`Kd_Tahun` = sakip_dpa_program_indikator.`Kd_Tahun`
                    AND sas.`Kd_SasaranProgramOPD` = sakip_dpa_program_indikator.`Kd_SasaranProgramOPD`
            ) AS anggaran_tw2_program,
            (
                SELECT SUM(IFNULL(sas.Anggaran_TW3, 0)) FROM sakip_dpa_kegiatan sas
                WHERE
                    sas.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                    AND sas.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                    AND sas.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                    AND sas.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                    AND sas.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
                    AND sas.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                    AND sas.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                    AND sas.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                    AND sas.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                    AND sas.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                    AND sas.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                    AND sas.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                    AND sas.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
                    AND sas.`Kd_IndikatorSasaranOPD` = sakip_dpa_program_indikator.`Kd_IndikatorSasaranOPD`
                    AND sas.`Kd_ProgramOPD` = sakip_dpa_program_indikator.`Kd_ProgramOPD`
                    AND sas.`Kd_Tahun` = sakip_dpa_program_indikator.`Kd_Tahun`
                    AND sas.`Kd_SasaranProgramOPD` = sakip_dpa_program_indikator.`Kd_SasaranProgramOPD`
            ) AS anggaran_tw3_program,
            (
                SELECT SUM(IFNULL(sas.Anggaran_TW4, 0)) FROM sakip_dpa_kegiatan sas
                WHERE
                    sas.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                    AND sas.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                    AND sas.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                    AND sas.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                    AND sas.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
                    AND sas.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                    AND sas.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                    AND sas.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                    AND sas.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                    AND sas.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                    AND sas.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                    AND sas.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                    AND sas.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
                    AND sas.`Kd_IndikatorSasaranOPD` = sakip_dpa_program_indikator.`Kd_IndikatorSasaranOPD`
                    AND sas.`Kd_ProgramOPD` = sakip_dpa_program_indikator.`Kd_ProgramOPD`
                    AND sas.`Kd_Tahun` = sakip_dpa_program_indikator.`Kd_Tahun`
                    AND sas.`Kd_SasaranProgramOPD` = sakip_dpa_program_indikator.`Kd_SasaranProgramOPD`
            ) AS anggaran_tw4_program,
            (
                SELECT SUM(IFNULL(keg.Total, 0)) FROM sakip_msubbid_kegiatan keg
                WHERE
                    keg.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                    AND keg.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                    AND keg.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                    AND keg.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                    AND keg.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
                    AND keg.`Kd_Pemda` = sakip_dpa_program_indikator.`Kd_Pemda`
                    AND keg.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                    AND keg.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                    AND keg.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                    AND keg.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                    AND keg.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                    AND keg.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                    AND keg.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                    AND keg.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
                    AND keg.`Kd_IndikatorSasaranOPD` = sakip_dpa_program_indikator.`Kd_IndikatorSasaranOPD`
                    AND keg.`Kd_ProgramOPD` = sakip_dpa_program_indikator.`Kd_ProgramOPD`
                    AND keg.`Kd_SasaranProgramOPD` = sakip_dpa_program_indikator.`Kd_SasaranProgramOPD`
                    AND keg.`Kd_KegiatanOPD` in (
                      select DISTINCT(dpa_keg.Kd_KegiatanOPD) from sakip_dpa_kegiatan dpa_keg
                      where dpa_keg.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                        AND dpa_keg.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                        AND dpa_keg.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                        AND dpa_keg.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                        AND dpa_keg.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
                        AND dpa_keg.`Kd_Pemda` = sakip_dpa_program_indikator.`Kd_Pemda`
                        AND dpa_keg.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                        AND dpa_keg.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                        AND dpa_keg.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                        AND dpa_keg.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                        AND dpa_keg.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                        AND dpa_keg.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                        AND dpa_keg.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                        AND dpa_keg.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
                        AND dpa_keg.`Kd_IndikatorSasaranOPD` = sakip_dpa_program_indikator.`Kd_IndikatorSasaranOPD`
                        AND dpa_keg.`Kd_ProgramOPD` = sakip_dpa_program_indikator.`Kd_ProgramOPD`
                        AND dpa_keg.`Kd_SasaranProgramOPD` = sakip_dpa_program_indikator.`Kd_SasaranProgramOPD`
                    )
            ) AS anggaran_renstra,
            #sakip_mbid_program_sasaran.Akhir as target_renstra,
            (
                SELECT SUM(IFNULL(prog.Target, 0)) FROM sakip_mbid_program_indikator prog
                WHERE
                    prog.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                    AND prog.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                    AND prog.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                    AND prog.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                    AND prog.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
                    AND prog.`Kd_Pemda` = sakip_dpa_program_indikator.`Kd_Pemda`
                    AND prog.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                    AND prog.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                    AND prog.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                    AND prog.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                    AND prog.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                    AND prog.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                    AND prog.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                    AND prog.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
                    AND prog.`Kd_IndikatorSasaranOPD` = sakip_dpa_program_indikator.`Kd_IndikatorSasaranOPD`
                    AND prog.`Kd_ProgramOPD` in (
                      select DISTINCT(dpa_prog.Kd_ProgramOPD) from sakip_dpa_program dpa_prog
                      where dpa_prog.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                        AND dpa_prog.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                        AND dpa_prog.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                        AND dpa_prog.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                        AND dpa_prog.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
                        AND dpa_prog.`Kd_Pemda` = sakip_dpa_program_indikator.`Kd_Pemda`
                        AND dpa_prog.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                        AND dpa_prog.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                        AND dpa_prog.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                        AND dpa_prog.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                        AND dpa_prog.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                        AND dpa_prog.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                        AND dpa_prog.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                        AND dpa_prog.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
                        AND dpa_prog.`Kd_IndikatorSasaranOPD` = sakip_dpa_program_indikator.`Kd_IndikatorSasaranOPD`
                    )
                    AND prog.`Kd_SasaranProgramOPD` = sakip_dpa_program_indikator.`Kd_SasaranProgramOPD`
                    AND prog.`Kd_IndikatorProgramOPD` = sakip_dpa_program_indikator.`Kd_IndikatorProgramOPD`
            ) as target_renstra,
            sakip_mbid.Nm_Bid,
            sakip_dpa_program_indikator.*
            FROM sakip_dpa_program_indikator
            LEFT JOIN sakip_mpemda
                ON sakip_mpemda.Kd_Pemda = sakip_dpa_program_indikator.Kd_Pemda
            LEFT JOIN sakip_mbid
                ON sakip_mbid.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                AND sakip_mbid.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                AND sakip_mbid.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                AND sakip_mbid.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                AND sakip_mbid.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
            LEFT JOIN sakip_mopd_sasaran
                ON sakip_mopd_sasaran.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                AND sakip_mopd_sasaran.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                AND sakip_mopd_sasaran.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                AND sakip_mopd_sasaran.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                AND sakip_mopd_sasaran.`Kd_Pemda` = sakip_dpa_program_indikator.`Kd_Pemda`
                AND sakip_mopd_sasaran.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                AND sakip_mopd_sasaran.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                AND sakip_mopd_sasaran.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                AND sakip_mopd_sasaran.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                AND sakip_mopd_sasaran.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                AND sakip_mopd_sasaran.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                AND sakip_mopd_sasaran.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                AND sakip_mopd_sasaran.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
            LEFT JOIN sakip_dpa_program
                ON sakip_dpa_program.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                AND sakip_dpa_program.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                AND sakip_dpa_program.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                AND sakip_dpa_program.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                AND sakip_dpa_program.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
                AND sakip_dpa_program.`Kd_Pemda` = sakip_dpa_program_indikator.`Kd_Pemda`
                AND sakip_dpa_program.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                AND sakip_dpa_program.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                AND sakip_dpa_program.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                AND sakip_dpa_program.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                AND sakip_dpa_program.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                AND sakip_dpa_program.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                AND sakip_dpa_program.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                AND sakip_dpa_program.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
                AND sakip_dpa_program.`Kd_IndikatorSasaranOPD` = sakip_dpa_program_indikator.`Kd_IndikatorSasaranOPD`
                AND sakip_dpa_program.`Kd_ProgramOPD` = sakip_dpa_program_indikator.`Kd_ProgramOPD`
                AND sakip_dpa_program.`Kd_Tahun` = sakip_dpa_program_indikator.`Kd_Tahun`
            LEFT JOIN sakip_mbid_program_sasaran
                ON sakip_mbid_program_sasaran.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                AND sakip_mbid_program_sasaran.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                AND sakip_mbid_program_sasaran.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                AND sakip_mbid_program_sasaran.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                AND sakip_mbid_program_sasaran.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
                AND sakip_mbid_program_sasaran.`Kd_Pemda` = sakip_dpa_program_indikator.`Kd_Pemda`
                AND sakip_mbid_program_sasaran.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                AND sakip_mbid_program_sasaran.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                AND sakip_mbid_program_sasaran.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                AND sakip_mbid_program_sasaran.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                AND sakip_mbid_program_sasaran.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                AND sakip_mbid_program_sasaran.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                AND sakip_mbid_program_sasaran.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                AND sakip_mbid_program_sasaran.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
                AND sakip_mbid_program_sasaran.`Kd_IndikatorSasaranOPD` = sakip_dpa_program_indikator.`Kd_IndikatorSasaranOPD`
                AND sakip_mbid_program_sasaran.`Kd_ProgramOPD` = sakip_dpa_program_indikator.`Kd_ProgramOPD`
                AND sakip_mbid_program_sasaran.`Kd_Tahun` = sakip_mpemda.Kd_Tahun_To
                AND sakip_mbid_program_sasaran.`Kd_SasaranProgramOPD` = sakip_dpa_program_indikator.`Kd_SasaranProgramOPD`
                AND sakip_mbid_program_sasaran.`Kd_Satuan` = sakip_dpa_program_indikator.`Kd_Satuan`
            WHERE
                sakip_dpa_program_indikator.`Kd_Tahun` = ?
                AND sakip_dpa_program_indikator.`Kd_Urusan` = ?
                AND sakip_dpa_program_indikator.`Kd_Bidang` = ?
                AND sakip_dpa_program_indikator.`Kd_Unit` = ?
                AND sakip_dpa_program_indikator.`Kd_Sub` = ?
            group by
                sakip_dpa_program_indikator.Kd_TujuanOPD,
                sakip_dpa_program_indikator.Kd_IndikatorTujuanOPD,
                sakip_dpa_program_indikator.Kd_SasaranOPD,
                sakip_dpa_program_indikator.Kd_IndikatorSasaranOPD,
                sakip_dpa_program_indikator.Kd_ProgramOPD,
                sakip_dpa_program_indikator.Kd_SasaranProgramOPD,
                sakip_dpa_program_indikator.Kd_IndikatorProgramOPD
            ORDER BY
                sakip_dpa_program_indikator.Kd_SasaranOPD,
                sakip_dpa_program_indikator.Kd_ProgramOPD,
                sakip_dpa_program_indikator.Kd_SasaranProgramOPD,
                sakip_dpa_program_indikator.Kd_IndikatorProgramOPD
            ";
            $dat['rprogram'] = $res = $this->db->query($q, $params)->result_array();
            $dat['edit'] = true;
            $dat['data'] = array(
                COL_KD_TAHUN => !empty($this->input->post(COL_KD_TAHUN)) ? $this->input->post(COL_KD_TAHUN) : date("Y"),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB)
            );
        }

        $nmSub = "";
        $strOPD = explode('.', $ruser[COL_COMPANYID]);
        $eplandb = $this->load->database("eplan", true);
        if($dat['edit']) {
            $eplandb->where(COL_KD_URUSAN, $dat['data'][COL_KD_URUSAN]);
            $eplandb->where(COL_KD_BIDANG, $dat['data'][COL_KD_BIDANG]);
            $eplandb->where(COL_KD_UNIT, $dat['data'][COL_KD_UNIT]);
            $eplandb->where(COL_KD_SUB, $dat['data'][COL_KD_SUB]);
            $subunit = $eplandb->get("ref_sub_unit")->row_array();
            if($subunit) {
                $nmSub = $subunit["Nm_Sub_Unit"];
            }
        }
        if($ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEKEUANGAN) {
            $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
            $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
            $eplandb->where(COL_KD_UNIT, $strOPD[2]);
            $eplandb->where(COL_KD_SUB, $strOPD[3]);
            $subunit = $eplandb->get("ref_sub_unit")->row_array();
            if($subunit) {
                $nmSub = $subunit["Nm_Sub_Unit"];
            }
        }
        $dat['strOPD'] = $strOPD;
        $dat['nmSub'] = $nmSub;
        $dat['eplandb'] = $eplandb;
        if($cetak) $this->load->view('report/evaluasi_partial', $dat);
        else $this->load->view('report/evaluasi', $dat);
    }

    function evaluasi() {
        $ruser = GetLoggedUser();
        $dat['title'] = "Evaluasi Kinerja";
        $dat['edit'] = false;
        $dat['cetak'] = $cetak = $this->input->post("cetak");
        $dat['SumberData'] = $this->input->post("SumberData");

        $tblpref = "sakip_dpa";
        if($dat['SumberData'] == "PERUBAHAN") {
          $tblpref = "sakip_pdpa";
        }

        if(!empty($_POST)) {
            $dat['rprogram'] = $res = array();
            $dat['edit'] = true;

            $params = array(
                $this->input->post(COL_KD_URUSAN),
                $this->input->post(COL_KD_BIDANG),
                $this->input->post(COL_KD_UNIT),
                $this->input->post(COL_KD_SUB)
            );

            $q = "
            SELECT
            CONCAT(
                s.Kd_Pemda,
                s.Kd_TujuanOPD,
                s.Kd_IndikatorTujuanOPD,
                s.Kd_SasaranOPD
            ) AS UNIQ_SASARAN_OPD,
            s.*,
            (
                SELECT COUNT(DISTINCT
                        s_.Kd_Pemda,
                        s_.Kd_Urusan,
                        s_.Kd_Bidang,
                        s_.Kd_Unit,
                        s_.Kd_Sub,
                        i.Kd_Bid,
                        i.Kd_Subbid,
                        s_.Kd_TujuanOPD,
                        s_.Kd_IndikatorTujuanOPD,
                        s_.Kd_SasaranOPD,
                        i.Kd_IndikatorSasaranOPD,
                        i.Kd_ProgramOPD,
                        i.Kd_Tahun,
                        i.Kd_SasaranProgramOPD,
                        i.Kd_KegiatanOPD,
                        i.Kd_SasaranKegiatanOPD,
                        i.Kd_IndikatorKegiatanOPD
                ) FROM @@TBLPREV@@_kegiatan_indikator i
                INNER JOIN sakip_mopd_sasaran s_
                        ON s_.`Kd_Urusan` = i.`Kd_Urusan`
                        AND s_.`Kd_Bidang` = i.`Kd_Bidang`
                        AND s_.`Kd_Unit` = i.`Kd_Unit`
                        AND s_.`Kd_Sub` = i.`Kd_Sub`
                        AND s_.`Kd_Pemda` = i.`Kd_Pemda`
                        AND s_.`Kd_Misi` = i.`Kd_Misi`
                        AND s_.`Kd_Tujuan` = i.`Kd_Tujuan`
                        AND s_.`Kd_IndikatorTujuan` = i.`Kd_IndikatorTujuan`
                        AND s_.`Kd_Sasaran` = i.`Kd_Sasaran`
                        AND s_.`Kd_IndikatorSasaran` = i.`Kd_IndikatorSasaran`
                        AND s_.`Kd_TujuanOPD` = i.`Kd_TujuanOPD`
                        AND s_.`Kd_IndikatorTujuanOPD` = i.`Kd_IndikatorTujuanOPD`
                        AND s_.`Kd_SasaranOPD` = i.`Kd_SasaranOPD`
                WHERE
                    i.`Kd_Urusan` = s.`Kd_Urusan`
                    AND i.`Kd_Bidang` = s.`Kd_Bidang`
                    AND i.`Kd_Unit` = s.`Kd_Unit`
                    AND i.`Kd_Sub` = s.`Kd_Sub`
                    AND i.`Kd_Pemda` = s.`Kd_Pemda`
                    AND i.`Kd_TujuanOPD` = s.`Kd_TujuanOPD`
                    AND i.`Kd_IndikatorTujuanOPD` = s.`Kd_IndikatorTujuanOPD`
                    AND i.`Kd_SasaranOPD` = s.`Kd_SasaranOPD`
            ) AS count_ikkegiatan,
            (
                SELECT COUNT(DISTINCT
                        s_.Kd_Pemda,
                        s_.Kd_Urusan,
                        s_.Kd_Bidang,
                        s_.Kd_Unit,
                        s_.Kd_Sub,
                        i.Kd_Bid,
                        s_.Kd_TujuanOPD,
                        s_.Kd_IndikatorTujuanOPD,
                        s_.Kd_SasaranOPD,
                        i.Kd_IndikatorSasaranOPD,
                        i.Kd_ProgramOPD,
                        i.Kd_Tahun,
                        i.Kd_SasaranProgramOPD,
                        i.Kd_IndikatorProgramOPD
                ) FROM @@TBLPREV@@_program_indikator i
                INNER JOIN sakip_mopd_sasaran s_
                    ON s_.`Kd_Urusan` = i.`Kd_Urusan`
                    AND s_.`Kd_Bidang` = i.`Kd_Bidang`
                    AND s_.`Kd_Unit` = i.`Kd_Unit`
                    AND s_.`Kd_Sub` = i.`Kd_Sub`
                    AND s_.`Kd_Pemda` = i.`Kd_Pemda`
                    AND s_.`Kd_Misi` = i.`Kd_Misi`
                    AND s_.`Kd_Tujuan` = i.`Kd_Tujuan`
                    AND s_.`Kd_IndikatorTujuan` = i.`Kd_IndikatorTujuan`
                    AND s_.`Kd_Sasaran` = i.`Kd_Sasaran`
                    AND s_.`Kd_IndikatorSasaran` = i.`Kd_IndikatorSasaran`
                    AND s_.`Kd_TujuanOPD` = i.`Kd_TujuanOPD`
                    AND s_.`Kd_IndikatorTujuanOPD` = i.`Kd_IndikatorTujuanOPD`
                    AND s_.`Kd_SasaranOPD` = i.`Kd_SasaranOPD`
                WHERE
                    i.`Kd_Urusan` = s.`Kd_Urusan`
                    AND i.`Kd_Bidang` = s.`Kd_Bidang`
                    AND i.`Kd_Unit` = s.`Kd_Unit`
                    AND i.`Kd_Sub` = s.`Kd_Sub`
                    AND i.`Kd_Pemda` = s.`Kd_Pemda`
                    AND i.`Kd_TujuanOPD` = s.`Kd_TujuanOPD`
                    AND i.`Kd_IndikatorTujuanOPD` = s.`Kd_IndikatorTujuanOPD`
                    AND i.`Kd_SasaranOPD` = s.`Kd_SasaranOPD`
            ) AS count_ikprogram
            FROM sakip_mopd_sasaran s
            WHERE
                s.`Kd_Urusan` = ?
                AND s.`Kd_Bidang` = ?
                AND s.`Kd_Unit` = ?
                AND s.`Kd_Sub` = ?
            GROUP BY
                s.Kd_Pemda,
                s.Kd_Urusan,
                s.Kd_Bidang,
                s.Kd_Unit,
                s.Kd_Sub,
                s.Kd_TujuanOPD,
                s.Kd_IndikatorTujuanOPD,
                s.Kd_SasaranOPD
            ";
            $q = str_replace("@@TBLPREV@@", $tblpref, $q);
            $dat['rsasaran'] = $res = $this->db->query($q, $params)->result_array();
            $dat['edit'] = true;
            $dat['data'] = array(
                COL_KD_TAHUN => !empty($this->input->post(COL_KD_TAHUN)) ? $this->input->post(COL_KD_TAHUN) : date("Y"),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB)
            );
        }
        $nmSub = "";
        $strOPD = explode('.', $ruser[COL_COMPANYID]);
        $eplandb = $this->load->database("eplan", true);
        if($dat['edit']) {
            $eplandb->where(COL_KD_URUSAN, $dat['data'][COL_KD_URUSAN]);
            $eplandb->where(COL_KD_BIDANG, $dat['data'][COL_KD_BIDANG]);
            $eplandb->where(COL_KD_UNIT, $dat['data'][COL_KD_UNIT]);
            $eplandb->where(COL_KD_SUB, $dat['data'][COL_KD_SUB]);
            $subunit = $eplandb->get("ref_sub_unit")->row_array();
            if($subunit) {
                $nmSub = $subunit["Nm_Sub_Unit"];
            }
        }
        if($ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEKEUANGAN) {
            $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
            $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
            $eplandb->where(COL_KD_UNIT, $strOPD[2]);
            $eplandb->where(COL_KD_SUB, $strOPD[3]);
            $subunit = $eplandb->get("ref_sub_unit")->row_array();
            if($subunit) {
                $nmSub = $subunit["Nm_Sub_Unit"];
            }
        }
        $dat['strOPD'] = $strOPD;
        $dat['nmSub'] = $nmSub;
        $dat['eplandb'] = $eplandb;
        if($cetak) $this->load->view('report/evaluasi_partial_', $dat);
        else $this->load->view('report/evaluasi_', $dat);
    }

    function lakip_kinerja($no) {
        $ruser = GetLoggedUser();
        $dat['title'] = "LAKIP - Capaian Kinerja";
        $dat['edit'] = false;
        $dat['cetak'] = $cetak = $this->input->post("cetak");
        $dat['no'] = $no;

        if(!empty($_POST)) {
            $this->db->where(COL_KD_TAHUN_FROM." <=", $this->input->post(COL_KD_TAHUN));
            $this->db->where(COL_KD_TAHUN_TO." >=", $this->input->post(COL_KD_TAHUN));
            $this->db->order_by(COL_KD_TAHUN_FROM, 'desc');
            $dat['rpemda'] = $rpemda = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();

            $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA,"inner");
            $this->db->where(TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_PEMDA, $rpemda[COL_KD_PEMDA]);
            $this->db->where(TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_URUSAN, $this->input->post(COL_KD_URUSAN));
            $this->db->where(TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_BIDANG, $this->input->post(COL_KD_BIDANG));
            $this->db->where(TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_UNIT, $this->input->post(COL_KD_UNIT));
            $this->db->where(TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SUB, $this->input->post(COL_KD_SUB));
            $this->db->group_by(array(
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_PEMDA,
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_URUSAN,
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_BIDANG,
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_UNIT,
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SUB,
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUANOPD,
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD,
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARANOPD
            ));
            $this->db->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_MISI, 'asc');
            $this->db->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUAN, 'asc');
            $this->db->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORTUJUAN, 'asc');
            $this->db->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARAN, 'asc');
            $this->db->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORSASARAN, 'asc');
            $this->db->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUANOPD, 'asc');
            $this->db->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORTUJUANOPD, 'asc');
            $this->db->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARANOPD, 'asc');
            $rsasaran = $this->db->get(TBL_SAKIP_MOPD_SASARAN)->result_array();
            $dat['sasaran'] = $rsasaran;
            $dat['edit'] = true;
            $dat['data'] = array(
                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB)
            );
        }

        $nmSub = "";
        $strOPD = explode('.', $ruser[COL_COMPANYID]);
        $eplandb = $this->load->database("eplan", true);
        if($dat['edit']) {
            $eplandb->where(COL_KD_URUSAN, $dat['data'][COL_KD_URUSAN]);
            $eplandb->where(COL_KD_BIDANG, $dat['data'][COL_KD_BIDANG]);
            $eplandb->where(COL_KD_UNIT, $dat['data'][COL_KD_UNIT]);
            $eplandb->where(COL_KD_SUB, $dat['data'][COL_KD_SUB]);
            $subunit = $eplandb->get("ref_sub_unit")->row_array();
            if($subunit) {
                $nmSub = $subunit["Nm_Sub_Unit"];
            }
        }
        if($ruser[COL_ROLEID] == ROLEKADIS) {
            $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
            $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
            $eplandb->where(COL_KD_UNIT, $strOPD[2]);
            $eplandb->where(COL_KD_SUB, $strOPD[3]);
            $subunit = $eplandb->get("ref_sub_unit")->row_array();
            if($subunit) {
                $nmSub = $subunit["Nm_Sub_Unit"];
            }
        }
        $dat['strOPD'] = $strOPD;
        $dat['nmSub'] = $nmSub;
        $dat['eplandb'] = $eplandb;

        if($cetak) $this->load->view('report/lakip_kinerja_partial', $dat);
        else $this->load->view('report/lakip_kinerja', $dat);
    }

    function lakip_anggaran() {
        $ruser = GetLoggedUser();
        $dat['title'] = "LAKIP - Realisasi Anggaran";
        $dat['edit'] = false;
        $dat['cetak'] = $cetak = $this->input->post("cetak");

        if(!empty($_POST)) {
            $this->db->where(COL_KD_TAHUN_FROM." <=", $this->input->post(COL_KD_TAHUN));
            $this->db->where(COL_KD_TAHUN_TO." >=", $this->input->post(COL_KD_TAHUN));
            $this->db->order_by(COL_KD_TAHUN_FROM, 'desc');
            $dat['rpemda'] = $rpemda = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();

            $this->db->select("
            CONCAT(
                sakip_dpa_program_sasaran.Kd_Pemda,
                sakip_dpa_program_sasaran.Kd_TujuanOPD,
                sakip_dpa_program_sasaran.Kd_IndikatorTujuanOPD,
                sakip_dpa_program_sasaran.Kd_SasaranOPD
            ) AS UNIQ_SASARAN_OPD,
            CONCAT(
                sakip_dpa_program_sasaran.Kd_Pemda,
                sakip_dpa_program_sasaran.Kd_TujuanOPD,
                sakip_dpa_program_sasaran.Kd_IndikatorTujuanOPD,
                sakip_dpa_program_sasaran.Kd_SasaranOPD,
                sakip_dpa_program_sasaran.Kd_IndikatorSasaranOPD
            ) AS UNIQ_IKSASARAN_OPD,"
                .TBL_SAKIP_DPA_PROGRAM_SASARAN.".*,".TBL_SAKIP_MOPD_SASARAN.".".COL_NM_SASARANOPD.",".TBL_SAKIP_MOPD_IKSASARAN.".".COL_NM_INDIKATORSASARANOPD.",".TBL_SAKIP_DPA_PROGRAM.".".COL_NM_PROGRAMOPD);
            $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_PEMDA,"inner");
            $this->db->join(TBL_SAKIP_MOPD_SASARAN,
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_SASARANOPD
                ,"left");
            $this->db->join(TBL_SAKIP_MOPD_IKSASARAN,
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_SASARANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARANOPD
                ,"left");
            $this->db->join(TBL_SAKIP_DPA_PROGRAM,
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_BID." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_BID." AND ".

                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_MISI." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_SASARANOPD." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_PROGRAMOPD." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_TAHUN
                ,"left");
            $this->db->where(TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_PEMDA, $rpemda[COL_KD_PEMDA]);
            $this->db->where(TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_TAHUN, $this->input->post(COL_KD_TAHUN));
            $this->db->where(TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_URUSAN, $this->input->post(COL_KD_URUSAN));
            $this->db->where(TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_BIDANG, $this->input->post(COL_KD_BIDANG));
            $this->db->where(TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_UNIT, $this->input->post(COL_KD_UNIT));
            $this->db->where(TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_SUB, $this->input->post(COL_KD_SUB));
            $this->db->group_by(array(
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_PEMDA,
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_URUSAN,
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_BIDANG,
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_UNIT,
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_SUB,
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_TUJUANOPD,
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD,
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_SASARANOPD,
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_INDIKATORSASARANOPD,
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_PROGRAMOPD,
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_SASARANPROGRAMOPD
            ));
            $this->db->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_MISI, 'asc');
            $this->db->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_TUJUAN, 'asc');
            $this->db->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORTUJUAN, 'asc');
            $this->db->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_SASARAN, 'asc');
            $this->db->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORSASARAN, 'asc');
            $this->db->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_TUJUANOPD, 'asc');
            $this->db->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORTUJUANOPD, 'asc');
            $this->db->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_SASARANOPD, 'asc');
            $this->db->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORSASARANOPD, 'asc');
            $this->db->order_by(TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_PROGRAMOPD, 'asc');
            $rprogram = $this->db->get(TBL_SAKIP_DPA_PROGRAM_SASARAN)->result_array();
            $dat['program'] = $rprogram;
            $dat['edit'] = true;
            $dat['data'] = array(
                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB)
            );
        }

        $nmSub = "";
        $strOPD = explode('.', $ruser[COL_COMPANYID]);
        $eplandb = $this->load->database("eplan", true);
        if($dat['edit']) {
            $eplandb->where(COL_KD_URUSAN, $dat['data'][COL_KD_URUSAN]);
            $eplandb->where(COL_KD_BIDANG, $dat['data'][COL_KD_BIDANG]);
            $eplandb->where(COL_KD_UNIT, $dat['data'][COL_KD_UNIT]);
            $eplandb->where(COL_KD_SUB, $dat['data'][COL_KD_SUB]);
            $subunit = $eplandb->get("ref_sub_unit")->row_array();
            if($subunit) {
                $nmSub = $subunit["Nm_Sub_Unit"];
            }
        }
        if($ruser[COL_ROLEID] == ROLEKADIS) {
            $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
            $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
            $eplandb->where(COL_KD_UNIT, $strOPD[2]);
            $eplandb->where(COL_KD_SUB, $strOPD[3]);
            $subunit = $eplandb->get("ref_sub_unit")->row_array();
            if($subunit) {
                $nmSub = $subunit["Nm_Sub_Unit"];
            }
        }
        $dat['strOPD'] = $strOPD;
        $dat['nmSub'] = $nmSub;
        $dat['eplandb'] = $eplandb;

        if($cetak) $this->load->view('report/lakip_anggaran_partial', $dat);
        else $this->load->view('report/lakip_anggaran', $dat);
    }

    function lakip() {
        $ruser = GetLoggedUser();
        $dat['title'] = "Laporan Kinerja";
        $dat['edit'] = false;
        $dat['cetak'] = $cetak = $this->input->post("cetak");

        if(!empty($_POST)) {
          $this->db->where(COL_KD_TAHUN_FROM." <=", $this->input->post(COL_KD_TAHUN));
          $this->db->where(COL_KD_TAHUN_TO." >=", $this->input->post(COL_KD_TAHUN));
          $this->db->order_by(COL_KD_TAHUN_FROM, 'desc');
          $dat['rpemda'] = $rpemda = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();
          $dat['edit'] = true;
          $dat['data'] = array(
            COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
            COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
            COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
            COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
            COL_KD_SUB => $this->input->post(COL_KD_SUB)
          );
        }

        $nmSub = "";
        $strOPD = explode('.', $ruser[COL_COMPANYID]);
        $eplandb = $this->load->database("eplan", true);
        if($dat['edit']) {
            $eplandb->where(COL_KD_URUSAN, $dat['data'][COL_KD_URUSAN]);
            $eplandb->where(COL_KD_BIDANG, $dat['data'][COL_KD_BIDANG]);
            $eplandb->where(COL_KD_UNIT, $dat['data'][COL_KD_UNIT]);
            $eplandb->where(COL_KD_SUB, $dat['data'][COL_KD_SUB]);
            $subunit = $eplandb->get("ref_sub_unit")->row_array();
            if($subunit) {
                $nmSub = $subunit["Nm_Sub_Unit"];
            }
        }
        if($ruser[COL_ROLEID] != ROLEADMIN) {
            $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
            $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
            $eplandb->where(COL_KD_UNIT, $strOPD[2]);
            $eplandb->where(COL_KD_SUB, $strOPD[3]);
            $subunit = $eplandb->get("ref_sub_unit")->row_array();
            if($subunit) {
                $nmSub = $subunit["Nm_Sub_Unit"];
            }
        }
        $dat['strOPD'] = $strOPD;
        $dat['nmSub'] = $nmSub;
        $dat['eplandb'] = $eplandb;

        if(!empty($_POST)) {
          $rsasaran = $this->db
          ->select('sakip_mopd_iksasaran.*, sakip_mopd_sasaran.Nm_SasaranOPD')
          ->join(TBL_SAKIP_MOPD_SASARAN,
              TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_URUSAN." AND ".
              TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_BIDANG." AND ".
              TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_UNIT." AND ".
              TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_SUB." AND ".

              TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_PEMDA." AND ".
              TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_MISI." AND ".
              TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_TUJUAN." AND ".
              TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
              TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_SASARAN." AND ".
              TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
              TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_TUJUANOPD." AND ".
              TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
              TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_SASARANOPD
              ,"left")
          ->where(TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_PEMDA, $rpemda[COL_KD_PEMDA])
          ->where(TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_URUSAN, $this->input->post(COL_KD_URUSAN))
          ->where(TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_BIDANG, $this->input->post(COL_KD_BIDANG))
          ->where(TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_UNIT, $this->input->post(COL_KD_UNIT))
          ->where(TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SUB, $this->input->post(COL_KD_SUB))
          ->order_by(TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARANOPD, 'asc')
          ->order_by(TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARANOPD, 'asc')
          ->get(TBL_SAKIP_MOPD_IKSASARAN)
          ->result_array();

          $rkegiatan = $this->db
          ->select('sakip_dpa_kegiatan.*, sakip_dpa_program.Nm_ProgramOPD, sakip_mopd_sasaran.Nm_SasaranOPD')
          ->join(TBL_SAKIP_DPA_PROGRAM,
              TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_URUSAN." AND ".
              TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BIDANG." AND ".
              TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_UNIT." AND ".
              TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SUB." AND ".
              TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_TAHUN." AND ".

              TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_PEMDA." AND ".
              TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_MISI." AND ".
              TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_TUJUAN." AND ".
              TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORTUJUAN." AND ".
              TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SASARAN." AND ".
              TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORSASARAN." AND ".
              TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_TUJUANOPD." AND ".
              TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
              TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SASARANOPD." AND ".
              TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_PROGRAMOPD
              ,"left")
          ->join(TBL_SAKIP_MOPD_SASARAN,
              TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_URUSAN." AND ".
              TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BIDANG." AND ".
              TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_UNIT." AND ".
              TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SUB." AND ".

              TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_PEMDA." AND ".
              TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_MISI." AND ".
              TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_TUJUAN." AND ".
              TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORTUJUAN." AND ".
              TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SASARAN." AND ".
              TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORSASARAN." AND ".
              TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_TUJUANOPD." AND ".
              TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
              TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SASARANOPD
              ,"left")
          ->where(TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_PEMDA, $rpemda[COL_KD_PEMDA])
          ->where(TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_URUSAN, $this->input->post(COL_KD_URUSAN))
          ->where(TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_BIDANG, $this->input->post(COL_KD_BIDANG))
          ->where(TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_UNIT, $this->input->post(COL_KD_UNIT))
          ->where(TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_SUB, $this->input->post(COL_KD_SUB))
          ->where(TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_TAHUN, $this->input->post(COL_KD_TAHUN))
          ->order_by(TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_SASARANOPD, 'asc')
          ->order_by(TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_PROGRAMOPD, 'asc')
          ->order_by(TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_KEGIATANOPD, 'asc')
          ->group_by(array(TBL_SAKIP_MOPD_SASARAN.'.'.COL_NM_SASARANOPD, TBL_SAKIP_DPA_PROGRAM.'.'.COL_NM_PROGRAMOPD, TBL_SAKIP_DPA_KEGIATAN.'.'.COL_NM_KEGIATANOPD))
          ->get(TBL_SAKIP_DPA_KEGIATAN)
          ->result_array();

          $qAlokasi = @"
          select
          sas.*,
          (select sum(keg.Budget)
          	from sakip_dpa_kegiatan keg
          	where
          		keg.Kd_Pemda = sas.Kd_Pemda
          		and keg.Kd_Urusan = sas.Kd_Urusan
          		and keg.Kd_bidang = sas.Kd_Bidang
          		and keg.Kd_Unit = sas.Kd_Unit
          		and keg.Kd_Sub = sas.Kd_Sub
          		and keg.Kd_Tahun = ?
          ) as anggaranTotal,
          (select sum(keg.Budget)
          	from sakip_dpa_kegiatan keg
          	where
          		keg.Kd_Pemda = sas.Kd_Pemda
          		and keg.Kd_Urusan = sas.Kd_Urusan
          		and keg.Kd_bidang = sas.Kd_Bidang
          		and keg.Kd_Unit = sas.Kd_Unit
          		and keg.Kd_Sub = sas.Kd_Sub
          		and keg.Kd_TujuanOPD = sas.Kd_TujuanOPD
          		and keg.Kd_IndikatorTujuanOPD = sas.Kd_IndikatorTujuanOPD
          		and keg.Kd_SasaranOPD = sas.Kd_SasaranOPD
          		and keg.Kd_Tahun = ?
          ) as anggaranAlokasi
          from sakip_mopd_sasaran sas
          where
          	sas.Kd_Pemda = ?
          	and sas.Kd_Urusan = ?
          	and sas.Kd_Bidang = ?
          	and sas.Kd_Unit = ?
          	and sas.Kd_Sub = ?
          group by sas.Nm_SasaranOPD
          ";
          $parAlokasi = array(
            $this->input->post(COL_KD_TAHUN),
            $this->input->post(COL_KD_TAHUN),
            $rpemda[COL_KD_PEMDA],
            $this->input->post(COL_KD_URUSAN),
            $this->input->post(COL_KD_BIDANG),
            $this->input->post(COL_KD_UNIT),
            $this->input->post(COL_KD_SUB)
          );
          $rAlokasiSasaran = $this->db
          ->query($qAlokasi, $parAlokasi)
          ->result_array();

          $qRealisasi = @"
          select
          iks.*,
          sas.Nm_SasaranOPD,
          (select sum(keg.Budget)
          	from sakip_dpa_kegiatan keg
          	where
          		keg.Kd_Pemda = iks.Kd_Pemda
          		and keg.Kd_Urusan = iks.Kd_Urusan
          		and keg.Kd_bidang = iks.Kd_Bidang
          		and keg.Kd_Unit = iks.Kd_Unit
          		and keg.Kd_Sub = iks.Kd_Sub
          		and keg.Kd_TujuanOPD = iks.Kd_TujuanOPD
          		and keg.Kd_IndikatorTujuanOPD = iks.Kd_IndikatorTujuanOPD
          		and keg.Kd_SasaranOPD = iks.Kd_SasaranOPD
              and keg.Kd_IndikatorSasaranOPD = iks.Kd_IndikatorSasaranOPD
          		and keg.Kd_Tahun = ?
          ) as anggaranAlokasi,
          (select sum(coalesce(keg.Anggaran_TW1, 0)+coalesce(keg.Anggaran_TW2, 0)+coalesce(keg.Anggaran_TW3, 0)+coalesce(keg.Anggaran_TW4, 0))
          	from sakip_dpa_kegiatan keg
          	where
          		keg.Kd_Pemda = iks.Kd_Pemda
          		and keg.Kd_Urusan = iks.Kd_Urusan
          		and keg.Kd_Bidang = iks.Kd_Bidang
          		and keg.Kd_Unit = iks.Kd_Unit
          		and keg.Kd_Sub = iks.Kd_Sub
          		and keg.Kd_TujuanOPD = iks.Kd_TujuanOPD
          		and keg.Kd_IndikatorTujuanOPD = iks.Kd_IndikatorTujuanOPD
          		and keg.Kd_SasaranOPD = iks.Kd_SasaranOPD
              and keg.Kd_IndikatorSasaranOPD = iks.Kd_IndikatorSasaranOPD
          		and keg.Kd_Tahun = ?
          ) as anggaranRealisasi
          from sakip_mopd_iksasaran iks
          left join sakip_mopd_sasaran sas on
            sas.Kd_Urusan = iks.Kd_Urusan
            and sas.Kd_Bidang = iks.Kd_Bidang
            and sas.Kd_Unit = iks.Kd_Unit
            and sas.Kd_Sub = iks.Kd_Sub
            and sas.Kd_Pemda = iks.Kd_Pemda
            and sas.Kd_Misi = iks.Kd_Misi
            and sas.Kd_Tujuan = iks.Kd_Tujuan
            and sas.Kd_IndikatorTujuan = iks.Kd_IndikatorTujuan
            and sas.Kd_Sasaran = iks.Kd_Sasaran
            and sas.Kd_IndikatorSasaran = iks.Kd_IndikatorSasaran
            and sas.Kd_TujuanOPD = iks.Kd_TujuanOPD
            and sas.Kd_IndikatorTujuanOPD = iks.Kd_IndikatorTujuanOPD
            and sas.Kd_SasaranOPD = iks.Kd_SasaranOPD
          where
          	iks.Kd_Pemda = ?
          	and iks.Kd_Urusan = ?
          	and iks.Kd_Bidang = ?
          	and iks.Kd_Unit = ?
          	and iks.Kd_Sub = ?
          group by sas.Nm_SasaranOPD, iks.Nm_IndikatorSasaranOPD
          ";
          $parRealisasi = array(
            $this->input->post(COL_KD_TAHUN),
            $this->input->post(COL_KD_TAHUN),
            $rpemda[COL_KD_PEMDA],
            $this->input->post(COL_KD_URUSAN),
            $this->input->post(COL_KD_BIDANG),
            $this->input->post(COL_KD_UNIT),
            $this->input->post(COL_KD_SUB)
          );
          $rRealisasiSasaran = $this->db
          ->query($qRealisasi, $parRealisasi)
          ->result_array();
          $lastq = $this->db->last_query();
          //echo $lastq;
          //exit;

          $phpWord = new \PhpOffice\PhpWord\PhpWord();
          \PhpOffice\PhpWord\Settings::setTempDir(FCPATH.'assets/phpword/tempdir');

          $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(FCPATH.'assets/phpword/tempdir/LAKIP-FINAL.docx');
          //var_dump($templateProcessor->getVariables());
          //exit;

          $templateProcessor->setValue('lakip-tahun', $this->input->post(COL_KD_TAHUN));
          $templateProcessor->setValue('nama-opd', $nmSub);
          $templateProcessor->setValue('n1', $rpemda[COL_KD_TAHUN_FROM]+1);
          $templateProcessor->setValue('n2', $rpemda[COL_KD_TAHUN_FROM]+2);
          $templateProcessor->setValue('n3', $rpemda[COL_KD_TAHUN_FROM]+3);
          $templateProcessor->setValue('n4', $rpemda[COL_KD_TAHUN_FROM]+4);
          $templateProcessor->setValue('n5', $rpemda[COL_KD_TAHUN_FROM]+5);
          $templateProcessor->setValue('1-2', $this->input->post('12_narasi'));
          $templateProcessor->setValue('1-3', $this->input->post('13_narasi'));
          $templateProcessor->setValue('1-4', $this->input->post('14_narasi'));
          $templateProcessor->setValue('1-5', $this->input->post('15_narasi'));
          $templateProcessor->setValue('1-6', $this->input->post('16_narasi'));
          $templateProcessor->setValue('1-7', $this->input->post('17_narasi'));
          $templateProcessor->setValue('2-1', $this->input->post('21_narasi'));
          $templateProcessor->setValue('2-1-1', $this->input->post('211_narasi'));
          $templateProcessor->setValue('2-1-2-narasi', $this->input->post('212_narasi'));
          $templateProcessor->setValue('2-1-3-narasi', $this->input->post('213_narasi'));
          $templateProcessor->setValue('2-3', $this->input->post('23_narasi'));
          $templateProcessor->setValue('3-1-narasi', $this->input->post('31_narasi'));
          $templateProcessor->setValue('3-2-narasi', $this->input->post('32_narasi'));
          $templateProcessor->setValue('3-3-narasi', $this->input->post('33_narasi'));
          $templateProcessor->setValue('3-4', $this->input->post('34_narasi'));
          $templateProcessor->setValue('4', $this->input->post('4_narasi'));

          $arr212 = array();
          if(!empty($rsasaran)) {
            $no = 1;
            foreach($rsasaran as $s) {
              $arr212_t = array();
              for($i_=($rpemda[COL_KD_TAHUN_FROM]+1);$i_<=$rpemda[COL_KD_TAHUN_TO];$i_++) {
                $rcapaian = $this->db
                ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_PEMDA, $rpemda[COL_KD_PEMDA])
                ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_URUSAN, $this->input->post(COL_KD_URUSAN))
                ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_BIDANG, $this->input->post(COL_KD_BIDANG))
                ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_UNIT, $this->input->post(COL_KD_UNIT))
                ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_SUB, $this->input->post(COL_KD_SUB))

                ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_PEMDA, $s[COL_KD_PEMDA])
                ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_MISI, $s[COL_KD_MISI])
                ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_TUJUAN, $s[COL_KD_TUJUAN])
                ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_INDIKATORTUJUAN, $s[COL_KD_INDIKATORTUJUAN])
                ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_SASARAN, $s[COL_KD_SASARAN])
                ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_INDIKATORSASARAN, $s[COL_KD_INDIKATORSASARAN])
                ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_TUJUANOPD, $s[COL_KD_TUJUANOPD])
                ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_INDIKATORTUJUANOPD, $s[COL_KD_INDIKATORTUJUANOPD])
                ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_SASARANOPD, $s[COL_KD_SASARANOPD])
                ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_INDIKATORSASARANOPD, $s[COL_KD_INDIKATORSASARANOPD])

                ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_TAHUN, $i_)
                ->get(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN)
                ->row_array();

                $arr212_t[$i_] = !empty($rcapaian)?$rcapaian[COL_TARGET]:'-';
              }

              $arr212[] = array(
                't212_1'=>$no,
                't212_2'=>$s[COL_NM_SASARANOPD],
                't212_3'=>$s[COL_NM_INDIKATORSASARANOPD],
                't212_4'=>'-',
                't212_5'=>$arr212_t[$rpemda[COL_KD_TAHUN_FROM]+1],
                't212_6'=>$arr212_t[$rpemda[COL_KD_TAHUN_FROM]+2],
                't212_7'=>$arr212_t[$rpemda[COL_KD_TAHUN_FROM]+3],
                't212_8'=>$arr212_t[$rpemda[COL_KD_TAHUN_FROM]+4],
                't212_9'=>$arr212_t[$rpemda[COL_KD_TAHUN_FROM]+5]
              );
              $no++;
            }
            //$templateProcessor->cloneRowAndSetValues('t212_1', $arr212);
          }

          $arr213 = array();
          if(!empty($rkegiatan)) {
            $no = 1;
            foreach($rkegiatan as $k) {
              $arr213[] = array(
                't213_1'=>$no,
                't213_2'=>$k[COL_NM_SASARANOPD],
                't213_3'=>$k[COL_NM_PROGRAMOPD],
                't213_4'=>$k[COL_NM_KEGIATANOPD]
              );
              $no++;
            }
            //$templateProcessor->cloneRowAndSetValues('t213_1', $arr213);
          }

          $arr232 = array();
          if(!empty($rAlokasiSasaran)) {
            $no = 1;
            foreach($rAlokasiSasaran as $k) {
              $arr232[] = array(
                't232_1'=>$no,
                't232_2'=>$k[COL_NM_SASARANOPD],
                't232_3'=>number_format($k['anggaranAlokasi']),
                't232_4'=>$k['anggaranTotal']>0?number_format($k['anggaranAlokasi']/$k['anggaranTotal']*100,2).' %':'-',
                't232_5'=>'-'
              );
              $no++;
            }
            //$templateProcessor->cloneRowAndSetValues('t232_1', $arr232);
          }

          $arr31 = array();
          if(!empty($rsasaran)) {
            $no = 1;
            foreach($rsasaran as $s) {
              $rcapaian = $this->db
              ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_PEMDA, $rpemda[COL_KD_PEMDA])
              ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_URUSAN, $this->input->post(COL_KD_URUSAN))
              ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_BIDANG, $this->input->post(COL_KD_BIDANG))
              ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_UNIT, $this->input->post(COL_KD_UNIT))
              ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_SUB, $this->input->post(COL_KD_SUB))

              ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_PEMDA, $s[COL_KD_PEMDA])
              ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_MISI, $s[COL_KD_MISI])
              ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_TUJUAN, $s[COL_KD_TUJUAN])
              ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_INDIKATORTUJUAN, $s[COL_KD_INDIKATORTUJUAN])
              ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_SASARAN, $s[COL_KD_SASARAN])
              ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_INDIKATORSASARAN, $s[COL_KD_INDIKATORSASARAN])
              ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_TUJUANOPD, $s[COL_KD_TUJUANOPD])
              ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_INDIKATORTUJUANOPD, $s[COL_KD_INDIKATORTUJUANOPD])
              ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_SASARANOPD, $s[COL_KD_SASARANOPD])
              ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_INDIKATORSASARANOPD, $s[COL_KD_INDIKATORSASARANOPD])

              ->where(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.'.'.COL_KD_TAHUN, $this->input->post(COL_KD_TAHUN))
              ->get(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN)
              ->row_array();

              $arr31[] = array(
                't31_1'=>$no,
                't31_2'=>$s[COL_NM_SASARANOPD],
                't31_3'=>$s[COL_NM_INDIKATORSASARANOPD],
                't31_4'=>'-',
                't31_5'=>!empty($rcapaian)?$rcapaian[COL_TARGET]:'-',
                't31_6'=>!empty($rcapaian)?$rcapaian[COL_REALISASI]:'-',
                't31_7'=>'-'
              );
              $no++;
            }
            //$templateProcessor->cloneRowAndSetValues('t31_1', $arr31);
          }

          $arr32 = array();
          if(!empty($rsasaran)) {
            $no = 1;
            foreach($rsasaran as $s) {
              $arr32[] = array(
                't32_1'=>$no,
                't32_2'=>$s[COL_NM_SASARANOPD],
                't32_3'=>$s[COL_NM_INDIKATORSASARANOPD],
                't32_4'=>$s[COL_NM_FORMULA]
              );
              $no++;
            }
            //$templateProcessor->cloneRowAndSetValues('t32_1', $arr32);
          }

          $arr332 = array();
          if(!empty($rRealisasiSasaran)) {
            $no = 1;
            foreach($rRealisasiSasaran as $k) {
              $arr332[] = array(
                't332_1'=>$no,
                't332_2'=>$k[COL_NM_SASARANOPD],
                't332_3'=>$k[COL_NM_INDIKATORSASARANOPD],
                't332_4'=>number_format($k['anggaranAlokasi']),
                't332_5'=>number_format($k['anggaranRealisasi']),
                't332_6'=>$k['anggaranAlokasi']>0?number_format($k['anggaranRealisasi']/$k['anggaranAlokasi']*100,2).' %':'-'
              );
              $no++;
            }
            //$templateProcessor->cloneRowAndSetValues('t332_1', $arr332);
          }

          $datInsert = array(
            COL_KD_PEMDA=>$rpemda[COL_KD_PEMDA],
            COL_KD_TAHUN=>$this->input->post(COL_KD_TAHUN),
            COL_KD_URUSAN=>$this->input->post(COL_KD_URUSAN),
            COL_KD_BIDANG=>$this->input->post(COL_KD_BIDANG),
            COL_KD_UNIT=>$this->input->post(COL_KD_UNIT),
            COL_KD_SUB=>$this->input->post(COL_KD_SUB),
            COL_NM_FORMLAKIP=>json_encode($_POST),

            COL_CREATE_BY=>$ruser[COL_USERNAME],
            COL_CREATE_DATE=>date('Y-m-d H:i:s')
          );

          $this->db->insert(TBL_SAKIP_MOPD_LAKIP, $datInsert);

          $filename = 'LAKIP-'.date('Y-m-d-Hi') . '.docx';
          header("Content-Description: File Transfer");
          header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          header('Content-Transfer-Encoding: binary');
          header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
          header('Expires: 0');
          //$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
          //$objWriter->save('php://output');

          $templateProcessor->saveAs('php://output');
          exit();
        } else {
          if($cetak) $this->load->view('report/lakip_partial', $dat);
          else $this->load->view('report/lakip', $dat);
        }


    }
}
