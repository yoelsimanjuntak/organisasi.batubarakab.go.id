<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 30/09/2018
 * Time: 00:24
 */
class Ajax extends MY_Controller {
    function browse_pemda() {
        $this->db->select('*, '.COL_KD_PEMDA.' AS ID, CONCAT('.COL_KD_TAHUN_FROM.',\' s.d \','.COL_KD_TAHUN_TO.',\' : \','.COL_NM_PEJABAT.') AS Text');
        $this->db->where(COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(COL_KD_TAHUN_TO." >=", date("Y"));
        $this->db->order_by(COL_KD_TAHUN_FROM, 'desc');
        $data['res'] = $this->db->get(TBL_SAKIP_MPEMDA)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_misi() {
        $kdPemda = $this->input->get("Kd_Pemda");

        $this->db->select('*, '.COL_KD_MISI.' AS ID, CONCAT('.COL_KD_MISI.',\' \','.COL_NM_MISI.') AS Text');
        $this->db->where(COL_KD_PEMDA, $kdPemda);
        $this->db->order_by(COL_KD_MISI, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_MPMD_MISI)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_tujuan() {
        $kdPemda = $this->input->get("Kd_Pemda");
        $kdMisi = $this->input->get("Kd_Misi");

        $this->db->select('*, '.COL_KD_TUJUAN.' AS ID, CONCAT('.COL_KD_MISI.',\'.\','.COL_KD_TUJUAN.',\' \','.COL_NM_TUJUAN.') AS Text');
        $this->db->where(COL_KD_PEMDA, $kdPemda);
        $this->db->where(COL_KD_MISI, $kdMisi);
        $this->db->order_by(COL_KD_TUJUAN, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_MPMD_TUJUAN)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_iktujuan() {
        $kdPemda = $this->input->get("Kd_Pemda");
        $kdMisi = $this->input->get("Kd_Misi");
        $kdTujuan = $this->input->get("Kd_Tujuan");

        $this->db->select('*, '.COL_KD_INDIKATORTUJUAN.' AS ID, CONCAT('.COL_KD_MISI.',\'.\','.COL_KD_TUJUAN.',\'.\','.COL_KD_INDIKATORTUJUAN.',\' \','.COL_NM_INDIKATORTUJUAN.') AS Text');
        $this->db->where(COL_KD_PEMDA, $kdPemda);
        $this->db->where(COL_KD_MISI, $kdMisi);
        $this->db->where(COL_KD_TUJUAN, $kdTujuan);
        $this->db->order_by(COL_KD_INDIKATORTUJUAN, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_MPMD_IKTUJUAN)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_sasaran() {
        $kdPemda = $this->input->get("Kd_Pemda");
        $kdMisi = $this->input->get("Kd_Misi");
        $kdTujuan = $this->input->get("Kd_Tujuan");
        $kdIKTujuan = $this->input->get("Kd_IndikatorTujuan");

        $this->db->select('*, '.COL_KD_SASARAN.' AS ID, CONCAT('.COL_KD_MISI.',\'.\','.COL_KD_TUJUAN.',\'.\','.COL_KD_INDIKATORTUJUAN.',\'.\','.COL_KD_SASARAN.',\' \','.COL_NM_SASARAN.') AS Text');
        $this->db->where(COL_KD_PEMDA, $kdPemda);
        $this->db->where(COL_KD_MISI, $kdMisi);
        $this->db->where(COL_KD_TUJUAN, $kdTujuan);
        $this->db->where(COL_KD_INDIKATORTUJUAN, $kdIKTujuan);
        $this->db->order_by(COL_KD_SASARAN, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_MPMD_SASARAN)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_iksasaran() {
        $kdPemda = $this->input->get("Kd_Pemda");
        $kdMisi = $this->input->get("Kd_Misi");
        $kdTujuan = $this->input->get("Kd_Tujuan");
        $kdIKTujuan = $this->input->get("Kd_IndikatorTujuan");
        $kdSasaran = $this->input->get("Kd_Sasaran");

        $this->db->select('*, '.COL_KD_INDIKATORSASARAN.' AS ID, CONCAT('.COL_KD_MISI.',\'.\','.COL_KD_TUJUAN.',\'.\','.COL_KD_INDIKATORTUJUAN.',\'.\','.COL_KD_SASARAN.',\'.\','.COL_KD_INDIKATORSASARAN.',\' \','.COL_NM_INDIKATORSASARAN.') AS Text');
        $this->db->where(COL_KD_PEMDA, $kdPemda);
        $this->db->where(COL_KD_MISI, $kdMisi);
        $this->db->where(COL_KD_TUJUAN, $kdTujuan);
        $this->db->where(COL_KD_INDIKATORTUJUAN, $kdIKTujuan);
        $this->db->where(COL_KD_SASARAN, $kdSasaran);
        $this->db->order_by(COL_KD_INDIKATORSASARAN, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_MPMD_IKSASARAN)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_tujuan_opd() {
        $kdUrusan = $this->input->get("Kd_Urusan");
        $kdBidang = $this->input->get("Kd_Bidang");
        $kdUnit = $this->input->get("Kd_Unit");
        $kdSub = $this->input->get("Kd_Sub");
        $kdPemda = $this->input->get("Kd_Pemda");
        $kdMisi = $this->input->get("Kd_Misi");
        $kdTujuan = $this->input->get("Kd_Tujuan");
        $kdIKTujuan = $this->input->get("Kd_IndikatorTujuan");
        $kdSasaran = $this->input->get("Kd_Sasaran");
        $kdIkSasaran = $this->input->get("Kd_IndikatorSasaran");

        $this->db->select('*, '.COL_KD_TUJUANOPD.' AS ID, CONCAT('.COL_KD_MISI.',\'.\','.COL_KD_TUJUAN.',\'.\','.COL_KD_INDIKATORTUJUAN.',\'.\','.COL_KD_SASARAN.',\'.\','.COL_KD_INDIKATORSASARAN.',\'.\','.COL_KD_TUJUANOPD.','.',\' \','.COL_NM_TUJUANOPD.') AS Text');
        $this->db->where(COL_KD_PEMDA, $kdPemda);
        $this->db->where(COL_KD_MISI, $kdMisi);
        $this->db->where(COL_KD_TUJUAN, $kdTujuan);
        $this->db->where(COL_KD_INDIKATORTUJUAN, $kdIKTujuan);
        $this->db->where(COL_KD_SASARAN, $kdSasaran);
        $this->db->where(COL_KD_INDIKATORSASARAN, $kdIkSasaran);
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $this->db->order_by(COL_KD_TUJUANOPD, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_MOPD_TUJUAN)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_iktujuan_opd_ver_2() {
        $kdUrusan = $this->input->get("Kd_Urusan");
        $kdBidang = $this->input->get("Kd_Bidang");
        $kdUnit = $this->input->get("Kd_Unit");
        $kdSub = $this->input->get("Kd_Sub");
        $kdPemda = $this->input->get("Kd_Pemda");
        $kdMisi = $this->input->get("Kd_Misi");
        $kdTujuan = $this->input->get("Kd_Tujuan");
        $kdIKTujuan = $this->input->get("Kd_IndikatorTujuan");
        $kdSasaran = $this->input->get("Kd_Sasaran");
        $kdIkSasaran = $this->input->get("Kd_IndikatorSasaran");
        $kdTujuanOPD = $this->input->get("Kd_TujuanOPD");

        $this->db->select('*, '.COL_KD_INDIKATORTUJUANOPD.' AS ID, CONCAT('.COL_KD_MISI.',\'.\','.COL_KD_TUJUAN.',\'.\','.COL_KD_INDIKATORTUJUAN.',\'.\','.COL_KD_SASARAN.',\'.\','.COL_KD_INDIKATORSASARAN.',\'.\','.COL_KD_TUJUANOPD.',\'.\','.COL_KD_INDIKATORTUJUANOPD.','.',\' \','.COL_NM_INDIKATORTUJUANOPD.') AS Text');
        $this->db->where(COL_KD_PEMDA, $kdPemda);
        $this->db->where(COL_KD_MISI, $kdMisi);
        $this->db->where(COL_KD_TUJUAN, $kdTujuan);
        $this->db->where(COL_KD_INDIKATORTUJUAN, $kdIKTujuan);
        $this->db->where(COL_KD_SASARAN, $kdSasaran);
        $this->db->where(COL_KD_INDIKATORSASARAN, $kdIkSasaran);
        $this->db->where(COL_KD_TUJUANOPD, $kdTujuanOPD);
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $this->db->order_by(COL_KD_INDIKATORTUJUANOPD, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_MOPD_IKTUJUAN)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_sasaran_opd() {
        $kdUrusan = $this->input->get("Kd_Urusan");
        $kdBidang = $this->input->get("Kd_Bidang");
        $kdUnit = $this->input->get("Kd_Unit");
        $kdSub = $this->input->get("Kd_Sub");
        $kdPemda = $this->input->get("Kd_Pemda");
        $kdMisi = $this->input->get("Kd_Misi");
        $kdTujuan = $this->input->get("Kd_Tujuan");
        $kdIKTujuan = $this->input->get("Kd_IndikatorTujuan");
        $kdSasaran = $this->input->get("Kd_Sasaran");
        $kdIkSasaran = $this->input->get("Kd_IndikatorSasaran");
        $kdTujuanOPD = $this->input->get("Kd_TujuanOPD");
        $kdIkTujuanOPD = $this->input->get("Kd_IndikatorTujuanOPD");

        $this->db->select('*, '.COL_KD_SASARANOPD.' AS ID, CONCAT('.COL_KD_MISI.',\'.\','.COL_KD_TUJUAN.',\'.\','.COL_KD_INDIKATORTUJUAN.',\'.\','.COL_KD_SASARAN.',\'.\','.COL_KD_INDIKATORSASARAN.',\'.\','.COL_KD_TUJUANOPD.',\'.\','.COL_KD_INDIKATORTUJUANOPD.',\'.\','.COL_KD_SASARANOPD.','.',\' \','.COL_NM_SASARANOPD.') AS Text');
        $this->db->where(COL_KD_PEMDA, $kdPemda);
        $this->db->where(COL_KD_MISI, $kdMisi);
        $this->db->where(COL_KD_TUJUAN, $kdTujuan);
        $this->db->where(COL_KD_INDIKATORTUJUAN, $kdIKTujuan);
        $this->db->where(COL_KD_SASARAN, $kdSasaran);
        $this->db->where(COL_KD_INDIKATORSASARAN, $kdIkSasaran);
        $this->db->where(COL_KD_TUJUANOPD, $kdTujuanOPD);
        $this->db->where(COL_KD_INDIKATORTUJUANOPD, $kdIkTujuanOPD);
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $this->db->order_by(COL_KD_SASARANOPD, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_MOPD_SASARAN)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_iksasaran_opd_ver_2() {
        $kdUrusan = $this->input->get("Kd_Urusan");
        $kdBidang = $this->input->get("Kd_Bidang");
        $kdUnit = $this->input->get("Kd_Unit");
        $kdSub = $this->input->get("Kd_Sub");
        $kdPemda = $this->input->get("Kd_Pemda");
        $kdMisi = $this->input->get("Kd_Misi");
        $kdTujuan = $this->input->get("Kd_Tujuan");
        $kdIKTujuan = $this->input->get("Kd_IndikatorTujuan");
        $kdSasaran = $this->input->get("Kd_Sasaran");
        $kdIkSasaran = $this->input->get("Kd_IndikatorSasaran");
        $kdTujuanOPD = $this->input->get("Kd_TujuanOPD");
        $kdIkTujuanOPD = $this->input->get("Kd_IndikatorTujuanOPD");
        $kdSasaranOPD = $this->input->get("Kd_SasaranOPD");

        $this->db->select('*, '.COL_KD_INDIKATORSASARANOPD.' AS ID, CONCAT('.COL_KD_MISI.',\'.\','.COL_KD_TUJUAN.',\'.\','.COL_KD_INDIKATORTUJUAN.',\'.\','.COL_KD_SASARAN.',\'.\','.COL_KD_INDIKATORSASARAN.',\'.\','.COL_KD_TUJUANOPD.',\'.\','.COL_KD_INDIKATORTUJUANOPD.',\'.\','.COL_KD_SASARANOPD.',\'.\','.COL_KD_INDIKATORSASARANOPD.','.',\' \','.COL_NM_INDIKATORSASARANOPD.') AS Text');
        $this->db->where(COL_KD_PEMDA, $kdPemda);
        $this->db->where(COL_KD_MISI, $kdMisi);
        $this->db->where(COL_KD_TUJUAN, $kdTujuan);
        $this->db->where(COL_KD_INDIKATORTUJUAN, $kdIKTujuan);
        $this->db->where(COL_KD_SASARAN, $kdSasaran);
        $this->db->where(COL_KD_INDIKATORSASARAN, $kdIkSasaran);
        $this->db->where(COL_KD_TUJUANOPD, $kdTujuanOPD);
        $this->db->where(COL_KD_INDIKATORTUJUANOPD, $kdIkTujuanOPD);
        $this->db->where(COL_KD_SASARANOPD, $kdSasaranOPD);
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $this->db->order_by(COL_KD_SASARANOPD, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_MOPD_IKSASARAN)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_opd() {
        $eplandb = $this->load->database("eplan", true);
        $eplandb->select('*, CONCAT('.COL_KD_URUSAN.',\'|\','.COL_KD_BIDANG.',\'|\','.COL_KD_UNIT.',\'|\','.COL_KD_SUB.') AS ID, CONCAT('.COL_KD_URUSAN.',\'.\','.COL_KD_BIDANG.',\'.\','.COL_KD_UNIT.',\'.\','.COL_KD_SUB.',\' \',Nm_Sub_Unit) AS Text');
        $eplandb->order_by(COL_KD_URUSAN, 'asc');
        $eplandb->order_by(COL_KD_BIDANG, 'asc');
        $eplandb->order_by(COL_KD_UNIT, 'asc');
        $eplandb->order_by(COL_KD_SUB, 'asc');
        $data['res'] = $eplandb->get("ref_sub_unit")->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_iktujuan_opd() {
        $kdUrusan = $this->input->get("Kd_Urusan");
        $kdBidang = $this->input->get("Kd_Bidang");
        $kdUnit = $this->input->get("Kd_Unit");
        $kdSub = $this->input->get("Kd_Sub");

        $this->db->select('*,
            CONCAT('.TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_PEMDA.',\'|\','.TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_MISI.',\'|\','.TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUAN.',\'|\','.TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN.',\'|\','.TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SASARAN.',\'|\','.TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORSASARAN.',\'|\','.TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUANOPD.',\'|\','.TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUANOPD.') AS ID,
            CONCAT(\'( \','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.
            ',\' s.d \','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.
            ',\' ) \','.TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_MISI.',\'.\','.TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUAN.',\'.\','.TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN.',\'.\','.TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SASARAN.',\'.\','.TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORSASARAN.',\'.\','.TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUANOPD.',\'.\','.TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUANOPD.',\' \','.TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_NM_INDIKATORTUJUANOPD.') AS Text');
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_PEMDA,"inner");
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_TO." >=", date("Y"));
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $data['res'] = $this->db->get(TBL_SAKIP_MOPD_IKTUJUAN)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_bid() {
        $kdUrusan = $this->input->get("Kd_Urusan");
        $kdBidang = $this->input->get("Kd_Bidang");
        $kdUnit = $this->input->get("Kd_Unit");
        $kdSub = $this->input->get("Kd_Sub");

        $this->db->select('*,'.TBL_SAKIP_MBID.'.'.COL_KD_BID.' AS ID,
            CONCAT('.TBL_SAKIP_MBID.'.'.COL_KD_BID.',\'. \','.TBL_SAKIP_MBID.'.'.COL_NM_BID.') AS Text');
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $data['res'] = $this->db->get(TBL_SAKIP_MBID)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_subbid() {
        $kdUrusan = $this->input->get("Kd_Urusan");
        $kdBidang = $this->input->get("Kd_Bidang");
        $kdUnit = $this->input->get("Kd_Unit");
        $kdSub = $this->input->get("Kd_Sub");
        $kdBid = $this->input->get("Kd_Bid");

        $this->db->select('*,'.TBL_SAKIP_MSUBBID.'.'.COL_KD_SUBBID.' AS ID,
            CONCAT('.TBL_SAKIP_MSUBBID.'.'.COL_KD_BID.',\'.\','.TBL_SAKIP_MSUBBID.".".COL_KD_SUBBID.',\'. \','.TBL_SAKIP_MSUBBID.'.'.COL_NM_SUBBID.') AS Text');
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $this->db->where(COL_KD_BID, $kdBid);
        $data['res'] = $this->db->get(TBL_SAKIP_MSUBBID)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_individu() {
        $kdUrusan = $this->input->get("Kd_Urusan");
        $kdBidang = $this->input->get("Kd_Bidang");
        $kdUnit = $this->input->get("Kd_Unit");
        $kdSub = $this->input->get("Kd_Sub");
        $kdBid = $this->input->get("Kd_Bid");
        $kdSubbid = $this->input->get("Kd_Subbid");

        $this->db->select('*,'.TBL_SAKIP_INDIVIDU_SASARAN.'.'.COL_KD_SASARANINDIVIDU.' AS ID,
            CONCAT('.TBL_SAKIP_INDIVIDU_SASARAN.'.'.COL_KD_BID.',\'.\','.TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SUBBID.',\'.\','.TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SASARANINDIVIDU.',\'. \','.TBL_SAKIP_INDIVIDU_SASARAN.'.'.COL_NM_JABATAN.') AS Text');
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $this->db->where(COL_KD_BID, $kdBid);
        $this->db->where(COL_KD_SUBBID, $kdSubbid);
        $this->db->where(COL_KD_TAHUN, date("Y"));
        $data['res'] = $this->db->get(TBL_SAKIP_INDIVIDU_SASARAN)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_iksasaran_opd() {
        $kdUrusan = $this->input->get("Kd_Urusan");
        $kdBidang = $this->input->get("Kd_Bidang");
        $kdUnit = $this->input->get("Kd_Unit");
        $kdSub = $this->input->get("Kd_Sub");

        $this->db->select('*,
            CONCAT('.TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_PEMDA.
            ',\'|\','.TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_MISI.
            ',\'|\','.TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUAN.
            ',\'|\','.TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN.
            ',\'|\','.TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARAN.
            ',\'|\','.TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN.
            ',\'|\','.TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUANOPD.
            ',\'|\','.TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUANOPD.
            ',\'|\','.TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARANOPD.
            ',\'|\','.TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARANOPD.
            ',\'|\','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.
            ',\'|\','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.
            ') AS ID,
            CONCAT(\'( \','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.
            ',\' s.d \','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.
            ',\' ) \','.TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_MISI.
            ',\'.\','.TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUAN.
            ',\'.\','.TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN.
            ',\'.\','.TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARAN.
            ',\'.\','.TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN.
            ',\'.\','.TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUANOPD.
            ',\'.\','.TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUANOPD.
            ',\'.\','.TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARANOPD.
            ',\'.\','.TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARANOPD.
            ',\' \','.TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_NM_INDIKATORSASARANOPD.
            ') AS Text');
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_PEMDA,"inner");
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_TO." >=", date("Y"));
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $data['res'] = $this->db->get(TBL_SAKIP_MOPD_IKSASARAN)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_prog() {
        $kdUrusan = $this->input->get("Kd_Urusan");
        $kdBidang = $this->input->get("Kd_Bidang");
        $kdUnit = $this->input->get("Kd_Unit");
        $kdSub = $this->input->get("Kd_Sub");
        $kdPemda = $this->input->get("Kd_Pemda");
        $data['res'] = [];

        $rpemda = $this->db->where(COL_KD_PEMDA, $kdPemda)->get(TBL_SAKIP_MPEMDA)->row_array();
        if(count($rpemda) > 0) {
            $eplandb = $this->load->database("eplan", true);
            $eplandb->select('*,
            CONCAT((Tahun+1),\'|\',KD_Prog,\'|\',Ket_Prog) AS ID,
            CONCAT(\'(\',(Tahun+1), \')\''.
                    ',\' \','.COL_KD_URUSAN.
                    ',\'.\','.COL_KD_BIDANG.
                    ',\'.\','.COL_KD_UNIT.
                    ',\'.\','.COL_KD_SUB.
                    ',\'.\',Kd_Prog,\' \',Ket_Prog
                ) AS Text');
            $eplandb->where("Tahun >=", $rpemda[COL_KD_TAHUN_FROM]);
            $eplandb->where("Tahun <=", $rpemda[COL_KD_TAHUN_TO]);
            $eplandb->where(COL_KD_URUSAN, $kdUrusan);
            $eplandb->where(COL_KD_BIDANG, $kdBidang);
            $eplandb->where(COL_KD_UNIT, $kdUnit);
            $eplandb->where(COL_KD_SUB, $kdSub);
            $eplandb->order_by(COL_KD_URUSAN, 'asc');
            $eplandb->order_by(COL_KD_BIDANG, 'asc');
            $eplandb->order_by(COL_KD_UNIT, 'asc');
            $eplandb->order_by(COL_KD_SUB, 'asc');
            $eplandb->order_by("Kd_Prog", 'asc');
            $data['res'] = $eplandb->get("ta_program")->result_array();
        }
        $this->load->view('ajax/browse', $data);
    }

    function browse_program_opd() {
        $kdUrusan = $this->input->get("Kd_Urusan");
        $kdBidang = $this->input->get("Kd_Bidang");
        $kdUnit = $this->input->get("Kd_Unit");
        $kdSub = $this->input->get("Kd_Sub");
        $kdBid = $this->input->get("Kd_Bid");

        $this->db->select('*,
            CONCAT('.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PEMDA.
            ',\'|\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_MISI.
            ',\'|\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TUJUAN.
            ',\'|\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORTUJUAN.
            ',\'|\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SASARAN.
            ',\'|\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORSASARAN.
            ',\'|\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TUJUANOPD.
            ',\'|\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORTUJUANOPD.
            ',\'|\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SASARANOPD.
            ',\'|\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORSASARANOPD.
            ',\'|\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TAHUN.
            ',\'|\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PROGRAMOPD.
            ',\'|\','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.
            ',\'|\','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.
            ',\'|\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_NM_PROGRAMOPD.
            ') AS ID,
            CONCAT(\'( \', '.COL_KD_TAHUN.', \' ) \','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_URUSAN.
            ',\'.\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_BIDANG.
            ',\'.\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_UNIT.
            ',\'.\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SUB.
            ',\'.\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PROGRAMOPD.
            ',\' \','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_NM_PROGRAMOPD.
            ') AS Text');
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA,"inner");
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_TO." >=", date("Y"));
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $this->db->where(COL_KD_BID, $kdBid);
        $data['res'] = $this->db->get(TBL_SAKIP_MBID_PROGRAM)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_program_dpa() {
        $kdUrusan = $this->input->get("Kd_Urusan");
        $kdBidang = $this->input->get("Kd_Bidang");
        $kdUnit = $this->input->get("Kd_Unit");
        $kdSub = $this->input->get("Kd_Sub");
        $kdBid = $this->input->get("Kd_Bid");

        $this->db->select('*,
            CONCAT('.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_PEMDA.
            ',\'|\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_MISI.
            ',\'|\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_TUJUAN.
            ',\'|\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORTUJUAN.
            ',\'|\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_SASARAN.
            ',\'|\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORSASARAN.
            ',\'|\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_TUJUANOPD.
            ',\'|\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORTUJUANOPD.
            ',\'|\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_SASARANOPD.
            ',\'|\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORSASARANOPD.
            ',\'|\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_TAHUN.
            ',\'|\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_PROGRAMOPD.
            ',\'|\','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.
            ',\'|\','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.
            ',\'|\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_NM_PROGRAMOPD.
            ') AS ID,
            CONCAT(\'( \', '.COL_KD_TAHUN.', \' ) \','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_URUSAN.
            ',\'.\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_BIDANG.
            ',\'.\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_UNIT.
            ',\'.\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_SUB.
            ',\'.\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_PROGRAMOPD.
            ',\' \','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_NM_PROGRAMOPD.
            ') AS Text');
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA,"inner");
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_TO." >=", date("Y"));
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $this->db->where(COL_KD_BID, $kdBid);
        $data['res'] = $this->db->get(TBL_SAKIP_DPA_PROGRAM)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_keg() {
        $kdUrusan = $this->input->get("Kd_Urusan");
        $kdBidang = $this->input->get("Kd_Bidang");
        $kdUnit = $this->input->get("Kd_Unit");
        $kdSub = $this->input->get("Kd_Sub");
        $kdTahun = $this->input->get("Kd_Tahun");
        $kdProg = $this->input->get("Kd_Prog");
        $data['res'] = [];

        $eplandb = $this->load->database("eplan", true);
        $eplandb->select('*,
            CONCAT(Kd_Keg,\'|\',Ket_Kegiatan) AS ID,
            CONCAT(\'(\',(Tahun+1), \')\''.
            ',\' \','.COL_KD_URUSAN.
            ',\'.\','.COL_KD_BIDANG.
            ',\'.\','.COL_KD_UNIT.
            ',\'.\','.COL_KD_SUB.
            ',\'.\',Kd_Prog,\'.\',Kd_Keg,\' \',Ket_Kegiatan
                ) AS Text');
        $eplandb->where("Tahun", $kdTahun);
        $eplandb->where("Kd_Prog", $kdProg);
        $eplandb->where(COL_KD_URUSAN, $kdUrusan);
        $eplandb->where(COL_KD_BIDANG, $kdBidang);
        $eplandb->where(COL_KD_UNIT, $kdUnit);
        $eplandb->where(COL_KD_SUB, $kdSub);
        $eplandb->order_by(COL_KD_URUSAN, 'asc');
        $eplandb->order_by(COL_KD_BIDANG, 'asc');
        $eplandb->order_by(COL_KD_UNIT, 'asc');
        $eplandb->order_by(COL_KD_SUB, 'asc');
        $eplandb->order_by("Kd_Prog", 'asc');
        $data['res'] = $eplandb->get("ta_kegiatan")->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_iksasaran_bid() {
        $kdUrusan = $this->input->get("Kd_Urusan");
        $kdBidang = $this->input->get("Kd_Bidang");
        $kdUnit = $this->input->get("Kd_Unit");
        $kdSub = $this->input->get("Kd_Sub");
        $kdBid = $this->input->get("Kd_Bid");

        $this->db->select('*,
            CONCAT('.TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_PEMDA.
            ',\'|\','.TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_MISI.
            ',\'|\','.TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_TUJUAN.
            ',\'|\','.TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_INDIKATORTUJUAN.
            ',\'|\','.TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_SASARAN.
            ',\'|\','.TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_INDIKATORSASARAN.
            ',\'|\','.TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_TUJUANOPD.
            ',\'|\','.TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_INDIKATORTUJUANOPD.
            ',\'|\','.TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_SASARANOPD.
            ',\'|\','.TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_INDIKATORSASARANOPD.
            ',\'|\','.TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_SASARANPROGRAMOPD.
            ',\'|\','.TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_INDIKATORPROGRAMOPD.
            ',\'|\','.TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_TAHUN.
            ') AS ID,
            CONCAT(\'( \','.TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_TAHUN.
            //',\' s.d \','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.
            ',\' ) \','/*.TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_MISI.
            ',\'.\','.TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_TUJUAN.
            ',\'.\','.TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_INDIKATORTUJUAN.
            ',\'.\','.TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_SASARAN.
            ',\'.\','.TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_INDIKATORSASARAN.
            ',\'.\','.TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_TUJUANOPD.
            ',\'.\','.TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_INDIKATORTUJUANOPD.
            ',\'.\','.TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_SASARANOPD.
            ',\'.\','.TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_INDIKATORSASARANOPD.
            ',\'.\','.TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_SASARANPROGRAMOPD.
            ',\'.\','.TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_INDIKATORPROGRAMOPD*/.
            ',\' \','.TBL_SAKIP_MBID_INDIKATOR.'.'.COL_NM_INDIKATORPROGRAMOPD.
            ') AS Text');
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_INDIKATOR.".".COL_KD_PEMDA,"inner");
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_TO." >=", date("Y"));
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $this->db->where(COL_KD_BID, $kdBid);
        $data['res'] = $this->db->get(TBL_SAKIP_MBID_INDIKATOR)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_sasaran_subbid() {
        $kdUrusan = $this->input->get("Kd_Urusan");
        $kdBidang = $this->input->get("Kd_Bidang");
        $kdUnit = $this->input->get("Kd_Unit");
        $kdSub = $this->input->get("Kd_Sub");
        $kdBid = $this->input->get("Kd_Bid");
        $kdSubbid = $this->input->get("Kd_Subbid");

        $this->db->select('*,
            CONCAT('.TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_PEMDA.
            ',\'|\','.TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_MISI.
            ',\'|\','.TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_TUJUAN.
            ',\'|\','.TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_INDIKATORTUJUAN.
            ',\'|\','.TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_SASARAN.
            ',\'|\','.TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_INDIKATORSASARAN.
            ',\'|\','.TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_TUJUANOPD.
            ',\'|\','.TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD.
            ',\'|\','.TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_SASARANOPD.
            ',\'|\','.TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_INDIKATORSASARANOPD.
            ',\'|\','.TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_SASARANPROGRAMOPD.
            ',\'|\','.TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_INDIKATORPROGRAMOPD.
            ',\'|\','.TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_TAHUN.
            ',\'|\','.TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_SASARANSUBBIDANG.
            ') AS ID,
            CONCAT(\'( \','.TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_TAHUN.',\' )\' '.
            ',\' \','.TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_NM_SASARANSUBBIDANG.
            ') AS Text');
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_PEMDA,"inner");
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_TO." >=", date("Y"));
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $this->db->where(COL_KD_BID, $kdBid);
        $this->db->where(COL_KD_SUBBID, $kdSubbid);
        $data['res'] = $this->db->get(TBL_SAKIP_MSUBBID_SASARAN)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_sasaranprogram() {
        $kdUrusan = $this->input->get("Kd_Urusan");
        $kdBidang = $this->input->get("Kd_Bidang");
        $kdUnit = $this->input->get("Kd_Unit");
        $kdSub = $this->input->get("Kd_Sub");
        $kdBid = $this->input->get("Kd_Bid");
        $kdTahun = $this->input->get("Kd_Tahun");
        $kdProg = $this->input->get("Kd_Prog");
        $data['res'] = [];

        $this->db->select('*,
            CONCAT(Kd_SasaranProgramOPD,\'|\',Nm_SasaranProgramOPD) AS ID,
            CONCAT('.COL_KD_SASARANPROGRAMOPD.
            ',\'.\',\' \','.COL_NM_SASARANPROGRAMOPD.') AS Text');
        $this->db->where(COL_KD_TAHUN, $kdTahun);
        $this->db->where(COL_KD_PROGRAMOPD, $kdProg);
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $this->db->where(COL_KD_BID, $kdBid);
        $this->db->group_by(array(COL_KD_SASARANPROGRAMOPD, COL_NM_SASARANPROGRAMOPD));
        $this->db->order_by(COL_KD_URUSAN, 'asc');
        $this->db->order_by(COL_KD_BIDANG, 'asc');
        $this->db->order_by(COL_KD_UNIT, 'asc');
        $this->db->order_by(COL_KD_SUB, 'asc');
        $this->db->order_by(COL_KD_PROGRAMOPD, 'asc');
        $this->db->order_by(COL_KD_SASARANPROGRAMOPD, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_MBID_PROGRAM_SASARAN)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_sasarankegiatan() {
        $kdUrusan = $this->input->get("Kd_Urusan");
        $kdBidang = $this->input->get("Kd_Bidang");
        $kdUnit = $this->input->get("Kd_Unit");
        $kdSub = $this->input->get("Kd_Sub");
        $kdBid = $this->input->get("Kd_Bid");
        $kdSubbBid = $this->input->get("Kd_Subbid");
        $kdTahun = $this->input->get("Kd_Tahun");
        $kdProg = $this->input->get("Kd_Prog");
        $kdSasaranProg = $this->input->get("Kd_SasaranProg");
        $kdKegiatan = $this->input->get("Kd_Kegiatan");
        $data['res'] = [];

        $this->db->select('*,
            CONCAT(Kd_SasaranKegiatanOPD,\'|\',Nm_SasaranKegiatanOPD) AS ID,
            CONCAT('.COL_KD_SASARANKEGIATANOPD.
            ',\'.\',\' \','.COL_NM_SASARANKEGIATANOPD.') AS Text');
        $this->db->where(COL_KD_TAHUN, $kdTahun);
        $this->db->where(COL_KD_PROGRAMOPD, $kdProg);
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $this->db->where(COL_KD_BID, $kdBid);
        $this->db->where(COL_KD_SUBBID, $kdSubbBid);
        $this->db->where(COL_KD_SASARANPROGRAMOPD, $kdSasaranProg);
        $this->db->where(COL_KD_KEGIATANOPD, $kdKegiatan);
        $this->db->group_by(array(COL_KD_SASARANKEGIATANOPD, COL_NM_SASARANKEGIATANOPD));
        $this->db->order_by(COL_KD_URUSAN, 'asc');
        $this->db->order_by(COL_KD_BIDANG, 'asc');
        $this->db->order_by(COL_KD_UNIT, 'asc');
        $this->db->order_by(COL_KD_SUB, 'asc');
        $this->db->order_by(COL_KD_PROGRAMOPD, 'asc');
        $this->db->order_by(COL_KD_SASARANPROGRAMOPD, 'asc');
        $this->db->order_by(COL_KD_KEGIATANOPD, 'asc');
        $this->db->order_by(COL_KD_SASARANKEGIATANOPD, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_sasaranprogram_dpa() {
        $kdUrusan = $this->input->get("Kd_Urusan");
        $kdBidang = $this->input->get("Kd_Bidang");
        $kdUnit = $this->input->get("Kd_Unit");
        $kdSub = $this->input->get("Kd_Sub");
        $kdBid = $this->input->get("Kd_Bid");
        $kdTahun = $this->input->get("Kd_Tahun");
        $kdProg = $this->input->get("Kd_Prog");
        $data['res'] = [];

        $this->db->select('*,
            CONCAT(Kd_SasaranProgramOPD,\'|\',Nm_SasaranProgramOPD) AS ID,
            CONCAT('.COL_KD_SASARANPROGRAMOPD.
            ',\'.\',\' \','.COL_NM_SASARANPROGRAMOPD.') AS Text');
        $this->db->where(COL_KD_TAHUN, $kdTahun);
        $this->db->where(COL_KD_PROGRAMOPD, $kdProg);
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $this->db->where(COL_KD_BID, $kdBid);
        $this->db->order_by(COL_KD_URUSAN, 'asc');
        $this->db->order_by(COL_KD_BIDANG, 'asc');
        $this->db->order_by(COL_KD_UNIT, 'asc');
        $this->db->order_by(COL_KD_SUB, 'asc');
        $this->db->order_by(COL_KD_PROGRAMOPD, 'asc');
        $this->db->order_by(COL_KD_SASARANPROGRAMOPD, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_DPA_PROGRAM_SASARAN)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function get_list_indikatorprogram() {
        $kdUrusan = $this->input->get("Kd_Urusan");
        $kdBidang = $this->input->get("Kd_Bidang");
        $kdUnit = $this->input->get("Kd_Unit");
        $kdSub = $this->input->get("Kd_Sub");
        $kdBid = $this->input->get("Kd_Bid");
        $kdTahun = $this->input->get("Kd_Tahun");
        $kdProg = $this->input->get("Kd_Prog");
        $kdSasaranProg = $this->input->get("Kd_SasaranProg");
        $data['res'] = [];

        $this->db->select("*, 'aab' as sel_satuan");
        $this->db->where(COL_KD_TAHUN, $kdTahun);
        $this->db->where(COL_KD_PROGRAMOPD, $kdProg);
        $this->db->where(COL_KD_SASARANPROGRAMOPD, $kdSasaranProg);
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $this->db->where(COL_KD_BID, $kdBid);
        $this->db->group_by(array(COL_KD_INDIKATORPROGRAMOPD, COL_NM_INDIKATORPROGRAMOPD));
        $this->db->order_by(COL_KD_URUSAN, 'asc');
        $this->db->order_by(COL_KD_BIDANG, 'asc');
        $this->db->order_by(COL_KD_UNIT, 'asc');
        $this->db->order_by(COL_KD_SUB, 'asc');
        $this->db->order_by(COL_KD_PROGRAMOPD, 'asc');
        $this->db->order_by(COL_KD_SASARANPROGRAMOPD, 'asc');
        $this->db->order_by(COL_KD_INDIKATORPROGRAMOPD, 'asc');
        $res = $this->db->get(TBL_SAKIP_MBID_PROGRAM_INDIKATOR)->result_array();

        echo json_encode($res);
    }

    function get_list_indikatorkegiatan() {
        $kdUrusan = $this->input->get("Kd_Urusan");
        $kdBidang = $this->input->get("Kd_Bidang");
        $kdUnit = $this->input->get("Kd_Unit");
        $kdSub = $this->input->get("Kd_Sub");
        $kdBid = $this->input->get("Kd_Bid");
        $kdSubbid = $this->input->get("Kd_Subbid");
        $kdTahun = $this->input->get("Kd_Tahun");
        $kdProg = $this->input->get("Kd_Prog");
        $kdSasaranProg = $this->input->get("Kd_SasaranProg");
        $kdKegiatan = $this->input->get("Kd_Kegiatan");
        $kdSasaranKeg = $this->input->get("Kd_SasaranKeg");
        $data['res'] = [];

        $this->db->where(COL_KD_TAHUN, $kdTahun);
        $this->db->where(COL_KD_PROGRAMOPD, $kdProg);
        $this->db->where(COL_KD_SASARANPROGRAMOPD, $kdSasaranProg);
        $this->db->where(COL_KD_KEGIATANOPD, $kdKegiatan);
        $this->db->where(COL_KD_SASARANKEGIATANOPD, $kdSasaranKeg);
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $this->db->where(COL_KD_BID, $kdBid);
        $this->db->where(COL_KD_SUBBID, $kdSubbid);
        $this->db->group_by(array(COL_KD_INDIKATORKEGIATANOPD, COL_NM_INDIKATORKEGIATANOPD));
        $this->db->order_by(COL_KD_URUSAN, 'asc');
        $this->db->order_by(COL_KD_BIDANG, 'asc');
        $this->db->order_by(COL_KD_UNIT, 'asc');
        $this->db->order_by(COL_KD_SUB, 'asc');
        $this->db->order_by(COL_KD_PROGRAMOPD, 'asc');
        $this->db->order_by(COL_KD_SASARANPROGRAMOPD, 'asc');
        $this->db->order_by(COL_KD_KEGIATANOPD, 'asc');
        $this->db->order_by(COL_KD_INDIKATORKEGIATANOPD, 'asc');
        $res = $this->db->get(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR)->result_array();

        echo json_encode($res);
    }

    function browse_kegiatan_opd() {
        $kdUrusan = $this->input->get("Kd_Urusan");
        $kdBidang = $this->input->get("Kd_Bidang");
        $kdUnit = $this->input->get("Kd_Unit");
        $kdSub = $this->input->get("Kd_Sub");
        $kdBid = $this->input->get("Kd_Bid");
        $kdSubBid = $this->input->get("Kd_Subbid");
        $kdTahun = $this->input->get("Kd_Tahun");
        $kdProgram = $this->input->get("Kd_Prog");
        $kdSasaranProg = $this->input->get("Kd_SasaranProg");

        $this->db->select('*,
            CONCAT('.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_PEMDA.
            ',\'|\','.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_MISI.
            ',\'|\','.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_TUJUAN.
            ',\'|\','.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_INDIKATORTUJUAN.
            ',\'|\','.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_SASARAN.
            ',\'|\','.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_INDIKATORSASARAN.
            ',\'|\','.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_TUJUANOPD.
            ',\'|\','.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_INDIKATORTUJUANOPD.
            ',\'|\','.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_SASARANOPD.
            ',\'|\','.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_INDIKATORSASARANOPD.
            ',\'|\','.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_TAHUN.
            ',\'|\','.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_PROGRAMOPD.
            ',\'|\','.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_SASARANPROGRAMOPD.
            ',\'|\','.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_KEGIATANOPD.
            ',\'|\','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.
            ',\'|\','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.
            ',\'|\','.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_TOTAL.
            ',\'|\','.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_SUMBERDANA.
            ',\'|\','.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_NM_KEGIATANOPD.
            ') AS ID,
            CONCAT(\'( \', '.TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TAHUN.', \' ) \','.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_URUSAN.
            ',\'.\','.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_BIDANG.
            ',\'.\','.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_UNIT.
            ',\'.\','.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_SUB.
            ',\'.\','.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_PROGRAMOPD.
            ',\'.\','.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_KEGIATANOPD.
            ',\' \','.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_NM_KEGIATANOPD.
            ') AS Text');
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA,"inner");
        /*$this->db->join(TBL_SAKIP_DPA_PROGRAM,
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARANOPD." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BID." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PROGRAMOPD." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TAHUN
            ,"inner");*/
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_TO." >=", date("Y"));
        $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_URUSAN, $kdUrusan);
        $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BIDANG, $kdBidang);
        $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_UNIT, $kdUnit);
        $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUB, $kdSub);
        $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BID, $kdBid);
        $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUBBID, $kdSubBid);
        if(!empty($kdTahun)) $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TAHUN, $kdTahun);
        if(!empty($kdProgram)) $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PROGRAMOPD, $kdProgram);
        if(!empty($kdSasaranProg)) $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARANPROGRAMOPD, $kdSasaranProg);
        $data['res'] = $this->db->get(TBL_SAKIP_MSUBBID_KEGIATAN)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_kegiatan_dpa() {
        $kdUrusan = $this->input->get("Kd_Urusan");
        $kdBidang = $this->input->get("Kd_Bidang");
        $kdUnit = $this->input->get("Kd_Unit");
        $kdSub = $this->input->get("Kd_Sub");
        $kdBid = $this->input->get("Kd_Bid");
        $kdSubBid = $this->input->get("Kd_Subbid");
        $kdTahun = $this->input->get("Kd_Tahun");
        $kdProgram = $this->input->get("Kd_Prog");
        $kdSasaranProg = $this->input->get("Kd_SasaranProg");

        $this->db->select('*,
            CONCAT('.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_PEMDA.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_MISI.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_TUJUAN.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_INDIKATORTUJUAN.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_SASARAN.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_INDIKATORSASARAN.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_TUJUANOPD.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_INDIKATORTUJUANOPD.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_SASARANOPD.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_INDIKATORSASARANOPD.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_TAHUN.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_PROGRAMOPD.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_SASARANPROGRAMOPD.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_KEGIATANOPD.
            ',\'|\','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.
            ',\'|\','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_TOTAL.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_SUMBERDANA.
            ') AS ID,
            CONCAT(\'( \', '.COL_KD_TAHUN.', \' ) \','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_URUSAN.
            ',\'.\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_BIDANG.
            ',\'.\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_UNIT.
            ',\'.\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_SUB.
            ',\'.\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_PROGRAMOPD.
            ',\'.\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_KEGIATANOPD.
            ',\' \','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_NM_KEGIATANOPD.
            ') AS Text');
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_PEMDA,"inner");
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_TO." >=", date("Y"));
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $this->db->where(COL_KD_BID, $kdBid);
        $this->db->where(COL_KD_SUBBID, $kdSubBid);
        if(!empty($kdTahun)) $this->db->where(COL_KD_TAHUN, $kdTahun);
        if(!empty($kdProgram)) $this->db->where(COL_KD_PROGRAMOPD, $kdProgram);
        if(!empty($kdSasaranProg)) $this->db->where(COL_KD_SASARANPROGRAMOPD, $kdSasaranProg);
        $data['res'] = $this->db->get(TBL_SAKIP_DPA_KEGIATAN)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function autocomplete_kdprogram_opd() {
        $kdPemda = $this->input->get("Kd_Pemda");
        $kdUrusan = $this->input->get("Kd_Urusan");
        $kdBidang = $this->input->get("Kd_Bidang");
        $kdUnit = $this->input->get("Kd_Unit");
        $kdSub = $this->input->get("Kd_Sub");
        $query = $this->input->get("query");

        $this->db->select(COL_NM_PROGRAMOPD." as value");
        $this->db->distinct();
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA,"inner");
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_PEMDA, $kdPemda);
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $this->db->like(COL_NM_PROGRAMOPD, $query);
        $rprg = $this->db->get(TBL_SAKIP_MBID_PROGRAM)->result_array();
        $res = array(
            'query' => $query,
            'suggestions' => $rprg
        );
        echo json_encode($res);
    }

    function autocomplete_program_opd() {
        $kdPemda = $this->input->get("Kd_Pemda");
        $kdUrusan = $this->input->get("Kd_Urusan");
        $kdBidang = $this->input->get("Kd_Bidang");
        $kdUnit = $this->input->get("Kd_Unit");
        $kdSub = $this->input->get("Kd_Sub");
        $query = $this->input->get("query");

        $this->db->select('CONCAT('.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TAHUN.',\' - \','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PROGRAMOPD.',\' : \','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_NM_PROGRAMOPD.') as value, CONCAT('.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PROGRAMOPD.',\'|\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_NM_PROGRAMOPD.') as data');
        $this->db->distinct();
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA,"inner");
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_PEMDA, $kdPemda);
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $this->db->like(COL_NM_PROGRAMOPD, $query);
        $rprg = $this->db->get(TBL_SAKIP_MBID_PROGRAM)->result_array();
        $res = array(
            'query' => $query,
            'suggestions' => $rprg
        );
        echo json_encode($res);
    }
    function autocomplete_sasaran_opd() {
        $kdPemda = $this->input->get("Kd_Pemda");
        $kdUrusan = $this->input->get("Kd_Urusan");
        $kdBidang = $this->input->get("Kd_Bidang");
        $kdUnit = $this->input->get("Kd_Unit");
        $kdSub = $this->input->get("Kd_Sub");
        $query = $this->input->get("query");

        $this->db->select('CONCAT('.TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARANOPD.',\' : \','.TBL_SAKIP_MOPD_SASARAN.'.'.COL_NM_SASARANOPD.') as value, CONCAT('.TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARANOPD.',\'|\','.TBL_SAKIP_MOPD_SASARAN.'.'.COL_NM_SASARANOPD.') as data');
        $this->db->distinct();
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA,"inner");
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_PEMDA, $kdPemda);
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $this->db->like(COL_NM_SASARANOPD, $query);
        $rsasaran = $this->db->get(TBL_SAKIP_MOPD_SASARAN)->result_array();
        $res = array(
            'query' => $query,
            'suggestions' => $rsasaran
        );
        echo json_encode($res);
    }

    function get_opt_misi() {
        $kdPemda = $this->input->post("Kd_Pemda");

        echo GetCombobox("select * from sakip_mpmd_misi where Kd_Pemda = $kdPemda", COL_KD_MISI, COL_NM_MISI, null, true, false, '-- Semua --');
    }

    function get_opt_tujuan() {
        $kdPemda = $this->input->post("Kd_Pemda");
        $kdMisi = $this->input->post("Kd_Misi");

        echo GetCombobox("select * from sakip_mpmd_tujuan where Kd_Pemda = $kdPemda and Kd_Misi = $kdMisi", COL_KD_TUJUAN, COL_NM_TUJUAN);
    }
    function get_opt_iktujuan() {
        $kdPemda = $this->input->post(COL_KD_PEMDA);
        $kdMisi = $this->input->post(COL_KD_MISI);
        $kdTujuan = $this->input->post(COL_KD_TUJUAN);

        echo GetCombobox("select * from sakip_mpmd_iktujuan where Kd_Pemda = $kdPemda and Kd_Misi = $kdMisi and Kd_Tujuan = $kdTujuan", COL_KD_INDIKATORTUJUAN, COL_NM_INDIKATORTUJUAN);
    }
    function get_opt_sasaran() {
        $kdPemda = $this->input->post(COL_KD_PEMDA);
        $kdMisi = $this->input->post(COL_KD_MISI);
        $kdTujuan = $this->input->post(COL_KD_TUJUAN);
        $kdIkTujuan = $this->input->post(COL_KD_INDIKATORTUJUAN);

        echo GetCombobox("select * from sakip_mpmd_sasaran where Kd_Pemda = $kdPemda and Kd_Misi = $kdMisi and Kd_Tujuan = $kdTujuan and Kd_IndikatorTujuan = $kdIkTujuan", COL_KD_SASARAN, COL_NM_SASARAN);
    }
    function get_opt_iksasaran() {
        $kdPemda = $this->input->post(COL_KD_PEMDA);
        $kdMisi = $this->input->post(COL_KD_MISI);
        $kdTujuan = $this->input->post(COL_KD_TUJUAN);
        $kdIkTujuan = $this->input->post(COL_KD_INDIKATORTUJUAN);
        $kdSasaran = $this->input->post(COL_KD_SASARAN);

        echo GetCombobox("select * from sakip_mpmd_iksasaran where Kd_Pemda = $kdPemda and Kd_Misi = $kdMisi and Kd_Tujuan = $kdTujuan and Kd_IndikatorTujuan = $kdIkTujuan and Kd_Sasaran = $kdSasaran", COL_KD_INDIKATORSASARAN, COL_NM_INDIKATORSASARAN);
    }
    function get_opt_tujuanopd() {
        $kdPemda = $this->input->post(COL_KD_PEMDA);
        $kdMisi = $this->input->post(COL_KD_MISI);
        $kdTujuan = $this->input->post(COL_KD_TUJUAN);
        $kdIkTujuan = $this->input->post(COL_KD_INDIKATORTUJUAN);
        $kdSasaran = $this->input->post(COL_KD_SASARAN);
        $kdIkSasaran = $this->input->post(COL_KD_INDIKATORSASARAN);
        $kdUrusan = $this->input->post(COL_KD_URUSAN);
        $kdBidang = $this->input->post(COL_KD_BIDANG);
        $kdUnit = $this->input->post(COL_KD_UNIT);
        $kdSub = $this->input->post(COL_KD_SUB);

        echo GetCombobox("select * from sakip_mopd_tujuan where Kd_Pemda = $kdPemda and Kd_Misi = $kdMisi and Kd_Tujuan = $kdTujuan and Kd_IndikatorTujuan = $kdIkTujuan and Kd_Sasaran = $kdSasaran and Kd_IndikatorSasaran = $kdIkSasaran and Kd_Urusan = $kdUrusan and Kd_Bidang = $kdBidang and Kd_Unit = $kdUnit and Kd_Sub = $kdSub", COL_KD_TUJUANOPD, array(array(COL_KD_TUJUANOPD, COL_NM_TUJUANOPD), "."));
    }
    function get_opt_iktujuanopd() {
        $kdPemda = $this->input->post(COL_KD_PEMDA);
        $kdMisi = $this->input->post(COL_KD_MISI);
        $kdTujuan = $this->input->post(COL_KD_TUJUAN);
        $kdIkTujuan = $this->input->post(COL_KD_INDIKATORTUJUAN);
        $kdSasaran = $this->input->post(COL_KD_SASARAN);
        $kdIkSasaran = $this->input->post(COL_KD_INDIKATORSASARAN);
        $kdTujuanOPD = $this->input->post(COL_KD_TUJUANOPD);
        $kdUrusan = $this->input->post(COL_KD_URUSAN);
        $kdBidang = $this->input->post(COL_KD_BIDANG);
        $kdUnit = $this->input->post(COL_KD_UNIT);
        $kdSub = $this->input->post(COL_KD_SUB);

        echo GetCombobox("select * from sakip_mopd_iktujuan where Kd_Pemda = $kdPemda and Kd_Misi = $kdMisi and Kd_Tujuan = $kdTujuan and Kd_IndikatorTujuan = $kdIkTujuan and Kd_Sasaran = $kdSasaran and Kd_IndikatorSasaran = $kdIkSasaran and Kd_TujuanOPD = $kdTujuanOPD and Kd_Urusan = $kdUrusan and Kd_Bidang = $kdBidang and Kd_Unit = $kdUnit and Kd_Sub = $kdSub", COL_KD_INDIKATORTUJUANOPD, array(array(COL_KD_TUJUANOPD, COL_KD_INDIKATORTUJUANOPD, COL_NM_INDIKATORTUJUANOPD), "."));
    }
    function get_opt_sasaranopd() {
        $kdPemda = $this->input->post(COL_KD_PEMDA);
        $kdMisi = $this->input->post(COL_KD_MISI);
        $kdTujuan = $this->input->post(COL_KD_TUJUAN);
        $kdIkTujuan = $this->input->post(COL_KD_INDIKATORTUJUAN);
        $kdSasaran = $this->input->post(COL_KD_SASARAN);
        $kdIkSasaran = $this->input->post(COL_KD_INDIKATORSASARAN);
        $kdTujuanOPD = $this->input->post(COL_KD_TUJUANOPD);
        $kdIkTujuanOPD = $this->input->post(COL_KD_INDIKATORTUJUANOPD);
        $kdUrusan = $this->input->post(COL_KD_URUSAN);
        $kdBidang = $this->input->post(COL_KD_BIDANG);
        $kdUnit = $this->input->post(COL_KD_UNIT);
        $kdSub = $this->input->post(COL_KD_SUB);

        echo GetCombobox("select * from sakip_mopd_sasaran where Kd_Pemda = $kdPemda and Kd_Misi = $kdMisi and Kd_Tujuan = $kdTujuan and Kd_IndikatorTujuan = $kdIkTujuan and Kd_Sasaran = $kdSasaran and Kd_IndikatorSasaran = $kdIkSasaran and Kd_TujuanOPD = $kdTujuanOPD and Kd_IndikatorTujuanOPD = $kdIkTujuanOPD and Kd_Urusan = $kdUrusan and Kd_Bidang = $kdBidang and Kd_Unit = $kdUnit and Kd_Sub = $kdSub", COL_KD_SASARANOPD, array(array(COL_KD_TUJUANOPD, COL_KD_INDIKATORTUJUANOPD, COL_KD_SASARANOPD, COL_NM_SASARANOPD), "."));
    }
    function get_opt_iksasaranopd() {
        $kdPemda = $this->input->post(COL_KD_PEMDA);
        $kdMisi = $this->input->post(COL_KD_MISI);
        $kdTujuan = $this->input->post(COL_KD_TUJUAN);
        $kdIkTujuan = $this->input->post(COL_KD_INDIKATORTUJUAN);
        $kdSasaran = $this->input->post(COL_KD_SASARAN);
        $kdIkSasaran = $this->input->post(COL_KD_INDIKATORSASARAN);
        $kdTujuanOPD = $this->input->post(COL_KD_TUJUANOPD);
        $kdIkTujuanOPD = $this->input->post(COL_KD_INDIKATORTUJUANOPD);
        $kdSasaranOPD = $this->input->post(COL_KD_SASARANOPD);
        $kdUrusan = $this->input->post(COL_KD_URUSAN);
        $kdBidang = $this->input->post(COL_KD_BIDANG);
        $kdUnit = $this->input->post(COL_KD_UNIT);
        $kdSub = $this->input->post(COL_KD_SUB);
        echo GetCombobox("select * from sakip_mopd_iksasaran where Kd_Pemda = $kdPemda and Kd_Misi = $kdMisi and Kd_Tujuan = $kdTujuan and Kd_IndikatorTujuan = $kdIkTujuan and Kd_Sasaran = $kdSasaran and Kd_IndikatorSasaran = $kdIkSasaran and Kd_TujuanOPD = $kdTujuanOPD and Kd_IndikatorTujuanOPD = $kdIkTujuanOPD and Kd_SasaranOPD = $kdSasaranOPD and Kd_Urusan = $kdUrusan and Kd_Bidang = $kdBidang and Kd_Unit = $kdUnit and Kd_Sub = $kdSub", COL_KD_INDIKATORSASARANOPD, array(array(COL_KD_TUJUANOPD, COL_KD_INDIKATORTUJUANOPD, COL_KD_SASARANOPD, COL_KD_INDIKATORSASARANOPD, COL_NM_INDIKATORSASARANOPD), "."));
    }


    function browse_iku_pemda() {
        $kdPemda = $this->input->post("Kd_Pemda");

        $data['data'] = array(
            COL_KD_PEMDA => $kdPemda
        );

        $this->load->view('ajax/browse_iku', $data);
    }
    function browse_iku_opd() {
        $kdPemda = $this->input->post(COL_KD_PEMDA);
        $kdMisi = $this->input->post(COL_KD_MISI);
        $kdTujuan = $this->input->post(COL_KD_TUJUAN);
        $kdIkTujuan = $this->input->post(COL_KD_INDIKATORTUJUAN);
        $kdSasaran = $this->input->post(COL_KD_SASARAN);
        $kdIkSasaran = $this->input->post(COL_KD_INDIKATORSASARAN);
        $kdUrusan = $this->input->post(COL_KD_URUSAN);
        $kdBidang = $this->input->post(COL_KD_BIDANG);
        $kdUnit = $this->input->post(COL_KD_UNIT);
        $kdSub = $this->input->post(COL_KD_SUB);

        $data['data'] = array(
            COL_KD_PEMDA => $kdPemda,
            COL_KD_MISI => $kdMisi,
            COL_KD_TUJUAN => $kdTujuan,
            COL_KD_INDIKATORTUJUAN => $kdIkTujuan,
            COL_KD_SASARAN => $kdSasaran,
            COL_KD_INDIKATORSASARAN => $kdIkSasaran,
            COL_KD_URUSAN => $kdUrusan,
            COL_KD_BIDANG => $kdBidang,
            COL_KD_UNIT => $kdUnit,
            COL_KD_SUB => $kdSub,
        );

        $this->load->view('ajax/browse_iku_opd', $data);
    }

    function browse_program_opd_2() {
        $kdPemda = $this->input->post(COL_KD_PEMDA);
        $kdMisi = $this->input->post(COL_KD_MISI);
        $kdTujuan = $this->input->post(COL_KD_TUJUAN);
        $kdIkTujuan = $this->input->post(COL_KD_INDIKATORTUJUAN);
        $kdSasaran = $this->input->post(COL_KD_SASARAN);
        $kdIkSasaran = $this->input->post(COL_KD_INDIKATORSASARAN);
        $kdUrusan = $this->input->post(COL_KD_URUSAN);
        $kdBidang = $this->input->post(COL_KD_BIDANG);
        $kdUnit = $this->input->post(COL_KD_UNIT);
        $kdSub = $this->input->post(COL_KD_SUB);
        $kdBid = $this->input->post(COL_KD_BID);
        $kdTujuanOPD = $this->input->post(COL_KD_TUJUANOPD);
        $kdIkTujuanOPD = $this->input->post(COL_KD_INDIKATORTUJUANOPD);
        $kdSasaranOPD = $this->input->post(COL_KD_SASARANOPD);
        $kdIkSasaranOPD = $this->input->post(COL_KD_INDIKATORSASARANOPD);

        $this->db->select('*,
            CONCAT('.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PEMDA.
            ',\'|\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_MISI.
            ',\'|\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TUJUAN.
            ',\'|\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORTUJUAN.
            ',\'|\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SASARAN.
            ',\'|\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORSASARAN.
            ',\'|\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TUJUANOPD.
            ',\'|\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORTUJUANOPD.
            ',\'|\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SASARANOPD.
            ',\'|\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORSASARANOPD.
            ',\'|\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TAHUN.
            ',\'|\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PROGRAMOPD.
            ',\'|\','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.
            ',\'|\','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.
            ',\'|\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_NM_PROGRAMOPD.
            ') AS ID,
            CONCAT(\'( \', '.COL_KD_TAHUN.', \' ) \','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_URUSAN.
            ',\'.\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_BIDANG.
            ',\'.\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_UNIT.
            ',\'.\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SUB.
            ',\'.\','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PROGRAMOPD.
            ',\' \','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_NM_PROGRAMOPD.
            ') AS Text');
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA,"inner");
        /*$this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_TO." >=", date("Y"));
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $this->db->where(COL_KD_BID, $kdBid);*/
        $this->db->where(array(
            TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA=>$kdPemda,
            COL_KD_MISI=>$kdMisi,
            COL_KD_TUJUAN=>$kdTujuan,
            COL_KD_INDIKATORTUJUAN=>$kdIkTujuan,
            COL_KD_SASARAN=>$kdSasaran,
            COL_KD_INDIKATORSASARAN=>$kdIkSasaran,
            COL_KD_URUSAN=>$kdUrusan,
            COL_KD_BIDANG=>$kdBidang,
            COL_KD_UNIT=>$kdUnit,
            COL_KD_SUB=>$kdSub,
            COL_KD_BID=>$kdBid,
            COL_KD_TUJUANOPD=>$kdTujuanOPD,
            COL_KD_INDIKATORTUJUANOPD=>$kdIkTujuanOPD,
            COL_KD_SASARANOPD=>$kdSasaranOPD,
            COL_KD_INDIKATORSASARANOPD=>$kdIkSasaranOPD
        ));
        $data['res'] = $this->db->get(TBL_SAKIP_MBID_PROGRAM)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_sasaranprogram_2() {
        $kdPemda = $this->input->post(COL_KD_PEMDA);
        $kdMisi = $this->input->post(COL_KD_MISI);
        $kdTujuan = $this->input->post(COL_KD_TUJUAN);
        $kdIkTujuan = $this->input->post(COL_KD_INDIKATORTUJUAN);
        $kdSasaran = $this->input->post(COL_KD_SASARAN);
        $kdIkSasaran = $this->input->post(COL_KD_INDIKATORSASARAN);
        $kdUrusan = $this->input->post(COL_KD_URUSAN);
        $kdBidang = $this->input->post(COL_KD_BIDANG);
        $kdUnit = $this->input->post(COL_KD_UNIT);
        $kdSub = $this->input->post(COL_KD_SUB);
        $kdBid = $this->input->post(COL_KD_BID);
        $kdTujuanOPD = $this->input->post(COL_KD_TUJUANOPD);
        $kdIkTujuanOPD = $this->input->post(COL_KD_INDIKATORTUJUANOPD);
        $kdSasaranOPD = $this->input->post(COL_KD_SASARANOPD);
        $kdIkSasaranOPD = $this->input->post(COL_KD_INDIKATORSASARANOPD);
        $kdProgramOPD = $this->input->post(COL_KD_PROGRAMOPD);
        $kdTahun = $this->input->post(COL_KD_TAHUN);
        $data['res'] = [];

        $this->db->select('*,
            CONCAT(Kd_SasaranProgramOPD,\'|\',Nm_SasaranProgramOPD) AS ID,
            CONCAT('.COL_KD_SASARANPROGRAMOPD.
            ',\'.\',\' \','.COL_NM_SASARANPROGRAMOPD.') AS Text');
        /*$this->db->where(COL_KD_TAHUN, $kdTahun);
        $this->db->where(COL_KD_PROGRAMOPD, $kdProg);
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $this->db->where(COL_KD_BID, $kdBid);*/
        $this->db->where(array(
            COL_KD_PEMDA=>$kdPemda,
            COL_KD_MISI=>$kdMisi,
            COL_KD_TUJUAN=>$kdTujuan,
            COL_KD_INDIKATORTUJUAN=>$kdIkTujuan,
            COL_KD_SASARAN=>$kdSasaran,
            COL_KD_INDIKATORSASARAN=>$kdIkSasaran,
            COL_KD_URUSAN=>$kdUrusan,
            COL_KD_BIDANG=>$kdBidang,
            COL_KD_UNIT=>$kdUnit,
            COL_KD_SUB=>$kdSub,
            COL_KD_BID=>$kdBid,
            COL_KD_TUJUANOPD=>$kdTujuanOPD,
            COL_KD_INDIKATORTUJUANOPD=>$kdIkTujuanOPD,
            COL_KD_SASARANOPD=>$kdSasaranOPD,
            COL_KD_INDIKATORSASARANOPD=>$kdIkSasaranOPD,
            COL_KD_PROGRAMOPD=>$kdProgramOPD,
            COL_KD_TAHUN=>$kdTahun
        ));
        $this->db->order_by(COL_KD_URUSAN, 'asc');
        $this->db->order_by(COL_KD_BIDANG, 'asc');
        $this->db->order_by(COL_KD_UNIT, 'asc');
        $this->db->order_by(COL_KD_SUB, 'asc');
        $this->db->order_by(COL_KD_PROGRAMOPD, 'asc');
        $this->db->order_by(COL_KD_SASARANPROGRAMOPD, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_MBID_PROGRAM_SASARAN)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_kegiatan_opd_2() {
        $kdPemda = $this->input->post(COL_KD_PEMDA);
        $kdMisi = $this->input->post(COL_KD_MISI);
        $kdTujuan = $this->input->post(COL_KD_TUJUAN);
        $kdIkTujuan = $this->input->post(COL_KD_INDIKATORTUJUAN);
        $kdSasaran = $this->input->post(COL_KD_SASARAN);
        $kdIkSasaran = $this->input->post(COL_KD_INDIKATORSASARAN);
        $kdUrusan = $this->input->post(COL_KD_URUSAN);
        $kdBidang = $this->input->post(COL_KD_BIDANG);
        $kdUnit = $this->input->post(COL_KD_UNIT);
        $kdSub = $this->input->post(COL_KD_SUB);
        $kdBid = $this->input->post(COL_KD_BID);
        $kdSubbid = $this->input->post(COL_KD_SUBBID);
        $kdTujuanOPD = $this->input->post(COL_KD_TUJUANOPD);
        $kdIkTujuanOPD = $this->input->post(COL_KD_INDIKATORTUJUANOPD);
        $kdSasaranOPD = $this->input->post(COL_KD_SASARANOPD);
        $kdIkSasaranOPD = $this->input->post(COL_KD_INDIKATORSASARANOPD);
        $kdProgramOPD = $this->input->post(COL_KD_PROGRAMOPD);
        $kdTahun = $this->input->post(COL_KD_TAHUN);
        $kdSasaranProgramOPD = $this->input->post(COL_KD_SASARANPROGRAMOPD);
        $data['res'] = [];

        $this->db->select('*,
            CONCAT(Kd_KegiatanOPD,\'|\',Nm_KegiatanOPD,\'|\',Kd_SumberDana,\'|\',Total) AS ID,
            CONCAT('.COL_KD_KEGIATANOPD.
            ',\'.\',\' \','.COL_NM_KEGIATANOPD.') AS Text');
        /*$this->db->where(COL_KD_TAHUN, $kdTahun);
        $this->db->where(COL_KD_PROGRAMOPD, $kdProg);
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $this->db->where(COL_KD_BID, $kdBid);*/
        $this->db->where(array(
            COL_KD_PEMDA=>$kdPemda,
            COL_KD_MISI=>$kdMisi,
            COL_KD_TUJUAN=>$kdTujuan,
            COL_KD_INDIKATORTUJUAN=>$kdIkTujuan,
            COL_KD_SASARAN=>$kdSasaran,
            COL_KD_INDIKATORSASARAN=>$kdIkSasaran,
            COL_KD_URUSAN=>$kdUrusan,
            COL_KD_BIDANG=>$kdBidang,
            COL_KD_UNIT=>$kdUnit,
            COL_KD_SUB=>$kdSub,
            COL_KD_BID=>$kdBid,
            COL_KD_SUBBID=>$kdSubbid,
            COL_KD_TUJUANOPD=>$kdTujuanOPD,
            COL_KD_INDIKATORTUJUANOPD=>$kdIkTujuanOPD,
            COL_KD_SASARANOPD=>$kdSasaranOPD,
            COL_KD_INDIKATORSASARANOPD=>$kdIkSasaranOPD,
            COL_KD_PROGRAMOPD=>$kdProgramOPD,
            COL_KD_TAHUN=>$kdTahun,
            COL_KD_SASARANPROGRAMOPD=>$kdSasaranProgramOPD,
        ));
        $this->db->order_by(COL_KD_URUSAN, 'asc');
        $this->db->order_by(COL_KD_BIDANG, 'asc');
        $this->db->order_by(COL_KD_UNIT, 'asc');
        $this->db->order_by(COL_KD_SUB, 'asc');
        $this->db->order_by(COL_KD_PROGRAMOPD, 'asc');
        $this->db->order_by(COL_KD_SASARANPROGRAMOPD, 'asc');
        $this->db->order_by(COL_KD_KEGIATANOPD, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_MSUBBID_KEGIATAN)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_program_dpa_2() {
        $kdPemda = $this->input->post(COL_KD_PEMDA);
        $kdMisi = $this->input->post(COL_KD_MISI);
        $kdTujuan = $this->input->post(COL_KD_TUJUAN);
        $kdIkTujuan = $this->input->post(COL_KD_INDIKATORTUJUAN);
        $kdSasaran = $this->input->post(COL_KD_SASARAN);
        $kdIkSasaran = $this->input->post(COL_KD_INDIKATORSASARAN);
        $kdUrusan = $this->input->post(COL_KD_URUSAN);
        $kdBidang = $this->input->post(COL_KD_BIDANG);
        $kdUnit = $this->input->post(COL_KD_UNIT);
        $kdSub = $this->input->post(COL_KD_SUB);
        $kdBid = $this->input->post(COL_KD_BID);
        $kdTujuanOPD = $this->input->post(COL_KD_TUJUANOPD);
        $kdIkTujuanOPD = $this->input->post(COL_KD_INDIKATORTUJUANOPD);
        $kdSasaranOPD = $this->input->post(COL_KD_SASARANOPD);
        $kdIkSasaranOPD = $this->input->post(COL_KD_INDIKATORSASARANOPD);

        $this->db->select('*,
            CONCAT('.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_PEMDA.
            ',\'|\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_MISI.
            ',\'|\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_TUJUAN.
            ',\'|\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORTUJUAN.
            ',\'|\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_SASARAN.
            ',\'|\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORSASARAN.
            ',\'|\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_TUJUANOPD.
            ',\'|\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORTUJUANOPD.
            ',\'|\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_SASARANOPD.
            ',\'|\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORSASARANOPD.
            ',\'|\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_TAHUN.
            ',\'|\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_PROGRAMOPD.
            ',\'|\','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.
            ',\'|\','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.
            ',\'|\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_NM_PROGRAMOPD.
            ') AS ID,
            CONCAT(\'( \', '.COL_KD_TAHUN.', \' ) \','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_URUSAN.
            ',\'.\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_BIDANG.
            ',\'.\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_UNIT.
            ',\'.\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_SUB.
            ',\'.\','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_PROGRAMOPD.
            ',\' \','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_NM_PROGRAMOPD.
            ') AS Text');
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA,"inner");
        /*$this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_TO." >=", date("Y"));
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $this->db->where(COL_KD_BID, $kdBid);*/
        $this->db->where(array(
            TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA=>$kdPemda,
            COL_KD_MISI=>$kdMisi,
            COL_KD_TUJUAN=>$kdTujuan,
            COL_KD_INDIKATORTUJUAN=>$kdIkTujuan,
            COL_KD_SASARAN=>$kdSasaran,
            COL_KD_INDIKATORSASARAN=>$kdIkSasaran,
            COL_KD_URUSAN=>$kdUrusan,
            COL_KD_BIDANG=>$kdBidang,
            COL_KD_UNIT=>$kdUnit,
            COL_KD_SUB=>$kdSub,
            COL_KD_BID=>$kdBid,
            COL_KD_TUJUANOPD=>$kdTujuanOPD,
            COL_KD_INDIKATORTUJUANOPD=>$kdIkTujuanOPD,
            COL_KD_SASARANOPD=>$kdSasaranOPD,
            COL_KD_INDIKATORSASARANOPD=>$kdIkSasaranOPD
        ));
        $data['res'] = $this->db->get(TBL_SAKIP_DPA_PROGRAM)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_sasaranprogram_dpa_2() {
        $kdPemda = $this->input->post(COL_KD_PEMDA);
        $kdMisi = $this->input->post(COL_KD_MISI);
        $kdTujuan = $this->input->post(COL_KD_TUJUAN);
        $kdIkTujuan = $this->input->post(COL_KD_INDIKATORTUJUAN);
        $kdSasaran = $this->input->post(COL_KD_SASARAN);
        $kdIkSasaran = $this->input->post(COL_KD_INDIKATORSASARAN);
        $kdUrusan = $this->input->post(COL_KD_URUSAN);
        $kdBidang = $this->input->post(COL_KD_BIDANG);
        $kdUnit = $this->input->post(COL_KD_UNIT);
        $kdSub = $this->input->post(COL_KD_SUB);
        $kdBid = $this->input->post(COL_KD_BID);
        $kdTujuanOPD = $this->input->post(COL_KD_TUJUANOPD);
        $kdIkTujuanOPD = $this->input->post(COL_KD_INDIKATORTUJUANOPD);
        $kdSasaranOPD = $this->input->post(COL_KD_SASARANOPD);
        $kdIkSasaranOPD = $this->input->post(COL_KD_INDIKATORSASARANOPD);
        $kdProgramOPD = $this->input->post(COL_KD_PROGRAMOPD);
        $kdTahun = $this->input->post(COL_KD_TAHUN);
        $data['res'] = [];

        $this->db->select('*,
            CONCAT(Kd_SasaranProgramOPD,\'|\',Nm_SasaranProgramOPD) AS ID,
            CONCAT('.COL_KD_SASARANPROGRAMOPD.
            ',\'.\',\' \','.COL_NM_SASARANPROGRAMOPD.') AS Text');
        /*$this->db->where(COL_KD_TAHUN, $kdTahun);
        $this->db->where(COL_KD_PROGRAMOPD, $kdProg);
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $this->db->where(COL_KD_BID, $kdBid);*/
        $this->db->where(array(
            TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_PEMDA=>$kdPemda,
            COL_KD_MISI=>$kdMisi,
            COL_KD_TUJUAN=>$kdTujuan,
            COL_KD_INDIKATORTUJUAN=>$kdIkTujuan,
            COL_KD_SASARAN=>$kdSasaran,
            COL_KD_INDIKATORSASARAN=>$kdIkSasaran,
            COL_KD_URUSAN=>$kdUrusan,
            COL_KD_BIDANG=>$kdBidang,
            COL_KD_UNIT=>$kdUnit,
            COL_KD_SUB=>$kdSub,
            COL_KD_BID=>$kdBid,
            COL_KD_TUJUANOPD=>$kdTujuanOPD,
            COL_KD_INDIKATORTUJUANOPD=>$kdIkTujuanOPD,
            COL_KD_SASARANOPD=>$kdSasaranOPD,
            COL_KD_INDIKATORSASARANOPD=>$kdIkSasaranOPD,
            COL_KD_PROGRAMOPD=>$kdProgramOPD,
            COL_KD_TAHUN=>$kdTahun
        ));
        $this->db->order_by(COL_KD_URUSAN, 'asc');
        $this->db->order_by(COL_KD_BIDANG, 'asc');
        $this->db->order_by(COL_KD_UNIT, 'asc');
        $this->db->order_by(COL_KD_SUB, 'asc');
        $this->db->order_by(COL_KD_PROGRAMOPD, 'asc');
        $this->db->order_by(COL_KD_SASARANPROGRAMOPD, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_DPA_PROGRAM_SASARAN)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_kegiatan_dpa_2() {
        $kdPemda = $this->input->post(COL_KD_PEMDA);
        $kdMisi = $this->input->post(COL_KD_MISI);
        $kdTujuan = $this->input->post(COL_KD_TUJUAN);
        $kdIkTujuan = $this->input->post(COL_KD_INDIKATORTUJUAN);
        $kdSasaran = $this->input->post(COL_KD_SASARAN);
        $kdIkSasaran = $this->input->post(COL_KD_INDIKATORSASARAN);
        $kdUrusan = $this->input->post(COL_KD_URUSAN);
        $kdBidang = $this->input->post(COL_KD_BIDANG);
        $kdUnit = $this->input->post(COL_KD_UNIT);
        $kdSub = $this->input->post(COL_KD_SUB);
        $kdBid = $this->input->post(COL_KD_BID);
        $kdSubbid = $this->input->post(COL_KD_SUBBID);
        $kdTujuanOPD = $this->input->post(COL_KD_TUJUANOPD);
        $kdIkTujuanOPD = $this->input->post(COL_KD_INDIKATORTUJUANOPD);
        $kdSasaranOPD = $this->input->post(COL_KD_SASARANOPD);
        $kdIkSasaranOPD = $this->input->post(COL_KD_INDIKATORSASARANOPD);
        $kdProgramOPD = $this->input->post(COL_KD_PROGRAMOPD);
        $kdTahun = $this->input->post(COL_KD_TAHUN);
        $kdSasaranProgramOPD = $this->input->post(COL_KD_SASARANPROGRAMOPD);

        $this->db->select('*,
            CONCAT('.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_PEMDA.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_MISI.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_TUJUAN.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_INDIKATORTUJUAN.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_SASARAN.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_INDIKATORSASARAN.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_TUJUANOPD.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_INDIKATORTUJUANOPD.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_SASARANOPD.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_INDIKATORSASARANOPD.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_TAHUN.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_PROGRAMOPD.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_SASARANPROGRAMOPD.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_KEGIATANOPD.
            ',\'|\','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.
            ',\'|\','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_TOTAL.
            ',\'|\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_SUMBERDANA.
            ') AS ID,
            CONCAT(\'( \', '.COL_KD_TAHUN.', \' ) \','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_URUSAN.
            ',\'.\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_BIDANG.
            ',\'.\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_UNIT.
            ',\'.\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_SUB.
            ',\'.\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_PROGRAMOPD.
            ',\'.\','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_KD_KEGIATANOPD.
            ',\' \','.TBL_SAKIP_DPA_KEGIATAN.'.'.COL_NM_KEGIATANOPD.
            ') AS Text');
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_PEMDA,"inner");
        /*$this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_TO." >=", date("Y"));
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $this->db->where(COL_KD_BID, $kdBid);
        $this->db->where(COL_KD_SUBBID, $kdSubBid);*/
        $this->db->where(array(
            TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_PEMDA=>$kdPemda,
            COL_KD_MISI=>$kdMisi,
            COL_KD_TUJUAN=>$kdTujuan,
            COL_KD_INDIKATORTUJUAN=>$kdIkTujuan,
            COL_KD_SASARAN=>$kdSasaran,
            COL_KD_INDIKATORSASARAN=>$kdIkSasaran,
            COL_KD_URUSAN=>$kdUrusan,
            COL_KD_BIDANG=>$kdBidang,
            COL_KD_UNIT=>$kdUnit,
            COL_KD_SUB=>$kdSub,
            COL_KD_BID=>$kdBid,
            COL_KD_SUBBID=>$kdSubbid,
            COL_KD_TUJUANOPD=>$kdTujuanOPD,
            COL_KD_INDIKATORTUJUANOPD=>$kdIkTujuanOPD,
            COL_KD_SASARANOPD=>$kdSasaranOPD,
            COL_KD_INDIKATORSASARANOPD=>$kdIkSasaranOPD/*,
            COL_KD_PROGRAMOPD=>$kdProgramOPD,
            COL_KD_TAHUN=>$kdTahun,
            COL_KD_SASARANPROGRAMOPD=>$kdSasaranProgramOPD,*/
        ));
        if(!empty($kdTahun)) $this->db->where(COL_KD_TAHUN, $kdTahun);
        if(!empty($kdProgramOPD)) $this->db->where(COL_KD_PROGRAMOPD, $kdProgramOPD);
        if(!empty($kdSasaranProgramOPD)) $this->db->where(COL_KD_SASARANPROGRAMOPD, $kdSasaranProgramOPD);
        $data['res'] = $this->db->get(TBL_SAKIP_DPA_KEGIATAN)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_tujuan_opd_2() {
        $kdPemda = $this->input->post(COL_KD_PEMDA);
        $kdUrusan = $this->input->post(COL_KD_URUSAN);
        $kdBidang = $this->input->post(COL_KD_BIDANG);
        $kdUnit = $this->input->post(COL_KD_UNIT);
        $kdSub = $this->input->post(COL_KD_SUB);

        $this->db->join(TBL_SAKIP_MOPD_TUJUAN,
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORSASARAN. " AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_TUJUANOPD
            ,"inner");
        $this->db->where(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_PEMDA, $kdPemda);
        $this->db->where(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_URUSAN, $kdUrusan);
        $this->db->where(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_BIDANG, $kdBidang);
        $this->db->where(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_UNIT, $kdUnit);
        $this->db->where(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_SUB, $kdSub);
        $this->db->order_by(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_TUJUANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORTUJUANOPD, 'asc');
        $this->db->group_by(array(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_TUJUANOPD, TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORTUJUANOPD));
        $data['res'] = $this->db->get(TBL_SAKIP_MOPD_IKTUJUAN)->result_array();
        $this->load->view('ajax/browse_tujuan_opd', $data);
    }
    function get_iku_by_tujuan_opd() {
        $kdPemda = $this->input->post(COL_KD_PEMDA);
        $kdUrusan = $this->input->post(COL_KD_URUSAN);
        $kdBidang = $this->input->post(COL_KD_BIDANG);
        $kdUnit = $this->input->post(COL_KD_UNIT);
        $kdSub = $this->input->post(COL_KD_SUB);
        $kdTujuan = $this->input->post(COL_KD_TUJUANOPD);
        $kdIkTujuan = $this->input->post(COL_KD_INDIKATORTUJUANOPD);

        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_PEMDA,"inner");
        $this->db->join(TBL_SAKIP_MPMD_MISI,TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_MISI.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_MISI,"inner");
        $this->db->join(TBL_SAKIP_MPMD_TUJUAN,TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_TUJUAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_IKTUJUAN,TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORTUJUAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_SASARAN,TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_SASARAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_IKSASARAN,
            TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORSASARAN
            ,"inner");
        $this->db->where(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_PEMDA, $kdPemda);
        $this->db->where(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_URUSAN, $kdUrusan);
        $this->db->where(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_BIDANG, $kdBidang);
        $this->db->where(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_UNIT, $kdUnit);
        $this->db->where(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_SUB, $kdSub);
        $this->db->where(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_TUJUANOPD, $kdTujuan);
        $this->db->where(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORTUJUANOPD, $kdIkTujuan);
        $this->db->order_by(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_TUJUANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORTUJUANOPD, 'asc');
        $res = $this->db->get(TBL_SAKIP_MOPD_IKTUJUAN)->result_array();
        $html = "";
        foreach($res as $i) {
            $html .= '<table class="table table-bordered" style="outline-style: dashed;">';
            $html .= '<tr>';
            $html .= '<td style="width: 150px" class="control-label">Misi</td>';
            $html .= '<td style="width: 10px">:</td>';
            $html .= '<td><input type="hidden" name="'.COL_KD_MISI.'[]" value="'.$i[COL_KD_MISI].'" required ><span class="text-misi">'.$i[COL_KD_MISI].'.'.$i[COL_NM_MISI].'</span></td>';
            $html .= '</tr>';

            $html .= '<tr>';
            $html .= '<td style="width: 150px" class="control-label">Tujuan</td>';
            $html .= '<td style="width: 10px">:</td>';
            $html .= '<td><input type="hidden" name="'.COL_KD_TUJUAN.'[]" value="'.$i[COL_KD_TUJUAN].'" required ><span class="text-tujuan">'.$i[COL_KD_MISI].'.'.$i[COL_KD_TUJUAN].'.'.$i[COL_NM_TUJUAN].'</span></td>';
            $html .= '</tr>';

            $html .= '<tr>';
            $html .= '<td style="width: 150px" class="control-label">Indikator Tujuan</td>';
            $html .= '<td style="width: 10px">:</td>';
            $html .= '<td><input type="hidden" name="'.COL_KD_INDIKATORTUJUAN.'[]" value="'.$i[COL_KD_INDIKATORTUJUAN].'" required ><span class="text-iktujuan">'.$i[COL_KD_MISI].'.'.$i[COL_KD_TUJUAN].'.'.$i[COL_KD_INDIKATORTUJUAN].'.'.$i[COL_NM_INDIKATORTUJUAN].'</span></td>';
            $html .= '</tr>';

            $html .= '<tr>';
            $html .= '<td style="width: 150px" class="control-label">Sasaran</td>';
            $html .= '<td style="width: 10px">:</td>';
            $html .= '<td><input type="hidden" name="'.COL_KD_SASARAN.'[]" value="'.$i[COL_KD_SASARAN].'" required ><span class="text-sasaran">'.$i[COL_KD_MISI].'.'.$i[COL_KD_TUJUAN].'.'.$i[COL_KD_INDIKATORTUJUAN].'.'.$i[COL_KD_SASARAN].'.'.$i[COL_NM_SASARAN].'</span></td>';
            $html .= '</tr>';

            $html .= '<tr>';
            $html .= '<td style="width: 150px" class="control-label">Indikator Sasaran</td>';
            $html .= '<td style="width: 10px">:</td>';
            $html .= '<td><input type="hidden" name="'.COL_KD_INDIKATORSASARAN.'[]" value="'.$i[COL_KD_INDIKATORSASARAN].'" required ><span class="text-sasaran">'.$i[COL_KD_MISI].'.'.$i[COL_KD_TUJUAN].'.'.$i[COL_KD_INDIKATORTUJUAN].'.'.$i[COL_KD_SASARAN].'.'.$i[COL_KD_INDIKATORSASARAN].'.'.$i[COL_NM_INDIKATORSASARAN].'</span></td>';
            $html .= '</tr>';
            $html .= '</table>';
        }
        echo $html;
    }

    function browse_existing_sasaran_program_opd() {
        $kdPemda = $this->input->post(COL_KD_PEMDA);
        $kdUrusan = $this->input->post(COL_KD_URUSAN);
        $kdBidang = $this->input->post(COL_KD_BIDANG);
        $kdUnit = $this->input->post(COL_KD_UNIT);
        $kdSub = $this->input->post(COL_KD_SUB);
        $kdBid = $this->input->post(COL_KD_BID);
        $kdProgram = $this->input->post(COL_KD_PROGRAMOPD);
        $kdTahun = $this->input->post(COL_KD_TAHUN);

        $this->db->select('*, '.COL_KD_SASARANPROGRAMOPD.' AS ID, '.COL_NM_SASARANPROGRAMOPD.' AS Text');
        $this->db->where(COL_KD_PEMDA, $kdPemda);
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $this->db->where(COL_KD_BID, $kdBid);
        $this->db->where(COL_KD_PROGRAMOPD, $kdProgram);
        $this->db->where(COL_KD_TAHUN, $kdTahun);
        $this->db->group_by(array(COL_KD_SASARANPROGRAMOPD, COL_NM_SASARANPROGRAMOPD));
        $this->db->order_by(COL_KD_SASARANPROGRAMOPD, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_MBID_PROGRAM_SASARAN)->result_array();
        $this->load->view('ajax/browse', $data);
    }
    function get_indikator_by_sasaran_program_opd() {
        $kdPemda = $this->input->post(COL_KD_PEMDA);
        $kdUrusan = $this->input->post(COL_KD_URUSAN);
        $kdBidang = $this->input->post(COL_KD_BIDANG);
        $kdUnit = $this->input->post(COL_KD_UNIT);
        $kdSub = $this->input->post(COL_KD_SUB);
        $kdBid = $this->input->post(COL_KD_BID);
        $kdProgram = $this->input->post(COL_KD_PROGRAMOPD);
        $kdTahun = $this->input->post(COL_KD_TAHUN);
        $kdSasaranProgram = $this->input->post(COL_KD_SASARANPROGRAMOPD);

        $det = $this->db
            ->where(COL_KD_PEMDA, $kdPemda)
            ->where(COL_KD_TAHUN, $kdTahun)
            ->where(COL_KD_URUSAN, $kdUrusan)
            ->where(COL_KD_BIDANG, $kdBidang)
            ->where(COL_KD_UNIT, $kdUnit)
            ->where(COL_KD_SUB, $kdSub)
            ->where(COL_KD_BID, $kdBid)
            ->where(COL_KD_PROGRAMOPD, $kdProgram)
            ->where(COL_KD_SASARANPROGRAMOPD, $kdSasaranProgram)
            ->group_by(array(COL_KD_INDIKATORPROGRAMOPD, COL_NM_INDIKATORPROGRAMOPD))
            ->order_by(COL_KD_INDIKATORPROGRAMOPD, 'asc')
            ->get(TBL_SAKIP_MBID_PROGRAM_INDIKATOR)->result_array();
        //$sel = GetCombobox("SELECT * FROM ".TBL_SAKIP_MSATUAN." ORDER BY ".COL_NM_SATUAN, COL_KD_SATUAN, COL_NM_SATUAN, null, false, true);
        //return;
        $html = "";
        foreach($det as $m) {
            $html .= '<tr>';
            $html .= '<td><input type="text" name="text-det-no" class="form-control" value="'.$m[COL_KD_INDIKATORPROGRAMOPD].'" style="width: 50px" /></td>';
            $html .= '<td>';
            $html .= '<div class="row" style="margin-bottom: 10px">';
            $html .= '<div class="col-sm-12">';
            $html .= '<input name="text-det-desc" class="form-control" placeholder="Deskripsi" value="'.$m[COL_NM_INDIKATORPROGRAMOPD].'" />';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '<div class="row" style="margin-bottom: 10px">';
            $html .= '<div class="col-sm-6">';
            $html .= '<select name="select-det-satuan" class="form-control" style="width: 100%">';
            $sel = GetCombobox("SELECT * FROM ".TBL_SAKIP_MSATUAN." ORDER BY ".COL_NM_SATUAN, COL_KD_SATUAN, COL_NM_SATUAN, $m[COL_KD_SATUAN], false, true);
            $html .= $sel;
            $html .= '</select>';
            $html .= '</div>';
            $html .= '<div class="col-sm-6">';
            $html .= '<input type="text" name="text-det-target" placeholder="Target" class="form-control text-right money" value="'.$m[COL_TARGET].'" />';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '<div class="row">';
            //$html .= '<div class="col-sm-offset-6 col-sm-6 div-awal">';
            //$html .= '<input type="text" name="text-det-awal" placeholder="Kondisi Awal" class="form-control text-right money" value="'.$m[COL_AWAL].'" />';
            //$html .= '</div>';
            $html .= '</div>';
            $html .= '</td>';
            $html .= '<td><textarea name="text-det-formula" placeholder="Formulasi Perhitungan" class="form-control" rows="4">'.$m[COL_NM_FORMULA].'</textarea></td>';
            $html .= '<td><textarea name="text-det-sumber" placeholder="Sumber Data" class="form-control" rows="4">'.$m[COL_NM_SUMBERDATA].'</textarea></td>';
            $html .= '<td><button type="button" class="btn btn-default btn-flat btn-del-det"><i class="fa fa-minus"></i></button></td>';
        }
        echo $html;
    }

    function browse_existing_sasaran_kegiatan_opd() {
        $kdPemda = $this->input->post(COL_KD_PEMDA);
        $kdUrusan = $this->input->post(COL_KD_URUSAN);
        $kdBidang = $this->input->post(COL_KD_BIDANG);
        $kdUnit = $this->input->post(COL_KD_UNIT);
        $kdSub = $this->input->post(COL_KD_SUB);
        $kdBid = $this->input->post(COL_KD_BID);
        $kdSubbid = $this->input->post(COL_KD_SUBBID);
        $kdProgram = $this->input->post(COL_KD_PROGRAMOPD);
        $kdTahun = $this->input->post(COL_KD_TAHUN);
        $kdSasaranProgram = $this->input->post(COL_KD_SASARANPROGRAMOPD);
        $kdKegiatan = $this->input->post(COL_KD_KEGIATANOPD);

        $this->db->select('*, '.COL_KD_SASARANKEGIATANOPD.' AS ID, '.COL_NM_SASARANKEGIATANOPD.' AS Text');
        $this->db->where(COL_KD_PEMDA, $kdPemda);
        $this->db->where(COL_KD_URUSAN, $kdUrusan);
        $this->db->where(COL_KD_BIDANG, $kdBidang);
        $this->db->where(COL_KD_UNIT, $kdUnit);
        $this->db->where(COL_KD_SUB, $kdSub);
        $this->db->where(COL_KD_BID, $kdBid);
        $this->db->where(COL_KD_SUBBID, $kdSubbid);
        $this->db->where(COL_KD_PROGRAMOPD, $kdProgram);
        $this->db->where(COL_KD_TAHUN, $kdTahun);
        $this->db->where(COL_KD_SASARANPROGRAMOPD, $kdSasaranProgram);
        $this->db->where(COL_KD_KEGIATANOPD, $kdKegiatan);
        $this->db->group_by(array(COL_KD_SASARANKEGIATANOPD, COL_NM_SASARANKEGIATANOPD));
        $this->db->order_by(COL_KD_SASARANPROGRAMOPD, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN)->result_array();
        $this->load->view('ajax/browse', $data);
    }
    function get_indikator_by_sasaran_kegiatan_opd() {
        $kdPemda = $this->input->post(COL_KD_PEMDA);
        $kdUrusan = $this->input->post(COL_KD_URUSAN);
        $kdBidang = $this->input->post(COL_KD_BIDANG);
        $kdUnit = $this->input->post(COL_KD_UNIT);
        $kdSub = $this->input->post(COL_KD_SUB);
        $kdBid = $this->input->post(COL_KD_BID);
        $kdSubbid = $this->input->post(COL_KD_SUBBID);
        $kdProgram = $this->input->post(COL_KD_PROGRAMOPD);
        $kdTahun = $this->input->post(COL_KD_TAHUN);
        $kdSasaranProgram = $this->input->post(COL_KD_SASARANPROGRAMOPD);
        $kdKegiatan = $this->input->post(COL_KD_KEGIATANOPD);
        $kdSasaranKegiatan = $this->input->post(COL_KD_SASARANKEGIATANOPD);

        $det = $this->db
            ->where(COL_KD_PEMDA, $kdPemda)
            ->where(COL_KD_TAHUN, $kdTahun)
            ->where(COL_KD_URUSAN, $kdUrusan)
            ->where(COL_KD_BIDANG, $kdBidang)
            ->where(COL_KD_UNIT, $kdUnit)
            ->where(COL_KD_SUB, $kdSub)
            ->where(COL_KD_BID, $kdBid)
            ->where(COL_KD_SUBBID, $kdSubbid)
            ->where(COL_KD_PROGRAMOPD, $kdProgram)
            ->where(COL_KD_SASARANPROGRAMOPD, $kdSasaranProgram)
            ->where(COL_KD_KEGIATANOPD, $kdKegiatan)
            ->where(COL_KD_SASARANKEGIATANOPD, $kdSasaranKegiatan)
            ->group_by(array(COL_KD_INDIKATORKEGIATANOPD, COL_NM_INDIKATORKEGIATANOPD))
            ->order_by(COL_KD_INDIKATORKEGIATANOPD, 'asc')
            ->get(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR)->result_array();
        //$sel = GetCombobox("SELECT * FROM ".TBL_SAKIP_MSATUAN." ORDER BY ".COL_NM_SATUAN, COL_KD_SATUAN, COL_NM_SATUAN, null, false, true);
        //return;
        $html = "";
        foreach($det as $m) {
            $html .= '<tr>';
            $html .= '<td><input type="text" name="text-det-no" class="form-control" value="'.$m[COL_KD_INDIKATORKEGIATANOPD].'" style="width: 50px" /></td>';
            $html .= '<td>';
            $html .= '<div class="row" style="margin-bottom: 10px">';
            $html .= '<div class="col-sm-12">';
            $html .= '<input name="text-det-desc" class="form-control" placeholder="Deskripsi" value="'.$m[COL_NM_INDIKATORKEGIATANOPD].'" />';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '<div class="row" style="margin-bottom: 10px">';
            $html .= '<div class="col-sm-6">';
            $html .= '<select name="select-det-satuan" class="form-control" style="width: 100%">';
            $sel = GetCombobox("SELECT * FROM ".TBL_SAKIP_MSATUAN." ORDER BY ".COL_NM_SATUAN, COL_KD_SATUAN, COL_NM_SATUAN, $m[COL_KD_SATUAN], false, true);
            $html .= $sel;
            $html .= '</select>';
            $html .= '</div>';
            $html .= '<div class="col-sm-6">';
            $html .= '<input type="text" name="text-det-target" placeholder="Target" class="form-control text-right money" value="'.$m[COL_TARGET].'" />';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '<div class="row">';
            //$html .= '<div class="col-sm-offset-6 col-sm-6 div-awal">';
            //$html .= '<input type="text" name="text-det-awal" placeholder="Kondisi Awal" class="form-control text-right money" value="'.$m[COL_AWAL].'" />';
            //$html .= '</div>';
            $html .= '</div>';
            $html .= '</td>';
            $html .= '<td><textarea name="text-det-formula" placeholder="Formulasi Perhitungan" class="form-control" rows="4">'.$m[COL_NM_FORMULA].'</textarea></td>';
            $html .= '<td><textarea name="text-det-sumber" placeholder="Sumber Data" class="form-control" rows="4">'.$m[COL_NM_SUMBERDATA].'</textarea></td>';
            $html .= '<td><button type="button" class="btn btn-default btn-flat btn-del-det"><i class="fa fa-minus"></i></button></td>';
        }
        echo $html;
    }

    function get_opt_dpa_program() {
      $id = $this->input->post('key');
      $key = explode('.', $id);
      $cond = array(
        COL_KD_PEMDA=>$key[0],
        COL_KD_TAHUN=>$key[1],
        COL_KD_URUSAN=>$key[2],
        COL_KD_BIDANG=>$key[3],
        COL_KD_UNIT=>$key[4],
        COL_KD_SUB=>$key[5],
        COL_KD_MISI=>$key[6],
        COL_KD_TUJUAN=>$key[7],
        COL_KD_INDIKATORTUJUAN=>$key[8],
        COL_KD_SASARAN=>$key[9],
        COL_KD_INDIKATORSASARAN=>$key[10],
        COL_KD_TUJUANOPD=>$key[11],
        COL_KD_INDIKATORTUJUANOPD=>$key[12],
        COL_KD_SASARANOPD=>$key[13],
        COL_KD_INDIKATORSASARANOPD=>$key[14],
        COL_KD_BID=>$key[15]
      );

      $res = $this->db->where($cond)->get(TBL_SAKIP_MBID_PROGRAM)->result_array();
      $opt = '';
      foreach ($res as $r) {
        $opt .= '<option value="'.$r[COL_KD_PROGRAMOPD].'">'.$r[COL_KD_PROGRAMOPD].': '.$r[COL_NM_PROGRAMOPD].'</option>';
      }

      echo $opt;
    }

    function get_opt_pdpa_program() {
      $id = $this->input->post('key');
      $key = explode('.', $id);
      $cond = array(
        COL_KD_PEMDA=>$key[0],
        COL_KD_TAHUN=>$key[1],
        COL_KD_URUSAN=>$key[2],
        COL_KD_BIDANG=>$key[3],
        COL_KD_UNIT=>$key[4],
        COL_KD_SUB=>$key[5],
        COL_KD_MISI=>$key[6],
        COL_KD_TUJUAN=>$key[7],
        COL_KD_INDIKATORTUJUAN=>$key[8],
        COL_KD_SASARAN=>$key[9],
        COL_KD_INDIKATORSASARAN=>$key[10],
        COL_KD_TUJUANOPD=>$key[11],
        COL_KD_INDIKATORTUJUANOPD=>$key[12],
        COL_KD_SASARANOPD=>$key[13],
        COL_KD_INDIKATORSASARANOPD=>$key[14],
        COL_KD_BID=>$key[15]
      );

      $res = $this->db->where($cond)->get(TBL_SAKIP_DPA_PROGRAM)->result_array();
      $opt = '';
      foreach ($res as $r) {
        $opt .= '<option value="'.$r[COL_KD_PROGRAMOPD].'">'.$r[COL_KD_PROGRAMOPD].': '.$r[COL_NM_PROGRAMOPD].'</option>';
      }

      echo $opt;
    }

    function get_opt_dpa_kegiatan() {
      $id = $this->input->post('key');
      $key = explode('.', $id);
      $cond = array(
        COL_KD_PEMDA=>$key[0],
        COL_KD_TAHUN=>$key[1],
        COL_KD_URUSAN=>$key[2],
        COL_KD_BIDANG=>$key[3],
        COL_KD_UNIT=>$key[4],
        COL_KD_SUB=>$key[5],
        COL_KD_MISI=>$key[6],
        COL_KD_TUJUAN=>$key[7],
        COL_KD_INDIKATORTUJUAN=>$key[8],
        COL_KD_SASARAN=>$key[9],
        COL_KD_INDIKATORSASARAN=>$key[10],
        COL_KD_TUJUANOPD=>$key[11],
        COL_KD_INDIKATORTUJUANOPD=>$key[12],
        COL_KD_SASARANOPD=>$key[13],
        COL_KD_INDIKATORSASARANOPD=>$key[14],
        COL_KD_BID=>$key[15],
        COL_KD_PROGRAMOPD=>$key[16],
        COL_KD_SASARANPROGRAMOPD=>$key[17],
        COL_KD_SUBBID=>$key[18]
      );

      $res = $this->db->where($cond)->get(TBL_SAKIP_MSUBBID_KEGIATAN)->result_array();
      $opt = '';
      foreach ($res as $r) {
        $opt .= '<option value="'.$r[COL_KD_KEGIATANOPD].'">'.$r[COL_KD_KEGIATANOPD].': '.$r[COL_NM_KEGIATANOPD].'</option>';
      }

      echo $opt;
    }

    function get_opt_pdpa_kegiatan() {
      $id = $this->input->post('key');
      $key = explode('.', $id);
      $cond = array(
        COL_KD_PEMDA=>$key[0],
        COL_KD_TAHUN=>$key[1],
        COL_KD_URUSAN=>$key[2],
        COL_KD_BIDANG=>$key[3],
        COL_KD_UNIT=>$key[4],
        COL_KD_SUB=>$key[5],
        COL_KD_MISI=>$key[6],
        COL_KD_TUJUAN=>$key[7],
        COL_KD_INDIKATORTUJUAN=>$key[8],
        COL_KD_SASARAN=>$key[9],
        COL_KD_INDIKATORSASARAN=>$key[10],
        COL_KD_TUJUANOPD=>$key[11],
        COL_KD_INDIKATORTUJUANOPD=>$key[12],
        COL_KD_SASARANOPD=>$key[13],
        COL_KD_INDIKATORSASARANOPD=>$key[14],
        COL_KD_BID=>$key[15],
        COL_KD_PROGRAMOPD=>$key[16],
        COL_KD_SASARANPROGRAMOPD=>$key[17],
        COL_KD_SUBBID=>$key[18]
      );

      $res = $this->db->where($cond)->get(TBL_SAKIP_DPA_KEGIATAN)->result_array();
      $opt = '';
      foreach ($res as $r) {
        $opt .= '<option value="'.$r[COL_KD_KEGIATANOPD].'">'.$r[COL_KD_KEGIATANOPD].': '.$r[COL_NM_KEGIATANOPD].'</option>';
      }

      echo $opt;
    }

    function get_opt_prenja_kegiatan() {
      $id = $this->input->post('key');
      $key = explode('.', $id);
      $cond = array(
        COL_KD_PEMDA=>$key[0],
        COL_KD_TAHUN=>$key[1],
        COL_KD_URUSAN=>$key[2],
        COL_KD_BIDANG=>$key[3],
        COL_KD_UNIT=>$key[4],
        COL_KD_SUB=>$key[5],
        COL_KD_MISI=>$key[6],
        COL_KD_TUJUAN=>$key[7],
        COL_KD_INDIKATORTUJUAN=>$key[8],
        COL_KD_SASARAN=>$key[9],
        COL_KD_INDIKATORSASARAN=>$key[10],
        COL_KD_TUJUANOPD=>$key[11],
        COL_KD_INDIKATORTUJUANOPD=>$key[12],
        COL_KD_SASARANOPD=>$key[13],
        COL_KD_INDIKATORSASARANOPD=>$key[14],
        COL_KD_BID=>$key[15],
        COL_KD_PROGRAMOPD=>$key[16],
        COL_KD_SASARANPROGRAMOPD=>$key[17],
        COL_KD_SUBBID=>$key[18]
      );

      $res = $this->db->where($cond)->get(TBL_SAKIP_PRENJA_KEGIATAN)->result_array();
      $opt = '';
      foreach ($res as $r) {
        $opt .= '<option value="'.$r[COL_KD_KEGIATANOPD].'">'.$r[COL_KD_KEGIATANOPD].': '.$r[COL_NM_KEGIATANOPD].'</option>';
      }

      echo $opt;
    }

    function get_lakip_history() {
      $kdUrusan = $this->input->post(COL_KD_URUSAN);
      $kdBidang = $this->input->post(COL_KD_BIDANG);
      $kdUnit = $this->input->post(COL_KD_UNIT);
      $kdSub = $this->input->post(COL_KD_SUB);
      $kdTahun = $this->input->post(COL_KD_TAHUN);

      $rlakip = $this->db
      ->where(array(
        COL_KD_URUSAN=>$kdUrusan,
        COL_KD_BIDANG=>$kdBidang,
        COL_KD_UNIT=>$kdUnit,
        COL_KD_SUB=>$kdSub,
        COL_KD_TAHUN=>$kdTahun
      ))
      ->order_by(COL_CREATE_DATE, 'desc')
      ->get(TBL_SAKIP_MOPD_LAKIP)
      ->row_array();

      echo json_encode($rlakip);
    }
}
