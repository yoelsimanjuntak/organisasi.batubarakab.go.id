<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 06/10/2018
 * Time: 16:40
 */
class Msubbid extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin() || (GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEBAPPEDA && GetLoggedUser()[COL_ROLEID] != ROLEKADIS && GetLoggedUser()[COL_ROLEID] != ROLEKABID && GetLoggedUser()[COL_ROLEID] != ROLEKASUBBID)) {
            redirect('sakip/user/dashboard');
        }
    }

    function kegiatan() {
        $ruser = GetLoggedUser();
        $data['title'] = 'Kegiatan Sub Bidang OPD';
        $this->db->select('*,'.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_UNIQ.' as ID,'.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_TAHUN.' as Kd_Tahun,'.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_ISEPLAN.' as IsEplan');
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA,"inner");
        $this->db->join(TBL_SAKIP_MPMD_MISI,TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_MISI.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_MISI,"inner");
        $this->db->join(TBL_SAKIP_MPMD_TUJUAN,TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_IKTUJUAN,TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_SASARAN,TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_IKSASARAN,TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARAN,"inner");
        $this->db->join(TBL_SAKIP_MOPD_TUJUAN,
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARAN. " AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUANOPD
            ,"inner");
        $this->db->join(TBL_SAKIP_MOPD_IKTUJUAN,
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUANOPD
            ,"inner");
        $this->db->join(TBL_SAKIP_MOPD_SASARAN,
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARANOPD
            ,"inner");
        $this->db->join(TBL_SAKIP_MOPD_IKSASARAN,
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARANOPD." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARANOPD
            ,"inner");
        $this->db->join(TBL_SAKIP_MBID,
            TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUB." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BID
            ,"inner");
        $this->db->join(TBL_SAKIP_MSUBBID,
            TBL_SAKIP_MSUBBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MSUBBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MSUBBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MSUBBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUB." AND ".
            TBL_SAKIP_MSUBBID.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BID." AND ".
            TBL_SAKIP_MSUBBID.'.'.COL_KD_SUBBID." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUBBID
            ,"inner");
        $this->db->join(TBL_SAKIP_MBID_PROGRAM,
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARANOPD." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BID." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PROGRAMOPD." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TAHUN
            ,"inner");
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_TO." >=", date("Y"));
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUB, $strOPD[3]);
            if($ruser[COL_ROLEID] == ROLEKABID) $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BID, $strOPD[4]);
            if($ruser[COL_ROLEID] == ROLEKASUBBID) {
                $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BID, $strOPD[4]);
                $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUBBID, $strOPD[5]);
            }
        }
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TAHUN, 'desc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_URUSAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BIDANG, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_UNIT, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUB, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BID, 'asc');
        $this->db->order_by(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM, 'desc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_MISI, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PROGRAMOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_KEGIATANOPD, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_MSUBBID_KEGIATAN)->result_array();
        $this->load->view('msubbid/kegiatan', $data);
    }

    function kegiatan_add() {
        $data['title'] = "Kegiatan Sub Bidang OPD";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('msubbid/kegiatan');
            $isbulk = $this->input->post("IsBulk");

            $this->db->trans_begin();
            try {
                if($isbulk) {
                    $rpemda = $this->db->where(COL_KD_PEMDA, $this->input->post(COL_KD_PEMDA))->get(TBL_SAKIP_MPEMDA)->row_array();
                    if (!$rpemda) {
                        $resp['error'] = "Periode salah!";
                        $resp['success'] = 0;
                        echo json_encode($resp);
                        return;
                    }

                    $data = [];
                    $dataTarget = $this->input->post(COL_TARGET);
                    $dataPagu = $this->input->post(COL_TOTAL);
                    $dataTarget_N1 = $this->input->post(COL_TARGET_N1);
                    $dataPagu_N1 = $this->input->post(COL_TOTAL_N1);

                    for ($i = $rpemda[COL_KD_TAHUN_FROM] + 1; $i <= $rpemda[COL_KD_TAHUN_TO]; $i++) {
                        $arr_check = array(
                            COL_KD_TAHUN => $i, //$this->input->post(COL_KD_TAHUN),
                            COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                            COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                            COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                            COL_KD_SUB => $this->input->post(COL_KD_SUB),
                            COL_KD_BID => $this->input->post(COL_KD_BID),
                            COL_KD_SUBBID => $this->input->post(COL_KD_SUBBID),

                            COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                            COL_KD_MISI => $this->input->post(COL_KD_MISI),
                            COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                            COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                            COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                            COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                            COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                            COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                            COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                            COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                            COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD),
                            COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                            COL_KD_KEGIATANOPD => $this->input->post(COL_KD_KEGIATANOPD)
                        );
                        $rkeg_existing = $this->db->where($arr_check)->get(TBL_SAKIP_MSUBBID_KEGIATAN)->row_array();
                        if ($rkeg_existing) {
                            continue;
                        }
                        $data[] = array(
                            COL_KD_TAHUN => $i, //$this->input->post(COL_KD_TAHUN),
                            COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                            COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                            COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                            COL_KD_SUB => $this->input->post(COL_KD_SUB),
                            COL_KD_BID => $this->input->post(COL_KD_BID),
                            COL_KD_SUBBID => $this->input->post(COL_KD_SUBBID),

                            COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                            COL_KD_MISI => $this->input->post(COL_KD_MISI),
                            COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                            COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                            COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                            COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                            COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                            COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                            COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                            COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                            COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD),
                            COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                            COL_KD_KEGIATANOPD => $this->input->post(COL_KD_KEGIATANOPD),
                            //COL_NM_SASARANKEGIATANOPD => $this->input->post(COL_NM_SASARANKEGIATANOPD),
                            //COL_NM_INDIKATORKEGIATANOPD => $this->input->post(COL_NM_INDIKATORKEGIATANOPD),
                            COL_TOTAL => toNum($dataPagu[$i]), //$this->input->post(COL_TOTAL),
                            //COL_TOTAL_N1 => $dataPagu_N1[$i],
                            COL_ISEPLAN => !empty($this->input->post(COL_ISEPLAN))?1:0,
                            COL_NM_KEGIATANOPD => $this->input->post(COL_NM_KEGIATANOPD),
                            //COL_KD_SATUAN => $this->input->post(COL_KD_SATUAN),
                            //COL_AWAL => $this->input->post(COL_AWAL),
                            //COL_TARGET => $dataTarget[$i],//$this->input->post(COL_TARGET),
                            //COL_TARGET_N1 => $dataTarget_N1[$i],
                            //COL_AKHIR => $this->input->post(COL_AKHIR),
                            COL_REMARKS => $this->input->post(COL_REMARKS),
                            COL_KD_SUMBERDANA => $this->input->post(COL_KD_SUMBERDANA),

                            COL_CREATE_BY => $ruser[COL_USERNAME],
                            COL_CREATE_DATE => date('Y-m-d H:i:s')
                        );
                    }

                    if (!$this->db->insert_batch(TBL_SAKIP_MSUBBID_KEGIATAN, $data)) {
                        throw new Exception("Database error: ".$this->db->error());
                    }
                }
                else {
                    $data = array(
                        COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                        COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                        COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                        COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                        COL_KD_SUB => $this->input->post(COL_KD_SUB),
                        COL_KD_BID => $this->input->post(COL_KD_BID),
                        COL_KD_SUBBID => $this->input->post(COL_KD_SUBBID),

                        COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                        COL_KD_MISI => $this->input->post(COL_KD_MISI),
                        COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                        COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                        COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                        COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                        COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                        COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                        COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                        COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                        COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD),
                        COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                        COL_KD_KEGIATANOPD => $this->input->post(COL_KD_KEGIATANOPD),
                        COL_NM_KEGIATANOPD => $this->input->post(COL_NM_KEGIATANOPD),
                        //COL_NM_SASARANKEGIATANOPD => $this->input->post(COL_NM_SASARANKEGIATANOPD),
                        //COL_NM_INDIKATORKEGIATANOPD => $this->input->post(COL_NM_INDIKATORKEGIATANOPD),
                        COL_TOTAL => toNum($this->input->post(COL_TOTAL)),
                        //COL_TOTAL_N1 => $this->input->post(COL_TOTAL_N1),
                        COL_ISEPLAN => !empty($this->input->post(COL_ISEPLAN))?1:0,
                        //COL_NM_KEGIATANOPD => $this->input->post(COL_NM_KEGIATANOPD),
                        //COL_KD_SATUAN => $this->input->post(COL_KD_SATUAN),
                        //COL_AWAL => $this->input->post(COL_AWAL),
                        //COL_TARGET => $this->input->post(COL_TARGET),
                        //COL_TARGET_N1 => $this->input->post(COL_TARGET_N1),
                        //COL_AKHIR => $this->input->post(COL_AKHIR),
                        COL_REMARKS => $this->input->post(COL_REMARKS),
                        COL_KD_SUMBERDANA => $this->input->post(COL_KD_SUMBERDANA),

                        COL_CREATE_BY => $ruser[COL_USERNAME],
                        COL_CREATE_DATE => date('Y-m-d H:i:s')
                    );
                    if(!$this->db->insert(TBL_SAKIP_MSUBBID_KEGIATAN, $data)){
                        throw new Exception("Database error: ".$this->db->error());
                    }
                }

                $this->db->trans_commit();
                echo json_encode($resp);
            } catch (Exception $e) {
                $this->db->trans_rollback();
                $resp['error'] = $e->getMessage();
                $resp['success'] = 0;
                echo json_encode($resp);
                return;
            }

        }else{
            $this->db->where(COL_KD_TAHUN_FROM." <=", date("Y"));
            $this->db->where(COL_KD_TAHUN_TO." >=", date("Y"));
            $this->db->order_by(COL_KD_TAHUN_FROM, 'desc');
            $rpemda = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();

            if(!empty($rpemda)) {
                $data['data'] = array(
                    COL_KD_PEMDA => $rpemda[COL_KD_PEMDA],
                    "DefPeriod" => $rpemda[COL_KD_TAHUN_FROM]." s.d ".$rpemda[COL_KD_TAHUN_TO]." : ".$rpemda[COL_NM_PEJABAT]
                );
            }

            $this->load->view('msubbid/kegiatan_form',$data);
        }
    }

    function kegiatan_edit($id) {
        $ruser = GetLoggedUser();
        $this->db
            ->select(TBL_SAKIP_MSUBBID_KEGIATAN.'.*,'.
                TBL_SAKIP_MPMD_MISI.'.'.COL_NM_MISI.','.
                TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.','.
                TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.','.
                TBL_SAKIP_MPEMDA.'.'.COL_NM_PEJABAT.', '.
                TBL_SAKIP_MPMD_TUJUAN.'.'.COL_NM_TUJUAN.', '.
                TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_NM_INDIKATORTUJUAN.', '.
                TBL_SAKIP_MPMD_SASARAN.'.'.COL_NM_SASARAN.', '.
                TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_NM_INDIKATORSASARAN.', '.
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_NM_TUJUANOPD.', '.
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_NM_INDIKATORTUJUANOPD.', '.
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_NM_SASARANOPD.', '.
                TBL_SAKIP_MBID.'.'.COL_NM_BID.','.
                TBL_SAKIP_MSUBBID.'.'.COL_NM_SUBBID.','.
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_NM_INDIKATORSASARANOPD.','.
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_NM_PROGRAMOPD.','.
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_NM_SASARANPROGRAMOPD
            )
            //->select(TBL_SAKIP_MSUBBID_KEGIATAN.'.*,'.TBL_SAKIP_MBID.'.'.COL_NM_BID.','.TBL_SAKIP_MSUBBID.'.'.COL_NM_SUBBID.','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_NM_PROGRAMOPD.','.TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_NM_SASARANPROGRAMOPD)
            ->join(TBL_SAKIP_MBID,
                TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BID
                ,"inner")
            ->join(TBL_SAKIP_MSUBBID,
                TBL_SAKIP_MSUBBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BID." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_SUBBID." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUBBID
                ,"inner")
            ->join(TBL_SAKIP_MBID_PROGRAM,
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TAHUN." AND ".

                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARANOPD." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BID." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PROGRAMOPD
                ,"inner")
            ->join(TBL_SAKIP_MBID_PROGRAM_SASARAN,
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TAHUN." AND ".

                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARANOPD." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BID." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PROGRAMOPD." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_SASARANPROGRAMOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARANPROGRAMOPD
                ,"inner")
            ->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_PEMDA,"inner")
            ->join(TBL_SAKIP_MPMD_MISI,TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_MISI.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI,"inner")
            ->join(TBL_SAKIP_MPMD_TUJUAN,TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN,"inner")
            ->join(TBL_SAKIP_MPMD_IKTUJUAN,TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN,"inner")
            ->join(TBL_SAKIP_MPMD_SASARAN,TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN,"inner")
            ->join(TBL_SAKIP_MPMD_IKSASARAN,TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN,"inner")
            ->join(TBL_SAKIP_MOPD_TUJUAN,
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD
                ,"inner")
            ->join(TBL_SAKIP_MOPD_IKTUJUAN,
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD
                ,"inner")
            ->join(TBL_SAKIP_MOPD_SASARAN,
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARANOPD
                ,"inner")
            ->join(TBL_SAKIP_MOPD_IKSASARAN,
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARANOPD
                ,"inner")
            ->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_UNIQ, $id);

        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUB, $strOPD[3]);
            if($ruser[COL_ROLEID] == ROLEKABID) $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BID, $strOPD[4]);
            if($ruser[COL_ROLEID] == ROLEKASUBBID) {
                $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BID, $strOPD[4]);
                $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUBBID, $strOPD[5]);
            }
        }

        $rdata = $data['data'] = $this->db->get(TBL_SAKIP_MSUBBID_KEGIATAN)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = "Kegiatan Sub Bidang OPD";;
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('msubbid/kegiatan');
            $data = array(
                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_BID => $this->input->post(COL_KD_BID),
                COL_KD_SUBBID => $this->input->post(COL_KD_SUBBID),

                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                COL_KD_MISI => $this->input->post(COL_KD_MISI),
                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD),
                COL_KD_KEGIATANOPD => $this->input->post(COL_KD_KEGIATANOPD),
                //COL_NM_SASARANKEGIATANOPD => $this->input->post(COL_NM_SASARANKEGIATANOPD),
                //COL_NM_INDIKATORKEGIATANOPD => $this->input->post(COL_NM_INDIKATORKEGIATANOPD),
                COL_ISEPLAN => !empty($this->input->post(COL_ISEPLAN))?1:0,
                COL_NM_KEGIATANOPD => $this->input->post(COL_NM_KEGIATANOPD),
                //COL_KD_SATUAN => $this->input->post(COL_KD_SATUAN),
                COL_TOTAL => toNum($this->input->post(COL_TOTAL)),
                //COL_TOTAL_N1 => $this->input->post(COL_TOTAL_N1),
                //COL_AWAL => $this->input->post(COL_AWAL),
                //COL_TARGET => $this->input->post(COL_TARGET),
                //COL_TARGET_N1 => $this->input->post(COL_TARGET_N1),
                //COL_AKHIR => $this->input->post(COL_AKHIR),
                COL_REMARKS => $this->input->post(COL_REMARKS),
                COL_KD_SUMBERDANA => $this->input->post(COL_KD_SUMBERDANA),

                COL_EDIT_BY => $ruser[COL_USERNAME],
                COL_EDIT_DATE => date('Y-m-d H:i:s')
            );
            if(!$this->db->where(COL_UNIQ, $id)->update(TBL_SAKIP_MSUBBID_KEGIATAN, $data)){
                $resp['error'] = $e->getMessage();
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('msubbid/kegiatan_form',$data);
        }
    }

    function kegiatan_del(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_SAKIP_MSUBBID_KEGIATAN, array(COL_UNIQ => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function sasaran() {
        $ruser = GetLoggedUser();
        $data['title'] = 'Sasaran Eselon IV';
        $this->db->select('*,'.TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_UNIQ.' as ID,'.TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_TAHUN.' as Kd_Tahun');
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_PEMDA,"inner");
        $this->db->join(TBL_SAKIP_MPMD_MISI,TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_MISI.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_MISI,"inner");
        $this->db->join(TBL_SAKIP_MPMD_TUJUAN,TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_TUJUAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_IKTUJUAN,TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORTUJUAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_SASARAN,TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SASARAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_IKSASARAN,TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SASARAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORSASARAN,"inner");
        $this->db->join(TBL_SAKIP_MOPD_TUJUAN,
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORSASARAN. " AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_TUJUANOPD
            ,"inner");
        $this->db->join(TBL_SAKIP_MOPD_IKTUJUAN,
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORTUJUANOPD
            ,"inner");
        $this->db->join(TBL_SAKIP_MOPD_SASARAN,
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SASARANOPD
            ,"inner");
        $this->db->join(TBL_SAKIP_MOPD_IKSASARAN,
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SASARANOPD." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORSASARANOPD
            ,"inner");
            $this->db->join(TBL_SAKIP_MBID_SASARAN,
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BID." AND ".

                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SASARANOPD." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_TAHUN." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_SASARANPROGRAMOPD." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SASARANPROGRAMOPD
                ,"inner");
        $this->db->join(TBL_SAKIP_MBID_INDIKATOR,
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SUB." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BID." AND ".

            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SASARANOPD." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_TAHUN." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_SASARANPROGRAMOPD." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SASARANPROGRAMOPD." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_INDIKATORPROGRAMOPD." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORPROGRAMOPD
            ,"inner");
        $this->db->join(TBL_SAKIP_MBID,
            TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SUB." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BID
            ,"inner");
        $this->db->join(TBL_SAKIP_MSUBBID,
            TBL_SAKIP_MSUBBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MSUBBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MSUBBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MSUBBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SUB." AND ".
            TBL_SAKIP_MSUBBID.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BID." AND ".
            TBL_SAKIP_MSUBBID.'.'.COL_KD_SUBBID." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SUBBID
            ,"inner");
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SUB, $strOPD[3]);
            if($ruser[COL_ROLEID] == ROLEKABID) $this->db->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BID, $strOPD[4]);
            if($ruser[COL_ROLEID] == ROLEKASUBBID) {
                $this->db->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BID, $strOPD[4]);
                $this->db->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SUBBID, $strOPD[5]);
            }
        }
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_TO." >=", date("Y"));
        $this->db->order_by(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_TAHUN, 'desc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_URUSAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BIDANG, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_UNIT, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SUB, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BID, 'asc');
        $this->db->order_by(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM, 'desc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_MISI, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_TUJUAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORTUJUAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SASARAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORSASARAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_TUJUANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORTUJUANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SASARANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORSASARANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SASARANPROGRAMOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORPROGRAMOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SASARANSUBBIDANG, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_MSUBBID_SASARAN)->result_array();
        $this->load->view('msubbid/sasaran', $data);
    }

    function sasaran_add() {
        $data['title'] = "Sasaran Sub Bidang OPD";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('msubbid/sasaran');
            $data = array(
                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_BID => $this->input->post(COL_KD_BID),
                COL_KD_SUBBID => $this->input->post(COL_KD_SUBBID),

                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                COL_KD_MISI => $this->input->post(COL_KD_MISI),
                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                COL_KD_INDIKATORPROGRAMOPD => $this->input->post(COL_KD_INDIKATORPROGRAMOPD),
                COL_KD_SASARANSUBBIDANG => $this->input->post(COL_KD_SASARANSUBBIDANG),
                COL_NM_SASARANSUBBIDANG => $this->input->post(COL_NM_SASARANSUBBIDANG),
            );

            $this->db->trans_begin();
            try {
                if(!$this->db->insert(TBL_SAKIP_MSUBBID_SASARAN, $data)){
                    throw new Exception("Database error: ".$this->db->error());
                }
                $detNo = $this->input->post("NoDet");
                $detDesc = $this->input->post("KetDet");
                $det = [];
                for($i = 0; $i<count($detNo); $i++) {
                    $det[] = array(
                        COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                        COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                        COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                        COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                        COL_KD_SUB => $this->input->post(COL_KD_SUB),
                        COL_KD_BID => $this->input->post(COL_KD_BID),
                        COL_KD_SUBBID => $this->input->post(COL_KD_SUBBID),

                        COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                        COL_KD_MISI => $this->input->post(COL_KD_MISI),
                        COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                        COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                        COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                        COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                        COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                        COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                        COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                        COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                        COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                        COL_KD_INDIKATORPROGRAMOPD => $this->input->post(COL_KD_INDIKATORPROGRAMOPD),
                        COL_KD_SASARANSUBBIDANG => $this->input->post(COL_KD_SASARANSUBBIDANG),
                        COL_KD_INDIKATORSUBBIDANG => $detNo[$i],
                        COL_NM_INDIKATORSUBBIDANG => $detDesc[$i]
                    );
                }
                if(count($det) > 0) {
                    $res = $this->db->insert_batch(TBL_SAKIP_MSUBBID_INDIKATOR, $det);
                    if(!$res) {
                        throw new Exception("Database error: ".$this->db->error());
                    }
                }

                $this->db->trans_commit();
                echo json_encode($resp);
            } catch (Exception $e) {
                $this->db->trans_rollback();
                $resp['error'] = $e->getMessage();
                $resp['success'] = 0;
                echo json_encode($resp);
                return;
            }
        }else{
            $this->load->view('msubbid/sasaran_form',$data);
        }
    }

    function sasaran_edit($id) {
        $ruser = GetLoggedUser();
        $this->db
            ->select(TBL_SAKIP_MSUBBID_SASARAN.'.*,'.TBL_SAKIP_MBID.'.'.COL_NM_BID.','.TBL_SAKIP_MSUBBID.'.'.COL_NM_SUBBID.','.TBL_SAKIP_MBID_INDIKATOR.'.'.COL_NM_INDIKATORPROGRAMOPD/*.','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_NM_PROGRAMOPD.','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_ISEPLAN*/)
            ->join(TBL_SAKIP_MBID,
                TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BID
                ,"inner")
            ->join(TBL_SAKIP_MSUBBID,
                TBL_SAKIP_MSUBBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BID." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_SUBBID." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SUBBID
                ,"inner")
            ->join(TBL_SAKIP_MBID_INDIKATOR,
                TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BID." AND ".

                TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SASARANOPD." AND ".
                TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
                TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BID." AND ".
                TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_SASARANPROGRAMOPD." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SASARANPROGRAMOPD." AND ".
                TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_INDIKATORPROGRAMOPD." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORPROGRAMOPD
                ,"inner")
            ->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_UNIQ, $id);
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SUB, $strOPD[3]);
            if($ruser[COL_ROLEID] == ROLEKABID) $this->db->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BID, $strOPD[4]);
            if($ruser[COL_ROLEID] == ROLEKASUBBID) {
                $this->db->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BID, $strOPD[4]);
                $this->db->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SUBBID, $strOPD[5]);
            }
        }
        $rdata = $data['data'] = $this->db->get(TBL_SAKIP_MSUBBID_SASARAN)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = "Sasaran Sub Bidang OPD";;
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('msubbid/sasaran');
            $data = array(
                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_BID => $this->input->post(COL_KD_BID),
                COL_KD_SUBBID => $this->input->post(COL_KD_SUBBID),

                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                COL_KD_MISI => $this->input->post(COL_KD_MISI),
                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                COL_KD_INDIKATORPROGRAMOPD => $this->input->post(COL_KD_INDIKATORPROGRAMOPD),
                COL_KD_SASARANSUBBIDANG => $this->input->post(COL_KD_SASARANSUBBIDANG),
                COL_NM_SASARANSUBBIDANG => $this->input->post(COL_NM_SASARANSUBBIDANG),
            );

            $this->db->trans_begin();
            try {
                if(!$this->db->where(COL_UNIQ, $id)->update(TBL_SAKIP_MSUBBID_SASARAN, $data)){
                    throw new Exception("Database error: ".$this->db->error());
                }

                $detNo = $this->input->post("NoDet");
                $detKet = $this->input->post("KetDet");
                $arrDet = [];

                /* update / delete */
                $det = $this->db
                    //->select(COL_KD_PEMDA.",".COL_KD_MISI.",".COL_KD_TUJUAN.",".COL_KD_INDIKATORTUJUAN.",".COL_KD_SASARAN.",".COL_KD_INDIKATORSASARAN)
                    ->where(COL_KD_URUSAN, $rdata[COL_KD_URUSAN])
                    ->where(COL_KD_BIDANG, $rdata[COL_KD_BIDANG])
                    ->where(COL_KD_UNIT, $rdata[COL_KD_UNIT])
                    ->where(COL_KD_SUB, $rdata[COL_KD_SUB])
                    ->where(COL_KD_BID, $rdata[COL_KD_BID])
                    ->where(COL_KD_SUBBID, $rdata[COL_KD_SUBBID])

                    ->where(COL_KD_PEMDA, $rdata[COL_KD_PEMDA])
                    ->where(COL_KD_MISI, $rdata[COL_KD_MISI])
                    ->where(COL_KD_TUJUAN, $rdata[COL_KD_TUJUAN])
                    ->where(COL_KD_INDIKATORTUJUAN, $rdata[COL_KD_INDIKATORTUJUAN])
                    ->where(COL_KD_SASARAN, $rdata[COL_KD_SASARAN])
                    ->where(COL_KD_INDIKATORSASARAN, $rdata[COL_KD_INDIKATORSASARAN])
                    ->where(COL_KD_TUJUANOPD, $rdata[COL_KD_TUJUANOPD])
                    ->where(COL_KD_INDIKATORTUJUANOPD, $rdata[COL_KD_INDIKATORTUJUANOPD])
                    ->where(COL_KD_SASARANOPD, $rdata[COL_KD_SASARANOPD])
                    ->where(COL_KD_INDIKATORSASARANOPD, $rdata[COL_KD_INDIKATORSASARANOPD])
                    ->where(COL_KD_TAHUN, $rdata[COL_KD_TAHUN])
                    ->where(COL_KD_SASARANPROGRAMOPD, $rdata[COL_KD_SASARANPROGRAMOPD])
                    ->where(COL_KD_INDIKATORPROGRAMOPD, $rdata[COL_KD_INDIKATORPROGRAMOPD])
                    ->where(COL_KD_SASARANSUBBIDANG, $rdata[COL_KD_SASARANSUBBIDANG])
                    ->get(TBL_SAKIP_MSUBBID_INDIKATOR)
                    ->result_array();
                $detUpdated = [];
                foreach($det as $d) {
                    $arrCond = array(
                        COL_KD_URUSAN => $d[COL_KD_URUSAN],
                        COL_KD_BIDANG => $d[COL_KD_BIDANG],
                        COL_KD_UNIT => $d[COL_KD_UNIT],
                        COL_KD_SUB => $d[COL_KD_SUB],
                        COL_KD_BID => $d[COL_KD_BID],
                        COL_KD_SUBBID => $d[COL_KD_SUBBID],

                        COL_KD_PEMDA=>$d[COL_KD_PEMDA],
                        COL_KD_MISI=>$d[COL_KD_MISI],
                        COL_KD_TUJUAN=>$d[COL_KD_TUJUAN],
                        COL_KD_INDIKATORTUJUAN=>$d[COL_KD_INDIKATORTUJUAN],
                        COL_KD_SASARAN=>$d[COL_KD_SASARAN],
                        COL_KD_INDIKATORSASARAN=>$d[COL_KD_INDIKATORSASARAN],
                        COL_KD_TUJUANOPD=>$d[COL_KD_TUJUANOPD],
                        COL_KD_INDIKATORTUJUANOPD=>$d[COL_KD_INDIKATORTUJUANOPD],
                        COL_KD_SASARANOPD=>$d[COL_KD_SASARANOPD],
                        COL_KD_INDIKATORSASARANOPD=>$d[COL_KD_INDIKATORSASARANOPD],
                        COL_KD_TAHUN=>$d[COL_KD_TAHUN],
                        COL_KD_SASARANPROGRAMOPD=>$d[COL_KD_SASARANPROGRAMOPD],
                        COL_KD_INDIKATORPROGRAMOPD=>$d[COL_KD_INDIKATORPROGRAMOPD],
                        COL_KD_SASARANSUBBIDANG=>$d[COL_KD_SASARANSUBBIDANG],
                        COL_KD_INDIKATORSUBBIDANG=>$d[COL_KD_INDIKATORSUBBIDANG]
                    );
                    if(!empty($detNo) && in_array($d[COL_KD_INDIKATORPROGRAMOPD], $detNo)) {
                        $res = $this->db
                            ->where($arrCond)
                            ->update(TBL_SAKIP_MSUBBID_INDIKATOR, array(
                                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                                COL_KD_BID => $this->input->post(COL_KD_BID),
                                COL_KD_SUBBID => $this->input->post(COL_KD_SUBBID),

                                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                                COL_KD_MISI => $this->input->post(COL_KD_MISI),
                                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                                COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                                COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                                COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                                COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                                COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                                COL_KD_INDIKATORPROGRAMOPD => $this->input->post(COL_KD_INDIKATORPROGRAMOPD),
                                COL_KD_SASARANSUBBIDANG => $this->input->post(COL_KD_SASARANSUBBIDANG),

                                COL_KD_INDIKATORSUBBIDANG => $d[COL_KD_INDIKATORSUBBIDANG],
                                COL_NM_INDIKATORSUBBIDANG => $detKet[array_search($d[COL_KD_INDIKATORSUBBIDANG], $detNo)]
                            ));
                        if(!$res) {
                            throw new Exception("Database error: ".$this->db->error());
                        }
                    }
                    else {
                        $res = $this->db->delete(TBL_SAKIP_MSUBBID_INDIKATOR, $arrCond);
                        if(!$res) {
                            throw new Exception("Database error: ".$this->db->error());
                        }
                    }
                    $detUpdated[] = $d[COL_KD_INDIKATORSUBBIDANG];
                }
                /* update / delete */

                /* insert */
                for($i = 0; $i<count($detNo); $i++) {
                    if(!in_array($detNo[$i], $detUpdated)) {
                        $arrDet[] = array(
                            COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                            COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                            COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                            COL_KD_SUB => $this->input->post(COL_KD_SUB),
                            COL_KD_BID => $this->input->post(COL_KD_BID),
                            COL_KD_SUBBID => $this->input->post(COL_KD_SUBBID),

                            COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                            COL_KD_MISI => $this->input->post(COL_KD_MISI),
                            COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                            COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                            COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                            COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                            COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                            COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                            COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                            COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                            COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                            COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                            COL_KD_INDIKATORPROGRAMOPD => $this->input->post(COL_KD_INDIKATORPROGRAMOPD),
                            COL_KD_SASARANSUBBIDANG => $this->input->post(COL_KD_SASARANSUBBIDANG),

                            COL_KD_INDIKATORSUBBIDANG => $detNo[$i],
                            COL_NM_INDIKATORSUBBIDANG => $detKet[$i]
                        );
                    }
                }
                if(count($arrDet) > 0) {
                    $res = $this->db->insert_batch(TBL_SAKIP_MSUBBID_INDIKATOR, $arrDet);
                    if(!$res) {
                        throw new Exception("Database error: ".$this->db->error());
                    }
                }
                /* insert */
                $this->db->trans_commit();
                echo json_encode($resp);
            } catch (Exception $e) {
                $this->db->trans_rollback();
                $resp['error'] = $e->getMessage();
                $resp['success'] = 0;
                echo json_encode($resp);
                return;
            }
        }else{
            $this->load->view('msubbid/sasaran_form',$data);
        }
    }

    function sasaran_del(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $rsasaran = $this->db->where(COL_UNIQ, $datum)->get(TBL_SAKIP_MSUBBID_SASARAN)->row_array();
            $arrCond = array(
                COL_KD_URUSAN => $rsasaran[COL_KD_URUSAN],
                COL_KD_BIDANG => $rsasaran[COL_KD_BIDANG],
                COL_KD_UNIT => $rsasaran[COL_KD_UNIT],
                COL_KD_SUB => $rsasaran[COL_KD_SUB],
                COL_KD_BID => $rsasaran[COL_KD_BID],
                COL_KD_SUBBID => $rsasaran[COL_KD_SUBBID],

                COL_KD_PEMDA=>$rsasaran[COL_KD_PEMDA],
                COL_KD_MISI=>$rsasaran[COL_KD_MISI],
                COL_KD_TUJUAN=>$rsasaran[COL_KD_TUJUAN],
                COL_KD_INDIKATORTUJUAN=>$rsasaran[COL_KD_INDIKATORTUJUAN],
                COL_KD_SASARAN=>$rsasaran[COL_KD_SASARAN],
                COL_KD_INDIKATORSASARAN=>$rsasaran[COL_KD_INDIKATORSASARAN],
                COL_KD_TUJUANOPD=>$rsasaran[COL_KD_TUJUANOPD],
                COL_KD_INDIKATORTUJUANOPD=>$rsasaran[COL_KD_INDIKATORTUJUANOPD],
                COL_KD_SASARANOPD=>$rsasaran[COL_KD_SASARANOPD],
                COL_KD_INDIKATORSASARANOPD=>$rsasaran[COL_KD_INDIKATORSASARANOPD],
                COL_KD_TAHUN=>$rsasaran[COL_KD_TAHUN],
                COL_KD_SASARANPROGRAMOPD=>$rsasaran[COL_KD_SASARANPROGRAMOPD],
                COL_KD_INDIKATORPROGRAMOPD=>$rsasaran[COL_KD_INDIKATORPROGRAMOPD],
                COL_KD_SASARANSUBBIDANG=>$rsasaran[COL_KD_SASARANSUBBIDANG]
            );

            $this->db->delete(TBL_SAKIP_MSUBBID_INDIKATOR, $arrCond);
            $this->db->delete(TBL_SAKIP_MSUBBID_SASARAN, array(COL_UNIQ => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function sasaran_kegiatan() {
        $ruser = GetLoggedUser();
        $data['title'] = 'Sasaran Kegiatan OPD';
        $this->db->select('*,'.TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.'.'.COL_UNIQ.' as ID,'.TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.'.'.COL_KD_TAHUN.' as Kd_Tahun,'.TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.'.'.COL_NM_SASARANKEGIATANOPD.' as Nm_SasaranKegiatanOPD');
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_PEMDA,"inner");
        $this->db->join(TBL_SAKIP_MBID_PROGRAM,
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SASARANOPD." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BID." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_PROGRAMOPD." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_TAHUN
            ,"inner");
        $this->db->join(TBL_SAKIP_MSUBBID_KEGIATAN,
            TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SASARANOPD." AND ".
            TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
            TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BID." AND ".
            TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_PROGRAMOPD." AND ".
            TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_TAHUN." AND ".
            TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_SASARANPROGRAMOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SASARANPROGRAMOPD." AND ".
            TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_SUBBID." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SUBBID." AND ".
            TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_KEGIATANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_KEGIATANOPD
            ,"inner");
        $this->db->join(TBL_SAKIP_MBID,
            TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SUB." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BID
            ,"inner");
        $this->db->join(TBL_SAKIP_MSUBBID,
            TBL_SAKIP_MSUBBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MSUBBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MSUBBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MSUBBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SUB." AND ".
            TBL_SAKIP_MSUBBID.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BID." AND ".
            TBL_SAKIP_MSUBBID.'.'.COL_KD_SUBBID." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SUBBID
            ,"inner");
        $this->db->join(TBL_SAKIP_MPMD_MISI,TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_MISI.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_MISI,"inner");
        $this->db->join(TBL_SAKIP_MPMD_TUJUAN,TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_IKTUJUAN,TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_SASARAN,TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_IKSASARAN,TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARAN,"inner");
        $this->db->join(TBL_SAKIP_MOPD_TUJUAN,
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARAN. " AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUANOPD
            ,"inner");
        $this->db->join(TBL_SAKIP_MOPD_IKTUJUAN,
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUANOPD
            ,"inner");
        $this->db->join(TBL_SAKIP_MOPD_SASARAN,
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARANOPD
            ,"inner");
        $this->db->join(TBL_SAKIP_MOPD_IKSASARAN,
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARANOPD." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARANOPD
            ,"inner");
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SUB, $strOPD[3]);
            if($ruser[COL_ROLEID] == ROLEKABID) $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BID, $strOPD[4]);
            if($ruser[COL_ROLEID] == ROLEKASUBBID) {
                $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BID, $strOPD[4]);
                $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SUBBID, $strOPD[5]);
            }
        }
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_TO." >=", date("Y"));
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_TAHUN, 'desc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_URUSAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BIDANG, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_UNIT, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SUB, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BID, 'asc');
        $this->db->order_by(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM, 'desc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_MISI, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_TUJUAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORTUJUAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SASARAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORSASARAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_TUJUANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORTUJUANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SASARANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORSASARANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_PROGRAMOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SASARANPROGRAMOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_KEGIATANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SASARANKEGIATANOPD, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN)->result_array();
        $this->load->view('msubbid/sasaran_kegiatan', $data);
    }

    function sasaran_kegiatan_add() {
        $data['title'] = "Sasaran Kegiatan OPD";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('msubbid/sasaran-kegiatan');
            $data = array(
                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_BID => $this->input->post(COL_KD_BID),
                COL_KD_SUBBID => $this->input->post(COL_KD_SUBBID),

                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                COL_KD_MISI => $this->input->post(COL_KD_MISI),
                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD),
                COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                COL_KD_KEGIATANOPD => $this->input->post(COL_KD_KEGIATANOPD),
                COL_KD_SASARANKEGIATANOPD => $this->input->post(COL_KD_SASARANKEGIATANOPD),
                COL_NM_SASARANKEGIATANOPD => $this->input->post(COL_NM_SASARANKEGIATANOPD),
                COL_KD_SATUAN => $this->input->post(COL_KD_SATUAN),
                COL_AWAL => $this->input->post(COL_AWAL),
                COL_TARGET => $this->input->post(COL_TARGET),
                COL_AKHIR => $this->input->post(COL_AKHIR),
            );

            $this->db->trans_begin();
            try {
                if(!$this->db->insert(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN, $data)){
                    throw new Exception("Database error: ".$this->db->error());
                }
                $detNo = $this->input->post("NoDet");
                $detDesc = $this->input->post("KetDet");
                $detFormula = $this->input->post("FormulaDet");
                $detSumber = $this->input->post("SumberDet");
                $detPIC = $this->input->post("PICDet");
                $detSatuan = $this->input->post("SatuanDet");
                $detTarget = $this->input->post("TargetDet");
                $detAwal = $this->input->post("AwalDet");
                $detAkhir = $this->input->post("AkhirDet");
                $det = [];
                for($i = 0; $i<count($detNo); $i++) {
                    $det[] = array(
                        COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                        COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                        COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                        COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                        COL_KD_SUB => $this->input->post(COL_KD_SUB),
                        COL_KD_BID => $this->input->post(COL_KD_BID),
                        COL_KD_SUBBID => $this->input->post(COL_KD_SUBBID),

                        COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                        COL_KD_MISI => $this->input->post(COL_KD_MISI),
                        COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                        COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                        COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                        COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                        COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                        COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                        COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                        COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                        COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD),
                        COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                        COL_KD_KEGIATANOPD => $this->input->post(COL_KD_KEGIATANOPD),
                        COL_KD_SASARANKEGIATANOPD => $this->input->post(COL_KD_SASARANKEGIATANOPD),
                        COL_KD_INDIKATORKEGIATANOPD => $detNo[$i],
                        COL_NM_INDIKATORKEGIATANOPD => $detDesc[$i],
                        COL_NM_FORMULA => $detFormula[$i],
                        COL_NM_SUMBERDATA => $detSumber[$i],
                        COL_NM_PENANGGUNGJAWAB => $detPIC[$i],
                        COL_KD_SATUAN => $detSatuan[$i],
                        COL_TARGET => $detTarget[$i],
                        COL_AWAL => $detAwal[$i],
                        COL_AKHIR => $detAkhir[$i]
                    );
                }
                if(count($det) > 0) {
                    $res = $this->db->insert_batch(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR, $det);
                    if(!$res) {
                        throw new Exception("Database error: ".$this->db->error());
                    }
                }
                $this->db->trans_commit();
                echo json_encode($resp);
            } catch (Exception $e) {
                $this->db->trans_rollback();
                $resp['error'] = $e->getMessage();
                $resp['success'] = 0;
                echo json_encode($resp);
                return;
            }
        }else{
            $this->db->where(COL_KD_TAHUN_FROM." <=", date("Y"));
            $this->db->where(COL_KD_TAHUN_TO." >=", date("Y"));
            $this->db->order_by(COL_KD_TAHUN_FROM, 'desc');
            $rpemda = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();

            if(!empty($rpemda)) {
                $data['data'] = array(
                    COL_KD_PEMDA => $rpemda[COL_KD_PEMDA],
                    "DefPeriod" => $rpemda[COL_KD_TAHUN_FROM]." s.d ".$rpemda[COL_KD_TAHUN_TO]." : ".$rpemda[COL_NM_PEJABAT]
                );
            }

            $this->load->view('msubbid/sasaran_kegiatan_form',$data);
        }
    }

    function sasaran_kegiatan_edit($id) {
        $ruser = GetLoggedUser();
        $this->db
            //->select(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.'.*,'.TBL_SAKIP_MBID.'.'.COL_NM_BID.','.TBL_SAKIP_MSUBBID.'.'.COL_NM_SUBBID.','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_NM_PROGRAMOPD.','.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_ISEPLAN.','.TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_NM_KEGIATANOPD.','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO)
            ->select(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.'.*,'.
                TBL_SAKIP_MPMD_MISI.'.'.COL_NM_MISI.','.
                TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.','.
                TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.','.
                TBL_SAKIP_MPEMDA.'.'.COL_NM_PEJABAT.', '.
                TBL_SAKIP_MPMD_TUJUAN.'.'.COL_NM_TUJUAN.', '.
                TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_NM_INDIKATORTUJUAN.', '.
                TBL_SAKIP_MPMD_SASARAN.'.'.COL_NM_SASARAN.', '.
                TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_NM_INDIKATORSASARAN.', '.
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_NM_TUJUANOPD.', '.
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_NM_INDIKATORTUJUANOPD.', '.
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_NM_SASARANOPD.', '.
                TBL_SAKIP_MBID.'.'.COL_NM_BID.','.
                TBL_SAKIP_MSUBBID.'.'.COL_NM_SUBBID.','.
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_NM_INDIKATORSASARANOPD.','.
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_NM_PROGRAMOPD.','.
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_NM_SASARANPROGRAMOPD.','.
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_NM_KEGIATANOPD
            )
            ->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_PEMDA,"inner")
            ->join(TBL_SAKIP_MBID,
                TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BID
                ,"inner")
            ->join(TBL_SAKIP_MSUBBID,
                TBL_SAKIP_MSUBBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BID." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_SUBBID." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SUBBID
                ,"inner")
            ->join(TBL_SAKIP_MBID_PROGRAM,
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BID." AND ".

                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SASARANOPD." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_PROGRAMOPD." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_TAHUN
                ,"inner")
            ->join(TBL_SAKIP_MBID_PROGRAM_SASARAN,
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_TAHUN." AND ".

                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SASARANOPD." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BID." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_PROGRAMOPD." AND ".
                TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_SASARANPROGRAMOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SASARANPROGRAMOPD
                ,"inner")
            ->join(TBL_SAKIP_MSUBBID_KEGIATAN,
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BID." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_SUBBID." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SUBBID." AND ".

                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SASARANOPD." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_PROGRAMOPD." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_TAHUN." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_SASARANPROGRAMOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SASARANPROGRAMOPD." AND ".
                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_KEGIATANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_KEGIATANOPD
                ,"inner")
            ->join(TBL_SAKIP_MPMD_MISI,TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_MISI.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI,"inner")
            ->join(TBL_SAKIP_MPMD_TUJUAN,TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN,"inner")
            ->join(TBL_SAKIP_MPMD_IKTUJUAN,TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN,"inner")
            ->join(TBL_SAKIP_MPMD_SASARAN,TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN,"inner")
            ->join(TBL_SAKIP_MPMD_IKSASARAN,TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN,"inner")
            ->join(TBL_SAKIP_MOPD_TUJUAN,
                TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD
                ,"inner")
            ->join(TBL_SAKIP_MOPD_IKTUJUAN,
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD
                ,"inner")
            ->join(TBL_SAKIP_MOPD_SASARAN,
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARANOPD
                ,"inner")
            ->join(TBL_SAKIP_MOPD_IKSASARAN,
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARANOPD
                ,"inner")
            ->where(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_UNIQ, $id);
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SUB, $strOPD[3]);
            if($ruser[COL_ROLEID] == ROLEKABID) $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BID, $strOPD[4]);
            if($ruser[COL_ROLEID] == ROLEKASUBBID) {
                $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BID, $strOPD[4]);
                $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SUBBID, $strOPD[5]);
            }
        }
        $rdata = $data['data'] = $this->db->get(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = "Sasaran Kegiatan OPD";;
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('msubbid/sasaran_kegiatan');
            $data = array(
                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_BID => $this->input->post(COL_KD_BID),
                COL_KD_SUBBID => $this->input->post(COL_KD_SUBBID),

                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                COL_KD_MISI => $this->input->post(COL_KD_MISI),
                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD),
                COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                COL_KD_KEGIATANOPD => $this->input->post(COL_KD_KEGIATANOPD),
                COL_KD_SASARANKEGIATANOPD => $this->input->post(COL_KD_SASARANKEGIATANOPD),
                COL_NM_SASARANKEGIATANOPD => $this->input->post(COL_NM_SASARANKEGIATANOPD),
                COL_KD_SATUAN => $this->input->post(COL_KD_SATUAN),
                COL_AWAL => $this->input->post(COL_AWAL),
                COL_TARGET => $this->input->post(COL_TARGET),
                COL_AKHIR => $this->input->post(COL_AKHIR),
            );

            $this->db->trans_begin();
            try {
                if(!$this->db->where(COL_UNIQ, $id)->update(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN, $data)){
                    throw new Exception("Database error: ".$this->db->error());
                }

                $detNo = $this->input->post("NoDet");
                $detKet = $this->input->post("KetDet");
                $detFormula = $this->input->post("FormulaDet");
                $detSumber = $this->input->post("SumberDet");
                $detPIC = $this->input->post("PICDet");
                $detSatuan = $this->input->post("SatuanDet");
                $detTarget = $this->input->post("TargetDet");
                $detAwal = $this->input->post("AwalDet");
                $detAkhir = $this->input->post("AkhirDet");
                $arrDet = [];

                /* update / delete */
                $det = $this->db
                    //->select(COL_KD_PEMDA.",".COL_KD_MISI.",".COL_KD_TUJUAN.",".COL_KD_INDIKATORTUJUAN.",".COL_KD_SASARAN.",".COL_KD_INDIKATORSASARAN)
                    ->where(COL_KD_URUSAN, $rdata[COL_KD_URUSAN])
                    ->where(COL_KD_BIDANG, $rdata[COL_KD_BIDANG])
                    ->where(COL_KD_UNIT, $rdata[COL_KD_UNIT])
                    ->where(COL_KD_SUB, $rdata[COL_KD_SUB])
                    ->where(COL_KD_BID, $rdata[COL_KD_BID])
                    ->where(COL_KD_SUBBID, $rdata[COL_KD_SUBBID])

                    ->where(COL_KD_PEMDA, $rdata[COL_KD_PEMDA])
                    ->where(COL_KD_MISI, $rdata[COL_KD_MISI])
                    ->where(COL_KD_TUJUAN, $rdata[COL_KD_TUJUAN])
                    ->where(COL_KD_INDIKATORTUJUAN, $rdata[COL_KD_INDIKATORTUJUAN])
                    ->where(COL_KD_SASARAN, $rdata[COL_KD_SASARAN])
                    ->where(COL_KD_INDIKATORSASARAN, $rdata[COL_KD_INDIKATORSASARAN])
                    ->where(COL_KD_TUJUANOPD, $rdata[COL_KD_TUJUANOPD])
                    ->where(COL_KD_INDIKATORTUJUANOPD, $rdata[COL_KD_INDIKATORTUJUANOPD])
                    ->where(COL_KD_SASARANOPD, $rdata[COL_KD_SASARANOPD])
                    ->where(COL_KD_INDIKATORSASARANOPD, $rdata[COL_KD_INDIKATORSASARANOPD])
                    ->where(COL_KD_TAHUN, $rdata[COL_KD_TAHUN])
                    ->where(COL_KD_PROGRAMOPD, $rdata[COL_KD_PROGRAMOPD])
                    ->where(COL_KD_SASARANPROGRAMOPD, $rdata[COL_KD_SASARANPROGRAMOPD])
                    ->where(COL_KD_KEGIATANOPD, $rdata[COL_KD_KEGIATANOPD])
                    ->where(COL_KD_SASARANKEGIATANOPD, $rdata[COL_KD_SASARANKEGIATANOPD])
                    ->get(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR)
                    ->result_array();
                $detUpdated = [];
                foreach($det as $d) {
                    $arrCond = array(
                        COL_KD_URUSAN => $d[COL_KD_URUSAN],
                        COL_KD_BIDANG => $d[COL_KD_BIDANG],
                        COL_KD_UNIT => $d[COL_KD_UNIT],
                        COL_KD_SUB => $d[COL_KD_SUB],
                        COL_KD_BID => $d[COL_KD_BID],
                        COL_KD_SUBBID => $d[COL_KD_SUBBID],

                        COL_KD_PEMDA=>$d[COL_KD_PEMDA],
                        COL_KD_MISI=>$d[COL_KD_MISI],
                        COL_KD_TUJUAN=>$d[COL_KD_TUJUAN],
                        COL_KD_INDIKATORTUJUAN=>$d[COL_KD_INDIKATORTUJUAN],
                        COL_KD_SASARAN=>$d[COL_KD_SASARAN],
                        COL_KD_INDIKATORSASARAN=>$d[COL_KD_INDIKATORSASARAN],
                        COL_KD_TUJUANOPD=>$d[COL_KD_TUJUANOPD],
                        COL_KD_INDIKATORTUJUANOPD=>$d[COL_KD_INDIKATORTUJUANOPD],
                        COL_KD_SASARANOPD=>$d[COL_KD_SASARANOPD],
                        COL_KD_INDIKATORSASARANOPD=>$d[COL_KD_INDIKATORSASARANOPD],
                        COL_KD_TAHUN=>$d[COL_KD_TAHUN],
                        COL_KD_PROGRAMOPD=>$d[COL_KD_PROGRAMOPD],
                        COL_KD_SASARANPROGRAMOPD=>$d[COL_KD_SASARANPROGRAMOPD],
                        COL_KD_KEGIATANOPD=>$d[COL_KD_KEGIATANOPD],
                        COL_KD_SASARANKEGIATANOPD=>$d[COL_KD_SASARANKEGIATANOPD],
                        COL_KD_INDIKATORKEGIATANOPD=>$d[COL_KD_INDIKATORKEGIATANOPD]
                    );
                    if(!empty($detNo) && in_array($d[COL_KD_INDIKATORKEGIATANOPD], $detNo)) {
                        $res = $this->db
                            ->where($arrCond)
                            ->update(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR, array(
                                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                                COL_KD_BID => $this->input->post(COL_KD_BID),
                                COL_KD_SUBBID => $this->input->post(COL_KD_SUBBID),

                                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                                COL_KD_MISI => $this->input->post(COL_KD_MISI),
                                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                                COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                                COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                                COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                                COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                                COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD),
                                COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                                COL_KD_KEGIATANOPD => $this->input->post(COL_KD_KEGIATANOPD),
                                COL_KD_SASARANKEGIATANOPD => $this->input->post(COL_KD_SASARANKEGIATANOPD),
                                COL_KD_INDIKATORKEGIATANOPD => $d[COL_KD_INDIKATORKEGIATANOPD],
                                COL_NM_INDIKATORKEGIATANOPD => $detKet[array_search($d[COL_KD_INDIKATORKEGIATANOPD], $detNo)],
                                COL_NM_FORMULA => $detFormula[array_search($d[COL_KD_INDIKATORKEGIATANOPD], $detNo)],
                                COL_NM_SUMBERDATA => $detSumber[array_search($d[COL_KD_INDIKATORKEGIATANOPD], $detNo)],
                                COL_NM_PENANGGUNGJAWAB => $detPIC[array_search($d[COL_KD_INDIKATORKEGIATANOPD], $detNo)],
                                COL_KD_SATUAN => $detSatuan[array_search($d[COL_KD_INDIKATORKEGIATANOPD], $detNo)],
                                COL_TARGET => $detTarget[array_search($d[COL_KD_INDIKATORKEGIATANOPD], $detNo)],
                                COL_AWAL => $detAwal[array_search($d[COL_KD_INDIKATORKEGIATANOPD], $detNo)],
                                COL_AKHIR => $detAkhir[array_search($d[COL_KD_INDIKATORKEGIATANOPD], $detNo)]
                            ));
                        if(!$res) {
                            throw new Exception("Database error: ".$this->db->error());
                        }
                    }
                    else {
                        $res = $this->db->delete(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR, $arrCond);
                        if(!$res) {
                            throw new Exception("Database error: ".$this->db->error());
                        }
                    }
                    $detUpdated[] = $d[COL_KD_INDIKATORKEGIATANOPD];
                }
                /* update / delete */

                /* insert */
                for($i = 0; $i<count($detNo); $i++) {
                    if(!in_array($detNo[$i], $detUpdated)) {
                        $arrDet[] = array(
                            COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                            COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                            COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                            COL_KD_SUB => $this->input->post(COL_KD_SUB),
                            COL_KD_BID => $this->input->post(COL_KD_BID),
                            COL_KD_SUBBID => $this->input->post(COL_KD_SUBBID),

                            COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                            COL_KD_MISI => $this->input->post(COL_KD_MISI),
                            COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                            COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                            COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                            COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                            COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                            COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                            COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                            COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                            COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                            COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD),
                            COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                            COL_KD_KEGIATANOPD => $this->input->post(COL_KD_KEGIATANOPD),
                            COL_KD_SASARANKEGIATANOPD => $this->input->post(COL_KD_SASARANKEGIATANOPD),

                            COL_KD_INDIKATORKEGIATANOPD => $detNo[$i],
                            COL_NM_INDIKATORKEGIATANOPD => $detKet[$i],
                            COL_NM_FORMULA => $detFormula[$i],
                            COL_NM_SUMBERDATA => $detSumber[$i],
                            COL_NM_PENANGGUNGJAWAB => $detPIC[$i],
                            COL_KD_SATUAN => $detSatuan[$i],
                            COL_TARGET => $detTarget[$i],
                            COL_AWAL => $detAwal[$i],
                            COL_AKHIR => $detAkhir[$i]
                        );
                    }
                }
                if(count($arrDet) > 0) {
                    $res = $this->db->insert_batch(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR, $arrDet);
                    if(!$res) {
                        throw new Exception("Database error: ".$this->db->error());
                    }
                }
                /* insert */
                $this->db->trans_commit();
                echo json_encode($resp);

            } catch (Exception $e) {
                $this->db->trans_rollback();
                $resp['error'] = $e->getMessage();
                $resp['success'] = 0;
                echo json_encode($resp);
                return;
            }
        }else{
            $this->load->view('msubbid/sasaran_kegiatan_form',$data);
        }
    }

    function sasaran_kegiatan_del(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $rsasaran = $this->db->where(COL_UNIQ, $datum)->get(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN)->row_array();
            $arrCond = array(
                COL_KD_URUSAN => $rsasaran[COL_KD_URUSAN],
                COL_KD_BIDANG => $rsasaran[COL_KD_BIDANG],
                COL_KD_UNIT => $rsasaran[COL_KD_UNIT],
                COL_KD_SUB => $rsasaran[COL_KD_SUB],
                COL_KD_BID => $rsasaran[COL_KD_BID],
                COL_KD_SUBBID => $rsasaran[COL_KD_SUBBID],

                COL_KD_PEMDA=>$rsasaran[COL_KD_PEMDA],
                COL_KD_MISI=>$rsasaran[COL_KD_MISI],
                COL_KD_TUJUAN=>$rsasaran[COL_KD_TUJUAN],
                COL_KD_INDIKATORTUJUAN=>$rsasaran[COL_KD_INDIKATORTUJUAN],
                COL_KD_SASARAN=>$rsasaran[COL_KD_SASARAN],
                COL_KD_INDIKATORSASARAN=>$rsasaran[COL_KD_INDIKATORSASARAN],
                COL_KD_TUJUANOPD=>$rsasaran[COL_KD_TUJUANOPD],
                COL_KD_INDIKATORTUJUANOPD=>$rsasaran[COL_KD_INDIKATORTUJUANOPD],
                COL_KD_SASARANOPD=>$rsasaran[COL_KD_SASARANOPD],
                COL_KD_INDIKATORSASARANOPD=>$rsasaran[COL_KD_INDIKATORSASARANOPD],
                COL_KD_TAHUN=>$rsasaran[COL_KD_TAHUN],
                COL_KD_PROGRAMOPD=>$rsasaran[COL_KD_PROGRAMOPD],
                COL_KD_SASARANPROGRAMOPD=>$rsasaran[COL_KD_SASARANPROGRAMOPD],
                COL_KD_KEGIATANOPD=>$rsasaran[COL_KD_KEGIATANOPD],
                COL_KD_SASARANKEGIATANOPD=>$rsasaran[COL_KD_SASARANKEGIATANOPD]
            );

            $this->db->delete(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR, $arrCond);
            $this->db->delete(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN, array(COL_UNIQ => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function sasaran_individu() {
        $ruser = GetLoggedUser();
        $data['title'] = 'Sasaran Individu';
        $this->db->select('*,'.TBL_SAKIP_INDIVIDU_SASARAN.'.'.COL_UNIQ.' as ID,'.TBL_SAKIP_INDIVIDU_SASARAN.'.'.COL_KD_TAHUN.' as Kd_Tahun');
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_PEMDA,"inner");
        $this->db->join(TBL_SAKIP_MOPD_TUJUAN,
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORSASARAN. " AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_TUJUANOPD
            ,"left");
        $this->db->join(TBL_SAKIP_MOPD_IKTUJUAN,
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORTUJUANOPD
            ,"left");
        $this->db->join(TBL_SAKIP_MOPD_SASARAN,
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SASARANOPD
            ,"left");
        $this->db->join(TBL_SAKIP_MOPD_IKSASARAN,
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SASARANOPD." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORSASARANOPD
            ,"left");
            $this->db->join(TBL_SAKIP_MBID_SASARAN,
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_BID." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BID." AND ".

                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SASARANOPD." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_TAHUN." AND ".
                TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_SASARANPROGRAMOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SASARANPROGRAMOPD
                ,"left");
        $this->db->join(TBL_SAKIP_MBID_INDIKATOR,
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_UNIT." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_SUB." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SUB." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_BID." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BID." AND ".

            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_MISI." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SASARANOPD." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_TAHUN." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_SASARANPROGRAMOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SASARANPROGRAMOPD." AND ".
            TBL_SAKIP_MBID_INDIKATOR.'.'.COL_KD_INDIKATORPROGRAMOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORPROGRAMOPD
            ,"left");
        $this->db->join(TBL_SAKIP_MSUBBID_SASARAN,
            TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SUB." AND ".
            TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_BID." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BID." AND ".
            TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_SUBBID." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SUBBID." AND ".

            TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SASARANOPD." AND ".
            TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
            TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_TAHUN." AND ".
            TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_SASARANPROGRAMOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SASARANPROGRAMOPD." AND ".
            TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_INDIKATORPROGRAMOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORPROGRAMOPD." AND ".
            TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_SASARANSUBBIDANG." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SASARANSUBBIDANG
            ,"left");
        $this->db->join(TBL_SAKIP_MBID,
            TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SUB." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_BID." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BID
            ,"left");
        $this->db->join(TBL_SAKIP_MSUBBID,
            TBL_SAKIP_MSUBBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MSUBBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MSUBBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MSUBBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SUB." AND ".
            TBL_SAKIP_MSUBBID.'.'.COL_KD_BID." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BID." AND ".
            TBL_SAKIP_MSUBBID.'.'.COL_KD_SUBBID." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SUBBID
            ,"left");
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SUB, $strOPD[3]);
            if($ruser[COL_ROLEID] == ROLEKABID) $this->db->where(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BID, $strOPD[4]);
            if($ruser[COL_ROLEID] == ROLEKASUBBID) {
                $this->db->where(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BID, $strOPD[4]);
                $this->db->where(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SUBBID, $strOPD[5]);
            }
        }
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_TO." >=", date("Y"));
        $this->db->order_by(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_TAHUN, 'desc');
        $this->db->order_by(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_URUSAN, 'asc');
        $this->db->order_by(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BIDANG, 'asc');
        $this->db->order_by(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_UNIT, 'asc');
        $this->db->order_by(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SUB, 'asc');
        $this->db->order_by(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BID, 'asc');
        $this->db->order_by(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM, 'desc');
        $this->db->order_by(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_MISI, 'asc');
        $this->db->order_by(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_TUJUAN, 'asc');
        $this->db->order_by(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORTUJUAN, 'asc');
        $this->db->order_by(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SASARAN, 'asc');
        $this->db->order_by(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORSASARAN, 'asc');
        $this->db->order_by(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_TUJUANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORTUJUANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SASARANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORSASARANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SASARANPROGRAMOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORPROGRAMOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SASARANSUBBIDANG, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_INDIVIDU_SASARAN)->result_array();
        //echo $this->db->last_query();
        //return;
        $this->load->view('msubbid/sasaran_individu', $data);
    }

    function sasaran_individu_add() {
        $data['title'] = "Sasaran Individu";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('msubbid/sasaran-individu');
            $data = array(
                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_BID => $this->input->post(COL_KD_BID),
                COL_KD_SUBBID => $this->input->post(COL_KD_SUBBID),

                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                COL_KD_MISI => $this->input->post(COL_KD_MISI),
                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                COL_KD_INDIKATORPROGRAMOPD => $this->input->post(COL_KD_INDIKATORPROGRAMOPD),
                COL_KD_SASARANSUBBIDANG => $this->input->post(COL_KD_SASARANSUBBIDANG),
                COL_KD_SASARANINDIVIDU => $this->input->post(COL_KD_SASARANINDIVIDU),
                COL_NM_JABATAN => $this->input->post(COL_NM_JABATAN),
                COL_NM_PEGAWAI => $this->input->post(COL_NM_PEGAWAI),
            );

            $this->db->trans_begin();
            try {
                if(!$this->db->insert(TBL_SAKIP_INDIVIDU_SASARAN, $data)){
                  throw new Exception("Database error: ".$this->db->error());
                }
                $detNo = $this->input->post("NoDet");
                $detSasaran = $this->input->post("SasaranDet");
                $detIndikator = $this->input->post("IndikatorDet");
                $detTarget = $this->input->post("TargetDet");
                $det = [];
                for($i = 0; $i<count($detNo); $i++) {
                  $det[] = array(
                    COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                    COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                    COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                    COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                    COL_KD_SUB => $this->input->post(COL_KD_SUB),
                    COL_KD_BID => $this->input->post(COL_KD_BID),
                    COL_KD_SUBBID => $this->input->post(COL_KD_SUBBID),

                    COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                    COL_KD_MISI => $this->input->post(COL_KD_MISI),
                    COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                    COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                    COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                    COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                    COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                    COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                    COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                    COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                    COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                    COL_KD_INDIKATORPROGRAMOPD => $this->input->post(COL_KD_INDIKATORPROGRAMOPD),
                    COL_KD_SASARANSUBBIDANG => $this->input->post(COL_KD_SASARANSUBBIDANG),
                    COL_KD_SASARANINDIVIDU => $this->input->post(COL_KD_SASARANINDIVIDU),
                    COL_KD_INDIKATORINDIVIDU => $detNo[$i],
                    COL_NM_SASARANINDIVIDU => $detSasaran[$i],
                    COL_NM_INDIKATORINDIVIDU => $detIndikator[$i],
                    COL_NM_TARGET => $detTarget[$i]
                  );
                }
                if(count($det) > 0) {
                  $res = $this->db->insert_batch(TBL_SAKIP_INDIVIDU_INDIKATOR, $det);
                  if(!$res) {
                    throw new Exception("Database error: ".$this->db->error());
                  }
                }

                $this->db->trans_commit();
                echo json_encode($resp);
            } catch (Exception $e) {
                $this->db->trans_rollback();
                $resp['error'] = $e->getMessage();
                $resp['success'] = 0;
                echo json_encode($resp);
                return;
            }
        }else{
            $this->load->view('msubbid/sasaran_individu_form',$data);
        }
    }

    function sasaran_individu_edit($id) {
        $ruser = GetLoggedUser();
        $this->db
            ->select(TBL_SAKIP_INDIVIDU_SASARAN.'.*,'.TBL_SAKIP_MBID.'.'.COL_NM_BID.','.TBL_SAKIP_MSUBBID.'.'.COL_NM_SUBBID.','.TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_NM_SASARANSUBBIDANG/*.','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_NM_PROGRAMOPD.','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_ISEPLAN*/)
            ->join(TBL_SAKIP_MBID,
                TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_BID." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BID
                ,"inner")
            ->join(TBL_SAKIP_MSUBBID,
                TBL_SAKIP_MSUBBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_BID." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BID." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_SUBBID." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SUBBID
                ,"inner")
            ->join(TBL_SAKIP_MSUBBID_SASARAN,
                TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_BID." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BID." AND ".
                TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_SUBBID." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SUBBID." AND ".

                TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SASARANOPD." AND ".
                TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
                TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_TAHUN." AND ".
                TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_SASARANPROGRAMOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SASARANPROGRAMOPD." AND ".
                TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_INDIKATORPROGRAMOPD." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_INDIKATORPROGRAMOPD." AND ".
                TBL_SAKIP_MSUBBID_SASARAN.'.'.COL_KD_SASARANSUBBIDANG." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SASARANSUBBIDANG
                ,"inner")
            ->where(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_UNIQ, $id);
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SUB, $strOPD[3]);
            if($ruser[COL_ROLEID] == ROLEKABID) $this->db->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BID, $strOPD[4]);
            if($ruser[COL_ROLEID] == ROLEKASUBBID) {
                $this->db->where(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BID, $strOPD[4]);
                $this->db->where(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SUBBID, $strOPD[5]);
            }
        }
        $rdata = $data['data'] = $this->db->get(TBL_SAKIP_INDIVIDU_SASARAN)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = "Sasaran Individu";;
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('msubbid/sasaran-individu');
            $data = array(
                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_BID => $this->input->post(COL_KD_BID),
                COL_KD_SUBBID => $this->input->post(COL_KD_SUBBID),

                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                COL_KD_MISI => $this->input->post(COL_KD_MISI),
                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                COL_KD_INDIKATORPROGRAMOPD => $this->input->post(COL_KD_INDIKATORPROGRAMOPD),
                COL_KD_SASARANSUBBIDANG => $this->input->post(COL_KD_SASARANSUBBIDANG),
                COL_KD_SASARANINDIVIDU => $this->input->post(COL_KD_SASARANINDIVIDU),
                COL_NM_JABATAN => $this->input->post(COL_NM_JABATAN),
                COL_NM_PEGAWAI => $this->input->post(COL_NM_PEGAWAI)
            );

            $this->db->trans_begin();
            try {
                if(!$this->db->where(COL_UNIQ, $id)->update(TBL_SAKIP_INDIVIDU_SASARAN, $data)){
                    throw new Exception("Database error: ".$this->db->error());
                }

                $detNo = $this->input->post("NoDet");
                $detSasaran = $this->input->post("SasaranDet");
                $detIndikator = $this->input->post("IndikatorDet");
                $detTarget = $this->input->post("TargetDet");
                $arrDet = [];

                /* update / delete */
                $det = $this->db
                    //->select(COL_KD_PEMDA.",".COL_KD_MISI.",".COL_KD_TUJUAN.",".COL_KD_INDIKATORTUJUAN.",".COL_KD_SASARAN.",".COL_KD_INDIKATORSASARAN)
                    ->where(COL_KD_URUSAN, $rdata[COL_KD_URUSAN])
                    ->where(COL_KD_BIDANG, $rdata[COL_KD_BIDANG])
                    ->where(COL_KD_UNIT, $rdata[COL_KD_UNIT])
                    ->where(COL_KD_SUB, $rdata[COL_KD_SUB])
                    ->where(COL_KD_BID, $rdata[COL_KD_BID])
                    ->where(COL_KD_SUBBID, $rdata[COL_KD_SUBBID])

                    ->where(COL_KD_PEMDA, $rdata[COL_KD_PEMDA])
                    ->where(COL_KD_MISI, $rdata[COL_KD_MISI])
                    ->where(COL_KD_TUJUAN, $rdata[COL_KD_TUJUAN])
                    ->where(COL_KD_INDIKATORTUJUAN, $rdata[COL_KD_INDIKATORTUJUAN])
                    ->where(COL_KD_SASARAN, $rdata[COL_KD_SASARAN])
                    ->where(COL_KD_INDIKATORSASARAN, $rdata[COL_KD_INDIKATORSASARAN])
                    ->where(COL_KD_TUJUANOPD, $rdata[COL_KD_TUJUANOPD])
                    ->where(COL_KD_INDIKATORTUJUANOPD, $rdata[COL_KD_INDIKATORTUJUANOPD])
                    ->where(COL_KD_SASARANOPD, $rdata[COL_KD_SASARANOPD])
                    ->where(COL_KD_INDIKATORSASARANOPD, $rdata[COL_KD_INDIKATORSASARANOPD])
                    ->where(COL_KD_TAHUN, $rdata[COL_KD_TAHUN])
                    ->where(COL_KD_SASARANPROGRAMOPD, $rdata[COL_KD_SASARANPROGRAMOPD])
                    ->where(COL_KD_INDIKATORPROGRAMOPD, $rdata[COL_KD_INDIKATORPROGRAMOPD])
                    ->where(COL_KD_SASARANSUBBIDANG, $rdata[COL_KD_SASARANSUBBIDANG])
                    ->where(COL_KD_SASARANINDIVIDU, $rdata[COL_KD_SASARANINDIVIDU])
                    ->get(TBL_SAKIP_INDIVIDU_INDIKATOR)
                    ->result_array();
                $detUpdated = [];
                foreach($det as $d) {
                    $arrCond = array(
                        COL_KD_URUSAN => $d[COL_KD_URUSAN],
                        COL_KD_BIDANG => $d[COL_KD_BIDANG],
                        COL_KD_UNIT => $d[COL_KD_UNIT],
                        COL_KD_SUB => $d[COL_KD_SUB],
                        COL_KD_BID => $d[COL_KD_BID],
                        COL_KD_SUBBID => $d[COL_KD_SUBBID],

                        COL_KD_PEMDA=>$d[COL_KD_PEMDA],
                        COL_KD_MISI=>$d[COL_KD_MISI],
                        COL_KD_TUJUAN=>$d[COL_KD_TUJUAN],
                        COL_KD_INDIKATORTUJUAN=>$d[COL_KD_INDIKATORTUJUAN],
                        COL_KD_SASARAN=>$d[COL_KD_SASARAN],
                        COL_KD_INDIKATORSASARAN=>$d[COL_KD_INDIKATORSASARAN],
                        COL_KD_TUJUANOPD=>$d[COL_KD_TUJUANOPD],
                        COL_KD_INDIKATORTUJUANOPD=>$d[COL_KD_INDIKATORTUJUANOPD],
                        COL_KD_SASARANOPD=>$d[COL_KD_SASARANOPD],
                        COL_KD_INDIKATORSASARANOPD=>$d[COL_KD_INDIKATORSASARANOPD],
                        COL_KD_TAHUN=>$d[COL_KD_TAHUN],
                        COL_KD_SASARANPROGRAMOPD=>$d[COL_KD_SASARANPROGRAMOPD],
                        COL_KD_INDIKATORPROGRAMOPD=>$d[COL_KD_INDIKATORPROGRAMOPD],
                        COL_KD_SASARANSUBBIDANG=>$d[COL_KD_SASARANSUBBIDANG],
                        COL_KD_SASARANINDIVIDU=>$d[COL_KD_SASARANINDIVIDU],
                        COL_KD_INDIKATORINDIVIDU=>$d[COL_KD_INDIKATORINDIVIDU]
                    );
                    if(!empty($detNo) && in_array($d[COL_KD_SASARANSUBBIDANG], $detNo)) {
                        $res = $this->db
                            ->where($arrCond)
                            ->update(TBL_SAKIP_INDIVIDU_INDIKATOR, array(
                                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                                COL_KD_BID => $this->input->post(COL_KD_BID),
                                COL_KD_SUBBID => $this->input->post(COL_KD_SUBBID),

                                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                                COL_KD_MISI => $this->input->post(COL_KD_MISI),
                                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                                COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                                COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                                COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                                COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                                COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                                COL_KD_INDIKATORPROGRAMOPD => $this->input->post(COL_KD_INDIKATORPROGRAMOPD),
                                COL_KD_SASARANSUBBIDANG => $this->input->post(COL_KD_SASARANSUBBIDANG),
                                COL_KD_SASARANINDIVIDU => $this->input->post(COL_KD_SASARANINDIVIDU),

                                COL_KD_INDIKATORINDIVIDU => $d[COL_KD_INDIKATORINDIVIDU],
                                COL_NM_SASARANINDIVIDU => $detSasaran[array_search($d[COL_KD_INDIKATORINDIVIDU], $detNo)],
                                COL_NM_INDIKATORINDIVIDU => $detIndikator[array_search($d[COL_KD_INDIKATORINDIVIDU], $detNo)],
                                COL_NM_TARGET => $detTarget[array_search($d[COL_KD_INDIKATORINDIVIDU], $detNo)]
                            ));
                        if(!$res) {
                            throw new Exception("Database error: ".$this->db->error());
                        }
                    }
                    else {
                        $res = $this->db->delete(TBL_SAKIP_INDIVIDU_INDIKATOR, $arrCond);
                        if(!$res) {
                            throw new Exception("Database error: ".$this->db->error());
                        }
                    }
                    $detUpdated[] = $d[COL_KD_INDIKATORINDIVIDU];
                }
                /* update / delete */

                /* insert */
                for($i = 0; $i<count($detNo); $i++) {
                    if(!in_array($detNo[$i], $detUpdated)) {
                        $arrDet[] = array(
                            COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                            COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                            COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                            COL_KD_SUB => $this->input->post(COL_KD_SUB),
                            COL_KD_BID => $this->input->post(COL_KD_BID),
                            COL_KD_SUBBID => $this->input->post(COL_KD_SUBBID),

                            COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                            COL_KD_MISI => $this->input->post(COL_KD_MISI),
                            COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                            COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                            COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                            COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                            COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                            COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                            COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                            COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                            COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                            COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                            COL_KD_INDIKATORPROGRAMOPD => $this->input->post(COL_KD_INDIKATORPROGRAMOPD),
                            COL_KD_SASARANSUBBIDANG => $this->input->post(COL_KD_SASARANSUBBIDANG),
                            COL_KD_SASARANINDIVIDU => $this->input->post(COL_KD_SASARANINDIVIDU),

                            COL_KD_INDIKATORINDIVIDU => $detNo[$i],
                            COL_NM_SASARANINDIVIDU => $detSasaran[$i],
                            COL_NM_INDIKATORINDIVIDU => $detIndikator[$i],
                            COL_NM_TARGET => $detTarget[$i]
                        );
                    }
                }
                if(count($arrDet) > 0) {
                    $res = $this->db->insert_batch(TBL_SAKIP_INDIVIDU_INDIKATOR, $arrDet);
                    if(!$res) {
                        throw new Exception("Database error: ".$this->db->error());
                    }
                }
                /* insert */
                $this->db->trans_commit();
                echo json_encode($resp);
            } catch (Exception $e) {
                $this->db->trans_rollback();
                $resp['error'] = $e->getMessage();
                $resp['success'] = 0;
                echo json_encode($resp);
                return;
            }
        }else{
            $this->load->view('msubbid/sasaran_individu_form',$data);
        }
    }

    function sasaran_individu_del(){
      $data = $this->input->post('cekbox');
      $deleted = 0;
      foreach ($data as $datum) {
        $this->db->delete(TBL_SAKIP_INDIVIDU_SASARAN, array(COL_UNIQ => $datum));
        $deleted++;
      }
      if($deleted){
        ShowJsonSuccess($deleted." data dihapus");
      }else{
        ShowJsonError("Tidak ada dihapus");
      }
    }
}
