<?php $this->load->view('header')?>
<?php
$ruser = GetLoggedUser();
?>
<section class="content-header">
  <h1><?= $title ?></h1>
  <ol class="breadcrumb">
    <li>
      <a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a>
    </li>
    <li class="active"><?=$title?></li>
  </ol>
</section>
<section class="content">
  <div class="box box-default">
    <?=form_open(current_url(),array('role'=>'form', 'method'=>'get','id'=>'form-filter','class'=>'form-horizontal'))?>
    <div class="box-header with-border">
      <div class="row">
        <div class="col-sm-12">
          <div class="form-group">
            <label class="control-label col-sm-3">Periode</label>
            <div class="col-sm-6">
              <select name="Period" class="form-control no-clear">
                <?php
                $rpemda = $this->db->order_by(COL_KD_TAHUN_FROM,'desc')->get(TBL_SAKIP_MPEMDA)->result_array();
                $selected = $period;
                foreach ($rpemda as $p) {
                  ?>
                  <option value="<?=$p[COL_KD_PEMDA]?>" <?=$p[COL_KD_PEMDA]==$selected?'selected':''?>><?=$p[COL_KD_TAHUN_FROM].' s.d '.$p[COL_KD_TAHUN_TO].' - '.$p[COL_NM_PEJABAT]?></option>
                  <?php
                }
                ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-3">Tahun</label>
            <div class="col-sm-2">
              <input type="number" name="<?=COL_KD_TAHUN?>" class="form-control text-right" value="<?=$tahun?>" />
            </div>
            <div class="col-sm-4">
              <button type="submit" class="btn btn-success btn-flat"><i class="fas fa-check"></i>&nbsp;&nbsp;TAMPILKAN</button>
              <a href="<?=site_url('mpemda/capaian')."?Period=$period&Kd_Tahun=$tahun&Cetak=1"?>" target="_blank" class="btn btn-primary btn-flat"><i class="fas fa-print"></i>&nbsp;&nbsp;CETAK</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php
    if(!empty($res)) {
      $eplandb = $this->load->database("eplan", true);
      $subunit_all = $eplandb->get("ref_sub_unit")->result_array();
      ?>
      <div class="box-body">
        <div class="row">
          <div class="col-sm-12" style="overflow-x: scroll">
            <table class="table table-bordered">
              <tbody>
                <tr>
                  <th rowspan="2">No.</th>
                  <th rowspan="2">Sasaran</th>
                  <th rowspan="2" colspan="2">Indikator Kinerja</th>
                  <th colspan="5" style="background-color: #ff851b73">Target</th>
                  <th colspan="5" style="background-color: #01ff7054">Realisasi</th>
                  <th rowspan="2">OPD TERKAIT</th>
                </tr>
                <tr>
                  <th style="background-color: #ff851b73">TW. I</th>
                  <th style="background-color: #ff851b73">TW. II</th>
                  <th style="background-color: #ff851b73">TW. III</th>
                  <th style="background-color: #ff851b73">TW. IV</th>
                  <th style="background-color: #ff851b73">Akhir</th>
                  <th style="background-color: #01ff7054">TW. I</th>
                  <th style="background-color: #01ff7054">TW. II</th>
                  <th style="background-color: #01ff7054">TW. III</th>
                  <th style="background-color: #01ff7054">TW. IV</th>
                  <th style="background-color: #01ff7054">Akhir</th>
                </tr>
                <?php
                $no=0;
                $seq = 1;
                $seq_ = 1;
                foreach($res as $r) {
                  $ropd = $this->db
                  ->where(array(
                    COL_KD_PEMDA=>$r[COL_KD_PEMDA],
                    COL_KD_MISI=>$r[COL_KD_MISI],
                    COL_KD_TUJUAN=>$r[COL_KD_TUJUAN],
                    COL_KD_INDIKATORTUJUAN=>$r[COL_KD_INDIKATORTUJUAN],
                    COL_KD_SASARAN=>$r[COL_KD_SASARAN],
                    COL_KD_INDIKATORSASARAN=>$r[COL_KD_INDIKATORSASARAN],
                  ))
                  ->group_by(array(COL_KD_URUSAN,COL_KD_BIDANG,COL_KD_UNIT,COL_KD_SUB))
                  ->get(TBL_SAKIP_MOPD_TUJUAN)
                  ->result_array();
                  ?>
                  <tr>
                    <?php
                    if($no != $r['ID_Sasaran']) {
                      ?>
                      <td <?=$r['rowspan']>1?'rowspan="'.$r['rowspan'].'"':''?>><?=$seq?></td>
                      <td <?=$r['rowspan']>1?'rowspan="'.$r['rowspan'].'"':''?>><?=$r[COL_NM_SASARAN]?></td>
                      <?php
                      $seq++;
                      $seq_ = 1;
                    }
                    ?>
                    <td><?=$seq_?></td>
                    <td><?=$r[COL_NM_INDIKATORSASARAN]?></td>
                    <?php
                    for($i=1; $i<=4; $i++) {
                      ?>
                      <td class="text-center" style="white-space: nowrap; background-color: #ff851b73">
                        <?=anchor(site_url('mpemda/target/'.$period.'/'.$tahun.'/'.$i.'/'.$r[COL_KD_MISI].'/'.$r[COL_KD_TUJUAN].'/'.$r[COL_KD_INDIKATORTUJUAN].'/'.$r[COL_KD_SASARAN].'/'.$r[COL_KD_INDIKATORSASARAN]),
                        (!empty($r['Target_TW'.$i])?$r['Target_TW'.$i]:'(kosong)'),
                        array('class'=>'link-target','data-val'=>(!empty($r['Target_TW'.$i])?$r['Target_TW'.$i]:''),'data-label'=>'Target TW. '.$i))?>
                      </td>
                      <?php
                    }
                    ?>
                    <td class="text-center" style="white-space: nowrap; background-color: #ff851b73">
                      <?=anchor(site_url('mpemda/target/'.$period.'/'.$tahun.'/-1/'.$r[COL_KD_MISI].'/'.$r[COL_KD_TUJUAN].'/'.$r[COL_KD_INDIKATORTUJUAN].'/'.$r[COL_KD_SASARAN].'/'.$r[COL_KD_INDIKATORSASARAN]),
                      (!empty($r['Target'])?$r['Target']:'(kosong)'),
                      array('class'=>'link-target','data-val'=>(!empty($r['Target'])?$r['Target']:''),'data-label'=>'Target Akhir'))?>
                    </td>
                    <?php
                    for($i=1; $i<=4; $i++) {
                      ?>
                      <td class="text-center" style="white-space: nowrap; background-color: #01ff7054">
                        <?=anchor(site_url('mpemda/realisasi/'.$period.'/'.$tahun.'/'.$i.'/'.$r[COL_KD_MISI].'/'.$r[COL_KD_TUJUAN].'/'.$r[COL_KD_INDIKATORTUJUAN].'/'.$r[COL_KD_SASARAN].'/'.$r[COL_KD_INDIKATORSASARAN]),
                        (!empty($r['Realisasi_TW'.$i])?$r['Realisasi_TW'.$i]:'(kosong)'),
                        array('class'=>'link-realisasi','data-val'=>(!empty($r['Realisasi_TW'.$i])?$r['Realisasi_TW'.$i]:''),'data-label'=>'Realisasi TW. '.$i))?>
                      </td>
                      <?php
                    }
                    ?>
                    <td class="text-center" style="white-space: nowrap; background-color: #01ff7054">
                      <?=anchor(site_url('mpemda/realisasi/'.$period.'/'.$tahun.'/-1/'.$r[COL_KD_MISI].'/'.$r[COL_KD_TUJUAN].'/'.$r[COL_KD_INDIKATORTUJUAN].'/'.$r[COL_KD_SASARAN].'/'.$r[COL_KD_INDIKATORSASARAN]),
                      (!empty($r['Realisasi'])?$r['Realisasi']:'(kosong)'),
                      array('class'=>'link-realisasi','data-val'=>(!empty($r['Realisasi'])?$r['Realisasi']:''),'data-label'=>'Realisasi Akhir'))?>
                    </td>
                    <td style="white-space:nowrap">
                      <ul style="padding-left: 20px">
                        <?php
                        if(count($ropd) == count($subunit_all)) {
                          echo '<li>SEMUA OPD</li>';
                        } else {
                          foreach ($ropd as $opd) {
                            $nmSub = '-';
                            $eplandb->where(COL_KD_URUSAN, $opd[COL_KD_URUSAN]);
                            $eplandb->where(COL_KD_BIDANG, $opd[COL_KD_BIDANG]);
                            $eplandb->where(COL_KD_UNIT, $opd[COL_KD_UNIT]);
                            $eplandb->where(COL_KD_SUB, $opd[COL_KD_SUB]);
                            $subunit = $eplandb->get("ref_sub_unit")->row_array();
                            if($subunit) {
                              $nmSub = $subunit["Nm_Sub_Unit"];
                            }
                            $kdUrusan = $opd[COL_KD_URUSAN];
                            $kdBidang = $opd[COL_KD_BIDANG];
                            $kdUnit = $opd[COL_KD_UNIT];
                            $kdSub = $opd[COL_KD_SUB];
                            echo '<li>'.anchor(site_url('mopd/capaian')."?Period=$period&Kd_Tahun=$tahun&Kd_Urusan=$kdUrusan&Kd_Bidang=$kdBidang&Kd_Unit=$kdUnit&Kd_Sub=$kdSub&Cetak=1",$nmSub,array('target'=>'_blank')).'</li>';
                          }
                        }
                        ?>
                      </ul>
                    </td>
                  </tr>
                  <?php
                  $no = $r['ID_Sasaran'];
                  $seq_++;
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <?php
    }
    ?>

    <?=form_close()?>
  </div>
</section>
<div class="modal fade" id="modalPopUp" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fa fa-close"></i></span>
        </button>
        <h4 class="modal-title">Capaian Tahun <?=$tahun?></h4>
      </div>
      <div class="modal-body">
        <form action="#" method="post">
          <div class="form-group">
            <label>Tahun</label>
            <input type="text" name="VALUE" class="form-control" />
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal"><i class="fas fa-times"></i>&nbsp;Cancel</button>
        <button type="button" class="btn btn-primary btn-flat btn-ok"><i class="fas fa-check"></i>&nbsp;SIMPAN</button>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('loadjs')?>
<script>
$(document).ready(function(){
  $('.link-target, .link-realisasi').click(function() {
    var modal = $('#modalPopUp');
    var label = $(this).data('label');
    var val = $(this).data('val');
    var href = $(this).attr('href');

    modal.modal('show');
    $('label', modal).html(label);
    $('input', modal).val(val);
    $('.btn-ok', modal).unbind().click(function(){
      var dis = $(this);
      dis.attr('disabled', true);

      $('form', modal).ajaxSubmit({
        dataType: 'json',
        url : href,
        success: function(data) {
          if(data.error==0){
            location.reload();
          } else{
            alert(data.error);
          }
        },
        error: function() {
          alert('Server Error.');
        },
        complete: function() {
          dis.attr('disabled', false);
          modal.modal('hide')
        }
      });
    });
    return false;
  });
});
</script>
<?php $this->load->view('footer')?>
