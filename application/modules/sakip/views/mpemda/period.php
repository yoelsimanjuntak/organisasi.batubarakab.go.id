<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 29/09/2018
 * Time: 22:30
 */
?>

<?php $this->load->view('header')?>
<section class="content-header">
    <h1><?= $title ?>  <small>Data</small></h1>
    <ol class="breadcrumb">
        <li>
            <a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a>
        </li>
        <li class="active">
            Periode, Visi & Misi
        </li>
    </ol>
</section>

<section class="content">
    <p>
      <?=anchor('mpemda/period-add','<i class="far fa-plus"></i> TAMBAH',array('class'=>'btn btn-primary'))?>
    </p>
    <?php
    foreach($res as $d) {
      $rmisi = $this->db
      ->where(COL_KD_PEMDA, $d[COL_KD_PEMDA])
      ->order_by(COL_KD_MISI, "asc")
      ->get(TBL_SAKIP_MPMD_MISI)
      ->result_array();
      ?>
      <div class="box box-danger box-solid">
        <div class="box-header">
          <h3 class="box-title text-bold"><?=$d[COL_KD_TAHUN_FROM]." s.d ".$d[COL_KD_TAHUN_TO].": ".$d[COL_NM_PEJABAT]?></h3>
          <div class="box-tools pull-right">
            <a href="<?=site_url('mpemda/period-edit/'.$d[COL_KD_PEMDA])?>" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Edit">
              <i class="fa fa-pencil"></i>
            </a>
            <a href="<?=site_url('mpemda/period-hapus/'.$d[COL_KD_PEMDA])?>" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Hapus">
              <i class="fa fa-trash"></i>
            </a>
          </div>
        </div>
        <div class="box-body" style="padding: 0 !important">
          <table class="table">
            <tbody>
              <tr>
                <td style="width: 100px; white-space: nowrap">VISI</td>
                <td style="width: 10px; white-space: nowrap">:</td>
                <td class="text-bold"><?=$d[COL_NM_VISI]?></td>
              </tr>
              <tr>
                <td style="width: 100px; white-space: nowrap">MISI</td>
                <td style="width: 10px; white-space: nowrap">:</td>
                <td class="text-bold">
                  <?php
                  if(!empty($rmisi)) {
                    ?>
                    <ol>
                      <?php
                      foreach ($rmisi as $m) {
                        echo '<li>'.$m[COL_NM_MISI].'</li>';
                      }
                      ?>
                    </ol>
                    <?php
                  } else {
                    echo '-';
                  }
                  ?>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <?php
    }
    ?>

</section>
<?php $this->load->view('loadjs')?>
<?php $this->load->view('footer')?>
