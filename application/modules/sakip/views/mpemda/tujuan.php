<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 29/09/2018
 * Time: 22:30
 */
?>
<?php $this->load->view('header')
?>
<section class="content-header">
    <h1><?= $title ?>  <small>Data</small></h1>
    <ol class="breadcrumb">
        <li>
            <a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a>
        </li>
        <li class="active">
            Tujuan
        </li>
    </ol>
</section>

<section class="content">
    <p>
        <?=anchor('mpemda/tujuan-del','<i class="far fa-trash"></i> Hapus',array('class'=>'cekboxaction btn btn-danger','confirm'=>'Apa anda yakin?'))?>
        <?=anchor('mpemda/tujuan-add','<i class="far fa-plus"></i> Data Baru',array('class'=>'btn btn-primary'))?>
    </p>
    <div class="box box-danger">
      <div class="box-header with-border">
        <form id="form-filter" action="<?=current_url()?>" method="get" class="form-horizontal">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group" style="margin-bottom: 0 !important">
                <label class="col-sm-2 control-label">PERIODE PEMERINTAHAN</label>
                <div class="col-sm-6">
                  <select name="Periode" class="form-control">
                    <?=GetCombobox("select * from sakip_mpemda order by Kd_Tahun_From desc",COL_KD_PEMDA,array(COL_KD_TAHUN_FROM, COL_NM_PEJABAT), $periode)?>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="box-body" style="padding:0 !important">
        <?php
        $rmisi = $this->db
        ->where(COL_KD_PEMDA, $periode)
        ->order_by(COL_KD_MISI, "asc")
        ->get(TBL_SAKIP_MPMD_MISI)
        ->result_array();
        ?>
        <form id="dataform" method="post" action="#">
          <table class="table table-bordered table-condensed table-responsive" width="100%">
            <tbody>
              <?php
              foreach($rmisi as $m) {
                ?>
                <tr class="bg-gray">
                  <td class="text-right" style="width: 20px; white-space: nowrap">MISI</td>
                  <td class="text-bold" style="width: 20px; white-space: nowrap"><?=$m[COL_KD_MISI]?></td>
                  <td class="text-bold" colspan="2"><?=$m[COL_NM_MISI]?></td>
                </tr>
                <?php
                foreach($res as $d) {
                  if($d[COL_KD_PEMDA]==$m[COL_KD_PEMDA] && $d[COL_KD_MISI]==$m[COL_KD_MISI]) {
                    $riktujuan = $this->db
                    ->where(COL_KD_PEMDA, $d[COL_KD_PEMDA])
                    ->where(COL_KD_MISI, $d[COL_KD_MISI])
                    ->where(COL_KD_TUJUAN, $d[COL_KD_TUJUAN])
                    ->get(TBL_SAKIP_MPMD_IKTUJUAN)
                    ->result_array();
                    ?>
                    <tr>
                      <td class="text-right" style="width: 20px; white-space: nowrap">
                        <input type="checkbox" class="cekbox" name="cekbox[]" value="<?=$d["ID"]?>" />
                      </td>
                      <td style="width: 20px; white-space: nowrap"><?=$d[COL_KD_MISI].'.'.$d[COL_KD_TUJUAN]?></td>
                      <td style="padding-left: 20px !important" colspan="2">
                        <a href="<?=site_url('sakip/mpemda/tujuan-edit/'.$d["ID"])?>"><?=$d[COL_NM_TUJUAN]?></a>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2"></td>
                      <td style="padding-left: 20px !important; width: 50px; white-space: nowrap">Indikator :</td>
                      <td>
                        <?php
                        if(!empty($riktujuan)) {
                          ?>
                          <ol style="padding-inline-start: 25px">
                            <?php
                            foreach ($riktujuan as $ikt) {
                              echo '<li>'.$ikt[COL_NM_INDIKATORTUJUAN].'</li>';
                            }
                            ?>
                          </ol>
                          <?php
                        } else {
                          echo '-';
                        }
                        ?>
                      </td>
                    </tr>
                    <?php
                  }
                }
              }
              ?>
            </tbody>
          </table>
        </form>
          <!--<form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered table-hover">

              </table>
          </form>-->
      </div>
    </div>
</section>
<?php $this->load->view('loadjs')?>
<script type="text/javascript">
$(document).ready(function(){
  $('[name=Periode]').change(function() {
    $(this).closest('form').submit();
  });
});
</script>
<?php $this->load->view('footer')?>
