<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 30/09/2018
 * Time: 03:26
 */
$data = array();
$i = 0;
foreach ($res as $d) {
    /*$res[$i] = array(
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d["ID"] . '" />',
        $d[COL_KD_TAHUN_FROM]." s.d ".$d[COL_KD_TAHUN_TO],
        //$d[COL_KD_MISI]." ".$d[COL_NM_MISI],
        //$d[COL_KD_MISI].".".$d[COL_KD_TUJUAN]." ".$d[COL_NM_TUJUAN],
        $d[COL_KD_MISI].".".$d[COL_KD_TUJUAN]."<br />".$d[COL_NM_TUJUAN],
        $d[COL_KD_MISI].".".$d[COL_KD_TUJUAN].".".$d[COL_KD_INDIKATORTUJUAN]."<br />".$d[COL_NM_INDIKATORTUJUAN],
        anchor('mpemda/sasaran-edit/'.$d["ID"],$d[COL_KD_MISI].".".$d[COL_KD_TUJUAN].".".$d[COL_KD_INDIKATORTUJUAN].".".$d[COL_KD_SASARAN]."<br />".$d[COL_NM_SASARAN])
    );
    $i++;*/
}
$data = json_encode($res);
?>

<?php $this->load->view('header')
?>
<section class="content-header">
    <h1><?= $title ?>  <small>Data</small></h1>
    <ol class="breadcrumb">
        <li>
            <a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a>
        </li>
        <li class="active">
            Sasaran
        </li>
    </ol>
</section>
<section class="content">
    <p>
        <?=anchor('mpemda/sasaran-del','<i class="far fa-trash"></i> Hapus',array('class'=>'cekboxaction btn btn-danger btn-sm','confirm'=>'Apa anda yakin?'))
        ?>
        <?=anchor('mpemda/sasaran-add','<i class="far fa-plus"></i> Data Baru',array('class'=>'btn btn-primary btn-sm'))
        ?>
    </p>
    <div class="box box-danger">
      <div class="box-header with-border">
        <form id="form-filter" action="<?=current_url()?>" method="get" class="form-horizontal">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group" style="margin-bottom: 0 !important">
                <label class="col-sm-2 control-label">PERIODE PEMERINTAHAN</label>
                <div class="col-sm-6">
                  <select name="Periode" class="form-control">
                    <?=GetCombobox("select * from sakip_mpemda order by Kd_Tahun_From desc",COL_KD_PEMDA,array(COL_KD_TAHUN_FROM, COL_NM_PEJABAT), $periode)?>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="box-body" style="padding:0 !important">
        <?php
        $rmisi = $this->db
        ->where(COL_KD_PEMDA, $periode)
        ->order_by(COL_KD_MISI, "asc")
        ->get(TBL_SAKIP_MPMD_MISI)
        ->result_array();
        ?>
        <form id="dataform" method="post" action="#">
          <table class="table table-bordered table-condensed table-responsive" width="100%">
            <tbody>
              <?php
              foreach($rmisi as $m) {
                $rtujuan = $this->db
                ->where(COL_KD_PEMDA, $m[COL_KD_PEMDA])
                ->where(COL_KD_MISI, $m[COL_KD_MISI])
                ->get(TBL_SAKIP_MPMD_TUJUAN)
                ->result_array();
                ?>
                <tr>
                  <td class="text-right text-sm" style="width: 20px; white-space: nowrap">MISI</td>
                  <td class="text-bold" style="width: 20px; white-space: nowrap"><?=$m[COL_KD_MISI]?></td>
                  <td class="text-bold" colspan="2"><?=$m[COL_NM_MISI]?></td>
                </tr>
                <?php
                foreach($rtujuan as $t) {
                  $riktujuan = $this->db
                  ->where(COL_KD_PEMDA, $t[COL_KD_PEMDA])
                  ->where(COL_KD_MISI, $t[COL_KD_MISI])
                  ->where(COL_KD_TUJUAN, $t[COL_KD_TUJUAN])
                  ->get(TBL_SAKIP_MPMD_IKTUJUAN)
                  ->result_array();
                  ?>
                  <tr>
                    <td class="text-right text-sm" style="width: 20px; white-space: nowrap">TUJUAN</td>
                    <td style="width: 20px; white-space: nowrap"><?=$t[COL_KD_MISI].'.'.$t[COL_KD_TUJUAN]?></td>
                    <td style="padding-left: 20px !important" colspan="2"><?=$t[COL_NM_TUJUAN]?></td>
                  </tr>
                  <?php
                  foreach($riktujuan as $ikt) {
                    ?>
                    <tr>
                      <td class="text-right text-sm" style="width: 20px; white-space: nowrap">INDIKATOR TUJUAN</td>
                      <td style="width: 20px; white-space: nowrap"><?=$ikt[COL_KD_MISI].'.'.$ikt[COL_KD_TUJUAN].'.'.$ikt[COL_KD_INDIKATORTUJUAN]?></td>
                      <td style="padding-left: 40px !important" colspan="2"><?=$ikt[COL_NM_INDIKATORTUJUAN]?></td>
                    </tr>
                    <?php
                    foreach($res as $s) {
                      if($s[COL_KD_PEMDA]==$ikt[COL_KD_PEMDA] && $s[COL_KD_MISI]==$ikt[COL_KD_MISI] && $s[COL_KD_TUJUAN]==$ikt[COL_KD_TUJUAN] && $s[COL_KD_INDIKATORTUJUAN]==$ikt[COL_KD_INDIKATORTUJUAN]) {
                        $riksasaran = $this->db
                        ->where(COL_KD_PEMDA, $s[COL_KD_PEMDA])
                        ->where(COL_KD_MISI, $s[COL_KD_MISI])
                        ->where(COL_KD_TUJUAN, $s[COL_KD_TUJUAN])
                        ->where(COL_KD_INDIKATORTUJUAN, $s[COL_KD_INDIKATORTUJUAN])
                        ->where(COL_KD_SASARAN, $s[COL_KD_SASARAN])
                        ->get(TBL_SAKIP_MPMD_IKSASARAN)
                        ->result_array();
                        ?>
                        <tr>
                          <td class="text-right" style="width: 20px; white-space: nowrap">
                            <input type="checkbox" class="cekbox" name="cekbox[]" value="<?=$s["ID"]?>" />
                          </td>
                          <td style="width: 20px; white-space: nowrap"><?=$s[COL_KD_MISI].'.'.$s[COL_KD_TUJUAN].'.'.$s[COL_KD_INDIKATORTUJUAN].'.'.$s[COL_KD_SASARAN]?></td>
                          <td style="padding-left: 60px !important" colspan="2">
                            <a href="<?=site_url('sakip/mpemda/sasaran-edit/'.$s["ID"])?>"><?=$s[COL_NM_SASARAN]?></a>
                          </td>
                        </tr>
                        <tr>
                          <td colspan="2"></td>
                          <td style="padding-left: 60px !important; width: 50px; white-space: nowrap">Indikator :</td>
                          <td>
                            <?php
                            if(!empty($riksasaran)) {
                              ?>
                              <ol style="padding-inline-start: 25px; margin-bottom: 0">
                                <?php
                                foreach ($riksasaran as $iks) {
                                  echo '<li>'.$iks[COL_NM_INDIKATORSASARAN].'</li>';
                                }
                                ?>
                              </ol>
                              <?php
                            } else {
                              echo '-';
                            }
                            ?>
                          </td>
                        </tr>
                        <?php
                      }
                    }
                  }
                }
              }
              ?>
            </tbody>
          </table>
        </form>
      </div>
    </div>
</section>
<?php $this->load->view('loadjs')?>
<script type="text/javascript">
$(document).ready(function(){
  $('[name=Periode]').change(function() {
    $(this).closest('form').submit();
  });
});
</script>
<?php $this->load->view('footer')?>
