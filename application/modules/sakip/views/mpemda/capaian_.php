<html>
<head>
  <style>
  body {
    font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
  }
  th, td {
    padding: 5px;
  }
  </style>
</head>
<body>
  <h3 style="text-align: center; text-transform: uppercase; margin-bottom: 0">
    <?=$title?><br />
    PEMERINTAH KABUPATEN BATU BARA
  </h3>
  <hr style="margin-bottom: 1.5rem" />
  <table style="border: 1px solid #000; border-spacing: 0" border="1" width="100%">
    <tbody>
      <tr>
        <th rowspan="2">No.</th>
        <th rowspan="2">Sasaran</th>
        <th rowspan="2" colspan="2">Indikator Kinerja</th>
        <th colspan="5">Target</th>
        <th colspan="5">Realisasi</th>
        <th rowspan="2">OPD TERKAIT</th>
      </tr>
      <tr>
        <th style="white-space: nowrap">TW. I</th>
        <th style="white-space: nowrap">TW. II</th>
        <th style="white-space: nowrap">TW. III</th>
        <th style="white-space: nowrap">TW. IV</th>
        <th style="white-space: nowrap">Akhir</th>
        <th style="white-space: nowrap">TW. I</th>
        <th style="white-space: nowrap">TW. II</th>
        <th style="white-space: nowrap">TW. III</th>
        <th style="white-space: nowrap">TW. IV</th>
        <th style="white-space: nowrap">Akhir</th>
      </tr>
      <?php
      $no=0;
      $seq = 1;
      $seq_ = 1;
      $eplandb = $this->load->database("eplan", true);
      foreach($res as $r) {
        $ropd = $this->db
        ->where(array(
          COL_KD_PEMDA=>$r[COL_KD_PEMDA],
          COL_KD_MISI=>$r[COL_KD_MISI],
          COL_KD_TUJUAN=>$r[COL_KD_TUJUAN],
          COL_KD_INDIKATORTUJUAN=>$r[COL_KD_INDIKATORTUJUAN],
          COL_KD_SASARAN=>$r[COL_KD_SASARAN],
          COL_KD_INDIKATORSASARAN=>$r[COL_KD_INDIKATORSASARAN],
        ))
        ->group_by(array(COL_KD_URUSAN,COL_KD_BIDANG,COL_KD_UNIT,COL_KD_SUB))
        ->get(TBL_SAKIP_MOPD_TUJUAN)
        ->result_array();
        ?>
        <tr>
          <?php
          if($no != $r['ID_Sasaran']) {
            ?>
            <td <?=$r['rowspan']>1?'rowspan="'.$r['rowspan'].'"':''?>><?=$seq?></td>
            <td <?=$r['rowspan']>1?'rowspan="'.$r['rowspan'].'"':''?>><?=$r[COL_NM_SASARAN]?></td>
            <?php
            $seq++;
            $seq_ = 1;
          }
          ?>
          <td><?=$seq_?></td>
          <td><?=$r[COL_NM_INDIKATORSASARAN]?></td>
          <?php
          for($i=1; $i<=4; $i++) {
            ?>
            <td style="text-align: center; white-space: nowrap; background-color: #ff851b73">
              <?=(!empty($r['Target_TW'.$i])?$r['Target_TW'.$i]:'-')?>
            </td>
            <?php
          }
          ?>
          <td style="text-align: center; white-space: nowrap; background-color: #ff851b73">
            <?=(!empty($r['Target'])?$r['Target']:'-')?>
          </td>
          <?php
          for($i=1; $i<=4; $i++) {
            ?>
            <td style="text-align: center; white-space: nowrap; background-color: #01ff7054">
              <?=(!empty($r['Realisasi_TW'.$i])?$r['Realisasi_TW'.$i]:'-')?>
            </td>
            <?php
          }
          ?>
          <td style="text-align: center; white-space: nowrap; background-color: #01ff7054">
            <?=(!empty($r['Realisasi'])?$r['Realisasi']:'-')?>
          </td>
          <td style="white-space:nowrap">
            <ul>
              <?php
              foreach ($ropd as $opd) {
                $nmSub = '-';
                $eplandb->where(COL_KD_URUSAN, $opd[COL_KD_URUSAN]);
                $eplandb->where(COL_KD_BIDANG, $opd[COL_KD_BIDANG]);
                $eplandb->where(COL_KD_UNIT, $opd[COL_KD_UNIT]);
                $eplandb->where(COL_KD_SUB, $opd[COL_KD_SUB]);
                $subunit = $eplandb->get("ref_sub_unit")->row_array();
                if($subunit) {
                  $nmSub = $subunit["Nm_Sub_Unit"];
                }
                echo '<li>'.$nmSub.'</li>';
              }
              ?>
            </ul>
          </td>
        </tr>
        <?php
        $no = $r['ID_Sasaran'];
        $seq_++;
      }
      ?>
    </tbody>
  </table>
</body>
