<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 3/14/2019
 * Time: 10:26 PM
 */
$data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d[COL_UNIQ] . '" />',
        anchor('sakip/master/uom-edit/'.$d[COL_UNIQ],$d[COL_KD_SATUAN]),
        $d[COL_NM_SATUAN],
        $d[COL_CREATE_BY],
        !empty($d[COL_CREATE_DATE]) ? date("d-m-Y H:i:s", strtotime($d[COL_CREATE_DATE])) : "",
        $d[COL_EDIT_BY],
        !empty($d[COL_EDIT_DATE]) ? date("d-m-Y H:i:s", strtotime($d[COL_EDIT_DATE])) : ""
    );
    $i++;
}
$data = json_encode($res);
?>
<?php $this->load->view('header')
?>
    <section class="content-header">
        <h1><?= $title ?>  <small>Data</small></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="active">
                Satuan
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <p>
            <?=anchor('sakip/master/uom-del','<i class="far fa-trash"></i> Hapus',array('class'=>'cekboxaction btn btn-danger btn-sm','confirm'=>'Apa anda yakin?'))
            ?>
            <?=anchor('sakip/master/uom-add','<i class="far fa-plus"></i> Data Baru',array('class'=>'btn btn-primary btn-sm'))
            ?>
        </p>
        <div class="box box-default">
            <div class="box-body">
                <form id="dataform" method="post" action="#">
                    <table id="datalist" class="table table-bordered table-hover">

                    </table>
                </form>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs')?>
    <script type="text/javascript">
        $(document).ready(function() {
            var dataTable = $('#datalist').dataTable({
                //"sDom": "Rlfrtip",
                "aaData": <?=$data?>,
                //"bJQueryUI": true,
                "aaSorting" : [[1,'asc']],
                "scrollY" : 400,
                "scrollX": "200%",
                "iDisplayLength": 100,
                "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
                "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
                "columnDefs": [{"targets":[0],"className":"text-center"}],
                "aoColumns": [
                    {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />","sWidth":15,bSortable:false},
                    {"sTitle": "Kode"},
                    {"sTitle": "Nama"},
                    {"sTitle": "Dibuat Oleh"},
                    {"sTitle": "Dibuat Pada"},
                    {"sTitle": "Diubah Oleh"},
                    {"sTitle": "Diubah Pada"}
                ]
            });
            $('#cekbox').click(function(){
                if($(this).is(':checked')){
                    $('.cekbox').prop('checked',true);
                    console.log('clicked');
                }else{
                    $('.cekbox').prop('checked',false);
                }
            });
        });
    </script>

<?php $this->load->view('footer')
?>
