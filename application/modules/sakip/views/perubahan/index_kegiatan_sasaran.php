<table id="tbl-sasaran" class="table table-hover">
  <thead>
    <th>Sasaran / Indikator</th>
    <th>Aksi</th>
  </thead>
  <tbody>
    <?php
    if(count($sasaran) <= 0) {
      echo '<tr><td colspan="2">Tidak ada data.</td></tr>';
    } else {
      foreach ($sasaran as $sas) {
        $rindikator = $this->db
        ->where(array(
          TBL_SAKIP_PRENJA_KEGIATAN_INDIKATOR.'.'.COL_KD_TAHUN=>$sas[COL_KD_TAHUN],
          TBL_SAKIP_PRENJA_KEGIATAN_INDIKATOR.'.'.COL_KD_URUSAN=>$sas[COL_KD_URUSAN],
          TBL_SAKIP_PRENJA_KEGIATAN_INDIKATOR.'.'.COL_KD_BIDANG=>$sas[COL_KD_BIDANG],
          TBL_SAKIP_PRENJA_KEGIATAN_INDIKATOR.'.'.COL_KD_UNIT=>$sas[COL_KD_UNIT],
          TBL_SAKIP_PRENJA_KEGIATAN_INDIKATOR.'.'.COL_KD_SUB=>$sas[COL_KD_SUB],

          TBL_SAKIP_PRENJA_KEGIATAN_INDIKATOR.'.'.COL_KD_PEMDA=>$sas[COL_KD_PEMDA],
          TBL_SAKIP_PRENJA_KEGIATAN_INDIKATOR.'.'.COL_KD_MISI=>$sas[COL_KD_MISI],
          TBL_SAKIP_PRENJA_KEGIATAN_INDIKATOR.'.'.COL_KD_TUJUAN=>$sas[COL_KD_TUJUAN],
          TBL_SAKIP_PRENJA_KEGIATAN_INDIKATOR.'.'.COL_KD_INDIKATORTUJUAN=>$sas[COL_KD_INDIKATORTUJUAN],
          TBL_SAKIP_PRENJA_KEGIATAN_INDIKATOR.'.'.COL_KD_SASARAN=>$sas[COL_KD_SASARAN],
          TBL_SAKIP_PRENJA_KEGIATAN_INDIKATOR.'.'.COL_KD_INDIKATORSASARAN=>$sas[COL_KD_INDIKATORSASARAN],
          TBL_SAKIP_PRENJA_KEGIATAN_INDIKATOR.'.'.COL_KD_TUJUANOPD=>$sas[COL_KD_TUJUANOPD],
          TBL_SAKIP_PRENJA_KEGIATAN_INDIKATOR.'.'.COL_KD_INDIKATORTUJUANOPD=>$sas[COL_KD_INDIKATORTUJUANOPD],
          TBL_SAKIP_PRENJA_KEGIATAN_INDIKATOR.'.'.COL_KD_SASARANOPD=>$sas[COL_KD_SASARANOPD],
          TBL_SAKIP_PRENJA_KEGIATAN_INDIKATOR.'.'.COL_KD_INDIKATORSASARANOPD=>$sas[COL_KD_INDIKATORSASARANOPD],
          TBL_SAKIP_PRENJA_KEGIATAN_INDIKATOR.'.'.COL_KD_PROGRAMOPD=>$sas[COL_KD_PROGRAMOPD],
          TBL_SAKIP_PRENJA_KEGIATAN_INDIKATOR.'.'.COL_KD_SASARANPROGRAMOPD=>$sas[COL_KD_SASARANPROGRAMOPD],
          TBL_SAKIP_PRENJA_KEGIATAN_INDIKATOR.'.'.COL_KD_SUBBID=>$sas[COL_KD_SUBBID],
          TBL_SAKIP_PRENJA_KEGIATAN_INDIKATOR.'.'.COL_KD_KEGIATANOPD=>$sas[COL_KD_KEGIATANOPD],
          TBL_SAKIP_PRENJA_KEGIATAN_INDIKATOR.'.'.COL_KD_SASARANKEGIATANOPD=>$sas[COL_KD_SASARANKEGIATANOPD]
        ))
        ->order_by(TBL_SAKIP_PRENJA_KEGIATAN_INDIKATOR.'.'.COL_KD_INDIKATORKEGIATANOPD, 'asc')
        ->get(TBL_SAKIP_PRENJA_KEGIATAN_INDIKATOR)
        ->result_array();
        ?>
        <tr data-tt-id="<?=$sas[COL_KD_SASARANKEGIATANOPD]?>">
          <td><strong><?=$sas[COL_KD_SASARANKEGIATANOPD]?></strong>: <?=$sas[COL_NM_SASARANKEGIATANOPD]?></td>
          <td style="width: 5vw">
            <?php
            if(empty($dpa)) {
              ?>
              <a href="<?=site_url('sakip/perubahan/hapus-sasaran-kegiatan/'.$sas[COL_UNIQ])?>" class="btn btn-xs btn-danger btn-hapus-sasaran-kegiatan" style="padding: 2px 5px; font-size: 10px"><i class="fas fa-trash"></i>&nbsp;HAPUS</a>
              <?php
            } else {
              ?>
              -
              <?php
            }
            ?>
          </td>
        </tr>
        <?php
        foreach ($rindikator as $ik) {
          ?>
          <tr data-tt-id="<?=$ik[COL_KD_SASARANKEGIATANOPD].'.'.$ik[COL_KD_INDIKATORKEGIATANOPD]?>" data-tt-parent-id="<?=$ik[COL_KD_SASARANKEGIATANOPD]?>">
            <td><strong><?=$ik[COL_KD_SASARANKEGIATANOPD].'.'.$ik[COL_KD_INDIKATORKEGIATANOPD]?></strong>: <?=$ik[COL_NM_INDIKATORKEGIATANOPD]?></td>
            <td style="width: 5vw">-</td>
          </tr>
          <?php
        }
      }
    }
    ?>
  </tbody>
</table>
<script>
$(document).ready(function() {
  $('#tbl-sasaran').treetable({ expandable: true, clickableNodeNames: true });
  $('.btn-hapus-sasaran-kegiatan', $('#tbl-sasaran')).click(function() {
    var btn = $(this);
    if (confirm("Apakah anda yakin?") == true) {
      $.post(btn.attr('href'), function(data) {
        if(data.error==0){
          btn.closest('.modal-body').trigger('reload');
        } else {
          alert('Server Error');
        }
      }, 'json').fail(function() {
        alert('Response Error');
      });
    }
    return false;
  });
});
</script>
