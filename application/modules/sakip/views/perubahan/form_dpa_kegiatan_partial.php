<?php
$cond = array(
  COL_KD_PEMDA=>$program[COL_KD_PEMDA],
  COL_KD_TAHUN=>$program[COL_KD_TAHUN],
  COL_KD_URUSAN=>$program[COL_KD_URUSAN],
  COL_KD_BIDANG=>$program[COL_KD_BIDANG],
  COL_KD_UNIT=>$program[COL_KD_UNIT],
  COL_KD_SUB=>$program[COL_KD_SUB],
  COL_KD_MISI=>$program[COL_KD_MISI],
  COL_KD_TUJUAN=>$program[COL_KD_TUJUAN],
  COL_KD_INDIKATORTUJUAN=>$program[COL_KD_INDIKATORTUJUAN],
  COL_KD_SASARAN=>$program[COL_KD_SASARAN],
  COL_KD_INDIKATORSASARAN=>$program[COL_KD_INDIKATORSASARAN],
  COL_KD_TUJUANOPD=>$program[COL_KD_TUJUANOPD],
  COL_KD_INDIKATORTUJUANOPD=>$program[COL_KD_INDIKATORTUJUANOPD],
  COL_KD_SASARANOPD=>$program[COL_KD_SASARANOPD],
  COL_KD_INDIKATORSASARANOPD=>$program[COL_KD_INDIKATORSASARANOPD],
  COL_KD_BID=>$program[COL_KD_BID],
  COL_KD_PROGRAMOPD=>$program[COL_KD_PROGRAMOPD]
);
$rsasaranprog = $this->db
->where($cond)
->order_by(COL_KD_SASARANPROGRAMOPD, 'asc')
->get(TBL_SAKIP_PDPA_PROGRAM_SASARAN)
->result_array();
$qsasaranprog = $this->db->last_query();
?>
<?=form_open(current_url(),array('role'=>'form','id'=>'form-program', 'class'=>'form-horizontal'))?>
<div class="form-group">
  <label class="control-label col-sm-3">Sasaran Program</label>
  <div class="col-sm-8">
    <select name="<?=COL_KD_SASARANPROGRAMOPD?>" class="form-control" style="width: 100%" required <?=!empty($dpa)?'disabled':''?>>
      <?=GetCombobox($qsasaranprog, COL_KD_SASARANPROGRAMOPD, COL_NM_SASARANPROGRAMOPD, (!empty($data)?$data[COL_KD_SASARANPROGRAMOPD]:null))?>
    </select>
  </div>
</div>
<div class="form-group">
  <label class="control-label col-sm-3">Sub Bidang</label>
  <div class="col-sm-8">
    <select name="<?=COL_KD_SUBBID?>" class="form-control" style="width: 100%" required <?=!empty($dpa)?'disabled':''?>>
      <?=GetCombobox("select * from sakip_msubbid where Kd_Urusan = $kdUrusan and Kd_Bidang = $kdBidang and Kd_Unit = $kdUnit and Kd_Sub = $kdSub and Kd_Bid = $kdBid order by Nm_Subbid", COL_KD_SUBBID, COL_NM_SUBBID, (!empty($data)?$data[COL_KD_SUBBID]:null))?>
    </select>
  </div>
</div>
<div class="form-group">
  <div class="col-sm-8 col-sm-offset-3">
    <div class="checkbox">
      <label>
        <input type="checkbox" name="<?=COL_IS_RENJA?>" <?=!empty($data)?($data[COL_IS_RENJA] ? "checked" : "") : "checked"?>>
        Import dari Renja
      </label>
    </div>
  </div>
</div>
<div class="form-group is-eplan">
  <label class="control-label col-sm-3">Kegiatan (Renja)</label>
  <div class="col-sm-8">
    <select name="<?=COL_KD_KEGIATANOPD?>" class="form-control" style="width: 100%">
      <?php
      if(!empty($data)) {
        $kdSubbid = $data[COL_KD_SUBBID];
        $qcond = '';
        foreach ($cond as $key => $value) {
          $qcond .= "and ".$key." = '$value' ";
        }
        echo GetCombobox("select * from sakip_pdpa_kegiatan where 1=1 $qcond and Kd_Subbid = $kdSubbid order by Nm_KegiatanOPD", COL_KD_KEGIATANOPD, COL_NM_KEGIATANOPD, (!empty($data)?$data[COL_KD_KEGIATANOPD]:null));
      }
      ?>
    </select>
    <p class="help-block">Import dari Renja akan <strong>otomatis</strong> mengisi sasaran dan indikator sesuai Dokumen Renja.</p>
  </div>
</div>
<div class="form-group is-not-eplan">
  <label class="control-label col-sm-3">Kode Kegiatan</label>
  <div class="col-sm-4">
    <input type="number" placeholder="Kode" class="form-control" name="<?=COL_KD_KEGIATANOPD?>" value="<?=!empty($data)?$data[COL_KD_KEGIATANOPD]:''?>" required <?=!empty($dpa)?'disabled':''?> />
  </div>
</div>
<div class="form-group is-not-eplan">
  <label class="control-label col-sm-3">Nama Kegiatan</label>
  <div class="col-sm-8">
    <input type="text" placeholder="Nama" class="form-control" name="<?=COL_NM_KEGIATANOPD?>" value="<?=!empty($data)?$data[COL_NM_KEGIATANOPD]:''?>" required <?=!empty($dpa)?'disabled':''?> />
  </div>
</div>
<div class="form-group">
  <label class="control-label col-sm-3">Sumber Dana</label>
  <div class="col-sm-2">
    <select name="<?=COL_KD_SUMBERDANA?>" class="form-control">
      <?=GetCombobox("SELECT * FROM ".TBL_SAKIP_MSUMBERDANA." ORDER BY ".COL_NM_SUMBERDANA, COL_KD_SUMBERDANA, COL_NM_SUMBERDANA, (!empty($data) ? $data[COL_KD_SUMBERDANA] : null))?>
    </select>
  </div>
</div>
<div class="form-group">
  <label class="control-label col-sm-3">Budget</label>
  <div class="col-sm-3" id="div_pagu">
    <input type="text" class="form-control money" placeholder="Budget" name="<?=COL_BUDGET?>" value="<?= !empty($data) ? $data[COL_BUDGET] : ""?>" style="text-align: right" required />
  </div>
</div>
<div class="row" style="padding-top: 10px; border-top: 1px solid #f4f4f4">
  <div class="col-sm-12 text-center">
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fas fa-times"></i>&nbsp;CLOSE</button>&nbsp;
    <button type="submit" class="btn btn-success"><i class="fas fa-check"></i>&nbsp;SUBMIT</button>
  </div>
</div>
<?=form_close()?>
<script type="text/javascript">
$(document).ready(function() {
  $("[name=Is_Renja]").change(function() {
    if($(this).is(":checked")) {
      $('input, select', $('.is-eplan')).attr('disabled', false);
      $('.is-eplan').show();
      $('input, select', $('.is-not-eplan')).attr('disabled', true);
      $('.is-not-eplan').hide();
    }
    else {
      $('input, select', $('.is-eplan')).attr('disabled', true);
      $('.is-eplan').hide();
      $('input, select', $('.is-not-eplan')).attr('disabled', false);
      $('.is-not-eplan').show();
    }
  }).trigger("change");

  $("[name=Kd_Subbid],[Name=Kd_SasaranProgramOPD]").change(function() {
    var subbid = $("[name=Kd_Subbid]").val();
    var sasaran = $("[name=Kd_SasaranProgramOPD]").val();
    var key_ = '<?=$id?>'+'.'+sasaran+'.'+subbid;

    $('[name=Kd_KegiatanOPD]').load('<?=site_url("sakip/ajax/get-opt-prenja-kegiatan")?>', {key: key_}, function() {

    });
  });
  <?php
    if(empty($data)) {
      echo '$("[name=Kd_Subbid]").trigger("change")';
    }
  ?>
});
</script>
