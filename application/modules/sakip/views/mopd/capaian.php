<?php $this->load->view('header')?>
<?php
$ruser = GetLoggedUser();
$kdUrusan = $this->input->get(COL_KD_URUSAN);
$kdBidang = $this->input->get(COL_KD_BIDANG);
$kdUnit = $this->input->get(COL_KD_UNIT);
$kdSub = $this->input->get(COL_KD_SUB);
?>
<section class="content-header">
  <h1><?= $title ?></h1>
  <ol class="breadcrumb">
    <li>
      <a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a>
    </li>
    <li class="active"><?=$title?></li>
  </ol>
</section>
<section class="content">
  <div class="box box-default">
    <?=form_open(current_url(),array('role'=>'form', 'method'=>'get','id'=>'form-filter','class'=>'form-horizontal'))?>
    <div class="box-header with-border">
      <div class="row">
        <div class="col-sm-12">
          <div class="form-group">
            <label class="control-label col-sm-3">Periode</label>
            <div class="col-sm-6">
              <select name="Period" class="form-control no-clear">
                <?php
                $rpemda = $this->db->order_by(COL_KD_TAHUN_FROM,'desc')->get(TBL_SAKIP_MPEMDA)->result_array();
                $selected = $period;
                foreach ($rpemda as $p) {
                  ?>
                  <option value="<?=$p[COL_KD_PEMDA]?>" <?=$p[COL_KD_PEMDA]==$selected?'selected':''?>><?=$p[COL_KD_TAHUN_FROM].' s.d '.$p[COL_KD_TAHUN_TO].' - '.$p[COL_NM_PEJABAT]?></option>
                  <?php
                }
                ?>
              </select>
            </div>
          </div>
          <div class="form-group">
              <label class="control-label col-sm-3">OPD</label>
              <div class="col-sm-6">
                  <div class="input-group">
                      <?php
                      $nmSub = "";
                      $strOPD = explode('.', $ruser[COL_COMPANYID]);
                      if($ruser[COL_ROLEID] != ROLEADMIN) {
                        $kdUrusan = $strOPD[0];
                        $kdBidang = $strOPD[1];
                        $kdUnit = $strOPD[2];
                        $kdSub = $strOPD[3];
                      }
                      $eplandb = $this->load->database("eplan", true);
                      $eplandb->where(COL_KD_URUSAN, $kdUrusan);
                      $eplandb->where(COL_KD_BIDANG, $kdBidang);
                      $eplandb->where(COL_KD_UNIT, $kdUnit);
                      $eplandb->where(COL_KD_SUB, $kdSub);
                      $subunit = $eplandb->get("ref_sub_unit")->row_array();
                      if($subunit) {
                          $nmSub = $subunit["Nm_Sub_Unit"];
                      }
                      ?>
                      <input type="text" class="form-control" name="text-opd" value="<?=$nmSub?>" disabled />
                      <input type="hidden" name="<?=COL_KD_URUSAN?>" value="<?=$kdUrusan?>" required />
                      <input type="hidden" name="<?=COL_KD_BIDANG?>" value="<?=$kdBidang?>" required />
                      <input type="hidden" name="<?=COL_KD_UNIT?>" value="<?=$kdUnit?>" required />
                      <input type="hidden" name="<?=COL_KD_SUB?>" value="<?=$kdSub?>" required />
                      <div class="input-group-btn">
                          <button type="button" class="btn btn-default btn-flat btn-browse-opd" data-toggle="modal" data-target="#browseOPD" data-toggle="tooltip" data-placement="top" title="Pilih OPD" <?=($ruser[COL_ROLEID] != ROLEADMIN ? "disabled" : "")?>><i class="fa fa-ellipsis-h"></i></button>
                      </div>
                  </div>
              </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-3">Tahun</label>
            <div class="col-sm-2">
              <input type="number" name="<?=COL_KD_TAHUN?>" class="form-control text-right" value="<?=$tahun?>" />
            </div>
            <div class="col-sm-4">
              <button type="submit" class="btn btn-success btn-flat"><i class="fas fa-check"></i>&nbsp;&nbsp;TAMPILKAN</button>
              <a href="<?=site_url('mopd/capaian')."?Period=$period&Kd_Tahun=$tahun&Kd_Urusan=$kdUrusan&Kd_Bidang=$kdBidang&Kd_Unit=$kdUnit&Kd_Sub=$kdSub&Cetak=1"?>" target="_blank" class="btn btn-primary btn-flat"><i class="fas fa-print"></i>&nbsp;&nbsp;CETAK</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php
    if(!empty($res)) {
      ?>
      <div class="box-body">
        <div class="row">
          <div class="col-sm-12" style="overflow-x: scroll">
            <table class="table table-bordered">
              <tbody>
                <tr>
                  <th rowspan="2">No.</th>
                  <th rowspan="2">Sasaran</th>
                  <th rowspan="2" colspan="2">Indikator Kinerja</th>
                  <th colspan="5" style="background-color: #ff851b73">Target</th>
                  <th colspan="5" style="background-color: #01ff7054">Realisasi</th>
                </tr>
                <tr>
                  <th style="background-color: #ff851b73">TW. I</th>
                  <th style="background-color: #ff851b73">TW. II</th>
                  <th style="background-color: #ff851b73">TW. III</th>
                  <th style="background-color: #ff851b73">TW. IV</th>
                  <th style="background-color: #ff851b73">Akhir</th>
                  <th style="background-color: #01ff7054">TW. I</th>
                  <th style="background-color: #01ff7054">TW. II</th>
                  <th style="background-color: #01ff7054">TW. III</th>
                  <th style="background-color: #01ff7054">TW. IV</th>
                  <th style="background-color: #01ff7054">Akhir</th>
                </tr>
                <?php
                $no=0;
                $seq = 1;
                $seq_ = 1;
                foreach($res as $r) {
                  ?>
                  <tr>
                    <?php
                    if($no != $r['ID_Sasaran']) {
                      ?>
                      <td <?=$r['rowspan']>1?'rowspan="'.$r['rowspan'].'"':''?>><?=$seq?></td>
                      <td <?=$r['rowspan']>1?'rowspan="'.$r['rowspan'].'"':''?>><?=$r[COL_NM_SASARANOPD]?></td>
                      <?php
                      $seq++;
                      $seq_ = 1;
                    }
                    ?>
                    <td><?=$seq_?></td>
                    <td><?=nl2br($r[COL_NM_INDIKATORSASARANOPD])?></td>
                    <?php
                    for($i=1; $i<=4; $i++) {
                      ?>
                      <td class="text-center" style="white-space: nowrap; background-color: #ff851b73">
                        <?=anchor(site_url('mopd/target/'.$period.'/'.$tahun.'/'.$i),
                        (!empty($r['Target_TW'.$i])?nl2br($r['Target_TW'.$i]):'(kosong)'),
                        array(
                          'class'=>'link-target',
                          'data-val'=>(!empty($r['Target_TW'.$i])?$r['Target_TW'.$i]:''),
                          'data-label'=>'Target TW. '.$i,
                          'data-misi'=>$r[COL_KD_MISI],
                          'data-tujuan'=>$r[COL_KD_TUJUAN],
                          'data-iktujuan'=>$r[COL_KD_INDIKATORTUJUAN],
                          'data-sasaran'=>$r[COL_KD_SASARAN],
                          'data-iksasaran'=>$r[COL_KD_INDIKATORSASARAN],
                          'data-tujuanopd'=>$r[COL_KD_TUJUANOPD],
                          'data-iktujuanopd'=>$r[COL_KD_INDIKATORTUJUANOPD],
                          'data-sasaranopd'=>$r[COL_KD_SASARANOPD],
                          'data-iksasaranopd'=>$r[COL_KD_INDIKATORSASARANOPD]
                        ))?>
                      </td>
                      <?php
                    }
                    ?>
                    <td class="text-center" style="white-space: nowrap; background-color: #ff851b73">
                      <?=anchor(site_url('mopd/target/'.$period.'/'.$tahun.'/-1'),
                      (!empty($r['Target'])?nl2br($r['Target']):'(kosong)'),
                      array(
                        'class'=>'link-target',
                        'data-val'=>(!empty($r['Target'])?$r['Target']:''),
                        'data-label'=>'Target',
                        'data-misi'=>$r[COL_KD_MISI],
                        'data-tujuan'=>$r[COL_KD_TUJUAN],
                        'data-iktujuan'=>$r[COL_KD_INDIKATORTUJUAN],
                        'data-sasaran'=>$r[COL_KD_SASARAN],
                        'data-iksasaran'=>$r[COL_KD_INDIKATORSASARAN],
                        'data-tujuanopd'=>$r[COL_KD_TUJUANOPD],
                        'data-iktujuanopd'=>$r[COL_KD_INDIKATORTUJUANOPD],
                        'data-sasaranopd'=>$r[COL_KD_SASARANOPD],
                        'data-iksasaranopd'=>$r[COL_KD_INDIKATORSASARANOPD]
                      ))?>
                    </td>
                    <?php
                    for($i=1; $i<=4; $i++) {
                      ?>
                      <td class="text-center" style="white-space: nowrap; background-color: #01ff7054">
                        <?=anchor(site_url('mopd/realisasi/'.$period.'/'.$tahun.'/'.$i),
                        (!empty($r['Realisasi_TW'.$i])?nl2br($r['Realisasi_TW'.$i]):'(kosong)'),
                        array(
                          'class'=>'link-realisasi',
                          'data-val'=>(!empty($r['Realisasi_TW'.$i])?$r['Realisasi_TW'.$i]:''),
                          'data-label'=>'Realisasi TW. '.$i,
                          'data-misi'=>$r[COL_KD_MISI],
                          'data-tujuan'=>$r[COL_KD_TUJUAN],
                          'data-iktujuan'=>$r[COL_KD_INDIKATORTUJUAN],
                          'data-sasaran'=>$r[COL_KD_SASARAN],
                          'data-iksasaran'=>$r[COL_KD_INDIKATORSASARAN],
                          'data-tujuanopd'=>$r[COL_KD_TUJUANOPD],
                          'data-iktujuanopd'=>$r[COL_KD_INDIKATORTUJUANOPD],
                          'data-sasaranopd'=>$r[COL_KD_SASARANOPD],
                          'data-iksasaranopd'=>$r[COL_KD_INDIKATORSASARANOPD]
                        ))?>
                      </td>
                      <?php
                    }
                    ?>
                    <td class="text-center" style="white-space: nowrap; background-color: #01ff7054">
                      <?=anchor(site_url('mopd/realisasi/'.$period.'/'.$tahun.'/-1'),
                      (!empty($r['Realisasi'])?nl2br($r['Realisasi']):'(kosong)'),
                      array(
                        'class'=>'link-realisasi',
                        'data-val'=>(!empty($r['Realisasi'])?$r['Realisasi']:''),
                        'data-label'=>'Realisasi',
                        'data-misi'=>$r[COL_KD_MISI],
                        'data-tujuan'=>$r[COL_KD_TUJUAN],
                        'data-iktujuan'=>$r[COL_KD_INDIKATORTUJUAN],
                        'data-sasaran'=>$r[COL_KD_SASARAN],
                        'data-iksasaran'=>$r[COL_KD_INDIKATORSASARAN],
                        'data-tujuanopd'=>$r[COL_KD_TUJUANOPD],
                        'data-iktujuanopd'=>$r[COL_KD_INDIKATORTUJUANOPD],
                        'data-sasaranopd'=>$r[COL_KD_SASARANOPD],
                        'data-iksasaranopd'=>$r[COL_KD_INDIKATORSASARANOPD]
                      ))?>
                    </td>
                  </tr>
                  <?php
                  $no = $r['ID_Sasaran'];
                  $seq_++;
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <?php
    }
    ?>

    <?=form_close()?>
  </div>
</section>
<div class="modal fade" id="modalPopUp" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fa fa-close"></i></span>
        </button>
        <h4 class="modal-title">Capaian Tahun <?=$tahun?></h4>
      </div>
      <div class="modal-body">
        <form action="#" method="post">
          <div class="form-group">
            <label>Label</label>
            <textarea rows="3" name="VALUE" class="form-control"></textarea>
            <!--<input type="text" name="VALUE" class="form-control" />-->
            <input type="hidden" name="<?=COL_KD_URUSAN?>" value="<?=$kdUrusan?>" />
            <input type="hidden" name="<?=COL_KD_BIDANG?>" value="<?=$kdBidang?>" />
            <input type="hidden" name="<?=COL_KD_UNIT?>" value="<?=$kdUnit?>" />
            <input type="hidden" name="<?=COL_KD_SUB?>" value="<?=$kdSub?>" />

            <input type="hidden" name="<?=COL_KD_MISI?>" />
            <input type="hidden" name="<?=COL_KD_TUJUAN?>" />
            <input type="hidden" name="<?=COL_KD_INDIKATORTUJUAN?>" />
            <input type="hidden" name="<?=COL_KD_SASARAN?>" />
            <input type="hidden" name="<?=COL_KD_INDIKATORSASARAN?>" />
            <input type="hidden" name="<?=COL_KD_TUJUANOPD?>" />
            <input type="hidden" name="<?=COL_KD_INDIKATORTUJUANOPD?>" />
            <input type="hidden" name="<?=COL_KD_SASARANOPD?>" />
            <input type="hidden" name="<?=COL_KD_INDIKATORSASARANOPD?>" />
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal"><i class="fas fa-times"></i>&nbsp;Cancel</button>
        <button type="button" class="btn btn-primary btn-flat btn-ok"><i class="fas fa-check"></i>&nbsp;SIMPAN</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="browseOPD" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">OPD</h4>
      </div>
      <div class="modal-body">...</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">CLOSE</button>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('loadjs')?>
<script>
$(document).ready(function(){
  $('#browseOPD').on('show.bs.modal', function (event) {
      var modalBody = $(".modal-body", $("#browseOPD"));
      $(this).removeData('bs.modal');
      modalBody.html("<p style='font-style: italic'>Loading..</p>");
      modalBody.load("<?=site_url("ajax/browse-opd")?>", function () {
          $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
              var kdSub = $(this).val().split('|');
              $("[name=Kd_Urusan]").val(kdSub[0]);
              $("[name=Kd_Bidang]").val(kdSub[1]);
              $("[name=Kd_Unit]").val(kdSub[2]);
              $("[name=Kd_Sub]").val(kdSub[3]);
          });
          $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
              $("[name=text-opd]").val($(this).val());
          });
      });
  });

  $('.link-target, .link-realisasi').click(function() {
    var modal = $('#modalPopUp');
    var label = $(this).data('label');
    var val = $(this).data('val');
    var href = $(this).attr('href');

    modal.modal('show');
    $('label', modal).html(label);
    $('[name=VALUE]', modal).val(val);
    $('input[name=Kd_Misi]', modal).val($(this).data('misi'));
    $('input[name=Kd_Tujuan]', modal).val($(this).data('tujuan'));
    $('input[name=Kd_IndikatorTujuan]', modal).val($(this).data('iktujuan'));
    $('input[name=Kd_Sasaran]', modal).val($(this).data('sasaran'));
    $('input[name=Kd_IndikatorSasaran]', modal).val($(this).data('iksasaran'));
    $('input[name=Kd_TujuanOPD]', modal).val($(this).data('tujuanopd'));
    $('input[name=Kd_IndikatorTujuanOPD]', modal).val($(this).data('iktujuanopd'));
    $('input[name=Kd_SasaranOPD]', modal).val($(this).data('sasaranopd'));
    $('input[name=Kd_IndikatorSasaranOPD]', modal).val($(this).data('iksasaranopd'));
    $('.btn-ok', modal).unbind().click(function(){
      var dis = $(this);
      dis.attr('disabled', true);

      $('form', modal).ajaxSubmit({
        dataType: 'json',
        url : href,
        success: function(data) {
          if(data.error==0){
            location.reload();
          } else{
            alert(data.error);
          }
        },
        error: function() {
          alert('Server Error.');
        },
        complete: function() {
          dis.attr('disabled', false);
          modal.modal('hide')
        }
      });
    });
    return false;
  });
});
</script>
<?php $this->load->view('footer')?>
