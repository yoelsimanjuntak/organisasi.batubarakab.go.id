<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 30/09/2018
 * Time: 11:06
 */
$ruser = GetLoggedUser();
?>

<?php $this->load->view('header')
?>
<section class="content-header">
    <h1><?= $title ?>  <small>Data</small></h1>
    <ol class="breadcrumb">
        <li>
            <a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a>
        </li>
        <li class="active">
            Tujuan
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <p>
        <?=anchor('mopd/tujuan-del','<i class="far fa-trash"></i> Hapus',array('class'=>'cekboxaction btn btn-sm btn-danger','confirm'=>'Apa anda yakin?'))
        ?>
        <?=anchor('mopd/tujuan-add','<i class="far fa-plus"></i> Data Baru',array('class'=>'btn btn-sm btn-primary'))
        ?>
    </p>
    <div class="box box-danger">
      <div class="box-header with-border">
        <form id="form-filter" action="<?=current_url()?>" method="get" class="form-horizontal">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label class="col-sm-2 control-label">PERIODE PEMERINTAHAN</label>
                <div class="col-sm-6">
                  <select name="Periode" class="form-control">
                    <?=GetCombobox("select * from sakip_mpemda order by Kd_Tahun_From desc",COL_KD_PEMDA,array(COL_KD_TAHUN_FROM, COL_NM_PEJABAT), $periode)?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">OPD</label>
                <div class="col-sm-6">
                  <div class="input-group">
                    <?php
                    $nmSub = "";
                    $strOPD = explode('.', $ruser[COL_COMPANYID]);
                    if(!empty($_GET[COL_KD_URUSAN])) {

                        $eplandb = $this->load->database("eplan", true);
                        $eplandb->where(COL_KD_URUSAN, $_GET[COL_KD_URUSAN]);
                        $eplandb->where(COL_KD_BIDANG, $_GET[COL_KD_BIDANG]);
                        $eplandb->where(COL_KD_UNIT, $_GET[COL_KD_UNIT]);
                        $eplandb->where(COL_KD_SUB, $_GET[COL_KD_SUB]);
                        $subunit = $eplandb->get("ref_sub_unit")->row_array();
                        if($subunit) {
                            $nmSub = $subunit["Nm_Sub_Unit"];
                        }
                    }
                    if($ruser[COL_ROLEID] == ROLEKADIS) {
                        $eplandb = $this->load->database("eplan", true);
                        $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
                        $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
                        $eplandb->where(COL_KD_UNIT, $strOPD[2]);
                        $eplandb->where(COL_KD_SUB, $strOPD[3]);
                        $subunit = $eplandb->get("ref_sub_unit")->row_array();
                        if($subunit) {
                            $nmSub = $subunit["Nm_Sub_Unit"];
                        }
                    }

                    ?>
                    <input type="text"
                      class="form-control"
                      name="text-opd"
                      value="<?= !empty($_GET[COL_KD_URUSAN])?$_GET[COL_KD_URUSAN]."."
                              .$_GET[COL_KD_BIDANG]."."
                              .$_GET[COL_KD_UNIT]."."
                              .$_GET[COL_KD_SUB]." "
                              .$nmSub : ($ruser[COL_ROLEID] == ROLEKADIS ? $strOPD[0].".".$strOPD[1].".".$strOPD[2].".".$strOPD[3]." ".$nmSub : "")?>"
                      readonly>
                    <input type="hidden" name="<?=COL_KD_URUSAN?>" value="<?= !empty($_GET[COL_KD_URUSAN]) ? $_GET[COL_KD_URUSAN] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[0]:"")?>" required >
                    <input type="hidden" name="<?=COL_KD_BIDANG?>" value="<?= !empty($_GET[COL_KD_URUSAN]) ? $_GET[COL_KD_BIDANG] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[1]:"")?>" required >
                    <input type="hidden" name="<?=COL_KD_UNIT?>" value="<?= !empty($_GET[COL_KD_URUSAN]) ? $_GET[COL_KD_UNIT] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[2]:"")?>" required >
                    <input type="hidden" name="<?=COL_KD_SUB?>" value="<?= !empty($_GET[COL_KD_URUSAN]) ? $_GET[COL_KD_SUB] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[3]:"")?>" required >
                    <div class="input-group-btn">
                      <button type="button" class="btn btn-default btn-flat btn-browse-opd" data-toggle="modal" data-target="#browseOPD" data-toggle="tooltip" data-placement="top" title="Pilih OPD" <?=!empty($_GET[COL_KD_URUSAN])?"":($ruser[COL_ROLEID] == ROLEKADIS ? "disabled" : "")?>><i class="fa fa-ellipsis-h"></i></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="box-body" style="padding: 0 !important">
        <form id="dataform" method="post" action="#">
          <table class="table table-bordered table-condensed table-responsive" width="100%">
            <thead>
              <tr>
                <th>#</th>
                <th>No.</th>
                <th>Tujuan</th>
                <th>IKU Kabupaten</th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach($res as $r) {
                $riksasaran = $this->db
                ->select('sakip_mpmd_iksasaran.*, sakip_mpmd_misi.Nm_Misi')
                ->join(TBL_SAKIP_MPMD_MISI,TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_MISI.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_MISI,"inner")
                ->join(TBL_SAKIP_MPMD_IKSASARAN,TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_PEMDA.
                                " AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_MISI.
                                " AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_TUJUAN.
                                " AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_INDIKATORTUJUAN.
                                " AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_SASARAN.
                                " AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_INDIKATORSASARAN
                ,"inner")
                ->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_PEMDA, $periode)
                ->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_URUSAN, $r[COL_KD_URUSAN])
                ->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_BIDANG, $r[COL_KD_BIDANG])
                ->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_UNIT, $r[COL_KD_UNIT])
                ->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SUB, $r[COL_KD_SUB])
                ->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUANOPD, $r[COL_KD_TUJUANOPD])
                ->order_by(COL_KD_MISI)
                ->order_by(COL_KD_TUJUAN)
                ->order_by(COL_KD_INDIKATORTUJUAN)
                ->order_by(COL_KD_SASARAN)
                ->order_by(COL_KD_INDIKATORSASARAN)
                ->get(TBL_SAKIP_MOPD_TUJUAN)
                ->result_array();
                ?>
                <tr>
                  <td rowspan="<?=count($riksasaran)?>" class="text-center" style="width: 10px; white-space: nowrap; vertical-align: middle">
                    <input type="checkbox" class="cekbox" name="cekbox[]" value="<?=$r["ID"]?>" />
                  </td>
                  <td rowspan="<?=count($riksasaran)?>" class="text-right" style="width: 10px; white-space: nowrap;  vertical-align: middle"><?=$r[COL_KD_TUJUANOPD]?></td>
                  <td style="vertical-align: middle" rowspan="<?=count($riksasaran)?>"><?=anchor('mopd/tujuan-edit/'.$r["ID"],$r[COL_NM_TUJUANOPD])?></td>
                  <td style="vertical-align: middle">
                    <?=$riksasaran[0][COL_KD_MISI].'.'.$riksasaran[0][COL_KD_TUJUAN].'.'.$riksasaran[0][COL_KD_INDIKATORTUJUAN].'.'.$riksasaran[0][COL_KD_SASARAN].'.'.$riksasaran[0][COL_KD_INDIKATORSASARAN].' '.$riksasaran[0][COL_NM_INDIKATORSASARAN]?>
                  </td>
                </tr>
                <?php
                for($x=1; $x<count($riksasaran); $x++) {
                  ?>
                  <tr>
                    <td style="white-space: nowrap">
                      <?=$riksasaran[$x][COL_KD_MISI].'.'.$riksasaran[$x][COL_KD_TUJUAN].'.'.$riksasaran[$x][COL_KD_INDIKATORTUJUAN].'.'.$riksasaran[$x][COL_KD_SASARAN].'.'.$riksasaran[$x][COL_KD_INDIKATORSASARAN].' '.$riksasaran[$x][COL_NM_INDIKATORSASARAN]?>
                    </td>
                  </tr>
                  <?php
                }
              }
              ?>
            </tbody>
          </table>
        </form>
      </div>
    </div>

</section>
<div class="modal fade" id="browseOPD" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Browse</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('loadjs')?>
<script type="text/javascript">
$(document).ready(function(){
  $('#browseOPD').on('show.bs.modal', function (event) {
      var modalBody = $(".modal-body", $("#browseOPD"));
      $(this).removeData('bs.modal');
      modalBody.html("<p style='font-style: italic'>Loading..</p>");
      modalBody.load("<?=site_url("ajax/browse-opd")?>", function () {
          $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
              var kdSub = $(this).val().split('|');
              $("[name=Kd_Urusan]").val(kdSub[0]).change();
              $("[name=Kd_Bidang]").val(kdSub[1]).change();
              $("[name=Kd_Unit]").val(kdSub[2]).change();
              $("[name=Kd_Sub]").val(kdSub[3]).change();
          });
          $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
              $("[name=text-opd]").val($(this).val());
          });
      });
  });
  $('[name=Periode],[name=Kd_Urusan],[name=Kd_Bidang],[name=Kd_Unit],[name=Kd_Sub]').change(function() {
    $(this).closest('form').submit();
  });
});
</script>
<?php $this->load->view('footer')?>
