<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 30/09/2018
 * Time: 11:12
 */
$this->load->view('header') ?>
<?php
$ruser = GetLoggedUser();
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?= $title ?> <small> Form</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?=site_url('mopd/tujuan')?>"> Tujuan</a></li>
            <li class="active"><?=$edit?'Edit':'Add'?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">
                        <?=form_open(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
                        <div style="display: none" class="alert alert-danger errorBox">
                            <i class="fa fa-ban"></i>
                            <span class="errorMsg"></span>
                        </div>
                        <?php
                        if($this->input->get('success') == 1){
                            ?>
                            <div class="alert alert-success">
                                <i class="fa fa-check"></i>
                                <span class="">Data disimpan</span>
                            </div>
                        <?php
                        }
                        ?>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Periode</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-periode" value="<?= $edit ? $data[COL_KD_TAHUN_FROM]." s.d ".$data[COL_KD_TAHUN_TO]." : ".$data[COL_NM_PEJABAT] : ""?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_PEMDA?>" value="<?= $edit ? $data[COL_KD_PEMDA] : ""?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-period" data-toggle="modal" data-target="#browsePeriod" data-toggle="tooltip" data-placement="top" title="Pilih Periode" <?=$edit?"":""?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Misi</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-misi" value="<?= $edit ? $data[COL_KD_MISI].". ".$data[COL_NM_MISI] : ""?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_MISI?>" value="<?= $edit ? $data[COL_KD_MISI] : ""?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-misi" data-toggle="modal" data-target="#browseMisi" data-toggle="tooltip" data-placement="top" title="Pilih Misi" <?=$edit?"":""?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Tujuan</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-tujuan" value="<?= $edit ? $data[COL_KD_MISI].".".$data[COL_KD_TUJUAN]." ".$data[COL_NM_TUJUAN] : ""?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_TUJUAN?>" value="<?= $edit ? $data[COL_KD_TUJUAN] : ""?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-tujuan" data-toggle="modal" data-target="#browseTujuan" data-toggle="tooltip" data-placement="top" title="Pilih Tujuan" <?=$edit?"":""?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Indikator Tujuan</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-iktujuan" value="<?= $edit ? $data[COL_KD_MISI].".".$data[COL_KD_TUJUAN].".".$data[COL_KD_INDIKATORTUJUAN]." ".$data[COL_NM_INDIKATORTUJUAN] : ""?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_INDIKATORTUJUAN?>" value="<?= $edit ? $data[COL_KD_INDIKATORTUJUAN] : ""?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-iktujuan" data-toggle="modal" data-target="#browseIKTujuan" data-toggle="tooltip" data-placement="top" title="Pilih Indikator Tujuan" <?=$edit?"":""?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Sasaran</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-sasaran" value="<?= $edit ? $data[COL_KD_MISI].".".$data[COL_KD_TUJUAN].".".$data[COL_KD_INDIKATORTUJUAN].".".$data[COL_KD_SASARAN]." ".$data[COL_NM_SASARAN] : ""?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_SASARAN?>" value="<?= $edit ? $data[COL_KD_SASARAN] : ""?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-sasaran" data-toggle="modal" data-target="#browseSasaran" data-toggle="tooltip" data-placement="top" title="Pilih Sasaran" <?=$edit?"":""?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Indikator Sasaran</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-iksasaran" value="<?= $edit ? $data[COL_KD_MISI].".".$data[COL_KD_TUJUAN].".".$data[COL_KD_INDIKATORTUJUAN].".".$data[COL_KD_SASARAN].".".$data[COL_KD_INDIKATORSASARAN]." ".$data[COL_NM_INDIKATORSASARAN] : ""?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_INDIKATORSASARAN?>" value="<?= $edit ? $data[COL_KD_INDIKATORSASARAN] : ""?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-iksasaran" data-toggle="modal" data-target="#browseIKSasaran" data-toggle="tooltip" data-placement="top" title="Pilih Indikator Sasaran" <?=$edit?"":""?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">OPD</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <?php
                                    $nmSub = "";
                                    $strOPD = explode('.', $ruser[COL_COMPANYID]);
                                    if($edit) {

                                        $eplandb = $this->load->database("eplan", true);
                                        $eplandb->where(COL_KD_URUSAN, $data[COL_KD_URUSAN]);
                                        $eplandb->where(COL_KD_BIDANG, $data[COL_KD_BIDANG]);
                                        $eplandb->where(COL_KD_UNIT, $data[COL_KD_UNIT]);
                                        $eplandb->where(COL_KD_SUB, $data[COL_KD_SUB]);
                                        $subunit = $eplandb->get("ref_sub_unit")->row_array();
                                        if($subunit) {
                                            $nmSub = $subunit["Nm_Sub_Unit"];
                                        }
                                    }
                                    if($ruser[COL_ROLEID] == ROLEKADIS) {
                                        $eplandb = $this->load->database("eplan", true);
                                        $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
                                        $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
                                        $eplandb->where(COL_KD_UNIT, $strOPD[2]);
                                        $eplandb->where(COL_KD_SUB, $strOPD[3]);
                                        $subunit = $eplandb->get("ref_sub_unit")->row_array();
                                        if($subunit) {
                                            $nmSub = $subunit["Nm_Sub_Unit"];
                                        }
                                    }

                                    ?>
                                    <input type="text" class="form-control" name="text-opd" value="<?= $edit ? $data[COL_KD_URUSAN].".".$data[COL_KD_BIDANG].".".$data[COL_KD_UNIT].".".$data[COL_KD_SUB]." ".$nmSub : ($ruser[COL_ROLEID] == ROLEKADIS ? $strOPD[0].".".$strOPD[1].".".$strOPD[2].".".$strOPD[3]." ".$nmSub : "")?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_URUSAN?>" value="<?= $edit ? $data[COL_KD_URUSAN] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[0]:"")?>" required >
                                    <input type="hidden" name="<?=COL_KD_BIDANG?>" value="<?= $edit ? $data[COL_KD_BIDANG] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[1]:"")?>" required >
                                    <input type="hidden" name="<?=COL_KD_UNIT?>" value="<?= $edit ? $data[COL_KD_UNIT] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[2]:"")?>" required >
                                    <input type="hidden" name="<?=COL_KD_SUB?>" value="<?= $edit ? $data[COL_KD_SUB] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[3]:"")?>" required >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-opd" data-toggle="modal" data-target="#browseOPD" data-toggle="tooltip" data-placement="top" title="Pilih OPD" <?=$edit?"":($ruser[COL_ROLEID] == ROLEKADIS ? "disabled" : "")?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">No.</label>
                            <div class="col-sm-2">
                                <input type="number" class="form-control" name="<?=COL_KD_TUJUANOPD?>" value="<?= $edit ? $data[COL_KD_TUJUANOPD] : ""?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Tujuan</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="<?=COL_NM_TUJUANOPD?>" value="<?= $edit ? $data[COL_NM_TUJUANOPD] : ""?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Indikator Tujuan</label>
                            <div class="col-sm-8">
                                <table class="table table-bordered" id="tbl-det">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th width="80%">Deskripsi</th>
                                        <th style="width: 40px"><button type="button" id="btn-add-det" class="btn btn-default btn-flat"><i class="fa fa-plus"></i></button></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="tr-blueprint-det" style="display: none">
                                        <td><input type="text" name="text-det-no" class="form-control" /></td>
                                        <td><input type="text" name="text-det-desc" class="form-control" /></td>
                                        <td>
                                            <button type="button" class="btn btn-default btn-flat btn-del-det"><i class="fa fa-minus"></i></button>
                                        </td>
                                    </tr>
                                    <?php
                                    $det = $this->db
                                        ->where(COL_KD_URUSAN, ($edit?$data[COL_KD_URUSAN]:-999))
                                        ->where(COL_KD_BIDANG, ($edit?$data[COL_KD_BIDANG]:-999))
                                        ->where(COL_KD_UNIT, ($edit?$data[COL_KD_UNIT]:-999))
                                        ->where(COL_KD_SUB, ($edit?$data[COL_KD_SUB]:-999))

                                        ->where(COL_KD_PEMDA, ($edit?$data[COL_KD_PEMDA]:-999))
                                        ->where(COL_KD_MISI, ($edit?$data[COL_KD_MISI]:-999))
                                        ->where(COL_KD_TUJUAN, ($edit?$data[COL_KD_TUJUAN]:-999))
                                        ->where(COL_KD_INDIKATORTUJUAN, ($edit?$data[COL_KD_INDIKATORTUJUAN]:-999))
                                        ->where(COL_KD_SASARAN, ($edit?$data[COL_KD_SASARAN]:-999))
                                        ->where(COL_KD_INDIKATORSASARAN, ($edit?$data[COL_KD_INDIKATORSASARAN]:-999))
                                        ->where(COL_KD_TUJUANOPD, ($edit?$data[COL_KD_TUJUANOPD]:-999))
                                        ->order_by(COL_KD_INDIKATORTUJUANOPD, 'asc')
                                        ->get(TBL_SAKIP_MOPD_IKTUJUAN)->result_array();
                                    foreach($det as $m) {
                                        ?>
                                        <tr>
                                            <td><input type="text" name="text-det-no" class="form-control" value="<?=$m[COL_KD_INDIKATORTUJUANOPD]?>" /></td>
                                            <td><input type="text" name="text-det-desc" class="form-control" value="<?=$m[COL_NM_INDIKATORTUJUANOPD]?>" /></td>
                                            <td>
                                                <button type="button" class="btn btn-default btn-flat btn-del-det"><i class="fa fa-minus"></i></button>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    ?>

                                    <?php
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12" style="text-align: right">
                                <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
                                <a href="<?=site_url('mopd/tujuan')?>" class="btn btn-default btn-flat">Kembali ke Daftar&nbsp;&nbsp;<i class="fa fa-arrow-right"></i> </a>
                            </div>

                        </div>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="browsePeriod" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseMisi" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseTujuan" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseIKTujuan" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseSasaran" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseIKSasaran" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseOPD" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view('loadjs') ?>
    <script type="text/javascript">
        $(".btn-del-det").click(function () {
            var row = $(this).closest("tr");
            row.remove();
        });

        $("#btn-add-det").click(function () {
            var tbl = $(this).closest("table");
            var blueprint = tbl.find(".tr-blueprint-det").first().clone();

            blueprint.appendTo(tbl).removeClass("tr-blueprint-det").show();
            $(".btn-del-det", blueprint).click(function () {
                var row = $(this).closest("tr");
                row.remove();
            });
        });
        $("#main-form").validate({
            submitHandler : function(form){
                $(form).find('btn').attr('disabled',true);
                var det = [];
                var rowDet = $("#tbl-det>tbody").find("tr:not(.tr-blueprint-det)");

                for (var i = 0; i < rowDet.length; i++) {
                    var noDet = $("[name=text-det-no]", $(rowDet[i])).val();
                    var ketDet = $("[name=text-det-desc]", $(rowDet[i])).val();
                    det.push({ No: noDet, Ket: ketDet });
                }
                $(".appended", $("#main-form")).remove();
                for (var i = 0; i < det.length; i++) {
                    var newEl = "<input type='hidden' class='appended' name=NoDet[" + i + "] value='" + det[i].No + "' /><input type='hidden' class='appended' name=KetDet[" + i + "] value='" + det[i].Ket + "' />";
                    $("#main-form").append(newEl);
                }
                $(form).ajaxSubmit({
                    dataType: 'json',
                    type : 'post',
                    success : function(data){
                        $(form).find('btn').attr('disabled',false);
                        if(data.error != 0){
                            $('.errorBox').show().find('.errorMsg').text(data.error);
                        }else{
                            window.location.href = data.redirect;
                        }
                    },
                    error : function(a,b,c){
                        console.log(a);
                        console.log(b);
                        console.log(c);
                        $(".modal-body", $("#alertDialog")).html(a.responseText);
                        $("#alertDialog").modal('show');
                        //alert('Response Error');
                    }
                });
                return false;
            }
        });

        $('.modal').on('hidden.bs.modal', function (event) {
            $(this).find(".modal-body").empty();
        });

        $('#browsePeriod').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browsePeriod"));
            $(this).removeData('bs.modal');
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-pemda")?>", function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=Kd_Pemda]").val($(this).val());

                    $("[name=Kd_Misi]").val("");
                    $("[name=text-misi]").val("");
                    $("[name=Kd_Tujuan]").val("");
                    $("[name=text-tujuan]").val("");
                    $("[name=Kd_IndikatorTujuan]").val("");
                    $("[name=text-iktujuan]").val("");
                    $("[name=Kd_Sasaran]").val("");
                    $("[name=text-sasaran]").val("");
                    $("[name=Kd_IndikatorSasaran]").val("");
                    $("[name=text-iksasaran]").val("");
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-periode]").val($(this).val());
                });
            });
        });

        $('#browseMisi').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseMisi"));
            $(this).removeData('bs.modal');
            var kdPemda = $("[name=Kd_Pemda]").val();
            if(!kdPemda) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih periode pemerintahan terlebih dahulu!</p>");
                return;
            }
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-misi")?>"+"?Kd_Pemda="+kdPemda, function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=Kd_Misi]").val($(this).val());

                    $("[name=Kd_Tujuan]").val("");
                    $("[name=text-tujuan]").val("");
                    $("[name=Kd_IndikatorTujuan]").val("");
                    $("[name=text-iktujuan]").val("");
                    $("[name=Kd_Sasaran]").val("");
                    $("[name=text-sasaran]").val("");
                    $("[name=Kd_IndikatorSasaran]").val("");
                    $("[name=text-iksasaran]").val("");
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-misi]").val($(this).val());
                });
            });
        });

        $('#browseTujuan').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseTujuan"));
            $(this).removeData('bs.modal');
            var kdPemda = $("[name=Kd_Pemda]").val();
            var kdMisi = $("[name=Kd_Misi]").val();
            if(!kdPemda) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih periode pemerintahan terlebih dahulu!</p>");
                return;
            }
            if(!kdMisi) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih misi terlebih dahulu!</p>");
                return;
            }
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-tujuan")?>"+"?Kd_Pemda="+kdPemda+"&Kd_Misi="+kdMisi, function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=Kd_Tujuan]").val($(this).val());

                    $("[name=Kd_IndikatorTujuan]").val("");
                    $("[name=text-iktujuan]").val("");
                    $("[name=Kd_Sasaran]").val("");
                    $("[name=text-sasaran]").val("");
                    $("[name=Kd_IndikatorSasaran]").val("");
                    $("[name=text-iksasaran]").val("");
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-tujuan]").val($(this).val());
                });
            });
        });

        $('#browseIKTujuan').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseIKTujuan"));
            $(this).removeData('bs.modal');
            var kdPemda = $("[name=Kd_Pemda]").val();
            var kdMisi = $("[name=Kd_Misi]").val();
            var kdTujuan = $("[name=Kd_Tujuan]").val();
            if(!kdPemda) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih periode pemerintahan terlebih dahulu!</p>");
                return;
            }
            if(!kdMisi) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih misi terlebih dahulu!</p>");
                return;
            }
            if(!kdTujuan) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih tujuan terlebih dahulu!</p>");
                return;
            }
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-iktujuan")?>"+"?Kd_Pemda="+kdPemda+"&Kd_Misi="+kdMisi+"&Kd_Tujuan="+kdTujuan, function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=Kd_IndikatorTujuan]").val($(this).val());

                    $("[name=Kd_Sasaran]").val("");
                    $("[name=text-sasaran]").val("");
                    $("[name=Kd_IndikatorSasaran]").val("");
                    $("[name=text-iksasaran]").val("");
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-iktujuan]").val($(this).val());
                });
            });
        });

        $('#browseSasaran').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseSasaran"));
            $(this).removeData('bs.modal');
            var kdPemda = $("[name=Kd_Pemda]").val();
            var kdMisi = $("[name=Kd_Misi]").val();
            var kdTujuan = $("[name=Kd_Tujuan]").val();
            var kdIKTujuan = $("[name=Kd_IndikatorTujuan]").val();
            if(!kdPemda) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih periode pemerintahan terlebih dahulu!</p>");
                return;
            }
            if(!kdMisi) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih misi terlebih dahulu!</p>");
                return;
            }
            if(!kdTujuan) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih tujuan terlebih dahulu!</p>");
                return;
            }
            if(!kdIKTujuan) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih indikator tujuan terlebih dahulu!</p>");
                return;
            }
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-sasaran")?>"+"?Kd_Pemda="+kdPemda+"&Kd_Misi="+kdMisi+"&Kd_Tujuan="+kdTujuan+"&Kd_IndikatorTujuan="+kdIKTujuan, function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=Kd_Sasaran]").val($(this).val());

                    $("[name=Kd_IndikatorSasaran]").val("");
                    $("[name=text-iksasaran]").val("");
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-sasaran]").val($(this).val());
                });
            });
        });

        $('#browseIKSasaran').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseIKSasaran"));
            $(this).removeData('bs.modal');
            var kdPemda = $("[name=Kd_Pemda]").val();
            var kdMisi = $("[name=Kd_Misi]").val();
            var kdTujuan = $("[name=Kd_Tujuan]").val();
            var kdIKTujuan = $("[name=Kd_IndikatorTujuan]").val();
            var kdSasaran = $("[name=Kd_Sasaran]").val();
            if(!kdPemda) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih periode pemerintahan terlebih dahulu!</p>");
                return;
            }
            if(!kdMisi) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih misi terlebih dahulu!</p>");
                return;
            }
            if(!kdTujuan) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih tujuan terlebih dahulu!</p>");
                return;
            }
            if(!kdIKTujuan) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih indikator tujuan terlebih dahulu!</p>");
                return;
            }
            if(!kdSasaran) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih sasaran terlebih dahulu!</p>");
                return;
            }
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-iksasaran")?>"+"?Kd_Pemda="+kdPemda+"&Kd_Misi="+kdMisi+"&Kd_Tujuan="+kdTujuan+"&Kd_IndikatorTujuan="+kdIKTujuan+"&Kd_Sasaran="+kdSasaran, function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=Kd_IndikatorSasaran]").val($(this).val());
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-iksasaran]").val($(this).val());
                });
            });
        });

        $('#browseOPD').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseOPD"));
            $(this).removeData('bs.modal');
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-opd")?>", function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    var kdSub = $(this).val().split('|');
                    $("[name=Kd_Urusan]").val(kdSub[0]);
                    $("[name=Kd_Bidang]").val(kdSub[1]);
                    $("[name=Kd_Unit]").val(kdSub[2]);
                    $("[name=Kd_Sub]").val(kdSub[3]);
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-opd]").val($(this).val());
                });
            });
        });
    </script>
<?php $this->load->view('footer') ?>