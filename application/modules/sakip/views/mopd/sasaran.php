<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 30/09/2018
 * Time: 13:51
 */
$ruser = GetLoggedUser();
?>

<?php $this->load->view('header')
?>
<section class="content-header">
    <h1><?= $title ?>  <small>Data</small></h1>
    <ol class="breadcrumb">
        <li>
            <a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a>
        </li>
        <li class="active">
            Sasaran OPD
        </li>
    </ol>
</section>
<section class="content">
    <p>
        <?=anchor('mopd/sasaran-del','<i class="far fa-trash"></i> Hapus',array('class'=>'cekboxaction btn btn-sm btn-danger','confirm'=>'Apa anda yakin?'))
        ?>
        <?=anchor('mopd/sasaran-add','<i class="far fa-plus"></i> Data Baru',array('class'=>'btn btn-sm btn-primary'))
        ?>
    </p>
    <div class="box box-danger">
      <div class="box-header with-border">
        <form id="form-filter" action="<?=current_url()?>" method="get" class="form-horizontal">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label class="col-sm-2 control-label">PERIODE PEMERINTAHAN</label>
                <div class="col-sm-6">
                  <select name="Periode" class="form-control">
                    <?=GetCombobox("select * from sakip_mpemda order by Kd_Tahun_From desc",COL_KD_PEMDA,array(COL_KD_TAHUN_FROM, COL_NM_PEJABAT), $periode)?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">OPD</label>
                <div class="col-sm-6">
                  <div class="input-group">
                    <?php
                    $nmSub = "";
                    $strOPD = explode('.', $ruser[COL_COMPANYID]);
                    if(!empty($_GET[COL_KD_URUSAN])) {

                        $eplandb = $this->load->database("eplan", true);
                        $eplandb->where(COL_KD_URUSAN, $_GET[COL_KD_URUSAN]);
                        $eplandb->where(COL_KD_BIDANG, $_GET[COL_KD_BIDANG]);
                        $eplandb->where(COL_KD_UNIT, $_GET[COL_KD_UNIT]);
                        $eplandb->where(COL_KD_SUB, $_GET[COL_KD_SUB]);
                        $subunit = $eplandb->get("ref_sub_unit")->row_array();
                        if($subunit) {
                            $nmSub = $subunit["Nm_Sub_Unit"];
                        }
                    }
                    if($ruser[COL_ROLEID] == ROLEKADIS) {
                        $eplandb = $this->load->database("eplan", true);
                        $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
                        $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
                        $eplandb->where(COL_KD_UNIT, $strOPD[2]);
                        $eplandb->where(COL_KD_SUB, $strOPD[3]);
                        $subunit = $eplandb->get("ref_sub_unit")->row_array();
                        if($subunit) {
                            $nmSub = $subunit["Nm_Sub_Unit"];
                        }
                    }

                    ?>
                    <input type="text"
                      class="form-control"
                      name="text-opd"
                      value="<?= !empty($_GET[COL_KD_URUSAN])?$_GET[COL_KD_URUSAN]."."
                              .$_GET[COL_KD_BIDANG]."."
                              .$_GET[COL_KD_UNIT]."."
                              .$_GET[COL_KD_SUB]." "
                              .$nmSub : ($ruser[COL_ROLEID] == ROLEKADIS ? $strOPD[0].".".$strOPD[1].".".$strOPD[2].".".$strOPD[3]." ".$nmSub : "")?>"
                      readonly>
                    <input type="hidden" name="<?=COL_KD_URUSAN?>" value="<?= !empty($_GET[COL_KD_URUSAN]) ? $_GET[COL_KD_URUSAN] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[0]:"")?>" required >
                    <input type="hidden" name="<?=COL_KD_BIDANG?>" value="<?= !empty($_GET[COL_KD_URUSAN]) ? $_GET[COL_KD_BIDANG] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[1]:"")?>" required >
                    <input type="hidden" name="<?=COL_KD_UNIT?>" value="<?= !empty($_GET[COL_KD_URUSAN]) ? $_GET[COL_KD_UNIT] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[2]:"")?>" required >
                    <input type="hidden" name="<?=COL_KD_SUB?>" value="<?= !empty($_GET[COL_KD_URUSAN]) ? $_GET[COL_KD_SUB] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[3]:"")?>" required >
                    <div class="input-group-btn">
                      <button type="button" class="btn btn-default btn-flat btn-browse-opd" data-toggle="modal" data-target="#browseOPD" data-toggle="tooltip" data-placement="top" title="Pilih OPD" <?=!empty($_GET[COL_KD_URUSAN])?"":($ruser[COL_ROLEID] == ROLEKADIS ? "disabled" : "")?>><i class="fa fa-ellipsis-h"></i></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="box-body" style="padding: 0 !important">
        <form id="dataform" method="post" action="#">
          <table class="table table-bordered table-condensed table-responsive" width="100%">
            <tbody>
              <?php
              $rtujuan = $this->db
              ->join(TBL_SAKIP_MPMD_IKSASARAN,
                TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_PEMDA
                ." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_MISI
                ." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_TUJUAN
                ." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORTUJUAN
                ." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_SASARAN
                ." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORSASARAN
                ,"inner")
                ->join(TBL_SAKIP_MOPD_TUJUAN,
                  TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_URUSAN
                  ." AND ".TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_BIDANG
                  ." AND ".TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_UNIT
                  ." AND ".TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_SUB

                  ." AND ".TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_PEMDA
                  ." AND ".TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_MISI
                  ." AND ".TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_TUJUAN
                  ." AND ".TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORTUJUAN
                  ." AND ".TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_SASARAN
                  ." AND ".TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORSASARAN
                  ." AND ".TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_TUJUANOPD
                  ,"inner")
              ->where(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_URUSAN, !empty($_GET[COL_KD_URUSAN])?$_GET[COL_KD_URUSAN]:($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[0]:-999))
              ->where(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_BIDANG, !empty($_GET[COL_KD_BIDANG])?$_GET[COL_KD_BIDANG]:($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[1]:-999))
              ->where(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_UNIT, !empty($_GET[COL_KD_UNIT])?$_GET[COL_KD_UNIT]:($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[2]:-999))
              ->where(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_SUB, !empty($_GET[COL_KD_SUB])?$_GET[COL_KD_SUB]:($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[3]:-999))
              ->where(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_PEMDA, $periode)
              ->get(TBL_SAKIP_MOPD_IKTUJUAN)
              ->result_array();
              foreach ($rtujuan as $rt) {
                $rsasaran = $this->db
                ->select('*,'.TBL_SAKIP_MOPD_SASARAN.'.'.COL_UNIQ.' as ID')
                ->where(COL_KD_URUSAN, $rt[COL_KD_URUSAN])
                ->where(COL_KD_BIDANG, $rt[COL_KD_BIDANG])
                ->where(COL_KD_UNIT, $rt[COL_KD_UNIT])
                ->where(COL_KD_SUB, $rt[COL_KD_SUB])

                ->where(COL_KD_PEMDA, $rt[COL_KD_PEMDA])
                ->where(COL_KD_MISI, $rt[COL_KD_MISI])
                ->where(COL_KD_TUJUAN, $rt[COL_KD_TUJUAN])
                ->where(COL_KD_INDIKATORTUJUAN, $rt[COL_KD_INDIKATORTUJUAN])
                ->where(COL_KD_SASARAN, $rt[COL_KD_SASARAN])
                ->where(COL_KD_INDIKATORSASARAN, $rt[COL_KD_INDIKATORSASARAN])
                ->where(COL_KD_TUJUANOPD, $rt[COL_KD_TUJUANOPD])
                ->where(COL_KD_INDIKATORTUJUANOPD, $rt[COL_KD_INDIKATORTUJUANOPD])
                ->get(TBL_SAKIP_MOPD_SASARAN)
                ->result_array();
                ?>
                <tr class="bg-gray">
                  <td class="text-right text-sm" style="width: 20px; white-space: nowrap">IKU KABUPATEN</td>
                  <td class="text-bold" style="width: 20px; white-space: nowrap"><?=$rt[COL_KD_MISI].'.'.$rt[COL_KD_TUJUAN].'.'.$rt[COL_KD_INDIKATORTUJUAN].'.'.$rt[COL_KD_SASARAN].'.'.$rt[COL_KD_INDIKATORSASARAN]?></td>
                  <td class="text-bold" colspan="2"><?=$rt[COL_NM_INDIKATORSASARAN]?></td>
                </tr>
                <tr class="bg-gray">
                  <td class="text-right text-sm" style="width: 20px; white-space: nowrap">TUJUAN OPD</td>
                  <td class="text-bold" style="width: 20px; white-space: nowrap"><?=$rt[COL_KD_MISI].'.'.$rt[COL_KD_TUJUAN].'.'.$rt[COL_KD_INDIKATORTUJUAN].'.'.$rt[COL_KD_SASARAN].'.'.$rt[COL_KD_INDIKATORSASARAN].'.'.$rt[COL_KD_TUJUANOPD]?></td>
                  <td class="text-bold" colspan="2"><?=$rt[COL_NM_TUJUANOPD]?></td>
                </tr>
                <tr class="bg-gray">
                  <td class="text-right text-sm" style="width: 20px; white-space: nowrap">INDIKATOR TUJUAN OPD</td>
                  <td class="text-bold" style="width: 20px; white-space: nowrap"><?=$rt[COL_KD_MISI].'.'.$rt[COL_KD_TUJUAN].'.'.$rt[COL_KD_INDIKATORTUJUAN].'.'.$rt[COL_KD_SASARAN].'.'.$rt[COL_KD_INDIKATORSASARAN].'.'.$rt[COL_KD_TUJUANOPD].'.'.$rt[COL_KD_INDIKATORTUJUANOPD]?></td>
                  <td class="text-bold" colspan="2"><?=$rt[COL_NM_INDIKATORTUJUANOPD]?></td>
                </tr>
                <?php
                if(!empty($rsasaran)) {
                  foreach($rsasaran as $rs) {
                    ?>
                    <tr>
                      <td class="text-right" style="width: 20px; white-space: nowrap">
                        <input type="checkbox" class="cekbox" name="cekbox[]" value="<?=$rs["ID"]?>" />
                      </td>
                      <td class="text-bold text-right" style="width: 20px; white-space: nowrap">
                        <?=$rt[COL_KD_MISI].'.'.$rt[COL_KD_TUJUAN].'.'.$rt[COL_KD_INDIKATORTUJUAN].'.'.$rt[COL_KD_SASARAN].'.'.$rt[COL_KD_INDIKATORSASARAN].'.'.$rt[COL_KD_TUJUANOPD].'.'.$rt[COL_KD_INDIKATORTUJUANOPD].'.'.$rs[COL_KD_SASARANOPD]?>
                      </td>
                      <td colspan="2" class="text-bold"><?=anchor('mopd/sasaran-edit/'.$rs["ID"],$rs[COL_NM_SASARANOPD])?></td>
                    </tr>
                    <tr>
                      <td class="text-right" style="width: 20px; white-space: nowrap"></td>
                      <td class="text-right text-sm" style="width: 20px; white-space: nowrap">INDIKATOR :</td>
                      <td colspan="2">
                        <?php
                        $riksasaran = $this->db
                        ->where(COL_KD_URUSAN, $rs[COL_KD_URUSAN])
                        ->where(COL_KD_BIDANG, $rs[COL_KD_BIDANG])
                        ->where(COL_KD_UNIT, $rs[COL_KD_UNIT])
                        ->where(COL_KD_SUB, $rs[COL_KD_SUB])

                        ->where(COL_KD_PEMDA, $rs[COL_KD_PEMDA])
                        ->where(COL_KD_MISI, $rs[COL_KD_MISI])
                        ->where(COL_KD_TUJUAN, $rs[COL_KD_TUJUAN])
                        ->where(COL_KD_INDIKATORTUJUAN, $rs[COL_KD_INDIKATORTUJUAN])
                        ->where(COL_KD_SASARAN, $rs[COL_KD_SASARAN])
                        ->where(COL_KD_INDIKATORSASARAN, $rs[COL_KD_INDIKATORSASARAN])
                        ->where(COL_KD_TUJUANOPD, $rs[COL_KD_TUJUANOPD])
                        ->where(COL_KD_INDIKATORTUJUANOPD, $rs[COL_KD_INDIKATORTUJUANOPD])
                        ->where(COL_KD_SASARANOPD, $rs[COL_KD_SASARANOPD])
                        ->get(TBL_SAKIP_MOPD_IKSASARAN)
                        ->result_array();
                        if(!empty($riksasaran)) {
                          ?>
                          <ol style="padding-inline-start: 25px; margin-bottom: 0">
                            <?php
                            foreach ($riksasaran as $iks) {
                              echo '<li>'.$iks[COL_NM_INDIKATORSASARANOPD].'</li>';
                            }
                            ?>
                          </ol>
                          <?php
                        } else {
                          echo '-';
                        }
                        ?>
                      </td>
                    </tr>
                    <?php
                  }
                } else {
                  ?>
                  <tr>
                    <td colspan="4" class="text-danger text-center text-bold"><span style="font-style: italic">(KOSONG)</span></td>
                  </tr>
                  <?php
                }
              }
              ?>
            </tbody>
          </table>
        </form>

      </div>
    </div>
</section>
<div class="modal fade" id="browseOPD" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Browse</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('loadjs')?>
<script type="text/javascript">
$(document).ready(function(){
  $('#browseOPD').on('show.bs.modal', function (event) {
      var modalBody = $(".modal-body", $("#browseOPD"));
      $(this).removeData('bs.modal');
      modalBody.html("<p style='font-style: italic'>Loading..</p>");
      modalBody.load("<?=site_url("ajax/browse-opd")?>", function () {
          $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
              var kdSub = $(this).val().split('|');
              $("[name=Kd_Urusan]").val(kdSub[0]).change();
              $("[name=Kd_Bidang]").val(kdSub[1]).change();
              $("[name=Kd_Unit]").val(kdSub[2]).change();
              $("[name=Kd_Sub]").val(kdSub[3]).change();
          });
          $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
              $("[name=text-opd]").val($(this).val());
          });
      });
  });
  $('[name=Periode],[name=Kd_Urusan],[name=Kd_Bidang],[name=Kd_Unit],[name=Kd_Sub]').change(function() {
    $(this).closest('form').submit();
  });
});
</script>
<?php $this->load->view('footer')?>
