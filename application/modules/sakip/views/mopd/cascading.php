<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 30/09/2018
 * Time: 13:51
 */
$ruser = GetLoggedUser();
?>

<?php $this->load->view('header')
?>
<section class="content-header">
    <h1><?= $title ?>  <small>Data</small></h1>
    <ol class="breadcrumb">
        <li>
            <a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a>
        </li>
        <li class="active">
            Cascading OPD
        </li>
    </ol>
</section>
<section class="content">
    <div class="box box-danger">
      <div class="box-header with-border">
        <form id="form-filter" action="<?=current_url()?>" method="get" class="form-horizontal">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label class="col-sm-2 control-label">PERIODE PEMERINTAHAN</label>
                <div class="col-sm-6">
                  <select name="Periode" class="form-control">
                    <?=GetCombobox("select * from sakip_mpemda order by Kd_Tahun_From desc",COL_KD_PEMDA,array(COL_KD_TAHUN_FROM, COL_NM_PEJABAT), $periode)?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">OPD</label>
                <div class="col-sm-6">
                  <div class="input-group">
                    <?php
                    $nmSub = "";
                    $strOPD = explode('.', $ruser[COL_COMPANYID]);
                    if(!empty($_GET[COL_KD_URUSAN])) {

                        $eplandb = $this->load->database("eplan", true);
                        $eplandb->where(COL_KD_URUSAN, $_GET[COL_KD_URUSAN]);
                        $eplandb->where(COL_KD_BIDANG, $_GET[COL_KD_BIDANG]);
                        $eplandb->where(COL_KD_UNIT, $_GET[COL_KD_UNIT]);
                        $eplandb->where(COL_KD_SUB, $_GET[COL_KD_SUB]);
                        $subunit = $eplandb->get("ref_sub_unit")->row_array();
                        if($subunit) {
                            $nmSub = $subunit["Nm_Sub_Unit"];
                        }
                    }
                    if($ruser[COL_ROLEID] == ROLEKADIS) {
                        $eplandb = $this->load->database("eplan", true);
                        $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
                        $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
                        $eplandb->where(COL_KD_UNIT, $strOPD[2]);
                        $eplandb->where(COL_KD_SUB, $strOPD[3]);
                        $subunit = $eplandb->get("ref_sub_unit")->row_array();
                        if($subunit) {
                            $nmSub = $subunit["Nm_Sub_Unit"];
                        }
                    }

                    ?>
                    <input type="text"
                      class="form-control"
                      name="text-opd"
                      value="<?= !empty($_GET[COL_KD_URUSAN])?$_GET[COL_KD_URUSAN]."."
                              .$_GET[COL_KD_BIDANG]."."
                              .$_GET[COL_KD_UNIT]."."
                              .$_GET[COL_KD_SUB]." "
                              .$nmSub : ($ruser[COL_ROLEID] == ROLEKADIS ? $strOPD[0].".".$strOPD[1].".".$strOPD[2].".".$strOPD[3]." ".$nmSub : "")?>"
                      readonly>
                    <input type="hidden" name="<?=COL_KD_URUSAN?>" value="<?= !empty($_GET[COL_KD_URUSAN]) ? $_GET[COL_KD_URUSAN] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[0]:"")?>" required >
                    <input type="hidden" name="<?=COL_KD_BIDANG?>" value="<?= !empty($_GET[COL_KD_URUSAN]) ? $_GET[COL_KD_BIDANG] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[1]:"")?>" required >
                    <input type="hidden" name="<?=COL_KD_UNIT?>" value="<?= !empty($_GET[COL_KD_URUSAN]) ? $_GET[COL_KD_UNIT] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[2]:"")?>" required >
                    <input type="hidden" name="<?=COL_KD_SUB?>" value="<?= !empty($_GET[COL_KD_URUSAN]) ? $_GET[COL_KD_SUB] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[3]:"")?>" required >
                    <div class="input-group-btn">
                      <button type="button" class="btn btn-default btn-flat btn-browse-opd" data-toggle="modal" data-target="#browseOPD" data-toggle="tooltip" data-placement="top" title="Pilih OPD" <?=!empty($_GET[COL_KD_URUSAN])?"":($ruser[COL_ROLEID] == ROLEKADIS ? "disabled" : "")?>><i class="fa fa-ellipsis-h"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">TAHUN</label>
                <div class="col-sm-2">
                  <input name="Tahun" type="number" class="form-control" value="<?= $tahun ?>" />
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    <?php
    $riksasaran = $this->db
    ->join(TBL_SAKIP_MOPD_SASARAN,
      TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_URUSAN
      ." AND ".TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_BIDANG
      ." AND ".TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_UNIT
      ." AND ".TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_SUB

      ." AND ".TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_PEMDA
      ." AND ".TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_MISI
      ." AND ".TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_TUJUAN
      ." AND ".TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORTUJUAN
      ." AND ".TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_SASARAN
      ." AND ".TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORSASARAN
      ." AND ".TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_TUJUANOPD
      ." AND ".TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORTUJUANOPD
      ." AND ".TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_SASARANOPD
      ,"inner")
    ->where(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_URUSAN, !empty($_GET[COL_KD_URUSAN])?$_GET[COL_KD_URUSAN]:($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[0]:-999))
    ->where(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_BIDANG, !empty($_GET[COL_KD_BIDANG])?$_GET[COL_KD_BIDANG]:($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[1]:-999))
    ->where(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_UNIT, !empty($_GET[COL_KD_UNIT])?$_GET[COL_KD_UNIT]:($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[2]:-999))
    ->where(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_SUB, !empty($_GET[COL_KD_SUB])?$_GET[COL_KD_SUB]:($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[3]:-999))
    ->where(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_PEMDA, $periode)
    ->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_TUJUANOPD)
    ->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORTUJUANOPD)
    ->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORSASARANOPD)
    ->get(TBL_SAKIP_MOPD_IKSASARAN)
    ->result_array();

    foreach($riksasaran as $iks) {
      $rsasaranbid = $this->db
      ->select('*,'.TBL_SAKIP_MBID_SASARAN.'.'.COL_UNIQ.' as ID')
      ->join(TBL_SAKIP_MBID,
          TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_URUSAN." AND ".
          TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_BIDANG." AND ".
          TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_UNIT." AND ".
          TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_SUB." AND ".
          TBL_SAKIP_MBID.'.'.COL_KD_BID." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_BID
          ,"inner")
      ->where(TBL_SAKIP_MBID_SASARAN.".".COL_KD_URUSAN, $iks[COL_KD_URUSAN])
      ->where(TBL_SAKIP_MBID_SASARAN.".".COL_KD_BIDANG, $iks[COL_KD_BIDANG])
      ->where(TBL_SAKIP_MBID_SASARAN.".".COL_KD_UNIT, $iks[COL_KD_UNIT])
      ->where(TBL_SAKIP_MBID_SASARAN.".".COL_KD_SUB, $iks[COL_KD_SUB])
      ->where(TBL_SAKIP_MBID_SASARAN.".".COL_KD_TAHUN, $tahun)

      ->where(TBL_SAKIP_MBID_SASARAN.".".COL_KD_PEMDA, $iks[COL_KD_PEMDA])
      ->where(TBL_SAKIP_MBID_SASARAN.".".COL_KD_MISI, $iks[COL_KD_MISI])
      ->where(TBL_SAKIP_MBID_SASARAN.".".COL_KD_TUJUAN, $iks[COL_KD_TUJUAN])
      ->where(TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORTUJUAN, $iks[COL_KD_INDIKATORTUJUAN])
      ->where(TBL_SAKIP_MBID_SASARAN.".".COL_KD_SASARAN, $iks[COL_KD_SASARAN])
      ->where(TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORSASARAN, $iks[COL_KD_INDIKATORSASARAN])
      ->where(TBL_SAKIP_MBID_SASARAN.".".COL_KD_TUJUANOPD, $iks[COL_KD_TUJUANOPD])
      ->where(TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORTUJUANOPD, $iks[COL_KD_INDIKATORTUJUANOPD])
      ->where(TBL_SAKIP_MBID_SASARAN.".".COL_KD_SASARANOPD, $iks[COL_KD_INDIKATORTUJUANOPD])
      ->where(TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORSASARANOPD, $iks[COL_KD_INDIKATORTUJUANOPD])

      ->order_by(TBL_SAKIP_MBID_SASARAN.".".COL_KD_SASARANPROGRAMOPD)
      ->get(TBL_SAKIP_MBID_SASARAN)
      ->result_array();
      $q = $this->db->last_query();
      ?>
      <div class="box box-default collapsed-box">
        <div class="box-header with-border">
          <div class="row">
            <div class="col-sm-2 text-right">
              SASARAN OPD :
            </div>
            <div class="col-sm-10 text-bold" style="padding-left: 0 !important">
              <?=strtoupper($iks[COL_NM_INDIKATORSASARANOPD])?>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-2 text-right">
              INDIKATOR SASARAN OPD :
            </div>
            <div class="col-sm-10 text-bold" style="padding-left: 0 !important">
              <?=strtoupper($iks[COL_NM_SASARANOPD])?>
            </div>
          </div>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
          </div>
        </div>
        <div class="box-body" style="padding: 0 !important">
          <table class="table table-bordered table-responsive">
            <thead>
              <tr>
                <th class="text-center" style="width: 10px; text-overflow: nowrap">#</th>
                <th>Unit Kerja / Jabatan</th>
                <th colspan="2">Sasaran / Indikator</th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach($rsasaranbid as $sb) {
                ?>
                <tr>
                  <td class="text-center" style="width: 10px; text-overflow: nowrap">##</td>
                  <td><?=strtoupper($sb[COL_NM_BID])?></td>
                </tr>
                <?php
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
      <?php
    }
    ?>
</section>
<div class="modal fade" id="browseOPD" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Browse</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('loadjs')?>
<script type="text/javascript">
$(document).ready(function(){
  $('#browseOPD').on('show.bs.modal', function (event) {
      var modalBody = $(".modal-body", $("#browseOPD"));
      $(this).removeData('bs.modal');
      modalBody.html("<p style='font-style: italic'>Loading..</p>");
      modalBody.load("<?=site_url("ajax/browse-opd")?>", function () {
          $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
              var kdSub = $(this).val().split('|');
              $("[name=Kd_Urusan]").val(kdSub[0]).change();
              $("[name=Kd_Bidang]").val(kdSub[1]).change();
              $("[name=Kd_Unit]").val(kdSub[2]).change();
              $("[name=Kd_Sub]").val(kdSub[3]).change();
          });
          $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
              $("[name=text-opd]").val($(this).val());
          });
      });
  });
  $('[name=Periode],[name=Kd_Urusan],[name=Kd_Bidang],[name=Kd_Unit],[name=Kd_Sub],[name=Tahun]').change(function() {
    $(this).closest('form').submit();
  });
});
</script>
<?php $this->load->view('footer')?>
