<?php $this->load->view('header') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>
<?php
$ruser = GetLoggedUser();
//$dusun = $this->db->count_all_results(TBL_MDUSUN);
//$dasawisma = $this->db->count_all_results(TBL_MDASAWISMA);
//$keluarga = $this->db->count_all_results(TBL_MKELUARGA);
?>
<style>
    .todo-list>li:hover {
        background-color: #ccc;
    }
</style>
<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-sm-6">
            <?php
            $this->db->where(COL_KD_TAHUN_FROM." <=", date("Y"));
            $this->db->where(COL_KD_TAHUN_TO." >=", date("Y"));
            $this->db->order_by(COL_KD_TAHUN_FROM, "desc");
            $rperiod = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();
            $rmisi = $this->db
            ->where(COL_KD_PEMDA, !empty($rperiod[COL_KD_PEMDA])?$rperiod[COL_KD_PEMDA]:-999)
            ->order_by(COL_KD_MISI, "asc")
            ->get(TBL_SAKIP_MPMD_MISI)
            ->result_array();
            $rtujuan = $this->db
            ->where(COL_KD_PEMDA, !empty($rperiod[COL_KD_PEMDA])?$rperiod[COL_KD_PEMDA]:-999)
            ->get(TBL_SAKIP_MPMD_TUJUAN)
            ->num_rows();
            $riktujuan = $this->db
            ->where(COL_KD_PEMDA, !empty($rperiod[COL_KD_PEMDA])?$rperiod[COL_KD_PEMDA]:-999)
            ->get(TBL_SAKIP_MPMD_IKTUJUAN)
            ->num_rows();
            $rsasaran = $this->db
            ->where(COL_KD_PEMDA, !empty($rperiod[COL_KD_PEMDA])?$rperiod[COL_KD_PEMDA]:-999)
            ->get(TBL_SAKIP_MPMD_SASARAN)
            ->num_rows();
            $riksasaran = $this->db
            ->where(COL_KD_PEMDA, !empty($rperiod[COL_KD_PEMDA])?$rperiod[COL_KD_PEMDA]:-999)
            ->get(TBL_SAKIP_MPMD_IKSASARAN)
            ->num_rows();
            ?>
            <div class="box box-danger box-solid">
                <div class="box-header">
                    <h3 class="box-title" style="font-weight: bold"><?=GetSetting('SETTING_ORG_REGION')?></h3>
                </div>
                <div class="box-body" style="padding: 0 !important">
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <td style="white-space: nowrap">PERIODE</td>
                            <td style="width: 10px">:</td>
                            <td colspan="5"><b><?=$rperiod?$rperiod[COL_KD_TAHUN_FROM]." s.d ".$rperiod[COL_KD_TAHUN_TO]:"-"?></b></td>
                        </tr>
                        <tr>
                            <td style="white-space: nowrap">KEPALA DAERAH</td>
                            <td style="width: 10px">:</td>
                            <td colspan="5"><b><?=$rperiod?strtoupper($rperiod[COL_NM_PEJABAT]):"-"?></b></td>
                        </tr>
                        <tr>
                            <td style="white-space: nowrap">VISI</td>
                            <td style="width: 10px">:</td>
                            <td colspan="5"><b><?=$rperiod?strtoupper($rperiod[COL_NM_VISI]):"-"?></b></td>
                        </tr>
                        <tr>
                            <td style="white-space: nowrap">MISI</td>
                            <td style="width: 10px">:</td>
                            <td class="text-right">
                              <b>
                                <?php
                                if($ruser[COL_ROLEID]==ROLEADMIN) {
                                  ?>
                                  <a href="<?=site_url('sakip/mpemda/period')?>"><span class="badge bg-yellow"><?=number_format(count($rmisi))?></span></a>
                                  <?php
                                } else {
                                  ?>
                                  <span class="badge bg-yellow"><?=number_format(count($rmisi))?></span>
                                  <?php
                                }
                                ?>
                              </b>
                            </td>
                            <td colspan="4"></td>
                        </tr>
                        <tr>
                            <td style="white-space: nowrap">TUJUAN</td>
                            <td style="width: 10px">:</td>
                            <td class="text-right">
                              <?php
                              if($ruser[COL_ROLEID]==ROLEADMIN) {
                                ?>
                                <a href="<?=site_url('sakip/mpemda/tujuan')?>"><span class="badge bg-yellow"><?=number_format($rtujuan)?></span></a>
                                <?php
                              } else {
                                ?>
                                <span class="badge bg-yellow"><?=number_format($rtujuan)?></span>
                                <?php
                              }
                              ?>
                            </td>
                            <td style="width: 50px"></td>

                            <td style="width: 100px; white-space: nowrap">INDIKATOR TUJUAN</td>
                            <td style="width: 10px">:</td>
                            <td class="text-right">
                              <?php
                              if($ruser[COL_ROLEID]==ROLEADMIN) {
                                ?>
                                <a href="<?=site_url('sakip/mpemda/tujuan')?>"><span class="badge bg-yellow"><?=number_format($riktujuan)?></span></a>
                                <?php
                              } else {
                                ?>
                                <span class="badge bg-yellow"><?=number_format($riktujuan)?></span>
                                <?php
                              }
                              ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="white-space: nowrap">SASARAN</td>
                            <td style="width: 10px">:</td>
                            <td class="text-right">
                              <?php
                              if($ruser[COL_ROLEID]==ROLEADMIN) {
                                ?>
                                <a href="<?=site_url('sakip/mpemda/sasaran')?>"><span class="badge bg-yellow"><?=number_format($rsasaran)?></span></a>
                                <?php
                              } else {
                                ?>
                                <span class="badge bg-yellow"><?=number_format($rsasaran)?></span>
                                <?php
                              }
                              ?>
                            </td>
                            <td style="width: 50px"></td>

                            <td style="width: 100px; white-space: nowrap">INDIKATOR SASARAN</td>
                            <td style="width: 10px">:</td>
                            <td class="text-right">
                              <?php
                              if($ruser[COL_ROLEID]==ROLEADMIN) {
                                ?>
                                <a href="<?=site_url('sakip/mpemda/sasaran')?>"><span class="badge bg-yellow"><?=number_format($riksasaran)?></span></a>
                                <?php
                              } else {
                                ?>
                                <span class="badge bg-yellow"><?=number_format($riksasaran)?></span>
                                <?php
                              }
                              ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
          <?php
          if($ruser[COL_ROLEID]==ROLEADMIN) {
            $sumrka = $this->db
            ->select_sum(COL_TOTAL)
            ->where(COL_KD_PEMDA, $rperiod[COL_KD_PEMDA])
            ->where(COL_KD_TAHUN, date('Y'))
            ->get(TBL_SAKIP_MSUBBID_KEGIATAN)
            ->row_array();
            $sumdpa = $this->db
            ->select_sum(COL_BUDGET)
            ->where(COL_KD_PEMDA, $rperiod[COL_KD_PEMDA])
            ->where(COL_KD_TAHUN, date('Y'))
            ->get(TBL_SAKIP_DPA_KEGIATAN)
            ->row_array();
            $sumrealisasi = $this->db
            ->select('sum(Anggaran_TW1+Anggaran_TW2+Anggaran_TW3+Anggaran_TW4) as Anggaran')
            ->where(COL_KD_PEMDA, $rperiod[COL_KD_PEMDA])
            ->where(COL_KD_TAHUN, date('Y'))
            ->get(TBL_SAKIP_DPA_KEGIATAN)
            ->row_array();
            ?>
            <div class="box box-danger box-solid">
                <div class="box-header">
                    <h5 class="box-title" style="font-weight: bold; font-size: 18px">TAHUN ANGGARAN <?=date('Y')?></h5>
                </div>
                <div class="box-body" style="padding: 0 !important">
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <td style="width: 100px; white-space: nowrap">TOTAL RKA</td>
                            <td style="width: 10px">:</td>
                            <td class="text-right"><b><span class="badge bg-default">Rp. <?=number_format(!empty($sumrka)?$sumrka[COL_TOTAL]:0)?></span></b></td>
                        </tr>
                        <tr>
                            <td style="width: 100px; white-space: nowrap">TOTAL DPA</td>
                            <td style="width: 10px">:</td>
                            <td class="text-right"><b><span class="badge bg-default">Rp. <?=number_format(!empty($sumrka)?$sumdpa[COL_BUDGET]:0)?></span></b></td>
                        </tr>
                        <tr>
                            <td style="width: 100px; white-space: nowrap">TOTAL REALISASI</td>
                            <td style="width: 10px">:</td>
                            <td class="text-right"><b><span class="badge bg-default">Rp. <?=number_format(!empty($sumrealisasi)?$sumrealisasi['Anggaran']:0)?></span></b></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php
          } else {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $kdUrusan = $strOPD[0];
            $kdBidang = $strOPD[1];
            $kdUnit = $strOPD[2];
            $kdSub = $strOPD[3];

            $opdcond = array(COL_KD_URUSAN=>$kdUrusan, COL_KD_BIDANG=>$kdBidang, COL_KD_UNIT=>$kdUnit, COL_KD_SUB=>$kdSub);

            $rsubunit = $this->db
            ->where($opdcond)
            ->get('ref_sub_unit')
            ->row_array();

            if(!empty($rsubunit)) {
              $rtujuanopd = $this->db
              ->where($opdcond)
              ->group_by(COL_KD_TUJUANOPD)
              ->get(TBL_SAKIP_MOPD_TUJUAN)
              ->num_rows();
              $riktujuanopd = $this->db
              ->where($opdcond)
              ->where(array(COL_KD_PEMDA=>$rperiod[COL_KD_PEMDA]))
              ->group_by(COL_KD_TUJUANOPD)
              ->get(TBL_SAKIP_MOPD_IKTUJUAN)
              ->num_rows();
              $rsasaranopd = $this->db
              ->where($opdcond)
              ->where(array(COL_KD_PEMDA=>$rperiod[COL_KD_PEMDA]))
              ->get(TBL_SAKIP_MOPD_SASARAN)
              ->num_rows();
              $riksasaranopd = $this->db
              ->where($opdcond)
              ->where(array(COL_KD_PEMDA=>$rperiod[COL_KD_PEMDA]))
              ->get(TBL_SAKIP_MOPD_IKSASARAN)
              ->num_rows();
              $rprogram = $this->db
              ->where($opdcond)
              ->where(array(COL_KD_PEMDA=>$rperiod[COL_KD_PEMDA]))
              ->where(COL_KD_TAHUN, date('Y'))
              ->get(TBL_SAKIP_MBID_PROGRAM)
              ->num_rows();
              $rprogram_ = $this->db
              ->where($opdcond)
              ->where(array(COL_KD_PEMDA=>$rperiod[COL_KD_PEMDA]))
              ->where(COL_KD_TAHUN, date('Y'))
              ->get(TBL_SAKIP_DPA_PROGRAM)
              ->num_rows();
              $rkegiatan = $this->db
              ->where($opdcond)
              ->where(array(COL_KD_PEMDA=>$rperiod[COL_KD_PEMDA]))
              ->where(COL_KD_TAHUN, date('Y'))
              ->get(TBL_SAKIP_MSUBBID_KEGIATAN)
              ->num_rows();
              $rkegiatan_ = $this->db
              ->where($opdcond)
              ->where(array(COL_KD_PEMDA=>$rperiod[COL_KD_PEMDA]))
              ->where(COL_KD_TAHUN, date('Y'))
              ->get(TBL_SAKIP_DPA_KEGIATAN)
              ->num_rows();
              $sumrka = $this->db
              ->select_sum(COL_TOTAL)
              ->where($opdcond)
              ->where(array(COL_KD_PEMDA=>$rperiod[COL_KD_PEMDA]))
              ->where(COL_KD_TAHUN, date('Y'))
              ->get(TBL_SAKIP_MSUBBID_KEGIATAN)
              ->row_array();
              $sumdpa = $this->db
              ->select_sum(COL_BUDGET)
              ->where($opdcond)
              ->where(array(COL_KD_PEMDA=>$rperiod[COL_KD_PEMDA]))
              ->where(COL_KD_TAHUN, date('Y'))
              ->get(TBL_SAKIP_DPA_KEGIATAN)
              ->row_array();
              $sumrealisasi = $this->db
              ->select('sum(Anggaran_TW1+Anggaran_TW2+Anggaran_TW3+Anggaran_TW4) as Anggaran')
              ->where($opdcond)
              ->where(array(COL_KD_PEMDA=>$rperiod[COL_KD_PEMDA]))
              ->where(COL_KD_TAHUN, date('Y'))
              ->get(TBL_SAKIP_DPA_KEGIATAN)
              ->row_array();
              ?>
              <div class="box box-danger box-solid">
                  <div class="box-header">
                      <h5 class="box-title" style="font-weight: bold; font-size: 18px"><?=strtoupper($rsubunit[COL_NM_SUB_UNIT])?></h5>
                  </div>
                  <div class="box-body" style="padding: 0 !important">
                      <table class="table table-striped">
                          <tbody>
                          <tr>
                              <td style="width: 100px; white-space: nowrap">TAHUN ANGGARAN</td>
                              <td style="width: 10px">:</td>
                              <td colspan="5"><b><?=date('Y')?></b></td>
                          </tr>
                          <tr>
                              <td style="width: 100px; white-space: nowrap">KEPALA OPD</td>
                              <td style="width: 10px">:</td>
                              <td colspan="5"><b><?=$rperiod?strtoupper($rsubunit[COL_NM_PIMPINAN]):"-"?></b></td>
                          </tr>
                          <tr>
                              <td style="width: 100px; white-space: nowrap">TUJUAN</td>
                              <td style="width: 10px">:</td>
                              <td class="text-right">
                                <a href="<?=site_url('sakip/mopd/tujuan')?>"><span class="badge bg-yellow"><?=number_format($rtujuanopd)?></span></a>
                              </td>
                              <td style="width: 50px"></td>

                              <td style="width: 100px; white-space: nowrap">INDIKATOR TUJUAN</td>
                              <td style="width: 10px">:</td>
                              <td class="text-right">
                                <a href="<?=site_url('sakip/mopd/tujuan')?>"><span class="badge bg-yellow"><?=number_format($riktujuanopd)?></span></a>
                              </td>
                          </tr>
                          <tr>
                              <td style="white-space: nowrap">SASARAN</td>
                              <td style="width: 10px">:</td>
                              <td class="text-right">
                                <a href="<?=site_url('sakip/mopd/sasaran')?>"><span class="badge bg-yellow"><?=number_format($rsasaranopd)?></span></a>
                              </td>
                              <td style="width: 50px"></td>

                              <td style="width: 100px; white-space: nowrap">INDIKATOR SASARAN</td>
                              <td style="width: 10px">:</td>
                              <td class="text-right">
                                <a href="<?=site_url('sakip/mopd/sasaran')?>"><span class="badge bg-yellow"><?=number_format($riksasaranopd)?></span></a>
                              </td>
                          </tr>
                          <tr>
                              <td style="white-space: nowrap">RENJA - PROGRAM</td>
                              <td style="width: 10px">:</td>
                              <td class="text-right">
                                <a href="<?=site_url('sakip/renja/index-renja')?>"><span class="badge bg-yellow"><?=number_format($rprogram)?></span></a>
                              </td>
                              <td style="width: 50px"></td>

                              <td style="width: 100px; white-space: nowrap">DPA - PROGRAM</td>
                              <td style="width: 10px">:</td>
                              <td class="text-right">
                                <a href="<?=site_url('sakip/renja/index-dpa')?>"><span class="badge bg-yellow"><?=number_format($rprogram_)?></span></a>
                              </td>
                          </tr>
                          <tr>
                              <td style="white-space: nowrap">RENJA - KEGIATAN</td>
                              <td style="width: 10px">:</td>
                              <td class="text-right">
                                <a href="<?=site_url('sakip/renja/index-renja')?>"><span class="badge bg-yellow"><?=number_format($rkegiatan)?></span></a>
                              </td>
                              <td style="width: 50px"></td>

                              <td style="width: 100px; white-space: nowrap">DPA - KEGIATAN</td>
                              <td style="width: 10px">:</td>
                              <td class="text-right">
                                <a href="<?=site_url('sakip/renja/index-dpa')?>"><span class="badge bg-yellow"><?=number_format($rkegiatan_)?></span></a>
                              </td>
                          </tr>
                          <tr>
                              <td style="width: 100px; white-space: nowrap">TOTAL RKA</td>
                              <td style="width: 10px">:</td>
                              <td class="text-right" colspan="5"><b><span class="badge bg-default">Rp. <?=number_format(!empty($sumrka)?$sumrka[COL_TOTAL]:0)?></span></b></td>
                          </tr>
                          <tr>
                              <td style="width: 100px; white-space: nowrap">TOTAL DPA</td>
                              <td style="width: 10px">:</td>
                              <td class="text-right" colspan="5"><b><span class="badge bg-default">Rp. <?=number_format(!empty($sumrka)?$sumdpa[COL_BUDGET]:0)?></span></b></td>
                          </tr>
                          <tr>
                              <td style="width: 100px; white-space: nowrap">TOTAL REALISASI</td>
                              <td style="width: 10px">:</td>
                              <td class="text-right" colspan="5"><b><span class="badge bg-default">Rp. <?=number_format(!empty($sumrealisasi)?$sumrealisasi['Anggaran']:0)?></span></b></td>
                          </tr>
                          </tbody>
                      </table>
                  </div>
              </div>
              <?php
            }
          }
          ?>
        </div>
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<?php $this->load->view('loadjs')?>
<?php $this->load->view('footer')?>
