<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 3/14/2019
 * Time: 11:31 PM
 */
$this->load->view('header');
$ruser = GetLoggedUser();
$eplandb = $this->load->database("eplan", true);
?>
    <section class="content-header">
        <h1> <?= $title ?> <small> Generate</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">TC. 27</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">
                        <?=form_open(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
                        <div class="form-group">
                            <label class="control-label col-sm-2">OPD</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-opd" value="<?= $edit ? $data[COL_KD_URUSAN].".".$data[COL_KD_BIDANG].".".$data[COL_KD_UNIT].".".$data[COL_KD_SUB]." ".$nmSub : ($ruser[COL_ROLEID] == ROLEKADIS ? $strOPD[0].".".$strOPD[1].".".$strOPD[2].".".$strOPD[3]." ".$nmSub : "")?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_URUSAN?>" value="<?= $edit ? $data[COL_KD_URUSAN] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[0]:"")?>" required   >
                                    <input type="hidden" name="<?=COL_KD_BIDANG?>" value="<?= $edit ? $data[COL_KD_BIDANG] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[1]:"")?>" required   >
                                    <input type="hidden" name="<?=COL_KD_UNIT?>" value="<?= $edit ? $data[COL_KD_UNIT] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[2]:"")?>" required   >
                                    <input type="hidden" name="<?=COL_KD_SUB?>" value="<?= $edit ? $data[COL_KD_SUB] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[3]:"")?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-opd" data-toggle="modal" data-target="#browseOPD" data-toggle="tooltip" data-placement="top" title="Pilih OPD" <?=($ruser[COL_ROLEID] == ROLEKADIS ? "disabled" : "")?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Tahun</label>
                            <div class="col-sm-2">
                                <input type="number" class="form-control" name="<?=COL_KD_TAHUN?>" value="<?= $edit ? $data[COL_KD_TAHUN] : date("Y")?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-2">Sumber Data</label>
                          <div class="col-sm-2">
                            <select class="form-control" name="SumberData">
                              <option value="INDUK" <?=!empty($SumberData)&&$SumberData=='INDUK'?'selected':''?>>INDUK</option>
                              <option value="PERUBAHAN" <?=!empty($SumberData)&&$SumberData=='PERUBAHAN'?'selected':''?>>PERUBAHAN</option>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12" style="text-align: right">
                                <button type="submit" class="btn btn-success btn-flat" title="Cetak" name="cetak" value="1"><i class="fa fa-print"></i> Cetak</button>
                                <button type="submit" class="btn btn-primary btn-flat" title="Lihat"><i class="fa fa-arrow-circle-right"></i> Lihat</button>
                            </div>
                        </div>
                        <?=form_close()?>
<?php if(!empty($data)) $this->load->view('report/rencana_aksi_partial'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="browseOPD" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    </div>
<?php $this->load->view('loadjs') ?>
    <script>
        $(document).ready(function() {
            $('.modal').on('hidden.bs.modal', function (event) {
                $(this).find(".modal-body").empty();
            });

            $('#browseOPD').on('show.bs.modal', function (event) {
                var modalBody = $(".modal-body", $("#browseOPD"));
                $(this).removeData('bs.modal');
                modalBody.html("<p style='font-style: italic'>Loading..</p>");
                modalBody.load("<?=site_url("ajax/browse-opd")?>", function () {
                    $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                        var kdSub = $(this).val().split('|');
                        $("[name=Kd_Urusan]").val(kdSub[0]);
                        $("[name=Kd_Bidang]").val(kdSub[1]);
                        $("[name=Kd_Unit]").val(kdSub[2]);
                        $("[name=Kd_Sub]").val(kdSub[3]);
                    });
                    $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                        $("[name=text-opd]").val($(this).val());
                    });
                });
            });
        });
    </script>
<?php $this->load->view('footer') ?>
