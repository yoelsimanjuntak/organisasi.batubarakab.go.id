<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 9/25/2019
 * Time: 10:49 PM
 */
if(!empty($cetak)) {
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=ESAKIP - Laporan DPA.xls");
}
?>
<style>
    .tbl-info td {
        padding: 4px;
    }
    td.blank {
        color: transparent !important;
    }
</style>
<div class="table-responsive">
    <table class="table table-bordered" style="font-size: 9pt !important;" border="1">
        <caption style="text-align: center">
            <h5><?="DOKUMEN RKA <br />".strtoupper($nmSub)." KABUPATEN BATU BARA<br />TAHUN ".$data[COL_KD_TAHUN]?>
        </caption>
        <thead>
        <tr>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
            <th>6</th>
        </tr>
        <tr>
            <th>NO</th>
            <th>PROGRAM</th>
            <th>ANGGARAN</th>
            <th>NO</th>
            <th>KEGIATAN</th>
            <th>ANGGARAN</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $count_prg = 1;
        $count_keg = 1;
        $sum_keg = 0;
        $sum_prog = 0;
        $last = array(
            COL_KD_PEMDA => "@@",
            COL_KD_MISI => "@@",
            COL_KD_TUJUAN => "@@",
            COL_KD_INDIKATORTUJUAN => "@@",
            COL_KD_SASARAN => "@@",
            COL_KD_INDIKATORSASARAN => "@@",
            COL_KD_TUJUANOPD => "@@",
            COL_KD_INDIKATORTUJUANOPD => "@@",
            COL_KD_SASARANOPD => "@@",
            COL_KD_INDIKATORSASARANOPD => "@@",
            COL_KD_URUSAN => "@@",
            COL_KD_BIDANG => "@@",
            COL_KD_UNIT => "@@",
            COL_KD_SUB => "@@",
            COL_KD_BID => "@@",
            COL_KD_SUBBID => "@@",
            COL_KD_PROGRAMOPD => "@@",
            COL_NM_PROGRAMOPD => "@@",
            COL_KD_SASARANPROGRAMOPD => "@@",
            COL_KD_KEGIATANOPD => "@@",
            COL_NM_KEGIATANOPD => "@@",
            COL_KD_INDIKATORKEGIATANOPD => "@@",
            //"Satuan1" => "@@",
            //"Target1" => "@@",
            "Total1" => "@@",
            //COL_NM_KEGIATANOPD => "@@",
            //COL_KD_INDIKATORKEGIATANOPD => "@@",
            "Satuan2" => "@@",
            "Target2" => "@@",
            "Total2" => "@@"
        );
        foreach($program as $prg) {
            if($last[COL_NM_PROGRAMOPD] != $prg[COL_NM_PROGRAMOPD])  {
                //$count_keg = 1;
            }
            $indikatorprogram = $this->db
                ->where(COL_KD_URUSAN, $prg[COL_KD_URUSAN])
                ->where(COL_KD_BIDANG, $prg[COL_KD_BIDANG])
                ->where(COL_KD_UNIT, $prg[COL_KD_UNIT])
                ->where(COL_KD_SUB, $prg[COL_KD_SUB])
                //->where(COL_KD_BID, $prg[COL_KD_BID])

                ->where(COL_KD_PEMDA, $prg[COL_KD_PEMDA])
                ->where(COL_KD_MISI, $prg[COL_KD_MISI])
                ->where(COL_KD_TUJUAN, $prg[COL_KD_TUJUAN])
                ->where(COL_KD_INDIKATORTUJUAN, $prg[COL_KD_INDIKATORTUJUAN])
                ->where(COL_KD_SASARAN, $prg[COL_KD_SASARAN])
                ->where(COL_KD_INDIKATORSASARAN, $prg[COL_KD_INDIKATORSASARAN])
                ->where(COL_KD_TUJUANOPD, $prg[COL_KD_TUJUANOPD])
                ->where(COL_KD_INDIKATORTUJUANOPD, $prg[COL_KD_INDIKATORTUJUANOPD])
                ->where(COL_KD_SASARANOPD, $prg[COL_KD_SASARANOPD])
                ->where(COL_KD_INDIKATORSASARANOPD, $prg[COL_KD_INDIKATORSASARANOPD])
                ->where(COL_KD_PROGRAMOPD, $prg[COL_KD_PROGRAMOPD])
                ->where(COL_KD_SASARANPROGRAMOPD, $prg[COL_KD_SASARANPROGRAMOPD])
                ->where(COL_KD_TAHUN, $prg[COL_KD_TAHUN])
                ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_PROGRAMOPD, 'asc')
                ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_SASARANPROGRAMOPD, 'asc')
                ->get(TBL_SAKIP_DPA_PROGRAM_INDIKATOR)
                ->result_array();
            $q = $this->db->last_query();

            $indikatorprogram_ = "";
            $indikatorprogram_satuan = "";
            $indikatorprogram_target = "";
            if(count($indikatorprogram) > 0) {
                if(count($indikatorprogram) > 1) {
                    $indikatorprogram_ = "<ul style='margin-left: 0px; padding-left: 15px; text-align: left'>";
                    $indikatorprogram_satuan = "<ul style='margin-left: 0px; padding-left: 15px; text-align: left'>";
                    $indikatorprogram_target = "<ul style='margin-left: 0px; padding-left: 15px; text-align: left'>";
                    foreach($indikatorprogram as $s) {
                        $indikatorprogram_ .= "<li>".$s[COL_NM_INDIKATORPROGRAMOPD]."</li>";
                        $indikatorprogram_satuan .= "<li>".$s[COL_KD_SATUAN]."</li>";
                        $indikatorprogram_target .= "<li>".number_format($s[COL_TARGET], 2)."</li>";
                    }
                    $indikatorprogram_ .= "</ul>";
                    $indikatorprogram_satuan .= "</ul>";
                    $indikatorprogram_target .= "</ul>";
                }
                else {
                    $indikatorprogram_ = $indikatorprogram[0][COL_NM_INDIKATORPROGRAMOPD];
                    $indikatorprogram_satuan = $indikatorprogram[0][COL_KD_SATUAN];
                    $indikatorprogram_target = number_format($indikatorprogram[0][COL_TARGET], 2);
                }
            }

            /*$indikatorkegiatan = $this->db
                ->where(COL_KD_URUSAN, $prg[COL_KD_URUSAN])
                ->where(COL_KD_BIDANG, $prg[COL_KD_BIDANG])
                ->where(COL_KD_UNIT, $prg[COL_KD_UNIT])
                ->where(COL_KD_SUB, $prg[COL_KD_SUB])
                ->where(COL_KD_BID, $prg[COL_KD_BID])

                ->where(COL_KD_PEMDA, $prg[COL_KD_PEMDA])
                ->where(COL_KD_MISI, $prg[COL_KD_MISI])
                ->where(COL_KD_TUJUAN, $prg[COL_KD_TUJUAN])
                ->where(COL_KD_INDIKATORTUJUAN, $prg[COL_KD_INDIKATORTUJUAN])
                ->where(COL_KD_SASARAN, $prg[COL_KD_SASARAN])
                ->where(COL_KD_INDIKATORSASARAN, $prg[COL_KD_INDIKATORSASARAN])
                ->where(COL_KD_TUJUANOPD, $prg[COL_KD_TUJUANOPD])
                ->where(COL_KD_INDIKATORTUJUANOPD, $prg[COL_KD_INDIKATORTUJUANOPD])
                ->where(COL_KD_SASARANOPD, $prg[COL_KD_SASARANOPD])
                ->where(COL_KD_INDIKATORSASARANOPD, $prg[COL_KD_INDIKATORSASARANOPD])
                ->where(COL_KD_PROGRAMOPD, $prg[COL_KD_PROGRAMOPD])
                ->where(COL_KD_SASARANPROGRAMOPD, $prg[COL_KD_SASARANPROGRAMOPD])
                ->where(COL_KD_KEGIATANOPD, $prg[COL_KD_KEGIATANOPD])
                //->where(COL_KD_SASARANKEGIATANOPD, $prg[COL_KD_SASARANKEGIATANOPD])
                ->where(COL_KD_TAHUN, $prg[COL_KD_TAHUN])
                ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_PROGRAMOPD, 'asc')
                ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_SASARANPROGRAMOPD, 'asc')
                ->get(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR)
                ->result_array();

            $indikatorkegiatan_ = "";
            if(count($indikatorkegiatan) > 0) {
                $indikatorkegiatan_ = "<ul style='margin-left: 0px; padding-left: 15px; text-align: left'>";
                foreach($indikatorkegiatan as $s) {
                    $indikatorkegiatan_ .= "<li>".$s[COL_NM_INDIKATORKEGIATANOPD]."</li>";
                }
                $indikatorkegiatan_ .= "</ul>";
            }*/

            $curr_prg = $prg[COL_KD_PEMDA].$prg[COL_KD_MISI].$prg[COL_KD_TUJUAN].$prg[COL_KD_INDIKATORTUJUAN].$prg[COL_KD_SASARAN].$prg[COL_KD_INDIKATORSASARAN].$prg[COL_KD_TUJUANOPD].$prg[COL_KD_INDIKATORTUJUANOPD].$prg[COL_KD_SASARANOPD].$prg[COL_KD_INDIKATORSASARANOPD].$prg[COL_KD_PROGRAMOPD];
            $last_prg = $last[COL_KD_PEMDA].$last[COL_KD_MISI].$last[COL_KD_TUJUAN].$last[COL_KD_INDIKATORTUJUAN].$last[COL_KD_SASARAN].$last[COL_KD_INDIKATORSASARAN].$last[COL_KD_TUJUANOPD].$last[COL_KD_INDIKATORTUJUANOPD].$last[COL_KD_SASARANOPD].$last[COL_KD_INDIKATORSASARANOPD].$last[COL_KD_PROGRAMOPD];
            ?>
            <tr>
                <?php
                if(/*$prg[COL_NM_PROGRAMOPD]!=$last[COL_NM_PROGRAMOPD]*/$curr_prg!=$last_prg){
                    ?>
                    <td <?=$prg["count_program_span"]>1?'rowspan="'.$prg["count_program_span"].'"':''?>><?=/*"PRG#".*/$count_prg?></td>
                    <td <?=$prg["count_program_span"]>1?'rowspan="'.$prg["count_program_span"].'"':''?>><?=$prg[COL_NM_PROGRAMOPD]?></td>
                    <?php
                }
                ?>

                <?php
              if($curr_prg/*.".".$prg[COL_KD_BID].".".$prg[COL_KD_SASARANPROGRAMOPD]*/!=$last_prg/*.".".$last[COL_KD_BID].".".$last[COL_KD_SASARANPROGRAMOPD]*/){
                    $count_keg = 1;
                    ?>
                    <td <?=$prg[/*"count_sasaranprog_span"*/"count_program_span"]>1?'rowspan="'.$prg[/*"count_sasaranprog_span"*/"count_program_span"].'"':''?> style="text-align: right"><?=number_format($prg["sum_program_total"], 0)?></td>
                <?php
                    $sum_prog += $prg["sum_program_total"];
                }
                ?>
                <?php
                if($curr_prg.".".$prg[COL_KD_SASARANPROGRAMOPD].".".$prg[COL_KD_KEGIATANOPD].".".$prg[COL_KD_BID].".".$prg[COL_KD_SUBBID]!=$last_prg.".".$last[COL_KD_SASARANPROGRAMOPD].".".$last[COL_KD_KEGIATANOPD].".".$last[COL_KD_BID].".".$last[COL_KD_SUBBID]){
                    ?>
                    <td <?=$prg["count_kegiatan_span"]>1?'rowspan="'.$prg["count_kegiatan_span"].'"':''?>><?=$count_keg?></td>
                    <td <?=$prg["count_kegiatan_span"]>1?'rowspan="'.$prg["count_kegiatan_span"].'"':''?>><?=$prg[COL_NM_KEGIATANOPD]?></td>
                <?php
                }
                ?>
                <?php
              if(/*$curr_prg.".".$prg[COL_KD_SASARANPROGRAMOPD].".".$prg[COL_KD_KEGIATANOPD]!=$last_prg.".".$last[COL_KD_SASARANPROGRAMOPD].".".$last[COL_KD_KEGIATANOPD]*/$curr_prg.".".$prg[COL_KD_SASARANPROGRAMOPD].".".$prg[COL_KD_KEGIATANOPD].".".$prg[COL_KD_BID].".".$prg[COL_KD_SUBBID]!=$last_prg.".".$last[COL_KD_SASARANPROGRAMOPD].".".$last[COL_KD_KEGIATANOPD].".".$last[COL_KD_BID].".".$last[COL_KD_SUBBID]){
                    ?>
                    <td style="text-align: right" <?=$prg["count_kegiatan_span"]>1?'rowspan="'.$prg["count_kegiatan_span"].'"':''?>><?=number_format($prg["Budget2"], 0)?></td>
                <?php
                    $sum_keg += $prg["Budget2"];
                }
                ?>
                <!--<td style="text-align: right"><?=number_format($prg["Budget2"], 0)?></td>-->
            </tr>
            <?php
            if($curr_prg!=$last_prg)  {
                $count_prg++;
            }
            if($curr_prg.".".$prg[COL_KD_SASARANPROGRAMOPD].".".$prg[COL_KD_KEGIATANOPD].".".$prg[COL_KD_BID].".".$prg[COL_KD_SUBBID]!=$last_prg.".".$last[COL_KD_SASARANPROGRAMOPD].".".$last[COL_KD_KEGIATANOPD].".".$last[COL_KD_BID].".".$last[COL_KD_SUBBID])  {
                $count_keg++;
            }
            $last = array(
                COL_KD_PEMDA => $prg[COL_KD_PEMDA],
                COL_KD_MISI => $prg[COL_KD_MISI],
                COL_KD_TUJUAN => $prg[COL_KD_TUJUAN],
                COL_KD_INDIKATORTUJUAN => $prg[COL_KD_INDIKATORTUJUAN],
                COL_KD_SASARAN => $prg[COL_KD_SASARAN],
                COL_KD_INDIKATORSASARAN => $prg[COL_KD_INDIKATORSASARAN],
                COL_KD_TUJUANOPD => $prg[COL_KD_TUJUANOPD],
                COL_KD_INDIKATORTUJUANOPD => $prg[COL_KD_INDIKATORTUJUANOPD],
                COL_KD_SASARANOPD => $prg[COL_KD_SASARANOPD],
                COL_KD_INDIKATORSASARANOPD => $prg[COL_KD_INDIKATORSASARANOPD],
                COL_KD_URUSAN => $prg[COL_KD_URUSAN],
                COL_KD_BIDANG => $prg[COL_KD_BIDANG],
                COL_KD_UNIT => $prg[COL_KD_UNIT],
                COL_KD_SUB => $prg[COL_KD_SUB],
                COL_KD_BID => $prg[COL_KD_BID],
                COL_KD_SUBBID => $prg[COL_KD_SUBBID],
                COL_KD_PROGRAMOPD => $prg[COL_KD_PROGRAMOPD],
                COL_NM_PROGRAMOPD => $prg[COL_NM_PROGRAMOPD],
                COL_KD_SASARANPROGRAMOPD => $prg[COL_KD_SASARANPROGRAMOPD],
                COL_KD_KEGIATANOPD => $prg[COL_KD_KEGIATANOPD],
                COL_NM_KEGIATANOPD => $prg[COL_NM_KEGIATANOPD],
                COL_KD_INDIKATORKEGIATANOPD => $prg[COL_KD_INDIKATORKEGIATANOPD],
                //"Satuan1" => "@@",
                //"Target1" => "@@",
                "Total1" => $prg["Total1"],
                //COL_NM_KEGIATANOPD => "@@",
                //COL_KD_INDIKATORKEGIATANOPD => "@@",
                "Satuan2" => $prg["Satuan2"],
                "Target2" => $prg["Target2"],
                "Total2" => $prg["Total2"]
            );
        }
        ?>
        <tr>
            <td style="text-align: right" colspan="2"><b>TOTAL</b></td>
            <td style="text-align: right"><?=number_format($sum_prog, 0)?></td>
            <td style="text-align: right" colspan="2"><b>TOTAL</b></td>
            <td style="text-align: right"><?=number_format($sum_keg, 0)?></td>
        </tr>
        </tbody>
    </table>
</div>
