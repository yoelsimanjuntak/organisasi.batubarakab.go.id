<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 3/17/2019
 * Time: 8:58 PM
 */
$arrMisi = array();
$eplandb = $this->load->database("eplan", true);
$arrSasaranPmd = array();
foreach($misi as $m) {
    $this->db->select("CONCAT(sakip_mopd_iksasaran.Kd_Pemda, sakip_mopd_iksasaran.Kd_Misi, sakip_mopd_iksasaran.Kd_Tujuan, sakip_mopd_iksasaran.KD_IndikatorTujuan, sakip_mopd_iksasaran.Kd_Sasaran) as ID");
    $this->db->distinct();
    $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_PEMDA,"inner");
    $this->db->where(TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_PEMDA, $m[COL_KD_PEMDA]);
    $this->db->where(TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_MISI, $m[COL_KD_MISI]);
    $this->db->where(TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_URUSAN, $this->input->get(COL_KD_URUSAN));
    $this->db->where(TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_BIDANG, $this->input->get(COL_KD_BIDANG));
    $this->db->where(TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_UNIT, $this->input->get(COL_KD_UNIT));
    $this->db->where(TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SUB, $this->input->get(COL_KD_SUB));
    $rtujuan_distinct = $this->db->get(TBL_SAKIP_MOPD_IKSASARAN)->result_array();

    $distinct_ = array();
    if(empty($rtujuan_distinct) || count($rtujuan_distinct) == 0) {
        continue;
    }
    else {
        foreach($rtujuan_distinct as $d) {
            $distinct_[] = $d["ID"];
        }
    }

    $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MPMD_SASARAN.".".COL_KD_PEMDA,"inner");
    $this->db->where(TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_PEMDA, $m[COL_KD_PEMDA]);
    $this->db->where(TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_MISI, $m[COL_KD_MISI]);
    $this->db->where_in("CONCAT(sakip_mpmd_sasaran.Kd_Pemda, sakip_mpmd_sasaran.Kd_Misi, sakip_mpmd_sasaran.Kd_Tujuan, sakip_mpmd_sasaran.KD_IndikatorTujuan, sakip_mpmd_sasaran.Kd_Sasaran)", $distinct_);
    $this->db->order_by(TBL_SAKIP_MPMD_SASARAN.".".COL_KD_MISI, 'asc');
    $this->db->order_by(TBL_SAKIP_MPMD_SASARAN.".".COL_KD_SASARAN, 'asc');
    $rpmdsasaran = $this->db->get(TBL_SAKIP_MPMD_SASARAN)->result_array();

    //echo $this->db->last_query();
    //echo var_dump($distinct_);
    //return;

    foreach($rpmdsasaran as $spmd) {
        $this->db->select("CONCAT(sakip_mopd_iksasaran.Kd_Pemda, sakip_mopd_iksasaran.Kd_Misi, sakip_mopd_iksasaran.Kd_Tujuan, sakip_mopd_iksasaran.KD_IndikatorTujuan, sakip_mopd_iksasaran.Kd_Sasaran, sakip_mopd_iksasaran.Kd_IndikatorSasaran) as ID");
        $this->db->distinct();
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_PEMDA,"inner");
        $this->db->where(TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_PEMDA, $m[COL_KD_PEMDA]);
        $this->db->where(TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_MISI, $m[COL_KD_MISI]);
        $this->db->where(TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_URUSAN, $this->input->get(COL_KD_URUSAN));
        $this->db->where(TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_BIDANG, $this->input->get(COL_KD_BIDANG));
        $this->db->where(TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_UNIT, $this->input->get(COL_KD_UNIT));
        $this->db->where(TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SUB, $this->input->get(COL_KD_SUB));
        $r_iksasaran_distinct = $this->db->get(TBL_SAKIP_MOPD_IKSASARAN)->result_array();

        $distinct_ = array();
        if(empty($r_iksasaran_distinct) || count($r_iksasaran_distinct) == 0) {
            continue;
        }
        else {
            foreach($r_iksasaran_distinct as $d) {
                $distinct_[] = $d["ID"];
            }
        }

        $arrIkSasaranPmd = array();
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_PEMDA,"inner");
        $this->db->where(TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_PEMDA, $m[COL_KD_PEMDA]);
        $this->db->where(TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_MISI, $m[COL_KD_MISI]);
        $this->db->where(TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_TUJUAN, $spmd[COL_KD_TUJUAN]);
        $this->db->where(TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN, $spmd[COL_KD_INDIKATORTUJUAN]);
        $this->db->where(TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_SASARAN, $spmd[COL_KD_SASARAN]);
        $this->db->where_in("CONCAT(sakip_mpmd_iksasaran.Kd_Pemda, sakip_mpmd_iksasaran.Kd_Misi, sakip_mpmd_iksasaran.Kd_Tujuan, sakip_mpmd_iksasaran.KD_IndikatorTujuan, sakip_mpmd_iksasaran.Kd_Sasaran, sakip_mpmd_iksasaran.Kd_IndikatorSasaran)", $distinct_);
        $this->db->order_by(TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_MISI, 'asc');
        $this->db->order_by(TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_SASARAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_INDIKATORSASARAN, 'asc');
        $rindikatorsaspmd = $this->db->get(TBL_SAKIP_MPMD_IKSASARAN)->result_array();

        foreach($rindikatorsaspmd as $ikspmd) {
            $arrTujuan = array();
            $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_PEMDA,"inner");
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_PEMDA, $m[COL_KD_PEMDA]);
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_MISI, $m[COL_KD_MISI]);
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUAN, $ikspmd[COL_KD_TUJUAN]);
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORTUJUAN, $ikspmd[COL_KD_INDIKATORTUJUAN]);
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SASARAN, $ikspmd[COL_KD_SASARAN]);
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORSASARAN, $ikspmd[COL_KD_INDIKATORSASARAN]);
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_URUSAN, $this->input->get(COL_KD_URUSAN));
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_BIDANG, $this->input->get(COL_KD_BIDANG));
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_UNIT, $this->input->get(COL_KD_UNIT));
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SUB, $this->input->get(COL_KD_SUB));
            $this->db->order_by(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_TUJUAN, 'asc');
            $rtujuan = $this->db->get(TBL_SAKIP_MOPD_TUJUAN)->result_array();
            foreach($rtujuan as $t) {
                $arrIkTujuan = array();
                $iktujuan = $this->db
                    ->where(COL_KD_URUSAN, $t[COL_KD_URUSAN])
                    ->where(COL_KD_BIDANG, $t[COL_KD_BIDANG])
                    ->where(COL_KD_UNIT, $t[COL_KD_UNIT])
                    ->where(COL_KD_SUB, $t[COL_KD_SUB])

                    ->where(COL_KD_PEMDA, $t[COL_KD_PEMDA])
                    ->where(COL_KD_MISI, $t[COL_KD_MISI])
                    ->where(COL_KD_TUJUAN, $t[COL_KD_TUJUAN])
                    ->where(COL_KD_INDIKATORTUJUAN, $t[COL_KD_INDIKATORTUJUAN])
                    ->where(COL_KD_SASARAN, $t[COL_KD_SASARAN])
                    ->where(COL_KD_INDIKATORSASARAN, $t[COL_KD_INDIKATORSASARAN])
                    ->where(COL_KD_TUJUANOPD, $t[COL_KD_TUJUANOPD])
                    ->order_by(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORTUJUANOPD, 'asc')
                    ->get(TBL_SAKIP_MOPD_IKTUJUAN)
                    ->result_array();

                $arrSasaran = array();
                $sasaran = $this->db
                    ->where(COL_KD_URUSAN, $t[COL_KD_URUSAN])
                    ->where(COL_KD_BIDANG, $t[COL_KD_BIDANG])
                    ->where(COL_KD_UNIT, $t[COL_KD_UNIT])
                    ->where(COL_KD_SUB, $t[COL_KD_SUB])

                    ->where(COL_KD_PEMDA, $t[COL_KD_PEMDA])
                    ->where(COL_KD_MISI, $t[COL_KD_MISI])
                    ->where(COL_KD_TUJUAN, $t[COL_KD_TUJUAN])
                    ->where(COL_KD_INDIKATORTUJUAN, $t[COL_KD_INDIKATORTUJUAN])
                    ->where(COL_KD_SASARAN, $t[COL_KD_SASARAN])
                    ->where(COL_KD_INDIKATORSASARAN, $t[COL_KD_INDIKATORSASARAN])
                    ->where(COL_KD_TUJUANOPD, $t[COL_KD_TUJUANOPD])
                    //->where(COL_KD_INDIKATORTUJUANOPD, $t[COL_KD_INDIKATORTUJUANOPD])
                    ->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARANOPD, 'asc')
                    ->get(TBL_SAKIP_MOPD_SASARAN)
                    ->result_array();

                foreach($sasaran as $s) {
                    $arrIkSasaran = array();
                    $iksasaran = $this->db
                        ->where(COL_KD_URUSAN, $s[COL_KD_URUSAN])
                        ->where(COL_KD_BIDANG, $s[COL_KD_BIDANG])
                        ->where(COL_KD_UNIT, $s[COL_KD_UNIT])
                        ->where(COL_KD_SUB, $s[COL_KD_SUB])

                        ->where(COL_KD_PEMDA, $s[COL_KD_PEMDA])
                        ->where(COL_KD_MISI, $s[COL_KD_MISI])
                        ->where(COL_KD_TUJUAN, $s[COL_KD_TUJUAN])
                        ->where(COL_KD_INDIKATORTUJUAN, $s[COL_KD_INDIKATORTUJUAN])
                        ->where(COL_KD_SASARAN, $s[COL_KD_SASARAN])
                        ->where(COL_KD_INDIKATORSASARAN, $s[COL_KD_INDIKATORSASARAN])
                        ->where(COL_KD_TUJUANOPD, $s[COL_KD_TUJUANOPD])
                        ->where(COL_KD_INDIKATORTUJUANOPD, $s[COL_KD_INDIKATORTUJUANOPD])
                        ->where(COL_KD_SASARANOPD, $s[COL_KD_SASARANOPD])
                        ->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORSASARANOPD, 'asc')
                        ->get(TBL_SAKIP_MOPD_IKSASARAN)
                        ->result_array();

                    $arrProgram = array();
                    $qselect = @"*, (SELECT SUM(COALESCE(sakip_dpa_kegiatan.Pergeseran, sakip_dpa_kegiatan.Budget)) FROM `sakip_dpa_kegiatan`
                                WHERE `sakip_dpa_kegiatan`.`Kd_Urusan` = `sakip_dpa_program`.`Kd_Urusan`
                                AND `sakip_dpa_kegiatan`.`Kd_Bidang` = `sakip_dpa_program`.`Kd_Bidang`
                                AND `sakip_dpa_kegiatan`.`Kd_Unit` = `sakip_dpa_program`.`Kd_Unit`
                                AND `sakip_dpa_kegiatan`.`Kd_Sub` = `sakip_dpa_program`.`Kd_Sub`
                                AND `sakip_dpa_kegiatan`.`Kd_Bid` = `sakip_dpa_program`.`Kd_Bid`
                                AND `sakip_dpa_kegiatan`.`Kd_Pemda` = `sakip_dpa_program`.`Kd_Pemda`
                                AND `sakip_dpa_kegiatan`.`Kd_Misi` = `sakip_dpa_program`.`Kd_Misi`
                                AND `sakip_dpa_kegiatan`.`Kd_Tujuan` = `sakip_dpa_program`.`Kd_Tujuan`
                                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_dpa_program`.`Kd_IndikatorTujuan`
                                AND `sakip_dpa_kegiatan`.`Kd_Sasaran` = `sakip_dpa_program`.`Kd_Sasaran`
                                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_dpa_program`.`Kd_IndikatorSasaran`
                                AND `sakip_dpa_kegiatan`.`Kd_TujuanOPD` = `sakip_dpa_program`.`Kd_TujuanOPD`
                                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_program`.`Kd_IndikatorTujuanOPD`
                                AND `sakip_dpa_kegiatan`.`Kd_SasaranOPD` = `sakip_dpa_program`.`Kd_SasaranOPD`
                                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_program`.`Kd_IndikatorSasaranOPD`
                                AND `sakip_dpa_kegiatan`.`Kd_ProgramOPD` = `sakip_dpa_program`.`Kd_ProgramOPD`
                                AND `sakip_dpa_kegiatan`.`Kd_Tahun` = `sakip_dpa_program`.`Kd_Tahun`
                            ) AS TotalProgram";
                    if(!empty($SumberData) && $SumberData == 'PERUBAHAN') {
                      $qselect = @"*, (SELECT SUM(COALESCE(null, sakip_pdpa_kegiatan.Budget)) FROM `sakip_pdpa_kegiatan`
                                  WHERE `sakip_pdpa_kegiatan`.`Kd_Urusan` = `sakip_pdpa_program`.`Kd_Urusan`
                                  AND `sakip_pdpa_kegiatan`.`Kd_Bidang` = `sakip_pdpa_program`.`Kd_Bidang`
                                  AND `sakip_pdpa_kegiatan`.`Kd_Unit` = `sakip_pdpa_program`.`Kd_Unit`
                                  AND `sakip_pdpa_kegiatan`.`Kd_Sub` = `sakip_pdpa_program`.`Kd_Sub`
                                  AND `sakip_pdpa_kegiatan`.`Kd_Bid` = `sakip_pdpa_program`.`Kd_Bid`
                                  AND `sakip_pdpa_kegiatan`.`Kd_Pemda` = `sakip_pdpa_program`.`Kd_Pemda`
                                  AND `sakip_pdpa_kegiatan`.`Kd_Misi` = `sakip_pdpa_program`.`Kd_Misi`
                                  AND `sakip_pdpa_kegiatan`.`Kd_Tujuan` = `sakip_pdpa_program`.`Kd_Tujuan`
                                  AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_pdpa_program`.`Kd_IndikatorTujuan`
                                  AND `sakip_pdpa_kegiatan`.`Kd_Sasaran` = `sakip_pdpa_program`.`Kd_Sasaran`
                                  AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_pdpa_program`.`Kd_IndikatorSasaran`
                                  AND `sakip_pdpa_kegiatan`.`Kd_TujuanOPD` = `sakip_pdpa_program`.`Kd_TujuanOPD`
                                  AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_program`.`Kd_IndikatorTujuanOPD`
                                  AND `sakip_pdpa_kegiatan`.`Kd_SasaranOPD` = `sakip_pdpa_program`.`Kd_SasaranOPD`
                                  AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_program`.`Kd_IndikatorSasaranOPD`
                                  AND `sakip_pdpa_kegiatan`.`Kd_ProgramOPD` = `sakip_pdpa_program`.`Kd_ProgramOPD`
                                  AND `sakip_pdpa_kegiatan`.`Kd_Tahun` = `sakip_pdpa_program`.`Kd_Tahun`
                              ) AS TotalProgram";
                    }
                    $program = $this->db
                        ->select($qselect)
                        ->where(COL_KD_URUSAN, $s[COL_KD_URUSAN])
                        ->where(COL_KD_BIDANG, $s[COL_KD_BIDANG])
                        ->where(COL_KD_UNIT, $s[COL_KD_UNIT])
                        ->where(COL_KD_SUB, $s[COL_KD_SUB])

                        ->where(COL_KD_PEMDA, $s[COL_KD_PEMDA])
                        ->where(COL_KD_MISI, $s[COL_KD_MISI])
                        ->where(COL_KD_TUJUAN, $s[COL_KD_TUJUAN])
                        ->where(COL_KD_INDIKATORTUJUAN, $s[COL_KD_INDIKATORTUJUAN])
                        ->where(COL_KD_SASARAN, $s[COL_KD_SASARAN])
                        ->where(COL_KD_INDIKATORSASARAN, $s[COL_KD_INDIKATORSASARAN])
                        ->where(COL_KD_TUJUANOPD, $s[COL_KD_TUJUANOPD])
                        ->where(COL_KD_INDIKATORTUJUANOPD, $s[COL_KD_INDIKATORTUJUANOPD])
                        ->where(COL_KD_SASARANOPD, $s[COL_KD_SASARANOPD])
                        //->where(COL_KD_INDIKATORSASARANOPD, $s[COL_KD_INDIKATORSASARANOPD])
                        ->where(COL_KD_TAHUN, $this->input->get(COL_KD_TAHUN))
                        ->order_by(COL_KD_INDIKATORSASARANOPD, 'asc')
                        ->order_by(COL_KD_PROGRAMOPD, 'asc')
                        ->get((!empty($SumberData)&&$SumberData=='PERUBAHAN'?TBL_SAKIP_PDPA_PROGRAM:TBL_SAKIP_DPA_PROGRAM))
                        ->result_array();

                    foreach($program as $p) {
                      $rbidang = $this->db
                      ->where(COL_KD_URUSAN, $p[COL_KD_URUSAN])
                      ->where(COL_KD_BIDANG, $p[COL_KD_BIDANG])
                      ->where(COL_KD_UNIT, $p[COL_KD_UNIT])
                      ->where(COL_KD_SUB, $p[COL_KD_SUB])
                      ->where(COL_KD_BID, $p[COL_KD_BID])
                      ->get(TBL_SAKIP_MBID)
                      ->row_array();

                        $arrSasaranProgram = array();
                        $sasaranprogram = $this->db
                            ->where(COL_KD_URUSAN, $p[COL_KD_URUSAN])
                            ->where(COL_KD_BIDANG, $p[COL_KD_BIDANG])
                            ->where(COL_KD_UNIT, $p[COL_KD_UNIT])
                            ->where(COL_KD_SUB, $p[COL_KD_SUB])
                            ->where(COL_KD_BID, $p[COL_KD_BID])

                            ->where(COL_KD_PEMDA, $p[COL_KD_PEMDA])
                            ->where(COL_KD_MISI, $p[COL_KD_MISI])
                            ->where(COL_KD_TUJUAN, $p[COL_KD_TUJUAN])
                            ->where(COL_KD_INDIKATORTUJUAN, $p[COL_KD_INDIKATORTUJUAN])
                            ->where(COL_KD_SASARAN, $p[COL_KD_SASARAN])
                            ->where(COL_KD_INDIKATORSASARAN, $p[COL_KD_INDIKATORSASARAN])
                            ->where(COL_KD_TUJUANOPD, $p[COL_KD_TUJUANOPD])
                            ->where(COL_KD_INDIKATORTUJUANOPD, $s[COL_KD_INDIKATORTUJUANOPD])
                            ->where(COL_KD_SASARANOPD, $p[COL_KD_SASARANOPD])
                            ->where(COL_KD_INDIKATORSASARANOPD, $p[COL_KD_INDIKATORSASARANOPD])
                            ->where(COL_KD_TAHUN, $p[COL_KD_TAHUN])
                            ->where(COL_KD_PROGRAMOPD, $p[COL_KD_PROGRAMOPD])
                            ->order_by(COL_KD_SASARANPROGRAMOPD, 'asc')
                            ->get((!empty($SumberData)&&$SumberData=='PERUBAHAN'?TBL_SAKIP_PDPA_PROGRAM_SASARAN:TBL_SAKIP_DPA_PROGRAM_SASARAN))
                            ->result_array();

                        foreach($sasaranprogram as $sprog) {
                            $arrIkProgram = array();
                            $ikprogram = $this->db
                                ->where(COL_KD_URUSAN, $p[COL_KD_URUSAN])
                                ->where(COL_KD_BIDANG, $p[COL_KD_BIDANG])
                                ->where(COL_KD_UNIT, $p[COL_KD_UNIT])
                                ->where(COL_KD_SUB, $p[COL_KD_SUB])
                                ->where(COL_KD_BID, $p[COL_KD_BID])

                                ->where(COL_KD_PEMDA, $p[COL_KD_PEMDA])
                                ->where(COL_KD_MISI, $p[COL_KD_MISI])
                                ->where(COL_KD_TUJUAN, $p[COL_KD_TUJUAN])
                                ->where(COL_KD_INDIKATORTUJUAN, $p[COL_KD_INDIKATORTUJUAN])
                                ->where(COL_KD_SASARAN, $p[COL_KD_SASARAN])
                                ->where(COL_KD_INDIKATORSASARAN, $p[COL_KD_INDIKATORSASARAN])
                                ->where(COL_KD_TUJUANOPD, $p[COL_KD_TUJUANOPD])
                                ->where(COL_KD_INDIKATORTUJUANOPD, $s[COL_KD_INDIKATORTUJUANOPD])
                                ->where(COL_KD_SASARANOPD, $p[COL_KD_SASARANOPD])
                                ->where(COL_KD_INDIKATORSASARANOPD, $p[COL_KD_INDIKATORSASARANOPD])
                                ->where(COL_KD_TAHUN, $p[COL_KD_TAHUN])
                                ->where(COL_KD_PROGRAMOPD, $p[COL_KD_PROGRAMOPD])
                                ->where(COL_KD_SASARANPROGRAMOPD, $sprog[COL_KD_SASARANPROGRAMOPD])
                                ->order_by(COL_KD_INDIKATORPROGRAMOPD, 'asc')
                                ->get((!empty($SumberData)&&$SumberData=='PERUBAHAN'?TBL_SAKIP_PDPA_PROGRAM_INDIKATOR:TBL_SAKIP_DPA_PROGRAM_INDIKATOR))
                                ->result_array();

                            $htmlSasaranPrg = "<p class='node-name'>Sasaran Program</p><p class='node-title'>".$sprog[COL_NM_SASARANPROGRAMOPD]."</p>";
                            $ikprogram_ = "";
                            if(count($ikprogram) > 0) {
                                $ikprogram_ .= "<p class='node-title' style='margin-bottom: 0px'>Indikator :</p><ul style='margin-left: 0px; padding-left: 15px; text-align: left'>";
                                foreach($ikprogram as $ikp) {
                                    /*$arrIkProgram[] = array(
                                    "text" => array("name"=> "Indikator Program", "title"=> $ikp[COL_NM_INDIKATORPROGRAMOPD]),
                                    "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                                    //"children" => $arrProgramChild,
                                    "HTMLclass" => "bg-red"
                                    );*/
                                    $ikprogram_ .= "<li>".$ikp[COL_NM_INDIKATORPROGRAMOPD]."</li>";
                                }
                                $ikprogram_ .= "</ul>";
                            }
                            $htmlSasaranPrg .= $ikprogram_;

                            $arrKegiatan = array();
                            $kegiatan = $this->db
                                ->where(COL_KD_URUSAN, $p[COL_KD_URUSAN])
                                ->where(COL_KD_BIDANG, $p[COL_KD_BIDANG])
                                ->where(COL_KD_UNIT, $p[COL_KD_UNIT])
                                ->where(COL_KD_SUB, $p[COL_KD_SUB])
                                ->where(COL_KD_BID, $p[COL_KD_BID])

                                ->where(COL_KD_PEMDA, $p[COL_KD_PEMDA])
                                ->where(COL_KD_MISI, $p[COL_KD_MISI])
                                ->where(COL_KD_TUJUAN, $p[COL_KD_TUJUAN])
                                ->where(COL_KD_INDIKATORTUJUAN, $p[COL_KD_INDIKATORTUJUAN])
                                ->where(COL_KD_SASARAN, $p[COL_KD_SASARAN])
                                ->where(COL_KD_INDIKATORSASARAN, $p[COL_KD_INDIKATORSASARAN])
                                ->where(COL_KD_TUJUANOPD, $p[COL_KD_TUJUANOPD])
                                ->where(COL_KD_INDIKATORTUJUANOPD, $s[COL_KD_INDIKATORTUJUANOPD])
                                ->where(COL_KD_SASARANOPD, $p[COL_KD_SASARANOPD])
                                ->where(COL_KD_INDIKATORSASARANOPD, $p[COL_KD_INDIKATORSASARANOPD])
                                ->where(COL_KD_TAHUN, $p[COL_KD_TAHUN])
                                ->where(COL_KD_PROGRAMOPD, $p[COL_KD_PROGRAMOPD])
                                ->where(COL_KD_SASARANPROGRAMOPD, $sprog[COL_KD_SASARANPROGRAMOPD])
                                ->order_by(COL_KD_KEGIATANOPD, 'asc')
                                ->get((!empty($SumberData)&&$SumberData=='PERUBAHAN'?TBL_SAKIP_PDPA_KEGIATAN:TBL_SAKIP_DPA_KEGIATAN))
                                ->result_array();
                            foreach($kegiatan as $keg) {
                              $rsubbidang = $this->db
                              ->where(COL_KD_URUSAN, $keg[COL_KD_URUSAN])
                              ->where(COL_KD_BIDANG, $keg[COL_KD_BIDANG])
                              ->where(COL_KD_UNIT, $keg[COL_KD_UNIT])
                              ->where(COL_KD_SUB, $keg[COL_KD_SUB])
                              ->where(COL_KD_BID, $keg[COL_KD_BID])
                              ->where(COL_KD_SUBBID, $keg[COL_KD_SUBBID])
                              ->get(TBL_SAKIP_MSUBBID)
                              ->row_array();

                                $nmKeg = $keg[COL_NM_KEGIATANOPD];

                                $arrSasaranKegiatan = array();
                                $kegiatan_sasaran = $this->db
                                    ->where(COL_KD_URUSAN, $keg[COL_KD_URUSAN])
                                    ->where(COL_KD_BIDANG, $keg[COL_KD_BIDANG])
                                    ->where(COL_KD_UNIT, $keg[COL_KD_UNIT])
                                    ->where(COL_KD_SUB, $keg[COL_KD_SUB])
                                    ->where(COL_KD_BID, $keg[COL_KD_BID])
                                    ->where(COL_KD_SUBBID, $keg[COL_KD_SUBBID])

                                    ->where(COL_KD_PEMDA, $keg[COL_KD_PEMDA])
                                    ->where(COL_KD_MISI, $keg[COL_KD_MISI])
                                    ->where(COL_KD_TUJUAN, $keg[COL_KD_TUJUAN])
                                    ->where(COL_KD_INDIKATORTUJUAN, $keg[COL_KD_INDIKATORTUJUAN])
                                    ->where(COL_KD_SASARAN, $keg[COL_KD_SASARAN])
                                    ->where(COL_KD_INDIKATORSASARAN, $keg[COL_KD_INDIKATORSASARAN])
                                    ->where(COL_KD_TUJUANOPD, $keg[COL_KD_TUJUANOPD])
                                    ->where(COL_KD_INDIKATORTUJUANOPD, $keg[COL_KD_INDIKATORTUJUANOPD])
                                    ->where(COL_KD_SASARANOPD, $keg[COL_KD_SASARANOPD])
                                    ->where(COL_KD_INDIKATORSASARANOPD, $keg[COL_KD_INDIKATORSASARANOPD])
                                    ->where(COL_KD_TAHUN, $keg[COL_KD_TAHUN])
                                    ->where(COL_KD_PROGRAMOPD, $keg[COL_KD_PROGRAMOPD])
                                    ->where(COL_KD_SASARANPROGRAMOPD, $keg[COL_KD_SASARANPROGRAMOPD])
                                    ->where(COL_KD_KEGIATANOPD, $keg[COL_KD_KEGIATANOPD])
                                    ->order_by(COL_KD_SASARANKEGIATANOPD, 'asc')
                                    ->get((!empty($SumberData)&&$SumberData=='PERUBAHAN'?TBL_SAKIP_PDPA_KEGIATAN_SASARAN:TBL_SAKIP_DPA_KEGIATAN_SASARAN))
                                    ->result_array();
                                foreach($kegiatan_sasaran as $skeg) {
                                    $arrIKKegiatan = array();
                                    $kegiatan_indikator = $this->db
                                        ->where(COL_KD_URUSAN, $skeg[COL_KD_URUSAN])
                                        ->where(COL_KD_BIDANG, $skeg[COL_KD_BIDANG])
                                        ->where(COL_KD_UNIT, $skeg[COL_KD_UNIT])
                                        ->where(COL_KD_SUB, $skeg[COL_KD_SUB])
                                        ->where(COL_KD_BID, $skeg[COL_KD_BID])
                                        ->where(COL_KD_SUBBID, $skeg[COL_KD_SUBBID])

                                        ->where(COL_KD_PEMDA, $skeg[COL_KD_PEMDA])
                                        ->where(COL_KD_MISI, $skeg[COL_KD_MISI])
                                        ->where(COL_KD_TUJUAN, $skeg[COL_KD_TUJUAN])
                                        ->where(COL_KD_INDIKATORTUJUAN, $skeg[COL_KD_INDIKATORTUJUAN])
                                        ->where(COL_KD_SASARAN, $skeg[COL_KD_SASARAN])
                                        ->where(COL_KD_INDIKATORSASARAN, $skeg[COL_KD_INDIKATORSASARAN])
                                        ->where(COL_KD_TUJUANOPD, $skeg[COL_KD_TUJUANOPD])
                                        ->where(COL_KD_INDIKATORTUJUANOPD, $skeg[COL_KD_INDIKATORTUJUANOPD])
                                        ->where(COL_KD_SASARANOPD, $skeg[COL_KD_SASARANOPD])
                                        ->where(COL_KD_INDIKATORSASARANOPD, $skeg[COL_KD_INDIKATORSASARANOPD])
                                        ->where(COL_KD_TAHUN, $skeg[COL_KD_TAHUN])
                                        ->where(COL_KD_PROGRAMOPD, $skeg[COL_KD_PROGRAMOPD])
                                        ->where(COL_KD_SASARANPROGRAMOPD, $skeg[COL_KD_SASARANPROGRAMOPD])
                                        ->where(COL_KD_KEGIATANOPD, $skeg[COL_KD_KEGIATANOPD])
                                        ->where(COL_KD_SASARANKEGIATANOPD, $skeg[COL_KD_SASARANKEGIATANOPD])
                                        ->order_by(COL_KD_INDIKATORKEGIATANOPD, 'asc')
                                        ->get((!empty($SumberData)&&$SumberData=='PERUBAHAN'?TBL_SAKIP_PDPA_KEGIATAN_INDIKATOR:TBL_SAKIP_DPA_KEGIATAN_INDIKATOR))
                                        ->result_array();

                                    $htmlSasaranKeg = "<p class='node-name'>Sasaran Kegiatan</p><p class='node-title'>".$skeg[COL_NM_SASARANKEGIATANOPD]."</p>";
                                    $ikkegiatan_ = "";
                                    if(count($kegiatan_indikator) > 0) {
                                        $ikkegiatan_ .= "<p class='node-title' style='margin-bottom: 0px'>Indikator :</p><ul style='margin-left: 0px; padding-left: 15px; text-align: left'>";
                                        foreach($kegiatan_indikator as $ikk) {
                                            $ikkegiatan_ .= "<li>".$ikk[COL_NM_INDIKATORKEGIATANOPD]."</li>";
                                        }
                                        $ikkegiatan_ .= "</ul>";
                                    }
                                    $htmlSasaranKeg .= $ikkegiatan_;

                                    $arrSasaranKegiatan[] = array(
                                        "innerHTML" => $htmlSasaranKeg,
                                        "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                                        "HTMLclass" => "bg-gray node-wide"
                                    );
                                }
                                $nmKeg = ($keg[COL_NM_KEGIATANOPD] ? $keg[COL_NM_KEGIATANOPD] : "-")." : Rp. ".number_format((isset($keg[COL_PERGESERAN])?$keg[COL_PERGESERAN]:$keg[COL_BUDGET]), 0);

                                $arrKegiatan[] = array(
                                    "text" => array("name"=> /*"Kegiatan"*/!empty($rsubbidang)?$rsubbidang[COL_NM_SUBBID]:'-', "title"=> $nmKeg),
                                    "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                                    "HTMLclass" => "bg-fuchsia",
                                    "children" => $arrSasaranKegiatan /*array(
                                    array(
                                        "text" => array("name"=> "Sasaran Kegiatan", "title"=> $keg[COL_NM_SASARANKEGIATANOPD]),
                                        "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                                        "HTMLclass" => "bg-gray",
                                        "children" => array(
                                            array(
                                                "text" => array("name"=> "Indikator Kegiatan", "title"=> $keg[COL_NM_INDIKATORKEGIATANOPD]),
                                                "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                                                "HTMLclass" => "bg-silver"
                                                //"children" => $arrProgramChild
                                            )
                                        )
                                    )
                                )*/
                                );
                            }
                            /*I$arrKegiatanTemp = array(
                                "pseudo" => true,
                                "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                                "children" => $arrKegiatan
                            );*/

                            if(count($arrKegiatan) > 0) {
                                //$arrIkProgram[] = $arrKegiatanTemp;
                                $arrIkProgram = array_merge($arrIkProgram, $arrKegiatan);
                            }
                            $arrSasaranProgram[] = array(
                                //"text" => array("name"=> "Sasaran Program", "title"=> $sprog[COL_NM_SASARANPROGRAMOPD]." - Indikator : ".$ikprogram_),
                                //"text" => array("name"=> "Sasaran Program", "title"=> $sprog[COL_NM_SASARANPROGRAMOPD], "desc" => $ikprogram_),
                                "innerHTML" => $htmlSasaranPrg,
                                "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                                "children" => $arrIkProgram,
                                "HTMLclass" => "bg-greenlight node-wide"
                            );
                        }

                        $arrProgram[] = array(
                            "text" => array("name"=> /*"Program"*/!empty($rbidang)?$rbidang[COL_NM_BID]:'-', "title"=> $p[COL_NM_PROGRAMOPD]." : Rp. ".number_format($p["TotalProgram"], 0)),
                            "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                            "children" => $arrSasaranProgram,
                            "HTMLclass" => "bg-yellow"
                        );
                        //echo json_encode($arrIkProgram);
                        //return;
                    }

                    /*$arrIkSasaran[] = array(
                        "text" => array("name"=> $iks[COL_KD_MISI].".".$iks[COL_KD_TUJUANOPD].".".$iks[COL_KD_INDIKATORTUJUANOPD].".".$iks[COL_KD_SASARANOPD].".".$iks[COL_KD_INDIKATORSASARANOPD].". Indikator Sasaran Strategis", "title"=> $iks[COL_NM_INDIKATORSASARANOPD]),
                        "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                        "children" => $arrProgram,
                        "HTMLclass" => "bg-orange"
                    );
                    foreach($iksasaran as $iks) {

                    }*/

                    $htmlSasaranOPD = "<p class='node-name'>Sasaran</p><p class='node-title'>".$s[COL_NM_SASARANOPD]."</p>";
                    $iksasaran_ = "";
                    if(count($iksasaran) > 0) {
                        $iksasaran_ .= "<p class='node-title' style='margin-bottom: 0px'>Indikator :</p><ul style='margin-left: 0px; padding-left: 15px; text-align: left'>";
                        foreach($iksasaran as $ikk) {
                            $iksasaran_ .= "<li>".$ikk[COL_NM_INDIKATORSASARANOPD]."</li>";
                        }
                        $iksasaran_ .= "</ul>";
                    }
                    $htmlSasaranOPD .= $iksasaran_;

                    $arrSasaran[] = array(
                        //"text" => array("name"=> $s[COL_KD_MISI].".".$s[COL_KD_TUJUANOPD].".".$s[COL_KD_INDIKATORTUJUANOPD].".".$s[COL_KD_SASARANOPD].". Sasaran Strategis", "title"=> $s[COL_NM_SASARANOPD]),
                        "innerHTML" => $htmlSasaranOPD,
                        "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                        "children" => $arrProgram,
                        "HTMLclass" => "bg-green node-wide"
                    );
                }

                $htmlTujuanOPD = "<p class='node-name'>Tujuan</p><p class='node-title'>".$t[COL_NM_TUJUANOPD]."</p>";
                $iktujuan_ = "";
                if(count($iktujuan) > 0) {
                    $iktujuan_ .= "<p class='node-title' style='margin-bottom: 0px'>Indikator :</p><ul style='margin-left: 0px; padding-left: 15px; text-align: left'>";
                    foreach($iktujuan as $ikk) {
                        $iktujuan_ .= "<li>".$ikk[COL_NM_INDIKATORTUJUANOPD]."</li>";
                    }
                    $iktujuan_ .= "</ul>";
                }
                $htmlTujuanOPD .= $iktujuan_;

                $arrTujuan[] = array(
                    //"text" => array("name"=> $t[COL_KD_MISI].".".$t[COL_KD_TUJUANOPD].". Tujuan", "title"=> $t[COL_NM_TUJUANOPD]),
                    "innerHTML" => $htmlTujuanOPD,
                    "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                    "children" => $arrSasaran,
                    "HTMLclass" => "bg-teal node-wide"
                );
            }

            $arrIkSasaranPmd[] = array(
                "text" => array("name"=> "Indikator Sasaran", "title"=> $ikspmd[COL_NM_INDIKATORSASARAN]),
                "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                "children" => $arrTujuan,
                "HTMLclass" => "bg-fuchsia"
            );
        }

        $arrSasaranPmd[] = array(
            "text" => array("name"=> "Sasaran Bupati", "title"=> $spmd[COL_NM_SASARAN]),
            "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
            "children" => $arrIkSasaranPmd,
            "HTMLclass" => "bg-yellow"
        );
    }

    $arrMisi[] = array(
        "text" => array("name"=> $m[COL_KD_MISI].". Misi", "title"=> $m[COL_NM_MISI]),
        "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
        "children" => $arrSasaranPmd,
        "HTMLclass" => "bg-aqua"
    );
}
$nodes = array(
    "text" => array("name"=> "Pohon Kinerja Bupati", "title"=> "Tahun ".$data[COL_KD_TAHUN]),
    "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
    "children" => $arrSasaranPmd,
    "HTMLclass" => "bg-primary"
);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=!empty($title) ? 'E-SAKIP | '.$title : SITENAME?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- JQUERY -->
    <script src="<?=base_url()?>assets/themes/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>

    <script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/raphael.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/treant/Treant.js"></script>
    <link href="<?=base_url()?>assets/treant/Treant.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="<?=base_url()?>assets/treant/vendor/perfect-scrollbar/perfect-scrollbar.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte/dist/css/skins/_all-skins.min.css">
    <script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
</head>
<body>
<style>
    .nodeExample1 {
        border: 1px solid #000;
        padding : 0px !important;
        width : 12vh !important;
        font-size: 8pt;
        color: #000 !important;
    }
    .node-wide {
        width : 25vh !important;
    }
    .nodeExample1 .node-name {
        font-weight: bold;
        margin: 0 0 5px !important;
        border-bottom: 1px solid #000;
        padding: 2px;
    }
    .nodeExample1 .node-title {
        #text-align: justify;
        padding: 2px;
    }
    .chart {
        overflow: auto;
    }
    .bg-greenlight {
        background-color: #c4dd39 !important;
    }
</style>
<a id="btn-download" download="Pohon Kinerja.jpg" href="">Download</a>
<div id="chart">
    <h4 style="text-align: center">
      POHON KINERJA<br />
      <strong><?=strtoupper($nmSub)?><br />KABUPATEN BATU BARA</strong><br />
      <small style="font-style: italic">Dicetak melalui aplikasi <strong><?=$this->setting_web_name.' - '.$this->setting_web_desc?></strong></small>
    </h4><hr />
    <div class="chart" id="basic-example">

    </div>
</div>
<div id="canvas" style="display: none">

</div>
<script src="<?=base_url()?>assets/js/html2canvas.min.js"></script>
<script>
    console.log(<?=json_encode($nodes)?>);
    var chart_config = {
        chart: {
            container: "#basic-example",
            scrollbar: "fancy",
            //animateOnInit: true,
            hideRootNode: true,
            rootOrientation:  'WEST', // NORTH || EAST || WEST || SOUTH
            connectors: {
                type: "step",
                style: {
                    "stroke-width": 1
                }
            },
            node: {
                HTMLclass: 'nodeExample1'
            },
            nodeAlign: 'TOP',
            padding: 10
        },
        nodeStructure: <?=json_encode($nodes)?>
    };
    new Treant( chart_config );
    html2canvas(document.querySelector("#chart"), {scale: 1.75}).then(canvas => {
        document.getElementById("canvas").appendChild(canvas);
    var img = canvas.toDataURL("image/jpg");
    $("#btn-download").attr("href", img);
    });
</script>
</body>
