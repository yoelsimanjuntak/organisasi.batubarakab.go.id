<style>
body {
  font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
}
th, td {
  padding: 5px;
}
</style>
<html>
<head>
    <title>Perjanjian Kinerja</title>
</head>
<body>
<?php
$isPergeseran = $this->input->get("IsPergeseran") == 'on';
$kd_urusan = $this->input->get(COL_KD_URUSAN);
$kd_bidang = $this->input->get(COL_KD_BIDANG);
$kd_unit = $this->input->get(COL_KD_UNIT);
$kd_sub = $this->input->get(COL_KD_SUB);
$kd_bid = $this->input->get(COL_KD_BID);
$kd_subbid = $this->input->get(COL_KD_SUBBID);
$kd_individu = $this->input->get(COL_KD_SASARANINDIVIDU);
if(!empty($kd_urusan) && !empty($kd_bidang) && !empty($kd_unit) && !empty($kd_sub) && empty($kd_bid) && empty($kd_subbid) && empty($kd_individu)) {
    ?>
    <?php
    if(!empty($kop1) && !empty($kop2)) {
      ?>
      <table width="100%" style="border-bottom: 1px solid #000000">
        <tr>
          <td rowspan="2" style="text-align: center; width: 60px; height: 100px">
            <img class="user-image" src="<?=MY_IMAGEURL?>logo.png" style="width: 60px" alt="Logo">
          </td>
          <td style="text-align: center; padding-bottom: 0px"><h3 style="margin-bottom: 0px">PEMERINTAH KABUPATEN BATU BARA</h3></td>
        </tr>
        <tr>
          <td style="text-align: center; padding-top: 0px; white-space: nowrap">
            <h2 style="margin-bottom: 0px"><?=strtoupper($nmSub)?></h2>
            <p style="margin-top: 0px"><?=$kop1?><br /><?=$kop2?></p>
          </td>
        </tr>
      </table>
      <?php
    } else {
      ?>
      <br />
      <br />
      <br />
      <?php
    }
    ?>
    <table width="100%">
    <tr>
        <td colspan="2" style="text-align: center; vertical-align: middle">
          <br />
          <h4>PERNYATAAN PERJANJIAN KINERJA <br  />TAHUN <?=$data[COL_KD_TAHUN]?></h4>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: justify; vertical-align: middle">
            <p>Dalam rangka mewujudkan manajemen pemerintahan yang efektif, transparan, dan akuntabel serta berorientasi pada hasil, yang bertanda tangan dibawah ini:</p><br />
            <table>
                <tr>
                    <td>Nama</td>
                    <td style="width:60px; text-align: right">:</td>
                    <td><?=$nmPimpinanSub?></td>
                </tr>
                <tr>
                    <td>Jabatan</td>
                    <td style="width: 60px; text-align: right">:</td>
                    <td style="text-align: left"><?= (strtolower($nmSub) == 'inspektorat' ? 'Inspektur' : (strtolower($nmSub) == 'sekretariat daerah kabupaten' ? 'Sekretaris Daerah' : 'Kepala '.$nmSub))?> KABUPATEN BATU BARA</td>
                </tr>
            </table>
            <p>Selanjutnya disebut Pihak Pertama</p><br />
            <table>
                <tr>
                    <td>Nama</td>
                    <td style="width: 60px; text-align: right">:</td>
                    <td><?=$rpemda[COL_NM_PEJABAT]?></td>
                </tr>
                <tr>
                    <td>Jabatan</td>
                    <td style="width: 60px; text-align: right">:</td>
                    <td>BUPATI BATU BARA</td>
                </tr>
            </table>
            <p>Selaku atasan Pihak Pertama, selanjutnya disebut Pihak Kedua</p><br />

            <p>
                Pihak Pertama berjanji akan mewujudkan target kinerja yang seharusnya sesuai lampiran perjanjian ini,
                dalam rangka mencapai target kinerja jangka menengah seperti yang telah ditetapkan dalam dokumen perencanaan. Menjadi tanggung jawab kami.
            </p>
            <br />
            <p>
                Pihak Kedua akan melakukan supervisi yang diperlukan serta akan melakukan evaluasi terhadap capaian kinerja dari perjanjian ini dan
                mengambil tindakan yang diperlukan dalam rangka pemberian penghargaan dan sanksi.
            </p>
        </td>
    </tr>
    <tr>
        <td colspan="2"><br /><br /></td>
    </tr>
  </table>
      <table width="100%">
      <tr>
        <td style="width: 80%; text-align: right">
          BATU BARA,
        </td>
        <td style="white-space: nowrap; padding-left: 100px">
          <?=date("Y")?>
        </td>
      </tr>
      </table>
      <table width="100%">
        <tr>
            <td style="text-align: center">Pihak Kedua,</td>
            <td style="text-align: center">Pihak Pertama,</td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td style="text-align: center"><strong><?=$rpemda[COL_NM_PEJABAT]?></strong></td>
            <td style="text-align: center"><strong><?=$nmPimpinanSub?></strong></td>
        </tr>
      </table>
    <pagebreak></pagebreak>
    <table width="100%">
        <tr>
            <td colspan="2" style="text-align: center; vertical-align: middle">
                <h4>
                    PERJANJIAN KINERJA TAHUN <?=$data[COL_KD_TAHUN]?>
                    <br />
                    <?=strtoupper($nmSub)?>
                    <br />
                    KABUPATEN BATU BARA
                </h4>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: left; vertical-align: middle">
                <table style="border: 1px solid #000; border-spacing: 0" border="1" width="100%">
                    <tr>
                        <th>No.</th>
                        <th>Sasaran Strategis</th>
                        <th>Indikator Kinerja</th>
                        <th>Target</th>
                    </tr>
                    <tr>
                        <th>(1)</th>
                        <th>(2)</th>
                        <th>(3)</th>
                        <th>(4)</th>
                    </tr>
                    <?php
                    $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA,"inner");
                    $this->db->where(TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_PEMDA, $rpemda[COL_KD_PEMDA]);
                    $this->db->where(TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_URUSAN, $this->input->get(COL_KD_URUSAN));
                    $this->db->where(TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_BIDANG, $this->input->get(COL_KD_BIDANG));
                    $this->db->where(TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_UNIT, $this->input->get(COL_KD_UNIT));
                    $this->db->where(TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SUB, $this->input->get(COL_KD_SUB));
                    $this->db->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUANOPD, 'asc');
                    $this->db->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARANOPD, 'asc');
                    $this->db->group_by(array(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUANOPD, TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORTUJUANOPD, TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARANOPD));
                    $rsasaran = $this->db->get(TBL_SAKIP_MOPD_SASARAN)->result_array();
                    $counter = 1;
                    foreach($rsasaran as $s) {
                      $indikator = $this->db
                          ->where(COL_KD_URUSAN, $s[COL_KD_URUSAN])
                          ->where(COL_KD_BIDANG, $s[COL_KD_BIDANG])
                          ->where(COL_KD_UNIT, $s[COL_KD_UNIT])
                          ->where(COL_KD_SUB, $s[COL_KD_SUB])

                          ->where(COL_KD_PEMDA, $s[COL_KD_PEMDA])
                          /*->where(COL_KD_MISI, $s[COL_KD_MISI])
                          ->where(COL_KD_TUJUAN, $s[COL_KD_TUJUAN])
                          ->where(COL_KD_INDIKATORTUJUAN, $s[COL_KD_INDIKATORTUJUAN])
                          ->where(COL_KD_SASARAN, $s[COL_KD_SASARAN])
                          ->where(COL_KD_INDIKATORSASARAN, $s[COL_KD_INDIKATORSASARAN])*/
                          ->where(COL_KD_TUJUANOPD, $s[COL_KD_TUJUANOPD])
                          ->where(COL_KD_INDIKATORTUJUANOPD, $s[COL_KD_INDIKATORTUJUANOPD])
                          ->where(COL_KD_SASARANOPD, $s[COL_KD_SASARANOPD])
                          ->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORSASARANOPD, 'asc')
                          ->group_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_NM_INDIKATORSASARANOPD)
                          ->get(TBL_SAKIP_MOPD_IKSASARAN)->result_array();

                          ?>
                          <tr>
                            <td <?=count($indikator) > 1 ? 'rowspan='.count($indikator) : ''?> style="text-align: right"><?=$counter?></td>
                            <td <?=count($indikator) > 1 ? 'rowspan='.count($indikator) : ''?>><?=$s[COL_NM_SASARANOPD]?></td>
                            <?php
                            if(count($indikator) > 0) {
                              $rcapaian = $this->db
                                  ->where(COL_KD_URUSAN, $indikator[0][COL_KD_URUSAN])
                                  ->where(COL_KD_BIDANG, $indikator[0][COL_KD_BIDANG])
                                  ->where(COL_KD_UNIT, $indikator[0][COL_KD_UNIT])
                                  ->where(COL_KD_SUB, $indikator[0][COL_KD_SUB])
                                  ->where(COL_KD_TAHUN, $this->input->get(COL_KD_TAHUN))

                                  ->where(COL_KD_PEMDA, $indikator[0][COL_KD_PEMDA])
                                  ->where(COL_KD_MISI, $indikator[0][COL_KD_MISI])
                                  ->where(COL_KD_TUJUAN, $indikator[0][COL_KD_TUJUAN])
                                  ->where(COL_KD_INDIKATORTUJUAN, $indikator[0][COL_KD_INDIKATORTUJUAN])
                                  ->where(COL_KD_SASARAN, $indikator[0][COL_KD_SASARAN])
                                  ->where(COL_KD_INDIKATORSASARAN, $indikator[0][COL_KD_INDIKATORSASARAN])
                                  ->where(COL_KD_TUJUANOPD, $indikator[0][COL_KD_TUJUANOPD])
                                  ->where(COL_KD_INDIKATORTUJUANOPD, $indikator[0][COL_KD_INDIKATORTUJUANOPD])
                                  ->where(COL_KD_SASARANOPD, $indikator[0][COL_KD_SASARANOPD])
                                  ->where(COL_KD_INDIKATORSASARANOPD, $indikator[0][COL_KD_INDIKATORSASARANOPD])
                                  ->order_by(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.".".COL_KD_INDIKATORSASARANOPD, 'asc')
                                  ->get(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN)
                                  ->row_array();
                              $lastq = $this->db->last_query();
                              ?>
                              <td><?=$indikator[0][COL_NM_INDIKATORSASARANOPD]?></td>
                              <td><?=!empty($rcapaian)?$rcapaian[COL_TARGET]:'-'?></td>
                              <?php
                            } else {

                            }
                            ?>
                          </tr>
                          <?php
                          if(count($indikator) > 1) {
                            for($i=1; $i<count($indikator); $i++) {
                              $rcapaian = $this->db
                                  ->where(COL_KD_URUSAN, $indikator[$i][COL_KD_URUSAN])
                                  ->where(COL_KD_BIDANG, $indikator[$i][COL_KD_BIDANG])
                                  ->where(COL_KD_UNIT, $indikator[$i][COL_KD_UNIT])
                                  ->where(COL_KD_SUB, $indikator[$i][COL_KD_SUB])
                                  ->where(COL_KD_TAHUN, $this->input->get(COL_KD_TAHUN))

                                  ->where(COL_KD_PEMDA, $indikator[$i][COL_KD_PEMDA])
                                  ->where(COL_KD_MISI, $indikator[$i][COL_KD_MISI])
                                  ->where(COL_KD_TUJUAN, $indikator[$i][COL_KD_TUJUAN])
                                  ->where(COL_KD_INDIKATORTUJUAN, $indikator[$i][COL_KD_INDIKATORTUJUAN])
                                  ->where(COL_KD_SASARAN, $indikator[$i][COL_KD_SASARAN])
                                  ->where(COL_KD_INDIKATORSASARAN, $indikator[$i][COL_KD_INDIKATORSASARAN])
                                  ->where(COL_KD_TUJUANOPD, $indikator[$i][COL_KD_TUJUANOPD])
                                  ->where(COL_KD_INDIKATORTUJUANOPD, $indikator[$i][COL_KD_INDIKATORTUJUANOPD])
                                  ->where(COL_KD_SASARANOPD, $indikator[$i][COL_KD_SASARANOPD])
                                  ->where(COL_KD_INDIKATORSASARANOPD, $indikator[$i][COL_KD_INDIKATORSASARANOPD])
                                  ->order_by(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN.".".COL_KD_INDIKATORSASARANOPD, 'asc')
                                  ->get(TBL_SAKIP_MOPD_IKSASARAN_CAPAIAN)->row_array();
                              $lastq = $this->db->last_query();
                              ?>
                              <tr>
                                <td><?=$indikator[$i][COL_NM_INDIKATORSASARANOPD]?></td>
                                <td><?=!empty($rcapaian)?$rcapaian[COL_TARGET]:'-'?></td>
                              </tr>
                              <?php
                            }
                          }
                        ?>
                        <!--<tr>
                            <td <?=count($indikator) > 1 ? 'rowspan='.count($indikator) : ''?> style="text-align: right"><?=$counter?></td>
                            <td <?=count($indikator) > 1 ? 'rowspan='.count($indikator) : ''?>><?=$s[COL_NM_SASARANOPD]?></td>
                            <td>
                                <?php
                                if(count($indikator) > 1) {
                                    ?>
                                    <ul>
                                        <?php
                                        foreach($indikator as $ik) {
                                            ?>
                                            <li><?=$ik[COL_NM_INDIKATORSASARANOPD]?></li>
                                        <?php
                                        }
                                        ?>
                                    </ul>
                                <?php
                                } else {
                                    echo count($indikator) > 0 ? $indikator[0][COL_NM_INDIKATORSASARANOPD] : "";
                                }
                                ?>
                            </td>
                            <td style="text-align: left">
                                <?php
                                $program = $this->db
                                    ->select(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_TARGET.", ".TBL_SAKIP_MSATUAN.".".COL_NM_SATUAN.", ".TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_SATUAN." as Kd_Satuan")
                                    ->join(TBL_SAKIP_MSATUAN,TBL_SAKIP_MSATUAN.'.'.COL_KD_SATUAN." = ".TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_SATUAN,"left")
                                    ->where(COL_KD_URUSAN, $s[COL_KD_URUSAN])
                                    ->where(COL_KD_BIDANG, $s[COL_KD_BIDANG])
                                    ->where(COL_KD_UNIT, $s[COL_KD_UNIT])
                                    ->where(COL_KD_SUB, $s[COL_KD_SUB])
                                    ->where(COL_KD_TAHUN, $data[COL_KD_TAHUN])

                                    ->where(COL_KD_PEMDA, $s[COL_KD_PEMDA])
                                    ->where(COL_KD_MISI, $s[COL_KD_MISI])
                                    ->where(COL_KD_TUJUAN, $s[COL_KD_TUJUAN])
                                    ->where(COL_KD_INDIKATORTUJUAN, $s[COL_KD_INDIKATORTUJUAN])
                                    ->where(COL_KD_SASARAN, $s[COL_KD_SASARAN])
                                    ->where(COL_KD_INDIKATORSASARAN, $s[COL_KD_INDIKATORSASARAN])
                                    ->where(COL_KD_TUJUANOPD, $s[COL_KD_TUJUANOPD])
                                    ->where(COL_KD_INDIKATORTUJUANOPD, $s[COL_KD_INDIKATORTUJUANOPD])
                                    ->where(COL_KD_SASARANOPD, $s[COL_KD_SASARANOPD])
                                    ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_PROGRAMOPD, 'asc')
                                    ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_SASARANPROGRAMOPD, 'asc')
                                    ->group_by(array(
                                        TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_TARGET,
                                        TBL_SAKIP_MSATUAN.".".COL_NM_SATUAN
                                    ))
                                    ->get(TBL_SAKIP_DPA_PROGRAM_INDIKATOR)->result_array();
                                ?>
                                <?php
                                if(count($program) > 1) {
                                    ?>
                                    <ul style='margin-left: 0px; padding-left: 15px; text-align: left'>
                                        <?php
                                        foreach($program as $p) {
                                            ?>
                                            <li style='white-space: nowrap'><?=$p[COL_TARGET]?> <?=$p[COL_KD_SATUAN]?></li>
                                        <?php
                                        }
                                        ?>
                                    </ul>
                                <?php
                                } else {
                                    echo count($program) > 0 ? $program[0][COL_TARGET]." ".$program[0][COL_KD_SATUAN] : "";
                                }
                                ?>
                            </td>
                        </tr>-->
                        <?php
                        $counter++;
                    }
                    ?>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table style="border: 1px solid #000; border-spacing: 0" border="1" width="100%">
                    <tr>
                        <th>No.</th>
                        <th>Program</th>
                        <th>Anggaran</th>
                        <th>Keterangan</th>
                    </tr>
                    <tr>
                        <th>(1)</th>
                        <th>(2)</th>
                        <th>(3)</th>
                        <th>(4)</th>
                    </tr>
                    <?php
                    if(!empty($SumberData) && $SumberData == 'PERUBAHAN') {
                      $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_PDPA_PROGRAM.".".COL_KD_PEMDA,"inner");
                      $this->db->where(TBL_SAKIP_PDPA_PROGRAM.'.'.COL_KD_PEMDA, $rpemda[COL_KD_PEMDA]);
                      $this->db->where(TBL_SAKIP_PDPA_PROGRAM.'.'.COL_KD_URUSAN, $this->input->get(COL_KD_URUSAN));
                      $this->db->where(TBL_SAKIP_PDPA_PROGRAM.'.'.COL_KD_BIDANG, $this->input->get(COL_KD_BIDANG));
                      $this->db->where(TBL_SAKIP_PDPA_PROGRAM.'.'.COL_KD_UNIT, $this->input->get(COL_KD_UNIT));
                      $this->db->where(TBL_SAKIP_PDPA_PROGRAM.'.'.COL_KD_SUB, $this->input->get(COL_KD_SUB));
                      $this->db->where(TBL_SAKIP_PDPA_PROGRAM.'.'.COL_KD_TAHUN, $this->input->get(COL_KD_TAHUN));
                      $this->db->group_by(TBL_SAKIP_PDPA_PROGRAM.".".COL_KD_PROGRAMOPD);
                      $this->db->order_by(TBL_SAKIP_PDPA_PROGRAM.".".COL_KD_TUJUANOPD, 'asc');
                      $this->db->order_by(TBL_SAKIP_PDPA_PROGRAM.".".COL_KD_SASARANOPD, 'asc');
                      $this->db->order_by(TBL_SAKIP_PDPA_PROGRAM.".".COL_KD_PROGRAMOPD, 'asc');
                      $rprogram = $this->db->get(TBL_SAKIP_PDPA_PROGRAM)->result_array();
                    } else {
                      $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA,"inner");
                      $this->db->where(TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_PEMDA, $rpemda[COL_KD_PEMDA]);
                      $this->db->where(TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_URUSAN, $this->input->get(COL_KD_URUSAN));
                      $this->db->where(TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_BIDANG, $this->input->get(COL_KD_BIDANG));
                      $this->db->where(TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_UNIT, $this->input->get(COL_KD_UNIT));
                      $this->db->where(TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_SUB, $this->input->get(COL_KD_SUB));
                      $this->db->where(TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_TAHUN, $this->input->get(COL_KD_TAHUN));
                      $this->db->group_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PROGRAMOPD);
                      $this->db->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUANOPD, 'asc');
                      $this->db->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARANOPD, 'asc');
                      $this->db->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PROGRAMOPD, 'asc');
                      $rprogram = $this->db->get(TBL_SAKIP_DPA_PROGRAM)->result_array();
                    }

                    $counter = 1;
                    $sum = 0;
                    foreach($rprogram as $p) {
                        ?>
                        <tr>
                            <td style="text-align: right"><?=$counter?></td>
                            <td><?=$p[COL_NM_PROGRAMOPD]?></td>
                            <td style="text-align: right">
                                <?php
                                $totalkeg = $this->db
                                    ->select_sum(($isPergeseran?COL_PERGESERAN:COL_BUDGET))
                                    ->where(COL_KD_URUSAN, $p[COL_KD_URUSAN])
                                    ->where(COL_KD_BIDANG, $p[COL_KD_BIDANG])
                                    ->where(COL_KD_UNIT, $p[COL_KD_UNIT])
                                    ->where(COL_KD_SUB, $p[COL_KD_SUB])
                                    //->where(COL_KD_BID, $p[COL_KD_BID])

                                    ->where(COL_KD_PEMDA, $p[COL_KD_PEMDA])
                                    ->where(COL_KD_MISI, $p[COL_KD_MISI])
                                    //->where(COL_KD_TUJUAN, $p[COL_KD_TUJUAN])
                                    //->where(COL_KD_INDIKATORTUJUAN, $p[COL_KD_INDIKATORTUJUAN])
                                    //->where(COL_KD_SASARAN, $p[COL_KD_SASARAN])
                                    //->where(COL_KD_INDIKATORSASARAN, $p[COL_KD_INDIKATORSASARAN])
                                    //->where(COL_KD_TUJUANOPD, $p[COL_KD_TUJUANOPD])
                                    //->where(COL_KD_INDIKATORTUJUANOPD, $p[COL_KD_INDIKATORTUJUANOPD])
                                    //->where(COL_KD_SASARANOPD, $p[COL_KD_SASARANOPD])
                                    //->where(COL_KD_INDIKATORSASARANOPD, $p[COL_KD_INDIKATORSASARANOPD])
                                    ->where(COL_KD_PROGRAMOPD, $p[COL_KD_PROGRAMOPD])
                                    ->where(COL_KD_TAHUN, $p[COL_KD_TAHUN])
                                    ->order_by(COL_KD_TAHUN, 'asc')
                                    ->order_by(COL_KD_PROGRAMOPD, 'asc')
                                    ->get((!empty($SumberData)&&$SumberData=='PERUBAHAN'?TBL_SAKIP_PDPA_KEGIATAN:TBL_SAKIP_DPA_KEGIATAN))
                                    ->row_array();

                                echo "Rp . ".number_format($totalkeg[($isPergeseran?COL_PERGESERAN:COL_BUDGET)], 0)
                                ?>
                            </td>
                            <td>
                                <?=$p[COL_REMARKS]?>
                            </td>
                        </tr>
                        <?php
                        $sum = $sum + $totalkeg[($isPergeseran?COL_PERGESERAN:COL_BUDGET)];
                        $counter++;
                    }
                    ?>
                    <tr>
                        <th colspan="2" style="text-align: right">Jumlah</th>
                        <td style="text-align: right"><?="Rp. ".number_format($sum, 0)?></td>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
      </table>
      <table width="100%" style="padding-left: 100px">
        <tr>
            <td colspan="2">
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; text-align: center">BUPATI BATU BARA</td>
            <td style="font-weight: bold; text-align: center"><?= strtolower($nmSub) == 'inspektorat' ? 'INSPEKTUR' : (strtolower($nmSub) == 'sekretariat daerah kabupaten' ? 'SEKRETARIS DAERAH' : 'KEPALA '.strtoupper($nmSub))?></td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; text-align: center"><?=$rpemda[COL_NM_PEJABAT]?></td>
            <td style="font-weight: bold; text-align: center"><?=$nmPimpinanSub?></td>
        </tr>
    </table>
    <?php
}

else if(!empty($kd_urusan) && !empty($kd_bidang) && !empty($kd_unit) && !empty($kd_sub) && !empty($kd_bid) && empty($kd_subbid) && empty($kd_individu)) {
    $this->db->where(TBL_SAKIP_MBID.'.'.COL_KD_URUSAN, $this->input->get(COL_KD_URUSAN));
    $this->db->where(TBL_SAKIP_MBID.'.'.COL_KD_BIDANG, $this->input->get(COL_KD_BIDANG));
    $this->db->where(TBL_SAKIP_MBID.'.'.COL_KD_UNIT, $this->input->get(COL_KD_UNIT));
    $this->db->where(TBL_SAKIP_MBID.'.'.COL_KD_SUB, $this->input->get(COL_KD_SUB));
    $this->db->where(TBL_SAKIP_MBID.'.'.COL_KD_BID, $this->input->get(COL_KD_BID));
    $this->db->order_by(TBL_SAKIP_MBID.".".COL_KD_BID, 'asc');
    $rbid = $this->db->get(TBL_SAKIP_MBID)->result_array();
    foreach($rbid as $bid) {
        ?>
        <!--<pagebreak></pagebreak>-->
        <?php
        if(!empty($kop1) && !empty($kop2)) {
          ?>
          <table width="100%" style="border-bottom: 1px solid #000000">
            <tr>
              <td rowspan="2" style="text-align: center; width: 60px; height: 100px">
                <img class="user-image" src="<?=MY_IMAGEURL?>logo.png" style="width: 60px" alt="Logo">
              </td>
              <td style="text-align: center; padding-bottom: 0px"><h3 style="margin-bottom: 0px">PEMERINTAH KABUPATEN BATU BARA</h3></td>
            </tr>
            <tr>
              <td style="text-align: center; padding-top: 0px; white-space: nowrap">
                <h2 style="margin-bottom: 0px"><?=strtoupper($nmSub)?></h2>
                <p style="margin-top: 0px"><?=$kop1?><br /><?=$kop2?></p>
              </td>
            </tr>
          </table>
          <?php
        } else {
          ?>
          <br />
          <br />
          <br />
          <?php
        }
        ?>
        <table width="100%">
            <tr>
                <td colspan="2" style="text-align: center; vertical-align: middle">
                  <br />
                  <h4>PERNYATAAN PERJANJIAN KINERJA <br  />TAHUN <?= $data[COL_KD_TAHUN] ?></h4>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: justify; vertical-align: middle">
                    <p>Dalam rangka mewujudkan manajemen pemerintahan yang efektif, transparan, dan akuntabel serta
                        berorientasi pada hasil, yang bertanda tangan dibawah ini:</p><br/>
                    <table>
                        <tr>
                            <td>Nama</td>
                            <td style="width: 60px; text-align: right">:</td>
                            <td><?= $bid[COL_NM_KABID] ?></td>
                        </tr>
                        <tr>
                            <td>Jabatan</td>
                            <td style="width: 60px; text-align: right">:</td>
                            <td style="text-align: left"><?=strpos(strtolower($bid[COL_NM_BID]), "sekretariat") !== false ? 'Sekretaris' : 'Kepala '.$bid[COL_NM_BID]?> <?= $nmSub ?> KABUPATEN BATU BARA
                            </td>
                        </tr>
                    </table>
                    <p>Selanjutnya disebut Pihak Pertama</p><br/>
                    <table>
                        <tr>
                            <td>Nama</td>
                            <td style="width:60px; text-align: right">:</td>
                            <td><?= $nmPimpinanSub ?></td>
                        </tr>
                        <tr>
                            <td>Jabatan</td>
                            <td style="width: 60px; text-align: right">:</td>
                            <td style="text-align: left"><?= (strtolower($nmSub) == 'inspektorat' ? 'Inspektur' : (strtolower($nmSub) == 'sekretariat daerah' ? 'Sekretaris Daerah' : 'Kepala '.$nmSub)) ?> KABUPATEN BATU BARA</td>
                        </tr>
                    </table>
                    <p>Selaku atasan Pihak Pertama, selanjutnya disebut Pihak Kedua</p><br/>

                    <p>
                        Pihak Pertama berjanji akan mewujudkan target kinerja yang seharusnya sesuai lampiran perjanjian
                        ini,
                        dalam rangka mencapai target kinerja jangka menengah seperti yang telah ditetapkan dalam dokumen
                        perencanaan. Menjadi tanggung jawab kami.
                    </p>
                    <br/>

                    <p>
                        Pihak Kedua akan melakukan supervisi yang diperlukan serta akan melakukan evaluasi terhadap
                        capaian kinerja dari perjanjian ini dan
                        mengambil tindakan yang diperlukan dalam rangka pemberian penghargaan dan sanksi.
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="2"><br /><br /></td>
            </tr>
          </table>
          <table width="100%">
          <tr>
            <td style="width: 80%; text-align: right">
              BATU BARA,
            </td>
            <td style="white-space: nowrap; padding-left: 100px">
              <?=date("Y")?>
            </td>
          </tr>
          </table>
          <table width="100%">
            <tr>
                <td style="text-align: center">Pihak Kedua,</td>
                <td style="text-align: center">Pihak Pertama,</td>
            </tr>
            <tr>
                <td colspan="2">
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                </td>
            </tr>
            <tr>
                <td style="text-align: center; font-weight: bold"><?= $nmPimpinanSub ?></td>
                <td style="text-align: center; font-weight: bold"><?= $bid[COL_NM_KABID] ?></td>
            </tr>
        </table>
        <pagebreak></pagebreak>
        <table width="100%">
            <tr>
                <td colspan="2" style="text-align: center; vertical-align: middle">
                    <h4>
                        PERJANJIAN KINERJA TAHUN <?= $data[COL_KD_TAHUN] ?>
                        <br/>
                        <?= strtoupper($nmSub) ?>
                        <br/>
                        KABUPATEN BATU BARA
                    </h4>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: left; vertical-align: middle">
                    <table style="border: 1px solid #000; border-spacing: 0" border="1" width="100%">
                        <tr>
                            <th>No.</th>
                            <th>Sasaran Strategis</th>
                            <th>Indikator Kinerja</th>
                            <th>Target</th>
                        </tr>
                        <tr>
                            <th>(1)</th>
                            <th>(2)</th>
                            <th>(3)</th>
                            <th>(4)</th>
                        </tr>
                        <?php
                        if(!empty($SumberData) && $SumberData == 'PERUBAHAN') {
                          $program = $this->db
                              ->join(TBL_SAKIP_MSATUAN, TBL_SAKIP_MSATUAN . '.' . COL_KD_SATUAN . " = " . TBL_SAKIP_PDPA_PROGRAM_SASARAN . "." . COL_KD_SATUAN, "left")
                              ->where(COL_KD_URUSAN, $bid[COL_KD_URUSAN])
                              ->where(COL_KD_BIDANG, $bid[COL_KD_BIDANG])
                              ->where(COL_KD_UNIT, $bid[COL_KD_UNIT])
                              ->where(COL_KD_SUB, $bid[COL_KD_SUB])
                              ->where(COL_KD_BID, $bid[COL_KD_BID])
                              ->where(COL_KD_PEMDA, $data[COL_KD_PEMDA])
                              ->where(COL_KD_TAHUN, $data[COL_KD_TAHUN])
                              ->group_by(TBL_SAKIP_PDPA_PROGRAM_SASARAN . "." . COL_KD_PROGRAMOPD)
                              ->group_by(TBL_SAKIP_PDPA_PROGRAM_SASARAN . "." . COL_KD_SASARANPROGRAMOPD)
                              ->order_by(TBL_SAKIP_PDPA_PROGRAM_SASARAN . "." . COL_KD_PROGRAMOPD, 'asc')
                              ->order_by(TBL_SAKIP_PDPA_PROGRAM_SASARAN . "." . COL_KD_SASARANPROGRAMOPD, 'asc')
                              ->get(TBL_SAKIP_PDPA_PROGRAM_SASARAN)->result_array();
                        } else {
                          $program = $this->db
                              ->join(TBL_SAKIP_MSATUAN, TBL_SAKIP_MSATUAN . '.' . COL_KD_SATUAN . " = " . TBL_SAKIP_DPA_PROGRAM_SASARAN . "." . COL_KD_SATUAN, "left")
                              ->where(COL_KD_URUSAN, $bid[COL_KD_URUSAN])
                              ->where(COL_KD_BIDANG, $bid[COL_KD_BIDANG])
                              ->where(COL_KD_UNIT, $bid[COL_KD_UNIT])
                              ->where(COL_KD_SUB, $bid[COL_KD_SUB])
                              ->where(COL_KD_BID, $bid[COL_KD_BID])
                              ->where(COL_KD_PEMDA, $data[COL_KD_PEMDA])
                              ->where(COL_KD_TAHUN, $data[COL_KD_TAHUN])
                              ->group_by(TBL_SAKIP_DPA_PROGRAM_SASARAN . "." . COL_KD_PROGRAMOPD)
                              ->group_by(TBL_SAKIP_DPA_PROGRAM_SASARAN . "." . COL_KD_SASARANPROGRAMOPD)
                              ->order_by(TBL_SAKIP_DPA_PROGRAM_SASARAN . "." . COL_KD_PROGRAMOPD, 'asc')
                              ->order_by(TBL_SAKIP_DPA_PROGRAM_SASARAN . "." . COL_KD_SASARANPROGRAMOPD, 'asc')
                              ->get(TBL_SAKIP_DPA_PROGRAM_SASARAN)->result_array();
                        }

                        $counter = 1;
                        ?>
                        <!--<tr>
                        <td colspan="4"><?= $this->db->last_query() ?></td>
                    </tr>-->
                        <?php
                        foreach ($program as $p) {
                            ?>
                            <tr>
                                <td style="text-align: right"><?= $counter ?></td>
                                <td><?= $p[COL_NM_SASARANPROGRAMOPD] ?></td>
                                <?php
                                $indikator = $this->db
                                    ->where(COL_KD_URUSAN, $p[COL_KD_URUSAN])
                                    ->where(COL_KD_BIDANG, $p[COL_KD_BIDANG])
                                    ->where(COL_KD_UNIT, $p[COL_KD_UNIT])
                                    ->where(COL_KD_SUB, $p[COL_KD_SUB])
                                    ->where(COL_KD_BID, $p[COL_KD_BID])
                                    ->where(COL_KD_PEMDA, $p[COL_KD_PEMDA])
                                    ->where(COL_KD_MISI, $p[COL_KD_MISI])
                                    ->where(COL_KD_TUJUAN, $p[COL_KD_TUJUAN])
                                    ->where(COL_KD_INDIKATORTUJUAN, $p[COL_KD_INDIKATORTUJUAN])
                                    ->where(COL_KD_SASARAN, $p[COL_KD_SASARAN])
                                    ->where(COL_KD_INDIKATORSASARAN, $p[COL_KD_INDIKATORSASARAN])
                                    ->where(COL_KD_TUJUANOPD, $p[COL_KD_TUJUANOPD])
                                    ->where(COL_KD_INDIKATORTUJUANOPD, $p[COL_KD_INDIKATORTUJUANOPD])
                                    ->where(COL_KD_SASARANOPD, $p[COL_KD_SASARANOPD])
                                    ->where(COL_KD_INDIKATORSASARANOPD, $p[COL_KD_INDIKATORSASARANOPD])
                                    ->where(COL_KD_PROGRAMOPD, $p[COL_KD_PROGRAMOPD])
                                    ->where(COL_KD_TAHUN, $p[COL_KD_TAHUN])
                                    ->where(COL_KD_SASARANPROGRAMOPD, $p[COL_KD_SASARANPROGRAMOPD])
                                    ->order_by(COL_KD_INDIKATORPROGRAMOPD, 'asc')
                                    ->get((!empty($SumberData)&&$SumberData=='PERUBAHAN'?TBL_SAKIP_PDPA_PROGRAM_INDIKATOR:TBL_SAKIP_DPA_PROGRAM_INDIKATOR))->result_array();
                                ?>
                                <td>
                                    <?php
                                    if (count($indikator) > 1) {
                                        ?>
                                        <ul style="margin-left: 0px; padding-left: 15px">
                                            <?php
                                            foreach ($indikator as $ik) {
                                                ?>
                                                <li><?= $ik[COL_NM_INDIKATORPROGRAMOPD] ?></li>
                                            <?php
                                            }
                                            ?>
                                        </ul>
                                    <?php
                                    } else {
                                        echo count($indikator) > 0 ? $indikator[0][COL_NM_INDIKATORPROGRAMOPD] : "";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if (count($indikator) > 1) {
                                        ?>
                                        <ul style="margin-left: 0px; padding-left: 15px">
                                            <?php
                                            foreach ($indikator as $ik) {
                                                ?>
                                                <li><?= $ik[COL_TARGET]." ".$ik[COL_KD_SATUAN] ?></li>
                                            <?php
                                            }
                                            ?>
                                        </ul>
                                    <?php
                                    } else {
                                        echo count($indikator) > 0 ? $indikator[0][COL_TARGET]." ".$indikator[0][COL_KD_SATUAN] : "";
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php
                            $counter++;
                        }
                        ?>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table style="border: 1px solid #000; border-spacing: 0" border="1" width="100%">
                        <tr>
                            <th>No.</th>
                            <th>Program</th>
                            <th>Anggaran</th>
                            <th>Keterangan</th>
                        </tr>
                        <tr>
                            <th>(1)</th>
                            <th>(2)</th>
                            <th>(3)</th>
                            <th>(4)</th>
                        </tr>
                        <?php
                        if(!empty($SumberData) && $SumberData == 'PERUBAHAN') {
                          $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_PDPA_PROGRAM.".".COL_KD_PEMDA,"inner");
                          $this->db->where(TBL_SAKIP_PDPA_PROGRAM.'.'.COL_KD_PEMDA, $data[COL_KD_PEMDA]);
                          $this->db->where(TBL_SAKIP_PDPA_PROGRAM.'.'.COL_KD_URUSAN, $this->input->get(COL_KD_URUSAN));
                          $this->db->where(TBL_SAKIP_PDPA_PROGRAM.'.'.COL_KD_BIDANG, $this->input->get(COL_KD_BIDANG));
                          $this->db->where(TBL_SAKIP_PDPA_PROGRAM.'.'.COL_KD_UNIT, $this->input->get(COL_KD_UNIT));
                          $this->db->where(TBL_SAKIP_PDPA_PROGRAM.'.'.COL_KD_SUB, $this->input->get(COL_KD_SUB));
                          $this->db->where(TBL_SAKIP_PDPA_PROGRAM.'.'.COL_KD_BID, $this->input->get(COL_KD_BID));
                          $this->db->where(TBL_SAKIP_PDPA_PROGRAM.'.'.COL_KD_TAHUN, $this->input->get(COL_KD_TAHUN));
                          $this->db->group_by(TBL_SAKIP_PDPA_PROGRAM.".".COL_KD_PROGRAMOPD);
                          $this->db->order_by(TBL_SAKIP_PDPA_PROGRAM.".".COL_KD_TUJUANOPD, 'asc');
                          $this->db->order_by(TBL_SAKIP_PDPA_PROGRAM.".".COL_KD_SASARANOPD, 'asc');
                          $this->db->order_by(TBL_SAKIP_PDPA_PROGRAM.".".COL_KD_PROGRAMOPD, 'asc');
                          $rprogram = $this->db->get(TBL_SAKIP_PDPA_PROGRAM)->result_array();
                        } else {
                          $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA,"inner");
                          $this->db->where(TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_PEMDA, $data[COL_KD_PEMDA]);
                          $this->db->where(TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_URUSAN, $this->input->get(COL_KD_URUSAN));
                          $this->db->where(TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_BIDANG, $this->input->get(COL_KD_BIDANG));
                          $this->db->where(TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_UNIT, $this->input->get(COL_KD_UNIT));
                          $this->db->where(TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_SUB, $this->input->get(COL_KD_SUB));
                          $this->db->where(TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_BID, $this->input->get(COL_KD_BID));
                          $this->db->where(TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_TAHUN, $this->input->get(COL_KD_TAHUN));
                          $this->db->group_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PROGRAMOPD);
                          $this->db->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUANOPD, 'asc');
                          $this->db->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARANOPD, 'asc');
                          $this->db->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PROGRAMOPD, 'asc');
                          $rprogram = $this->db->get(TBL_SAKIP_DPA_PROGRAM)->result_array();
                        }
                        $counter = 1;
                        $sum = 0;
                        foreach($rprogram as $p) {
                            ?>
                            <tr>
                                <td style="text-align: right"><?=$counter?></td>
                                <td><?=$p[COL_NM_PROGRAMOPD]?></td>
                                <td style="text-align: right">
                                    <?php
                                    $totalkeg = $this->db
                                        ->select_sum(($isPergeseran?COL_PERGESERAN:COL_BUDGET))
                                        ->where(COL_KD_URUSAN, $p[COL_KD_URUSAN])
                                        ->where(COL_KD_BIDANG, $p[COL_KD_BIDANG])
                                        ->where(COL_KD_UNIT, $p[COL_KD_UNIT])
                                        ->where(COL_KD_SUB, $p[COL_KD_SUB])
                                        ->where(COL_KD_BID, $p[COL_KD_BID])

                                        ->where(COL_KD_PEMDA, $p[COL_KD_PEMDA])
                                        ->where(COL_KD_MISI, $p[COL_KD_MISI])
                                        ->where(COL_KD_TUJUAN, $p[COL_KD_TUJUAN])
                                        ->where(COL_KD_INDIKATORTUJUAN, $p[COL_KD_INDIKATORTUJUAN])
                                        ->where(COL_KD_SASARAN, $p[COL_KD_SASARAN])
                                        ->where(COL_KD_INDIKATORSASARAN, $p[COL_KD_INDIKATORSASARAN])
                                        ->where(COL_KD_TUJUANOPD, $p[COL_KD_TUJUANOPD])
                                        ->where(COL_KD_INDIKATORTUJUANOPD, $p[COL_KD_INDIKATORTUJUANOPD])
                                        ->where(COL_KD_SASARANOPD, $p[COL_KD_SASARANOPD])
                                        ->where(COL_KD_INDIKATORSASARANOPD, $p[COL_KD_INDIKATORSASARANOPD])
                                        ->where(COL_KD_PROGRAMOPD, $p[COL_KD_PROGRAMOPD])
                                        ->where(COL_KD_TAHUN, $p[COL_KD_TAHUN])
                                        ->order_by(COL_KD_TAHUN, 'asc')
                                        ->order_by(COL_KD_PROGRAMOPD, 'asc')
                                        ->get((!empty($SumberData)&&$SumberData=='PERUBAHAN'?TBL_SAKIP_PDPA_KEGIATAN:TBL_SAKIP_DPA_KEGIATAN))
                                        ->row_array();

                                    echo "Rp. ".number_format($totalkeg[($isPergeseran?COL_PERGESERAN:COL_BUDGET)], 0)
                                    ?>
                                </td>
                                <td>
                                    <?=$p[COL_REMARKS]?>
                                </td>
                            </tr>
                            <?php
                            $sum = $sum + $totalkeg[($isPergeseran?COL_PERGESERAN:COL_BUDGET)];
                            $counter++;
                        }
                        ?>
                        <tr>
                            <th colspan="2" style="text-align: right">Jumlah</th>
                            <td style="text-align: right"><?="Rp. ".number_format($sum, 0)?></td>
                            <td></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br/>
                    <br/>
                </td>
            </tr>
            </table>
            <table width="100%" style="padding-left: 100px">
            <tr>
                <td style="font-weight: bold">KEPALA <?= strtoupper($nmSub) ?></td>
                <td style="font-weight: bold"><?= strpos(strtolower($bid[COL_NM_BID]), "sekretariat") !== false ? 'SEKRETARIS' : 'KEPALA '.strtoupper($bid[COL_NM_BID]) ?></td>
            </tr>
            <tr>
                <td colspan="2">
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold"><?= $nmPimpinanSub ?></td>
                <td style="font-weight: bold"><?= $bid[COL_NM_KABID] ?></td>
            </tr>
        </table>
    <?php
    }
?>
</table>
    <?php
}
else if(!empty($kd_urusan) && !empty($kd_bidang) && !empty($kd_unit) && !empty($kd_sub) && !empty($kd_bid) && !empty($kd_subbid) && empty($kd_individu)) {
    $this->db->join(TBL_SAKIP_MBID,
    TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID.".".COL_KD_URUSAN." AND ".
    TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID.".".COL_KD_BIDANG." AND ".
    TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID.".".COL_KD_UNIT." AND ".
    TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID.".".COL_KD_SUB." AND ".
    TBL_SAKIP_MBID.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID.".".COL_KD_BID,"inner");
    $this->db->where(TBL_SAKIP_MSUBBID.'.'.COL_KD_URUSAN, $this->input->get(COL_KD_URUSAN));
    $this->db->where(TBL_SAKIP_MSUBBID.'.'.COL_KD_BIDANG, $this->input->get(COL_KD_BIDANG));
    $this->db->where(TBL_SAKIP_MSUBBID.'.'.COL_KD_UNIT, $this->input->get(COL_KD_UNIT));
    $this->db->where(TBL_SAKIP_MSUBBID.'.'.COL_KD_SUB, $this->input->get(COL_KD_SUB));
    $this->db->where(TBL_SAKIP_MSUBBID.'.'.COL_KD_BID, $this->input->get(COL_KD_BID));
    $this->db->where(TBL_SAKIP_MSUBBID.'.'.COL_KD_SUBBID, $this->input->get(COL_KD_SUBBID));
    $this->db->order_by(TBL_SAKIP_MSUBBID.".".COL_KD_BID, 'asc');
    $this->db->order_by(TBL_SAKIP_MSUBBID.".".COL_KD_SUBBID, 'asc');
    $rsubbid = $this->db->get(TBL_SAKIP_MSUBBID)->result_array();
    foreach($rsubbid as $sbid) {
        ?>
        <!--<pagebreak></pagebreak>-->
        <?php
        if(!empty($kop1) && !empty($kop2)) {
          ?>
          <table width="100%" style="border-bottom: 1px solid #000000">
            <tr>
              <td rowspan="2" style="text-align: center; width: 60px; height: 100px">
                <img class="user-image" src="<?=MY_IMAGEURL?>logo.png" style="width: 60px" alt="Logo">
              </td>
              <td style="text-align: center; padding-bottom: 0px"><h3 style="margin-bottom: 0px">PEMERINTAH KABUPATEN BATU BARA</h3></td>
            </tr>
            <tr>
              <td style="text-align: center; padding-top: 0px; white-space: nowrap">
                <h2 style="margin-bottom: 0px"><?=strtoupper($nmSub)?></h2>
                <p style="margin-top: 0px"><?=$kop1?><br /><?=$kop2?></p>
              </td>
            </tr>
          </table>
          <?php
        } else {
          ?>
          <br />
          <br />
          <br />
          <?php
        }
        ?>
        <table width="100%">
            <tr>
                <td colspan="2" style="text-align: center; vertical-align: middle">
                  <br />
                    <h4>PERNYATAAN PERJANJIAN KINERJA <br  />TAHUN <?= $data[COL_KD_TAHUN] ?></h4>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: justify; vertical-align: middle">
                    <p>Dalam rangka mewujudkan manajemen pemerintahan yang efektif, transparan, dan akuntabel serta
                        berorientasi pada hasil, yang bertanda tangan dibawah ini:</p><br/>
                    <table>
                        <tr>
                            <td>Nama</td>
                            <td style="width: 60px; text-align: right">:</td>
                            <td><?= $sbid[COL_NM_KASUBBID] ?></td>
                        </tr>
                        <tr>
                            <td>Jabatan</td>
                            <td style="width: 60px; text-align: right">:</td>
                            <td style="text-align: left">Kepala <?= $sbid[COL_NM_SUBBID] ?> <?= $nmSub ?> KABUPATEN BATU BARA
                            </td>
                        </tr>
                    </table>
                    <p>Selanjutnya disebut Pihak Pertama</p><br/>
                    <table>
                        <tr>
                            <td>Nama</td>
                            <td style="width: 60px; text-align: right">:</td>
                            <td><?= $sbid[COL_NM_KABID] ?></td>
                        </tr>
                        <tr>
                            <td>Jabatan</td>
                            <td style="width: 60px; text-align: right">:</td>
                            <td style="text-align: left"><?= (strtolower($sbid[COL_NM_BID]) == 'inspektorat' ? 'Inspektur' : (strtolower($sbid[COL_NM_BID]) == 'sekretariat' ? 'Sekretaris' : 'Kepala '.$sbid[COL_NM_BID])) ?> <?= $nmSub ?> KABUPATEN BATU BARA
                            </td>
                        </tr>
                    </table>
                    <p>Selaku atasan Pihak Pertama, selanjutnya disebut Pihak Kedua</p><br/>

                    <p>
                        Pihak Pertama berjanji akan mewujudkan target kinerja yang seharusnya sesuai lampiran perjanjian
                        ini,
                        dalam rangka mencapai target kinerja jangka menengah seperti yang telah ditetapkan dalam dokumen
                        perencanaan. Menjadi tanggung jawab kami.
                    </p>
                    <br/>

                    <p>
                        Pihak Kedua akan melakukan supervisi yang diperlukan serta akan melakukan evaluasi terhadap
                        capaian
                        kinerja dari perjanjian ini dan
                        mengambil tindakan yang diperlukan dalam rangka pemberian penghargaan dan sanksi.
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="2"><br /><br /></td>
            </tr>
            </table>
            <table width="100%">
            <tr>
              <td style="width: 80%; text-align: right">
                BATU BARA,
              </td>
              <td style="white-space: nowrap; padding-left: 100px">
                <?=date("Y")?>
              </td>
            </tr>
            </table>
            <table width="100%">
            <tr>
                <td style="text-align: center">Pihak Kedua,</td>
                <td style="text-align: center">Pihak Pertama,</td>
            </tr>
            <tr>
                <td colspan="2">
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                </td>
            </tr>
            <tr>
                <td style="text-align: center; font-weight: bold"><?= $sbid[COL_NM_KABID] ?></td>
                <td style="text-align: center; font-weight: bold"><?= $sbid[COL_NM_KASUBBID] ?></td>
            </tr>
        </table>
        <pagebreak></pagebreak>
        <table width="100%">
            <tr>
                <td colspan="2" style="text-align: center; vertical-align: middle">
                    <h4>
                        PERJANJIAN KINERJA TAHUN <?= $data[COL_KD_TAHUN] ?>
                        <br/>
                        <?= strtoupper($nmSub) ?>
                        <br/>
                        KABUPATEN BATU BARA
                    </h4>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: left; vertical-align: middle">
                    <table style="border: 1px solid #000; border-spacing: 0" border="1" width="100%">
                        <tr>
                            <th>No.</th>
                            <th>Sasaran Strategis</th>
                            <th>Indikator Kinerja</th>
                            <th>Target</th>
                        </tr>
                        <tr>
                            <th>(1)</th>
                            <th>(2)</th>
                            <th>(3)</th>
                            <th>(4)</th>
                        </tr>
                        <?php
                        if(!empty($SumberData) && $SumberData == 'PERUBAHAN') {
                          $kegiatan = $this->db
                              ->join(TBL_SAKIP_MSATUAN, TBL_SAKIP_MSATUAN . '.' . COL_KD_SATUAN . " = " . TBL_SAKIP_PDPA_KEGIATAN_SASARAN . "." . COL_KD_SATUAN, "left")
                              ->where(COL_KD_URUSAN, $sbid[COL_KD_URUSAN])
                              ->where(COL_KD_BIDANG, $sbid[COL_KD_BIDANG])
                              ->where(COL_KD_UNIT, $sbid[COL_KD_UNIT])
                              ->where(COL_KD_SUB, $sbid[COL_KD_SUB])
                              ->where(COL_KD_BID, $sbid[COL_KD_BID])
                              ->where(COL_KD_SUBBID, $sbid[COL_KD_SUBBID])
                              ->where(COL_KD_PEMDA, $rpemda[COL_KD_PEMDA])
                              ->where(COL_KD_TAHUN, $data[COL_KD_TAHUN])
                              ->group_by(TBL_SAKIP_PDPA_KEGIATAN_SASARAN . "." . COL_KD_KEGIATANOPD)
                              ->group_by(TBL_SAKIP_PDPA_KEGIATAN_SASARAN . "." . COL_KD_SASARANKEGIATANOPD)
                              ->order_by(TBL_SAKIP_PDPA_KEGIATAN_SASARAN . "." . COL_KD_KEGIATANOPD, 'asc')
                              ->get(TBL_SAKIP_PDPA_KEGIATAN_SASARAN)->result_array();
                        } else {
                          $kegiatan = $this->db
                              ->join(TBL_SAKIP_MSATUAN, TBL_SAKIP_MSATUAN . '.' . COL_KD_SATUAN . " = " . TBL_SAKIP_DPA_KEGIATAN_SASARAN . "." . COL_KD_SATUAN, "left")
                              ->where(COL_KD_URUSAN, $sbid[COL_KD_URUSAN])
                              ->where(COL_KD_BIDANG, $sbid[COL_KD_BIDANG])
                              ->where(COL_KD_UNIT, $sbid[COL_KD_UNIT])
                              ->where(COL_KD_SUB, $sbid[COL_KD_SUB])
                              ->where(COL_KD_BID, $sbid[COL_KD_BID])
                              ->where(COL_KD_SUBBID, $sbid[COL_KD_SUBBID])
                              ->where(COL_KD_PEMDA, $rpemda[COL_KD_PEMDA])
                              ->where(COL_KD_TAHUN, $data[COL_KD_TAHUN])
                              ->group_by(TBL_SAKIP_DPA_KEGIATAN_SASARAN . "." . COL_KD_KEGIATANOPD)
                              ->group_by(TBL_SAKIP_DPA_KEGIATAN_SASARAN . "." . COL_KD_SASARANKEGIATANOPD)
                              ->order_by(TBL_SAKIP_DPA_KEGIATAN_SASARAN . "." . COL_KD_KEGIATANOPD, 'asc')
                              ->get(TBL_SAKIP_DPA_KEGIATAN_SASARAN)->result_array();
                        }

                        $counter = 1;
                        ?>
                        <tr>
                        <?php
                        foreach ($kegiatan as $keg) {
                          if(!empty($SumberData) && $SumberData == 'PERUBAHAN') {
                            $kegiatan_indikator = $this->db
                                ->where(COL_KD_URUSAN, $sbid[COL_KD_URUSAN])
                                ->where(COL_KD_BIDANG, $sbid[COL_KD_BIDANG])
                                ->where(COL_KD_UNIT, $sbid[COL_KD_UNIT])
                                ->where(COL_KD_SUB, $sbid[COL_KD_SUB])
                                ->where(COL_KD_BID, $sbid[COL_KD_BID])
                                ->where(COL_KD_SUBBID, $sbid[COL_KD_SUBBID])
                                ->where(COL_KD_PEMDA, $rpemda[COL_KD_PEMDA])
                                ->where(COL_KD_TAHUN, $data[COL_KD_TAHUN])
                                ->where(COL_KD_KEGIATANOPD, $keg[COL_KD_KEGIATANOPD])
                                ->where(COL_KD_SASARANKEGIATANOPD, $keg[COL_KD_SASARANKEGIATANOPD])
                                ->group_by(TBL_SAKIP_PDPA_KEGIATAN_INDIKATOR . "." . COL_KD_INDIKATORKEGIATANOPD)
                                ->order_by(TBL_SAKIP_PDPA_KEGIATAN_INDIKATOR . "." . COL_KD_INDIKATORKEGIATANOPD, 'asc')
                                ->get(TBL_SAKIP_PDPA_KEGIATAN_INDIKATOR)->result_array();
                          } else {
                            $kegiatan_indikator = $this->db
                                ->where(COL_KD_URUSAN, $sbid[COL_KD_URUSAN])
                                ->where(COL_KD_BIDANG, $sbid[COL_KD_BIDANG])
                                ->where(COL_KD_UNIT, $sbid[COL_KD_UNIT])
                                ->where(COL_KD_SUB, $sbid[COL_KD_SUB])
                                ->where(COL_KD_BID, $sbid[COL_KD_BID])
                                ->where(COL_KD_SUBBID, $sbid[COL_KD_SUBBID])
                                ->where(COL_KD_PEMDA, $rpemda[COL_KD_PEMDA])
                                ->where(COL_KD_TAHUN, $data[COL_KD_TAHUN])
                                ->where(COL_KD_KEGIATANOPD, $keg[COL_KD_KEGIATANOPD])
                                ->where(COL_KD_SASARANKEGIATANOPD, $keg[COL_KD_SASARANKEGIATANOPD])
                                ->group_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR . "." . COL_KD_INDIKATORKEGIATANOPD)
                                ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR . "." . COL_KD_INDIKATORKEGIATANOPD, 'asc')
                                ->get(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR)->result_array();
                          }
                            ?>
                            <tr>
                                <td style="text-align: right"><?= $counter ?></td>
                                <td><?= $keg[COL_NM_SASARANKEGIATANOPD] ?></td>
                                <td>
                                    <?php
                                    if (count($kegiatan_indikator) > 1) {
                                        ?>
                                        <ul style="margin-left: 0px; padding-left: 15px">
                                            <?php
                                            foreach ($kegiatan_indikator as $ik) {
                                                ?>
                                                <li><?= $ik[COL_NM_INDIKATORKEGIATANOPD] ?></li>
                                            <?php
                                            }
                                            ?>
                                        </ul>
                                    <?php
                                    } else {
                                        echo count($kegiatan_indikator) > 0 ? $kegiatan_indikator[0][COL_NM_INDIKATORKEGIATANOPD] : "";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if (count($kegiatan_indikator) > 1) {
                                        ?>
                                        <ul style="margin-left: 0px; padding-left: 15px">
                                            <?php
                                            foreach ($kegiatan_indikator as $ik) {
                                                ?>
                                                <li><?= $ik[COL_TARGET]." ".$ik[COL_KD_SATUAN] ?></li>
                                            <?php
                                            }
                                            ?>
                                        </ul>
                                    <?php
                                    } else {
                                        echo count($kegiatan_indikator) > 0 ? $kegiatan_indikator[0][COL_TARGET]." ".$kegiatan_indikator[0][COL_KD_SATUAN] : "";
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php
                            $counter++;
                        }
                        ?>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br/>
                    <br/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table style="border: 1px solid #000; border-spacing: 0" border="1" width="100%">
                        <tr>
                            <th>No.</th>
                            <th>Kegiatan</th>
                            <th>Anggaran</th>
                            <th>Keterangan</th>
                        </tr>
                        <tr>
                            <th>(1)</th>
                            <th>(2)</th>
                            <th>(3)</th>
                            <th>(4)</th>
                        </tr>
                        <?php
                        $counter = 1;
                        $sum = 0;
                        if(!empty($SumberData) && $SumberData == 'PERUBAHAN') {
                          $select = @"
                          sakip_pdpa_kegiatan.*,
                          sakip_pdpa_kegiatan.Budget AS sum_budget,
                          ";
                          $kegiatan_ = $this->db
                              ->where(COL_KD_URUSAN, $sbid[COL_KD_URUSAN])
                              ->where(COL_KD_BIDANG, $sbid[COL_KD_BIDANG])
                              ->where(COL_KD_UNIT, $sbid[COL_KD_UNIT])
                              ->where(COL_KD_SUB, $sbid[COL_KD_SUB])
                              ->where(COL_KD_BID, $sbid[COL_KD_BID])
                              ->where(COL_KD_SUBBID, $sbid[COL_KD_SUBBID])
                              ->where(COL_KD_PEMDA, $rpemda[COL_KD_PEMDA])
                              ->where(COL_KD_TAHUN, $data[COL_KD_TAHUN])
                              ->group_by(TBL_SAKIP_PDPA_KEGIATAN . "." . COL_KD_KEGIATANOPD)
                              ->order_by(TBL_SAKIP_PDPA_KEGIATAN . "." . COL_KD_KEGIATANOPD, 'asc')
                              ->get(TBL_SAKIP_PDPA_KEGIATAN)->result_array();
                        } else {
                          $select = @"
                          sakip_dpa_kegiatan.*,
                          sakip_dpa_kegiatan.Budget AS sum_budget,
                          ";
                          $kegiatan_ = $this->db
                              ->where(COL_KD_URUSAN, $sbid[COL_KD_URUSAN])
                              ->where(COL_KD_BIDANG, $sbid[COL_KD_BIDANG])
                              ->where(COL_KD_UNIT, $sbid[COL_KD_UNIT])
                              ->where(COL_KD_SUB, $sbid[COL_KD_SUB])
                              ->where(COL_KD_BID, $sbid[COL_KD_BID])
                              ->where(COL_KD_SUBBID, $sbid[COL_KD_SUBBID])
                              ->where(COL_KD_PEMDA, $rpemda[COL_KD_PEMDA])
                              ->where(COL_KD_TAHUN, $data[COL_KD_TAHUN])
                              ->group_by(TBL_SAKIP_DPA_KEGIATAN . "." . COL_KD_KEGIATANOPD)
                              ->order_by(TBL_SAKIP_DPA_KEGIATAN . "." . COL_KD_KEGIATANOPD, 'asc')
                              ->get(TBL_SAKIP_DPA_KEGIATAN)->result_array();
                        }
                        foreach ($kegiatan_ as $p) {
                            $nmKeg = $p[COL_NM_KEGIATANOPD];
                            if (empty($nmKeg) && $p[COL_ISEPLAN]) {
                                $eplandb->where(COL_KD_URUSAN, $p[COL_KD_URUSAN]);
                                $eplandb->where(COL_KD_BIDANG, $p[COL_KD_BIDANG]);
                                $eplandb->where(COL_KD_UNIT, $p[COL_KD_UNIT]);
                                $eplandb->where(COL_KD_SUB, $p[COL_KD_SUB]);
                                $eplandb->where("Tahun", $p[COL_KD_TAHUN] - 1);
                                $eplandb->where("Kd_Prog", $p[COL_KD_PROGRAMOPD]);
                                $eplandb->where("Kd_Keg", $p[COL_KD_KEGIATANOPD]);
                                $keg = $eplandb->get("ta_kegiatan")->row_array();
                                if ($keg) {
                                    $nmKeg = $keg["Ket_Kegiatan"];
                                }
                            } else {
                                $nmKeg = $p[COL_NM_KEGIATANOPD];
                            }
                            ?>
                            <tr>
                                <td style="text-align: right"><?= $counter ?></td>
                                <td><?= $nmKeg ?></td>
                                <td style="text-align: right">
                                    <?= "Rp. " . number_format($p[($isPergeseran?COL_PERGESERAN:COL_BUDGET)], 0) ?>
                                </td>
                                <td>
                                    <?=$p[COL_REMARKS]?>
                                </td>
                            </tr>
                            <?php
                            $counter++;
                            $sum = $sum + $p[($isPergeseran?COL_PERGESERAN:COL_BUDGET)];
                        }
                        ?>
                        <tr>
                            <th colspan="2" style="text-align: right">Jumlah</th>
                            <td style="text-align: right"><?="Rp. ".number_format($sum, 0)?></td>
                            <td></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br/>
                    <br/>
                </td>
            </tr>
            </table>
            <table width="100%" style="padding-left: 100px">
            <tr>
                <td style="font-weight: bold"><?= strpos(strtolower($sbid[COL_NM_BID]), "sekretariat") !== false ? 'SEKRETARIS' : 'KEPALA '.strtoupper($sbid[COL_NM_BID]) ?> <?= strtoupper($nmSub) ?></td>
                <td style="font-weight: bold">KEPALA <?= strtoupper($sbid[COL_NM_SUBBID]) ?></td>
            </tr>
            <tr>
                <td colspan="2">
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold"><?= $sbid[COL_NM_KABID] ?></td>
                <td style="font-weight: bold"><?= $sbid[COL_NM_KASUBBID] ?></td>
            </tr>
        </table>
    <?php
    }
} else if(!empty($kd_urusan) && !empty($kd_bidang) && !empty($kd_unit) && !empty($kd_sub) && !empty($kd_bid) && !empty($kd_subbid) && !empty($kd_individu)) {
  $this->db->join(TBL_SAKIP_MSUBBID,
  TBL_SAKIP_MSUBBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_URUSAN." AND ".
  TBL_SAKIP_MSUBBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BIDANG." AND ".
  TBL_SAKIP_MSUBBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_UNIT." AND ".
  TBL_SAKIP_MSUBBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SUB." AND ".
  TBL_SAKIP_MSUBBID.'.'.COL_KD_BID." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_BID." AND ".
  TBL_SAKIP_MSUBBID.'.'.COL_KD_SUBBID." = ".TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SUBBID,"inner");
  $this->db->where(TBL_SAKIP_INDIVIDU_SASARAN.'.'.COL_KD_URUSAN, $this->input->get(COL_KD_URUSAN));
  $this->db->where(TBL_SAKIP_INDIVIDU_SASARAN.'.'.COL_KD_BIDANG, $this->input->get(COL_KD_BIDANG));
  $this->db->where(TBL_SAKIP_INDIVIDU_SASARAN.'.'.COL_KD_UNIT, $this->input->get(COL_KD_UNIT));
  $this->db->where(TBL_SAKIP_INDIVIDU_SASARAN.'.'.COL_KD_SUB, $this->input->get(COL_KD_SUB));
  $this->db->where(TBL_SAKIP_INDIVIDU_SASARAN.'.'.COL_KD_BID, $this->input->get(COL_KD_BID));
  $this->db->where(TBL_SAKIP_INDIVIDU_SASARAN.'.'.COL_KD_SUBBID, $this->input->get(COL_KD_SUBBID));
  $this->db->where(TBL_SAKIP_INDIVIDU_SASARAN.'.'.COL_KD_SASARANINDIVIDU, $this->input->get(COL_KD_SASARANINDIVIDU));
  $this->db->where(TBL_SAKIP_INDIVIDU_SASARAN.'.'.COL_KD_TAHUN, date('Y'));
  $rindividu = $this->db->get(TBL_SAKIP_INDIVIDU_SASARAN)->row_array();

  $this->db->where(TBL_SAKIP_INDIVIDU_INDIKATOR.'.'.COL_KD_URUSAN, $this->input->get(COL_KD_URUSAN));
  $this->db->where(TBL_SAKIP_INDIVIDU_INDIKATOR.'.'.COL_KD_BIDANG, $this->input->get(COL_KD_BIDANG));
  $this->db->where(TBL_SAKIP_INDIVIDU_INDIKATOR.'.'.COL_KD_UNIT, $this->input->get(COL_KD_UNIT));
  $this->db->where(TBL_SAKIP_INDIVIDU_INDIKATOR.'.'.COL_KD_SUB, $this->input->get(COL_KD_SUB));
  $this->db->where(TBL_SAKIP_INDIVIDU_INDIKATOR.'.'.COL_KD_BID, $this->input->get(COL_KD_BID));
  $this->db->where(TBL_SAKIP_INDIVIDU_INDIKATOR.'.'.COL_KD_SUBBID, $this->input->get(COL_KD_SUBBID));
  $this->db->where(TBL_SAKIP_INDIVIDU_INDIKATOR.'.'.COL_KD_SASARANINDIVIDU, $this->input->get(COL_KD_SASARANINDIVIDU));
  $this->db->where(TBL_SAKIP_INDIVIDU_INDIKATOR.'.'.COL_KD_TAHUN, date('Y'));
  $rindikator = $this->db->get(TBL_SAKIP_INDIVIDU_INDIKATOR)->result_array();
  ?>
  <?php
  if(!empty($kop1) && !empty($kop2)) {
    ?>
    <table width="100%" style="border-bottom: 1px solid #000000">
      <tr>
        <td rowspan="2" style="text-align: center; width: 60px; height: 100px">
          <img class="user-image" src="<?=MY_IMAGEURL?>logo.png" style="width: 60px" alt="Logo">
        </td>
        <td style="text-align: center; padding-bottom: 0px"><h3 style="margin-bottom: 0px">PEMERINTAH KABUPATEN BATU BARA</h3></td>
      </tr>
      <tr>
        <td style="text-align: center; padding-top: 0px; white-space: nowrap">
          <h2 style="margin-bottom: 0px"><?=strtoupper($nmSub)?></h2>
          <p style="margin-top: 0px"><?=$kop1?><br /><?=$kop2?></p>
        </td>
      </tr>
    </table>
    <?php
  } else {
    ?>
    <br />
    <br />
    <br />
    <?php
  }
  ?>
  <table width="100%">
      <tr>
          <td colspan="2" style="text-align: center; vertical-align: middle">
            <br />
              <h4>PERNYATAAN PERJANJIAN KINERJA <br  />TAHUN <?= $data[COL_KD_TAHUN] ?></h4>
          </td>
      </tr>
      <tr>
          <td colspan="2" style="text-align: justify; vertical-align: middle">
              <p>Dalam rangka mewujudkan manajemen pemerintahan yang efektif, transparan, dan akuntabel serta
                  berorientasi pada hasil, yang bertanda tangan dibawah ini:</p><br/>
              <table>
                  <tr>
                      <td>Nama</td>
                      <td style="width: 60px; text-align: right">:</td>
                      <td><?= $rindividu[COL_NM_PEGAWAI] ?></td>
                  </tr>
                  <tr>
                      <td>Jabatan</td>
                      <td style="width: 60px; text-align: right">:</td>
                      <td style="text-align: left"><?= $rindividu[COL_NM_JABATAN] ?>
                      </td>
                  </tr>
              </table>
              <p>Selanjutnya disebut Pihak Pertama</p><br/>
              <table>
                  <tr>
                      <td>Nama</td>
                      <td style="width: 60px; text-align: right">:</td>
                      <td><?= $rindividu[COL_NM_KASUBBID] ?></td>
                  </tr>
                  <tr>
                      <td>Jabatan</td>
                      <td style="width: 60px; text-align: right">:</td>
                      <td style="text-align: left">Kepala <?= $rindividu[COL_NM_SUBBID] ?>
                      </td>
                  </tr>
              </table>
              <p>Selaku atasan Pihak Pertama, selanjutnya disebut Pihak Kedua</p><br/>

              <p>
                  Pihak Pertama berjanji akan mewujudkan target kinerja yang seharusnya sesuai lampiran perjanjian
                  ini,
                  dalam rangka mencapai target kinerja jangka menengah seperti yang telah ditetapkan dalam dokumen
                  perencanaan. Menjadi tanggung jawab kami.
              </p>
              <br/>

              <p>
                  Pihak Kedua akan melakukan supervisi yang diperlukan serta akan melakukan evaluasi terhadap
                  capaian
                  kinerja dari perjanjian ini dan
                  mengambil tindakan yang diperlukan dalam rangka pemberian penghargaan dan sanksi.
              </p>
          </td>
      </tr>
      <tr>
          <td colspan="2"><br /><br /></td>
      </tr>
    </table>
    <table width="100%">
      <tr>
        <td style="width: 80%; text-align: right">
          BATU BARA,
        </td>
        <td style="white-space: nowrap; padding-left: 100px">
          <?=date("Y")?>
        </td>
      </tr>
    </table>
    <table width="100%" style="padding-left: 100px">
      <tr>
          <td style="text-align: center">Pihak Kedua,<br /><strong><?=strtoupper('KEPALA '.$rindividu[COL_NM_SUBBID])?></strong></td>
          <td style="text-align: center">Pihak Pertama,<br /><strong><?=strtoupper($rindividu[COL_NM_JABATAN])?></strong></td>
      </tr>
      <tr>
          <td colspan="2">
              <br/>
              <br/>
              <br/>
              <br/>
              <br/>
              <br/>
          </td>
      </tr>
      <tr>
          <td style="text-align: center; font-weight: bold"><?= $rindividu[COL_NM_KASUBBID] ?></td>
          <td style="text-align: center; font-weight: bold"><?= $rindividu[COL_NM_PEGAWAI] ?></td>
      </tr>
    </table>
  <pagebreak></pagebreak>
  <table width="100%">
    <tr>
      <td style="text-align: center; vertical-align: middle">
        <h4>
          PERJANJIAN KINERJA TAHUN <?= $data[COL_KD_TAHUN] ?>
          <br/>
          <?= strtoupper($nmSub) ?>
          <br/>
          KABUPATEN BATU BARA
        </h4>
      </td>
    </tr>
  </table>
  <br />
  <table width="100%" style="border: 1px solid #000; border-spacing: 0" border="1">
    <tr>
      <td style="font-weight: bold; text-align: center">No.</td>
      <td style="font-weight: bold; text-align: center">Sasaran</td>
      <td style="font-weight: bold; text-align: center">Indikator</td>
      <td style="font-weight: bold; text-align: center">Target</td>
    </tr>
    <tr>
      <td style="font-weight: bold; text-align: center">(1)</td>
      <td style="font-weight: bold; text-align: center">(2)</td>
      <td style="font-weight: bold; text-align: center">(3)</td>
      <td style="font-weight: bold; text-align: center">(4)</td>
    </tr>
    <?php
    foreach ($rindikator as $i) {
      ?>
      <tr>
        <td style="text-align: center"><?=$i[COL_KD_INDIKATORINDIVIDU]?></td>
        <td style="vertical-align: middle"><?=$i[COL_NM_SASARANINDIVIDU]?></td>
        <td style="vertical-align: middle"><?=$i[COL_NM_INDIKATORINDIVIDU]?></td>
        <td style="text-align: center; vertical-align: middle"><?=$i[COL_NM_TARGET]?></td>
      </tr>
      <?php
    }
    ?>
  </table>
  <br />
  <br />
  <table width="100%">
    <tr>
      <td style="width: 80%; text-align: right">
        BATU BARA,
      </td>
      <td style="white-space: nowrap; padding-left: 100px">
        <?=date("Y")?>
      </td>
    </tr>
  </table>
  <table width="100%" style="padding-left: 100px">
    <tr>
        <td style="text-align: center">Pihak Kedua,<br /><strong><?=strtoupper('KEPALA '.$rindividu[COL_NM_SUBBID])?></strong></td>
        <td style="text-align: center">Pihak Pertama,<br /><strong><?=strtoupper($rindividu[COL_NM_JABATAN])?></strong></td>
    </tr>
    <tr>
        <td colspan="2">
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
        </td>
    </tr>
    <tr>
        <td style="text-align: center; font-weight: bold"><?= $rindividu[COL_NM_KASUBBID] ?></td>
        <td style="text-align: center; font-weight: bold"><?= $rindividu[COL_NM_PEGAWAI] ?></td>
    </tr>
  </table>
  <?php
}
?>
</body>
</html>
