<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 3/14/2019
 * Time: 11:31 PM
 */
if(!empty($cetak)) {
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=ESAKIP - Rencana Aksi.xls");
}
?>
<style>
    .tbl-info td {
        padding: 4px;
    }
    td.blank {
        color: transparent !important;
    }
</style>
<div class="table-responsive">
    <table class="table table-bordered" style="font-size: 9pt !important; max-width: 200%; overflow-x: scroll" border="1">
        <caption style="text-align: center">
            <h5><?="RENCANA AKSI <br />".strtoupper($nmSub)." KABUPATEN BATU BARA<br />TAHUN ".$data[COL_KD_TAHUN]?>
        </caption>
        <thead>
        <tr>
            <th>SASARAN KINERJA</th>
            <th>INDIKATOR KINERJA</th>
            <th>PENANGGUNG JAWAB</th>

            <th>NO</th>
            <th>PROGRAM</th>
            <th>INDIKATOR PROGRAM</th>
            <th>SATUAN & TARGET</th>
            <th>ANGGARAN</th>
            <th>NO</th>
            <th>KEGIATAN</th>
            <th>INDIKATOR KEGIATAN</th>
            <th>SATUAN</th>
            <th>TARGET / ANGGARAN</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $count_prg = 1;
        $count_keg = 1;
        $last = array(
            COL_KD_PEMDA => "@@",
            COL_KD_MISI => "@@",
            COL_KD_TUJUAN => "@@",
            COL_KD_INDIKATORTUJUAN => "@@",
            COL_KD_SASARAN => "@@",
            COL_KD_INDIKATORSASARAN => "@@",
            COL_KD_TUJUANOPD => "@@",
            COL_KD_INDIKATORTUJUANOPD => "@@",
            COL_KD_SASARANOPD => "@@",
            COL_KD_INDIKATORSASARANOPD => "@@",
            COL_KD_URUSAN => "@@",
            COL_KD_BIDANG => "@@",
            COL_KD_UNIT => "@@",
            COL_KD_SUB => "@@",
            COL_KD_BID => "@@",
            COL_KD_SUBBID => "@@",
            COL_KD_PROGRAMOPD => "@@",
            COL_NM_PROGRAMOPD => "@@",
            COL_KD_SASARANPROGRAMOPD => "@@",
            COL_KD_KEGIATANOPD => "@@",
            COL_NM_KEGIATANOPD => "@@",
            "Satuan2" => "@@",
            "Target2" => "@@"
        );

        foreach($program as $prg) {
            if($prg[COL_KD_PROGRAMOPD].".".$prg[COL_KD_SASARANPROGRAMOPD] != $last[COL_KD_PROGRAMOPD].".".$last[COL_KD_SASARANPROGRAMOPD])  {
                //$count_keg = 1;
            }
            $indikatoropd = $this->db
                ->where(COL_KD_URUSAN, $prg[COL_KD_URUSAN])
                ->where(COL_KD_BIDANG, $prg[COL_KD_BIDANG])
                ->where(COL_KD_UNIT, $prg[COL_KD_UNIT])
                ->where(COL_KD_SUB, $prg[COL_KD_SUB])

                ->where(COL_KD_PEMDA, $prg[COL_KD_PEMDA])
                ->where(COL_KD_MISI, $prg[COL_KD_MISI])
                ->where(COL_KD_TUJUAN, $prg[COL_KD_TUJUAN])
                ->where(COL_KD_INDIKATORTUJUAN, $prg[COL_KD_INDIKATORTUJUAN])
                ->where(COL_KD_SASARAN, $prg[COL_KD_SASARAN])
                ->where(COL_KD_INDIKATORSASARAN, $prg[COL_KD_INDIKATORSASARAN])
                ->where(COL_KD_TUJUANOPD, $prg[COL_KD_TUJUANOPD])
                ->where(COL_KD_INDIKATORTUJUANOPD, $prg[COL_KD_INDIKATORTUJUANOPD])
                ->where(COL_KD_SASARANOPD, $prg[COL_KD_SASARANOPD])
                ->where(COL_KD_INDIKATORSASARANOPD, $prg[COL_KD_INDIKATORSASARANOPD])
                ->get(TBL_SAKIP_MOPD_IKSASARAN)
                ->row_array();
            $sasaranopd = $this->db
                ->where(COL_KD_URUSAN, $prg[COL_KD_URUSAN])
                ->where(COL_KD_BIDANG, $prg[COL_KD_BIDANG])
                ->where(COL_KD_UNIT, $prg[COL_KD_UNIT])
                ->where(COL_KD_SUB, $prg[COL_KD_SUB])

                ->where(COL_KD_PEMDA, $prg[COL_KD_PEMDA])
                ->where(COL_KD_MISI, $prg[COL_KD_MISI])
                ->where(COL_KD_TUJUAN, $prg[COL_KD_TUJUAN])
                ->where(COL_KD_INDIKATORTUJUAN, $prg[COL_KD_INDIKATORTUJUAN])
                ->where(COL_KD_SASARAN, $prg[COL_KD_SASARAN])
                ->where(COL_KD_INDIKATORSASARAN, $prg[COL_KD_INDIKATORSASARAN])
                ->where(COL_KD_TUJUANOPD, $prg[COL_KD_TUJUANOPD])
                ->where(COL_KD_INDIKATORTUJUANOPD, $prg[COL_KD_INDIKATORTUJUANOPD])
                ->where(COL_KD_SASARANOPD, $prg[COL_KD_SASARANOPD])
                ->get(TBL_SAKIP_MOPD_SASARAN)
                ->row_array();

            $indikatorprogram = $this->db
                ->where(COL_KD_URUSAN, $prg[COL_KD_URUSAN])
                ->where(COL_KD_BIDANG, $prg[COL_KD_BIDANG])
                ->where(COL_KD_UNIT, $prg[COL_KD_UNIT])
                ->where(COL_KD_SUB, $prg[COL_KD_SUB])
                //->where(COL_KD_BID, $prg[COL_KD_BID])

                ->where(COL_KD_PEMDA, $prg[COL_KD_PEMDA])
                ->where(COL_KD_MISI, $prg[COL_KD_MISI])
                ->where(COL_KD_TUJUAN, $prg[COL_KD_TUJUAN])
                ->where(COL_KD_INDIKATORTUJUAN, $prg[COL_KD_INDIKATORTUJUAN])
                ->where(COL_KD_SASARAN, $prg[COL_KD_SASARAN])
                ->where(COL_KD_INDIKATORSASARAN, $prg[COL_KD_INDIKATORSASARAN])
                ->where(COL_KD_TUJUANOPD, $prg[COL_KD_TUJUANOPD])
                ->where(COL_KD_INDIKATORTUJUANOPD, $prg[COL_KD_INDIKATORTUJUANOPD])
                ->where(COL_KD_SASARANOPD, $prg[COL_KD_SASARANOPD])
                ->where(COL_KD_INDIKATORSASARANOPD, $prg[COL_KD_INDIKATORSASARANOPD])
                ->where(COL_KD_PROGRAMOPD, $prg[COL_KD_PROGRAMOPD])
                ->where(COL_KD_SASARANPROGRAMOPD, $prg[COL_KD_SASARANPROGRAMOPD])
                ->where(COL_KD_TAHUN, $prg[COL_KD_TAHUN])
                ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_PROGRAMOPD, 'asc')
                ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_SASARANPROGRAMOPD, 'asc')
                ->get(TBL_SAKIP_DPA_PROGRAM_INDIKATOR)
                ->result_array();

            $indikatorprogram_ = "";
            $indikatorprogram_satuan = "";
            $indikatorprogram_target = "";
            if(count($indikatorprogram) > 0) {
                if(count($indikatorprogram) > 1) {
                    $indikatorprogram_ = "<ul style='margin-left: 0px; padding-left: 15px; text-align: left'>";
                    $indikatorprogram_satuan = "<ul style='margin-left: 0px; padding-left: 15px; text-align: left'>";
                    $indikatorprogram_target = "<ul style='margin-left: 0px; padding-left: 15px; text-align: left'>";
                    foreach ($indikatorprogram as $s) {
                        $indikatorprogram_ .= "<li>" . $s[COL_NM_INDIKATORPROGRAMOPD] . "</li>";
                        $indikatorprogram_satuan .= "<li>" . $s[COL_KD_SATUAN] . "</li>";
                        $indikatorprogram_target .= "<li>" . number_format($s[COL_TARGET], 2) . "</li>";
                    }
                    $indikatorprogram_ .= "</ul>";
                    $indikatorprogram_satuan .= "</ul>";
                    $indikatorprogram_target .= "</ul>";
                }
                else {
                    $indikatorprogram_ = $indikatorprogram[0][COL_NM_INDIKATORPROGRAMOPD];
                    $indikatorprogram_satuan = $indikatorprogram[0][COL_KD_SATUAN];
                    $indikatorprogram_target = number_format($indikatorprogram[0][COL_TARGET], 2);
                }
            }

            $indikatorkegiatan = $this->db
                ->where(COL_KD_URUSAN, $prg[COL_KD_URUSAN])
                ->where(COL_KD_BIDANG, $prg[COL_KD_BIDANG])
                ->where(COL_KD_UNIT, $prg[COL_KD_UNIT])
                ->where(COL_KD_SUB, $prg[COL_KD_SUB])
                ->where(COL_KD_BID, $prg[COL_KD_BID])

                ->where(COL_KD_PEMDA, $prg[COL_KD_PEMDA])
                ->where(COL_KD_MISI, $prg[COL_KD_MISI])
                ->where(COL_KD_TUJUAN, $prg[COL_KD_TUJUAN])
                ->where(COL_KD_INDIKATORTUJUAN, $prg[COL_KD_INDIKATORTUJUAN])
                ->where(COL_KD_SASARAN, $prg[COL_KD_SASARAN])
                ->where(COL_KD_INDIKATORSASARAN, $prg[COL_KD_INDIKATORSASARAN])
                ->where(COL_KD_TUJUANOPD, $prg[COL_KD_TUJUANOPD])
                ->where(COL_KD_INDIKATORTUJUANOPD, $prg[COL_KD_INDIKATORTUJUANOPD])
                ->where(COL_KD_SASARANOPD, $prg[COL_KD_SASARANOPD])
                ->where(COL_KD_INDIKATORSASARANOPD, $prg[COL_KD_INDIKATORSASARANOPD])
                ->where(COL_KD_PROGRAMOPD, $prg[COL_KD_PROGRAMOPD])
                ->where(COL_KD_SASARANPROGRAMOPD, $prg[COL_KD_SASARANPROGRAMOPD])
                ->where(COL_KD_KEGIATANOPD, $prg[COL_KD_KEGIATANOPD])
                //->where(COL_KD_SASARANKEGIATANOPD, $prg[COL_KD_SASARANKEGIATANOPD])
                ->where(COL_KD_TAHUN, $prg[COL_KD_TAHUN])
                ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_PROGRAMOPD, 'asc')
                ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_SASARANPROGRAMOPD, 'asc')
                ->get(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR)
                ->result_array();

            /*$indikatorkegiatan_ = "";
            if(count($indikatorkegiatan) > 0) {
                $indikatorkegiatan_ = "<ul style='margin-left: 0px; padding-left: 15px; text-align: left'>";
                foreach($indikatorkegiatan as $s) {
                    $indikatorkegiatan_ .= "<li>".$s[COL_NM_INDIKATORKEGIATANOPD]."</li>";
                }
                $indikatorkegiatan_ .= "</ul>";
            }*/

            ?>
            <tr>
                <?php
                $curr_prg = $prg[COL_KD_PEMDA].$prg[COL_KD_MISI].$prg[COL_KD_TUJUAN].$prg[COL_KD_INDIKATORTUJUAN].$prg[COL_KD_SASARAN].$prg[COL_KD_INDIKATORSASARAN].$prg[COL_KD_TUJUANOPD].$prg[COL_KD_INDIKATORTUJUANOPD].$prg[COL_KD_SASARANOPD].$prg[COL_KD_INDIKATORSASARANOPD].$prg[COL_KD_PROGRAMOPD];
                $last_prg = $last[COL_KD_PEMDA].$last[COL_KD_MISI].$last[COL_KD_TUJUAN].$last[COL_KD_INDIKATORTUJUAN].$last[COL_KD_SASARAN].$last[COL_KD_INDIKATORSASARAN].$last[COL_KD_TUJUANOPD].$last[COL_KD_INDIKATORTUJUANOPD].$last[COL_KD_SASARANOPD].$last[COL_KD_INDIKATORSASARANOPD].$last[COL_KD_PROGRAMOPD];
                if(/*$prg[COL_NM_PROGRAMOPD]!=$last[COL_NM_PROGRAMOPD]*/$curr_prg!=$last_prg){
                    ?>
                    <td <?=$prg["count_program_span"]>1?'rowspan="'.$prg["count_program_span"].'"':''?>><?=$sasaranopd[COL_NM_SASARANOPD]?></td>
                    <td <?=$prg["count_program_span"]>1?'rowspan="'.$prg["count_program_span"].'"':''?>><?=$indikatoropd[COL_NM_INDIKATORSASARANOPD]?></td>
                    <td <?=$prg["count_program_span"]>1?'rowspan="'.$prg["count_program_span"].'"':''?>><?='Kepala '.$nmSub?></td>
                    <td <?=$prg["count_program_span"]>1?'rowspan="'.$prg["count_program_span"].'"':''?>><?=$count_prg?></td>
                    <td <?=$prg["count_program_span"]>1?'rowspan="'.$prg["count_program_span"].'"':''?>><?=$prg[COL_NM_PROGRAMOPD]?></td>
                <?php
                }
                ?>

                <?php
              if($curr_prg/*.".".$prg[COL_KD_BID].".".$prg[COL_KD_SASARANPROGRAMOPD]*/!=$last_prg/*.".".$last[COL_KD_BID].".".$last[COL_KD_SASARANPROGRAMOPD]*/){
                    $count_keg = 1;
                    ?>
                    <td <?=$prg["count_program_span"]>1?'rowspan="'.$prg["count_program_span"].'"':''?>><?=$indikatorprogram_?></td>
                    <td <?=$prg["count_program_span"]>1?'rowspan="'.$prg["count_program_span"].'"':''?>>
                        <?php
                        foreach($indikatorprogram as $s) {
                            ?>
                            <table style="white-space: nowrap; margin-bottom: 10px" frame="below">
                                <tr style="background-color: darkgray">
                                    <td style="padding: 3px">Satuan</td>
                                    <td>:</td>
                                    <td style="padding: 3px; font-weight: bold"><?=$s[COL_KD_SATUAN]?></td>
                                </tr>
                                <tr style="background-color: darkgray">
                                    <td style="padding: 3px; border-bottom: 1px solid #000">Target</td>
                                    <td style=" border-bottom: 1px solid #000">:</td>
                                    <td style="padding: 3px; border-bottom: 1px solid #000; text-align: right; font-weight: bold"><?=number_format($s[COL_TARGET],2)?></td>
                                </tr>
                                <tr>
                                    <td style="padding: 3px">TW I</td>
                                    <td>:</td>
                                    <td style="padding: 3px; text-align: right; font-weight: bold"><?=number_format($s[COL_TARGET_TW1],2)?></td>
                                </tr>
                                <tr>
                                    <td style="padding: 3px">TW II</td>
                                    <td>:</td>
                                    <td style="padding: 3px; text-align: right; font-weight: bold"><?=number_format($s[COL_TARGET_TW2],2)?></td>
                                </tr>
                                <tr>
                                    <td style="padding: 3px">TW III</td>
                                    <td>:</td>
                                    <td style="padding: 3px; text-align: right; font-weight: bold"><?=number_format($s[COL_TARGET_TW3],2)?></td>
                                </tr>
                                <tr>
                                    <td style="padding: 3px">TW IV</td>
                                    <td>:</td>
                                    <td style="padding: 3px; text-align: right; font-weight: bold"><?=number_format($s[COL_TARGET_TW4],2)?></td>
                                </tr>
                            </table>
                            <?php
                        }
                        ?>
                    </td>
                    <td <?=$prg["count_program_span"]>1?'rowspan="'.$prg["count_program_span"].'"':''?> style="text-align: right">
                        <!--<ul style='margin-left: 0px; padding-left: 15px; text-align: left'>
                            <li style='white-space: nowrap'>TW 1 : <?=number_format($prg["Program_TW1"],2)?></li>
                            <li style='white-space: nowrap'>TW 2 : <?=number_format($prg["Program_TW2"],2)?></li>
                            <li style='white-space: nowrap'>TW 3 : <?=number_format($prg["Program_TW3"],2)?></li>
                            <li style='white-space: nowrap'>TW 4 : <?=number_format($prg["Program_TW4"],2)?></li>
                        </ul>-->
                        <table style="white-space: nowrap">
                            <tr>
                                <td style="padding: 3px">TW I</td>
                                <td>:</td>
                                <td style="padding: 3px; text-align: right; font-weight: bold"><?=number_format($prg["sum_program_tw1"],0)?></td>
                            </tr>
                            <tr>
                                <td style="padding: 3px">TW II</td>
                                <td>:</td>
                                <td style="padding: 3px; text-align: right; font-weight: bold"><?=number_format($prg["sum_program_tw2"],0)?></td>
                            </tr>
                            <tr>
                                <td style="padding: 3px">TW III</td>
                                <td>:</td>
                                <td style="padding: 3px; text-align: right; font-weight: bold"><?=number_format($prg["sum_program_tw3"],0)?></td>
                            </tr>
                            <tr>
                                <td style="padding: 3px">TW IV</td>
                                <td>:</td>
                                <td style="padding: 3px; text-align: right; font-weight: bold"><?=number_format($prg["sum_program_tw4"],0)?></td>
                            </tr>
                        </table>
                    </td>
                <?php
                }
                ?>
                <?php
                /*if($prg[COL_NM_PROGRAMOPD]!=$last[COL_NM_PROGRAMOPD]){
                    ?>
                    <td <?=$prg["count_program_span"]>0?'rowspan="'.$prg["count_program_span"].'"':''?> style="text-align: right"><?=number_format($prg["sum_program_total"], 0)?></td>
                <?php
                }*/
                ?>
                <?php
                if($curr_prg.".".$prg[COL_KD_SASARANPROGRAMOPD].".".$prg[COL_KD_KEGIATANOPD].".".$prg[COL_KD_BID].".".$prg[COL_KD_SUBBID]!=$last_prg.".".$last[COL_KD_SASARANPROGRAMOPD].".".$last[COL_KD_KEGIATANOPD].".".$last[COL_KD_BID].".".$last[COL_KD_SUBBID]){
                    ?>
                    <td <?=$prg["count_kegiatan_span"]>1?'rowspan="'.$prg["count_kegiatan_span"].'"':''?>><?=$count_keg?></td>
                    <td <?=$prg["count_kegiatan_span"]>1?'rowspan="'.$prg["count_kegiatan_span"].'"':''?>><?=$prg[COL_NM_KEGIATANOPD]?></td>
                <?php
                }
                ?>
                <td><?=$prg[COL_NM_INDIKATORKEGIATANOPD]?></td>
                <td><?=$prg["Satuan2"]?></td>
                <?php
                if($curr_prg.".".$prg[COL_KD_SASARANPROGRAMOPD].".".$prg[COL_KD_KEGIATANOPD].".".$prg[COL_KD_BID].".".$prg[COL_KD_SUBBID]!=$last_prg.".".$last[COL_KD_SASARANPROGRAMOPD].".".$last[COL_KD_KEGIATANOPD].".".$last[COL_KD_BID].".".$last[COL_KD_SUBBID]){
                    ?>
                    <td <?=$prg["count_kegiatan_span"]>1?'rowspan="'.$prg["count_kegiatan_span"].'"':''?> style="text-align: right">
                        <table style="white-space: nowrap">
                            <tr>
                                <td style="padding: 3px">TW I</td>
                                <td>:</td>
                                <td style="padding: 3px; text-align: right; font-weight: bold"><?=number_format($prg["Kegiatan_TW1"],2)?></td>
                                <td>/</td>
                                <td style="padding: 3px; text-align: right; font-weight: bold"><?=number_format($prg["B_Kegiatan_TW1"],0)?></td>
                            </tr>
                            <tr>
                                <td style="padding: 3px">TW II</td>
                                <td>:</td>
                                <td style="padding: 3px; text-align: right; font-weight: bold"><?=number_format($prg["Kegiatan_TW2"],2)?></td>
                                <td>/</td>
                                <td style="padding: 3px; text-align: right; font-weight: bold"><?=number_format($prg["B_Kegiatan_TW2"],0)?></td>
                            </tr>
                            <tr>
                                <td style="padding: 3px">TW III</td>
                                <td>:</td>
                                <td style="padding: 3px; text-align: right; font-weight: bold"><?=number_format($prg["Kegiatan_TW3"],2)?></td>
                                <td>/</td>
                                <td style="padding: 3px; text-align: right; font-weight: bold"><?=number_format($prg["B_Kegiatan_TW3"],0)?></td>
                            </tr>
                            <tr>
                                <td style="padding: 3px">TW IV</td>
                                <td>:</td>
                                <td style="padding: 3px; text-align: right; font-weight: bold"><?=number_format($prg["Kegiatan_TW4"],2)?></td>
                                <td>/</td>
                                <td style="padding: 3px; text-align: right; font-weight: bold"><?=number_format($prg["B_Kegiatan_TW4"],0)?></td>
                            </tr>
                        </table>
                    </td>
                <?php
                }
                ?>
                <?php
                /*if($prg[COL_NM_KEGIATANOPD]!=$last[COL_NM_KEGIATANOPD]){
                    ?>
                    <td style="text-align: right" <?=$prg["count_kegiatan_span"]>0?'rowspan="'.$prg["count_kegiatan_span"].'"':''?>><?=number_format($prg["Total2"], 0)?></td>
                <?php
                }*/
                ?>
            </tr>
            <?php
            if($curr_prg!=$last_prg)  {
                $count_prg++;
            }
            if($curr_prg.".".$prg[COL_KD_SASARANPROGRAMOPD].".".$prg[COL_KD_KEGIATANOPD].".".$prg[COL_KD_BID].".".$prg[COL_KD_SUBBID]!=$last_prg.".".$last[COL_KD_SASARANPROGRAMOPD].".".$last[COL_KD_KEGIATANOPD].".".$last[COL_KD_BID].".".$last[COL_KD_SUBBID])  {
                $count_keg++;
            }
            $last = array(
                COL_KD_PEMDA => $prg[COL_KD_PEMDA],
                COL_KD_MISI => $prg[COL_KD_MISI],
                COL_KD_TUJUAN => $prg[COL_KD_TUJUAN],
                COL_KD_INDIKATORTUJUAN => $prg[COL_KD_INDIKATORTUJUAN],
                COL_KD_SASARAN => $prg[COL_KD_SASARAN],
                COL_KD_INDIKATORSASARAN => $prg[COL_KD_INDIKATORSASARAN],
                COL_KD_TUJUANOPD => $prg[COL_KD_TUJUANOPD],
                COL_KD_INDIKATORTUJUANOPD => $prg[COL_KD_INDIKATORTUJUANOPD],
                COL_KD_SASARANOPD => $prg[COL_KD_SASARANOPD],
                COL_KD_INDIKATORSASARANOPD => $prg[COL_KD_INDIKATORSASARANOPD],
                COL_KD_URUSAN => $prg[COL_KD_URUSAN],
                COL_KD_BIDANG => $prg[COL_KD_BIDANG],
                COL_KD_UNIT => $prg[COL_KD_UNIT],
                COL_KD_SUB => $prg[COL_KD_SUB],
                COL_KD_BID => $prg[COL_KD_BID],
                COL_KD_SUBBID => $prg[COL_KD_SUBBID],
                COL_KD_PROGRAMOPD => $prg[COL_KD_PROGRAMOPD],
                COL_NM_PROGRAMOPD => $prg[COL_NM_PROGRAMOPD],
                COL_KD_SASARANPROGRAMOPD => $prg[COL_KD_SASARANPROGRAMOPD],
                COL_KD_KEGIATANOPD => $prg[COL_KD_KEGIATANOPD],
                COL_NM_KEGIATANOPD => $prg[COL_NM_KEGIATANOPD],
                COL_KD_INDIKATORKEGIATANOPD => $prg[COL_KD_INDIKATORKEGIATANOPD],
                "Satuan2" => $prg["Satuan2"],
                "Target2" => $prg["Target2"]
            );
            /*$last[COL_KD_BID] = $prg[COL_KD_BID];
            $last[COL_KD_PROGRAMOPD] = $prg[COL_KD_PROGRAMOPD];
            $last[COL_KD_KEGIATANOPD] = $prg[COL_KD_KEGIATANOPD];
            $last[COL_NM_PROGRAMOPD] = $prg[COL_NM_PROGRAMOPD];
            $last[COL_KD_SASARANPROGRAMOPD] = $prg[COL_KD_SASARANPROGRAMOPD];
            $last["Satuan1"] = $prg["Satuan1"];
            $last["Target1"] = $prg["Target1"];
            $last[COL_NM_KEGIATANOPD] = $prg[COL_NM_KEGIATANOPD];
            $last[COL_KD_SASARANKEGIATANOPD] = $prg[COL_KD_SASARANKEGIATANOPD];
            $last["Satuan2"] = $prg["Satuan2"];
            $last["Target2"] = $prg["Target2"];
            $last[COL_KD_BID] = $prg[COL_KD_BID];
            $last[COL_KD_SUBBID] = $prg[COL_KD_SUBBID];*/
        }
        ?>
        </tbody>
    </table>
</div>
