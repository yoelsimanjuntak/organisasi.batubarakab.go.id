<?php
class Home extends MY_Controller {

  public function index() {
    $data['title'] = 'Beranda';

    $this->load->model('mpost');

    $data['berita'] = $this->mpost->search(9,"",1);
    $data['galeri'] = $this->mpost->search(10,"",2);
    $data['dokumen'] = $this->mpost->search(5,"",3);
		//$this->template->set('title', 'Home');
		$this->template->load('gotto' , 'home/index', $data);
    //$this->load->view('home/index');
  }

  public function index_temp() {
    $data['title'] = 'Beranda';
		$this->template->load('frontend-mediplus' , 'home/index_temp', $data);
  }

  public function page($slug) {
    $this->load->model('mpost');
    $data['title'] = 'Page';

    $this->db->join(TBL__POSTCATEGORIES,TBL__POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL__POSTS.".".COL_POSTCATEGORYID,"inner");
    $this->db->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__POSTS.".".COL_CREATEDBY,"inner");
    $this->db->where("(".TBL__POSTS.".".COL_POSTSLUG." = '".$slug."' OR ".TBL__POSTS.".".COL_POSTID." = '".$slug."')");
    $rpost = $this->db->get(TBL__POSTS)->row_array();
    if(!$rpost) {
        show_404();
        return false;
    }

    $this->db->where(COL_POSTID, $rpost[COL_POSTID]);
    $this->db->set(COL_TOTALVIEW, COL_TOTALVIEW."+1", FALSE);
    $this->db->update(TBL__POSTS);

    $data['title'] = $rpost[COL_POSTCATEGORYNAME];//strlen($rpost[COL_POSTTITLE]) > 50 ? substr($rpost[COL_POSTTITLE], 0, 50) . "..." : $rpost[COL_POSTTITLE];
    $data['data'] = $rpost;
    $data['berita'] = $this->mpost->search(5,"",1);
    $this->template->load('gotto' , 'home/page', $data);
  }

  public function _404() {
    $data['title'] = 'Error';
    if(IsLogin()) {
      $this->template->load('backend' , 'home/_error', $data);
    } else {
      $this->template->load('frontend' , 'home/_error', $data);
    }
  }

  public function post_old($cat) {
    $data['title'] = 'Tautan';
    $start = !empty($_GET['start'])&&$_GET['start']>0?$_GET['start']:0;

    if(!empty($_GET['dateFrom'])) $this->db->where(TBL__POSTS.".".COL_CREATEDON." >= ", $_GET['dateFrom']);
    if(!empty($_GET['dateTo'])) $this->db->where(TBL__POSTS.".".COL_CREATEDON." <= ", $_GET['dateTo']);
    if(!empty($_GET['keyword'])) {
      $this->db->group_start();
      $this->db->like(TBL__POSTS.".".COL_POSTCONTENT, $_GET['keyword']);
      $this->db->or_like(TBL__POSTS.".".COL_POSTTITLE, $_GET['keyword']);
      $this->db->group_end();
    }

    $q = $this->db
    ->join(TBL__POSTCATEGORIES,TBL__POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL__POSTS.".".COL_POSTCATEGORYID,"inner")
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__POSTS.".".COL_CREATEDBY,"inner")
    ->where(TBL__POSTS.".".COL_POSTCATEGORYID, $cat)
    ->order_by(TBL__POSTS.".".COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL__POSTS, TRUE);

    $data['res'] = $rpost = $this->db->query($q." LIMIT 10 OFFSET $start")->result_array();
    $data['count'] = $this->db->query($q)->num_rows();
    $data['start'] = $start;

    if(!empty($rpost)) {
      $data['title'] = $rpost[0][COL_POSTCATEGORYNAME];
    }

    $data['data'] = $rpost;
    $this->template->load('frontend' , 'home/post', $data);
  }

  public function post($cat) {
    $data['title'] = 'Tautan';
    $start = !empty($_GET['start'])&&$_GET['start']>0?$_GET['start']:0;
    $rcat = $this->db
    ->where(COL_POSTCATEGORYID, $cat)
    ->get(TBL__POSTCATEGORIES)
    ->row_array();
    if(empty($rcat)) {
      show_404();
      return false;
    }

    if(!empty($_GET['dateFrom'])) $this->db->where(TBL__POSTS.".".COL_CREATEDON." >= ", $_GET['dateFrom']);
    if(!empty($_GET['dateTo'])) $this->db->where(TBL__POSTS.".".COL_CREATEDON." <= ", $_GET['dateTo']);
    if(!empty($_GET['keyword'])) {
      $this->db->group_start();
      $this->db->like(TBL__POSTS.".".COL_POSTCONTENT, $_GET['keyword']);
      $this->db->or_like(TBL__POSTS.".".COL_POSTTITLE, $_GET['keyword']);
      $this->db->group_end();
    }

    $q = $this->db
    ->join(TBL__POSTCATEGORIES,TBL__POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL__POSTS.".".COL_POSTCATEGORYID,"inner")
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__POSTS.".".COL_CREATEDBY,"inner")
    ->where(TBL__POSTS.".".COL_POSTCATEGORYID, $cat)
    ->order_by(TBL__POSTS.".".COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL__POSTS, TRUE);

    $data['res'] = $rpost = $this->db->query($q." LIMIT 9 OFFSET $start")->result_array();
    $data['count'] = $this->db->query($q)->num_rows();
    $data['start'] = $start;

    if(!empty($rpost)) {
      $data['title'] = $rpost[0][COL_POSTCATEGORYNAME];
    }

    $data['data'] = $rpost;
    $data['rcat'] = $rcat;
    $this->template->load('gotto' , 'home/post', $data);
  }

  public function lapor() {
    $data['title'] = 'LAPORKAN BENCANA';
    if(!empty($_POST)) {
      $dat = array(
        COL_NMPELAPOR=>$this->input->post(COL_NMPELAPOR),
        COL_NMNIK=>$this->input->post(COL_NMNIK),
        COL_NMALAMAT=>$this->input->post(COL_NMALAMAT),
        COL_NMNOMORHP=>$this->input->post(COL_NMNOMORHP),
        COL_NMLAPORAN=>$this->input->post(COL_NMLAPORAN),
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );

      $res = $this->db->insert(TBL_TLAPORAN, $dat);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        return;
      }

      ShowJsonSuccess('Laporan berhasil dikirim. Terima kasih.');
      return;
    }
    $this->template->load('frontend' , 'home/lapor', $data);
  }

  public function report() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN) {
      redirect('site/user/dashboard');
    }

    $data['title'] = 'LAPORAN BENCANA';
    $this->template->load('backend' , 'home/report', $data);
  }

  public function report_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_CREATEDON);
    $cols = array(COL_NMPELAPOR, COL_NMNIK, COL_NMNOMORHP, COL_NMALAMAT, COL_NMLAPORAN);
    $queryTotal = $this->db->query("select * from tlaporan");

    $i = 0;
    foreach($cols as $item){
      if($_POST['search']['value']){
        if($i===0){
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        }else{
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(isset($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(isset($orderdef)){
        $order = $orderdef;
        $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->get_compiled_select(TBL_TLAPORAN, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];
    $n=0;
    foreach($rec->result_array() as $r) {
      $data[] = array(
        '<a href="'.site_url('site/home/report-view/'.$r[COL_UNIQ]).'" class="btn-view-laporan"><i class="fad fa-eye"></i></a>',
        !empty($r[COL_CREATEDON])?date('Y-m-d H:i', strtotime($r[COL_CREATEDON])):'-',
        $r[COL_NMPELAPOR],
        $r[COL_NMNIK],
        $r[COL_NMNOMORHP],
        $r[COL_NMALAMAT],
        strlen($r[COL_NMLAPORAN]) > 50 ? substr($r[COL_NMLAPORAN], 0, 50) . "..." : $r[COL_NMLAPORAN]
      );
      $n++;
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryTotal->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function report_view($id) {
    $rdata = $this->db
    ->select('tlaporan.*')
    ->where(COL_UNIQ, $id)
    ->get(TBL_TLAPORAN)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }

    $html = @"
    <table class=\"table table-striped mb-0\" width=\"100%%\">
    <tr>
      <td>Waktu</td><td>:</td><td>%s</td>
    </tr>
    <tr>
      <td>Nama</td><td>:</td><td>%s</td>
    </tr>
    <tr>
      <td>NIK</td><td>:</td><td>%s</td>
    </tr>
    <tr>
      <td>Nomor HP</td><td>:</td><td>%s</td>
    </tr>
    <tr>
      <td>Alamat</td><td>:</td><td>%s</td>
    </tr>
    </table>
    <p class=\"pl-3 pr-3 pt-2\" style=\"white-space: pre-line; border-top: 1px solid #dedede\">%s</p>
    ";
    echo sprintf($html, (!empty($rdata[COL_CREATEDON])?date('Y-m-d H:i',strtotime($rdata[COL_CREATEDON])):'-'),$rdata[COL_NMPELAPOR],$rdata[COL_NMNIK],$rdata[COL_NMNOMORHP],$rdata[COL_NMALAMAT],$rdata[COL_NMLAPORAN]);
  }

  public function sakip() {
    $data['title'] = 'Rincian Kinerja Perangkat Daerah';
    $this->template->load('frontend-adminlte' , 'home/sakip', $data);
  }
}
 ?>
