<?php
$this->db->where(COL_PMDTAHUNMULAI." <=", date("Y"));
$this->db->where(COL_PMDTAHUNAKHIR." >=", date("Y"));
$this->db->order_by(COL_PMDTAHUNMULAI, "desc");
$rperiod = $this->db->get(TBL_SAKIPV2_PEMDA)->row_array();

$rmisi = $this->db
->where(COL_IDPMD, !empty($rperiod[COL_PMDID])?$rperiod[COL_PMDID]:-999)
->order_by(COL_MISINO, "asc")
->get(TBL_SAKIPV2_PEMDA_MISI)
->result_array();

$rtujuan = $this->db
->where(COL_KD_PEMDA, !empty($rperiod[COL_PMDID])?$rperiod[COL_PMDID]:-999)
->order_by(COL_KD_MISI)
->order_by(COL_KD_TUJUAN)
->get(TBL_SAKIP_MPMD_TUJUAN)
->result_array();

$rsasaran = $this->db
->where(COL_KD_PEMDA, !empty($rperiod[COL_PMDID])?$rperiod[COL_PMDID]:-999)
->order_by(COL_KD_MISI)
->order_by(COL_KD_TUJUAN)
->order_by(COL_KD_INDIKATORTUJUAN)
->order_by(COL_KD_SASARAN)
->get(TBL_SAKIP_MPMD_SASARAN)
->result_array();
?>
<section class="hero-section d-flex justify-content-center align-items-center" style="background-image: url('<?=MY_IMAGEURL.'img-bg-hero.png'?>')">
  <div class="section-overlay" style="opacity: 0.75 !important"></div>
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-12 mb-5 mb-lg-0">
        <div class="hero-section-text mt-5">
          <!--<h6 class="text-white">Selamat Datang</h6>-->
          <h4 class="hero-title text-white mt-4 mb-4"><?=nl2br($this->setting_web_desc)?></h4>
          <a href="#contact-section" class="custom-btn btn">Hubungi Kami <i class="far fa-arrow-right-circle"></i></a>
        </div>
      </div>
      <div class="col-lg-6 col-12">
        <div class="owl-carousel owl-theme carousel-hero">
          <?php
          foreach(glob(MY_IMAGEPATH.'slider/*') as $filename) {
            ?>
            <div class="item" style="height: 360px; background-image: url('<?=MY_IMAGEURL.'slider/'.basename($filename)?>'); background-size: cover; background-position: center; border-radius: 2%"></div>
            <?php
          }
          ?>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="categories-section section-padding" id="categories-section">
  <div class="container">
    <div class="row justify-content-center align-items-center">
      <div class="col-lg-12 col-12 text-center">
        <h2 class="mb-5">Portal Aplikasi</h2>
      </div>
      <div class="col-lg-2 col-md-2 col-6">
        <div class="categories-block">
          <a href="https://bagianorganisasi.batubarakab.go.id/sakipv2/user/login.jsp" class="d-flex flex-column justify-content-center align-items-center h-100">
            <i class="categories-icon bi-window"></i>
            <small class="categories-block-title">E-SAKIP</small>
          </a>
        </div>
      </div>
      <div class="col-lg-2 col-md-2 col-6">
        <div class="categories-block">
          <a href="https://bagianorganisasi.batubarakab.go.id/skm/" class="d-flex flex-column justify-content-center align-items-center h-100">
            <i class="categories-icon bi-people"></i>
            <small class="categories-block-title">SI SUKMA</small>
          </a>
        </div>
      </div>
      <div class="col-lg-2 col-md-2 col-6">
        <div class="categories-block">
          <a href="https://bagianorganisasi.batubarakab.go.id/rb/user/login.jsp" class="d-flex flex-column justify-content-center align-items-center h-100">
            <i class="categories-icon bi-window"></i>
            <small class="categories-block-title">SI RB</small>
          </a>
        </div>
      </div>
      <div class="col-lg-2 col-md-2 col-6">
        <div class="categories-block">
          <a href="<?=site_url('site/user/login')?>" class="d-flex flex-column justify-content-center align-items-center h-100">
            <i class="categories-icon bi-globe"></i>
            <small class="categories-block-title">CMS</small>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="pb-5">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-12">
        <div class="custom-text-block custom-border-radius-start">
          <h4 class="text-white mb-0">PEMERINTAH KABUPATEN BATU BARA</h4>
          <h6 class="text-white mb-4">PERIODE <?=$rperiod?$rperiod[COL_PMDTAHUNMULAI]." - ".$rperiod[COL_PMDTAHUNAKHIR]:"-"?></h6>
          <p class="text-white mb-1">Kepala Daerah <span style="float: right; font-weight: bold; color: var(--secondary-color);"><?=$rperiod?$rperiod[COL_PMDPEJABAT]:"-"?></span></p>
          <p class="text-white mb-1">Wakil Kepala Daerah <span style="float: right; font-weight: bold; color: var(--secondary-color);"><?=$rperiod?$rperiod[COL_PMDPEJABATWAKIL]:"-"?></span></p>
          <p class="text-white mb-1">Visi:</p>
          <p class="text-white mb-1" style="font-style: italic"><?=$rperiod[COL_PMDVISI]?></p>
        </div>
      </div>
      <div class="col-lg-6 col-12">
        <div class="video-thumb">
          <img src="<?=MY_IMAGEURL.'img-bupati.png'?>" class="about-image custom-border-radius-end img-fluid" alt="">
        </div>
      </div>
    </div>
  </div>
</section>
<section class="cta-section mt-5" style="background: url('<?=MY_IMAGEURL.'img-bg-section.jpeg'?>') !important">
  <div class="section-overlay" style="background: var(--border-color) !important"></div>
  <div class="container">
    <div class="row justify-content-center align-items-center">
      <div class="col-lg-2 col-md-2 col-6">
        <div class="categories-block">
          <a href="https://bagianorganisasi.batubarakab.go.id/sakipv2/user/login.jsp" class="d-flex flex-column justify-content-center align-items-center h-100">
            <h3 class="text-white mb-0"><?=number_format(count($rmisi))?></h3>
            <small class="categories-block-title">MISI</small>
          </a>
        </div>
      </div>
      <div class="col-lg-2 col-md-2 col-6">
        <div class="categories-block">
          <a href="https://bagianorganisasi.batubarakab.go.id/rb/user/login.jsp" class="d-flex flex-column justify-content-center align-items-center h-100">
            <h3 class="text-white mb-0"><?=number_format(count($rtujuan))?></h3>
            <small class="categories-block-title">TUJUAN</small>
          </a>
        </div>
      </div>
      <div class="col-lg-2 col-md-2 col-6">
        <div class="categories-block">
          <a href="https://bagianorganisasi.batubarakab.go.id/skm/" class="d-flex flex-column justify-content-center align-items-center h-100">
            <h3 class="text-white mb-0"><?=number_format(count($rsasaran))?></h3>
            <small class="categories-block-title">SASARAN</small>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="about-section section-padding">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-12">
        <div class="about-image-wrap custom-border-radius-start">
          <img src="<?=MY_IMAGEURL.'img-bg-kabag.png'?>" class="about-image custom-border-radius-start img-fluid" alt="" style="height: 400px">
          <div class="about-info" style="border-top: 15px solid var(--custom-btn-bg-color) !important; border-radius: 15px !important; padding: 15px !important">
            <h6 class="text-white mb-0 me-2" style="font-size: 12pt">Ihramli Efendi, S.Sos.I</h6>
            <p class="text-white mb-0" style="font-size: 12pt">Kepala Bagian Organisasi</p>
          </div>
        </div>
      </div>
      <div class="col-lg-5 col-12">
        <div class="instagram-block owl-carousel carousel-galeri">
          <?php
          foreach(glob(MY_IMAGEPATH.'slider-galeri/*') as $filename) {
            ?>
            <div class="item" style="height: 400px; background-image: url('<?=MY_IMAGEURL.'slider-galeri/'.basename($filename)?>'); background-size: cover; background-position: center;"></div>
            <?php
          }
          ?>
          <!--<img src="<?=MY_IMAGEURL.'img-bg-kabag.png'?>" class="about-image custom-border-radius-end img-fluid" alt="">-->
          <!--<div class="instagram-block-text">
            <a href="#" class="custom-btn btn" target="_blank">Lorem Ipsum</a>
          </div>-->
        </div>
      </div>
      <div class="col-lg-4 col-12">
        <div class="custom-text-block custom-border-radius-end" style="background: var(--primary-color) !important">
          <h4 class="text-white mb-0"><?=$this->setting_web_name?></h4>
          <small class="text-white"><?=GetSetting('SETTING_ORG_REGION')?></small>
          <p class="text-white mb-0 mt-4">Unit Layanan:</p>
          <ul>
            <li class="text-white"><a href="#" data-toggle="modal" data-target="#modalSubbagPelayananPublik" style="margin-top: 0 !important">Pelayanan Publik & Tata Laksana</a></li>
            <li class="text-white"><a href="#" data-toggle="modal" data-target="#modalSubbagKelembagaan" style="margin-top: 0 !important">Kelembagaan & Analisis Jabatan</a></li>
            <li class="text-white"><a href="#" data-toggle="modal" data-target="#modalSubbagKinerja" style="margin-top: 0 !important">Kinerja & Reformasi Birokrasi</a></li>
          </ul>
        </div>
      </div>

    </div>
  </div>
</section>
<section class="job-section recent-jobs-section section-padding">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-6 col-12 mb-4">
        <h4>Artikel / Berita Terkini</h4>
        <p>Berita terkini seputar kegiatan <?=ucwords(strtolower($this->setting_web_name))?>.</p>
      </div>
      <div class="clearfix"></div>
      <?php
      foreach($berita as $b) {
        $strippedcontent = strip_tags($b[COL_POSTCONTENT]);
        $tags = explode(",",$b[COL_POSTMETATAGS]);
        $img = $this->db->where(COL_ISTHUMBNAIL,1)->where(COL_POSTID, $b[COL_POSTID])->get(TBL__POSTIMAGES)->row_array();
        ?>
        <div class="col-lg-4 col-md-6 col-12">
          <div class="job-thumb job-thumb-box">
            <div
            class="job-image-box-wrap"
            style="
              height: 250px;
              width: 100%;
              background-image: url('<?=!empty($img)&&file_exists(MY_UPLOADPATH.$img[COL_IMGPATH])?MY_UPLOADURL.$img[COL_IMGPATH]:MY_IMAGEURL.'no-image.png'?>');
              background-size: cover;
              background-repeat: no-repeat;
              background-position: center;
            ">
              <div class="job-image-box-wrap-info d-flex align-items-center">
                <?php
                if(!empty($tags)) {
                  ?>
                  <p class="mb-0">
                    <?php
                    $ct = 0;
                    foreach($tags as $t) {
                      if($ct>2) break;
                      ?>
                      <span class="badge badge-level"><?=(strlen($t) > 10 ? substr(strtoupper($t), 0, 10) . "..." : strtoupper($t))?></span>
                      <?php
                      $ct++;
                    }
                    ?>
                  </p>
                  <?php
                }
                ?>
              </div>
            </div>
            <div class="job-body" style="min-height: 320px; max-height: 320px">
              <h5 class="job-title">
                <a href="<?=site_url('site/home/page/'.$b[COL_POSTSLUG])?>" class="job-title-link"><?=strlen($b[COL_POSTTITLE]) > 60 ? substr($b[COL_POSTTITLE], 0, 60) . "..." : $b[COL_POSTTITLE] ?></a>
              </h5>
              <div class="d-flex align-items-center">
                <p class="job-location"><i class="custom-icon far fa-user-circle"></i>&nbsp;&nbsp;<?=$b[COL_NAME]?></p>
                <p class="job-date"><i class="custom-icon far fa-calendar"></i>&nbsp;&nbsp;<?=date('d-m-Y', strtotime($b[COL_CREATEDON]))?></p>
              </div>
              <div class="border-top pt-3">
                <p class="job-price"><?=strlen($strippedcontent) > 150 ? substr($strippedcontent, 0, 150) . "..." : $strippedcontent ?></p>
              </div>
            </div>
          </div>
        </div>
        <?php
      }
      ?>
      <div class="col-lg-12 col-12 recent-jobs-bottom d-flex ms-auto my-4">
        <a href="<?=site_url('site/home/post/1')?>" class="custom-btn btn ms-lg-auto" style="font-size: 14pt; padding: 15px 25px">Lihat Selengkapnya <i class="far fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
<section id="contact-section" class="reviews-section section-padding">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-12">
        <h3 class="text-center mb-5">Hubungi Kami</h3>
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col-lg-6 col-12 mb-lg-5 mb-3">
        <?=GetSetting('SETTING_LINK_GOOGLEMAP')?>
      </div>
      <div class="col-lg-5 col-12 mb-3 mx-auto">
        <div class="reviews-thumb" style="padding: 20px !important">
          <div class="contact-info d-flex align-items-center mb-3">
            <i class="custom-icon bi-building"></i>
            <p class="mb-0">
              <span class="contact-info-small-title">Alamat</span>
              <?=GetSetting('SETTING_ORG_ADDRESS')?>
            </p>
          </div>
          <div class="contact-info d-flex align-items-center">
            <i class="custom-icon bi-envelope"></i>
            <p class="mb-0">
              <span class="contact-info-small-title">Email</span>
              <a href="mailto:<?=GetSetting('SETTING_ORG_MAIL')?>" class="site-footer-link"><?=GetSetting('SETTING_ORG_MAIL')?></a>
            </p>
          </div>
          <div class="contact-info d-flex align-items-center">
            <i class="custom-icon bi-facebook"></i>
            <p class="mb-0">
              <span class="contact-info-small-title">Facebook</span>
              <a href="<?=GetSetting('SETTING_LINK_FACEBOOK')?>" class="site-footer-link" target="_blank"><?=$this->setting_web_name.' '.GetSetting('SETTING_ORG_REGION')?></a>
            </p>
          </div>
          <div class="contact-info d-flex align-items-center">
            <i class="custom-icon bi-instagram"></i>
            <p class="mb-0">
              <span class="contact-info-small-title">Instagram</span>
              <a href="<?=GetSetting('SETTING_LINK_INSTAGRAM')?>" class="site-footer-link" target="_blank"><?=GetSetting('SETTING_LINK_INSTAGRAM_ACC')?></a>
            </p>
          </div>
          <div class="contact-info d-flex align-items-center">
            <i class="custom-icon bi-youtube"></i>
            <p class="mb-0">
              <span class="contact-info-small-title">YouTube</span>
              <a href="<?=GetSetting('SETTING_LINK_YOUTUBE')?>" class="site-footer-link" target="_blank"><?=$this->setting_web_name.' '.GetSetting('SETTING_ORG_REGION')?></a>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div id="init" class="modal">
  <h5 style="color: var(--primary-color)">Selamat Datang!</h5>
  <p>Apakah anda merupakan penerima layanan publik di lingkungan Pemerintah Kabupaten Batu Bara?</p>
  <p>Mohon kesediaan anda untuk mengisi <strong>Survei Kepuasan Masyarakat</strong> melalui link berikut dalam rangka peningkatan kualitas layanan publik Pemerintah Kabupaten Batu Bara.</p>
  <a href="https://bagianorganisasi.batubarakab.go.id/skm/">Isi Survei Kepuasan Masyarakat <i class="far fa-arrow-right"></i></a>
</div>
<script type="text/javascript">
$('.carousel-hero').owlCarousel({
  loop:true,
  margin:10,
  /*nav:true,*/
  items: 1,
  autoplay:true,
  autoplayTimeout:3000,
  autoplayHoverPause:true
});
$('.carousel-galeri').owlCarousel({
  loop:true,
  margin:10,
  nav:false,
  items: 1,
  padding: 0,
  dots: false,
  autoplay:true,
  autoplayTimeout:3000,
  autoplayHoverPause:true
});
$('#init').modal();
</script>
