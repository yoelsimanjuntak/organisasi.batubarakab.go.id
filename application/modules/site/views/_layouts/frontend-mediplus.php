<?php
$numStatToday = $this->db->where('DATE_FORMAT(Timestamp, "%Y-%m-%d")=', date('Y-m-d'))->count_all_results(TBL__LOGS);
$numStatMonthly = $this->db->where('DATE_FORMAT(Timestamp, "%Y-%m")=', date('Y-m'))->count_all_results(TBL__LOGS);
$numStatTotal = $this->db->count_all_results(TBL__LOGS);
?>
<!doctype html>
<html class="no-js" lang="zxx">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="keywords" content="Site keywords here">
  <meta name="description" content="">
  <meta name='copyright' content=''>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title><?=!empty($title)?$title.' - '.$this->setting_web_name:$this->setting_web_name?></title>
  <link rel="icon" type="image/png" href="<?=MY_IMAGEURL.$this->setting_web_logo?>" />

  <!--<link rel="icon" href="<?=base_url()?>assets/themes/mediplus-lite/img/favicon.png">-->
  <!--<link href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">-->
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/mediplus-lite/style-font.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/mediplus-lite/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/mediplus-lite/css/nice-select.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />
  <!--<link rel="stylesheet" href="<?=base_url()?>assets/themes/mediplus-lite/css/icofont.css">-->
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/mediplus-lite/css/slicknav.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/mediplus-lite/css/owl-carousel.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/mediplus-lite/css/datepicker.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/mediplus-lite/css/animate.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/mediplus-lite/css/magnific-popup.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/mediplus-lite/css/normalize.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/mediplus-lite/style.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/mediplus-lite/css/responsive.css">

  <script src="<?=base_url()?>assets/themes/mediplus-lite/js/jquery.min.js"></script>
  <!-- DataTables -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/datatable/media/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

  <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/jquery.dataTables.min.js?ver=1"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/dataTables.bootstrap.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

  <style>
  .nowrap {
    white-space: nowrap;
  }
  td.dt-body-right {
    text-align: right !important;
  }
  #owl-pegawai .owl-item {
    display: flex;
    align-items: center;
    justify-content: center;
  }
  </style>
</head>
<body>
  <div class="preloader">
      <div class="loader">
          <div class="loader-outter"></div>
          <div class="loader-inner"></div>

          <!--<div class="indicator">
              <i class="far fa-cog fa-2x fa-spin"></i>
          </div>-->
      </div>
  </div>
  <!--<ul class="pro-features">
    <a class="get-pro" href="#">Get Pro</a>
    <li class="big-title">Pro Version Available on Themeforest</li>
    <li class="title">Pro Version Features</li>
    <li>2+ premade home pages</li>
    <li>20+ html pages</li>
    <li>Color Plate With 12+ Colors</li>
    <li>Sticky Header / Sticky Filters</li>
    <li>Working Contact Form With Google Map</li>
    <div class="button">
      <a href="http://preview.themeforest.net/item/mediplus-medical-and-doctor-html-template/full_screen_preview/26665910?_ga=2.145092285.888558928.1591971968-344530658.1588061879" target="_blank" class="btn">Pro Version Demo</a>
      <a href="https://themeforest.net/item/mediplus-medical-and-doctor-html-template/26665910" target="_blank" class="btn">Buy Pro Version</a>
    </div>
  </ul>-->
  <header class="header" >
    <!-- Topbar -->
    <div class="topbar" style="padding: 10px 0 !important; background: #323232 !important">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-5 col-12">
            <!-- Contact -->
            <!--<ul class="top-link">
              <li><a href="#">About</a></li>
              <li><a href="#">Doctors</a></li>
              <li><a href="#">Contact</a></li>
              <li><a href="#">FAQ</a></li>
            </ul>-->
            <!-- End Contact -->
          </div>
          <div class="col-lg-6 col-md-7 col-12">
            <!-- Top Contact -->
            <ul class="top-contact social">
              <li><a href=""><i class="fab fa-facebook-square"></a></i></li>
              <li><a href=""><i class="fab fa-instagram"></a></i></li>
              <li><a href=""><i class="fab fa-twitter"></a></i></li>
            </ul>
            <!-- End Top Contact -->
          </div>
        </div>
      </div>
    </div>
    <!-- End Topbar -->
    <!-- Header Inner -->
    <div class="header-inner">
      <div class="container">
        <div class="inner">
          <div class="row">
            <div class="col-lg-4 col-md-4 col-12">
              <!-- Start Logo -->
              <div class="logo">
                <a href="<?=site_url()?>"><img src="<?=base_url()?>assets/themes/mediplus-lite/img/logo.png" alt="#"></a>
              </div>
              <!-- End Logo -->
              <!-- Mobile Nav -->
              <div class="mobile-nav"></div>
              <!-- End Mobile Nav -->
            </div>
            <div class="col-lg-8 col-md-8 col-12">
              <!-- Main Menu -->
              <div class="main-menu">
                <nav class="navigation" style="display: flex !important">
                  <ul class="nav menu" style="margin-left: auto !important">
                    <li><a href="<?=site_url()?>">BERANDA </a></li>
                    <!--<li><a href="#">PROFIL <i class="far fa-chevron-down"></i></a>
                      <ul class="dropdown" style="border-left: none">
                        <li><a href="blog-single.html">Struktur Organisasi</a></li>
                        <li><a href="blog-single.html">Tugas Pokok dan Fungsi</a></li>
                        <li><a href="blog-single.html">Motto </a></li>
                        <li><a href="blog-single.html">Maklumat Pelayanan </a></li>
                      </ul>
                    </li>-->
                    <li><a href="<?=site_url('site/home/post/1')?>">BERITA </a></li>
                    <li><a href="<?=site_url('site/home/post/3')?>">DOKUMEN </a></li>
                    <li><a href="<?=site_url('site/home/sakip')?>">KINERJA PERANGKAT DAERAH </a></li>
                    <!--<li><a href="#">KONTAK </a></li>-->
                    <!--<li><a href="#">Pages <i class="icofont-rounded-down"></i></a>
                      <ul class="dropdown">
                        <li><a href="404.html">404 Error</a></li>
                      </ul>
                    </li>
                    <li><a href="#">Blogs <i class="icofont-rounded-down"></i></a>
                      <ul class="dropdown">
                        <li><a href="blog-single.html">Blog Details</a></li>
                      </ul>
                    </li>
                    <li><a href="contact.html">Contact Us</a></li>-->
                  </ul>
                </nav>
              </div>
              <!--/ End Main Menu -->
            </div>
            <!--<div class="col-lg-2 col-12">
              <div class="get-quote">
                <a href="appointment.html" class="btn">Book Appointment</a>
              </div>
            </div>-->
          </div>
        </div>
      </div>
    </div>
  </header>
  <?=$content?>
  <footer id="footer" class="footer ">
    <!-- Footer Top -->
    <div class="footer-top">
      <div class="container">
        <div class="row">
          <div class="col-lg-5 col-md-6 col-12">
            <div class="single-footer">
              <h2><?=$this->setting_org_name?></h2>
              <p><strong>Alamat</strong></p>
              <p><?=$this->setting_org_address?></p><br />
              <p><strong>No. Telepon</strong></p>
              <p><?=$this->setting_org_phone?$this->setting_org_phone:'-'?></p><br />
              <p><strong>Email</strong></p>
              <p><?=$this->setting_org_mail?$this->setting_org_mail:'-'?></p>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 col-12">
            <div class="single-footer f-link">
              <h2>Link Terkait</h2>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-12">
                  <ul>
                    <li><a href="https://www.batubarakab.go.id/" target="_blank"><i class="fa fa-caret-right" aria-hidden="true"></i>Website Resmi Kabupaten Batu Bara</a></li>
                    <li><a href="https://bagianorganisasi.batubarakab.go.id/sakip/user/login.jsp"><i class="fa fa-caret-right" aria-hidden="true"></i>E-SAKIP</a></li>
                    <li><a href="https://bagianorganisasi.batubarakab.go.id/skm/"><i class="fa fa-caret-right" aria-hidden="true"></i>SI SUKMA</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-12">
            <div class="single-footer f-link">
              <h2>Statistik Pengunjung</h2>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-12">
                  <ul class="time-sidual">
                    <li class="day">HARI INI<span><strong><?=number_format($numStatToday)?></strong></span></li>
  									<li class="day">BULAN INI<span><strong><?=number_format($numStatMonthly)?></strong></span></li>
  									<li class="day">TOTAL<span><strong><?=number_format($numStatTotal)?></strong></span></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--/ End Footer Top -->
    <!-- Copyright -->
    <div class="copyright">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-12">
            <div class="copyright-content">
              <p>© Copyright 2021  -  <?=$this->setting_org_name?>  </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--/ End Copyright -->
  </footer>
  <script src="<?=base_url()?>assets/themes/mediplus-lite/js/jquery-migrate-3.0.0.js"></script>
  <script src="<?=base_url()?>assets/themes/mediplus-lite/js/jquery-ui.min.js"></script>
  <script src="<?=base_url()?>assets/themes/mediplus-lite/js/easing.js"></script>
  <script src="<?=base_url()?>assets/themes/mediplus-lite/js/colors.js"></script>
  <script src="<?=base_url()?>assets/themes/mediplus-lite/js/popper.min.js"></script>
  <script src="<?=base_url()?>assets/themes/mediplus-lite/js/bootstrap-datepicker.js"></script>
  <script src="<?=base_url()?>assets/themes/mediplus-lite/js/jquery.nav.js"></script>
  <script src="<?=base_url()?>assets/themes/mediplus-lite/js/slicknav.min.js"></script>
  <script src="<?=base_url()?>assets/themes/mediplus-lite/js/jquery.scrollUp.min.js"></script>
  <script src="<?=base_url()?>assets/themes/mediplus-lite/js/niceselect.js"></script>
  <script src="<?=base_url()?>assets/themes/mediplus-lite/js/tilt.jquery.min.js"></script>
  <script src="<?=base_url()?>assets/themes/mediplus-lite/js/owl-carousel.js"></script>
  <script src="<?=base_url()?>assets/themes/mediplus-lite/js/jquery.counterup.min.js"></script>
  <script src="<?=base_url()?>assets/themes/mediplus-lite/js/steller.js"></script>
  <script src="<?=base_url()?>assets/themes/mediplus-lite/js/wow.min.js"></script>
  <script src="<?=base_url()?>assets/themes/mediplus-lite/js/jquery.magnific-popup.min.js"></script>
  <!--<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>-->
  <script src="<?=base_url()?>assets/themes/mediplus-lite/js/waypoints.min.js"></script>
  <script src="<?=base_url()?>assets/themes/mediplus-lite/js/bootstrap.min.js"></script>
  <script src="<?=base_url()?>assets/themes/mediplus-lite/js/main.js"></script>
  <script>
  $(document).ready(function() {
    $('a[href="<?=current_url()?>"]').closest('li').addClass('active');
  });
  </script>
</body>
</html>
